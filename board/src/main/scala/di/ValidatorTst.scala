package queens
package di

import scala.collection.Seq

import base.Board


class ValidatorTst
    extends Validator:

  override val name = "optimized"

  override def apply(solution: Seq[Point])
              (using Board): Boolean = true
