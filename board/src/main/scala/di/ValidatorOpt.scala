package queens
package di

import scala.collection.Seq

import scala.util.control.NonLocalReturns.{ returning, throwReturn => thr }

import base.Board

import common.geom.{ row, col }


class ValidatorOpt
    extends Validator:

  override val name = "optimized"

  override def apply(solution: Seq[Point])
              (using board: Board): Boolean = returning {
    if board.isEmpty then thr(ValidatorEmp(solution))

    for
      n <- 0 until board.N
      m <- (n + 1) until board.N
      p = solution(n)
      q = solution(m)
    do

      inline def verifyRows: Boolean = returning {
        for
          i <- 1.+(p.col min q.col) until (p.col max q.col)
        do
          if board(p.row)(i) then
            thr(true)
        false
      }

      inline def verifyColumns: Boolean = returning {
        for
          i <- 1.+(p.row min q.row) until (p.row max q.row)
        do
          if board(i)(p.col) then
            thr(true)
        false
      }

      inline def verifyMainDiagonals: Boolean = returning {
        var m = p.row min q.row
        var M = p.row max q.row
        var s = if p.row < q.row then p.col else q.col
        s -= m
        for
          i <- m+1 until M
        do
          if board(i)(s + i) then
            thr(true)
        false
      }

      inline def verifyDiagonals: Boolean = returning {
        val m = p.row min q.row
        val M = p.row max q.row
        var s = if p.row < q.row then p.col else q.col
        s += m
        for
          i <- m+1 until M
        do
          if board(i)(s - i) then
            thr(true)
        false
      }

      if false ||
        p.row == q.row && !verifyRows ||
        p.col == q.col && !verifyColumns ||
        p.row - p.col == q.row - q.col && !verifyMainDiagonals ||
        p.row + p.col == q.row + q.col && !verifyDiagonals ||
        false
      then
        thr(false)

    true
  }
