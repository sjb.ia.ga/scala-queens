package queens
package di

import scala.collection.Seq

import scala.util.control.NonLocalReturns.{ returning, throwReturn => thr }

import base.Board

import common.geom.{ row, col }


object ValidatorEmp
    extends Validator:

  override val name = "empty board"

  override def apply(solution: Seq[Point])
              (using board: Board): Boolean = returning {
    for
      n <- 0 until board.N
      m <- (n + 1) until board.N
      p = solution(n)
      q = solution(m)
    do
      if false ||
        p.row == q.row ||
        p.col == q.col ||
        p.row - p.col == q.row - q.col ||
        p.row + p.col == q.row + q.col ||
        false
      then
        thr(false)
    true
  }
