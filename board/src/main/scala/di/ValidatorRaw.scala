package queens
package di

import scala.collection.Seq

import scala.util.control.NonLocalReturns.{ returning, throwReturn => thr }

import base.Board

import common.geom.{ row, col }


class ValidatorRaw
    extends Validator:

  override val name = "raw"

  override def apply(solution: Seq[Point])
              (using board: Board): Boolean = returning {
    if board.isEmpty then thr(ValidatorEmp(solution))

    for
      n <- 0 until board.N
      m <- 0 until board.N
      p = solution(n)
      q = solution(m)
      if p != q
    do

      inline def verifyRows: Boolean = returning {
        for
          i <- (p.col min q.col) to (p.col max q.col)
        do
          if board(p.row)(i) then
            thr(true)
        false
      }

      inline def verifyColumns: Boolean = returning {
        for
          i <- (p.row min q.row) to (p.row max q.row)
        do
          if board(i)(p.col) then
            thr(true)
        false
      }

      inline def verifyMainDiagonals: Boolean = returning {
        for
          i <- (p.row min q.row) to (p.row max q.row)
        do
          if board(i)(if p.row < q.row then p.col + (i - p.row) else q.col + (i - q.row)) then
            thr(true)
        false
      }

      inline def verifyDiagonals: Boolean = returning {
        for
          i <- (p.row min q.row) to (p.row max q.row)
        do
          if board(i)(if p.row < q.row then p.col - (i - p.row) else q.col - (i - q.row)) then
            thr(true)
        false
      }

      if false ||
        p.row == q.row && !verifyRows ||
        p.col == q.col && !verifyColumns ||
        p.row - p.col == q.row - q.col && !verifyMainDiagonals ||
        p.row + p.col == q.row + q.col && !verifyDiagonals ||
        false
      then
        thr(false)

    true
  }
