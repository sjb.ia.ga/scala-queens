package queens
package base

import scala.collection.mutable.{ ListBuffer => MutableList }

import common.geom.{ row, col }


case class Board(N: Int, private[Board] val squares: List[List[Boolean]], seed: Option[Long]):
  require(squares.length == N && squares.forall(row => row.length == N))

  def this(squares: List[List[Boolean]])(using seed: Option[Long] = None) = this(squares.size, squares, seed)

  @inline def apply(i: Int)(j: Int): Boolean = squares(i)(j)

  def apply[T](f: (Int, Int) => T)(x: T): Seq[Seq[T]] =
    for
      i <- 0 until N
    yield
      for
        j <- 0 until N
      yield
        if squares(i)(j) then x
        else f(i, j)

  val isEmpty = squares.forall(!_.exists(identity))

  override def toString: String = this
    .apply((i, j) => "∴")("⬡")
    .map(_ mkString " ")
    .mkString("\n")


object Board:

  import scala.util.Random

  def apply(N: Int)(using seed: Option[Long] = None): Board =
    require(0 < N && N < 10)

    def rand = math.random * 5000000

    def rand1k = ((rand + rand).toInt / 10) % 1000000

    val min = (N * N * (if N < 5 then 0.3 else if N < 8 then 0.3 else 0.3)).toInt
    val add = 1 max (N * N * (if N < 5 then 0.3 else if N < 8 then 0.4 else 0.5)).toInt

    var rest = N * N

    while rest >= N * N do
      rest = (rand1k % add) + min

    val board: List[MutableList[Boolean]] = List.fill(N)(MutableList.fill(N)(false))

    while rest > 0 do
      val i, j = rand1k % N
      if !board(i)(j) then
        board(i)(j) = true
        rest -= 1

    var squares: List[List[Boolean]] = Nil

    for i <- 1 to N do squares = board(N - i).toList :: squares

    new Board(squares)


  extension(self: Board)
    def apply(p: Point): Board =
      val squares: List[MutableList[Boolean]] = List.fill(self.N)(MutableList())

      for
        i <- 0 until self.N
      do
        squares(i).addAll(self.squares(i))

      squares(p.row)(p.col) = true

      new Board(squares.map(_.toList))(using self.seed)

    def apply(solution: Solution): Board =
      assert(self.N == solution.size)

      val squares: List[MutableList[Boolean]] = List.fill(self.N)(MutableList())

      for
        i <- 0 until self.N
      do
        squares(i).addAll(self.squares(i))

      solution.map { p => squares(p.row)(p.col) = true }

      new Board(squares.map(_.toList))(using self.seed)


  import java.util.{ Date, UUID }
  import java.nio.file.Path

  type IO[T] = ((Board, T, Long, UUID), Option[LazyList[Solution]])

  class Cycle[T](
    val io: IO[T],
    val turn: Cycle[T],
    var image: Option[Path] = None
  )(var since: Date = null,
    var idle: Long = -1L
  ) extends Board(io._1._1.N, io._1._1.squares, io._1._1.seed):

    override def hashCode(): Int = (io, turn, image, since, idle).##

    override def equals(any: Any): Boolean = any match
      case that: Cycle[T] =>
        this.io == that.io &&
        this.image == that.image &&
        this.since == that.since &&
        this.idle == that.idle &&
        this.turn == that.turn
      case _ => false
