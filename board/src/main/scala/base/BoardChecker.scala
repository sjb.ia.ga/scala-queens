package queens
package base

import common.geom.{ row, col }


abstract trait BoardChecker:

  protected abstract trait Check:
    @inline def apply(q: Point)
                     (using Board)
                     (using Iterable[Point]): Boolean

  protected object EmptyBoardCheck extends Check:
    inline override def apply(q: Point)
                             (using Board)
                             (using it: Iterable[Point]): Boolean =
      it.exists(_.col == q.col)

  protected object BoardCheck extends Check:
    inline override def apply(q: Point)
                             (using board: Board)
                             (using Iterable[Point]): Boolean =
      board(q.row)(q.col)

  inline final protected def check(q: Point)
                                  (using board: Board)
                                  (using Iterable[Point]): Boolean =
    (if board.isEmpty then EmptyBoardCheck else BoardCheck)(q)
