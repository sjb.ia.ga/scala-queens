package queens
package base


class EmptyBoard(n: Int)(using s: Option[Long] = None)
    extends Board(n, List.fill(n)(List.fill(n)(false)), s):

  inline override def apply(i: Int)(j: Int): Boolean = false

  override val isEmpty = true
