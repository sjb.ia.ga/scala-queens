package queens
package common


package object geom:

  type Coord[T] = (T, T)

  extension [T](row: T)
    inline infix def x(col: T): Coord[T] = (row, col)

  extension [T](self: Coord[T])
    @inline def row: T = self._1
    @inline def col: T = self._2

  object Implicits:

    implicit sealed class `Coord*`[T](self: Coord[T]):
      @inline def row: T = self._1
      @inline def col: T = self._2
