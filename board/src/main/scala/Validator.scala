package queens

import scala.collection.Seq

import base.Board
import di.{ ValidatorEmp, ValidatorRaw, ValidatorOpt, ValidatorTst, Validator2 }


abstract trait Validator:

  def name: String

  def apply(solution: Seq[Point])(using Board): Boolean


object Validator:

  def apply(): Validator =
    val plainjava = sys.BooleanProp
      .keyExists(_root_.queens.main.Properties.Validator.plainjava)
      .value
    if plainjava
    then
      Validator2.$
    else
      val opt = sys.BooleanProp
        .keyExists(_root_.queens.main.Properties.Validator.optimized)
        .value
      if opt
      then
        new ValidatorOpt
      else
        new ValidatorRaw
