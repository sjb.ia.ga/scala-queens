package queens
package main


object Properties:

  object Validator:

    val optimized = "queens.di.validator.optimized"
    val plainjava = "queens.di.validator.plainjava"

  object CipherServer:

    val timeout = "queens.common.cipher.server.timeout"
    val host = "queens.common.cipher.server.host"
    val port = "queens.common.cipher.server.port"

  object DependencyInjection:

    val config = "queens.nest.config.mapper"
    val build = "queens.nest.config.builder"
    val guice = "queens.nest.di.guice"
    val macwire = "queens.nest.di.macwire"

  object WebSimple:

    val plainjava = "queens.web.simple.plainjava"
    val scheme = "queens.web.simple.scheme"
    val host = "queens.web.simple.host"
    val port = "queens.web.simple.port"
    val infra = "queens.web.simple.infra"

  object WebCue:

    val port = "queens.web.cue.port"
    val timeout = "queens.web.cue.timeout"
