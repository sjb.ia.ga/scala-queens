package object queens {

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]

}
