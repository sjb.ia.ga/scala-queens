package queens.di;

import static java.lang.Math.min;
import static java.lang.Math.max;

import scala.Tuple2;
import scala.collection.Seq;

import queens.base.Board;

import queens.Validator;


public final class Validator2 implements Validator {

    private static volatile Validator2 instance = null;

    public Validator2() {}

    synchronized public static Validator2 $() {
        if (instance == null)
            instance = new Validator2();

        return instance;
    }

    @Override
    public String name() {
        return "optimized2";
    }

    @Override
    public boolean apply(Seq<Tuple2<Object, Object>> solution, Board board) {
        final int N = board.N();

        if (board.isEmpty()) {
            for (int n = 0; n < N; n++)
                for (int k = n + 1; k < N; k++) {
                    Tuple2<Object, Object> p = solution.apply(n);
                    Tuple2<Object, Object> q = solution.apply(k);

                    int p_row = (Integer) p._1(); int p_col = (Integer) p._2();
                    int q_row = (Integer) q._1(); int q_col = (Integer) q._2();

                    if (p_row == q_row ||
                        p_col == q_col ||
                        p_row - p_col == q_row - q_col ||
                        p_row + p_col == q_row + q_col)
                        return false;
                }
            return true;
        }

        for (int n = 0; n < N; n++)
            for (int k = n + 1; k < N; k++) {
                Tuple2<Object, Object> p = solution.apply(n);
                Tuple2<Object, Object> q = solution.apply(k);

                int p_row = (Integer) p._1(); int p_col = (Integer) p._2();
                int q_row = (Integer) q._1(); int q_col = (Integer) q._2();

                if (p_row == q_row) { // verifyRows
                    int m = min(p_col, q_col);
                    int M = max(p_col, q_col);
                    int i = 1 + m;
                    for (; i < M; i++)
                        if (board.apply(p_row, i))
                            break;
                    if (i == M)
                        return false;
                }

                if (p_col == q_col) { // verifyColumns
                    int m = min(p_row, q_row);
                    int M = max(p_row, q_row);
                    int i = 1 + m;
                    for (; i < M; i++)
                        if (board.apply(i, p_col))
                            break;
                    if (i == M)
                        return false;
                }

                if (p_row - p_col == q_row - q_col) { // verifyMainDiagonals
                    int m = min(p_row, q_row);
                    int M = max(p_row, q_row);
                    int s = p_row < q_row ? p_col : q_col;
                    s = s - m;
                    int i = m + 1;
                    for (; i < M; i++)
                        if (board.apply(i, s + i))
                            break;
                    if (i == M)
                        return false;
                }

                if (p_row + p_col == q_row + q_col) { // verifyDiagonals
                    int m = min(p_row, q_row);
                    int M = max(p_row, q_row);
                    int s = p_row < q_row ? p_col : q_col;
                    s = s + m;
                    int i = m + 1;
                    for (; i < M; i++)
                        if (board.apply(i, s - i))
                            break;
                    if (i == M)
                        return false;
                }
            }

        return true;
    }

}
