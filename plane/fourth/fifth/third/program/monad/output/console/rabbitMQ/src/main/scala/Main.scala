package queens
package plane.fourth.fifth.third
package program.monad.output
package console
package rabbitMQ

import java.util.Properties

import scala.collection.JavaConverters.asJava
import scala.collection.JavaConverters.iterableAsScalaIterableConverter

import com.rabbitmq.client.{ ConnectionFactory, Connection, Channel, DeliverCallback }

import base.Board

import common.geom.x

import common.monad.rabbitMQ._
import common.monad.tag.integer.given

import common.pipeline.Context.Round

import dimensions.third.`Queens*`

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mco.rabbitMQ.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.`Console'o`

import version.v1.vector.breed.`Hatch'em, Spawn'it, Console'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Classic._
import Stepper._
import Algorithm._
import Recursive._
import Model.{ rabbitMQ, Messaging }
import Messaging.RabbitMQ

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.rabbitMQ.RabbitMQApp:

  TryBoard()

  def main(args: Array[String]): Unit =

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        // case (Aspect, Stepper(OneIncrement)) => true
        // case (Aspect, Stepper(Permutations)) => true
        case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Model, Messaging(RabbitMQ)) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case Nest(_, Recursive(_), _) => 1
      case Nest(_, Iterative, _) => 0
      case _ => 0
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Board = TryBoard(if args.isEmpty then 4 else args(0).toInt, true)

    println(given_Board)


    given Hatch.Flavor.Value = Hatch.Flavor.config

    Hatcher()


    val factory = ConnectionFactory()
    //factory.setHost("rabbitmq-container")
    factory.setHost("localhost")

    val connection = factory.newConnection()
    var channel: Channel = null

    given Ps = Ps {
      connection.createChannel()
    }(_.`Console'o Header` = false) // foreach/map/flatMap


    val hive: Iterable[Hive[Ps]] = for {
      it <- wildcard(multiplier)
    } yield {
      val hatch = Hatcher(it)
      val spawn = `Console'o`(it.model)
      Hive(hatch, spawn)()
    }


    try

      var f = false

      val emito = `Hatch'em, Spawn'it, Console'o`(hive)

      given Boolean = false // preemption off

      for
        o <- emito `#` 1
        if o(_.model == rabbitMQ)
        it_Nil <- o
      do
        o(it_Nil) {
          val Just(_, tag, uuid, _, major, count) = o `just get` it_Nil
          val minor = uuid.toString.replace('-', '_')

          channel = connection.createChannel()
          val queueName = channel.queueDeclare(s"${major}_${minor}", true, false, false, null).getQueue

          val deliverCb: DeliverCallback = { (_, delivery) =>
            val message = String(delivery.getBody, "UTF-8")
            val ls = message.split("\n").toList
            val n = ls.head.toLong
            val it = ls.tail.sliding(2, 2).foldLeft(List[Point]()) {
              case (r, List(row, col, _*)) =>
                r :+ (row.toInt x col.toInt)
            }
            var queens: `Queens*`[?, ?] = null
            o { qs => queens = qs; true }
            o.the.||(it, Round(given_Board, tag, queens, n), Some(uuid))
          }

          for
            _ <- 1L to count.toLong
          do
            channel.basicConsume(queueName, true, deliverCb, { _ => })

          f = true
        }

      if !f then
        println(s"!!! NO SOLUTIONS !!!")

    finally
      if channel ne null then channel.close
      given_Ps.it.close
      connection.close
