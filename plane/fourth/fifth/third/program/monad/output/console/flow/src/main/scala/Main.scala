package queens
package plane.fourth.fifth.third
package program.monad.output
package console
package flow

import base.Board
import common.monad.Item

import common.geom.{ row, col }

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mco.flow.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.`Console'o`

import version.v1.vector.breed.`Hatch'em, Spawn'it, Console'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Classic._
import Stepper._
import Algorithm._
import Recursive._
import Model.{ flow, Flow }

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.flow.FlowApp:

  TryBoard()

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.plainjava)
    .enable()

  def main(args: Array[String]): Unit =

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        case (Aspect, Classic(Callback)) => false
        // case (Aspect, Stepper(OneIncrement)) => true
        // case (Aspect, Stepper(Permutations)) => true
        // case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(TailCall)) => false
        case (Algorithm, Recursive(CatsEval)) => false
        case (Algorithm, Recursive(Continuation)) => false
        case (Algorithm, Recursive(Closure)) => false
        case (Algorithm, Recursive(Trampoline)) => false
        case (Algorithm, Recursive(Native)) => true
        case (Model, Flow) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case Nest(_, Recursive(_), _) => 1
      case Nest(_, Iterative, _) => 1
      case _ => 0
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Board = TryBoard(if args.isEmpty then 6 else args(0).toInt, empty = true)

    println(given_Board)


    given Hatch.Flavor.Value = Hatch.Flavor.build

    Hatcher()


    given Ps = Ps(_.`Console'o Header` = true)

    val hive: Iterable[Hive[Ps]] = for {
      it <- wildcard(multiplier)
    } yield {
      val hatch = Hatcher(it)
      val spawn = `Console'o`(it.model)
      Hive(hatch, spawn)()
    }

    val emito = `Hatch'em, Spawn'it, Console'o`(hive)


    given Boolean = false // preemption off

    var f = false

    if false then
      given_Ps.`Console'o Header` = false

      given Boolean = true // preemption on

      def stop: Boolean = prompt("stop", false)

      emito.foreach { o =>
          if o(_.model == flow) then
            o.foreach { (it: Item[Solution]) =>
              if it /* not preempt */ && it.exists(_.row == 0) && it.exists(_.col == 0) then
                f = true
                o(it) { !stop }
            }
      }

    if false then
      given Boolean = false // preemption off

      emito.foreach { o =>
        if o(_.model == flow) then
          for
            it <- o
            if it.exists(_.row == 0) && it.exists(_.col == 0)
          do
            o(it) { f = true }
      }

    if true then
      given Boolean = false // preemption off

      for
        o <- emito
        if o(_.model == flow)
        it <- o
        if it.exists(_.row == 0) && it.exists(_.col == 0)
      do
        o(it) { f = true }

    if !f then
      println("!!! NO SOLUTIONS !!!")
