package queens
package plane.fourth.fifth.third
package program.monad.output
package console
package awsSQS

import scala.collection.JavaConverters.iterableAsScalaIterableConverter

import com.amazonaws.auth.{ AWSStaticCredentialsProvider, BasicAWSCredentials }
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.sqs.AmazonSQSClientBuilder

import base.Board

import common.geom.x

import common.monad.awsSQS._
import common.monad.tag.integer.given

import common.pipeline.Context.Round

import dimensions.third.`Queens*`

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mco.awsSQS.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.`Console'o`

import version.v1.vector.breed.`Hatch'em, Spawn'it, Console'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Classic._
import Stepper._
import Algorithm._
import Recursive._
import Model.{ awsSQS, Messaging }
import Messaging.AWSSQS

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.awsSQS.AWSSQSApp:

  TryBoard()

  def main(args: Array[String]): Unit =

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        // case (Aspect, Stepper(OneIncrement)) => true
        // case (Aspect, Stepper(Permutations)) => true
        case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Model, Messaging(AWSSQS)) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case Nest(_, Recursive(_), _) => 1
      case Nest(_, Iterative, _) => 0
      case _ => 0
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Board = TryBoard(if args.isEmpty then 4 else args(0).toInt, true)

    println(given_Board)


    given Hatch.Flavor.Value = Hatch.Flavor.config

    Hatcher()


    given Ps = Ps {
      //https://github.com/softwaremill/elasticmq#using-the-amazon-java-sdk-to-access-an-elasticmq-server
      //val endpoint = "http://elasticmq-container:9324"
      val endpoint = "http://localhost:9324"
      val region = "elasticmq"
      val accessKey = "x"
      val secretKey = "x"
      AmazonSQSClientBuilder.standard()
        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
        .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, region))
        .build()
    }(_.`Console'o Header` = false) // foreach/map/flatMap


    val hive: Iterable[Hive[Ps]] = for {
      it <- wildcard(multiplier)
    } yield {
      val hatch = Hatcher(it)
      val spawn = `Console'o`(it.model)
      Hive(hatch, spawn)()
    }


    val sqsClient = {
      //https://github.com/softwaremill/elasticmq#using-the-amazon-java-sdk-to-access-an-elasticmq-server
      //val endpoint = "http://elasticmq-container:9324"
      val endpoint = "http://localhost:9324"
      val region = "elasticmq"
      val accessKey = "x"
      val secretKey = "x"
      AmazonSQSClientBuilder.standard()
        .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
        .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endpoint, region))
        .build()
    }


    var f = false

    val emito = `Hatch'em, Spawn'it, Console'o`(hive)

    given Boolean = false // preemption off

    for
      o <- emito `#` 1
      if o(_.model == awsSQS)
      it_Nil <- o
    do
      o(it_Nil) {
        val Just(_, tag, uuid, _, major, count) = o `just get` it_Nil
        val minor = uuid.toString.replace("-", "")

        val queueUrl = sqsClient.getQueueUrl(s"$major$minor").getQueueUrl

        for
          _ <- 1L to count.toLong
          message <- sqsClient.receiveMessage(queueUrl).getMessages.asScala
        do
          val ls = message.getBody.split("\n").toList
          val n = ls.head.toLong
          val it = ls.tail.sliding(2, 2).foldLeft(List[Point]()) {
            case (r, List(row, col, _*)) =>
              r :+ (row.toInt x col.toInt)
          }
          var queens: `Queens*`[?, ?] = null
          o { qs => queens = qs; true }
          o.the.||(it, Round(given_Board, tag, queens, n), Some(uuid))

        f = true
      }

    if !f then
      println(s"!!! NO SOLUTIONS !!!")
