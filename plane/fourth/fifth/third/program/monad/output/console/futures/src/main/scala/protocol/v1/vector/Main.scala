package queens
package plane.fourth.fifth.third
package program.monad.output
package console
package protocol.v1.vector
package futures

import java.util.UUID

import scala.collection.mutable.{ HashMap => Map }
import scala.collection.mutable.{ ListBuffer => MutableList }

import scala.concurrent.{ Future, ExecutionContext }

import akka.actor.typed.{ ActorSystem, Behavior }

import akka.actor.typed.scaladsl.Behaviors

import base.Board

import common.geom.{ row, col }

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mco.futures.{ Parameters => Ps }

import version.v1.vector.breed.`Hatch'em, Spawn'it, Console'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ futures, Parallel }
import dimensions.Dimension.Model.Parallel.Futures

import version.less.nest.Nest
import version.v1.vector.{ VectorV1, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.futures.FuturesApp:

  TryBoard()

  {
    import sys.Prop.StringProp
    sys.Prop("scala.concurrent.context.numThreads").set("x32")
  }

  import sys.Prop.IntProp
  sys.Prop("scala.concurrent.context.maxThreads").set("8192")
  sys.Prop("scala.concurrent.context.maxExtraThreads").set("8192")
  sys.Prop("scala.concurrent.context.minThreads").set("512")
  sys.Prop("java.util.concurrent.ForkJoinPool.common.parallelism").set("4096")

  sys.Prop(_root_.queens.main.Properties.CipherServer.port)
    .set("9999")

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  def main(args: Array[String]): Unit =

    given Board = TryBoard(if args.isEmpty then 4 else args(0).toInt)

    println(given_Board)


    val M = 1 // # futures iterations

    implicit val fs: Map[(Int, UUID), Seq[(Long, Option[Future[?]])]] = Map()

    val (ec, cbs) = this(M, `*`(), `**`)

    given ExecutionContext = ec

    given Ps = Ps(cbs)(_.`Console'o Header` = false) // foreach/map/flatMap

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue


    val wildcard = Wildcard(
      version.less.nest.Wildcard( (_, _) match {
        case (Aspect, Classic(Straight)) => true
        case (Aspect, Stepper(OneIncrement)) => true
        case (Aspect, Stepper(Permutations)) => false
        case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Model, Parallel(Futures)) => true
        case _ => false
      }),
      version.v1.mco.Wildcard.*[Ps]
    )

    val multiplier = Multiplier[VectorV1[Ps]] {
      case VectorV1[Ps](Nest(_, Iterative, _), _) => 0
      case _ => 1
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Hatch.Flavor.Value = Hatch.Flavor.build

    Hatcher()


    var size = 0
    val hive = MutableList[Hive[Ps]]()


    ActorSystem(queens(), "Queens") ! true

    def queens(): Behavior[Boolean] =
      Behaviors.setup { context => Behaviors
        .receiveMessage { st =>

          if st
          then

            import version.v1.vector.protocol.VectorV1Message
            import version.v1.vector.interface.ObjectVectorV1ProtocolIfce

            import version.v1.vector.Implicits._

            size = implicitly[ObjectVectorV1ProtocolIfce[VectorV1Message]]
              .SessionExpand(context,
                version.base.role.isInitial(_),
                { () => context.self ! false },
                println(_),
                { (_: VectorV1[Ps], it: Option[?] => Hive[Ps]) =>
                  hive += it(None)
                },
                wildcard,
                multiplier
              )

            if size > 0
            then
              Behaviors.same
            else
              Behaviors.stopped

          else

            var f = false

            try

              val emito = `Hatch'em, Spawn'it, Console'o`(hive)

              try
                stop(start)

                given Boolean = false // preemption off

                for
                  o <- emito `#` 1
                  // if o(_.model == futures)
                  it <- o
                  // if it.exists(_.row == 0) && it.exists(_.col == 0)
                do
                  o(it) { f = true }
                  if end then
                    o stop it

                `#`(1 -> hive.size)()

              finally
                stop()

            finally
              this()

              if !f then
                println("!!! NO SOLUTIONS !!!")

            Behaviors.stopped

        }

      }
