package queens
package plane.fourth.fifth.third
package program.monad.output
package console
package protocol.less.nest
package actors

import java.util.UUID

import scala.collection.mutable.{ ListBuffer => MutableList }
import scala.collection.mutable.{ HashMap => Map }

import akka.actor.typed.{ ActorSystem, ActorRef, Behavior }

import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import base.Board

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mco.actors.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.`Console'o`

import version.v1.vector.breed.`Hatch'em, Spawn'it, Console'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ actors, Parallel }
import dimensions.Dimension.Model.Parallel.Actors

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import conf.actors.Callbacks


object Main extends plane.fourth.fifth.third.program.monad.output.actors.ActorsApp:

  TryBoard()

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  import sys.Prop.IntProp
  sys.Prop(_root_.queens.main.Properties.CipherServer.port)
    .set("9999")

  def main(args: Array[String]): Unit =

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue


    given Board = TryBoard(if args.isEmpty then 4 else args(0).toInt, empty = true)

    println(given_Board)


    given Hatch.Flavor.Value = Hatch.Flavor.config

    Hatcher()


    var f = false

    val hive = Map[Int, MutableList[Hive[Ps]]]()


    ActorSystem(queens(), "Queens") ! true

    def queens(): Behavior[Boolean] =

      Behaviors.setup { implicit outer => Behaviors
        .receiveMessage { st =>

          if st
          then

            val M = 2 // # actors (iterations)

            implicit val ps: Map[(Int, UUID), Seq[(Long, Option[(ActorContext[?], ActorRef[?])])]] = Map()

            val cbs = this(M, `*`(), `**`)

            (1 to M).foreach(less(cbs) ! _)

            Behaviors.same

          else

            this()

            if !f then
              println(s"!!! NO SOLUTIONS${if end then " [INTERRUPTED]" else ""} !!!")

            Behaviors.stopped

        }
      }

    def less(cbs: Callbacks)(implicit
             rs: Map[(Int, UUID), Seq[(Long, Option[(ActorContext[?], ActorRef[?])])]],
             outer: ActorContext[Boolean]) =
      outer.spawnAnonymous[Int](Behaviors
        .receive { (inner, n) =>

          given Ps = Ps(inner, cbs)(_.`Console'o Header` = false)

          if n > 0
          then
            stop(start)

            sync { hive(n) = MutableList[Hive[Ps]]() }

            val wildcard = Wildcard (
              (_, _) match
                case (Aspect, Classic(Straight)) => true
                // case (Aspect, Stepper(OneIncrement)) => true
                // case (Aspect, Stepper(Permutations)) => true
                case (Algorithm, Iterative) => n == 1
                case (Algorithm, Recursive(Native)) => n == 2
                case (Model, Parallel(Actors)) => true
                case _ => false
            )

            val multiplier = Multiplier[Nest] {
              case Nest(_, Iterative, _) if n == 1 => 1
              case Nest(_, Recursive(Native), _) if n == 2 => 1
              case _ => 0
            }

            if wildcard(multiplier).isEmpty
            then
              println(s"!!! NO #$n EXPANSIONS !!!")

              stop()
              Behaviors.stopped

            else

              import version.less.nest.protocol.NestMessage
              import version.less.nest.interface.ObjectNestProtocolIfce

              import version.less.nest.Implicits._

              val size = implicitly[ObjectNestProtocolIfce[NestMessage]]
                .SessionExpand(inner,
                  version.base.role.isInitial(_),
                  { () => inner.self ! -n },
                  println(_),
                  { (_: Nest, hatch: Hatch[?]) =>
                    val spawn = `Console'o`(actors)
                    sync { hive(n) += Hive(hatch, spawn)() }
                  },
                  wildcard,
                  multiplier
                )

              if size > 0
              then
                Behaviors.same
              else
                stop()
                Behaviors.stopped

          else

            val emito = `Hatch'em, Spawn'it, Console'o`(sync(hive(-n)))

            try

              given Boolean = false // preemption off

              for
                o <- emito `#` -n
                it <- o
              do
                o(it) { f = true }
                if end then
                  o stop it

              `#`(-n -> hive(-n).length)()

            finally
              stop()

            Behaviors.stopped

        }
      )
