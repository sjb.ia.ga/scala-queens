package queens
package plane.fourth.fifth.third
package program.monad.output
package console
package protocol.v2.vector
package futures

import java.util.UUID

import scala.collection.mutable.{ ListBuffer => MutableList }
import scala.collection.mutable.{ HashMap => Map }

import scala.concurrent.{ Future, ExecutionContext }

import akka.actor.typed.{ ActorSystem, Behavior }

import akka.actor.typed.scaladsl.Behaviors

import base.Board

import common.geom.{ row, col }

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mco.futures.{ Parameters => Ps }

import version.v1.vector.breed.`Hatch'em, Spawn'it, Console'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model, Use }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ futures, Parallel }
import dimensions.Dimension.Model.Parallel.Futures
import dimensions.Dimension.Use.consoleOutput

import version.less.nest.Nest
import version.v2.vector.{ VectorV2, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.futures.FuturesApp:

  TryBoard()

  {
    import sys.Prop.StringProp
    sys.Prop("scala.concurrent.context.numThreads").set("x32")
  }

  import sys.Prop.IntProp
  sys.Prop("scala.concurrent.context.maxThreads").set("8192")
  sys.Prop("scala.concurrent.context.maxExtraThreads").set("8192")
  sys.Prop("scala.concurrent.context.minThreads").set("512")
  sys.Prop("java.util.concurrent.ForkJoinPool.common.parallelism").set("4096")

  sys.Prop(_root_.queens.main.Properties.CipherServer.port)
    .set("9999")

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  def main(args: Array[String]): Unit =

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue

    val wildcard = Wildcard( (_, _) match {
      case (Aspect, Stepper(OneIncrement)) => true
      case (Aspect, Stepper(Permutations)) => false
      case (Algorithm, Iterative) => true
      case (Algorithm, Recursive(Native)) => true
      case (Model, Parallel(Futures)) => true
      case (Use, `consoleOutput`) => true
      case (Algorithm, _) | (Aspect, _) | (Model, _) | (Use, _) => false
      case _ => true
      })

    val multiplier = Multiplier[VectorV2] {
      case VectorV2(_, Iterative, _, _, _) => 1
      case _ => 1
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Board = TryBoard(if args.isEmpty then 4 else args(0).toInt)

    println(given_Board)


    given Hatch.Flavor.Value = Hatch.Flavor.build

    Hatcher()


    val M = 1 // # futures iterations

    implicit val fs: Map[(Int, UUID), Seq[(Long, Option[Future[?]])]] = Map()

    val m = Map[Future[?], (Nest, LazyList[Solution])]()

    val (ec, cbs) = this(M, `*`(), `**`)

    given ExecutionContext = ec

    given Ps = Ps(cbs)(_.`Console'o Header` = false) // foreach/map/flatMap


    var size = 0
    val hive = MutableList[Hive[Ps]]()


    ActorSystem(queens(), "Queens") ! true

    def queens(): Behavior[Boolean] =
      Behaviors.setup { context => Behaviors
        .receiveMessage { st =>

          if st
          then

            import version.v2.plane.protocol.PlaneV2Message
            import version.v2.vector.interface.ObjectVectorV2ProtocolIfce

            import version.v2.vector.Implicits._
            import version.v2.vector.console.Implicits.given

            size = implicitly[ObjectVectorV2ProtocolIfce[PlaneV2Message]]
              .SessionExpand(context,
                version.base.role.isInitial(_),
                { () => context.self ! false },
                println(_),
                { (_: VectorV2, it: Option[?] => Hive[Ps]) =>
                  hive += it(None)
                },
                wildcard,
                multiplier
              )

            if size > 0
            then
              Behaviors.same
            else
              Behaviors.stopped

          else

            var f = false

            try

              val emito = `Hatch'em, Spawn'it, Console'o`(hive)

              try
                stop(start)

                given Boolean = false // preemption off

                for
                  o <- emito `#` 1
                  // if o(_.model == futures)
                  it <- o
                  // if it.exists(_.row == 0) && it.exists(_.col == 0)
                do
                  o(it) { f = true }
                  if end then
                    o stop it

                `#`(1 -> hive.size)()

              finally
                stop()

            finally
              this()

              if !f then
                println(s"!!! NO SOLUTIONS${if end then " [INTERRUPTED]" else ""} !!!")

            Behaviors.stopped
          }

        }
