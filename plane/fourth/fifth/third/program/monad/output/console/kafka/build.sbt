resolvers += "confluent" at "https://packages.confluent.io/maven/"

libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",

  "com.typesafe.akka" %% "akka-actor-typed" % "2.8.5",

  "org.apache.kafka" % "kafka-clients" % "7.4.0-ce",
  "org.apache.avro" % "avro" % "1.11.2",
  "io.confluent" % "kafka-avro-serializer" % "7.4.0",

  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
