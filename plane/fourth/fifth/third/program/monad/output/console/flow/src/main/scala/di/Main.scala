package queens
package plane.fourth.fifth.third
package program.monad.output
package console
package flow.di

import base.Board

import common.geom.{ row, col }

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mco.flow.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.`Console'o`

import version.v1.vector.breed.`Hatch'em, Spawn'it, Console'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Classic._
import Stepper._
import Algorithm._
import Recursive._
import Model.{ flow, Flow }

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.flow.FlowApp:

  TryBoard()

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  def main(args: Array[String]): Unit =

    given Long = if args.length > 1 then args(1).toLong else Int.MaxValue

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        // case (Aspect, Stepper(OneIncrement)) => true
        // case (Aspect, Stepper(Permutations)) => true
        // case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        // case (Algorithm, Recursive(TailCall)) => true
        // case (Algorithm, Recursive(CatsEval)) => true
        case (Model, Flow) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case Nest(_, Recursive(_), _) => 1
      case Nest(_, Iterative, _) => 1
      case _ => 0
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Board = {
      var b: Board = null

      var a = false
      while !a do
        b = TryBoard(if args.isEmpty then 6 else args(0).toInt)

        println(b)

        a = prompt("Proceed")

      b
    }


    var f = false


    given Hatch.Flavor.Value = `di-prompt`("DI", Hatch.Flavor.`di-guice`, Hatch.Flavor.`di-macwire`)

    Hatcher()


    given Ps = Ps(_.`Console'o Header` = true)

    val hive: Iterable[Hive[Ps]] = for {
      it <- wildcard(multiplier)
    } yield {
      val hatch = Hatcher(it)
      val spawn = `Console'o`(it.model)
      Hive(hatch, spawn)()
    }

    val emito = `Hatch'em, Spawn'it, Console'o`(hive)

    given Boolean = false // preemption off

    for
      o <- emito
      if o(_.model == flow)
      it <- o
      if it.exists(_.row == 0) && it.exists(_.col == 0)
    do
      o(it) { f = true }

    if !f then
      println("!!! NO SOLUTIONS !!!")
