package object queens:

  import java.util.UUID

  import base.Board

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]

  type Continuation[T] = Option[(Int, Coord[T], Solution, () => Any, Option[?])]


  type Simulate = dimensions.fifth.cycle.Simulate

  type IO = dimensions.fifth.cycle.IO


  import java.io.{ ObjectInputStream, ObjectOutputStream }

  type Load = (ObjectInputStream, () => Unit)
  type Save = Option[ObjectOutputStream] => Boolean


  type Identity_[A] = A

  type Iterable_[X[_], A] = Iterable[A]

  // ScalaTest
