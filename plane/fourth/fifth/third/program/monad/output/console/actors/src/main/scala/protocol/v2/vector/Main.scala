package queens
package plane.fourth.fifth.third
package program.monad.output
package console
package protocol.v2.vector
package actors

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import scala.collection.JavaConverters.mapAsScalaConcurrentMapConverter

import scala.collection.mutable.{ ListBuffer => MutableList }
import scala.collection.mutable.{ HashMap => MutableMap }

import akka.actor.typed.{ ActorSystem, ActorRef, Behavior }

import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import base.Board

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mco.actors.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.`Console'o`

import version.v1.vector.breed.`Hatch'em, Spawn'it, Console'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model, Use }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ actors, Parallel }
import dimensions.Dimension.Model.Parallel.Actors
import dimensions.Dimension.Use.consoleOutput

import version.less.nest.Nest
import version.v2.vector.{ VectorV2, Wildcard }
import version.base.Multiplier

import conf.actors.Callbacks


object Main extends plane.fourth.fifth.third.program.monad.output.actors.ActorsApp:

  TryBoard()

  import sys.Prop.IntProp
  sys.Prop(_root_.queens.main.Properties.CipherServer.port)
    .set("9999")

  def main(args: Array[String]): Unit =

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue


    given Board = TryBoard(if args.isEmpty then 4 else args(0).toInt, empty = true)

    println(given_Board)


    given Hatch.Flavor.Value = Hatch.Flavor.config

    Hatcher()


    var f = false

    var size = 0

    val hive = new Map[ActorRef[?], MutableList[Hive[Ps]]]().asScala


    ActorSystem(queens(), "Queens") ! true

    def queens(): Behavior[Boolean] =

      Behaviors.setup { implicit outer => Behaviors
        .receiveMessage { st =>

          if st
          then

            val M = 2 // # actors (iterations)

            implicit val ps: MutableMap[(Int, UUID), Seq[(Long, Option[(ActorContext[?], ActorRef[?])])]] = MutableMap()

            val cbs = this(M, `*`(), `**`)

            (1 to M).foreach(vectorV2(cbs) ! Left(_))

            Behaviors.same

          else

            this()

            if !f then
              println(s"!!! NO SOLUTIONS${if end then " [INTERRUPTED]" else ""} !!!")

            Behaviors.stopped

        }
      }

    def vectorV2(cbs: Callbacks)(implicit
                 ps: scala.collection.Map[(Int, UUID), Seq[(Long, Option[(ActorContext[?], ActorRef[?])])]],
                 outer: ActorContext[Boolean]) =
      outer.spawnAnonymous[Either[Int, Int]](Behaviors
        .receive { (inner, msg) =>

          given Ps = Ps(inner, cbs)(_.`Console'o Header` = true)

          msg match

            case Left(n) =>

              stop(start)

              val self = inner.self

              hive(self) = MutableList[Hive[Ps]]()

              val wildcard = Wildcard( (_, _) match {
                case (Aspect, Classic(Straight)) => true
                // case (Aspect, Stepper(OneIncrement)) => true
                // case (Aspect, Stepper(Permutations)) => true
                case (Model, Parallel(Actors)) => true
                case (Algorithm, Iterative) => n == 1
                case (Algorithm, Recursive(Native)) => n == 2
                case (Use, `consoleOutput`) => true
                case (Algorithm, _) | (Aspect, _) | (Model, _) | (Use, _) => false
                case _ => true
              })

              val multiplier = Multiplier[VectorV2] {
                case VectorV2(_, Iterative, _, _, _) => 1
                case _ => 1
              }

              if wildcard(multiplier).isEmpty
              then
                println(s"!!! NO #$n EXPANSIONS !!!")

                stop()
                Behaviors.stopped

              else

                import version.v2.plane.protocol.PlaneV2Message
                import version.v2.vector.interface.ObjectVectorV2ProtocolIfce

                import version.v2.vector.Implicits._
                import version.v2.vector.console.Implicits.given

                size = implicitly[ObjectVectorV2ProtocolIfce[PlaneV2Message]]
                  .SessionExpand(inner,
                    version.base.role.isInitial(_),
                    { () => self ! Right(n) },
                    println(_),
                    { (_: VectorV2, it: Option[?] => Hive[Ps]) =>
                      hive(self) += it(None)
                    },
                    wildcard,
                    multiplier
                  )

                if size > 0
                then
                  Behaviors.same
                else
                  stop()
                  Behaviors.stopped

            case Right(n) =>

              val self = inner.self

              val emito = `Hatch'em, Spawn'it, Console'o`(hive(self))

              try

                given Boolean = false // preemption off

                for
                  o <- emito `#` n
                  it <- o
                do
                  o(it) { f = true }
                  if end then
                    o stop it

                `#`(n -> hive(self).size)()

              finally
                stop()

              Behaviors.stopped

          }
        )
