package queens
package plane.fourth.fifth.third
package program.monad.output
package console
package protocol.v1.vector
package flow

import scala.collection.mutable.{ ListBuffer => MutableList }

import akka.actor.typed.{ ActorSystem, Behavior }

import akka.actor.typed.scaladsl.Behaviors

import base.Board

import common.geom.{ row, col }

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mco.flow.{ Parameters => Ps }

import version.v1.vector.breed.`Hatch'em, Spawn'it, Console'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ flow, Flow }

import version.less.nest.Nest
import version.v1.vector.{ VectorV1, Wildcard }
import version.base.Multiplier


object Main:

  TryBoard()

  import sys.Prop.IntProp
  sys.Prop(_root_.queens.main.Properties.CipherServer.port)
    .set("9999")

  def main(args: Array[String]): Unit =

    given Board = TryBoard(if args.isEmpty then 6 else args(0).toInt, empty = true)

    println(given_Board)


    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue

    given Ps = Ps(_.`Console'o Header` = true)


    val wildcard = Wildcard(
      version.less.nest.Wildcard( (_, _) match
        // case (Aspect, Classic(Straight)) => true
        case (Aspect, Stepper(OneIncrement)) => true
        case (Aspect, Stepper(Permutations)) => false
        case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Model, Flow) => true
        case _ => false
      ),
      version.v1.mco.Wildcard.*
    )

    val multiplier = Multiplier[VectorV1[Ps]] {
      case VectorV1[Ps](Nest(_, Iterative, _), _) => 0
      case _ => 1
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Hatch.Flavor.Value = Hatch.Flavor.build

    Hatcher()


    var size = 0
    val hive = MutableList[Hive[Ps]]()


    ActorSystem(queens(), "Queens") ! true

    def queens(): Behavior[Boolean] =
      Behaviors.setup { context => Behaviors
        .receiveMessage { st =>

          if st
          then

            import version.v1.vector.protocol.VectorV1Message
            import version.v1.vector.interface.ObjectVectorV1ProtocolIfce

            import version.v1.vector.Implicits._

            size = implicitly[ObjectVectorV1ProtocolIfce[VectorV1Message]]
              .SessionExpand(context,
                version.base.role.isInitial(_),
                { () => context.self ! false },
                println(_),
                { (v1: VectorV1[Ps], it: Option[?] => Hive[Ps]) =>
                  hive += it(None)
                },
                wildcard,
                multiplier
              )

            if size > 0
            then
              Behaviors.same
            else
              Behaviors.stopped

          else

            var f = false

            val emito = `Hatch'em, Spawn'it, Console'o`(hive)

            given Boolean = false // preemption off

            for
              o <- emito
              // if o(_.model == flow)
              it <- o
              // if it.exists(_.row == 0) && it.exists(_.col == 0)
            do
              o(it) { f = true }

            if !f then
              println("!!! NO SOLUTIONS !!!")

            Behaviors.stopped

        }

      }

