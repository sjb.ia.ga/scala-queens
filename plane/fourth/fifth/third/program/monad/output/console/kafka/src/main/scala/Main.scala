package queens
package plane.fourth.fifth.third
package program.monad.output
package console
package kafka

import java.util.Properties

import scala.collection.JavaConverters.asJava
import scala.collection.JavaConverters.iterableAsScalaIterableConverter

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig
import org.apache.avro.generic.{ GenericData, GenericRecord }
import org.apache.kafka.clients.consumer.{ ConsumerConfig, KafkaConsumer }
import org.apache.kafka.clients.producer.{ ProducerConfig, KafkaProducer }

import base.Board

import common.geom.{ row, x, col }

import common.monad.kafka._
import common.monad.tag.integer.given

import common.pipeline.Context.Round

import dimensions.third.`Queens*`

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mco.kafka.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.`Console'o`

import version.v1.vector.breed.`Hatch'em, Spawn'it, Console'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ kafka, Messaging }
import dimensions.Dimension.Model.Messaging.Kafka

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.kafka.KafkaApp:

  TryBoard()

  def main(args: Array[String]): Unit =

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        // case (Aspect, Stepper(OneIncrement)) => true
        // case (Aspect, Stepper(Permutations)) => true
        case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Model, Messaging(Kafka)) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case Nest(_, Recursive(Native), _) => 1
      case Nest(_, Iterative, _) => 0
      case _ => 0
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Board = TryBoard(if args.isEmpty then 4 else args(0).toInt, true)

    println(given_Board)


    given Hatch.Flavor.Value = Hatch.Flavor.config

    Hatcher()

    given Ps = Ps {
      import org.apache.kafka.common.serialization.StringSerializer
      import io.confluent.kafka.serializers.KafkaAvroSerializer
      val props = Properties()
      props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
      props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[StringSerializer].getCanonicalName)
      props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[KafkaAvroSerializer].getCanonicalName)
      props.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081")
      KafkaProducer[String, GenericRecord](props)
    }(_.`Console'o Header` = false) // foreach/map/flatMap


    val hive: Iterable[Hive[Ps]] = for {
      it <- wildcard(multiplier)
    } yield {
      val hatch = Hatcher(it)
      val spawn = `Console'o`(it.model)
      Hive(hatch, spawn)()
    }


    val props = {
      import org.apache.kafka.common.serialization.StringDeserializer
      import io.confluent.kafka.serializers.KafkaAvroDeserializer
      val props = Properties()
      props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
      props.put(ConsumerConfig.GROUP_ID_CONFIG, "kafka-Main")
      props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")
      props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true)
      props.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, 5000)
      props.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, 10000)
      props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, classOf[StringDeserializer].getCanonicalName)
      props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, classOf[KafkaAvroDeserializer].getCanonicalName)
      props.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081")
      props
    }


    var f = false

    val emito = `Hatch'em, Spawn'it, Console'o`(hive)

    given Boolean = false // preemption off

    for
      o <- emito `#` 1
      if o(_.model == kafka)
      it_Nil <- o
    do
      o(it_Nil) {
        val Just(_, tag, uuid, _, schema, topic) = o `just get` it_Nil

        val co = KafkaConsumer[String, GenericRecord](props)

        co.subscribe(asJava(List(topic)))

        try

          for
            record <- co.poll(1000).asScala
            if record.key == s"$uuid"
          do
            val n = record.value.get("number").asInstanceOf[Long]
            //https://stackoverflow.com/questions/35729253/accessing-nested-fields-in-avro-genericrecord-java-scala?rq=4
            val ps = record.value.get("points").asInstanceOf[GenericData.Array[GenericRecord]]
            val it = ps.asScala.foldLeft(List[Point]()) { (r, p) =>
              r :+ (p.get("row").asInstanceOf[Int] x p.get("column").asInstanceOf[Int])
            }
            var queens: `Queens*`[?, ?] = null
            o { qs => queens = qs; true }
            o.the.||(it, Round(given_Board, tag, queens, n), Some(uuid))

        finally
          co.unsubscribe()

        f = true
      }

    if !f then
      println(s"!!! NO SOLUTIONS !!!")
