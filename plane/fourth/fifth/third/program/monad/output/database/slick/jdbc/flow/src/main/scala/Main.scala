package queens
package plane.fourth.fifth.third
package program.monad.output
package database.slick.jdbc
package flow

import base.Board

import common.geom.{ row, col }

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mdo.slick.flow.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.store.`Slick Store'o`

import version.v1.vector.breed.Hive

import dimensions.Dimension.Use.Output
import dimensions.Dimension.Use.Output.Database
import dimensions.Dimension.Use.Output.Database.Slick
import dimensions.Dimension.Use.Output.Database.Slick.Profile
import dimensions.Dimension.Use.Output.Database.Slick.Profile._

import dimensions.Dimension.{ Aspect, Algorithm, Model, Use }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ flow, Flow }

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.database.slick.jdbc.SlickJdbcDatabaseApp:

  TryBoard()

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  def main(args: Array[String]): Unit =

    given Board = TryBoard(if args.isEmpty then 6 else args(0).toInt, empty = true)

    println(given_Board)


    val (profile, plainSQL) = this  { (profile: Profile) => profile match
                                        case H2 => false // true
                                        case SQLite => true // false
                                    }

    val wildcard = Wildcard {
      (_, _) match {
        case (Aspect, Classic(Straight)) => true
        // case (Aspect, Stepper(OneIncrement)) => true
        // case (Aspect, Stepper(Permutations)) => true
        // case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Model, Flow) => true
        case (Use, Output(Database(Slick(`profile`, `plainSQL`)))) => true
        case _ => false
      }
    }

    val multiplier = Multiplier[Nest] {
      case Nest(_, Iterative, _) => 1
      case Nest(_, Recursive(Native), _) => 1
      case _ => 0
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue


    given Hatch.Flavor.Value = Hatch.Flavor.build

    Hatcher()


    given Ps = Ps(_.`Console'o Header` = true)

    val hive: Iterable[Hive[Ps]] = for {
      it <- wildcard(multiplier)
    } yield {
      val hatch = Hatcher(it)
      val spawn = `Slick Store'o`(it.model)
      Hive(hatch, spawn)()
    }

    try

      var f = false

      val emito = this(hive)

      given Boolean = false // preemption off

      for
        o <- emito `#` 1
        if o(_.model == flow)
        it <- o
        if it.exists(_.row == 0) && it.exists(_.col == 0)
      do
        o(it) { f = true }


      if !f then
        println("!!! NO SOLUTIONS !!!")
      else
        this(emito `#` 1)

    finally
      this()
