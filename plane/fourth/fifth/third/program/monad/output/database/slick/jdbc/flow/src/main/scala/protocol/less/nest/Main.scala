package queens
package plane.fourth.fifth.third
package program.monad.output
package database.slick.jdbc
package protocol.less.nest
package flow

import scala.collection.mutable.{ ListBuffer => MutableList }

import akka.actor.typed.{ ActorSystem, Behavior }

import akka.actor.typed.scaladsl.Behaviors

import base.Board

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mdo.slick.flow.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.store.`Slick Store'o`

import version.v1.vector.breed.Hive

import dimensions.Dimension.Use.Output.Database.Slick.Profile._

import dimensions.Dimension.{ Aspect, Algorithm, Model, Use }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ flow, Flow }

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.database.slick.jdbc.SlickJdbcDatabaseApp:

  TryBoard()

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  import sys.Prop.IntProp
  sys.Prop(_root_.queens.main.Properties.CipherServer.port)
    .set("9999")

  def main(args: Array[String]): Unit =

    given Board = TryBoard(if args.isEmpty then 6 else args(0).toInt, empty = true)

    println(given_Board)


    val (profile, plainSQL) = this  { it => it match
                                        case H2 => false // true
                                        case SQLite => true // false
                                    }

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue

    val wildcard = Wildcard {
      (_, _) match {
        case (Aspect, Classic(Straight)) => true
        // case (Aspect, Stepper(OneIncrement)) => true
        // case (Aspect, Stepper(Permutations)) => true
        case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Model, Flow) => true
        case _ => false
      }
    }

    val multiplier = Multiplier[Nest] {
      case Nest(_, Iterative, _) => 1
      case Nest(_, Recursive(Native), _) => 1
      case _ => 0
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    var f = false

    given Hatch.Flavor.Value = Hatch.Flavor.build

    Hatcher()


    var size = 0
    val hive = MutableList[Hive[Ps]]()


    ActorSystem(queens(), "Queens") ! true

    def queens(): Behavior[Boolean] =
      Behaviors.setup { context => Behaviors
        .receiveMessage { st =>

          given Ps = Ps(_.`Console'o Header` = true)

          if st
          then

            import version.less.nest.protocol.NestMessage
            import version.less.nest.interface.ObjectNestProtocolIfce

            import version.less.nest.Implicits._

            size = implicitly[ObjectNestProtocolIfce[NestMessage]]
              .SessionExpand(context,
                version.base.role.isInitial(_),
                { () => context.self ! false },
                println(_),
                { (_: Nest, hatch: Hatch[?]) =>

                  val spawn = `Slick Store'o`(flow)

                  hive += Hive(hatch, spawn)()

                },
                wildcard,
                multiplier
              )

            if size > 0
            then
              Behaviors.same
            else
              Behaviors.stopped

          else

            try

              val emito = this(hive)

              given Boolean = false // preemption off

              for
                o <- emito `#` 1
                it <- o
              do
                o(it) { f = true }

              if !f then
                println("!!! NO SOLUTIONS !!!")
              else
                this(emito `#` 1)

            finally
              this()

            Behaviors.stopped

        }

      }
