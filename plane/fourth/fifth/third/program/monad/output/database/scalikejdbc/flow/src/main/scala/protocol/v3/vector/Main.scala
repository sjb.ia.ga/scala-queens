package queens
package plane.fourth.fifth.third
package program.monad.output
package database.scalikejdbc.jdbc
package protocol.v3.vector
package flow

import scala.collection.mutable.{ ListBuffer => MutableList }

import akka.actor.typed.{ ActorSystem, Behavior }

import akka.actor.typed.scaladsl.Behaviors

import base.Board

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mdo.scalikejdbc.flow.{ Parameters => Ps }

import version.v1.vector.breed.Hive

import dimensions.Dimension.Use.Output
import dimensions.Dimension.Use.Output.Database
import dimensions.Dimension.Use.Output.Database.Scalike
import dimensions.Dimension.Use.Output.Database.Scalike.Driver._

import dimensions.Dimension.{ Aspect, Algorithm, Model, Use }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ flow, Flow }

import version.less.nest.Nest
import version.v1.vector.{ VectorV1, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.database.scalikejdbc.ScalikejdbcDatabaseApp:

  TryBoard()

  import sys.Prop.IntProp
  sys.Prop(_root_.queens.main.Properties.CipherServer.port)
    .set("9999")

  def main(args: Array[String]): Unit =

    given Board = TryBoard(if args.isEmpty then 4 else args(0).toInt, empty = true)

    println(given_Board)

    given Ps = Ps(_.`Console'o Header` = true)

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue


    val (driver, plainSQL) = this  { it => it match
                                        case H2 => false // true
                                        case SQLite => true // false
                                    }

    val wildcard = Wildcard(
      version.less.nest.Wildcard( (_, _) match
        // case (Aspect, Classic(Straight)) => true
        case (Aspect, Stepper(OneIncrement)) => true
        case (Aspect, Stepper(Permutations)) => false
        case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Model, Flow) => true
        case _ => false
      ),
      version.patch.v1.mdo.Wildcard(
        version.patch.v1.mdo.Wildcard(
          flow,
          Output(Database(Scalike(`driver`, `plainSQL`)))
        )
      )
    )

    val multiplier = Multiplier[VectorV1[Ps]] {
      case VectorV1[Ps](Nest(_, Iterative, _), _) => 1
      case _ => 1
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Hatch.Flavor.Value = Hatch.Flavor.build

    Hatcher()


    var size = 0

    val hive = MutableList[Hive[Ps]]()


    ActorSystem(queens(), "Queens") ! true

    def queens(): Behavior[Boolean] =
      Behaviors.setup { context => Behaviors
        .receiveMessage { st =>

          if st
          then

            import version.v3.vector.{ V3NestMessage, V3MonadCOMessage }

            import version.v3.vector.interface.ObjectVectorV3ProtocolIfce
            import version.v3.vector.Implicits._

            size = implicitly[ObjectVectorV3ProtocolIfce]
              .SessionExpand(context,
                version.base.role.isInitial(_),
                { () => context.self ! false },
                { v3msg =>
                  v3msg match {
                    case V3NestMessage(msg) =>
                      println(s"nest = $msg")
                    case V3MonadCOMessage(msg) =>
                      println(s"mbo = $msg")
                  }
                },
                { (_: VectorV1[Ps], it: Option[?] => Hive[Ps]) =>
                  hive += it(None)
                },
                wildcard,
                multiplier
              )

            if size > 0
            then
              Behaviors.same
            else
              Behaviors.stopped

          else

            try

              var f = false

              val emito = this(hive)

              given Boolean = false // preemption off

              for
                o <- emito `#` 1
                it <- o
              do
                o(it) { f = true }

              if !f then
                println("!!! NO SOLUTIONS !!!")
              else
                this(emito `#` 1)

            finally
              this()

            Behaviors.stopped

        }

      }
