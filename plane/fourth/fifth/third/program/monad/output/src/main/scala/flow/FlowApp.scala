package queens
package plane.fourth.fifth.third.program.monad.output
package flow

import scala.collection.mutable.{ HashMap => Map }

import version.less.nest.Nest


abstract trait FlowApp extends ModelApp:

  extension [T](m: Map[T, LazyList[Solution]])
   def *=+(ks: (T, Solution)): Unit =
     try
       m(ks._1) = m(ks._1) :+ ks._2
     catch
       case _: java.util.NoSuchElementException =>
         m(ks._1) = LazyList.from(Nil)
         *=+(ks)


package `0`:

  package x:

    package sync:

      abstract trait FlowApp extends flow.FlowApp

    package async:

      import monix.eval.TaskApp

      abstract trait FlowApp extends TaskApp with flow.FlowApp

  package s:

    import cats.effect.IOApp

    abstract trait FlowApp extends IOApp with flow.FlowApp
