package queens
package plane.fourth.fifth.third.program.monad.output
package actors

import java.util.UUID

import java.util.concurrent.atomic.{ AtomicBoolean, AtomicLong }

import scala.concurrent.duration.{ Duration, FiniteDuration }

import scala.collection.mutable.{ HashMap => MutableMap }
import scala.collection.Map

import akka.actor.typed.ActorRef

import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import conf.actors.Callbacks

import version.less.nest.Nest


abstract trait ActorsApp extends ParallelApp:

  private val iterations: MutableMap[ActorRef[?], ActorContext[?]] = MutableMap()

  private val solutions: MutableMap[ActorRef[?], ActorRef[?]] = MutableMap()

  private def `iterations*`[T](cb: (T, Nest, UUID, ActorContext[?], ActorRef[?]) => Unit):
                                   (T, Nest, UUID, ActorContext[?], ActorRef[?]) => Unit =
    { (tag, nest, uuid, ctx, s) => sync { cb(tag, nest, uuid, ctx, s); iterations += s -> ctx } }

  private def `solutions*`[T](cb: (T, Nest, UUID, Option[Long], ActorRef[?], Option[(ActorContext[?], ActorRef[?])]) => Unit):
                                  (T, Nest, UUID, Option[Long], ActorRef[?], Option[(ActorContext[?], ActorRef[?])]) => Unit =
    { case (tag, nest, uuid, Some(number), s, o) =>
        sync {
          cb(tag, nest, uuid, Some(number), s, o)
          o match { case Some((_, r)) => solutions += r -> s case _ => }
        }
      case (tag, nest, uuid, number, s, o) =>
        sync {
          cb(tag, nest, uuid, number, s, o)
        }
    }


  final protected def `*`(cb: (Int, Nest, UUID, ActorContext[?], ActorRef[?]) => Unit =
                            { (_: Int, _: Nest, _: UUID, _: ActorContext[?], _: ActorRef[?]) => }):
                                          (Int, Nest, UUID, ActorContext[?], ActorRef[?]) => Unit = cb


  final def apply(count: Int = -1,
                  cb: (Int, Nest, UUID, ActorContext[?], ActorRef[?]) => Unit =
                    { (_: Int, _: Nest, _: UUID, _: ActorContext[?], _: ActorRef[?]) => },
                  `cb*`: (Int, Nest, UUID, Option[Long], ActorRef[?], Option[(ActorContext[?], ActorRef[?])]) => Unit =
                       { (_: Int, _: Nest, _: UUID, _: Option[Long], _: ActorRef[?], _: Option[(ActorContext[?], ActorRef[?])]) => })(using
                  context: ActorContext[Boolean]): Callbacks =

    if count == -1 then
      `#*`()
      assert(iterations.size == 2)
      iterations.toList.map(_.swap).map(_.stop(_))
      return null

    val cbs = Callbacks(`iterations*`(cb), `solutions*`(`cb*`))

    `#*`(count)

    val interrupter = context.spawnAnonymous[Unit](Behaviors
      .receiveMessage { _ =>
        `#*`(-2)

        Behaviors.stopped
      }
    )

    val stopper = context.spawnAnonymous[Unit](Behaviors
      .receiveMessage { _ =>
        while !start do
          try
            Thread.sleep(Duration.fromNanos(1000).toMillis)
          catch
            case _: InterruptedException =>

        while !stop do
          try
            Thread.sleep(Duration.fromNanos(10000).toMillis)
          catch
            case _: InterruptedException =>

        //Console.in.close()

        context.self ! false

        Behaviors.stopped
      }
    )

    interrupter ! ()
    stopper ! ()

    iterations += interrupter -> context
    iterations += stopper -> context

    cbs


  final override protected def `#`[T](ts: (T, Int)*)(
                                      ns: FiniteDuration)(implicit
                                      rs: Map[(T, UUID), Seq[(Long, Option[?])]]): Unit =
    super.`#`[T](ts*)(ns)

    val `r*` = sync {
      val ps = rs
        .filterKeys { (tag, _) => ts.map(_._1).contains(tag) }
        .values
        .map(_.asInstanceOf[Seq[(Long, Option[(ActorContext[?], ActorRef[?])])]]
              .filter(_._2.nonEmpty))
        .flatMap(_.map(_._2.get))

      var its: List[ActorRef[?]] = Nil
      var `its*`: List[(ActorContext[?], ActorRef[?])] = Nil

      ps.foreach { (_, r) =>
        val it = solutions(r)
        if !its.contains(it) then
          its = it :: its
          `its*` = iterations(it) -> it :: `its*`
          iterations -= it
      }

      `its*` // ++ ps
    }

    `r*`.foreach(_.stop(_))
