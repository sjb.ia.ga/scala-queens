package queens
package plane.fourth.fifth.third.program.monad
package output

import java.util.UUID

import java.util.concurrent.TimeoutException

import java.util.concurrent.atomic.{ AtomicBoolean, AtomicInteger }

import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future
import scala.concurrent.duration.{ Duration, FiniteDuration }

import scala.collection.mutable.{ HashMap => MutableMap }
import scala.collection.Map

import version.less.nest.Nest


abstract trait ParallelApp extends ModelApp:

  private var _start: AtomicInteger = _
  final protected def start: AtomicInteger = _start
  private var _stop: AtomicInteger = _
  final protected def stop: AtomicInteger = _stop
  private var _end: AtomicBoolean = _
  final protected def end: AtomicBoolean = _end

  given Conversion[AtomicBoolean, Boolean] = _.get()
  given Conversion[AtomicInteger, Boolean] = _.get() == 0

  extension (stop: AtomicInteger)
    def apply(): Unit = stop.decrementAndGet()
    def apply(start: AtomicInteger): Unit = { stop.incrementAndGet(); start() }


  extension (n: Int)
    def *(block: => Unit) = (1 to n).foreach { _ => block }

  extension [T](m: scala.collection.mutable.HashMap[T, (Nest, LazyList[Solution])])
    def =+(rs: (T, Solution)): Unit = m(rs._1) = m(rs._1)._1 -> (m(rs._1)._2 :+ rs._2)


  final protected def sync[R](e: => R): R =
    synchronized { e }


  final protected[output] def `#*`(count: Int = -1): Unit =
    if count >= 0 then
      _start = new AtomicInteger(count) // # iterations
      _stop = new AtomicInteger(0)
      _end = new AtomicBoolean(false)
      return
    else if count == -2 then
      println("PRESS ANY KEY <ENTER> TO INTERRUPT...")
      scala.io.StdIn.readLine
      end.set(true)
      return

    println(s"DONE${if end then "." else ": PRESS ANY KEY <ENTER>"}")


  final protected def `**`[S, R](using
                                 rs: MutableMap[(Int, UUID), Seq[(Long, Option[R])]]):
                                    (Int, Nest, UUID, Option[Long], S, Option[R]) => Unit =
    { case (t, _, uuid, Some(n), _, r) =>
        if !rs.contains((t, uuid)) then rs((t, uuid)) = Nil
        rs((t, uuid)) = rs((t, uuid)) :+ (n, r)
      case _ =>
    }


  protected def `#`[T](ts: (T, Int)*)(
                       ns: FiniteDuration = Duration.fromNanos(25000))(implicit
                       rs: Map[(T, UUID), Seq[(Long, Option[?])]]): Unit =

    def done = ts.foldLeft(true) { case (r, (tag, size)) =>
                                     r && sync {
                                            val `rs*` = rs.filterKeys(_._1 == tag)
                                            `rs*`.size == size &&
                                            `rs*`.foldLeft(true) { case (r, (_, s)) =>
                                                                    r && s.map(_._2).contains(None) }
                                          }
                                 }

    while !done do
      try
        Thread.sleep(ns.toMillis)
      catch
        case _: InterruptedException =>

    val total = ts.foldLeft(0) { case (r, (_, size)) => r + size }
    var ids: Seq[UUID] = Nil

    while ids.size < total do

      val ((tag, id), s) = sync(rs.filterKeys { (tag, id) =>
                                                ts.find(_._1 == tag).nonEmpty &&
                                                !ids.contains(id)
                                              }
                               ).head

      val (n, _) = s.find(_._2 eq None).get

      while sync(rs(tag -> id).size) <= n do
        try
          Thread.sleep(ns.toMillis)
        catch
          case _: InterruptedException =>

      ids = id +: ids
