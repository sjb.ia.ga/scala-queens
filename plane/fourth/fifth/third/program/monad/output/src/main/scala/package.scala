package object queens {

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]

  given Conversion[String, List[String]] = _
    .split("[\\[{(<.: ;,>)}\\]]")
    .filterNot(_.isEmpty)
    .map(_.replace('_', ' '))
    .toList

}
