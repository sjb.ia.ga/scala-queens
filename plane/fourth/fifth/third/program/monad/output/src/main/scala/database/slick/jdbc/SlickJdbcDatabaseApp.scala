package queens
package plane.fourth.fifth.third.program.monad.output
package database
package slick
package jdbc

import scala.concurrent.ExecutionContext.Implicits._

import common.geom.Implicits.`Coord*`

import common.NoTag

import dimensions.fifth.output.database.slick.dao.SlickDatabaseDAO.Implicits._
import dimensions.fifth.output.database.slick.jdbc.JdbcSlickDatabaseCallbacks
import dimensions.fifth.output.database.slick.jdbc.dao.JdbcSlickDatabaseDAO

import dimensions.fifth.output.console.conf.{ Conf => ConsoleP }
import conf.mdo.slick.flow.{ Parameters => P }

import version.v1.vector.breed.Hive

import version.v1.vector.breed.query.`Hatch'em, Spawn'it, Store'o Query'q`
import version.v1.vector.breed.query.`Hatch'em, Spawn'it, Jdbc Slick Store'o Query'q`

import dimensions.Dimension.Model.flow
import dimensions.Dimension.Use.Output.Database.Slick.Profile
import dimensions.Dimension.Use.Output.Database.Slick.Profile._


abstract trait SlickJdbcDatabaseApp extends DatabaseApp:

  private var _create: () => Unit = _
  private var _drop: () => Unit = _
  private var _apply: P ?=> Iterable[Hive[P]] => `Hatch'em, Spawn'it, Store'o Query'q`[P] = _

  def apply() = _drop()

  def apply(plain: Profile => Boolean): (Profile, Boolean) =

    print("Select Slick Jdbc profile [ H2 | SQLite ]: ")

    Console.in.readLine().toLowerCase match

      case "" | "h2" =>
        val plainSQL = plain(H2)

        import _root_.slick.jdbc.H2Profile

        import axis.fifth.output.database.slick.h2.{ init => create, drop }

        import dimensions.Dimension.Use.Output.Database.Slick.Profile.H2

        given JdbcSlickDatabaseDAO[H2Profile] = JdbcSlickDatabaseDAO[H2Profile]("h2mem_dc", H2, plainSQL)
        given JdbcSlickDatabaseCallbacks[H2Profile] = JdbcSlickDatabaseCallbacks[H2Profile]()

        _create = { () => create(given_JdbcSlickDatabaseDAO_H2Profile, given_JdbcSlickDatabaseDAO_H2Profile.tables, plainSQL) }

        _drop = { () => drop(given_JdbcSlickDatabaseDAO_H2Profile, given_JdbcSlickDatabaseDAO_H2Profile.tables, plainSQL) }

        _apply = { em => `Hatch'em, Spawn'it, Jdbc Slick Store'o Query'q`[P, H2Profile](em) }

        ( H2, plainSQL )

      case "sqlite" =>
        val plainSQL = plain(SQLite)

        import _root_.slick.jdbc.SQLiteProfile

        import axis.fifth.output.database.slick.sqlite.{ init => create, drop }

        import dimensions.Dimension.Use.Output.Database.Slick.Profile.SQLite

        given JdbcSlickDatabaseDAO[SQLiteProfile] = JdbcSlickDatabaseDAO[SQLiteProfile]("sqlite_dc", SQLite, plainSQL)
        given JdbcSlickDatabaseCallbacks[SQLiteProfile] = JdbcSlickDatabaseCallbacks[SQLiteProfile]()

        _create = { () => create(given_JdbcSlickDatabaseDAO_SQLiteProfile, given_JdbcSlickDatabaseDAO_SQLiteProfile.tables, plainSQL) }

        _drop = { () => drop(given_JdbcSlickDatabaseDAO_SQLiteProfile, given_JdbcSlickDatabaseDAO_SQLiteProfile.tables, plainSQL) }

        _apply = { em => `Hatch'em, Spawn'it, Jdbc Slick Store'o Query'q`[P, SQLiteProfile](em) }

        ( SQLite, plainSQL )

      case _ => ???


  def apply(em: Iterable[Hive[P]])(using
            params: P): `Hatch'em, Spawn'it, Store'o Query'q`[P] =
    _create()
    _apply(em)


  object v2:

    import version.v2.vector.VectorV2
    import version.v1.mco.MonadCO

    def apply(profile: Profile,
              plainSQL: Boolean)(using
              params: P
    ): VectorV2 => MonadCO[P] = (profile, plainSQL) match

      case (H2, false) =>
        import version.v2.vector.database.slick.h2.Implicits.given
        given_P_to_VectorV2_to_MonadCO_P[P]

      case (H2, true) =>
        import version.v2.vector.database.slick.h2.plainsql.Implicits.given
        given_P_to_VectorV2_to_MonadCO_P[P]

      case (SQLite, false) =>
        import version.v2.vector.database.slick.sqlite.Implicits.given
        given_P_to_VectorV2_to_MonadCO_P[P]

      case (SQLite, true) =>
        import version.v2.vector.database.slick.sqlite.plainsql.Implicits.given
        given_P_to_VectorV2_to_MonadCO_P[P]
