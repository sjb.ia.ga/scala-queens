package queens
package plane.fourth.fifth.third.program.monad.output
package database
package scalikejdbc

import _root_.scalikejdbc._

import common.geom.Implicits.`Coord*`

import common.NoTag

import dimensions.fifth.output.database.scalikejdbc.ScalikejdbcDatabaseCallbacks
import dimensions.fifth.output.database.scalikejdbc.dao.ScalikejdbcDatabaseDAO

import dimensions.fifth.output.console.conf.{ Conf => ConsoleP }
import conf.mdo.scalikejdbc.flow.{ Parameters => P }

import version.v1.vector.breed.Hive

import version.v1.vector.breed.query.`Hatch'em, Spawn'it, Store'o Query'q`
import version.v1.vector.breed.query.`Hatch'em, Spawn'it, Scalikejdbc Store'o Query'q`

import dimensions.Dimension.Model.flow
import dimensions.Dimension.Use.Output.Database.Scalike.Driver
import dimensions.Dimension.Use.Output.Database.Scalike.Driver._


abstract trait ScalikejdbcDatabaseApp extends DatabaseApp:

  private var _create: () => Unit = _
  private var _drop: () => Unit = _
  private var _apply: P ?=> Iterable[Hive[P]] => `Hatch'em, Spawn'it, Store'o Query'q`[P] = _

  def apply() = _drop()

  def apply(plain: Driver => Boolean): (Driver, Boolean) =

    print("Select Scalikejdbc driver [ H2 | SQLite ]: ")

    Console.in.readLine().toLowerCase match

      case "" | "h2" =>
        val plainSQL = plain(H2)

        import axis.fifth.output.database.scalikejdbc.h2.{ init => create, drop }

        import dimensions.Dimension.Use.Output.Database.Scalike.Driver.H2

        given ScalikejdbcDatabaseDAO = ScalikejdbcDatabaseDAO("h2", H2, plainSQL)
        given ScalikejdbcDatabaseCallbacks = ScalikejdbcDatabaseCallbacks()

        _create = { () => create(given_ScalikejdbcDatabaseDAO.driver, plainSQL) }

        _drop = { () => drop(given_ScalikejdbcDatabaseDAO.driver, plainSQL) }

        _apply = { em => `Hatch'em, Spawn'it, Scalikejdbc Store'o Query'q`(em) }

        ( H2, plainSQL )

      case "sqlite" =>
        val plainSQL = plain(SQLite)

        import axis.fifth.output.database.scalikejdbc.sqlite.{ init => create, drop }

        import dimensions.Dimension.Use.Output.Database.Scalike.Driver.SQLite

        given ScalikejdbcDatabaseDAO = ScalikejdbcDatabaseDAO("sqlite", SQLite, plainSQL)
        given ScalikejdbcDatabaseCallbacks = ScalikejdbcDatabaseCallbacks()

        _create = { () => create(given_ScalikejdbcDatabaseDAO.driver, plainSQL) }

        _drop = { () => drop(given_ScalikejdbcDatabaseDAO.driver, plainSQL) }

        _apply = { em => `Hatch'em, Spawn'it, Scalikejdbc Store'o Query'q`(em) }

        ( SQLite, plainSQL )

      case _ => ???


  def apply(em: Iterable[Hive[P]])(using
            params: P): `Hatch'em, Spawn'it, Store'o Query'q`[P] =
    _create()
    _apply(em)


  object v2:

    import version.v2.vector.VectorV2
    import version.v1.mco.MonadCO

    def apply(driver: Driver,
              plainSQL: Boolean)(using
              params: P
    ): VectorV2 => MonadCO[P] = (driver, plainSQL) match

      case (H2, false) =>
        import version.v2.vector.database.scalikejdbc.h2.Implicits.given
        given_P_to_VectorV2_to_MonadCO_P[P]

      case (H2, true) =>
        import version.v2.vector.database.scalikejdbc.h2.plainsql.Implicits.given
        given_P_to_VectorV2_to_MonadCO_P[P]

      case (SQLite, false) =>
        import version.v2.vector.database.scalikejdbc.sqlite.Implicits.given
        given_P_to_VectorV2_to_MonadCO_P[P]

      case (SQLite, true) =>
        import version.v2.vector.database.scalikejdbc.sqlite.plainsql.Implicits.given
        given_P_to_VectorV2_to_MonadCO_P[P]
