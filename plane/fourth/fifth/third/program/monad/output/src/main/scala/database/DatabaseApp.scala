package queens
package plane.fourth.fifth.third.program.monad.output
package database

import common.geom.Implicits.`Coord*`

import conf.mdo.flow.Conf
import dimensions.fifth.output.console.conf.{ Conf => ConsoleP }

import version.v1.vector.breed.query.`Hatch'em, Spawn'it, Store'o Query'q`

import dimensions.Dimension.Model.flow


abstract trait DatabaseApp extends ModelApp:

  def apply[P <: Conf](emitq: `Hatch'em, Spawn'it, Store'o Query'q`[P])(using
                       params: P,
                       preemption: Boolean): Unit =

    val board = params.board

    def solutions =
      println("<<< SOLUTIONS <<<")

      val sol_emitoq = emitq.`by Flow`.`console'o'q`.solutions()

      for
        it <- sol_emitoq
        if it( _.model == flow )
      do
        sol_emitoq(it) {
          var sep = ':'
          for
            sp <- it
          do
            it(sp) {
              import axis.fifth.output.database.Mappings._
              sp.the match
                case DbSolutionPoint(DbSolution(DbRound(_, _, DbQueen(at, _, am, _, ml, _, ie, _, ue, _, tags, _), _, tag, rn, _), _, sn, _), _, row, col) =>
                  if sep == ':' then
                    print(s"⊛[$tags]#$rn.$sn@$tag(${at.aspect}, ${am.algorithm}, ${ml.model})")
                  print(s"$sep $row x $col")
                  if sep == ':' then sep = ','
            }

          println()
        }

      import _root_.queens.given
      import dimensions.fourth.fifth.breed.query.`Query Solutions'to`

      val cb_sol_emitq = emitq.`by Flow`.`callback'q`.solutions { cb =>
        cb match
          case `Query Solutions'to`(qs, (_, tag), it, no) =>
            import common.pipeline.Context.Round
            import dimensions.fifth.output.console.QueensConsoleOutput
            import dimensions.fourth.fifth.breed.query.`Query Queens'to`

            object o
                extends QueensConsoleOutput:

              override protected type Q = `Query Queens'to`

              override protected val queens = qs

              def apply(solution: Solution): Unit =
                params match
                  case cp: ConsoleP if cp.`Console'o Header` =>
                    print(s"[$cb] ")
                  case _ =>
                ||(solution, Round(board, tag, queens, no))

            o(it)
        true
      }

      var empty = true

      for
        it <- cb_sol_emitq
        if it( _.model == flow )
        if it( (s: Solution, _) => s.exists(_.row == 0) && s.exists(_.col == 0) )
      do
        cb_sol_emitq(it) { empty = false }

      if empty then println("EMPTY")

      println(">>> SOLUTIONS >>>")
    end solutions

    solutions

    val del_sol_emitq = emitq.`by Flow`.`delete'q`.solutions()
    for
      it <- del_sol_emitq
      if it( _.model == flow )
    do
      del_sol_emitq(it){}

    println("!!! SOLUTIONS DELETED !!!")

    solutions


    def rounds =
      println("<<< ROUNDS <<<")

      val r_emitoq = emitq.`by Flow`.`console'o'q`.rounds()

      var empty = true

      for
        it <- r_emitoq
      do
        r_emitoq(it) { empty = false }

      if empty then println("EMPTY")

      println(">>> ROUNDS >>>")
    end rounds

    rounds

    val del_rnd_emitq = emitq.`by Flow`.`delete'q`.rounds()
    for
      it <- del_rnd_emitq
    do
      del_rnd_emitq(it){}

    println("!!! ROUNDS DELETED !!!")

    rounds


    def queens =
      println("<<< QUEENS <<<")

      val q_emitoq = emitq.`by Flow`.`console'o'q`.queens()

      for
        it <- q_emitoq
        if it.model == flow
      do
        q_emitoq(it){}

      val cb_q_emitq = emitq.`by Flow`.`callback'q`.queens(
        { cb =>
          println(s"$cb @ ${cb.aspect}, ${cb.algorithm}, ${cb.model}")
          true
        }
        , `rounds?` = Some(List(1))
      )

      var empty = true

      for
        it <- cb_q_emitq
        if it.model == flow
      do
        cb_q_emitq(it) { empty = false }

      if empty then println("EMPTY")

      println(">>> QUEENS >>>")
    end queens

    queens

    val del_que_emitq = emitq.`by Flow`.`delete'q`.queens()
    for
      it <- del_que_emitq
    do
      del_que_emitq(it){}

    println("!!! QUEENS DELETED !!!")

    queens


    def squares =
      println("<<< SQUARES <<<")

      var n = board.N
      val b: List[Array[Boolean]] = List.fill(n)(Array.fill(n)(false))

      val cb_sqr_emitq = emitq.`by Flow`.`callback'q`.squares { cb =>
        import axis.fifth.output.database.Mappings.DbSquare
        cb match
          case DbSquare(_, _, row, col, free) =>
            b(row)(col) = free
        true
      }

      var empty = true

      for
        it <- cb_sqr_emitq
      do
        cb_sqr_emitq(it) { empty = false }

      if empty then println("EMPTY")
      else println(b.map(_.map(if _ then "∴" else "⬡").mkString(" ")).mkString("\n"))

      println(">>> SQUARES >>>")
    end squares

    squares

    val del_sqr_emitq = emitq.`by Flow`.`delete'q`.squares()
    for
      it <- del_sqr_emitq
    do
      del_sqr_emitq(it){}

    println("!!! SQUARES DELETED !!!")

    squares
