package queens
package plane.fourth.fifth.third.program.monad.output

import dimensions.three.breed.Hatch.Flavor

import version.less.nest.Nest


abstract trait ModelApp:

  protected def prompt(text: String, default: Boolean = true): Boolean =
    var r: Option[Boolean] = None
    while r.isEmpty do
      print(s"$text? [${if default then "Y" else "y"}/${if default then "n" else "N"}] ")
      val l = scala.io.StdIn.readLine
      r = (if l.isBlank then (if default then 'Y' else 'N') else l.charAt(0)).toLower match
        case 'y' => Some(true)
        case 'n' => Some(false)
        case _ => None
    r.get

  protected def `di-prompt`(text: String, fs: Flavor.Value*): Flavor.Value =
    var r: Option[Flavor.Value] = None
    while r.isEmpty do
      print(s"$text? [ ${fs(0).toString.toUpperCase} | ${fs.tail.map(_.toString.toLowerCase).mkString(" | ")} ] ")
      val l = scala.io.StdIn.readLine
      r = Flavor(if l.isBlank then fs(0).toString else l.toLowerCase)
    r.get
