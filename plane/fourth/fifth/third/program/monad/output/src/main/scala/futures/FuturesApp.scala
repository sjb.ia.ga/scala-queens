package queens
package plane.fourth.fifth.third.program.monad.output
package futures

import java.util.UUID

import java.util.concurrent.TimeoutException

import scala.collection.mutable.{ HashMap => MutableMap }
import scala.collection.mutable.{ ListBuffer => MutableList }
import scala.collection.Map

import scala.concurrent.{ Await, ExecutionContext, Future }
import scala.concurrent.duration.{ Duration, FiniteDuration }

import conf.futures.Callbacks

import version.less.nest.Nest


abstract class FuturesApp extends ParallelApp:

  private val iterations: MutableList[Future[?]] = MutableList()

  private val solutions: MutableMap[Future[?], Future[?]] = MutableMap()

  private def `iterations*`[T](cb: (T, Nest, UUID, Future[?]) => Unit):
                                   (T, Nest, UUID, Future[?]) => Unit =
    { (tag, nest, uuid, s) => sync { cb(tag, nest, uuid, s); iterations += s } }

  private def `solutions*`[T](cb: (T, Nest, UUID, Option[Long], Future[?], Option[Future[?]]) => Unit):
                                  (T, Nest, UUID, Option[Long], Future[?], Option[Future[?]]) => Unit =
    { case (tag, nest, uuid, Some(number), s, o) =>
        sync {
          cb(tag, nest, uuid, Some(number), s, o)
          o match { case Some(f) => solutions += f -> s case _ => }
        }
      case (tag, nest, uuid, number, s, o) =>
        sync {
          cb(tag, nest, uuid, number, s, o)
        }
    }


  final protected def `*`(cb: (Int, Nest, UUID, Future[?]) => Unit =
                            { (_: Int, _: Nest, _: UUID, _: Future[?]) => }):
                                       (Int, Nest, UUID, Future[?]) => Unit = cb


  final def apply(count: Int = -1,
                  cb: (Int, Nest, UUID, Future[?]) => Unit =
                    { (_: Int, _: Nest, _: UUID, _: Future[?]) => },
                  `cb*`: (Int, Nest, UUID, Option[Long], Future[?], Option[Future[?]]) => Unit =
                       { (_: Int, _: Nest, _: UUID, _: Option[Long], _: Future[?], _: Option[Future[?]]) => },
                  executionContext: ExecutionContext = null): (ExecutionContext, Callbacks) =

    if count == -1 then
      //Console.in.close()
      `#*`()
      assert(iterations.size == 2)
      await(iterations.toSeq)
      return (null, null)

    implicit val ec: ExecutionContext =
      if executionContext ne null
      then
        executionContext
      else
        import scala.concurrent.ExecutionContext.Implicits._
        implicitly[ExecutionContext]

    val cbs = Callbacks(`iterations*`(cb), `solutions*`(`cb*`))

    `#*`(count)

    val interrupter = Future { `#*`(-2) }

    val stopper = Future {
      while !start do
        try
          Thread.sleep(Duration.fromNanos(1000).toMillis)
        catch
          case _: InterruptedException =>

      while !stop do
        try
          Thread.sleep(Duration.fromNanos(10000).toMillis)
        catch
          case _: InterruptedException =>
    }

    iterations ++= Seq(interrupter, stopper)

    (ec, cbs)


  final protected def await(fs: Seq[Future[?]])(
                            ns: FiniteDuration = Duration.fromNanos(500000))(
                            cb: Future[?] => Unit = { _ => }) =
    var n = fs.size
    while n > 0 do
      n = fs.size
      for it <- fs do
        if it.isCompleted
        then
          n -= 1
          sync(cb(it))
        else
          try
            Await.ready(it, ns)
          catch
            case _: InterruptedException | _: TimeoutException =>


  final override protected def `#`[T](ts: (T, Int)*)(
                                      ns: FiniteDuration)(implicit
                                      rs: Map[(T, UUID), Seq[(Long, Option[?])]]): Unit =
    super.`#`[T](ts*)(ns)

    val `f*` = sync {
      val fs = rs
        .filterKeys { (tag, _) => ts.map(_._1).contains(tag) }
        .values
        .map(_.asInstanceOf[Seq[(Long, Option[Future[?]])]]
              .filter(_._2.nonEmpty))
        .flatMap(_.map(_._2.get))

      var its: List[Future[?]] = Nil

      fs.foreach { f =>
        val it = solutions(f)
        if !its.contains(it) then
          its = it :: its
      }

      iterations --= its

      fs ++ its

    }

    await(`f*`.toSeq)()()
