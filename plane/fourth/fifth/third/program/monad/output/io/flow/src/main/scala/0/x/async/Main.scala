package queens
package plane.fourth.fifth.third
package zeroth
package program.monad.output
package io
package flow.x.async

import java.nio.file.Files

import cats.effect.ExitCode

import monix.eval.Task
import monix.tail.Iterant.eval

import base.Board
import common.monad.Item
import Item.given

import common.geom.{ row, col }

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.io.x.flow.async.{ Parameters => Ps }
import dimensions.fourth.fifth.zeroth.breed.x.async.`I'o`

import version.v0.vector.breed.flow.x.async.`Hatch'em, Spawn'it, I'o`
import version.v0.vector.breed.x.Hive

import dimensions.Dimension.{ Monad, Aspect, Algorithm, Model }
import Monad.monixIterantTask
import Aspect._
import Classic._
import Stepper._
import Algorithm._
import Recursive._
import Model.{ flow, Flow }

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.flow.`0`.x.async.FlowApp:

  TryBoard()

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  def run(args: List[String]): Task[ExitCode] =

    given Long = if args.length > 1 then args(1).toLong else Int.MaxValue

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        case (Aspect, Classic(Callback)) => false
        // case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Continuation)) => true
        case (Model, Flow) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case Nest(_, Recursive(_), _) => 1
      case Nest(_, Iterative, _) => 1
      case _ => 0
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return Task(ExitCode.Success)


    given Board = TryBoard(if args.isEmpty then 6 else args(0).toInt, empty = true)


    println(given_Board)


    given Hatch.Flavor.Value = Hatch.Flavor.build

    Hatcher()


    val tmpdir = Files.createTempDirectory("queens-plane-453-program-io-flow-0-x-async-")

    given Ps = Ps(tmpdir)

    val hive: Iterable[Hive[Task, Ps]] = for {
      it <- wildcard(multiplier)
    } yield {
      import dimensions.fifth.output.console.QueensConsoleOutput
      val hatch = Hatcher.`0`(monixIterantTask)(it)
      val spawn = `I'o`(it.model)
      Hive.async(hatch, spawn)(Some(QueensConsoleOutput(_)))
    }

    val emito = `Hatch'em, Spawn'it, I'o`(hive)


    ( for
        o <- emito
        if o(_.model == flow)
        it <- o
        if it -> (it.exists(_.row == 0) && it.exists(_.col == 0))
        _ <- eval(o(it){})
      yield
        ()
    ).completedL >> Task(Files.delete(tmpdir)) >> Task(ExitCode.Success)
