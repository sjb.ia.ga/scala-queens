package queens
package plane.fourth.fifth.third
package zeroth
package program.monad.output
package io
package flow.x.sync.di

import java.nio.file.Files

import cats.effect.ExitCode

import monix.eval.Coeval
import monix.tail.Iterant.eval
import Coeval.catsSync

import base.Board
import common.monad.Item

import common.geom.{ row, col }

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.io.x.flow.sync.{ Parameters => Ps }
import dimensions.fourth.fifth.zeroth.breed.x.sync.`I'o`

import version.v0.vector.breed.flow.x.sync.`Hatch'em, Spawn'it, I'o`
import version.v0.vector.breed.x.Hive

import dimensions.Dimension.{ Monad, Aspect, Algorithm, Model }
import Monad.monixIterantCoeval
import Aspect._
import Classic._
import Stepper._
import Algorithm._
import Recursive._
import Model.{ flow, Flow }

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.flow.`0`.x.sync.FlowApp:

  TryBoard()

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  def main(args: Array[String]): Unit =

    given Long = if args.length > 1 then args(1).toLong else Int.MaxValue

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => false
        case (Aspect, Classic(Callback)) => true
        // case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Continuation)) => true
        case (Model, Flow) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case Nest(_, Recursive(_), _) => 1
      case Nest(_, Iterative, _) => 1
      case _ => 0
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Board = {
      var b: Board = null

      var a = false
      while !a do
        b = TryBoard(if args.isEmpty then 6 else args(0).toInt)

        println(b)

        a = prompt("Proceed")

      b
    }


    given Hatch.Flavor.Value = `di-prompt`("DI", Hatch.Flavor.`di-guice`, Hatch.Flavor.`di-macwire`)

    Hatcher()


    val tmpdir = Files.createTempDirectory("queens-plane-453-program-io-flow-0-x-sync-di-")

    given Ps = Ps(tmpdir)

    val hive: Iterable[Hive[Coeval, Ps]] = for {
      it <- wildcard(multiplier)
    } yield {
      import dimensions.fifth.output.console.QueensConsoleOutput
      val hatch = Hatcher.`0`(monixIterantCoeval)(it)
      val spawn = `I'o`(it.model)
      Hive.sync(hatch, spawn)(Some(QueensConsoleOutput(_)))
    }

    val emito = `Hatch'em, Spawn'it, I'o`(hive)


    {
      ( for
          o <- emito
          if o(_.model == flow)
          it <- o
          if it -> (it.exists(_.row == 0) && it.exists(_.col == 0))
          _ <- eval(o(it){})
        yield
          ()
      ).completedL <* Coeval(Files.delete(tmpdir))
    }.value()
