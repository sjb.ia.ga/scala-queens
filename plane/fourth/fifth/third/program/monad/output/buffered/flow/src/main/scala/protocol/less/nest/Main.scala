package queens
package plane.fourth.fifth.third
package program.monad.output
package buffered
package protocol.less.nest
package flow

import akka.actor.typed.{ ActorSystem, Behavior }

import akka.actor.typed.scaladsl.Behaviors

import base.Board

import common.geom.{ row, col }

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mbo.flow.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.`Buffered'o`

import version.v1.vector.breed.`Hatch'em, Spawn'it, Buffered'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ flow, Flow }

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.flow.FlowApp:

  TryBoard()

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  import sys.Prop.IntProp
  sys.Prop(_root_.queens.main.Properties.CipherServer.port)
    .set("9999")

  def main(args: Array[String]): Unit =

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue

    val wildcard = Wildcard {
      (_, _) match {
        case (Aspect, Classic(Straight)) => true
        case (Aspect, Stepper(OneIncrement)) => true
        case (Aspect, Stepper(Permutations)) => true
        case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Model, Flow) => true
        case _ => false
      }
    }

    val multiplier = Multiplier[Nest] {
      case Nest(_, Iterative, _) => 0
      case Nest(_, Recursive(Native), _) => 1
      case _ => 0
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Board = TryBoard(if args.isEmpty then 6 else args(0).toInt, empty = true)

    println(given_Board)


    given Hatch.Flavor.Value = Hatch.Flavor.`di-guice`

    Hatcher()


    var f = false


    ActorSystem(queens(), "Queens") ! true

    def queens(): Behavior[Boolean] =
      Behaviors.setup { context => Behaviors
        .receiveMessage { st =>

          given Ps = Ps()

          if st
          then

            import version.less.nest.protocol.NestMessage
            import version.less.nest.interface.ObjectNestProtocolIfce

            import version.less.nest.Implicits._

            val size = implicitly[ObjectNestProtocolIfce[NestMessage]]
              .SessionExpand(context,
                version.base.role.isInitial(_),
                { () => context.self ! false },
                println(_),
                { (_: Nest, hatch: Hatch[?]) =>

                  import dimensions.fifth.output.console.QueensConsoleOutput

                  val spawn = `Buffered'o`(flow)

                  val hive = Hive(hatch, spawn)(Some(QueensConsoleOutput(_)))


                  val emito = `Hatch'em, Spawn'it, Buffered'o`(Some(hive))

                  given Boolean = false // preemption off

                  for
                    o <- emito `#` 1
                    it <- o
                    //if it.exists(_.row == 0) && it.exists(_.col == 0)
                  do
                    o(it) { f = true }

                },
                wildcard,
                multiplier
              )

            if size > 0
            then
              Behaviors.same
            else
              Behaviors.stopped

          else

            if !f then
              println("!!! NO SOLUTIONS !!!")

            Behaviors.stopped

        }

      }
