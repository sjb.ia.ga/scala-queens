package queens
package plane.fourth.fifth.third
package program.monad.output
package buffered
package protocol.v3.vector
package futures

import java.util.UUID

import scala.collection.mutable.{ ListBuffer => MutableList }
import scala.collection.mutable.{ HashMap => Map }

import scala.concurrent.{ Future, ExecutionContext }

import akka.actor.typed.{ ActorSystem, Behavior }

import akka.actor.typed.scaladsl.Behaviors

import base.Board

import common.geom.{ row, col }

import common.monad.futures._
import common.monad.tag.integer.given

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mbo.futures.{ Parameters => Ps }

import version.v1.vector.breed.`Hatch'em, Spawn'it, Buffered'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ futures, Parallel }
import dimensions.Dimension.Model.Parallel.Futures

import version.less.nest.Nest
import version.v1.vector.{ VectorV1, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.futures.FuturesApp:

  TryBoard()

  {
    import sys.Prop.StringProp
    sys.Prop("scala.concurrent.context.numThreads").set("x32")
  }

  import sys.Prop.IntProp
  sys.Prop("scala.concurrent.context.maxThreads").set("8192")
  sys.Prop("scala.concurrent.context.maxExtraThreads").set("8192")
  sys.Prop("scala.concurrent.context.minThreads").set("512")
  sys.Prop("java.util.concurrent.ForkJoinPool.common.parallelism").set("4096")

  sys.Prop(_root_.queens.main.Properties.CipherServer.port)
    .set("9999")

  def main(args: Array[String]): Unit =

    given Board = TryBoard(if args.isEmpty then 6 else args(0).toInt, empty = true)

    println(given_Board)


    val M = 1 // # futures iterations

    implicit val fs: Map[(Int, UUID), Seq[(Long, Option[Future[?]])]] = Map((-1, null) -> Nil)

    val m = Map[Future[?], (Nest, LazyList[Solution])]()

    val (ec, cbs) = this(M,
      `*` { (tag, nest, _, f: Future[?]) =>
              fs((-1, null)) = fs((-1, null)) :+ (-1L, Some(f))
              m(f) = nest -> LazyList.from(Nil)
          },
      `**`
    )

    given ExecutionContext = ec

    given Ps = Ps(cbs)() // foreach/map/flatMap

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue

    val wildcard = Wildcard(
      version.less.nest.Wildcard( (_, _) match {
        // case (Aspect, Classic(Straight)) => true
        case (Aspect, Stepper(OneIncrement)) => true
        case (Aspect, Stepper(Permutations)) => false
        case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Model, Parallel(Futures)) => true
        case _ => false
      }),
      version.patch.v1.mbo.Wildcard.*
    )

    val multiplier = Multiplier[VectorV1[Ps]] {
      case VectorV1[Ps](Nest(_, Iterative, _), _) => 0
      case _ => 1
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    import dimensions.fifth.output.console.QueensConsoleOutput
    val p = QueensConsoleOutput(_: Nest)


    given Hatch.Flavor.Value = Hatch.Flavor.build

    Hatcher()


    var size = 0

    val hive = MutableList[Hive[Ps]]()


    ActorSystem(queens(), "Queens") ! true

    def queens(): Behavior[Boolean] =
      Behaviors.setup { context => Behaviors
        .receiveMessage { st =>

          if st
          then

            import version.v3.vector.{ V3NestMessage, V3MonadCOMessage }

            import version.v3.vector.interface.ObjectVectorV3ProtocolIfce
            import version.v3.vector.Implicits._

            size = implicitly[ObjectVectorV3ProtocolIfce]
              .SessionExpand(context,
                version.base.role.isInitial(_),
                { () => context.self ! false },
                { v3msg =>
                  v3msg match {
                    case V3NestMessage(msg) =>
                      println(s"nest = $msg")
                    case V3MonadCOMessage(msg) =>
                      println(s"mbo = $msg")
                  }
                },
                { (_: VectorV1[Ps], it: Option[?] => Hive[Ps]) =>
                  hive += it(None)
                },
                wildcard,
                multiplier
              )

            if size > 0
            then
              Behaviors.same
            else
              Behaviors.stopped

          else

            var f = false

            try

              val emito = `Hatch'em, Spawn'it, Buffered'o`(hive)

              try
                stop(start)

                given Boolean = true // preemption on

                for
                  o <- emito `#` 1
                  // if o(_.model == futures)
                  it <- o
                do
                  o(it) {
                    val Just(_, _, _, _, f, _) = o `just get` it
                    sync { m =+ f -> o(o.length-1) }
                  }
                  if end then
                    o stop it

                `#`(1 -> hive.size)()

              finally
                stop()

              var ps: Seq[Future[?]] = Nil
              await(fs((-1, null)).map(_._2.get))() { it =>
                val (nest, r) = m(it)
                ps = ps :+ Future {
                  if !end then
                    val co = p(nest)
                    r.toList.foreach(co.||(_))
                  if r.nonEmpty then f = true
                }
              }
              await(ps)()()

            finally
              this()

              if !f then
                println(s"!!! NO SOLUTIONS${if end then " [INTERRUPTED]" else ""} !!!")

            Behaviors.stopped

        }

    }
