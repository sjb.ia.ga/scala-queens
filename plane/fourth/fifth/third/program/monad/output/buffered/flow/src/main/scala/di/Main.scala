package queens
package plane.fourth.fifth.third
package program.monad.output
package buffered
package flow.di

import base.Board

import common.geom.{ row, col }

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mbo.flow.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.`Buffered'o`

import version.v1.vector.breed.`Hatch'em, Spawn'it, Buffered'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ flow, Flow }

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.flow.FlowApp:

  TryBoard()

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  def main(args: Array[String]): Unit =

    var f = false

    given Long = if args.length > 1 then args(1).toLong else Int.MaxValue

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        // case (Aspect, Stepper(OneIncrement)) => true
        // case (Aspect, Stepper(Permutations)) => true
        // case (Algorithm, Iterative) => true
        // case (Algorithm, Recursive(Native)) => true
        case (Algorithm, Recursive(CatsEval)) => true
        case (Model, Flow) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case Nest(_, Recursive(_), _) => 1
      case Nest(_, Iterative, _) => 1
      case _ => 0
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Board = {
      var b: Board = null

      var a = false
      while !a do
        b = TryBoard(if args.isEmpty then 6 else args(0).toInt)

        println(b)

        a = prompt("Proceed")

      b
    }


    given Hatch.Flavor.Value = `di-prompt`("DI", Hatch.Flavor.`di-guice`, Hatch.Flavor.`di-macwire`)

    Hatcher()


    given Ps = Ps()

    val hive: Iterable[Hive[Ps]] = for {
      it <- wildcard(multiplier)
    } yield {
      import dimensions.fifth.output.console.QueensConsoleOutput
      val hatch = Hatcher(it)
      val spawn = `Buffered'o`(flow)
      Hive(hatch, spawn)(Some(QueensConsoleOutput(_)))
    }

    val emito = `Hatch'em, Spawn'it, Buffered'o`(hive)


    given Boolean = false // preemption off

    for
      o <- emito `#` 1
      if o(_.model == flow)
      it <- o
      if it.exists(_.row == 0) && it.exists(_.col == 0)
    do
      o(it) { f = true }

    if !f then
      println("!!! NO SOLUTIONS !!!")
