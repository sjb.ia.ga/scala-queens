package queens
package plane.fourth.fifth.third
package program.monad.output
package buffered
package protocol.v1.vector
package flow

import scala.collection.mutable.{ ListBuffer => MutableList }

import akka.actor.typed.{ ActorSystem, Behavior }

import akka.actor.typed.scaladsl.Behaviors

import base.Board

import common.geom.{ row, col }

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mbo.flow.{ Parameters => Ps }

import version.v1.vector.breed.`Hatch'em, Spawn'it, Buffered'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ flow, Flow }

import version.less.nest.Nest
import version.v1.vector.{ VectorV1, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.flow.FlowApp:

  TryBoard()

  import sys.Prop.IntProp
  sys.Prop(_root_.queens.main.Properties.CipherServer.port)
    .set("9999")

  def main(args: Array[String]): Unit =

    given Board = TryBoard(if args.isEmpty then 6 else args(0).toInt, empty = true)

    println(given_Board)


    given Ps = Ps()

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue


    val wildcard = Wildcard(
      version.less.nest.Wildcard( (_, _) match
        // case (Aspect, Classic(Straight)) => true
        case (Aspect, Stepper(OneIncrement)) => true
        case (Aspect, Stepper(Permutations)) => true
        case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Model, Flow) => true
        case _ => false
      ),
      version.patch.v1.mbo.Wildcard.*
    )

    val multiplier = Multiplier[VectorV1[Ps]] {
      case VectorV1[Ps](Nest(_, Iterative, _), _) => 1
      case _ => 1
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Hatch.Flavor.Value = Hatch.Flavor.build

    Hatcher()


    var size = 0
    val hive = MutableList[Hive[Ps]]()


    ActorSystem(queens(), "Queens") ! true

    def queens(): Behavior[Boolean] =
      Behaviors.setup { context => Behaviors
        .receiveMessage { st =>

          if st
          then

            import version.v1.vector.protocol.VectorV1Message
            import version.v1.vector.interface.ObjectVectorV1ProtocolIfce

            import version.v1.vector.Implicits._

            size = implicitly[ObjectVectorV1ProtocolIfce[VectorV1Message]]
              .SessionExpand(context,
                version.base.role.isInitial(_),
                { () => context.self ! false },
                println(_),
                { (v1: VectorV1[Ps], it: Option[Context => QueensUseSolution] => Hive[Ps]) =>
                  import dimensions.fifth.output.console.QueensConsoleOutput
                  hive += it(Some(QueensConsoleOutput(_)))
                },
                wildcard,
                multiplier
              )

            if size > 0
            then
              Behaviors.same
            else
              Behaviors.stopped

          else

            var f = false

            val emito = `Hatch'em, Spawn'it, Buffered'o`(hive)

            given Boolean = false // preemption off

            for
              o <- emito `#` 1
              // if o(_.model == flow)
              it <- o
              // if it.exists(_.row == 0) && it.exists(_.col == 0)
            do
              o(it) { f = true }

            if !f then
              println("!!! NO SOLUTIONS !!!")

            Behaviors.stopped

        }

      }

