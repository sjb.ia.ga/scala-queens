package queens
package plane.fourth.fifth.third
package program.monad.output
package buffered
package futures

import java.util.UUID

import scala.collection.mutable.{ HashMap => Map }

import scala.concurrent.{ Future, ExecutionContext }

import common.monad.futures._
import common.monad.tag.integer.given

import base.Board

import common.geom.{ row, col }

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mbo.futures.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.`Buffered'o`

import version.v1.vector.breed.`Hatch'em, Spawn'it, Buffered'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ futures, Parallel }
import dimensions.Dimension.Model.Parallel.Futures

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.futures.FuturesApp:

  TryBoard()

  {
    import sys.Prop.StringProp
    sys.Prop("scala.concurrent.context.numThreads").set("x32")
  }

  import sys.Prop.IntProp
  sys.Prop("scala.concurrent.context.maxThreads").set("8192")
  sys.Prop("scala.concurrent.context.maxExtraThreads").set("8192")
  sys.Prop("scala.concurrent.context.minThreads").set("512")
  sys.Prop("java.util.concurrent.ForkJoinPool.common.parallelism").set("4096")

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  def main(args: Array[String]): Unit =

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        // case (Aspect, Stepper(OneIncrement)) => true
        // case (Aspect, Stepper(Permutations)) => true
        case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Model, Parallel(Futures)) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case Nest(_, Recursive(Native), _) => 1
      case Nest(_, Iterative, _) => 1
      case _ => 0
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    given Board = TryBoard(if args.isEmpty then 4 else args(0).toInt)

    println(given_Board)


    import dimensions.fifth.output.console.QueensConsoleOutput
    val p = QueensConsoleOutput(_: Nest)


    given Hatch.Flavor.Value = Hatch.Flavor.config

    Hatcher()


    val M = 2 // # futures iterations

    implicit val fs: Map[(Int, UUID), Seq[(Long, Option[Future[?]])]] = Map((-1, null) -> Nil)

    val m = Map[Future[?], (Nest, LazyList[Solution])]()

    val (ec, cbs) = this(M,
      `*` { (tag, nest, _, f: Future[?]) =>
            fs((-1, null)) = fs((-1, null)) :+ (-1L, Some(f))
            m(f) = nest -> LazyList.from(Nil)
          },
      `**`
    )

    given ExecutionContext = ec

    given Ps = Ps(cbs)() // foreach/map/flatMap


    val hive: Iterable[Hive[Ps]] = for {
      it <- wildcard(multiplier)
    } yield {
      val hatch = Hatcher(it)
      val spawn = `Buffered'o`(it.model)
      Hive(hatch, spawn)()
    }


    var f = false

    try

      try
        M * stop(start)

        {
          val emito = `Hatch'em, Spawn'it, Buffered'o`(hive)

          given Boolean = true // preemption on

          for
            o <- emito `#` 1
            it <- o
          do
            o(it) {
              val Just(_, _, _, _, f, _) = o `just get` it
              sync { m =+ f -> o(o.length-1) }
            }
            if end then
              o stop it
        }

        {
          val emito = `Hatch'em, Spawn'it, Buffered'o`(hive.take(1))

          given Boolean = false // preemption off

          var nest: Nest = null

          m(null) = (nest, LazyList.from(

            for
              o <- emito `#` 2
              if o { q => nest = Nest(q.aspect, q.algorithm, q.model); true }
              it <- o
            yield
              o(it) { o(o.length-1) }

          ).map(_.get))

          m(null) = (nest, m(null)._2)

        }

        `#`(1 -> hive.size, 2 -> 1)()

      finally
        M * stop()

      var ps = Seq(Future {
        val (nest, r) = m(null)
        if !end then
          val co = p(nest)
          r.toList.foreach(co.||(_))
          if r.nonEmpty then f = true
      })

      await(fs((-1, null)).map(_._2.get))() { it =>
        val (nest, r) = m(it)
        ps = ps :+ Future {
          if !end then
            val co = p(nest)
            r.toList.foreach(co.||(_))
            if r.nonEmpty then f = true
        }
      }

      await(ps)()()

    finally
      this()

      if !f then
        println("!!! NO SOLUTIONS !!!")
