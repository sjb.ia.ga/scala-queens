package queens
package plane.fourth.fifth.third
package program.monad.output
package buffered
package protocol.v1.vector
package actors

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import scala.collection.JavaConverters.mapAsScalaConcurrentMapConverter

import scala.collection.mutable.{ ListBuffer => MutableList }
import scala.collection.mutable.{ HashMap => MutableMap }

import akka.actor.typed.{ ActorSystem, ActorRef, Behavior }

import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import base.Board

import common.monad.actors._
import common.monad.tag.integer.given

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mbo.actors.{ Parameters => Ps }

import version.v1.vector.breed.`Hatch'em, Spawn'it, Buffered'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ actors, Parallel }
import dimensions.Dimension.Model.Parallel.Actors

import version.less.nest.Nest
import version.v1.vector.{ VectorV1, Wildcard }
import version.base.Multiplier

import conf.actors.Callbacks


object Main extends plane.fourth.fifth.third.program.monad.output.actors.ActorsApp:

  TryBoard()

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  import sys.Prop.IntProp
  sys.Prop(_root_.queens.main.Properties.CipherServer.port)
    .set("9999")

  def main(args: Array[String]): Unit =

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue


    given Board = TryBoard(if args.isEmpty then 4 else args(0).toInt, empty = true)

    println(given_Board)


    import dimensions.fifth.output.console.QueensConsoleOutput
    val p = QueensConsoleOutput(_: Nest)


    given Hatch.Flavor.Value = Hatch.Flavor.build

    Hatcher()


    var f = false

    var size = 0

    val hive = new Map[ActorRef[?], MutableList[Hive[Ps]]]().asScala

    val m = MutableMap[ActorRef[?], (Nest, LazyList[Solution])]()


    ActorSystem(queens(), "Queens") ! true

    def queens(): Behavior[Boolean] =

      Behaviors.setup { implicit outer => Behaviors
        .receiveMessage { st =>

          if st
          then

            val M = 2 // # actors (iterations)

            implicit val ps: MutableMap[(Int, UUID), Seq[(Long, Option[(ActorContext[?], ActorRef[?])])]] = MutableMap()

            val cbs = this(M,
              `*` { (tag, nest, uuid, _, r: ActorRef[?]) =>
                      m(r) = nest -> LazyList.from(Nil)
                  },
              `**`
            )

            (1 to M).foreach(vectorV1(cbs) ! Left(_))

            Behaviors.same

          else

            this()

            if !f then
              println(s"!!! NO SOLUTIONS${if end then " [INTERRUPTED]" else ""} !!!")

            Behaviors.stopped

        }
      }

    def vectorV1(cbs: Callbacks)(implicit
                 ps: scala.collection.Map[(Int, UUID), Seq[(Long, Option[(ActorContext[?], ActorRef[?])])]],
                 outer: ActorContext[Boolean]) =
      outer.spawnAnonymous[Either[Int, Int]](Behaviors
        .receive { (inner, msg) =>

          given Ps = Ps(inner, cbs)()

          msg match

            case Left(n) =>

              stop(start)

              val self = inner.self

              hive(self) = MutableList[Hive[Ps]]()

              val wildcard = Wildcard(
                version.less.nest.Wildcard( (_, _) match {
                  case (Aspect, Classic(Straight)) => true
                  // case (Aspect, Stepper(OneIncrement)) => true
                  // case (Aspect, Stepper(Permutations)) => true
                  case (Algorithm, Iterative) => n == 1
                  case (Algorithm, Recursive(Native)) => n == 2
                  case (Model, Parallel(Actors)) => true
                  case _ => false
                }),
                version.patch.v1.mbo.Wildcard.*
              )

              val multiplier = Multiplier[VectorV1[Ps]] {
                case VectorV1[Ps](Nest(_, Iterative, _), _) if n == 1 => 1
                case VectorV1[Ps](Nest(_, Recursive(Native), _), _) if n == 2 => 1
                case _ => 0
              }

              if wildcard(multiplier).isEmpty
              then
                println(s"!!! NO #$n EXPANSIONS !!!")

                stop()
                Behaviors.stopped

              else

                import version.v1.vector.protocol.VectorV1Message
                import version.v1.vector.interface.ObjectVectorV1ProtocolIfce

                import version.v1.vector.Implicits._

                size = implicitly[ObjectVectorV1ProtocolIfce[VectorV1Message]]
                  .SessionExpand(inner,
                    version.base.role.isInitial(_),
                    { () => self ! Right(n) },
                    println(_),
                    { (_: VectorV1[Ps], it: Option[?] => Hive[Ps]) =>
                      hive(self) += it(None)
                    },
                    wildcard,
                    multiplier
                  )

                if size > 0
                then
                  Behaviors.same
                else
                  stop()
                  Behaviors.stopped

            case Right(n) =>

              val self = inner.self

              val emito = `Hatch'em, Spawn'it, Buffered'o`(hive(self))

              try

                given Boolean = false // preemption off

                for
                  o <- emito `#` n
                  it <- o
                do
                  o(it) {
                    val Just(_, _, _, _, r, _) = o `just get` it
                    sync { m =+ r -> o(o.length-1) }
                  }
                  if end then
                    o stop it

                `#`(n -> hive(self).size)()

                for
                  (_, (nest, r)) <- m
                do
                  if !end then
                    val co = p(nest)
                    r.toList.foreach(co.||(_))
                  if r.nonEmpty then f = true

              finally
                stop()

              Behaviors.stopped

        }
      )
