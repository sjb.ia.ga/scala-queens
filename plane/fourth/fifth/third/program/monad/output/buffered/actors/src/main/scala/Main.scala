package queens
package plane.fourth.fifth.third
package program.monad.output
package buffered
package actors

import java.util.UUID

import scala.collection.mutable.{ HashMap => Map }

import akka.actor.typed.{ ActorSystem, ActorRef, Behavior }

import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import base.Board

import common.geom.{ row, col }

import common.monad.actors._
import common.monad.tag.integer.given

import dimensions.three.breed.{ Hatch, Hatcher }

import conf.mbo.actors.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.`Buffered'o`

import version.v1.vector.breed.`Hatch'em, Spawn'it, Buffered'o`
import version.v1.vector.breed.Hive

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._
import dimensions.Dimension.Model.{ actors, Parallel }
import dimensions.Dimension.Model.Parallel.Actors

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


object Main extends plane.fourth.fifth.third.program.monad.output.actors.ActorsApp:

  TryBoard()

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  def main(args: Array[String]): Unit =

    given Long = if args.length > 1 then args(1).toLong else 10 //Int.MaxValue

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        // case (Aspect, Stepper(OneIncrement)) => true
        // case (Aspect, Stepper(Permutations)) => true
        case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Model, Parallel(Actors)) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case Nest(_, Recursive(Native), _) => 1
      case Nest(_, Iterative, _) => 1
      case _ => 0
    }

    if wildcard(multiplier).isEmpty then
      println("!!! NO EXPANSIONS !!!")
      return


    ActorSystem(queens(), "Queens") ! true

    def queens(): Behavior[Boolean] =

      var f = false

      Behaviors.setup { implicit outer => Behaviors
        .receiveMessage { st =>

          if st
          then

            given Board = TryBoard(if args.isEmpty then 6 else args(0).toInt, empty = true)

            println(given_Board)

            import dimensions.fifth.output.console.QueensConsoleOutput
            val p = QueensConsoleOutput(_: Nest)


            val M = 1 // # actors (iterations)

            implicit val ps: Map[(Int, UUID), Seq[(Long, Option[(ActorContext[?], ActorRef[?])])]] = Map()

            val m = Map[ActorRef[?], (Nest, LazyList[Solution])]()

            val cbs = this(M,
              `*` { (_, nest, _, _, r: ActorRef[?]) =>
                      m(r) = nest -> LazyList.from(Nil)
                  },
              `**`
            )

            outer.spawnAnonymous[Int](Behaviors
              .receive { (inner, n) =>

                given Ps = Ps(inner, cbs)() // foreach/map/flatMap

                given Hatch.Flavor.Value = Hatch.Flavor.build

                Hatcher()

                val hive: Iterable[Hive[Ps]] = for {
                  it <- wildcard(multiplier)
                } yield {
                  val hatch = Hatcher(it)
                  val spawn = `Buffered'o`(it.model)
                  Hive(hatch, spawn)()
                }

                val emito = `Hatch'em, Spawn'it, Buffered'o`(hive)

                try
                  stop(start)

                  given Boolean = false // preemption off

                  for
                    o <- emito `#` n
                    //if o(_.model == actors)
                    it <- o
                    //if it.exists(_.row == 0) && it.exists(_.col == 0)
                  do
                    o(it) {
                      val Just(_, _, _, _, r, _) = o `just get` it
                      sync { m =+ r -> o(o.length-1) }
                    }
                    if end then
                      o stop it

                  `#`(n -> hive.size)()

                  for
                    (_, (nest, r)) <- m
                  do
                    if !end then
                      val co = p(nest)
                      r.toList.foreach(co.||(_))
                    if r.nonEmpty then f = true

                finally
                  stop()

                Behaviors.stopped
              }
            ) ! 1

            Behaviors.same

          else

            this()

            if !f then
              println("!!! NO SOLUTIONS !!!")

            Behaviors.stopped

        }
      }

