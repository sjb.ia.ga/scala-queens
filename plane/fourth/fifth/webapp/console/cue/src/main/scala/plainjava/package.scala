package queens
package plane.fourth.fifth
package webapp.console
package cue
package plainjava

import base.Board

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Classic._
import Stepper._
import Algorithm._
import Recursive._
import Model.Parallel
import Parallel.Futures

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


package webflux:

  object jMain:

    import queens.pojo.jackson.Queens
    import queens.pojo.jPojo.given

    def apply(response: Queens): Board = response.getBoard()

    def apply(): Option[(Wildcard, Multiplier[Nest])] =

      val wildcard = Wildcard (
        (_, _) match
          case (Algorithm, Recursive(Native)) => false
          case (Model, Parallel(Futures)) => true
          case (Model, _) => false
          case _ => true
      )

      val multiplier = Multiplier.Once[Nest]()

      // val wildcard = Wildcard (
      //   (_, _) match
      //     case (Aspect, Classic(Straight)) => true
      //     case (Aspect, Classic(Callback)) => true
      //     case (Aspect, Stepper(OneIncrement)) => true
      //     case (Aspect, Stepper(Permutations)) => true
      //     case (Algorithm, Iterative) => true
      //     case (Algorithm, Recursive(Extern)) => true
      //     case (Model, Parallel(Futures)) => true
      //     case _ => false
      // )

      // val multiplier = Multiplier[Nest] {
      //   case Nest(_, Recursive(Extern), _) => 1
      //   case Nest(_, Iterative, _) => 1
      //   case _ => 0
      // }

      if wildcard(multiplier).isEmpty then
        None
      else
        Some(wildcard -> multiplier)
