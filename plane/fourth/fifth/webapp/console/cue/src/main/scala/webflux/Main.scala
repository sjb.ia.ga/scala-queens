package queens
package plane.fourth.fifth
package webapp.console
package cue.webflux

import scala.util.CommandLineParser.FromString
import scala.collection.immutable.{ ListMap => Map }
import scala.collection.JavaConverters.mapAsScalaMapConverter

import org.springframework.http.MediaType.APPLICATION_JSON_VALUE

import common.monad.tag.integer.given

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Classic._
import Stepper._
import Algorithm._
import Recursive._
import Model.Parallel
import Parallel.Futures

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import dimensions.fourth.app.Parameter
import Parameter.Board.{ size, empty, seed }
import Parameter.Solution.max
import dimensions.fourth.app.Parameter.Cue.timeout

import dimensions.fourth.webapp.QueensWebAppInterface.Iteration

import axis.fourth.webapp.cue.webflux._
import axis.fourth.webapp.cue.webflux.Macros.{ `co*` => co }
import axis.fourth.webapp.cue.webflux.{ `WebFlux Web*`, `WebFlux Web App`, `WebFlux Web Client` }
import `WebFlux Web Client`.Get

import queens.jQueens.parse

import pojo.jackson.Queens
import pojo.jPojo.given

given FromString[Array[String]] = parse(_)(APPLICATION_JSON_VALUE)(458)

// @main
// def main(args: Array[String]): Unit =

object Main:

  def main(_args: Array[String]): Unit =

    val args = given_FromString_Array.fromString(_args.mkString(" "))

    println(args.toList)

    sys.BooleanProp
      .valueIsTrue(_root_.queens.main.Properties.WebSimple.plainjava)
  //    .enable()
      .disable()

    sys.Prop
      .StringProp(_root_.queens.main.Properties.WebSimple.scheme)
      .setValue(args(1))

    sys.Prop
      .StringProp(_root_.queens.main.Properties.WebSimple.host)
      .setValue(args(2))

    sys.Prop
      .IntProp(_root_.queens.main.Properties.WebSimple.port)
      .setValue(args(3).toInt)

    sys.Prop
      .StringProp(_root_.queens.main.Properties.WebSimple.infra)
      .setValue(if args.length > 5 then args(5) else "")

    sys.Prop
      .IntProp(_root_.queens.main.Properties.WebCue.timeout)
      .setValue(60000)

    val media = args(4)

    given Long = 10


    val `oh, snap!` = `WebFlux Web App` {
      val wildcard = Wildcard (
        (_, _) match
          case (Algorithm, Recursive(Extern)) => true
          case (Algorithm, Recursive(_)) => false
          case (Model, Parallel(Futures)) => true
          case (Model, _) => false
          case _ => true
      )

      val multiplier = Multiplier.Once[Nest]()

      // val wildcard = Wildcard (
      //   (_, _) match
      //     case (Aspect, Classic(Straight)) => true
      //     case (Aspect, Classic(Callback)) => true
      //     case (Aspect, Stepper(OneIncrement)) => true
      //     case (Aspect, Stepper(Permutations)) => true
      //     case (Algorithm, Iterative) => true
      //     case (Algorithm, Recursive(Extern)) => true
      //     case (Model, Parallel(Futures)) => true
      //     case _ => false
      // )

      // val multiplier = Multiplier[Nest] {
      //   case Nest(_, Recursive(_), _) => 1
      //   case Nest(_, Iterative, _) => 1
      //   case _ => 0
      // }

      if wildcard(multiplier).isEmpty then
        println("!!! NO EXPANSIONS !!!")
        Nil

      else
        val N = try Some(args(0).toInt) catch _ => None
        val M = Some(given_Long)
        val TO = Some {
          sys.Prop
            .IntProp(_root_.queens.main.Properties.WebCue.timeout)
            .value
        }
        var P = Long.MinValue
        while P == Long.MinValue do P = scala.util.Random.nextLong
        `WebFlux Web*`(media)(wildcard, multiplier)(size(N), empty(Some(true)), seed(Some(P)), max(M), timeout(TO))
    }


    def log(response: Queens = null): Get => Boolean = _ match
      case Get((model, matrix, query), (url, path), media, params*) =>
        println(s"Client: ${`oh, snap!`}")
        println(s"Request: Content-Type=$media")
        println(s"         URL=${url+path+"/model/"+model+"?"+query.asScala.map(_+"="+_.get(0)).mkString("&")}")
        println(s"         data={")
        println(s"                 aspects: {\n                             ${matrix.getAspects.asScala.map(_+":"+_).mkString(",\n                            ")}\n                          },")
        println(s"                 algorithms: {\n                                ${matrix.getAlgorithms.asScala.map(_+":"+_).mkString(",\n                                ")}\n                             }")
        println(s"              }")
        val ps = params.map(_.toString).mkString(" ; ")
        println(s"Parameters: $ps")
        if response ne null then
          println(s"Response: " +
                    Map(
                      "count" -> response.getCount,
                      "started" -> response.getStarted,
                      "ended" -> response.getEnded,
                      "elapsed" -> response.getElapsed,
                      "idle" -> response.getIdle
                    ).map(_+"="+"'"+_+"'").mkString(" , ")
          )
        true


    given Iteration = Iteration { case (n, _) if n > 2 => None case (n, pl) if n > 1 => pl case _ =>
      Some {

        import common.pipeline.Metadata.Media

        new `WebFlux Web Client`:

          override protected val >> = {
            case Media(_, Some(it: Get)) => { _ =>
              log()(it)
              true
            }
          }

          @inline override protected def >>(feed: Element => Unit): Unit =
            feed(>>)
            feed(super.>>)

      }
    }

    given Boolean = false // preemption off

    var f = false

    for
      it <- `oh, snap!` `#` 1
    do
      `oh, snap!`(it) {
        val Just(_, tag, _, _, responses, _*) = `oh, snap!` `just get` it

        co(tag, responses)(log(_)(_))() { (_, _) => f = true }
      }

    if !f then
      println("!!! NO SOLUTIONS !!!")
