val springBootVersion = "3.3.0"

libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",
  "org.springframework.boot" % "spring-boot-starter-web" % springBootVersion
    exclude("org.springframework.boot", "spring-boot-starter-tomcat"),
  "io.projectreactor" % "reactor-core" % "3.6.7",
  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
