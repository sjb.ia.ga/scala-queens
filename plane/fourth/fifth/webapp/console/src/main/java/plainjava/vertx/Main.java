package queens.plane.fourth.fifth.webapp.console.plainjava.vertx;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;

import scala.Option;
import scala.Function1;
import scala.Tuple2;
import scala.Tuple4;
import scala.util.Try;
import scala.collection.Iterable;
import scala.collection.immutable.Seq;
import scala.runtime.BoxedUnit;
import static scala.runtime.BoxedUnit.UNIT;
import static scala.collection.JavaConverters.asJava;
import static scala.collection.JavaConverters.asScala;

import io.reactivex.rxjava3.core.Single;

import io.vertx.core.Vertx;

import queens.base.Board;

import queens.common.Pipeline;
import queens.common.pipeline.Context.Round;

import queens.common.monad.Item;
import queens.common.monad.tag.integer$;
import queens.common.monad.Grow.ItemValue;

import queens.version.less.nest.Nest;
import queens.version.less.nest.Wildcard;
import queens.version.base.Multiplier;

import queens.dimensions.fourth.app.Parameter;
import queens.dimensions.fourth.app.Parameter$Board$size$;
import queens.dimensions.fourth.app.Parameter$Board$empty$;
import queens.dimensions.fourth.app.Parameter$Solution$max$;

import queens.dimensions.fourth.webapp.QueensWebAppInterface.Iteration;
import queens.dimensions.fourth.webapp.QueensWebAppInterface.Iteration;
import queens.dimensions.fourth.webapp.QueensWebAppInterface.Web;
import queens.dimensions.fourth.webapp.Web$u0020Client;

import queens.axis.fourth.webapp.plainjava.vertx.Vertx$Web$times;
import queens.axis.fourth.webapp.plainjava.vertx.Vertx$Web$App;
import queens.axis.fourth.webapp.plainjava.vertx.Vertx$Web$Client;
import static queens.axis.fourth.webapp.plainjava.vertx.Vertx$Web$Client.Get;

import queens.pojo.jackson.Queens;
import queens.dimensions.third.Queens$times;

import queens.jQueens$;
import queens.main.Properties.WebSimple$;

import queens.axis.fourth.webapp.plainjava.vertx.Just;
import queens.axis.fourth.webapp.plainjava.vertx.ToJust;

import queens.dimensions.fourth.webapp.TagKeyEmptyIterable;

import queens.dimensions.fifth.QueensUseSolution;
import queens.dimensions.fifth.output.console.QueensConsoleOutput$;

import queens.plane.fourth.fifth.webapp.console.plainjava.vertx.jMain$;

import queens.pojo.jackson.FromResponseToQueens$;


@SuppressWarnings("unchecked")
public class Main {

    private static FromResponseToQueens$ FRTQ$ = FromResponseToQueens$.MODULE$;

    private static LinkedList<String> mediaTypes = new LinkedList<>();

    static {
        mediaTypes.add("text/plain");
        mediaTypes.add("application/json");
    }

    private Main(String[] args) {
        Vertx$Web$Client client$verticle = new Vertx$Web$Client();
        Vertx.vertx().deployVerticle(client$verticle).onSuccess(v -> {
            System.setProperty(WebSimple$.MODULE$.plainjava(), "true");
            System.setProperty(WebSimple$.MODULE$.scheme(), args[1]);

            System.setProperty(WebSimple$.MODULE$.host(), args[2]);

            try {
                int port = Integer.parseInt(args[3]);
                System.setProperty(WebSimple$.MODULE$.port(), args[3]);
            } catch (NumberFormatException __) {
            }

            if (args.length > 5) {
                System.setProperty(WebSimple$.MODULE$.infra(), args[5]);
            }

            String media = args[4];

            Long given_Long = 10L;

            Option<Tuple2<Wildcard, Multiplier<Nest>>> opt = jMain$.MODULE$.apply();

            if (opt.isEmpty()) {
                System.out.println("!!! NO EXPANSIONS !!!");
                return;
            }

            Wildcard wildcard = opt.get()._1();
            Multiplier<Nest> multiplier = opt.get()._2();

            Option<Object> N = Option.empty();
            try { N = Option.apply(Integer.parseInt(args[0])); } finally {}
            Option<Object> M = Option.apply(given_Long);

            LinkedList<Parameter<?>> params = new LinkedList<>();
            Parameter<?> size = Parameter$Board$size$.MODULE$.apply(N);
            Parameter<?> empty = Parameter$Board$empty$.MODULE$.apply(Option.apply(true));
            Parameter<?> max = Parameter$Solution$max$.MODULE$.apply(N);
            params.add(size); params.add(empty); params.add(max);

            Iterable<Web<Single<Queens>, Get>> webs = Vertx$Web$times.$().apply(media, wildcard, multiplier, asScala(params).toSeq());

            Vertx$Web$App oh$u002C$u0020snap$bang = Vertx$Web$App.apply(webs);

            BiFunction<Queens, Get, Boolean> log = (Queens response, Get it) -> {
                System.out.println("Client: " + oh$u002C$u0020snap$bang.toString());
                System.out.println("Request: Accept=" + it.media());
                System.out.println("         URL=" + it.url());

                StringBuilder sb = new StringBuilder("Parameters:");
                for (Parameter<?> param : asJava(it.params()))
                    sb
                        .append(" ")
                        .append(param.toString())
                        .append(" ;");
                System.out.println(sb.toString());

                if (response != null) {
                    sb = new StringBuilder("Response:")
                        .append(" " + "count" + "='" + response.getCount() + "' ," )
                        .append(" " + "started" + "='" + response.getStarted() + "' ," )
                        .append(" " + "ended" + "='" + response.getEnded() + "' ," )
                        .append(" " + "elapsed" + "='" + response.getElapsed() + "' ," )
                        .append(" " + "idle" + "='" + response.getIdle() + "' ," );
                    System.out.println(sb.toString());
                }

                return true;
            };

            boolean given_Boolean = false; // preemption off

            oh$u002C$u0020snap$bang.$hash(1).foreach(
                _pl -> {
                    Item<Web$u0020Client> pl = (Item<Web$u0020Client>) _pl;
                    oh$u002C$u0020snap$bang.<BoxedUnit>apply(
                        pl,
                        (__, ___) -> {
                            Function1<Object, Object> given_tag_integer = integer$.MODULE$.given_Any_to_Int();

                            ToJust<Integer> toJ = new ToJust<Integer>(tag -> (Integer) given_tag_integer.apply(tag));

                            Just<Integer> just = (Just<Integer>) oh$u002C$u0020snap$bang.<Integer, ItemValue<?>>just$u0020get(pl, toJ);

                            Iterable<Tuple4<Object, Get, Try<Single<Queens>>, Function1<Single<Queens>, Iterable<Queens$times<Tuple2<Object, Object>, Object>>>>> responses = just.responses();

                            for (Tuple4<Object, Get, Try<Single<Queens>>, Function1<Single<Queens>, Iterable<Queens$times<Tuple2<Object, Object>, Object>>>> response : asJava(responses)) {
                                Get method = response._2();

                                if (response._3().isFailure())
                                    continue;

                                Single<Queens> single = response._3().get();

                                Function1<Single<Queens>, Iterable<Queens$times<Tuple2<Object, Object>, Object>>> frtq = response._4();

                                TagKeyEmptyIterable<Queens$times<Tuple2<Object, Object>, Object>> tk = (TagKeyEmptyIterable<Queens$times<Tuple2<Object, Object>, Object>>) frtq.apply(single);

                                single.subscribe(
                                    pojo -> {
                                        if (log.apply(pojo, method)) {
                                            Board board = jMain$.MODULE$.apply(pojo);

                                            Iterable<Queens$times<Tuple2<Object, Object>, Object>> results = FRTQ$.apply(pojo, tk.tag(), tk.key());
                                            for (Queens$times<Tuple2<Object, Object>, Object> queens : asJava(results)) {
                                                QueensUseSolution co = QueensConsoleOutput$.MODULE$.apply(queens);

                                                AtomicInteger number = new AtomicInteger(1);
                                            queens.foreach(
                                                it -> {
                                                    if (number.get() == 0)
                                                        return UNIT;

                                                    Round round = Round.apply(board, 1, queens, number.get());

                                                    if (co.$bar$bar(it.the(), round, Option.empty()))
                                                        number.incrementAndGet();
                                                    else
                                                        number.set(0);

                                                    return UNIT;
                                                }
                                                , given_Long
                                                , given_Boolean
                                                           );
                                            }
                                        }
                                    });
                            };
                            return UNIT;
                        }
                    );
                    return UNIT;
                },
                new Iteration((_n, pl) -> {
                    long n = (long) _n;
                    if (n > 2)
                        return Option.empty();
                    if (n > 1)
                        return pl;

                    return Option.apply(client$verticle);
                })
                , given_Boolean
            );
        });
    }

    public static void main(String[] args) {
        String s = String.join(" ", args);
        Seq<String> ms = asScala(mediaTypes).toSeq();
        int to = jQueens$.MODULE$.parse$default$3(s, ms);
        new Main(jQueens$.MODULE$.parse(s, ms, to));
    }
}
