package queens
package plane.fourth.fifth
package webapp.console
package spring

import scala.util.CommandLineParser.FromString
import scala.collection.immutable.{ ListMap => Map }

import org.springframework.http.MediaType.{ APPLICATION_JSON_VALUE
                                          , APPLICATION_XML_VALUE
                                          , TEXT_PLAIN_VALUE }

import common.monad.tag.integer.given

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Classic._
import Stepper._
import Algorithm._
import Recursive._
import Model.{ flow, Flow }

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import dimensions.fourth.app.Parameter
import Parameter.Board.{ size, empty }
import Parameter.Solution.max

import dimensions.fourth.webapp.QueensWebAppInterface.Iteration

import axis.fourth.webapp.spring._
import axis.fourth.webapp.spring.Macros.{ `co*` => co }
import axis.fourth.webapp.spring.{ `Spring Web*`, `Spring Web App`, `Spring Web Client` }
import `Spring Web Client`.Get

import queens.jQueens.parse

import pojo.jackson.Queens
import pojo.jPojo.given

given FromString[Array[String]] = parse(_)(TEXT_PLAIN_VALUE
                                          ,APPLICATION_JSON_VALUE
                                          ,APPLICATION_XML_VALUE)()

// @main
// def main(args: Array[String]): Unit =

object Main:

  def main(_args: Array[String]): Unit =

    val args = given_FromString_Array.fromString(_args.mkString(" "))

    println(args.toList)

    sys.BooleanProp
      .valueIsTrue(_root_.queens.main.Properties.WebSimple.plainjava)
  //    .enable()
      .disable()

    sys.Prop
      .StringProp(_root_.queens.main.Properties.WebSimple.scheme)
      .setValue(args(1))

    sys.Prop
      .StringProp(_root_.queens.main.Properties.WebSimple.host)
      .setValue(args(2))

    sys.Prop
      .IntProp(_root_.queens.main.Properties.WebSimple.port)
      .setValue(args(3).toInt)

    sys.Prop
      .StringProp(_root_.queens.main.Properties.WebSimple.infra)
      .setValue(if args.length > 5 then args(5) else "")

    val media = args(4)

    given Long = 10


    val `oh, snap!` = `Spring Web App` {
      val wildcard = Wildcard (
        (_, _) match
          // case (Aspect, Stepper(OneIncrement)) => true
          // case (Aspect, Stepper(Permutations)) => true
          // case (Algorithm, Iterative) => true
          case (Algorithm, Recursive(Native)) => true
          case (Aspect, Classic(Straight)) => true
          case (Model, Flow) => true
          case _ => false
      )

      val multiplier = Multiplier[Nest] {
        case Nest(_, Recursive(_), _) => 2
        case Nest(_, Iterative, _) => 1
        case _ => 0
      }

      if wildcard(multiplier).isEmpty then
        println("!!! NO EXPANSIONS !!!")
        Nil

      else
        val N = try Some(args(0).toInt) catch _ => None
        val M = Some(given_Long)
        `Spring Web*`(media)(wildcard, multiplier)(size(N), empty(Some(false)), max(M))
    }


    def log(response: Queens = null): Get => Boolean = _ match
      case Get(_, url, media, params*) =>
        println(s"Client: ${`oh, snap!`}")
        println(s"Request: Accept=$media")
        println(s"         URL=$url")
        val ps = params.map(_.toString).mkString(" ; ")
        println(s"Parameters: $ps")
        if response ne null then
          println(s"Response: " +
                    Map(
                      "count" -> response.getCount,
                      "started" -> response.getStarted,
                      "ended" -> response.getEnded,
                      "elapsed" -> response.getElapsed,
                      "idle" -> response.getIdle
                    ).map(_+"="+"'"+_+"'").mkString(" , ")
          )
        true


    given Iteration = Iteration { case (n, _) if n > 2 => None case (n, pl) if n > 1 => pl case _ =>
      Some {

        import common.pipeline.Metadata.Media

        new `Spring Web Client`:

          override protected val >> = {
            case Media(_, Some(it: Get)) => { _ =>
              log()(it)
              true
            }
          }

          @inline override protected def >>(feed: Element => Unit): Unit =
            feed(>>)
            feed(super.>>)

      }
    }

    given Boolean = false // preemption off

    var f = false

    for
      it <- `oh, snap!` `#` 1
    do
      `oh, snap!`(it) {
        val Just(_, tag, _, _, _, results*) = `oh, snap!` `just get` it

        co(tag, results)(log(_)(_))() { (_, _) => f = true }
      }

    if !f then
      println("!!! NO SOLUTIONS !!!")
