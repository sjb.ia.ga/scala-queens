package queens
package plane.fourth.fifth
package webapp.console
package plainjava

import base.Board

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Classic._
import Stepper._
import Algorithm._
import Recursive._
import Model.{ flow, Flow }

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


package micronaut:

  object jMain:

    import queens.pojo.jackson.Queens
    import queens.pojo.jPojo.given

    def apply(response: Queens): Board = response.getBoard()

    def apply(): Option[(Wildcard, Multiplier[Nest])] =

      val wildcard = Wildcard (
        (_, _) match
          // case (Aspect, Stepper(OneIncrement)) => true
          // case (Aspect, Stepper(Permutations)) => true
          // case (Algorithm, Iterative) => true
          case (Algorithm, Recursive(Continuation)) => false
          case (Algorithm, Recursive(Native)) => true
          case (Aspect, Classic(Straight)) => true
          case (Model, Flow) => true
          case _ => false
      )

      val multiplier = Multiplier[Nest] {
        case Nest(_, Recursive(_), _) => 1
        case Nest(_, Iterative, _) => 1
        case _ => 0
      }

      if wildcard(multiplier).isEmpty then
        None
      else
        Some(wildcard -> multiplier)


package okhttp:

  object jMain:

    import queens.pojo.moshi.Queens
    import queens.pojo.jPojo.given

    def apply(response: Queens): Board = response.getBoard()

    def apply(): Option[(Wildcard, Multiplier[Nest])] =

      val wildcard = Wildcard (
        (_, _) match
          // case (Aspect, Stepper(OneIncrement)) => true
          // case (Aspect, Stepper(Permutations)) => true
          // case (Algorithm, Iterative) => true
          case (Algorithm, Recursive(Continuation)) => false
          case (Algorithm, Recursive(Native)) => true
          case (Aspect, Classic(Straight)) => true
          case (Model, Flow) => true
          case _ => false
      )

      val multiplier = Multiplier[Nest] {
        case Nest(_, Recursive(_), _) => 1
        case Nest(_, Iterative, _) => 1
        case _ => 0
      }

      if wildcard(multiplier).isEmpty then
        None
      else
        Some(wildcard -> multiplier)


package retrofit:

  object jMain:

    import queens.pojo.gson.Queens
    import queens.pojo.jPojo.given

    def apply(response: Queens): Board = response.getBoard()

    def apply(): Option[(Wildcard, Multiplier[Nest])] =

      val wildcard = Wildcard (
        (_, _) match
          // case (Aspect, Stepper(OneIncrement)) => true
          // case (Aspect, Stepper(Permutations)) => true
          // case (Algorithm, Iterative) => true
          case (Algorithm, Recursive(Continuation)) => false
          case (Algorithm, Recursive(Native)) => true
          case (Aspect, Classic(Straight)) => true
          case (Model, Flow) => true
          case _ => false
      )

      val multiplier = Multiplier[Nest] {
        case Nest(_, Recursive(_), _) => 1
        case Nest(_, Iterative, _) => 1
        case _ => 0
      }

      if wildcard(multiplier).isEmpty then
        None
      else
        Some(wildcard -> multiplier)


package spring:

  object jMain:

    import queens.pojo.jackson.Queens
    import queens.pojo.jPojo.given

    def apply(response: Queens): Board = response.getBoard()

    def apply(): Option[(Wildcard, Multiplier[Nest])] =

      val wildcard = Wildcard (
        (_, _) match
          // case (Aspect, Stepper(OneIncrement)) => true
          // case (Aspect, Stepper(Permutations)) => true
          // case (Algorithm, Iterative) => true
          case (Algorithm, Recursive(Continuation)) => false
          case (Algorithm, Recursive(Native)) => true
          case (Aspect, Classic(Straight)) => true
          case (Model, Flow) => true
          case _ => false
      )

      val multiplier = Multiplier[Nest] {
        case Nest(_, Recursive(_), _) => 1
        case Nest(_, Iterative, _) => 1
        case _ => 0
      }

      if wildcard(multiplier).isEmpty then
        None
      else
        Some(wildcard -> multiplier)


package vertx:

  object jMain:

    import queens.pojo.jackson.Queens
    import queens.pojo.jPojo.given

    def apply(response: Queens): Board = response.getBoard()

    def apply(): Option[(Wildcard, Multiplier[Nest])] =

      val wildcard = Wildcard (
        (_, _) match
          // case (Aspect, Stepper(OneIncrement)) => true
          // case (Aspect, Stepper(Permutations)) => true
          // case (Algorithm, Iterative) => true
          case (Algorithm, Recursive(Continuation)) => false
          case (Algorithm, Recursive(Native)) => true
          case (Aspect, Classic(Straight)) => true
          case (Model, Flow) => true
          case _ => false
      )

      val multiplier = Multiplier[Nest] {
        case Nest(_, Recursive(_), _) => 1
        case Nest(_, Iterative, _) => 1
        case _ => 0
      }

      if wildcard(multiplier).isEmpty then
        None
      else
        Some(wildcard -> multiplier)
