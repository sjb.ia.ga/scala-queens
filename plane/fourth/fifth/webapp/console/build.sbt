val springBootVersion = "3.3.0"
val http4sVersion = "0.22.15"

libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",
  "io.micronaut" % "micronaut-http" % "4.5.3",
  "io.micronaut" % "micronaut-core" % "4.5.3" % "runtime",
  "io.micronaut" % "micronaut-inject" % "4.5.3" % "runtime",
  "io.micronaut" % "micronaut-inject-java" % "4.5.3" % "runtime",
  "org.springframework.boot" % "spring-boot-starter-web" % springBootVersion
    exclude("org.springframework.boot", "spring-boot-starter-tomcat"),
  "com.softwaremill.sttp.model" %% "core" % "1.7.2",
  "io.vertx" % "vertx-rx-java3" % "4.5.8",
  "org.http4s" %% "http4s-core" % http4sVersion,
  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
