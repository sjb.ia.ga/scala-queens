package queens
package dimensions.fifth
package output.database

import scala.Function.const

import common.pipeline.Context.Round
import common.pipeline.Metadata
import Metadata.Boot
import Metadata.Database.Store

import dimensions.fifth.output.QueensOutputUseSolution


abstract trait QueensDatabaseOutput
    extends QueensOutputUseSolution:

  protected val callback: (Solution, Long) => Boolean

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(_, ctxt: Round, _) => Some(Store(ctxt, None))

  override protected val >> = QueensDatabaseOutput(callback)

  override def toString(): String = "Database :: " + super.toString


object QueensDatabaseOutput:

  private def apply(callback: (Solution, Long) => Boolean)
                   (metadata: Metadata): Solution => Boolean =
    metadata match
      case Store(Round(_, _, _, number), _) => callback(_, number)
      case _ => const(false)
