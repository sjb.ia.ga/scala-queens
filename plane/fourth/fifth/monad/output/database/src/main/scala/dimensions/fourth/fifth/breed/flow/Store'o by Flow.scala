package queens
package dimensions.fourth.fifth
package breed
package flow

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import dimensions.third.Queens

import conf.mdo.flow.Conf

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution

import common.Flag
import common.monad.Item


abstract class `Base.Store'o by Flow`[
  P <: Conf
] protected (
  _qs: Queens[?],
  _tag: Any,
  _its: Seq[UUID],
  _run: Either[Simulate, Boolean],
  _fs: Item[Solution] => Boolean*
)(
  _next: Option[Context => QueensUseSolution]
)(using
  override protected val * : P
)(using
  `_its*`: Map[UUID, (Boolean, Flag)],
  _pre: Boolean
) extends `Base.Output *'o by Flow`[P](_qs, _tag, _its, _run, _fs*)(_next)
    with `Store'o`[P]
