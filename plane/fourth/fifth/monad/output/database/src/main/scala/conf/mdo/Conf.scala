package queens
package conf.mdo


abstract trait Conf
    extends conf.emito.Conf:

  import dimensions.fourth.fifth.breed.`Store'o`

  override type P <: Conf

  override type T <: `Store'o`[P]
