package queens
package dimensions.fourth.fifth
package breed

import conf.mdo.Conf

import dimensions.fourth.fifth.breed.mdo.QueensMonadDatabaseOutput


abstract trait `Store'o`[
  P <: Conf
](using
  override protected val * : P
) extends `Output *'o`[P]
    with QueensMonadDatabaseOutput[P]:

  override def toString(): String = "Store'o " + super.toString
