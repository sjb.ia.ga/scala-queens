package queens
package dimensions.fourth.fifth
package breed
package mdo

import conf.mdo.Conf

import dimensions.fifth.output.database.QueensDatabaseOutput


abstract trait QueensMonadDatabaseOutput[
  P <: Conf
] extends QueensMonadOutput[P]
    with QueensDatabaseOutput
