package queens
package version.v1
package vector
package breed

import common.monad.Item

import conf.mdo.Conf


abstract trait `Hatch'em, Spawn'it, Store'o`[
  P <: Conf
](using
  override protected val * : P
) extends `Hatch'em, Spawn'it, Output'o`[P]:

  override protected type * <: `Hatch'em, Spawn'it, Store'o`[P]

  override def toString(): String = "Store " + super.toString
