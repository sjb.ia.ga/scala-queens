package queens
package conf.mdo.flow


abstract trait Conf
    extends conf.mdo.Conf
    with conf.emito.flow.Conf
