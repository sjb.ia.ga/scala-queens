package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package dao
package delete

import _root_.scalikejdbc._

import axis.fifth.output.database.Mappings.DbQueen

import dimensions.fifth.output.database.scalikejdbc.dao.ScalikejdbcDatabaseDAO


class `Impl.Scalikejdbc delete Query Queens'dao`(
  _id: Long
)(implicit
  _dao: ScalikejdbcDatabaseDAO
) extends `Impl.Scalikejdbc Query Queens'dao`(_id, null)
    with `Scalikejdbc delete Query *'dao`[DbQueen]
