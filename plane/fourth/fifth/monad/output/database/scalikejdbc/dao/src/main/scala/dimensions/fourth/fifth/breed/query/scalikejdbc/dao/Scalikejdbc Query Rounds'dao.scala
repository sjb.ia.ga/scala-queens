package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package dao

import _root_.scalikejdbc._

import dimensions.fifth.output.database.scalikejdbc.dao.ScalikejdbcDatabaseDAO


class `Impl.Scalikejdbc Query Rounds'dao`(
  _id: Long,
  _tag: String
)(implicit
  _dao: ScalikejdbcDatabaseDAO
) extends `Base.Scalikejdbc Query *'dao`(_id, _tag)
    with `Scalikejdbc Query Rounds'dao`


import axis.fifth.output.database.Mappings.DbRound

abstract trait `Scalikejdbc Query Rounds'dao`
    extends `Scalikejdbc Query *'dao` {

  @inline def mapRounds(
    `rounds?`: Option[Iterable[Long]]
  ): Seq[DbRound] =
    NamedDB(dao.driver) localTx { implicit session =>
      dao.mapRounds(`rounds?`)
    }

}
