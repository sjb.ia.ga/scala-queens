package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package dao

import _root_.scalikejdbc._

import dimensions.fifth.output.database.scalikejdbc.dao.ScalikejdbcDatabaseDAO


class `Impl.Scalikejdbc Query Queens'dao`(
  _id: Long,
  _tag: String
)(implicit
  _dao: ScalikejdbcDatabaseDAO
) extends `Base.Scalikejdbc Query *'dao`(_id, _tag)
    with `Scalikejdbc Query Queens'dao`


import axis.fifth.output.database.Mappings.DbQueen

import version.less.nest.Nest

abstract trait `Scalikejdbc Query Queens'dao`
    extends `Scalikejdbc Query *'dao` {

  @inline def mapQueens(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  ): Seq[DbQueen] =
    NamedDB(dao.driver) localTx { implicit session =>
      dao.mapQueens(`nests?`, `rounds?`)
    }

}
