package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package dao

import _root_.scalikejdbc._

import dimensions.fifth.output.database.scalikejdbc.dao.ScalikejdbcDatabaseDAO


abstract class `Base.Scalikejdbc Query *'dao` protected (
  override protected val boardId: Long,
  override protected val tag: String
)(implicit
  override protected val dao: ScalikejdbcDatabaseDAO
) extends `Scalikejdbc Query *'dao`


abstract trait `Scalikejdbc Query *'dao` {

  implicit protected val boardId: Long

  implicit protected val tag: String

  protected val dao: ScalikejdbcDatabaseDAO

  final val plainSQL = dao.plainSQL

  override def toString(): String = dao.toString

}
