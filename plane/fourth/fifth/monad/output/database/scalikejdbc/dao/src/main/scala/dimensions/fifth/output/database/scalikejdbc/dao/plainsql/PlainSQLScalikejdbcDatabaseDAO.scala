package queens
package dimensions.fifth
package output.database
package scalikejdbc
package dao
package plainsql

import scala.collection.Map
import scala.collection.mutable.{ ListBuffer => MutableList, LinkedHashMap => MutableMap }

import _root_.scalikejdbc._

import axis.fifth.output.database.dbdimensions.Mappings.{
  DbAspect,
  DbAlgorithm,
  DbModel,
  DbInterface,
  DbUse
}

import axis.fifth.output.database.Mappings._

import dimensions.Dimension.Use.Output.Database.Scalike.Driver

import version.less.nest.Nest


abstract trait PlainSQLScalikejdbcDatabaseDAO {

  val driver: Symbol

  val scalikeDriver: Driver

  import axis.fifth.output.database.Implicits._
  import axis.fifth.output.database.scalikejdbc.plainsql.dbdimensions.X

  def mapAspect(
    id: Long
  )(implicit
    session: DBSession
  ): DbAspect = {

    import axis.fifth.output.database.dbdimensions.Mappings.Classic._
    import axis.fifth.output.database.dbdimensions.Mappings.Stepper._
    import axis.fifth.output.database.dbdimensions.Mappings.{ DbClassicAspect, DbStepperAspect }

    var aspect: Option[DbAspect] = None

    if (aspect.isEmpty)
      aspect =
        sql"""select at."ASPECT", at."CLASSIC_ID", lk."LOOK", cl."LOOK_ID"
              from "ASPECTS" at
              join "CLASSICS" cl on at."CLASSIC_ID" = cl."ID"
              join "LOOKS" lk on cl."LOOK_ID" = lk."ID"
              where at."ID" = $id and at."STEPPER_ID" = $X
           """
          .map(rs => (rs.string("ASPECT"), rs.int("CLASSIC_ID"), rs.string("LOOK"), rs.int("LOOK_ID")))
          .list()()
          .headOption
          .map { case (it, cl_id, look, lk_id) =>
            DbAspect(it
                    ,Some(
                      DbClassicAspect(
                        DbClassic(
                          DbLook(look
                                ,lk_id.?
                          )
                          ,cl_id.?
                        )
                        ,id.?
                      )
                    )
                    ,cl_id.?
                    ,None
                    ,None
                    ,id.?
            )
          }

    if (aspect.isEmpty)
      aspect =
        sql"""select at."ASPECT", at."STEPPER_ID", nx."NEXT", st."NEXT_ID"
              from "ASPECTS" at
              join "STEPPERS" st on at."STEPPER_ID" = st."ID"
              join "NEXTS" nx on st."NEXT_ID" = nx."ID"
              where at."ID" = $id and at."CLASSIC_ID" = $X
           """
          .map(rs => (rs.string("ASPECT"), rs.int("STEPPER_ID"), rs.string("NEXT"), rs.int("NEXT_ID")))
          .list()()
          .headOption
          .map { case (it, st_id, next, nx_id) =>
            DbAspect(it
                    ,None
                    ,None
                    ,Some(
                      DbStepperAspect(
                        DbStepper(
                          DbNext(next
                                ,nx_id.?
                          )
                          ,st_id.?
                        )
                        ,id.?
                      )
                    )
                    ,st_id.?
                    ,id.?
            )
          }

    assert(aspect.isDefined)

    aspect.get
  }

  def mapAlgorithm(
    id: Long
  )(implicit
    session: DBSession
  ): DbAlgorithm = {

    import axis.fifth.output.database.dbdimensions.Mappings.Recursive._
    import axis.fifth.output.database.dbdimensions.Mappings.DbRecursiveAlgorithm

    var algorithm: Option[DbAlgorithm] = None

    if (algorithm.isEmpty)
      algorithm =
        sql"""select am."ALGORITHM"
              from "ALGORITHMS" am
              where am."ID" = $id and am."RECURSIVE_ID" = $X
           """
          .map(rs => rs.string("ALGORITHM"))
          .list()()
          .headOption
          .map { it =>
            DbAlgorithm(it
                       ,None
                       ,None
                       ,id.?
            )
          }

    if (algorithm.isEmpty)
      algorithm =
        sql"""select am."ALGORITHM", am."RECURSIVE_ID", nt."NATURE", rc."NATURE_ID"
              from "ALGORITHMS" am
              join "RECURSIVES" rc on am."RECURSIVE_ID" = rc."ID"
              join "NATURES" nt on rc."NATURE_ID" = nt."ID"
              where am."ID" = $id and not am."RECURSIVE_ID" = $X
           """
          .map(rs => (rs.string("ALGORITHM"), rs.int("RECURSIVE_ID"), rs.string("NATURE"), rs.int("NATURE_ID")))
          .list()()
          .headOption
          .map { case (it, rc_id, nature, nt_id) =>
            DbAlgorithm(it
                       ,Some(
                         DbRecursiveAlgorithm(
                           DbRecursive(
                             DbNature(nature
                                     ,nt_id.?
                             )
                             ,rc_id.?
                           )
                           ,id.?
                         )
                       )
                       ,rc_id.?
                       ,id.?
            )
          }

    assert(algorithm.isDefined)

    algorithm.get
  }

  def mapModel(
    id: Long
  )(implicit
    session: DBSession
  ): DbModel = {

    import axis.fifth.output.database.dbdimensions.Mappings.Parallel._
    import axis.fifth.output.database.dbdimensions.Mappings.DbParallelModel

    var model: Option[DbModel] = None

    if (model.isEmpty)
      model =
        sql"""select ml."MODEL"
              from "MODELS" ml
              where ml."ID" = $id and ml."PARALLEL_ID" = $X
           """
          .map(rs => rs.string("MODEL"))
          .list()()
          .headOption
          .map { it =>
            DbModel(it
                   ,None
                   ,None
                   ,id.?
            )
          }

    if (model.isEmpty)
      model =
        sql"""select ml."MODEL", ml."PARALLEL_ID", pd."PARADIGM", pl."PARADIGM_ID"
              from "MODELS" ml
              join "PARALLELS" pl on ml."PARALLEL_ID" = pl."ID"
              join "PARADIGMS" pd on pl."PARADIGM_ID" = pd."ID"
              where ml."ID" = $id and not ml."PARALLEL_ID" = $X
           """
          .map(rs => (rs.string("MODEL"), rs.int("PARALLEL_ID"), rs.string("PARADIGM"), rs.int("PARADIGM_ID")))
          .list()()
          .headOption
          .map { case (it, pl_id, paradigm, pd_id) =>
            DbModel(it
                   ,Some(
                     DbParallelModel(
                       DbParallel(
                         DbParadigm(paradigm
                                   ,pd_id.?
                         )
                         ,pl_id.?
                       )
                       ,id.?
                     )
                   )
                   ,pl_id.?
                   ,id.?
            )
          }

    assert(model.isDefined)

    model.get
  }

  def mapInterface(
    id: Long
  )(implicit
    session: DBSession
  ): DbInterface = {

    import axis.fifth.output.database.dbdimensions.Mappings.Program._
    import axis.fifth.output.database.dbdimensions.Mappings.DbProgramInterface

    var interface: Option[DbInterface] = None

    if (interface.isEmpty)
      interface =
        sql"""select ie."INTERFACE", ie."PROGRAM_ID", st."STYLE", pr."STYLE_ID"
              from "INTERFACES" ie
              join "PROGRAMS" pr on ie."PROGRAM_ID" = pr."ID"
              join "STYLES" st on pr."STYLE_ID" = st."ID"
           """
          .map(rs => (rs.string("INTERFACE"), rs.int("PROGRAM_ID"), rs.string("STYLE"), rs.int("STYLE_ID")))
          .list()()
          .headOption
          .map { case (it, pr_id, style, st_id) =>
            DbInterface(it
                       ,DbProgramInterface(
                         DbProgram(
                           DbStyle(style
                                  ,st_id.?
                           )
                           ,st_id
                           ,pr_id.?
                         )
                         ,id.?
                       )
                       ,pr_id
                       ,id.?
            )
          }

    assert(interface.isDefined)

    interface.get
  }

  def mapUse(
    id: Long
  )(implicit
    session: DBSession
  ): DbUse = {

    import axis.fifth.output.database.dbdimensions.Mappings.Output.Database.Slick._
    import axis.fifth.output.database.dbdimensions.Mappings.Output.Database.Scalike._
    import axis.fifth.output.database.dbdimensions.Mappings.Output.Database.DbDatabase
    import axis.fifth.output.database.dbdimensions.Mappings.Output.{ DbDatabaseKind, DbKind, DbDatabaseOutput, DbOutput }
    import axis.fifth.output.database.dbdimensions.Mappings.{ DbDatabaseOutputUse, DbOutputUse, DbUse }

    var use: Option[DbUse] = None

    if (use.isEmpty)
      use =
        sql"""select ue."USE", ue."OUTPUT_ID", ki."KIND", ot."KIND_ID"
              from "USES" ue
              join "OUTPUTS" ot on ue."OUTPUT_ID" = ot."ID"
              join "KINDS" ki on ot."KIND_ID" = ki."ID"
              where ue."ID" = $id and ki."DATABASE_ID" = $X
           """
          .map(rs => (rs.string("USE"), rs.int("OUTPUT_ID"), rs.string("KIND"), rs.int("KIND_ID")))
          .list()()
          .headOption
          .map { case (it, ot_id, kind, ki_id) =>
            DbUse(it
                 ,DbOutputUse(
                   DbOutput(
                     DbKind(kind
                           ,None
                           ,ki_id.?
                     )
                     ,ki_id
                     ,ot_id.?
                   )
                   ,None
                   ,id.?
                 )
                 ,ot_id
                 ,id.?
            )
          }

    if (use.isEmpty)
      use =
        sql"""select ue."USE", ue."OUTPUT_ID",
                     ot."KIND_ID", ki."KIND",
                     ki."DATABASE_ID", db."STORAGE",
                     db."SLICK_PROFILE_ID", sp."PROFILE", sp."PLAIN_SQL"
              from "USES" ue
              join "OUTPUTS" ot on ue."OUTPUT_ID" = ot."ID"
              join "KINDS" ki on ot."KIND_ID" = ki."ID"
              join "DATABASES" db on ki."DATABASE_ID" = db."ID"
              join "SLICK_PROFILES" sp on db."SLICK_PROFILE_ID" = sp."ID"
              where ue."ID" = $id
              and not ki."DATABASE_ID" = $X
              and not db."SLICK_PROFILE_ID" = $X
           """
          .map(rs => (rs.string("USE"), rs.int("OUTPUT_ID")
                     ,rs.int("KIND_ID"), rs.string("KIND")
                     ,rs.int("DATABASE_ID"), rs.string("STORAGE")
                     ,rs.int("SLICK_PROFILE_ID"), rs.string("PROFILE"), rs.boolean("PLAIN_SQL")))
          .list()()
          .headOption
          .map { case (it, ot_id, ki_id, kind, db_id, storage, sp_id, profile, plainSQL) =>
            DbUse(it
                 ,DbOutputUse(
                   DbOutput(
                     DbKind(kind
                           ,db_id.?
                           ,ki_id.?
                     )
                     ,ki_id
                     ,ot_id.?
                   )
                   ,Some(
                     DbDatabaseOutputUse(
                       DbDatabaseOutput(
                         DbDatabaseKind(
                           DbDatabase(storage
                                     ,Some(
                                       DbSlickProfile(profile
                                                     ,plainSQL
                                                     ,sp_id.?
                                       )
                                     )
                                     ,sp_id.?
                                     ,None
                                     ,None
                                     ,db_id.?
                           )
                           ,db_id
                           ,ki_id.?
                         )
                         ,ot_id.?
                       )
                       ,id.?
                     )
                   )
                   ,id.?
                 )
                 ,ot_id
                 ,id.?
            )
          }

    if (use.isEmpty)
      use =
        sql"""select ue."USE", ue."OUTPUT_ID",
                     ot."KIND_ID", ki."KIND",
                     ki."DATABASE_ID", db."STORAGE",
                     db."SCALIKE_DRIVER_ID", sd."DRIVER", sd."PLAIN_SQL"
              from "USES" ue
              join "OUTPUTS" ot on ue."OUTPUT_ID" = ot."ID"
              join "KINDS" ki on ot."KIND_ID" = ki."ID"
              join "DATABASES" db on ki."DATABASE_ID" = db."ID"
              join "SCALIKE_DRIVERS" sd on db."SCALIKE_DRIVER_ID" = sd."ID"
              where ue."ID" = $id
              and not ki."DATABASE_ID" = $X
              and not db."SCALIKE_DRIVER_ID" = $X
           """
          .map(rs => (rs.string("USE"), rs.int("OUTPUT_ID")
                     ,rs.int("KIND_ID"), rs.string("KIND")
                     ,rs.int("DATABASE_ID"), rs.string("STORAGE")
                     ,rs.int("SCALIKE_DRIVER_ID"), rs.string("DRIVER"), rs.boolean("PLAIN_SQL")))
          .list()()
          .headOption
          .map { case (it, ot_id, ki_id, kind, db_id, storage, sd_id, driver, plainSQL) =>
            DbUse(it
                 ,DbOutputUse(
                   DbOutput(
                     DbKind(kind
                           ,db_id.?
                           ,ki_id.?
                     )
                     ,ki_id
                     ,ot_id.?
                   )
                   ,Some(
                     DbDatabaseOutputUse(
                       DbDatabaseOutput(
                         DbDatabaseKind(
                           DbDatabase(storage
                                     ,None
                                     ,None
                                     ,Some(
                                       DbScalikeDriver(driver
                                                      ,plainSQL
                                                      ,sd_id.?
                                       )
                                     )
                                     ,sd_id.?
                                     ,db_id.?
                           )
                           ,db_id
                           ,ki_id.?
                         )
                         ,ot_id.?
                       )
                       ,id.?
                     )
                   )
                   ,id.?
                 )
                 ,ot_id
                 ,id.?
            )
          }

    assert(use.isDefined)

    use.get
  }

  //////////////////////////////////////////////////////////////////////////////

  def mapSquares(implicit
    boardId: Long,
    session: DBSession
  ): Seq[DbSquare] =
    sql"""select b."SIZE", s."ROW_NO", s."COLUMN", s."IS_FREE"
          from "SQUARES" s
          join "BOARDS" b on s."BOARD_ID" = b."ID"
          where b."ID" = $boardId
       """
      .map(rs => (rs.int("SIZE"), rs.int("ROW_NO"), rs.int("COLUMN"), rs.boolean("IS_FREE")))
      .list()()
      .map { case (size, row, col, isFree) =>
        DbSquare(DbBoard(size
                        ,boardId.?
                )
                ,boardId
                ,row
                ,col
                ,isFree
        )
      }


  def mapQueens(
    `nests?`: Option[Iterable[Nest]] = None,
    `rounds?`: Option[Iterable[Long]] = None
  )(implicit
    boardId: Long,
    tag: String,
    session: DBSession
  ): Seq[DbQueen] = {

    implicit val filter: Option[Nest => Option[Nest]] = `nests?`.map { ns => { it => ns.find(it.==) } }
    import axis.fifth.output.database.scalikejdbc.dao.DAO.Implicits.Queens._

    (

      `nests?` match {

        case Some(values) => `rounds?` match {

          case Some(numbers) =>

            sql"""select q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                  from "ROUNDS" r
                  join "BOARDS" b on r."BOARD_ID" = b."ID"
                  join "QUEENS" q on r."QUEEN_ID" = q."ID"
                  join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                  join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                  join "MODELS" ml on q."MODEL_ID" = ml."ID"
                  join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                  join "USES" ue on q."USE_ID" = ue."ID"
                  where b."ID" = $boardId and r."TAG" = $tag
                  and r."NUMBER" in (${numbers.mkString(", ")})
                  and at."ASPECT" in (${values.map(_.aspect).mkString(", ")})
                  and am."ALGORITHM" in (${values.map(_.algorithm).mkString(", ")})
                  and ml."MODEL" in (${values.map(_.model).mkString(", ")})
               """

          case _ =>

            sql"""select q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                  from "ROUNDS" r
                  join "BOARDS" b on r."BOARD_ID" = b."ID"
                  join "QUEENS" q on r."QUEEN_ID" = q."ID"
                  join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                  join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                  join "MODELS" ml on q."MODEL_ID" = ml."ID"
                  join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                  join "USES" ue on q."USE_ID" = ue."ID"
                  where b."ID" = $boardId and r."TAG" = $tag
                  and at."ASPECT" in (${values.map(_.aspect).mkString(", ")})
                  and am."ALGORITHM" in (${values.map(_.algorithm).mkString(", ")})
                  and ml."MODEL" in (${values.map(_.model).mkString(", ")})
               """

        }

        case _ => `rounds?` match {

          case Some(numbers) =>

            sql"""select q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                  from "ROUNDS" r
                  join "BOARDS" b on r."BOARD_ID" = b."ID"
                  join "QUEENS" q on r."QUEEN_ID" = q."ID"
                  join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                  join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                  join "MODELS" ml on q."MODEL_ID" = ml."ID"
                  join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                  join "USES" ue on q."USE_ID" = ue."ID"
                  where b."ID" = $boardId and r."TAG" = $tag
                  and r."NUMBER" in (${numbers.mkString(", ")})
               """

          case _ =>

            sql"""select q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                  from "ROUNDS" r
                  join "BOARDS" b on r."BOARD_ID" = b."ID"
                  join "QUEENS" q on r."QUEEN_ID" = q."ID"
                  join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                  join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                  join "MODELS" ml on q."MODEL_ID" = ml."ID"
                  join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                  join "USES" ue on q."USE_ID" = ue."ID"
                  where b."ID" = $boardId and r."TAG" = $tag
               """

        }

      }

  )
    .map(rs => (rs.int(1), rs.string(2), rs.int(3), rs.int(4), rs.int(5), rs.int(6), rs.int(7)))
    .list()()
    .map { case (id, tags, at_id, am_id, ml_id, ie_id, ue_id) =>
      DbQueen(mapAspect(at_id), at_id
             ,mapAlgorithm(am_id), am_id
             ,mapModel(ml_id), ml_id
             ,mapInterface(ie_id), ie_id
             ,mapUse(ue_id), ue_id
             ,tags
             ,id.?
      )
    }
      .matching

  }


  def mapRounds(
    `rounds?`: Option[Iterable[Long]] = None
  )(implicit
    boardId: Long,
    tag: String,
    session: DBSession
  ): Seq[DbRound] = (

    `rounds?` match {

      case Some(numbers) =>

        sql"""select b."SIZE", r."ID", r."NUMBER", q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
              from "ROUNDS" r
              join "BOARDS" b on r."BOARD_ID" = b."ID"
              join "QUEENS" q on r."QUEEN_ID" = q."ID"
              join "ASPECTS" at on q."ASPECT_ID" = at."ID"
              join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
              join "MODELS" ml on q."MODEL_ID" = ml."ID"
              join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
              join "USES" ue on q."USE_ID" = ue."ID"
              where b."ID" = $boardId and r."TAG" = $tag
              and r."NUMBER" in (${numbers.mkString(", ")})
           """

      case _ =>

        sql"""select b."SIZE", r."ID", r."NUMBER", q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
              from "ROUNDS" r
              join "BOARDS" b on r."BOARD_ID" = b."ID"
              join "QUEENS" q on r."QUEEN_ID" = q."ID"
              join "ASPECTS" at on q."ASPECT_ID" = at."ID"
              join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
              join "MODELS" ml on q."MODEL_ID" = ml."ID"
              join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
              join "USES" ue on q."USE_ID" = ue."ID"
              where b."ID" = $boardId and r."TAG" = $tag
           """

    }

  )
    .map(rs => ((rs.int(1), rs.int(2), rs.int(3))
               ,(rs.int(4), rs.string(5), rs.int(6), rs.int(7), rs.int(8), rs.int(9), rs.int(10))))
    .list()()
    .map { case ((size, id, number), (qid, tags, at_id, am_id, ml_id, ie_id, ue_id)) =>
      val b = DbBoard(size, boardId.?)
      val q = DbQueen(mapAspect(at_id), at_id
                     ,mapAlgorithm(am_id), am_id
                     ,mapModel(ml_id), ml_id
                     ,mapInterface(ie_id), ie_id
                     ,mapUse(ue_id), ue_id
                     ,tags
                     ,qid.?
              )
      DbRound(b, boardId
             ,q, qid
             ,tag
             ,number
             ,id.?
      )
    }


  def mapSolutions(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    session: DBSession
  ): Map[DbSolution, Seq[(DbSquare, Option[DbSolutionPoint])]] = {
    implicit val r = MutableMap[DbSolution, MutableList[DbSolutionPoint]]()
    `mapSolutionPoints*`(`nests?`, `rounds?`)
    mapSquares
  }

  private def `mapSolutionPoints*`(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    r: MutableMap[DbSolution, MutableList[DbSolutionPoint]],
    session: DBSession
  ): Unit = {

    implicit val filter: Option[Nest => Option[Nest]] = `nests?`.map { ns => { it => ns.find(it.==) } }
    import axis.fifth.output.database.scalikejdbc.dao.DAO.Implicits.Solutions._

    (

      `nests?` match {

        case Some(values) => `rounds?` match {

          case Some(numbers) =>

          sql"""select b."SIZE", r."ID", r."NUMBER", s."ID", s."NUMBER", p."ROW_NO", p."COLUMN", q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                from "ROUNDS" r
                join "BOARDS" b on r."BOARD_ID" = b."ID"
                join "QUEENS" q on r."QUEEN_ID" = q."ID"
                join "SOLUTIONS" s on s."ROUND_ID" = r."ID"
                join "SOLUTION_POINTS" sp on sp."SOLUTION_ID" = s."ID"
                join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                join "MODELS" ml on q."MODEL_ID" = ml."ID"
                join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                join "USES" ue on q."USE_ID" = ue."ID"
                where b."ID" = $boardId and r."TAG" = $tag
                and r."NUMBER" in (${numbers.mkString(", ")})
                and at."ASPECT" in (${values.map(_.aspect).mkString(", ")})
                and am."ALGORITHM" in (${values.map(_.algorithm).mkString(", ")})
                and ml."MODEL" in (${values.map(_.model).mkString(", ")})
             """

          case _ =>

          sql"""select b."SIZE", r."ID", r."NUMBER", s."ID", s."NUMBER", p."ROW_NO", p."COLUMN", q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                from "ROUNDS" r
                join "BOARDS" b on r."BOARD_ID" = b."ID"
                join "QUEENS" q on r."QUEEN_ID" = q."ID"
                join "SOLUTIONS" s on s."ROUND_ID" = r."ID"
                join "SOLUTION_POINTS" sp on sp."SOLUTION_ID" = s."ID"
                join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                join "MODELS" ml on q."MODEL_ID" = ml."ID"
                join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                join "USES" ue on q."USE_ID" = ue."ID"
                where b."ID" = $boardId and r."TAG" = $tag
                and at."ASPECT" in (${values.map(_.aspect).mkString(", ")})
                and am."ALGORITHM" in (${values.map(_.algorithm).mkString(", ")})
                and ml."MODEL" in (${values.map(_.model).mkString(", ")})
             """

        }

        case _ => `rounds?` match {

          case Some(numbers) =>

          sql"""select b."SIZE", r."ID", r."NUMBER", s."ID", s."NUMBER", p."ROW_NO", p."COLUMN", q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                from "ROUNDS" r
                join "BOARDS" b on r."BOARD_ID" = b."ID"
                join "QUEENS" q on r."QUEEN_ID" = q."ID"
                join "SOLUTIONS" s on s."ROUND_ID" = r."ID"
                join "SOLUTION_POINTS" sp on sp."SOLUTION_ID" = s."ID"
                join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                join "MODELS" ml on q."MODEL_ID" = ml."ID"
                join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                join "USES" ue on q."USE_ID" = ue."ID"
                where b."ID" = $boardId and r."TAG" = $tag
                and r."NUMBER" in (${numbers.mkString(", ")})
             """

          case _ =>

          sql"""select b."SIZE", r."ID", r."NUMBER", s."ID", s."NUMBER", sp."ROW_NO", sp."COLUMN", q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                from "ROUNDS" r
                join "BOARDS" b on r."BOARD_ID" = b."ID"
                join "QUEENS" q on r."QUEEN_ID" = q."ID"
                join "SOLUTIONS" s on s."ROUND_ID" = r."ID"
                join "SOLUTION_POINTS" sp on sp."SOLUTION_ID" = s."ID"
                join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                join "MODELS" ml on q."MODEL_ID" = ml."ID"
                join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                join "USES" ue on q."USE_ID" = ue."ID"
                where b."ID" = $boardId and r."TAG" = $tag
             """

        }

      }

    )
      .map(rs => ((rs.int(1), rs.int(2), rs.int(3))
                 ,(rs.int(4), rs.int(5))
                 ,(rs.int(6), rs.int(7))
                 ,(rs.int(8), rs.string(9), rs.int(10), rs.int(11), rs.int(12), rs.int(13), rs.int(14))))
      .list()()
      .map { case ((size, rid, rno), (id, number), (row, col), (qid, tags, at_id, am_id, ml_id, ie_id, ue_id)) =>
        val b = DbBoard(size, boardId.?)
        val q = DbQueen(mapAspect(at_id), at_id
                       ,mapAlgorithm(am_id), am_id
                       ,mapModel(ml_id), ml_id
                       ,mapInterface(ie_id), ie_id
                       ,mapUse(ue_id), ue_id
                       ,tags
                       ,qid.?
                )
        val r = DbRound(b, boardId
                       ,q, qid
                       ,tag
                       ,number
                       ,rid.?
                )

        DbSolutionPoint(
          DbSolution(r, rid
                    ,number
                    ,id.?
          )
          ,id
          ,col
          ,row
        )
    }
      .matching
      .appending

  }

  //////////////////////////////////////////////////////////////////////////////

  def deleteSquare(
    it: DbSquare
  )(implicit
    session: DBSession
  ): Unit = {
    val boardId = it.board_db.id
    sql"""delete from "SQUARES"
          where "BOARD_ID" = $boardId
          and "ROW_NO" = ${it.row_db}
          and "COLUMN" = ${it.col_db}
       """.update()()
  }

  def deleteQueen(
    it: DbQueen
  )(implicit
    session: DBSession
  ): Unit = {
    sql"""delete from "QUEENS" where "ID" = ${it.id}""".update()()
  }

  def deleteRound(
    it: DbRound
  )(implicit
    session: DBSession
  ): Unit = {
    sql"""delete from "ROUNDS" where "ID" = ${it.id}""".update()()
  }

  def deleteSolution(
    it: DbSolution
  )(implicit
    session: DBSession
  ): Unit = {
    sql"""delete from "SOLUTION_POINTS" where "SOLUTION_ID" = ${it.id}""".update()()
    sql"""delete from "SOLUTIONS" where "ID" = ${it.id}""".update()()
  }

}
