package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package dao

import scala.collection.Map

import _root_.scalikejdbc._

import dimensions.fifth.output.database.scalikejdbc.dao.ScalikejdbcDatabaseDAO


class `Impl.Scalikejdbc Query Solutions'dao`(
  _id: Long,
  _tag: String
)(implicit
  _dao: ScalikejdbcDatabaseDAO
) extends `Base.Scalikejdbc Query *'dao`(_id, _tag)
    with `Scalikejdbc Query Solutions'dao`


import axis.fifth.output.database.Mappings.{ DbSquare, DbSolution, DbSolutionPoint }

import base.Board

import version.less.nest.Nest

abstract trait `Scalikejdbc Query Solutions'dao`
    extends `Scalikejdbc Query *'dao` {

  @inline def mapSolutions(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  ): Map[DbSolution, Seq[(DbSquare, Option[DbSolutionPoint])]] =
    NamedDB(dao.driver) localTx { implicit session =>
      dao.mapSolutions(`nests?`, `rounds?`)
    }

}
