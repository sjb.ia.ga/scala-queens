package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package dao
package delete

import _root_.scalikejdbc._

import axis.fifth.output.database.Mappings._


abstract trait `Scalikejdbc delete Query *'dao`[
  E <: DbEntity
] extends `Scalikejdbc Query *'dao` {

  def delete(e: E): Boolean = {
    NamedDB(dao.driver) localTx { implicit session =>
      e match {
        case it: DbSquare => dao.deleteSquare(it)
        case it: DbRound => dao.deleteRound(it)
        case it: DbQueen => dao.deleteQueen(it)
        case it: DbSolution => dao.deleteSolution(it)
        case _ => ???
      }
    }
    true
  }

}
