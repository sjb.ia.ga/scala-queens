package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package dao
package delete

import _root_.scalikejdbc._

import axis.fifth.output.database.Mappings.DbSquare

import dimensions.fifth.output.database.scalikejdbc.dao.ScalikejdbcDatabaseDAO


class `Impl.Scalikejdbc delete Query Squares'dao`(
  _id: Long
)(implicit
  _dao: ScalikejdbcDatabaseDAO
) extends `Impl.Scalikejdbc Query Squares'dao`(_id, null)
    with `Scalikejdbc delete Query *'dao`[DbSquare]
