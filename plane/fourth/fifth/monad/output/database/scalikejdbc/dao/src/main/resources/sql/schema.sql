create table LOOKS(
  ID int generated always as identity,
  LOOK varchar,
  primary key (ID)
);
             
create table CLASSICS(
  ID int generated always as identity,
  LOOK_ID int,
  primary key (ID),
  constraint LOOK_FK foreign key (LOOK_ID) references LOOKS(ID)
);
             
create table NEXTS(
  ID int generated always as identity,
  NEXT varchar,
  primary key (ID)
);
             
create table STEPPERS(
  ID int generated always as identity,
  NEXT_ID int,
  primary key (ID),
  constraint NEXT_FK foreign key (NEXT_ID) references NEXTS(ID)
);
             
create table ASPECTS(
  ID int generated always as identity,
  ASPECT varchar,
  CLASSIC_ID int,
  STEPPER_ID int,
  primary key (ID),
  constraint CLASSIC_FK foreign key (CLASSIC_ID) references CLASSICS(ID),
  constraint STEPPER_FK foreign key (STEPPER_ID) references STEPPERS(ID)
);
             
