package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package dao
package delete

import _root_.scalikejdbc._

import axis.fifth.output.database.Mappings.DbSolution

import dimensions.fifth.output.database.scalikejdbc.dao.ScalikejdbcDatabaseDAO


class `Impl.Scalikejdbc delete Query Solutions'dao`(
  _id: Long
)(implicit
  _dao: ScalikejdbcDatabaseDAO
) extends `Impl.Scalikejdbc Query Solutions'dao`(_id, null)
    with `Scalikejdbc delete Query *'dao`[DbSolution]
