package queens
package dimensions.fifth
package output.database
package scalikejdbc

import _root_.scalikejdbc._

import queens.base.Board
import dimensions.third.Queens

import dao.ScalikejdbcDatabaseDAO
import plainsql.PlainSQLScalikejdbcDatabaseCallbacks


final class ScalikejdbcDatabaseCallbacks(implicit
  override protected val dao: ScalikejdbcDatabaseDAO
) extends PlainSQLScalikejdbcDatabaseCallbacks {

  private val SQL6 = new axis.fifth.output.database.scalikejdbc.dao.SQL(dao.driver)
  private val DAO6 = new axis.fifth.output.database.scalikejdbc.dao.DAO(dao.SQL5, SQL6, dao.DAO5)

  override def apply(board: Board): Long = {
    if (dao.plainSQL)
      return super.apply(board)

    NamedDB(dao.driver) localTx { implicit session =>
      DAO6.addBoard(board)
    }
  }


  import dimensions.Dimension.Use.{ output$ }
  import dimensions.Dimension.Use.Output.{ database$ }
  import dimensions.Dimension.Use.Output.Database.{ scalike$ }
  import dimensions.Dimension.Interface.monadProgram
  import _root_.queens.Implicits._

  override def apply(queens: Queens[?]): Long = {
    if (dao.plainSQL)
      return super.apply(queens)

    val aspect = queens.aspect.toString()
    val algorithm = queens.algorithm.toString()
    val model = queens.model.toString()
    val interface = monadProgram.toString()
    val use = output$(database$(scalike$(dao.scalikeDriver.toString, false)))

    val tags: List[String] = queens.toString()

    NamedDB(dao.driver) localTx { implicit session =>
      DAO6.addQueens(aspect
                    ,algorithm
                    ,model
                    ,interface
                    ,use
                    ,tags mkString " "
      )
    }
  }


  override def apply(boardId: Long, tag: String, queenId: Long, roundNo: Option[Long] = None): Long = {
    if (dao.plainSQL)
      return super.apply(boardId, tag, queenId, roundNo)

    roundNo.map { no => this.roundNo.set(no - 1) }

    NamedDB(dao.driver) localTx { implicit session =>
      DAO6.addRound(boardId, queenId, tag, this.roundNo.get)
    }
  }


  override def apply(roundId: Long, solution: Solution, number: Long): Boolean = {
    if (dao.plainSQL) {
      return super.apply(roundId, solution, number)
    }

    NamedDB(dao.driver) localTx { implicit session =>
      DAO6.addSolution(roundId, solution, number)
    }

    true
  }

}


object ScalikejdbcDatabaseCallbacks {

  object Implicits {

    implicit def dcb2dao(it: ScalikejdbcDatabaseCallbacks): ScalikejdbcDatabaseDAO =
      it.dao

  }

}
