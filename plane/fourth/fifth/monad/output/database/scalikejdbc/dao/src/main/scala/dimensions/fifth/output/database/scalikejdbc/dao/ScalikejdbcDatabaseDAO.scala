package queens
package dimensions.fifth
package output.database
package scalikejdbc
package dao

import scala.collection.Map

import _root_.scalikejdbc._
import _root_.scalikejdbc.config._

import axis.fifth.output.database.dbdimensions.Mappings.{
  DbAspect,
  DbAlgorithm,
  DbModel,
  DbInterface,
  DbUse
}

import axis.fifth.output.database.Mappings._

import dao.plainsql.PlainSQLScalikejdbcDatabaseDAO

import dimensions.Dimension.Use.Output.Database.Scalike.Driver

import version.less.nest.Nest


final class ScalikejdbcDatabaseDAO(
  _driver: String,
  override val scalikeDriver: Driver,
  val plainSQL: Boolean = false
) extends PlainSQLScalikejdbcDatabaseDAO {

  override val driver: Symbol = Symbol(_driver)

  if (_driver.toLowerCase == "sqlite")
    GlobalSettings.jtaDataSourceCompatible = true

  DBs.setup(driver)

  private[scalikejdbc] val SQL5 = new axis.fifth.output.database.scalikejdbc.dao.dbdimensions.SQL(driver)
  private[scalikejdbc] val DAO5 = new axis.fifth.output.database.scalikejdbc.dao.dbdimensions.DAO(SQL5)
  private[scalikejdbc] val SQL6 = new axis.fifth.output.database.scalikejdbc.dao.SQL(driver)
  private[scalikejdbc] val DAO6 = new axis.fifth.output.database.scalikejdbc.dao.DAO(SQL5, SQL6, DAO5)


  override def toString(): String = if (plainSQL) "PlainSQL" else ""


  override def mapAspect(
    id: Long
  )(implicit
    session: DBSession
  ): DbAspect =
    if (plainSQL)
      super.mapAspect(id)
    else
      DAO5.Aspects.mapAspect(id)

  override def mapAlgorithm(
    id: Long
  )(implicit
    session: DBSession
  ): DbAlgorithm =
    if (plainSQL)
      super.mapAlgorithm(id)
    else
      DAO5.Algorithms.mapAlgorithm(id)

  override def mapModel(
    id: Long
  )(implicit
    session: DBSession
  ): DbModel =
    if (plainSQL)
      super.mapModel(id)
    else
      DAO5.Models.mapModel(id)

  override def mapInterface(
    id: Long
  )(implicit
    session: DBSession
  ): DbInterface =
    if (plainSQL)
      super.mapInterface(id)
    else
    DAO5.Interfaces.mapInterface(id)

  override def mapUse(
    id: Long
  )(implicit
    session: DBSession
  ): DbUse =
    if (plainSQL)
      super.mapUse(id)
    else
      DAO5.Uses.mapUse(id)

  //////////////////////////////////////////////////////////////////////////////

  override def mapSquares(implicit
    boardId: Long,
    session: DBSession
  ): Seq[DbSquare] =
    if (plainSQL)
      super.mapSquares
    else
      DAO6.mapSquares

  override def mapQueens(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    session: DBSession
  ): Seq[DbQueen] =
    if (plainSQL)
      super.mapQueens(`nests?`, `rounds?`)
    else
      DAO6.mapQueens(`nests?`, `rounds?`)

  override def mapRounds(
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    session: DBSession
  ): Seq[DbRound] =
    if (plainSQL)
      super.mapRounds(`rounds?`)
    else
      DAO6.mapRounds(`rounds?`)

  override def mapSolutions(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    session: DBSession
  ): Map[DbSolution, Seq[(DbSquare, Option[DbSolutionPoint])]] =
    if (plainSQL)
      super.mapSolutions(`nests?`, `rounds?`)
    else
      DAO6.mapSolutions(`nests?`, `rounds?`)

  //////////////////////////////////////////////////////////////////////////////

  override def deleteSquare(
    it: DbSquare
  )(implicit
    session: DBSession
  ): Unit =
    if (plainSQL)
      super.deleteSquare(it)
    else
      DAO6.deleteSquare(it)

  override def deleteQueen(
    it: DbQueen
  )(implicit
    session: DBSession
  ): Unit =
    if (plainSQL)
      super.deleteQueen(it)
    else
      DAO6.deleteQueen(it)

  override def deleteRound(
    it: DbRound
  )(implicit
    session: DBSession
  ): Unit =
    if (plainSQL)
      super.deleteRound(it)
    else
      DAO6.deleteRound(it)

  override def deleteSolution(
    it: DbSolution
  )(implicit
    session: DBSession
  ): Unit =
    if (plainSQL)
      super.deleteSolution(it)
    else
      DAO6.deleteSolution(it)

}
