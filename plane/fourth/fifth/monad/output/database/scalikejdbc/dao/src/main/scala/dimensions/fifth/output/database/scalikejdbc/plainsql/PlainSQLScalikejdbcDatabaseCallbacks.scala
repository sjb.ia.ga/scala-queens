package queens
package dimensions.fifth
package output.database
package scalikejdbc
package plainsql

import java.util.concurrent.atomic.AtomicLong

import _root_.scalikejdbc._

import common.geom.Implicits.`Coord*`

import queens.base.Board
import dimensions.third.Queens

import dao.plainsql.PlainSQLScalikejdbcDatabaseDAO


abstract class PlainSQLScalikejdbcDatabaseCallbacks {

  protected val dao: PlainSQLScalikejdbcDatabaseDAO

  implicit protected def atl2long(it: AtomicLong): Long =
    it.addAndGet(1)

  def apply(board: Board): Long = {

    NamedDB(dao.driver) localTx { implicit session =>

      val boardId =
        sql"""insert into "BOARDS"("SIZE")
              values (${board.N})
           """.updateAndReturnGeneratedKey()()
              .toLong

      for {
        row <- 0 until board.N
        col <- 0 until board.N
      } {
        sql"""insert into "SQUARES"("BOARD_ID", "ROW_NO", "COLUMN", "IS_FREE")
              values ($boardId, $row, $col, ${!board(row)(col)})
           """.update()()
      }

      boardId
    }

  }


  import dimensions.Dimension.Use.{ output$ }
  import dimensions.Dimension.Use.Output.{ database$ }
  import dimensions.Dimension.Use.Output.Database.{ scalike$ }
  import dimensions.Dimension.Interface.monadProgram
  import _root_.queens.Implicits._

  def apply(queens: Queens[?]): Long = {
    val aspect = queens.aspect.toString()
    val algorithm = queens.algorithm.toString()
    val model = queens.model.toString()
    val interface = monadProgram.toString()
    val use = output$(database$(scalike$(dao.scalikeDriver.toString, true)))

    val tags: List[String] = queens.toString()

    NamedDB(dao.driver) localTx { implicit session =>

      sql"""select at."ID", am."ID", ml."ID", ie."ID", ue."ID"
            from "ASPECTS" at, "ALGORITHMS" am, "MODELS" ml, "INTERFACES" ie, "USES" ue
            where at."ASPECT" = $aspect
            and am."ALGORITHM" = $algorithm
            and ml."MODEL" = $model
            and ie."INTERFACE" = $interface
            and ue."USE" = $use
         """
        .map(rs => (rs.string(1), rs.string(2), rs.string(3), rs.string(4), rs.string(5)))
        .list()()
        .headOption
        .map { case (aspectId, algorithmId, modelId, interfaceId, useId) =>
          sql"""insert into "QUEENS"("ASPECT_ID", "ALGORITHM_ID", "MODEL_ID", "INTERFACE_ID", "USE_ID", "TAGS")
                values ($aspectId, $algorithmId, $modelId, $interfaceId, $useId, ${tags mkString " "})
             """.updateAndReturnGeneratedKey()()
                .toLong
        }
        .get
    }

  }


  protected val roundNo = new AtomicLong(0)

  def apply(boardId: Long, tag: String, queenId: Long, roundNo: Option[Long] = None): Long = {
    roundNo.map { it => this.roundNo.set(it - 1) }

    NamedDB(dao.driver) localTx { implicit session =>

      val number: Long = this.roundNo

      val roundId =
        sql"""insert into "ROUNDS"("BOARD_ID", "QUEEN_ID", "TAG", "NUMBER")
              values ($boardId, $queenId, $tag, $number)
           """.updateAndReturnGeneratedKey()()
              .toLong

      roundId
    }
  }


  def apply(roundId: Long, solution: Solution, number: Long): Boolean = {

    NamedDB(dao.driver) localTx { implicit session =>

      val solutionId =
        sql"""insert into "SOLUTIONS"("ROUND_ID", "NUMBER")
              values ($roundId, $number)
           """.updateAndReturnGeneratedKey()()

      solution
        .map { it =>
          sql"""insert into "SOLUTION_POINTS"("SOLUTION_ID", "ROW_NO", "COLUMN")
                values ($solutionId, ${it.row}, ${it.col})
             """.update()()
        }
    }

    true
  }

}
