package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package dao

import _root_.scalikejdbc._

import dimensions.fifth.output.database.scalikejdbc.dao.ScalikejdbcDatabaseDAO


class `Impl.Scalikejdbc Query Squares'dao`(
  _id: Long,
  _tag: String
)(implicit
  _dao: ScalikejdbcDatabaseDAO
) extends `Base.Scalikejdbc Query *'dao`(_id, _tag)
    with `Scalikejdbc Query Squares'dao`


import axis.fifth.output.database.Mappings.DbSquare

abstract trait `Scalikejdbc Query Squares'dao`
    extends `Scalikejdbc Query *'dao` {

  @inline def mapSquares(): Seq[DbSquare] =
    NamedDB(dao.driver) localTx { implicit session =>
      dao.mapSquares
    }

}
