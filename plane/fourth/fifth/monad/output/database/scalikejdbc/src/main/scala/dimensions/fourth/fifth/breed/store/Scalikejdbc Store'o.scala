package queens
package dimensions.fourth.fifth
package breed
package store

import java.util.UUID

import conf.mdo.scalikejdbc.Conf

import mdo.QueensMonadScalikejdbcDatabaseOutput


abstract trait `Scalikejdbc Store'o`[
  P <: Conf
](using
  override protected val * : P
) extends `Store'o`[P]
    with QueensMonadScalikejdbcDatabaseOutput[P]:

  override protected type * <: `Scalikejdbc Store'o`[P]


/**
  * @see [[queens.plane.fourth.fifth.third.program.monad.output.database.scalikejdbc.flow.Main]] @{code val spawn = `Scalikejdbc Store'o`(it.model)}
  */
object `Scalikejdbc Store'o`
    extends Spawner:

  import java.util.concurrent.{ ConcurrentHashMap => Map }

  import dimensions.Dimension.Model
  import Model.{ Flow, Parallel }
  import Parallel.{ Actors, Futures }

  import common.Flag

  import common.pipeline.Context
  import dimensions.fifth.QueensUseSolution

  override def apply[
    P <: conf.emito.Conf
  ](model: Model
  )(using
    params: P
  ): Spawn[P] = new Spawn[P] {

    override def apply(next: Option[Context => QueensUseSolution]): * =

      new * :

        override protected def `apply*`(queens: *.Q,
                                        tag: Any,
                                        its: Seq[UUID],
                                        dryrun: Either[Simulate, Boolean]
        ): *.T =
          assert(model eq queens.model)

          given Map[UUID, (Boolean, Flag)] = new Map()

          ((model, params) match

            case (Flow, p: conf.mdo.scalikejdbc.flow.Parameters) =>

              new flow.`Impl.Scalikejdbc Store'o by Flow`(using p)(queens, tag, its, dryrun)(next)

            case _ => ???

          ).asInstanceOf[*.T]

    }
