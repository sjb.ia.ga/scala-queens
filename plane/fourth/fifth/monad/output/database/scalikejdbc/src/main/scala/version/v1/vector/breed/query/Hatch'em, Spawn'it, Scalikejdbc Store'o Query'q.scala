package queens
package version.v1
package vector
package breed
package query

import java.util.UUID
import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.Flag

import common.monad.Item

import conf.mdo.scalikejdbc.Conf

import _root_.scalikejdbc

import dimensions.fourth.fifth.breed.query.scalikejdbc.dao.{
  `Impl.Scalikejdbc Query Rounds'dao`,
  `Impl.Scalikejdbc Query Squares'dao`,
  `Impl.Scalikejdbc Query Queens'dao`,
  `Impl.Scalikejdbc Query Solutions'dao`
}
import dimensions.fourth.fifth.breed.query.scalikejdbc.dao.delete.{
  `Impl.Scalikejdbc delete Query Rounds'dao`,
  `Impl.Scalikejdbc delete Query Squares'dao`,
  `Impl.Scalikejdbc delete Query Queens'dao`,
  `Impl.Scalikejdbc delete Query Solutions'dao`
}

import dimensions.fourth.fifth.breed.query.scalikejdbc.callback.flow.{
  `Impl.Scalikejdbc callback Query Rounds'q by Flow`,
  `Scalikejdbc callback Query Rounds'q by Flow`,
  `Impl.Scalikejdbc callback Query Squares'q by Flow`,
  `Scalikejdbc callback Query Squares'q by Flow`,
  `Impl.Scalikejdbc callback Query Queens'q by Flow`,
  `Scalikejdbc callback Query Queens'q by Flow`,
  `Impl.Scalikejdbc callback Query Solutions'q by Flow`,
  `Scalikejdbc callback Query Solutions'q by Flow`
}

import dimensions.fourth.fifth.breed.query.scalikejdbc.delete.flow.{
  `Impl.Scalikejdbc delete Query Rounds'q by Flow`,
  `Scalikejdbc delete Query Rounds'q by Flow`,
  `Impl.Scalikejdbc delete Query Squares'q by Flow`,
  `Scalikejdbc delete Query Squares'q by Flow`,
  `Impl.Scalikejdbc delete Query Queens'q by Flow`,
  `Scalikejdbc delete Query Queens'q by Flow`,
  `Impl.Scalikejdbc delete Query Solutions'q by Flow`,
  `Scalikejdbc delete Query Solutions'q by Flow`
}

import dimensions.fourth.fifth.breed.query.scalikejdbc.println.flow.{
  `Impl.Scalikejdbc println Query Rounds'q by Flow`,
  `Scalikejdbc println Query Rounds'q by Flow`,
  `Impl.Scalikejdbc println Query Squares'q by Flow`,
  `Scalikejdbc println Query Squares'q by Flow`,
  `Impl.Scalikejdbc println Query Queens'q by Flow`,
  `Scalikejdbc println Query Queens'q by Flow`,
  `Impl.Scalikejdbc println Query Solutions'q by Flow`,
  `Scalikejdbc println Query Solutions'q by Flow`
}

import dimensions.fifth.output.database.scalikejdbc.ScalikejdbcDatabaseCallbacks

import version.less.nest.Nest


final class `Hatch'em, Spawn'it, Scalikejdbc Store'o Query'q`[
  P <: Conf
](
  _id: Long,
  _em: Iterable[Hive[P]],
  _tag: Any,
  _its: Seq[UUID] = UUID.randomUUID :: Nil
)(using
  override protected val * : P
)(
  override protected val fs: Item[*.T] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)] = new Map()
)(using
  _d: ScalikejdbcDatabaseCallbacks
) extends
    `Base.Hatch'em, Spawn'it, Output'o`[P](_em, _tag, _its),
    `Hatch'em, Spawn'it, Store'o Query'q`[P],
    `Base.Hatch'em, Spawn'it, Scalikejdbc Store'o`[P](_id):

  override protected type * = `Hatch'em, Spawn'it, Scalikejdbc Store'o Query'q`[P]

  override def `#`(t: Any): * =
    new `Hatch'em, Spawn'it, Scalikejdbc Store'o Query'q`[P](_id, _em, t, _its)(fs*)

  override protected def `apply°`(s: *.Item => Boolean*): * =
    new `Hatch'em, Spawn'it, Scalikejdbc Store'o Query'q`[P](_id, _em, _tag, _its)(s*)

  import `Hatch'em, Spawn'it, Scalikejdbc Store'o Query'q`.given


  class `flow'q`(params: conf.mdo.scalikejdbc.flow.Parameters)
      extends `By Flow Query`:

    override protected type `sq.P` = conf.query.scalikejdbc.flow.squares.Parameters

    implicit override protected val `sq.P`: `sq.P` = {
      import conf.query.scalikejdbc.flow.squares.Parameters.given
      params
    }

    override protected type `r.P` = conf.query.scalikejdbc.flow.rounds.Parameters

    implicit override protected val `r.P` = {
      import conf.query.scalikejdbc.flow.rounds.Parameters.given
      params
    }

    override protected type `q.P` = conf.query.scalikejdbc.flow.queens.Parameters

    implicit override protected val `q.P` = {
      import conf.query.scalikejdbc.flow.queens.Parameters.given
      params
    }

    override protected type `s.cb.P` = conf.query.scalikejdbc.flow.solutions.callback.Parameters

    implicit override protected val `s.cb.P` = {
      import conf.query.scalikejdbc.flow.solutions.callback.Parameters.given
      params
    }

    override object `callback'q`
        extends `By Flow callback Query`:

      override def squares(
        callback: `sq.P`.T => Boolean
      ): `Scalikejdbc callback Query Squares'q by Flow` =
        new `Impl.Scalikejdbc callback Query Squares'q by Flow`(
          callback.asInstanceOf[Any => Boolean],
          new `Impl.Scalikejdbc Query Squares'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def rounds(
        callback: `r.P`.T => Boolean,
        `rounds?`: Option[Iterable[Long]] = None
      ): `Scalikejdbc callback Query Rounds'q by Flow` =
        new `Impl.Scalikejdbc callback Query Rounds'q by Flow`(
          callback.asInstanceOf[Any => Boolean],
          `rounds?`,
          new `Impl.Scalikejdbc Query Rounds'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def queens(
        callback: `q.P`.T => Boolean,
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `Scalikejdbc callback Query Queens'q by Flow` =
        new `Impl.Scalikejdbc callback Query Queens'q by Flow`(
          callback.asInstanceOf[Any => Boolean],
          `nests?`, `rounds?`,
          new `Impl.Scalikejdbc Query Queens'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def solutions(
        callback: `s.cb.P`.T => Boolean,
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `Scalikejdbc callback Query Solutions'q by Flow` =
        new `Impl.Scalikejdbc callback Query Solutions'q by Flow`(
          callback.asInstanceOf[Any => Boolean],
          `nests?`, `rounds?`,
          new `Impl.Scalikejdbc Query Solutions'dao`(_id, _tag.toString)
        )(
          _tag
        )()


    override protected type `s.del.P` = conf.query.scalikejdbc.flow.solutions.delete.Parameters

    implicit override protected val `s.del.P` = {
      import conf.query.scalikejdbc.flow.solutions.delete.Parameters.given
      params
    }

    override object `delete'q`
        extends `By Flow delete Query`:

      override def squares(
      ): `Scalikejdbc delete Query Squares'q by Flow` =
        new `Impl.Scalikejdbc delete Query Squares'q by Flow`(
          new `Impl.Scalikejdbc delete Query Squares'dao`(_id)
        )(
          _tag
        )()

      override def rounds(
        `rounds?`: Option[Iterable[Long]] = None
      ): `Scalikejdbc delete Query Rounds'q by Flow` =
        new `Impl.Scalikejdbc delete Query Rounds'q by Flow`(
          `rounds?`,
          new `Impl.Scalikejdbc delete Query Rounds'dao`(_id)
        )(
          _tag
        )()

      override def queens(
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `Scalikejdbc delete Query Queens'q by Flow` =
        new `Impl.Scalikejdbc delete Query Queens'q by Flow`(
          `nests?`, `rounds?`,
          new `Impl.Scalikejdbc delete Query Queens'dao`(_id)
        )(
          _tag
        )()

      override def solutions(
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `Scalikejdbc delete Query Solutions'q by Flow` =
        new `Impl.Scalikejdbc delete Query Solutions'q by Flow`(
          `nests?`, `rounds?`,
          new `Impl.Scalikejdbc delete Query Solutions'dao`(_id)
        )(
          _tag
        )()


    override protected type `s.co.P` = conf.query.scalikejdbc.flow.solutions.println.Parameters

    implicit override protected val `s.co.P` = {
      import conf.query.scalikejdbc.flow.solutions.println.Parameters.given
      params
    }

    override object `console'o'q`
        extends `By Flow println Query`:

      override def squares(
      ): `Scalikejdbc println Query Squares'q by Flow` =
        new `Impl.Scalikejdbc println Query Squares'q by Flow`(
          new `Impl.Scalikejdbc Query Squares'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def rounds(
        `rounds?`: Option[Iterable[Long]] = None
      ): `Scalikejdbc println Query Rounds'q by Flow` =
        new `Impl.Scalikejdbc println Query Rounds'q by Flow`(
          `rounds?`,
          new `Impl.Scalikejdbc Query Rounds'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def queens(
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `Scalikejdbc println Query Queens'q by Flow` =
        new `Impl.Scalikejdbc println Query Queens'q by Flow`(
          `nests?`, `rounds?`,
          new `Impl.Scalikejdbc Query Queens'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def solutions(
        `nests?`: Option[Iterable[Nest]],
        `rounds?`: Option[Iterable[Long]]
      ): `Scalikejdbc println Query Solutions'q by Flow` =
        new `Impl.Scalikejdbc println Query Solutions'q by Flow`(
          `nests?`, `rounds?`,
          new `Impl.Scalikejdbc Query Solutions'dao`(_id, _tag.toString)
        )(
          _tag
        )()


  override def `by Flow`: `By Flow Query` = * match

    case params: conf.mdo.scalikejdbc.flow.Parameters => `flow'q`(params)

    case _ => ???


object `Hatch'em, Spawn'it, Scalikejdbc Store'o Query'q`:

  import dimensions.fifth.output.database.scalikejdbc.ScalikejdbcDatabaseCallbacks
  import dimensions.fifth.output.database.scalikejdbc.dao.ScalikejdbcDatabaseDAO
  import dimensions.fifth.output.database.scalikejdbc.ScalikejdbcDatabaseCallbacks.Implicits.dcb2dao

  given dao(using dcb: ScalikejdbcDatabaseCallbacks): ScalikejdbcDatabaseDAO = dcb2dao(dcb)

  import common.NoTag

  def apply[
    P <:  Conf
  ](
    em: Iterable[Hive[P]]
  )(using
    params: P,
    dbCallbacks: ScalikejdbcDatabaseCallbacks
  ): `Hatch'em, Spawn'it, Store'o Query'q`[P] =
    val board = params.board
    new `Hatch'em, Spawn'it, Scalikejdbc Store'o Query'q`[P](dbCallbacks(board), em, NoTag)(using params)()
