package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc

import _root_.scalikejdbc._

import dao.`Scalikejdbc Query Queens'dao`

import conf.query.scalikejdbc.{ Conf => Scalike }

import conf.query.queens.Conf


abstract trait `Scalikejdbc Query Queens'q`[
  P <: Conf & Scalike,
  DAO <: `Scalikejdbc Query Queens'dao`
](using
  override protected val * : P
) extends `Query Queens'q`[P]
    with `Scalikejdbc Query *'q`[P, DAO] { this: flow.`Scalikejdbc Query *'q by Flow`[P, DAO] =>

  override protected type * <: `Scalikejdbc Query Queens'q`[P, DAO]

  override protected def `map*`[R](
    block: ((QueryItem, Int)) => Option[R]
  ): Seq[Option[R]] = dao
    .mapQueens(nests, rounds)
    .zipWithIndex
    .map(block(_))

}
