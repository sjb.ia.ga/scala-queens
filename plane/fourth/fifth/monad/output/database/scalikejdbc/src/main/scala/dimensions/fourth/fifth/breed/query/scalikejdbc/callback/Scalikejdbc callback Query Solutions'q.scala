package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package callback

import dao.`Impl.Scalikejdbc Query Solutions'dao`

import conf.query.scalikejdbc.{ Conf => Scalike }

import conf.query.solutions.callback.Conf


abstract trait `Scalikejdbc callback Query Solutions'q`[
  P <: Conf & Scalike
](using
  override protected val * : P
) extends query.callback.`callback Query Solutions'q`[P]
    with `Scalikejdbc Query Solutions'q`[P, `Impl.Scalikejdbc Query Solutions'dao`] { this: scalikejdbc.flow.`Scalikejdbc Query *'q by Flow`[P, `Impl.Scalikejdbc Query Solutions'dao`] =>

  override protected type * <: `Scalikejdbc callback Query Solutions'q`[P]

}
