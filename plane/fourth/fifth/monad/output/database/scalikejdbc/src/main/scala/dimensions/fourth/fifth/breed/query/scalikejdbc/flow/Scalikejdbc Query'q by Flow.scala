package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package flow

import conf.query.scalikejdbc.Conf


abstract trait `Scalikejdbc Query *'q by Flow`[
  P <: Conf,
  DAO <: dao.`Scalikejdbc Query *'dao`
](using
  override protected val * : P
) extends query.flow.`Query *'q by Flow`[P]
    with `Scalikejdbc Query *'q`[P, DAO]:

  override protected type * <: `Scalikejdbc Query *'q by Flow`[P, DAO]
