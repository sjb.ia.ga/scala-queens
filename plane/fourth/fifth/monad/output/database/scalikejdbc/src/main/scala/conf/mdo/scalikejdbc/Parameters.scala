package queens
package conf.mdo.scalikejdbc

import java.util.concurrent.atomic.AtomicLong

import base.Board


abstract trait Conf
    extends conf.mdo.Conf:

  import dimensions.fourth.fifth.breed.store.`Scalikejdbc Store'o`

  override type P <: Conf

  override type T <: `Scalikejdbc Store'o`[P]

  private val no = new AtomicLong(0)

  def `Scalikejdbc Store'o Round`: Option[Long] =
    Some(no.incrementAndGet())


package flow:

  case class Parameters(override val board: Board)
      extends conf.mdo.scalikejdbc.Conf
      with conf.mdo.flow.Conf
      with dimensions.fifth.output.console.conf.Conf:

    import dimensions.fourth.fifth.breed.store.`Scalikejdbc Store'o`

    override type P = this.type

    override type T =`Scalikejdbc Store'o`[P]


  object Parameters:

    def apply(block: Parameters => Unit = { _ => })
             (using board: Board): Parameters =
      val params = new Parameters(board)
      block(params)
      params
