package queens
package dimensions.fourth.fifth
package breed
package store
package flow

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import dimensions.third.Queens

import conf.mdo.scalikejdbc.flow.{ Parameters => P }

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution

import common.Flag
import common.monad.Item


class `Impl.Scalikejdbc Store'o by Flow`(using
  override protected val * : P
)(
  _qs: Queens[?],
  _tag: Any,
  _its: Seq[UUID],
  _run: Either[Simulate, Boolean],
  _fs: Item[Solution] => Boolean*
)(
  _next: Option[Context => QueensUseSolution]
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends breed.flow.`Base.Output *'o by Flow`[P](_qs, _tag, _its, _run, _fs*)(_next)
    with `Scalikejdbc Store'o`[P]:

  override protected type * = `Impl.Scalikejdbc Store'o by Flow`

  override def `#`(_t: Any): * = this

  override protected def `apply*`(s: Item[Solution] => Boolean*): * =
    new `Impl.Scalikejdbc Store'o by Flow`(_qs, _tag, _its, _run, s*)(_next)
