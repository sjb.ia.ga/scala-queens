package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package delete

import _root_.scalikejdbc._

import dao.delete.`Scalikejdbc delete Query *'dao`

import axis.fifth.output.database.Mappings.DbEntity

import conf.query.scalikejdbc.Conf


abstract trait `Scalikejdbc delete Query *'q`[
  E <: DbEntity,
  P <: Conf,
  DAO <: `Scalikejdbc delete Query *'dao`[E]
] extends query.delete.`delete Query *'q`[P]
    with `Scalikejdbc Query *'q`[P, DAO]:

  override protected type * <: `Scalikejdbc delete Query *'q`[E, P, DAO]

  override protected val >> = query.delete.`delete Query *'q`[E](dao.delete)
