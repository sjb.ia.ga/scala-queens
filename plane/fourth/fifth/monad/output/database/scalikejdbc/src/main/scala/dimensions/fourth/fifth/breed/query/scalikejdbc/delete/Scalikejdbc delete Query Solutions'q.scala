package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package delete

import _root_.scalikejdbc._

import dao.delete.`Impl.Scalikejdbc delete Query Solutions'dao`

import conf.query.scalikejdbc.{ Conf => Scalike }

import conf.query.solutions.delete.Conf


abstract trait `Scalikejdbc delete Query Solutions'q`[
  P <: Conf & Scalike
](using
  override protected val * : P
) extends query.delete.`delete Query Solutions'q`[P]
    with `Scalikejdbc Query *'q`[P, `Impl.Scalikejdbc delete Query Solutions'dao`] { this: scalikejdbc.flow.`Scalikejdbc Query *'q by Flow`[P, `Impl.Scalikejdbc delete Query Solutions'dao`] =>

  override protected type * <: `Scalikejdbc delete Query Solutions'q`[P]

  import axis.fifth.output.database.Mappings.DbSolution

  import `Query Solutions'db`.given

  override protected val >> = query.delete.`delete Query *'q`[DbSolution](dao.delete)

}
