package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package delete
package flow

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import _root_.scalikejdbc._

import conf.query.scalikejdbc.flow.squares.{ Parameters => P }

import common.Flag
import common.monad.Item


final class `Impl.Scalikejdbc delete Query Squares'q by Flow`(using
  override protected val * : P
)(
  _dao: dao.delete.`Impl.Scalikejdbc delete Query Squares'dao`
)(
  override protected val tag: Any
)(
  override protected val fs: Item[*.T] => Boolean*
)(using
  override protected val iterations: Seq[UUID] = Nil,
  override protected val `iterations*`: Map[UUID, (Boolean, Flag)] = new Map()
) extends
    `Base.Scalikejdbc Query *'q`[P, dao.delete.`Impl.Scalikejdbc delete Query Squares'dao`](_dao),
    `Scalikejdbc delete Query Squares'q by Flow`:

  override protected type * = `Impl.Scalikejdbc delete Query Squares'q by Flow`

  override protected def `apply°`(s: *.Item => Boolean*): * =
    new `Impl.Scalikejdbc delete Query Squares'q by Flow`(_dao)(tag)(s*)


abstract trait `Scalikejdbc delete Query Squares'q by Flow`(using
  override protected val * : P
) extends scalikejdbc.delete.`Scalikejdbc delete Query Squares'q`[P]
    with scalikejdbc.flow.`Scalikejdbc Query *'q by Flow`[P, dao.delete.`Impl.Scalikejdbc delete Query Squares'dao`]
    with `Scalikejdbc Query Squares'q`[P, dao.delete.`Impl.Scalikejdbc delete Query Squares'dao`]:

  override protected type * <: `Scalikejdbc delete Query Squares'q by Flow`
