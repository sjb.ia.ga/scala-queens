package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc

import _root_.scalikejdbc._

import dao.`Scalikejdbc Query Solutions'dao`

import conf.query.scalikejdbc.{ Conf => Scalike }

import conf.query.solutions.Conf


abstract trait `Scalikejdbc Query Solutions'q`[
  P <: Conf & Scalike,
  DAO <: `Scalikejdbc Query Solutions'dao`
](using
  override protected val * : P
) extends `Query Solutions'q`[P]
    with `Scalikejdbc Query *'q`[P, DAO] { this: flow.`Scalikejdbc Query *'q by Flow`[P, DAO] =>

  override protected type * <: `Scalikejdbc Query Solutions'q`[P, DAO]

  override protected def `map*`[R](
    block: ((QueryItem, Int)) => Option[R]
  ): Seq[Option[R]] = dao
    .mapSolutions(nests, rounds)
    .values
    .toSeq
    .zipWithIndex
    .map(block(_))

}
