package queens
package dimensions.fourth.fifth
package breed
package store
package mdo

import conf.mdo.scalikejdbc.Conf

import dimensions.fourth.fifth.breed.mdo.QueensMonadDatabaseOutput
import dimensions.fifth.output.database.scalikejdbc.QueensScalikejdbcDatabaseOutput


abstract trait QueensMonadScalikejdbcDatabaseOutput[
  P <: Conf
] extends QueensMonadDatabaseOutput[P]
    with QueensScalikejdbcDatabaseOutput
