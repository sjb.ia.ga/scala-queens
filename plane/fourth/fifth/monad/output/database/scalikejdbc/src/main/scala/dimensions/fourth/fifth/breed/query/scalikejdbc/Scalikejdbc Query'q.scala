package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc

import _root_.scalikejdbc._

import dao.`Scalikejdbc Query *'dao`

import conf.query.scalikejdbc.Conf


abstract trait `Base.Scalikejdbc Query *'q`[
  P <: Conf,
  DAO <: `Scalikejdbc Query *'dao`
](using
  override protected val * : P
)(
  override protected val dao: DAO
) extends `Scalikejdbc Query *'q`[P, DAO]:

  override protected type * <: `Base.Scalikejdbc Query *'q`[P, DAO]


abstract trait `Scalikejdbc Query *'q`[
  P <: Conf,
  DAO <: `Scalikejdbc Query *'dao`
](using
  override protected val * : P
) extends `Query *'q`[P]:

  override protected type * <: `Scalikejdbc Query *'q`[P, DAO]

  protected val dao: DAO

  override def toString(): String =  dao.toString() + " Scalikejdbc " + super.toString
