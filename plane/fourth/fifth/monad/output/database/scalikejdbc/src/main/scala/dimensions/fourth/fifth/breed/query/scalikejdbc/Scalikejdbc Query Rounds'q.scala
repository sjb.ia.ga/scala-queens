package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc

import _root_.scalikejdbc._

import dao.`Scalikejdbc Query Rounds'dao`

import conf.query.scalikejdbc.{ Conf => Scalike }

import conf.query.rounds.Conf


abstract trait `Scalikejdbc Query Rounds'q`[
  P <: Conf & Scalike,
  DAO <: `Scalikejdbc Query Rounds'dao`
](using
  override protected val * : P
) extends `Query Rounds'q`[P]
    with `Scalikejdbc Query *'q`[P, DAO] { this: flow.`Scalikejdbc Query *'q by Flow`[P, DAO] =>

  override protected type * <: `Scalikejdbc Query Rounds'q`[P, DAO]

  override protected def `map*`[R](
    block: ((QueryItem, Int)) => Option[R]
  ): Seq[Option[R]] = dao
    .mapRounds(rounds)
    .zipWithIndex
    .map(block(_))

}
