package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package println

import dao.`Impl.Scalikejdbc Query Solutions'dao`

import conf.query.scalikejdbc.{ Conf => Scalike }

import conf.query.solutions.println.Conf


abstract trait `Scalikejdbc println Query Solutions'q`[
  P <: Conf & Scalike
](using
  override protected val * : P
) extends query.println.`println Query Solutions'q`[P]
    with `Scalikejdbc Query Solutions'q`[P, `Impl.Scalikejdbc Query Solutions'dao`] { this: scalikejdbc.flow.`Scalikejdbc Query *'q by Flow`[P, `Impl.Scalikejdbc Query Solutions'dao`] =>

  override protected type * <: `Scalikejdbc println Query Solutions'q`[P]

}
