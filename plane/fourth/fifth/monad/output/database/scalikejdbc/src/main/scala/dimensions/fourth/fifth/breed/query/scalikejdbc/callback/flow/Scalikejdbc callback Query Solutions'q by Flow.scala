package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package callback
package flow

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import _root_.scalikejdbc._

import base.Board

import conf.query.scalikejdbc.flow.solutions.callback.{ Parameters => P }

import common.Flag
import common.monad.Item

import version.less.nest.Nest


final class `Impl.Scalikejdbc callback Query Solutions'q by Flow`(using
  override protected val * : P
)(
  _cb: Any => Boolean,
  _ns: Option[Iterable[Nest]],
  _rs: Option[Iterable[Long]],
  _dao: dao.`Impl.Scalikejdbc Query Solutions'dao`
)(
  override protected val tag: Any
)(
  override protected val fs: Item[*.T] => Boolean*
)(using
  override protected val iterations: Seq[UUID] = Nil,
  override protected val `iterations*`: Map[UUID, (Boolean, Flag)] = new Map()
) extends
    `Base.Query Solutions'q`[P](_ns, _rs),
    query.callback.`Base.callback Query *'q`[P](_cb),
    `Base.Scalikejdbc Query *'q`[P, dao.`Impl.Scalikejdbc Query Solutions'dao`](_dao),
    `Scalikejdbc callback Query Solutions'q by Flow`:

  override protected type * = `Impl.Scalikejdbc callback Query Solutions'q by Flow`

  override protected def `apply°`(s: *.Item => Boolean*): * =
    new `Impl.Scalikejdbc callback Query Solutions'q by Flow`(_cb, _ns, _rs, _dao)(tag)(s*)


abstract trait `Scalikejdbc callback Query Solutions'q by Flow`(using
  override protected val * : P
) extends scalikejdbc.callback.`Scalikejdbc callback Query Solutions'q`[P]
    with scalikejdbc.flow.`Scalikejdbc Query *'q by Flow`[P, dao.`Impl.Scalikejdbc Query Solutions'dao`]
    with `Scalikejdbc Query Solutions'q`[P, dao.`Impl.Scalikejdbc Query Solutions'dao`]:

  override protected type * <: `Scalikejdbc callback Query Solutions'q by Flow`
