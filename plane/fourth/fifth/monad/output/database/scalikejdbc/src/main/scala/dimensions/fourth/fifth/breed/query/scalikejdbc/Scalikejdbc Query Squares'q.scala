package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc

import _root_.scalikejdbc._

import dao.`Scalikejdbc Query Squares'dao`

import conf.query.scalikejdbc.{ Conf => Scalike }

import conf.query.squares.Conf


abstract trait `Scalikejdbc Query Squares'q`[
  P <: Conf & Scalike,
  DAO <: `Scalikejdbc Query Squares'dao`
](using
  override protected val * : P
) extends `Query Squares'q`[P]
    with `Scalikejdbc Query *'q`[P, DAO] { this: flow.`Scalikejdbc Query *'q by Flow`[P, DAO] =>

  override protected type * <: `Scalikejdbc Query Squares'q`[P, DAO]

  override protected def `map*`[R](
    block: ((QueryItem, Int)) => Option[R]
  ): Seq[Option[R]] = dao
    .mapSquares()
    .zipWithIndex
    .map(block(_))

}
