package queens
package version.v1
package vector
package breed

import common.monad.Item

import conf.mdo.scalikejdbc.Conf

import dimensions.fifth.output.database.scalikejdbc.ScalikejdbcDatabaseCallbacks


abstract trait `Base.Hatch'em, Spawn'it, Scalikejdbc Store'o`[
  P <: Conf
](
  protected val boardId: Long
)(using
  override protected val * : P
)(using
  protected val dbCallbacks: ScalikejdbcDatabaseCallbacks
) extends `Hatch'em, Spawn'it, Store'o`[P]:

  override protected type * <: `Base.Hatch'em, Spawn'it, Scalikejdbc Store'o`[P]

  override protected def apply[R](
    it: *.Item,
    block: *.Item => R
  ): R =
    var queenId = 0L
    it.the { queens =>
      queenId = dbCallbacks(queens)
      false
    }
    val roundId = dbCallbacks(queenId
                             ,tag.toString
                             ,queenId
                             ,*.`Scalikejdbc Store'o Round`)
    it(dbCallbacks, roundId)
    block(it)

  override def toString(): String = "Scalikejdbc " + super.toString
