package queens
package dimensions.fifth
package output.database
package scalikejdbc


abstract trait QueensScalikejdbcDatabaseOutput
    extends QueensDatabaseOutput:

  protected var dbCallbacks: ScalikejdbcDatabaseCallbacks = _
  protected var roundId: Long = _

  def apply(dbCallbacks: ScalikejdbcDatabaseCallbacks, roundId: Long): Unit =
    this.dbCallbacks = dbCallbacks
    this.roundId = roundId

  final override protected val callback =
    { (solution: Solution, number: Long) => dbCallbacks(roundId, solution, number) }

  override def toString(): String = "Scalikejdbc " + super.toString
