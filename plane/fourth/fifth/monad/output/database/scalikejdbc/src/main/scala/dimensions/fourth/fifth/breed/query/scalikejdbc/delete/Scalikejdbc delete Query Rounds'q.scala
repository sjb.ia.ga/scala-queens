package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package delete

import _root_.scalikejdbc._

import dao.delete.`Impl.Scalikejdbc delete Query Rounds'dao`

import axis.fifth.output.database.Mappings.DbRound

import conf.query.scalikejdbc.{ Conf => Scalike }

import conf.query.rounds.Conf


abstract trait `Scalikejdbc delete Query Rounds'q`[
  P <: Conf & Scalike
](using
  override protected val * : P
) extends `Scalikejdbc delete Query *'q`[DbRound, P, `Impl.Scalikejdbc delete Query Rounds'dao`]:

  override protected type * <: `Scalikejdbc delete Query Rounds'q`[P]
