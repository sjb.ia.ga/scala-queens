package queens
package conf.query.scalikejdbc.flow

import scala.Function.const

import base.Board


abstract trait Conf
    extends conf.query.flow.Conf
    with conf.query.scalikejdbc.Conf
    with dimensions.fifth.output.console.conf.Conf


package squares:

  case class Parameters()
      extends Conf
      with conf.query.squares.flow.Conf


  object Parameters:

    import conf.mdo.scalikejdbc.flow.{ Parameters => O }
    import conf.query.scalikejdbc.flow.squares.{ Parameters => Q }

    given Conversion[O, Q] = const(Parameters())


package rounds:

  case class Parameters()
      extends Conf
      with conf.query.rounds.flow.Conf


  object Parameters:

    import conf.mdo.scalikejdbc.flow.{ Parameters => O }
    import conf.query.scalikejdbc.flow.rounds.{ Parameters => Q }

    given Conversion[O, Q] = const(Parameters())


package queens:

  case class Parameters()
      extends Conf
      with conf.query.queens.flow.Conf


  object Parameters:

    import conf.mdo.scalikejdbc.flow.{ Parameters => O }
    import conf.query.scalikejdbc.flow.queens.{ Parameters => Q }

    given Conversion[O, Q] = const(Parameters())


package solutions:

  package callback:

    case class Parameters()
        extends Conf
        with conf.query.solutions.callback.flow.Conf


    object Parameters:

      import conf.mdo.scalikejdbc.flow.{ Parameters => O }
      import conf.query.scalikejdbc.flow.solutions.callback.{ Parameters => Q }

      given Conversion[O, Q] = const(Parameters())

  package delete:

    case class Parameters()
        extends Conf
        with conf.query.solutions.delete.flow.Conf


    object Parameters:

      import conf.mdo.scalikejdbc.flow.{ Parameters => O }
      import conf.query.scalikejdbc.flow.solutions.delete.{ Parameters => Q }

      given Conversion[O, Q] = const(Parameters())

  package println:

    case class Parameters()
        extends Conf
        with conf.query.solutions.println.flow.Conf


    object Parameters:

      import conf.mdo.scalikejdbc.flow.{ Parameters => O }
      import conf.query.scalikejdbc.flow.solutions.println.{ Parameters => Q }

      given Conversion[O, Q] = const(Parameters())
