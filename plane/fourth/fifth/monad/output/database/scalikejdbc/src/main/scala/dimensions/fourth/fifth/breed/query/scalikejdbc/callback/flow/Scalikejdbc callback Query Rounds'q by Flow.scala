package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package callback
package flow

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import _root_.scalikejdbc._

import conf.query.scalikejdbc.flow.rounds.{ Parameters => P }

import common.Flag
import common.monad.Item


final class `Impl.Scalikejdbc callback Query Rounds'q by Flow`(using
  override protected val * : P
)(
  _cb: Any => Boolean,
  _rs: Option[Iterable[Long]],
  _dao: dao.`Impl.Scalikejdbc Query Rounds'dao`
)(
  override protected val tag: Any
)(
  override protected val fs: Item[*.T] => Boolean*
)(using
  override protected val iterations: Seq[UUID] = Nil,
  override protected val `iterations*`: Map[UUID, (Boolean, Flag)] = new Map()
) extends
    `Base.Query Rounds'q`[P](_rs),
    query.callback.`Base.callback Query *'q`[P](_cb),
    `Base.Scalikejdbc Query *'q`[P, dao.`Impl.Scalikejdbc Query Rounds'dao`](_dao),
    `Scalikejdbc callback Query Rounds'q by Flow`:

  override protected type * = `Impl.Scalikejdbc callback Query Rounds'q by Flow`

  override protected def `apply°`(s: *.Item => Boolean*): * =
    new `Impl.Scalikejdbc callback Query Rounds'q by Flow`(_cb, _rs, _dao)(tag)(s*)


abstract trait `Scalikejdbc callback Query Rounds'q by Flow`(using
  override protected val * : P
) extends query.callback.`callback Query *'q`[P]
    with scalikejdbc.flow.`Scalikejdbc Query *'q by Flow`[P, dao.`Impl.Scalikejdbc Query Rounds'dao`]
    with `Scalikejdbc Query Rounds'q`[P, dao.`Impl.Scalikejdbc Query Rounds'dao`]:

  override protected type * <: `Scalikejdbc callback Query Rounds'q by Flow`
