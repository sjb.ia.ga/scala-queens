package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package delete

import _root_.scalikejdbc._

import dao.delete.`Impl.Scalikejdbc delete Query Queens'dao`

import axis.fifth.output.database.Mappings.DbQueen

import conf.query.scalikejdbc.{ Conf => Scalike }

import conf.query.queens.Conf


abstract trait `Scalikejdbc delete Query Queens'q`[
  P <: Conf & Scalike
](using
  override protected val * : P
) extends `Scalikejdbc delete Query *'q`[DbQueen, P, `Impl.Scalikejdbc delete Query Queens'dao`]:

  override protected type * <: `Scalikejdbc delete Query Queens'q`[P]
