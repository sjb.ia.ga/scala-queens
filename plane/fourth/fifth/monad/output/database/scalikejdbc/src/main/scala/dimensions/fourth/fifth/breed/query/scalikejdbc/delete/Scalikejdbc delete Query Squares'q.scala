package queens
package dimensions.fourth.fifth
package breed
package query
package scalikejdbc
package delete

import _root_.scalikejdbc._

import dao.delete.`Impl.Scalikejdbc delete Query Squares'dao`

import axis.fifth.output.database.Mappings.DbSquare

import conf.query.scalikejdbc.{ Conf => Scalike }

import conf.query.squares.Conf


abstract trait `Scalikejdbc delete Query Squares'q`[
  P <: Conf & Scalike
](using
  override protected val * : P
) extends `Scalikejdbc delete Query *'q`[DbSquare, P, `Impl.Scalikejdbc delete Query Squares'dao`]:

  override protected type * <: `Scalikejdbc delete Query Squares'q`[P]
