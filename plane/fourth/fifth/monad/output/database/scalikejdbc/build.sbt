libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",
  ("org.scalikejdbc" %% "scalikejdbc" % "3.5.0").cross(CrossVersion.for3Use2_13),
  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
