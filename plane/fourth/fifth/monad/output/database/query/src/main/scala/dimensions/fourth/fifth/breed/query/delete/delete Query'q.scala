package queens
package dimensions.fourth.fifth
package breed
package query
package delete

import scala.Function.const

import common.pipeline.Context.Empty
import common.pipeline.Metadata
import Metadata.Boot
import Metadata.Database.Delete

import conf.query.Conf


abstract trait `delete Query *'q`[
  P <: Conf
](using
  override protected val * : P
) extends `Query *'q`[P]:

  override protected type * <: `delete Query *'q`[P]

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(_, _, _) => { Some(Delete(Empty, None)) }

  override def toString(): String = "delete " + super.toString


object `delete Query *'q`:

  import axis.fifth.output.database.Mappings.DbEntity
  import common.pipeline.Metadata.Database.Delete

  private[query] def apply[
    E <: DbEntity
  ](delete: E => Boolean)(using to: (Any => E) = (_: Any).asInstanceOf[E])
   (metadata: Metadata): Any => Boolean =
    metadata match
      case Delete(Empty, None) => { it => delete(to(it)) }
      case _ => const(false)
