package queens
package dimensions.fourth.fifth
package breed
package query
package println

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import scala.Function.const

import common.Flag

import conf.query.solutions.println.Conf

import common.pipeline.Context.Empty
import common.pipeline.Metadata
import Metadata.Database.Query


abstract trait `println Query Solutions'q`[
  P <: Conf
](using
  override protected val * : P
) extends `println Query *'q`[P]
    with `Query Solutions'q`[P] { this: query.flow.`Query *'q by Flow`[P] =>

  override protected type * <: `println Query Solutions'q`[P]

  override protected val >> = `println Query Solutions'q`()

  @inline override protected def `to Query *'db`(it: QueryItem): *.T =
    val f: (`Query Queens'to`, Solution) => Map[UUID, (Boolean, Flag)] ?=> ? =
      { (qq, ls) => new println.flow.`Impl.Query Console'o Solutions'db by Flow`(it)(toString)(qq, ls)(tag, iterations)() }
    `Query Solutions'db`(it)(f)

}


object `println Query Solutions'q`:

  private def apply[QCoSDB]()
                           (metadata: Metadata): QCoSDB => Boolean =
    metadata match
      case Query(Empty, None) => _.asInstanceOf[`Query Console'o Solutions'db`].||(Nil)
      case _ => const(false)
