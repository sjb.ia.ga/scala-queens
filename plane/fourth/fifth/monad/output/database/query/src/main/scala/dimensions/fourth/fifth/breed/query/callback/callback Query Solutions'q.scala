package queens
package dimensions.fourth.fifth
package breed
package query
package callback

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.Flag

import conf.query.solutions.callback.Conf


abstract trait `callback Query Solutions'q`[
  P <: Conf
](using
  override protected val * : P
) extends `callback Query *'q`[P]
    with `Query Solutions'q`[P] { this: query.flow.`Query *'q by Flow`[P] =>

  override protected type * <: `callback Query Solutions'q`[P]

  import `Query Solutions'db`.given

  @inline override protected def `to Query *'db`(it: QueryItem): *.T =
    val f: (`Query Queens'to`, Solution) => Map[UUID, (Boolean, Flag)] ?=> ? =
      { (qq, ls) => new flow.`Impl.Query callback Solutions'db by Flow`(it)(toString())(qq, ls)(tag, iterations)() }
    `Query Solutions'db`(it)(f)

}
