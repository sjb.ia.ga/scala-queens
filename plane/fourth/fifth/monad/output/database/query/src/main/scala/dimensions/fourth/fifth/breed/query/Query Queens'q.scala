package queens
package dimensions.fourth.fifth
package breed
package query

import version.less.nest.Nest

import conf.query.queens.Conf

import axis.fifth.output.database.Mappings.DbQueen


abstract trait `Base.Query Queens'q`[
  P <: Conf
] protected (
  override protected val nests: Option[Iterable[Nest]],
  override protected val rounds: Option[Iterable[Long]]
)(using
  override protected val * : P
) extends `Query Queens'q`[P] { this: flow.`Query *'q by Flow`[P] =>

  override protected type * <: `Base.Query Queens'q`[P]

}


abstract trait `Query Queens'q`[
  P <: Conf
](using
  override protected val * : P
) extends `Query *'q`[P] { this: flow.`Query *'q by Flow`[P] =>

  override protected type `-|-` = `Query Queens'q`[P] & (flow.`Query *'q by Flow`[P])

  override protected type * <: `Query Queens'q`[P]

  override protected type QueryItem = DbQueen

  protected val nests: Option[Iterable[Nest]]
  protected val rounds: Option[Iterable[Long]]

  @inline override protected def `to Query *'db`(it: QueryItem): *.T =
    `Query Queens'db`(it)

  override def toString(): String = "Queens " + super.toString

}
