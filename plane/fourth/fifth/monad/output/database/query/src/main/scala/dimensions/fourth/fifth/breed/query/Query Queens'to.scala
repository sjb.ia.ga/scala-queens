package queens
package dimensions.fourth.fifth
package breed
package query

import dimensions.third.Queens

import version.less.nest.Nest


abstract trait `Query Queens'to`(
  nest: Nest,
  override protected val tags: List[String]
) extends Queens[?]:

  final override val aspect = nest.aspect

  final override val algorithm = nest.algorithm

  final override val model = nest.model

  override def toString(): String = tags mkString " "
