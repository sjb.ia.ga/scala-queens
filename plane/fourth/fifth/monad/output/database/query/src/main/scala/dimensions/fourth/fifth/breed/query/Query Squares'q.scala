package queens
package dimensions.fourth.fifth
package breed
package query

import conf.query.squares.Conf

import axis.fifth.output.database.Mappings.DbSquare


abstract trait `Query Squares'q`[
  P <: Conf
](using
  override protected val * : P
) extends `Query *'q`[P] { this: flow.`Query *'q by Flow`[P] =>

  override protected type `-|-` = `Query Squares'q`[P] & (flow.`Query *'q by Flow`[P])

  override protected type * <: `Query Squares'q`[P]

  override protected type QueryItem = DbSquare

  @inline override protected def `to Query *'db`(it: QueryItem): *.T =
    `Query Squares'db`(it)

  override def toString(): String = "Rounds " + super.toString

}
