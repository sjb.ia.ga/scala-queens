package queens
package dimensions.fourth.fifth
package breed
package query

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import base.Board

import common.Flag

import version.less.nest.Nest

import conf.query.solutions.Conf

import axis.fifth.output.database.Mappings.{ DbSquare, DbSolutionPoint }


abstract trait `Base.Query Solutions'q`[
  P <: Conf
] protected (
  override protected val nests: Option[Iterable[Nest]],
  override protected val rounds: Option[Iterable[Long]]
)(using
  override protected val * : P
) extends `Query Solutions'q`[P] { this: flow.`Query *'q by Flow`[P] =>

  override protected type * <: `Base.Query Solutions'q`[P]

}


abstract trait `Query Solutions'q`[
  P <: Conf
](using
  override protected val * : P
) extends `Query *'q`[P] { this: flow.`Query *'q by Flow`[P] =>

  override protected type `-|-` = `Query Solutions'q`[P] & (flow.`Query *'q by Flow`[P])

  override protected type * <: `Query Solutions'q`[P]

  override protected type QueryItem = Seq[(DbSquare, Option[DbSolutionPoint])]

  protected val nests: Option[Iterable[Nest]]
  protected val rounds: Option[Iterable[Long]]

  @inline override protected def `to Query *'db`(it: QueryItem): *.T =
    val f: (`Query Queens'to`, Solution) => Map[UUID, (Boolean, Flag)] ?=> ? =
      { (qq, ls) => new flow.`Impl.Query Solutions'db by Flow`(it)(qq, ls)(tag, iterations)() }
    `Query Solutions'db`(it)(f)

  override def toString(): String = "Solutions " + super.toString

}
