package queens
package dimensions.fourth.fifth
package breed
package query
package callback

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.Flag
import common.monad.Item

import axis.fifth.output.database.Mappings.{ DbSquare, DbSolutionPoint }


abstract class `Query callback Solutions'db`(
  it: Seq[(DbSquare, Option[DbSolutionPoint])]
)(
  header: String
)(
  _qq: `Query Queens'to`,
  _it: Solution
)(
  _tag: Any,
  _its: Seq[UUID]
)(
  _fs: Item[DbSolutionPoint] => Boolean*
)(using
  override protected val `iterations*`: Map[UUID, (Boolean, Flag)]
) extends `Query Solutions'db`(it)(_qq, _it)(_tag, _its)(_fs*):

  override def toString(): String = header + " :: " + super.toString
