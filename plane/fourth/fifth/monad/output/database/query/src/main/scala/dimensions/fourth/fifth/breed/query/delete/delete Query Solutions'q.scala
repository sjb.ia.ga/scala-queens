package queens
package dimensions.fourth.fifth
package breed
package query
package delete

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.Flag

import conf.query.solutions.delete.Conf


abstract trait `delete Query Solutions'q`[
  P <: Conf
](using
  override protected val * : P
) extends `delete Query *'q`[P]
    with `Query Solutions'q`[P] { this: query.flow.`Query *'q by Flow`[P] =>

  override protected type * <: `delete Query Solutions'q`[P]

}
