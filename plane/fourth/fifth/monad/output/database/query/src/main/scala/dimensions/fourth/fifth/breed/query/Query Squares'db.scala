package queens
package dimensions.fourth.fifth
package breed
package query

import axis.fifth.output.database.Mappings.{ DbBoard, DbSquare }

import axis.fifth.output.database.Implicits._

import common.geom.x


class `Query Squares'db` (
  _dbb: DbBoard,
  row: Int,
  col: Int,
  isFree: Boolean
) extends DbSquare(_dbb, _dbb.id, row, col, isFree)
    with `Query Squares'to`(row x col)


object `Query Squares'db`:

  def apply(it: DbSquare) =
    new `Query Squares'db`(it.board_db, it.row_db, it.col_db, it.isFree_db)
