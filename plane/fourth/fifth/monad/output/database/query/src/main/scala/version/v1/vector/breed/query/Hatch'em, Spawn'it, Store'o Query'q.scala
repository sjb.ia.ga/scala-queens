package queens
package version.v1
package vector
package breed
package query

import conf.mdo.Conf

import dimensions.fourth.fifth.breed.query.flow.`Query *'q by Flow`
import dimensions.fourth.fifth.breed.query.callback.`callback Query *'q`
import dimensions.fourth.fifth.breed.query.delete.`delete Query *'q`
import dimensions.fourth.fifth.breed.query.println.`println Query *'q`

import version.less.nest.Nest


abstract trait `Hatch'em, Spawn'it, Store'o Query'q`[
  P <: Conf
](using
  override protected val * : P
) extends `Hatch'em, Spawn'it, Store'o`[P]:

  override protected type * <: `Hatch'em, Spawn'it, Store'o Query'q`[P]

  protected abstract trait `By Flow Query`:

    protected type `sq.P` <: conf.query.squares.flow.Conf
    protected type `r.P` <: conf.query.rounds.flow.Conf
    protected type `q.P` <: conf.query.queens.flow.Conf
    protected type `s.cb.P` <: conf.query.solutions.callback.flow.Conf
    protected type `s.del.P` <: conf.query.solutions.delete.flow.Conf
    protected type `s.co.P` <: conf.query.solutions.println.flow.Conf

    protected val `sq.P`: `sq.P`
    protected val `r.P`: `r.P`
    protected val `q.P`: `q.P`
    protected val `s.cb.P`: `s.cb.P`
    protected val `s.del.P`: `s.del.P`
    protected val `s.co.P`: `s.co.P`

    protected abstract trait `By Flow callback Query`:

      def squares(
        callback: `sq.P`.T => Boolean
      ): `callback Query *'q`[`sq.P`] & `Query *'q by Flow`[`sq.P`]

      def rounds(
        callback: `r.P`.T => Boolean,
        `rounds?`: Option[Iterable[Long]] = None
      ): `callback Query *'q`[`r.P`] & `Query *'q by Flow`[`r.P`]

      def queens(
        callback: `q.P`.T => Boolean,
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `callback Query *'q`[`q.P`] & `Query *'q by Flow`[`q.P`]

      def solutions(
        callback: `s.cb.P`.T => Boolean,
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `callback Query *'q`[`s.cb.P`] & `Query *'q by Flow`[`s.cb.P`]


    protected abstract trait `By Flow delete Query`:

      def squares(
      ): `delete Query *'q`[`sq.P`] & `Query *'q by Flow`[`sq.P`]

      def rounds(
        `rounds?`: Option[Iterable[Long]] = None
      ): `delete Query *'q`[`r.P`] & `Query *'q by Flow`[`r.P`]

      def queens(
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `delete Query *'q`[`q.P`] & `Query *'q by Flow`[`q.P`]

      def solutions(
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `delete Query *'q`[`s.del.P`] & `Query *'q by Flow`[`s.del.P`]


    protected abstract trait `By Flow println Query`:

      def squares(
      ): `println Query *'q`[`sq.P`] & `Query *'q by Flow`[`sq.P`]

      def rounds(
        `rounds?`: Option[Iterable[Long]] = None
      ): `println Query *'q`[`r.P`] & `Query *'q by Flow`[`r.P`]

      def queens(
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `println Query *'q`[`q.P`] & `Query *'q by Flow`[`q.P`]

      def solutions(
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `println Query *'q`[`s.co.P`] & `Query *'q by Flow`[`s.co.P`]

    def `callback'q`: `By Flow callback Query`

    def `delete'q`: `By Flow delete Query`

    def `console'o'q`: `By Flow println Query`

  // def `By Actors Query'q`: `Query by Actors`

  // def `By Futures Query'q`: `Query by Futures`

  def `by Flow`: `By Flow Query`

  override def toString(): String = "Query " + super.toString
