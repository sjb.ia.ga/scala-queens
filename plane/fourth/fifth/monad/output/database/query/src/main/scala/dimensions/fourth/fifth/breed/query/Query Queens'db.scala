package queens
package dimensions.fourth.fifth
package breed
package query

import axis.fifth.output.database.Mappings.DbQueen
import axis.fifth.output.database.dbdimensions.Mappings.{
  DbAspect,
  DbAlgorithm,
  DbModel,
  DbInterface,
  DbUse
}
import axis.fifth.output.database.Implicits._

import base.Board

import version.less.nest.Nest

import queens.given_Conversion_String_List


class `Query Queens'db`(
  _id: Long,
  _at: DbAspect,
  _am: DbAlgorithm,
  _ml: DbModel,
  _ie: DbInterface,
  _ue: DbUse,
  _ts: String
)(
  _n: Nest
) extends DbQueen(_at, _at.id
                 ,_am, _am.id
                 ,_ml, _ml.id
                 ,_ie, _ie.id
                 ,_ue, _ue.id
                 ,_ts
                 ,_id.?)
    with `Query Queens'to`(_n, _ts)


object `Query Queens'db`:

  def apply(it: DbQueen) =
    new `Query Queens'db`(
      it.id,
      it.aspect_db,
      it.algorithm_db,
      it.model_db,
      it.interface_db,
      it.use_db,
      it.tags_db
    )(it.nest_mk)
