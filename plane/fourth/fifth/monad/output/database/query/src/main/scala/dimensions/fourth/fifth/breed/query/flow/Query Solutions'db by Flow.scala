package queens
package dimensions.fourth.fifth
package breed
package query
package flow

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import axis.fifth.output.database.Mappings.{ DbSquare, DbSolutionPoint }

import common.monad.Item

import common.Flag
import common.given

import `Query Solutions'db`.given


final class `Impl.Query Solutions'db by Flow`(
  it: Seq[(DbSquare, Option[DbSolutionPoint])]
)(
  _qq: `Query Queens'to`,
  _it: Solution
)(
  _tag: Any,
  _its: Seq[UUID]
)(
  _fs: Item[DbSolutionPoint] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Query Solutions'db`(it)(_qq, _it)(_tag, _its)(_fs*)
    with `Query Solutions'db by Flow`(it):

  override protected type * = `Impl.Query Solutions'db by Flow`

  override def `#`(_t: Any): * = this

  override protected def `apply*`(s: Item[DbSolutionPoint] => Boolean*): * =
    new `Impl.Query Solutions'db by Flow`(it)(_qq, _it)(_tag, _its)(fs*)


abstract trait `Query Solutions'db by Flow`(
  ls: Seq[DbSolutionPoint]
) { this: `Query Solutions'db` =>

  import common.Macros.tryc

  override protected def `for*`[R](block: Item[DbSolutionPoint] => R)(using
                                   preemption: Boolean): Iterable[R] =
    val id = UUID.randomUUID
    val flg = `flag*`(id)

    try
      `iterations*`.put(id, (true, flg))

      val `block*` = `apply*`(block)

      val r = ls
        .zipWithIndex
        .map { (db, n) =>
          if flg
          then
            if preemption then
              block(new Item(db, flg.slot))
            val vi = (null, tag, UUID.randomUUID, n+1L, (), ())
            `block*`(new Item(db, flg.slot, Some(vi)))
            else
              None
        }
      r
    finally
      tryc(`iterations*`.remove(id))


  @inline override def foreach(block: Item[DbSolutionPoint] => Unit)(using
                               preemption: Boolean): Iterable[Unit] =
    `for*`(block)

  @inline override def map[R](block: Item[DbSolutionPoint] => R)(using
                              preemption: Boolean): Iterable[R] =
    `for*`(block)

  @inline override def flatMap[R](block: Item[DbSolutionPoint] => Iterable[R])(using
                                  preemption: Boolean): Iterable[R] =
    `for*`(block).flatten

}
