package queens
package dimensions.fourth.fifth
package breed
package query

import conf.query.rounds.Conf

import axis.fifth.output.database.Mappings.DbRound


abstract trait `Base.Query Rounds'q`[
  P <: Conf
] protected (
  override protected val rounds: Option[Iterable[Long]]
)(using
  override protected val * : P
) extends `Query Rounds'q`[P] { this: flow.`Query *'q by Flow`[P] =>

  override protected type * <: `Base.Query Rounds'q`[P]

}


abstract trait `Query Rounds'q`[
  P <: Conf
](using
  override protected val * : P
) extends `Query *'q`[P] { this: flow.`Query *'q by Flow`[P] =>

  override protected type `-|-` = `Query Rounds'q`[P] & flow.`Query *'q by Flow`[P]

  override protected type * <: `Query Rounds'q`[P]

  override protected type QueryItem = DbRound

  @inline override protected def `to Query *'db`(it: QueryItem): *.T =
    `Query Rounds'db`(it)

  protected val rounds: Option[Iterable[Long]]

  override def toString(): String = "Rounds " + super.toString


}
