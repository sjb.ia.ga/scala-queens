package queens
package dimensions.fourth.fifth
package breed
package query


abstract trait `Query Squares'to`(val point: Point):

  override def toString(): String = point.toString()
