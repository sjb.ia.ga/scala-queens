package queens
package dimensions.fourth.fifth
package breed
package query

import axis.fifth.output.database.Mappings.{ DbBoard, DbQueen, DbRound }
import axis.fifth.output.database.Implicits._


class `Query Rounds'db`(
  _dbb: DbBoard,
  _dbq: DbQueen,
  _tag: String,
  _no: Long,
  _id: Long
) extends DbRound(_dbb, _dbb.id, _dbq, _dbq.id, _tag, _no, _id.?)
    with `Query Rounds'to`(_tag, _no)


object `Query Rounds'db`:

  def apply(it: DbRound) =
    new `Query Rounds'db`(it.board_db, it.queen_db, it.tag_db, it.number_db, it.id)
