package queens
package dimensions.fourth.fifth
package breed
package query


abstract trait `Query Rounds'to`(
  tag: String,
  number: Long
) extends Any:

  override def toString(): String = {
    val t = if tag eq null then "" else s"#$tag."
    s"Round $t$number"
  }
