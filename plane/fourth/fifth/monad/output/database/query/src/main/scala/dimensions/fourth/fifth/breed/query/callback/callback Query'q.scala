package queens
package dimensions.fourth.fifth
package breed
package query
package callback

import scala.Function.const

import common.pipeline.Context.Empty
import common.pipeline.Metadata
import Metadata.Database.Query

import conf.query.Conf


abstract trait `Base.callback Query *'q`[
  P <: Conf
] protected (using
  override protected val * : P
)(
  override protected val callback: Any => Boolean
) extends `callback Query *'q`[P]:

  override protected type * <: `Base.callback Query *'q`[P]


abstract trait `callback Query *'q`[
  P <: Conf
](using
  override protected val * : P
) extends `Query *'q`[P]:

  override protected type * <: `callback Query *'q`[P]

  protected val callback: Any => Boolean

  override protected val >> = `callback Query *'q`(callback)

  override def toString(): String = "callback " + super.toString


object `callback Query *'q`:

  private def apply[QDB](callback: QDB => Boolean)
                        (metadata: Metadata): QDB => Boolean =
    metadata match
      case Query(Empty, None) => callback
      case _ => const(false)
