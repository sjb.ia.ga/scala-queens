package queens
package dimensions.fourth.fifth
package breed
package query
package callback
package flow

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import axis.fifth.output.database.Mappings.{ DbSquare, DbSolutionPoint }

import common.monad.`MonadWithFilter"`

import common.Flag
import common.monad.Item

import `Query Solutions'db`.given


final class `Impl.Query callback Solutions'db by Flow` (
  it: Seq[(DbSquare, Option[DbSolutionPoint])]
)(
  _h: String
)(
  _qq: `Query Queens'to`,
  _it: Solution
)(
  _tag: Any,
  _its: Seq[UUID]
)(
  _fs: Item[DbSolutionPoint] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Query callback Solutions'db`(it)(_h)(_qq, _it)(_tag, _its)(_fs*)
    with query.flow.`Query Solutions'db by Flow`(it):

  override protected type * = `Impl.Query callback Solutions'db by Flow`

  override def `#`(_t: Any): * = this

  override protected def `apply*`(s: Item[DbSolutionPoint] => Boolean*): * =
    new `Impl.Query callback Solutions'db by Flow`(it)(_h)(_qq, _it)(_tag, _its)(fs*)
