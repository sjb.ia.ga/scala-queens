package queens
package dimensions.fourth.fifth
package breed
package query

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import axis.fifth.output.database.Mappings.{ DbSquare, DbSolution, DbSolutionPoint }

import common.monad.{ Item, `MonadWithFilter'` }

import common.Flag

import `Query Solutions'db`.given


abstract class `Query Solutions'db` protected (
  it: Seq[(DbSquare, Option[DbSolutionPoint])]
)(
  _qq: `Query Queens'to`,
  _it: Solution,
  _ns: (Long, Long) = it
)(
  override protected val tag: Any,
  _its: Seq[UUID]
)(
  override protected val fs: Item[DbSolutionPoint] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Query Solutions'to`(_qq, (_ns._1, tag), _it, _ns._2)
    with `MonadWithFilter'`[DbSolutionPoint, Boolean]
    with `Base.Breed *'"`[DbSolutionPoint](_its):

  override protected type * <: `Query Solutions'db`

  protected val solutionPoints: Seq[DbSolutionPoint] = it

  def apply(i: Long) = solutionPoints(i.toInt)

  def foreach(block: Item[DbSolutionPoint] => Unit)
             (using Boolean): Iterable[Unit]

  def map[R](block: Item[DbSolutionPoint] => R)
            (using Boolean): Iterable[R]

  def flatMap[R](block: Item[DbSolutionPoint] => Iterable[R])
                (using Boolean): Iterable[R]

  override def toString(): String = {
    val r = solutionPoints.head.solution_db.round_db
    val t = if r.tag_db eq null then "" else s"@${r.tag_db}"
    val n = r.number_db
    s"Round #$n$t" + " :: " + super.toString
  }


object `Query Solutions'db`:

  import dimensions.Dimension.Model
  import Model.Flow

  def apply[QDB](
    it: Seq[(DbSquare, Option[DbSolutionPoint])]
  )(
    mk: ((`Query Queens'to`, Solution) => Map[UUID, (Boolean, Flag)] ?=> ?)*
  ): QDB =
    import queens.given_Conversion_String_List

    val ls: Seq[DbSolutionPoint] = it

    val q = ls.head.solution_db.round_db.queen_db

    val qq: `Query Queens'to` = new `Query Queens'to`(q.nest_mk, q.tags_db) {}

    given Map[UUID, (Boolean, Flag)] = new Map()

    (Model(q.model_db.model).get match

      case Flow => mk(0)(qq, ls)

      case _ => ???

    ).asInstanceOf[QDB]


  given Conversion[Seq[(DbSquare, Option[DbSolutionPoint])], Seq[DbSolutionPoint]] = { self =>
    import axis.fifth.output.database.Mappings.DbBoard
    val (DbSquare(DbBoard(n, _), _, _, _, _), _) = self.head
    self.take(n).map(_._2.get)
  }

  given Conversion[Seq[DbSolutionPoint], Solution] = { self =>
    import axis.fifth.output.database.Mappings.Implicits._
    val it: Solution = self
    it
  }

  given Conversion[Seq[(DbSquare, Option[DbSolutionPoint])], (Long, Long)] = { self =>
    import axis.fifth.output.database.Mappings.DbRound
    val (_, Some(DbSolutionPoint(DbSolution(DbRound(_, _, _, _, _, rn, _), _, no, _), _, _, _))) = self.head : @unchecked
    rn -> no
  }

  // delete
  given Conversion[Any, DbSolution] = _
    .asInstanceOf[`Query Solutions'db`]
    .solutionPoints
    .head
    .solution_db
