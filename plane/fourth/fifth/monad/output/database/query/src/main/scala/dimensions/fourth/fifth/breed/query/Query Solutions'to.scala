package queens
package dimensions.fourth.fifth
package breed
package query

import java.util.UUID

import scala.reflect.ClassTag


abstract case class `Query Solutions'to` (
  queens: `Query Queens'to`,
  round: (Long, Any),
  solution: Solution,
  number: Long
):

  def apply(p: `Query Queens'to` => Boolean): Boolean =
    p(queens)

  def apply(p: (Solution, Long) => Boolean): Boolean =
    p(solution, number)

  override def toString(): String = {
    val t = if round._2.toString eq null then "" else s"@${round._2}"
    val n = round._1
    s"$queens :: Solution #$n.$number$t"
  }


object `Query Solutions'to`:
  given Conversion[`Query Solutions'to`, `Query Queens'to`] = _.queens
  given Conversion[`Query Solutions'to`, Solution] = _.solution
