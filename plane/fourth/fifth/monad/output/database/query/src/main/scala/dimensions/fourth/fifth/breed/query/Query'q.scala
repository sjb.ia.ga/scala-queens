package queens
package dimensions.fourth.fifth
package breed
package query

import java.lang.Long
import java.util.UUID

import common.{ Pipeline, `Tagged°` }
import common.monad.{ Item, `MonadWithFilter'°` }

import conf.query.Conf

import callback.`callback Query *'q`
import delete.`delete Query *'q`
import println.`println Query *'q`


abstract trait `Query *'q`[
  P <: Conf
](using
  override protected val * : P
) extends `MonadWithFilter'°`[P, Boolean]
    with `Breed *'°`[P]
    with `Tagged°`[P]
    with Pipeline[Any]:

  override protected type * <: `Query *'q`[P]

  override def `#`(t: Any): * = this.asInstanceOf[*]

  def foreach(block: *.Item => Unit)
             (using Boolean): Iterable[Unit]

  def map[R](block: *.Item => R)
            (using Boolean): Iterable[R]

  def flatMap[R](block: *.Item => Iterable[R])
                (using Boolean): Iterable[R]

  import common.pipeline.Context.Empty
  import common.pipeline.Metadata
  import Metadata.Boot
  import Metadata.Database.Query

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(_, _, _) => Some(Query(Empty, None))

  import scala.Function.const
  import common.monad.Item.given

  override def apply[E](it: *.Item)(expr: (UUID, Long) ?=> E): Option[E] =
    `just get*`(it) {
      case false => const(None)
      case _ => {
        case _ if !it => None
        case (_, (_, _, uuid, n, _, _)) if ||(it.the) =>
          val r = expr(using uuid, n)
          r match
            case false => this stop it
            case _ =>
          Some(r)
        case _ =>
          this stop it
          None
      }
    }

  protected type QueryItem

  protected def `to Query *'db`(it: QueryItem): *.T

  protected def `map*`[R](
    block: ((QueryItem, Int)) => Option[R]
  ): Seq[Option[R]]

  override def toString(): String = "Query"
