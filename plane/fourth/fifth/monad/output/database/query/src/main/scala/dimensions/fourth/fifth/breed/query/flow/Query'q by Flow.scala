package queens
package dimensions.fourth.fifth
package breed
package query
package flow

import java.util.UUID

import base.EmptyBoard

import conf.query.Conf

import common.Flag
import common.monad.Item
import common.given

import common.Macros.tryc


abstract trait `Query *'q by Flow`[
  P <: Conf
](using
  override protected val * : P
) extends `Query *'q`[P]:

  override protected type `-|-` <: `Query *'q by Flow`[P]

  override protected type * <: `Query *'q by Flow`[P]

  override protected type T56 = Unit

  override protected def `for*`[R](block: *.Item => R)(using
                                   preemption: Boolean): Iterable[R] =
    val id = UUID.randomUUID
    val flg = `flag*`(id)

    try
      `iterations*`.put(id, (true, flg))

      val `block*` = `apply°`(block)

      val r = `map*` { case (it, n) =>
        if flg
        then
          val db = `to Query *'db`(it)
          if preemption then
            block(new Item(db, flg.slot))
          val vi = (EmptyBoard(0), tag, UUID.randomUUID, n+1L, (), ())
          `block*`(new Item(db, flg.slot, Some(vi)))
        else
          None
      }
      r
    finally
      tryc(`iterations*`.remove(id))



  inline override def foreach(block: *.Item => Unit)(using
                              preemption: Boolean = false): Iterable[Unit] =
    `for*`(block)

  inline override def map[R](block: *.Item => R)(using
                             preemption: Boolean = false): Iterable[R] =
    `for*`(block)

  inline override def flatMap[R](block: *.Item => Iterable[R])(using
                                 preemption: Boolean = false): Iterable[R] =
    `for*`(block).flatten

  override def toString(): String = "Flow " + super.toString
