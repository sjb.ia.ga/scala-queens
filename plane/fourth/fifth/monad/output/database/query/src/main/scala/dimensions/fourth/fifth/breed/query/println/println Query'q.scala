package queens
package dimensions.fourth.fifth
package breed
package query
package println

import scala.Function.const

import conf.query.Conf


abstract trait `println Query *'q`[
  P <: Conf
](using
  override protected val * : P
) extends `Query *'q`[P]:

  override protected type * <: `println Query *'q`[P]

  override protected val >> = `println Query *'q`()

  override def toString(): String = "println " + super.toString


object `println Query *'q`:

  import common.pipeline.Context.Empty
  import common.pipeline.Metadata
  import Metadata.Database.Query

  private def apply[QDB]()
                        (metadata: Metadata): QDB => Boolean =
    metadata match
      case Query(Empty, None) => { it => Console.println(it); true }
      case _ => const(false)
