package queens
package dimensions.fourth.fifth
package breed
package query
package println

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import axis.fifth.output.database.Mappings.{ DbSquare, DbSolutionPoint }

import common.Flag
import common.monad.Item

import `Query Console'o Solutions'db`.given
import `Query Console'o Solutions'db`.`Solutions Console'o`


abstract class `Query Console'o Solutions'db` protected (
  it: Seq[(DbSquare, Option[DbSolutionPoint])]
)(
  header: String
)(
  _qq: `Query Queens'to`,
  _it: Solution
)(
  _tag: Any,
  _its: Seq[UUID]
)(
  _fs: Item[DbSolutionPoint] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Query Solutions'db`(it)(_qq, _it)(_tag, _its)(_fs*)
    with `Solutions Console'o`(_it, (_tag, _qq, it)):

  override val number = solutionPoints.head.solution_db.number_db

  override def toString(): String = {
    val `console'o` = super.toString
    header + " :: " + `console'o`.substring(0, `console'o`.lastIndexOf("::") - 1)
  }

object `Query Console'o Solutions'db`:

  import common.pipeline.Context
  import common.pipeline.Context.Round

  given Conversion[(Any, `Query Queens'to`, Seq[(DbSquare, Option[DbSolutionPoint])]), Round] = { self =>
    import axis.fifth.output.database.Mappings.Implicits._
    Round(self._3.map(_._1), self._1, self._2, self._3.head._2.get.solution_db.number_db)
  }


  import dimensions.fifth.output.console.QueensConsoleOutput

  abstract trait `Solutions Console'o`(
    solution: Solution,
    round: Round
  ) extends QueensConsoleOutput:

    override protected type Q = `Query Queens'to`

    override def ||(_it: Solution, _ctxt: Context, _data: Option[Any]): Boolean =
      super.||(solution, round, Some(UUID.randomUUID))
