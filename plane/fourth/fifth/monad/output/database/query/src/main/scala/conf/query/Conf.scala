package queens
package conf.query


abstract trait Conf
    extends conf.monad.Conf


package flow:

  abstract trait Conf
      extends conf.query.Conf
      with conf.flow.Conf


package squares:

  abstract trait Conf
      extends conf.query.Conf:

    import dimensions.fourth.fifth.breed.query.`Query Squares'db`

    override type T = `Query Squares'db`

  package flow:

    abstract trait Conf
        extends conf.query.flow.Conf
        with squares.Conf


package rounds:

  abstract trait Conf
      extends conf.query.Conf:

    import dimensions.fourth.fifth.breed.query.`Query Rounds'db`

    override type T = `Query Rounds'db`

  package flow:

    abstract trait Conf
        extends conf.query.flow.Conf
        with rounds.Conf


package queens:

  abstract trait Conf
      extends conf.query.Conf:

    import dimensions.fourth.fifth.breed.query.`Query Queens'db`

    override type T = `Query Queens'db`

  package flow:

    abstract trait Conf
        extends conf.query.flow.Conf
        with queens.Conf


package solutions:

  abstract trait Conf
      extends conf.query.Conf:

    import dimensions.fourth.fifth.breed.query.`Query Solutions'db`

    override type T <: `Query Solutions'db`

  package flow:

    abstract trait Conf
        extends conf.query.flow.Conf
        with solutions.Conf

  package callback:

    abstract trait Conf
        extends solutions.Conf:

      import dimensions.fourth.fifth.breed.query.callback.`Query callback Solutions'db`

      override type T = `Query callback Solutions'db`

    package flow:

      abstract trait Conf
          extends solutions.flow.Conf
          with callback.Conf

  package delete:

    abstract trait Conf
        extends solutions.Conf:

      import dimensions.fourth.fifth.breed.query.`Query Solutions'db`

      override type T = `Query Solutions'db`

    package flow:

      abstract trait Conf
          extends solutions.flow.Conf
          with delete.Conf

  package println:

    abstract trait Conf
        extends solutions.Conf:

      import dimensions.fourth.fifth.breed.query.println.`Query Console'o Solutions'db`

      override type T = `Query Console'o Solutions'db`

    package flow:

      abstract trait Conf
          extends solutions.flow.Conf
          with println.Conf
