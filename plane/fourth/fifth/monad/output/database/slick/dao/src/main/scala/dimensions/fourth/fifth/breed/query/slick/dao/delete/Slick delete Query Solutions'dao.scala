package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package delete

import scala.concurrent.{ Await, ExecutionContext }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile


import dimensions.fifth.output.database.slick.dao.plainsql.PlainSQLSlickDatabaseDAO

class `Impl.Slick delete Query Solutions'dao`[
  P <: JdbcProfile,
  DAO <: PlainSQLSlickDatabaseDAO[P]
](
  _id: Long,
  _tag: String
)(implicit
  _dao: DAO,
  _ec: ExecutionContext
) extends `Impl.Slick Query Solutions'dao`[P, DAO](_id, _tag)
    with plainsql.`PlainSQL Slick delete Query Solutions'dao`[P]


import axis.fifth.output.database.Mappings.DbSolution
import axis.fifth.output.database.Implicits._

abstract trait `Slick delete Query Solutions'dao`[P <: JdbcProfile]
    extends `Slick Query Solutions'dao`[P]
    with `Slick delete Query *'dao`[DbSolution, P] {

  import dao.tables.profile.api._
  import dao.tables.{ solutionPoints, solutions }

  protected def `deleteQuery*`(it: DbSolution) = solutionPoints
    .filter(_.solutionID === it.id)

  protected def deleteQuery(it: DbSolution) = solutions
    .filter(_.id === it.id)

  override def delete(it: DbSolution): Boolean = { Await
    .result(db
      .run(
        DBIO.seq(
          `deleteQuery*`(it)
            .delete,
          `deleteQuery`(it)
            .delete
        ).transactionally
      )
      ,Duration.Inf
    )
    true
  }

}
