package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package plainsql

import scala.concurrent.{ ExecutionContext, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile

import axis.fifth.output.database.Mappings.DbSquare


abstract trait `PlainSQL Slick Query Squares'dao`[P <: JdbcProfile]
    extends `Slick Query Squares'dao`[P]
    with `PlainSQL Slick Query *'dao`[P] {

  override def mapSquares(): Seq[DbSquare] =
    if (dao.plainSQL)
      mapSquares_plainSQL()
    else
      super.mapSquares()

  import `PlainSQL Slick Query Squares'dao`.itemQuery
  import `Slick Query Squares'dao`.Implicits._
  import dao.tables.profile.api._

  protected def mapSquares_plainSQL(): Seq[DbSquare] = Await
    .result(db
      .run(itemQuery[P, DAO])
      .sort
      ,Duration.Inf
    )
    .dbSeq(boardId)

}

object `PlainSQL Slick Query Squares'dao` {

  import dimensions.fifth.output.database.slick.dao.SlickDatabaseDAO

  type T = slick.dao.`Slick Query Squares'dao`.T

  def itemQuery[
    P <: JdbcProfile,
    DAO <: SlickDatabaseDAO[P]
  ](implicit
    boardId: Long,
    dao: DAO,
    ec: ExecutionContext
  ) = {

    import dao.tables.profile.api._

    sql"""select b."SIZE", s."ROW_NO", s."COLUMN", s."IS_FREE"
          from "SQUARES" s
          join "BOARDS" b on s."BOARD_ID" = b."ID"
          where b."ID" = $boardId
       """.as[T]

  }

}
