package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package delete

import scala.concurrent.{ Await, ExecutionContext }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile


import dimensions.fifth.output.database.slick.dao.plainsql.PlainSQLSlickDatabaseDAO

class `Impl.Slick delete Query Queens'dao`[
  P <: JdbcProfile,
  DAO <: PlainSQLSlickDatabaseDAO[P]
](
  _id: Long,
  _tag: String
)(implicit
  _dao: DAO,
  _ec: ExecutionContext
) extends `Impl.Slick Query Queens'dao`[P, DAO](_id, _tag)
    with plainsql.`PlainSQL Slick delete Query Queens'dao`[P]


import axis.fifth.output.database.Mappings.DbQueen
import axis.fifth.output.database.Implicits._

abstract trait `Slick delete Query Queens'dao`[P <: JdbcProfile]
    extends `Slick Query Queens'dao`[P]
    with `Slick delete Query *'dao`[DbQueen, P] {

  import dao.tables.profile.api._
  import dao.tables.queens

  protected def deleteQuery(it: DbQueen) = queens
    .filter(_.id === it.id)

  override def delete(it: DbQueen): Boolean = { Await
    .result(db
      .run(
        deleteQuery(it)
          .delete
      )
      ,Duration.Inf
    )
    true
  }

}
