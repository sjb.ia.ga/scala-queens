package object queens {

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Coord[Int]]

  object Implicits {

    implicit def str2tags(it: String): List[String] = it
      .split("[\\[{(<.: ;,>)}\\]]")
      .filterNot(_.isEmpty)
      .map(_.replace('_', ' '))
      .toList

  }

}
