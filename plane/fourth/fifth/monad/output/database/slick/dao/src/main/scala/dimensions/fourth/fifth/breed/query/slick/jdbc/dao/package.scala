package queens
package dimensions.fourth.fifth
package breed
package query
package slick

import _root_.slick.jdbc.JdbcProfile

import dimensions.fifth.output.database.slick.jdbc.dao.JdbcSlickDatabaseDAO


package jdbc {

  package dao {

    package object delete {

      import slick.dao.delete.{
        `Impl.Slick delete Query Rounds'dao`,
        `Impl.Slick delete Query Squares'dao`,
        `Impl.Slick delete Query Queens'dao`,
        `Impl.Slick delete Query Solutions'dao`,
      }

      type `Impl.Jdbc Slick delete Query Squares'dao`[P <: JdbcProfile] = `Impl.Slick delete Query Squares'dao`[P, JdbcSlickDatabaseDAO[P]]
      type `Impl.Jdbc Slick delete Query Rounds'dao`[P <: JdbcProfile] = `Impl.Slick delete Query Rounds'dao`[P, JdbcSlickDatabaseDAO[P]]
      type `Impl.Jdbc Slick delete Query Queens'dao`[P <: JdbcProfile] = `Impl.Slick delete Query Queens'dao`[P, JdbcSlickDatabaseDAO[P]]
      type `Impl.Jdbc Slick delete Query Solutions'dao`[P <: JdbcProfile] = `Impl.Slick delete Query Solutions'dao`[P, JdbcSlickDatabaseDAO[P]]

    }

  }

  package object dao {

    import slick.dao.{
      `Impl.Slick Query Rounds'dao`,
      `Impl.Slick Query Squares'dao`,
      `Impl.Slick Query Queens'dao`,
      `Impl.Slick Query Solutions'dao`
    }

    type `Impl.Jdbc Slick Query Squares'dao`[P <: JdbcProfile] = `Impl.Slick Query Squares'dao`[P, JdbcSlickDatabaseDAO[P]]
    type `Impl.Jdbc Slick Query Rounds'dao`[P <: JdbcProfile] = `Impl.Slick Query Rounds'dao`[P, JdbcSlickDatabaseDAO[P]]
    type `Impl.Jdbc Slick Query Queens'dao`[P <: JdbcProfile] = `Impl.Slick Query Queens'dao`[P, JdbcSlickDatabaseDAO[P]]
    type `Impl.Jdbc Slick Query Solutions'dao`[P <: JdbcProfile] = `Impl.Slick Query Solutions'dao`[P, JdbcSlickDatabaseDAO[P]]

  }

}
