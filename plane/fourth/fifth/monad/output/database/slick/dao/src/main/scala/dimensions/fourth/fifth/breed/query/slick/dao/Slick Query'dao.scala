package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao

import scala.concurrent.ExecutionContext

import _root_.slick.jdbc.JdbcProfile
import _root_.slick.jdbc.JdbcBackend.backend.Database


import dimensions.fifth.output.database.slick.dao.plainsql.PlainSQLSlickDatabaseDAO

class `Impl.Slick Query *'dao`[
  P <: JdbcProfile,
  D <: PlainSQLSlickDatabaseDAO[P]
] protected (
  override protected val boardId: Long,
  override protected val tag: String
)(implicit
  override protected val dao: D,
  override protected val executionContext: ExecutionContext
) extends plainsql.`PlainSQL Slick Query *'dao`[P] {

  override type DAO = D

}


import dimensions.fifth.output.database.slick.dao.SlickDatabaseDAO
import dimensions.fifth.output.database.slick.dao.SlickDatabaseDAO.Implicits._

abstract trait `Slick Query *'dao`[P <: JdbcProfile] {

  type DAO <: SlickDatabaseDAO[P]

  type DB = Database

  implicit protected val boardId: Long

  implicit protected val tag: String

  implicit protected val dao: DAO

  final val plainSQL = dao.plainSQL

  protected lazy val db: DB = dao

  implicit protected val executionContext: ExecutionContext

  override def toString(): String = dao.toString

}
