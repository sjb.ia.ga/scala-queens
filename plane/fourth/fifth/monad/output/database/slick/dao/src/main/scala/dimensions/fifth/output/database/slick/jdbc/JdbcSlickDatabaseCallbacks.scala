package queens
package dimensions.fifth
package output.database
package slick
package jdbc

import scala.concurrent.ExecutionContext

import _root_.slick.jdbc.JdbcProfile

import dao.JdbcSlickDatabaseDAO


final class JdbcSlickDatabaseCallbacks[
  P <: JdbcProfile
](implicit
  _dao: JdbcSlickDatabaseDAO[P],
  _ec: ExecutionContext
) extends plainsql.PlainSQLSlickDatabaseCallbacks[P, JdbcSlickDatabaseDAO[P]]


object JdbcSlickDatabaseCallbacks {

  def apply[P <: JdbcProfile]()(implicit dao: JdbcSlickDatabaseDAO[P], executionContext: ExecutionContext) =
    new JdbcSlickDatabaseCallbacks[P]

}
