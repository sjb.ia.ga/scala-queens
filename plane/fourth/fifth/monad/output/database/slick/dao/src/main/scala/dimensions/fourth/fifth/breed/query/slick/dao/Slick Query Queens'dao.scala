package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao

import scala.concurrent.{ ExecutionContext, Future, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile


import dimensions.fifth.output.database.slick.dao.plainsql.PlainSQLSlickDatabaseDAO

class `Impl.Slick Query Queens'dao`[
  P <: JdbcProfile,
  DAO <: PlainSQLSlickDatabaseDAO[P]
](
  _id: Long,
  _tag: String
)(implicit
  _dao: DAO,
  _ec: ExecutionContext
) extends `Impl.Slick Query *'dao`[P, DAO](_id, _tag)
    with plainsql.`PlainSQL Slick Query Queens'dao`[P]


import axis.fifth.output.database.Mappings.DbQueen
import axis.fifth.output.database.Implicits._

import version.less.nest.Nest

abstract trait `Slick Query Queens'dao`[P <: JdbcProfile]
    extends `Slick Query *'dao`[P] {

  import `Slick Query Queens'dao`.itemQuery
  import `Slick Query Queens'dao`.Implicits._
  import dao.tables.profile.api._

  def mapQueens(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  ): Seq[DbQueen] = Await
    .result(db
      .run(itemQuery[P, DAO](`nests?`, `rounds?`).result)
      .sort
      ,Duration.Inf
    )
    .dbSeq[P, DAO](`nests?`.map { ns => { it => ns.find(it.==) } })

}


object `Slick Query Queens'dao` {

  import dimensions.fifth.output.database.slick.dao.SlickDatabaseDAO

  type T = (Long, String, Long, Long, Long, Long, Long)

  def itemQuery[
    P <: JdbcProfile,
    DAO <: SlickDatabaseDAO[P]
  ](
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    dao: DAO,
    ec: ExecutionContext
  ) = {

    import dao.tables.rounds
    import dao.tables.profile.api._

    `nests?` match {

      case Some(values) => `rounds?` match {

        case Some(numbers) =>

          for {
            r <- rounds if r.boardID === boardId && r.tag === tag && r.number.inSet(numbers.toSet)
            q <- r.queen if r.queenID === q.id
            at <- q.aspect if q.aspectID === at.id && at.aspect.inSet(values.map(_.aspect.toString).toSet)
            am <- q.algorithm if q.algorithmID === am.id && am.algorithm.inSet(values.map(_.algorithm.toString).toSet)
            ml <- q.model if q.modelID === ml.id && ml.model.inSet(values.map(_.model.toString).toSet)
            ie <- q.interface if q.interfaceID === ie.id
            ue <- q.use if q.useID === ue.id
          } yield (q.id, q.tags, at.id, am.id, ml.id, ie.id, ue.id)

        case _ =>

          for {
            r <- rounds if r.boardID === boardId && r.tag === tag
            q <- r.queen if r.queenID === q.id
            at <- q.aspect if q.aspectID === at.id && at.aspect.inSet(values.map(_.aspect.toString).toSet)
            am <- q.algorithm if q.algorithmID === am.id && am.algorithm.inSet(values.map(_.algorithm.toString).toSet)
            ml <- q.model if q.modelID === ml.id && ml.model.inSet(values.map(_.model.toString).toSet)
            ie <- q.interface if q.interfaceID === ie.id
            ue <- q.use if q.useID === ue.id
          } yield (q.id, q.tags, at.id, am.id, ml.id, ie.id, ue.id)

      }

      case _ => `rounds?` match {

        case Some(numbers) =>

          for {
            r <- rounds if r.boardID === boardId && r.tag === tag && r.number.inSet(numbers.toSet)
            q <- r.queen if r.queenID === q.id
            at <- q.aspect if q.aspectID === at.id
            am <- q.algorithm if q.algorithmID === am.id
            ml <- q.model if q.modelID === ml.id
            ie <- q.interface if q.interfaceID === ie.id
            ue <- q.use if q.useID === ue.id
          } yield (q.id, q.tags, at.id, am.id, ml.id, ie.id, ue.id)

        case _ =>

          for {
            r <- rounds if r.boardID === boardId && r.tag === tag
            q <- r.queen if r.queenID === q.id
            at <- q.aspect if q.aspectID === at.id
            am <- q.algorithm if q.algorithmID === am.id
            ml <- q.model if q.modelID === ml.id
            ie <- q.interface if q.interfaceID === ie.id
            ue <- q.use if q.useID === ue.id
          } yield (q.id, q.tags, at.id, am.id, ml.id, ie.id, ue.id)

      }

    }

  }

  object Implicits {

    implicit class Sort(it: Future[Seq[T]]) {

      def sort(implicit ec: ExecutionContext) = it
        .map(_
          .sortBy(_._1)
        )

    }

    implicit class DbSeq(it: Seq[T]) {

      def dbSeq[
        P <: JdbcProfile,
        DAO <: SlickDatabaseDAO[P]
      ](
        filter: Option[Nest => Option[Nest]]
      )(implicit
        dao: DAO
      ) = it
        .map { case (qid, tags, atid, amid, mlid, ieid, ueid) =>

          val q = DbQueen(dao.mapAspect(atid), atid
                         ,dao.mapAlgorithm(amid), amid
                         ,dao.mapModel(mlid), mlid
                         ,dao.mapInterface(ieid), ieid
                         ,dao.mapUse(ueid), ueid
                         ,tags
                         ,qid.?
                  )

          filter.fold[Option[DbQueen]](Some(q)) { f =>
            f(q.nest_mk) match {
              case None => None
              case _ => Some(q)
            }
          }

        }
        .filter(_.nonEmpty)
        .map(_.get)

    }

  }

}
