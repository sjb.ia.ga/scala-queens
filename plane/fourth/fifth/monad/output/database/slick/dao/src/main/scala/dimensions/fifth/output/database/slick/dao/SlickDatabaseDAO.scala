package queens
package dimensions.fifth
package output.database
package slick
package dao

import scala.concurrent.ExecutionContext

import _root_.slick.jdbc.JdbcProfile
import _root_.slick.jdbc.JdbcBackend.backend.Database

import axis.fifth.output.database.dbdimensions.Mappings.{
  DbAspect,
  DbAlgorithm,
  DbModel,
  DbInterface,
  DbUse
}

import axis.fifth.output.database.slick.dao.Tables
import axis.fifth.output.database.slick.dao.dbdimensions.DAO

import dimensions.Dimension.Use.Output.Database.Slick.Profile


abstract trait SlickDatabaseDAO[P <: JdbcProfile] {

  val slickProfile: Profile

  val plainSQL: Boolean

  type DB = Database

  implicit protected val db: DB

  override def finalize(): Unit = db.close()


  override def toString(): String = if (plainSQL) " PlainSQL" else ""


  implicit protected val executionContext: ExecutionContext


  val tables: Tables[P]


  def mapAspect(id: Long): DbAspect =
    DAO.Aspects.mapAspect(id, tables.d)

  def mapAlgorithm(id: Long): DbAlgorithm =
    DAO.Algorithms.mapAlgorithm(id, tables.d)

  def mapModel(id: Long): DbModel =
    DAO.Models.mapModel(id, tables.d)

  def mapInterface(id: Long): DbInterface =
    DAO.Interfaces.mapInterface(id, tables.d)

  def mapUse(id: Long): DbUse =
    DAO.Uses.mapUse(id, tables.d)

}


object SlickDatabaseDAO {

  object Implicits {

    implicit def dao2db[DAO <: SlickDatabaseDAO[?], D <: Database](it: DAO): D =
      it.db.asInstanceOf[D]

  }

}
