package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package delete
package plainsql

import scala.concurrent.{ Await, ExecutionContext }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile

import axis.fifth.output.database.Mappings.DbSolution
import axis.fifth.output.database.Implicits._


abstract trait `PlainSQL Slick delete Query Solutions'dao`[P <: JdbcProfile]
    extends slick.dao.delete.`Slick delete Query Solutions'dao`[P]
    with `PlainSQL Slick delete Query *'dao`[DbSolution, P] {

  override def delete(it: DbSolution): Boolean =
    if (dao.plainSQL)
      delete_plainSQL(it)
    else
      super.delete(it)

  import dao.tables.profile.api._

  protected def `deleteQuery*_plainSQL`(it: DbSolution) = {
    val id: Long = it.id
    sqlu"""delete from "SOLUTION_POINTS" where "SOLUTION_ID" = $id"""
  }

  override protected def delete_plainSQL(it: DbSolution): Boolean = { Await
    .result(db
      .run(
        DBIO.seq(
          `deleteQuery*_plainSQL`(it),
          deleteQuery_plainSQL(it)
        ).transactionally
      )
      ,Duration.Inf
    )
    true
  }

}
