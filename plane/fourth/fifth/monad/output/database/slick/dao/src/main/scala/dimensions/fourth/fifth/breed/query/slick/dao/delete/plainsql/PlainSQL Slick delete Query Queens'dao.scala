package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package delete
package plainsql

import _root_.slick.jdbc.JdbcProfile

import axis.fifth.output.database.Mappings.DbQueen


abstract trait `PlainSQL Slick delete Query Queens'dao`[P <: JdbcProfile]
    extends slick.dao.delete.`Slick delete Query Queens'dao`[P]
    with `PlainSQL Slick delete Query *'dao`[DbQueen, P] {

  override def delete(it: DbQueen): Boolean =
    if (dao.plainSQL)
      delete_plainSQL(it)
    else
      super.delete(it)

}
