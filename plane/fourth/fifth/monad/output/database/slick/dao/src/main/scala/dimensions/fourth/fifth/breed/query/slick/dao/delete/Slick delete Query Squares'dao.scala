package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package delete

import scala.concurrent.{ ExecutionContext, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile


import dimensions.fifth.output.database.slick.dao.plainsql.PlainSQLSlickDatabaseDAO

class `Impl.Slick delete Query Squares'dao`[
  P <: JdbcProfile,
  DAO <: PlainSQLSlickDatabaseDAO[P]
](
  _id: Long,
  _tag: String
)(implicit
  _dao: DAO,
  _ec: ExecutionContext
) extends `Impl.Slick Query Squares'dao`[P, DAO](_id, _tag)
    with plainsql.`PlainSQL Slick delete Query Squares'dao`[P]


import axis.fifth.output.database.Mappings.DbSquare
import axis.fifth.output.database.Implicits._

abstract trait `Slick delete Query Squares'dao`[P <: JdbcProfile]
    extends `Slick Query Squares'dao`[P]
    with `Slick delete Query *'dao`[DbSquare, P] {

  import dao.tables.profile.api._
  import dao.tables.squares

  protected def deleteQuery(it: DbSquare) = squares
    .filter { _c =>
      _c.boardID === it.board_db.id &&
      _c.row === it.row_db &&
      _c.col === it.col_db
    }

  override def delete(it: DbSquare): Boolean = { Await
    .result(db
      .run(
        deleteQuery(it)
          .delete
      )
      ,Duration.Inf
    )
    true
  }

}
