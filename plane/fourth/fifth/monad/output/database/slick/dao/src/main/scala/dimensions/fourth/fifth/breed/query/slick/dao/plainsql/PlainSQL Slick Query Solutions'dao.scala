package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package plainsql

import scala.collection.Map
import scala.collection.mutable.{ ListBuffer => MutableList, LinkedHashMap => MutableMap }

import scala.concurrent.{ ExecutionContext, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile

import axis.fifth.output.database.Mappings.{ DbBoard, DbSquare, DbRound, DbQueen, DbSolution, DbSolutionPoint }
import axis.fifth.output.database.Implicits._

import version.less.nest.Nest


abstract trait `PlainSQL Slick Query Solutions'dao`[P <: JdbcProfile]
    extends `Slick Query Solutions'dao`[P]
    with `PlainSQL Slick Query *'dao`[P] {

  override def mapSolutions(
    `nests?`: Option[Iterable[Nest]],
    `solutions?`: Option[Iterable[Long]]
  ): Map[DbSolution, Seq[(DbSquare, Option[DbSolutionPoint])]] =
    if (dao.plainSQL)
      mapSolutions_plainSQL(`nests?`, `solutions?`)
    else
      super.mapSolutions(`nests?`, `solutions?`)

  protected def mapSolutions_plainSQL(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  ): Map[DbSolution, Seq[(DbSquare, Option[DbSolutionPoint])]] = {
    implicit val r = MutableMap[DbSolution, MutableList[DbSolutionPoint]]()
    `mapSolutions_plainSQL*`(`nests?`, `rounds?`)
  }

  import `PlainSQL Slick Query Squares'dao`.{ itemQuery => `itemQuery*` }
  import `Slick Query Squares'dao`.Implicits.{ DbSeq => `DbSeq*` }
  import `PlainSQL Slick Query Solutions'dao`.itemQuery
  import `Slick Query Solutions'dao`.Implicits._
  import dao.tables.profile.api._

  private def `mapSolutions_plainSQL*`(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    r: MutableMap[DbSolution, MutableList[DbSolutionPoint]]
  ): Seq[DbSquare] = {
    val (b, it) = Await
      .result(db
        .run(`itemQuery*`[P, DAO]
         zip itemQuery[P, DAO](`nests?`, `rounds?`))
        .sort
        ,Duration.Inf
    )

    it.dbMap[P, DAO](`nests?`.map { ns => { it => ns.find(it.==) } })
    b.dbSeq
  }

}


object `PlainSQL Slick Query Solutions'dao` {

  import dimensions.fifth.output.database.slick.dao.SlickDatabaseDAO

  type T = slick.dao.`Slick Query Solutions'dao`.T

  def itemQuery[
    P <: JdbcProfile,
    DAO <: SlickDatabaseDAO[P]
  ](
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    dao: DAO,
    ec: ExecutionContext
  ) = {

    import dao.tables.profile.api._

    `nests?` match {

      case Some(values) => `rounds?` match {

        case Some(numbers) =>

          sql"""select b."SIZE", r."ID", r."NUMBER", s."ID", s."NUMBER", sp."ROW_NO", sp."COLUMN", q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                from "ROUNDS" r
                join "BOARDS" b on r."BOARD_ID" = b."ID"
                join "QUEENS" q on r."QUEEN_ID" = q."ID"
                join "SOLUTIONS" s on s."ROUND_ID" = r."ID"
                join "SOLUTION_POINTS" sp on sp."SOLUTION_ID" = s."ID"
                join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                join "MODELS" ml on q."MODEL_ID" = ml."ID"
                join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                join "USES" ue on q."USE_ID" = ue."ID"
                where b."ID" = $boardId and r."TAG" = $tag
                and r."NUMBER" in (${numbers.mkString(", ")})
                and at."ASPECT" in (${values.map(_.aspect).mkString(", ")})
                and am."ALGORITHM" in (${values.map(_.algorithm).mkString(", ")})
                and ml."MODEL" in (${values.map(_.model).mkString(", ")})
             """.as[T]

        case _ =>

          sql"""select b."SIZE", r."ID", r."NUMBER", s."ID", s."NUMBER", sp."ROW_NO", sp."COLUMN", q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                from "ROUNDS" r
                join "BOARDS" b on r."BOARD_ID" = b."ID"
                join "QUEENS" q on r."QUEEN_ID" = q."ID"
                join "SOLUTIONS" s on s."ROUND_ID" = r."ID"
                join "SOLUTION_POINTS" sp on sp."SOLUTION_ID" = s."ID"
                join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                join "MODELS" ml on q."MODEL_ID" = ml."ID"
                join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                join "USES" ue on q."USE_ID" = ue."ID"
                where b."ID" = $boardId and r."TAG" = $tag
                and at."ASPECT" in (${values.map(_.aspect).mkString(", ")})
                and am."ALGORITHM" in (${values.map(_.algorithm).mkString(", ")})
                and ml."MODEL" in (${values.map(_.model).mkString(", ")})
             """.as[T]

      }

      case _ => `rounds?` match {

        case Some(numbers) =>

          sql"""select b."SIZE", r."ID", r."NUMBER", s."ID", s."NUMBER", sp."ROW_NO", sp."COLUMN", q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                from "ROUNDS" r
                join "BOARDS" b on r."BOARD_ID" = b."ID"
                join "QUEENS" q on r."QUEEN_ID" = q."ID"
                join "SOLUTIONS" s on s."ROUND_ID" = r."ID"
                join "SOLUTION_POINTS" sp on sp."SOLUTION_ID" = s."ID"
                join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                join "MODELS" ml on q."MODEL_ID" = ml."ID"
                join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                join "USES" ue on q."USE_ID" = ue."ID"
                where b."ID" = $boardId and r."TAG" = $tag
                and r."NUMBER" in (${numbers.mkString(", ")})
             """.as[T]

        case _ =>

          sql"""select b."SIZE", r."ID", r."NUMBER", s."ID", s."NUMBER", sp."ROW_NO", sp."COLUMN", q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                from "ROUNDS" r
                join "BOARDS" b on r."BOARD_ID" = b."ID"
                join "QUEENS" q on r."QUEEN_ID" = q."ID"
                join "SOLUTIONS" s on s."ROUND_ID" = r."ID"
                join "SOLUTION_POINTS" sp on sp."SOLUTION_ID" = s."ID"
                join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                join "MODELS" ml on q."MODEL_ID" = ml."ID"
                join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                join "USES" ue on q."USE_ID" = ue."ID"
                where b."ID" = $boardId and r."TAG" = $tag
             """.as[T]

      }

    }

  }

}
