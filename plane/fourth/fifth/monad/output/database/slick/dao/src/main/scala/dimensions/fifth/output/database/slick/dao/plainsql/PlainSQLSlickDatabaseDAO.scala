package queens
package dimensions.fifth
package output.database
package slick
package dao
package plainsql

import _root_.slick.jdbc.JdbcProfile


abstract trait PlainSQLSlickDatabaseDAO[P <: JdbcProfile]
    extends slick.dao.SlickDatabaseDAO[P]
