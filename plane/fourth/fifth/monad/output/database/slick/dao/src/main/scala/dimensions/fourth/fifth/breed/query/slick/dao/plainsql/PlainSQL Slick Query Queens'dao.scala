package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package plainsql

import scala.concurrent.{ ExecutionContext, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile

import axis.fifth.output.database.Mappings.DbQueen

import version.less.nest.Nest


abstract trait `PlainSQL Slick Query Queens'dao`[P <: JdbcProfile]
    extends `Slick Query Queens'dao`[P]
    with `PlainSQL Slick Query *'dao`[P] {

  override def mapQueens(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  ): Seq[DbQueen] =
    if (dao.plainSQL)
      mapQueens_plainSQL(`nests?`, `rounds?`)
    else
      super.mapQueens(`nests?`, `rounds?`)

  import `PlainSQL Slick Query Queens'dao`.itemQuery
  import `Slick Query Queens'dao`.Implicits._
  import dao.tables.profile.api._

  protected def mapQueens_plainSQL(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  ): Seq[DbQueen] = Await
    .result(db
      .run(itemQuery[P, DAO](`nests?`, `rounds?`))
      .sort
      ,Duration.Inf
    )
    .dbSeq[P, DAO](`nests?`.map { ns => { it => ns.find(it.==) } })

}


object `PlainSQL Slick Query Queens'dao` {

  import dimensions.fifth.output.database.slick.dao.SlickDatabaseDAO

  type T = slick.dao.`Slick Query Queens'dao`.T

  def itemQuery[
    P <: JdbcProfile,
    DAO <: SlickDatabaseDAO[P]
  ](
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    dao: DAO,
    ec: ExecutionContext
  ) = {

    import dao.tables.profile.api._

    `nests?` match {

      case Some(values) => `rounds?` match {

        case Some(numbers) =>

          sql"""select q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                from "ROUNDS" r
                join "BOARDS" b on r."BOARD_ID" = b."ID"
                join "QUEENS" q on r."QUEEN_ID" = q."ID"
                join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                join "MODELS" ml on q."MODEL_ID" = ml."ID"
                join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                join "USES" ue on q."USE_ID" = ue."ID"
                where b."ID" = $boardId and r."TAG" = $tag
                and r."NUMBER" in (${numbers.mkString(", ")})
                and at."ASPECT" in (${values.map(_.aspect).mkString(", ")})
                and am."ALGORITHM" in (${values.map(_.algorithm).mkString(", ")})
                and ml."MODEL" in (${values.map(_.model).mkString(", ")})
             """.as[T]

        case _ =>

          sql"""select q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                from "ROUNDS" r
                join "BOARDS" b on r."BOARD_ID" = b."ID"
                join "QUEENS" q on r."QUEEN_ID" = q."ID"
                join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                join "MODELS" ml on q."MODEL_ID" = ml."ID"
                join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                join "USES" ue on q."USE_ID" = ue."ID"
                where b."ID" = $boardId and r."TAG" = $tag
                and at."ASPECT" in (${values.map(_.aspect).mkString(", ")})
                and am."ALGORITHM" in (${values.map(_.algorithm).mkString(", ")})
                and ml."MODEL" in (${values.map(_.model).mkString(", ")})
             """.as[T]

      }

      case _ => `rounds?` match {

        case Some(numbers) =>

          sql"""select q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                from "ROUNDS" r
                join "BOARDS" b on r."BOARD_ID" = b."ID"
                join "QUEENS" q on r."QUEEN_ID" = q."ID"
                join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                join "MODELS" ml on q."MODEL_ID" = ml."ID"
                join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                join "USES" ue on q."USE_ID" = ue."ID"
                where b."ID" = $boardId and r."TAG" = $tag
                and r."NUMBER" in (${numbers.mkString(", ")})
             """.as[T]

        case _ =>

          sql"""select q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
                from "ROUNDS" r
                join "BOARDS" b on r."BOARD_ID" = b."ID"
                join "QUEENS" q on r."QUEEN_ID" = q."ID"
                join "ASPECTS" at on q."ASPECT_ID" = at."ID"
                join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
                join "MODELS" ml on q."MODEL_ID" = ml."ID"
                join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
                join "USES" ue on q."USE_ID" = ue."ID"
                where b."ID" = $boardId and r."TAG" = $tag
             """.as[T]

      }

    }

  }

}
