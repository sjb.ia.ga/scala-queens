package queens
package dimensions.fifth
package output.database
package slick
package jdbc
package dao

import scala.concurrent.ExecutionContext

import scala.reflect.ClassTag

import _root_.slick.basic.DatabaseConfig
import _root_.slick.jdbc.JdbcProfile

import dimensions.Dimension.Use.Output.Database.Slick.Profile


final class JdbcSlickDatabaseDAO[P <: JdbcProfile : ClassTag](
  databaseConfig: String,
  override val slickProfile: Profile,
  override val plainSQL: Boolean = false
)(implicit
  override protected val executionContext: ExecutionContext
) extends slick.dao.plainsql.PlainSQLSlickDatabaseDAO[P] {

  private val dc = DatabaseConfig.forConfig[P](databaseConfig)
  override protected lazy val db = dc.db.asInstanceOf[DB]


  override val tables =
    new axis.fifth.output.database.slick.dao.Tables[P](
      dc.profile,
      new axis.fifth.output.database.slick.dao.dbdimensions.Tables[P](dc.profile)
    )

  override def toString(): String = "Jdbc" + super.toString()

}
