package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package delete
package plainsql

import scala.concurrent.{ Await, ExecutionContext }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile

import axis.fifth.output.database.Mappings.DbSquare
import axis.fifth.output.database.Implicits._


abstract trait `PlainSQL Slick delete Query Squares'dao`[P <: JdbcProfile]
    extends slick.dao.delete.`Slick delete Query Squares'dao`[P]
    with `PlainSQL Slick delete Query *'dao`[DbSquare, P] {

  override def delete(it: DbSquare): Boolean =
    if (dao.plainSQL)
      delete_plainSQL(it)
    else
      super.delete(it)

  import dao.tables.profile.api._

  override protected def deleteQuery_plainSQL(it: DbSquare) = {
    val boardId: Long = it.board_db.id
    sqlu"""delete from "SQUARES"
           where "BOARD_ID" = $boardId
           and "ROW_NO" = ${it.row_db}
           and "COLUMN" = ${it.col_db}
        """
  }

  override protected def delete_plainSQL(it: DbSquare): Boolean = { Await
    .result(db
      .run(
        deleteQuery_plainSQL(it)
      )
      ,Duration.Inf
    )
    true
  }

}
