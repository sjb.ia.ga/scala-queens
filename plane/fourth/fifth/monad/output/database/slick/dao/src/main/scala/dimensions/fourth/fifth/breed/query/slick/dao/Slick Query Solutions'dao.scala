package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao

import scala.collection.Map
import scala.collection.mutable.{ ListBuffer => MutableList, LinkedHashMap => MutableMap }

import scala.concurrent.{ ExecutionContext, Future, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile


import dimensions.fifth.output.database.slick.dao.plainsql.PlainSQLSlickDatabaseDAO

class `Impl.Slick Query Solutions'dao`[
  P <: JdbcProfile,
  DAO <: PlainSQLSlickDatabaseDAO[P]
](
  _id: Long,
  _tag: String
)(implicit
  _dao: DAO,
  _ec: ExecutionContext
) extends `Impl.Slick Query *'dao`[P, DAO](_id, _tag)
    with plainsql.`PlainSQL Slick Query Solutions'dao`[P]


import axis.fifth.output.database.Mappings.{ DbBoard, DbSquare, DbRound, DbQueen, DbSolution, DbSolutionPoint }
import axis.fifth.output.database.Implicits._

import version.less.nest.Nest

abstract trait `Slick Query Solutions'dao`[P <: JdbcProfile]
    extends `Slick Query *'dao`[P] {

  def mapSolutions(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  ): Map[DbSolution, Seq[(DbSquare, Option[DbSolutionPoint])]] = {
    implicit val r = MutableMap[DbSolution, MutableList[DbSolutionPoint]]()
    `mapSolutions*`(`nests?`, `rounds?`)
  }

  import `Slick Query Squares'dao`.{ itemQuery => `itemQuery*` }
  import `Slick Query Squares'dao`.Implicits.{ DbSeq => `DbSeq*` }
  import `Slick Query Solutions'dao`._
  import `Slick Query Solutions'dao`.Implicits._
  import dao.tables.profile.api._

  private def `mapSolutions*`(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    r: MutableMap[DbSolution, MutableList[DbSolutionPoint]]
  ): Seq[DbSquare] = {
    val (b, it) = Await
      .result(db
        .run(`itemQuery*`[P, DAO].result
         zip itemQuery[P, DAO](`nests?`, `rounds?`).result)
        .sort
        ,Duration.Inf
    )

    it.dbMap[P, DAO](`nests?`.map { ns => { it => ns.find(it.==) } })
    b.dbSeq(boardId)
  }

}


object `Slick Query Solutions'dao` {

  import dimensions.fifth.output.database.slick.dao.SlickDatabaseDAO

  type S = `Slick Query Squares'dao`.T

  type T = (Int, Long, Long, Long, Long, Int, Int, Long, String, Long, Long, Long, Long, Long)

  def itemQuery[
    P <: JdbcProfile,
    DAO <: SlickDatabaseDAO[P]
  ](
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    dao: DAO,
    ec: ExecutionContext
  ) = {

    import dao.tables.{ rounds, solutions, solutionPoints }
    import dao.tables.profile.api._

    `nests?` match {

      case Some(values) => `rounds?` match {

        case Some(numbers) =>

          for {
            r <- rounds if r.boardID === boardId && r.tag === tag && r.number.inSet(numbers.toSet)
            b <- r.board if r.boardID === b.id
            q <- r.queen if r.queenID === q.id
            s <- solutions if s.roundID === r.id
            p <- solutionPoints if p.solutionID === s.id
            at <- q.aspect if q.aspectID === at.id && at.aspect.inSet(values.map(_.aspect.toString).toSet)
            am <- q.algorithm if q.algorithmID === am.id && am.algorithm.inSet(values.map(_.algorithm.toString).toSet)
            ml <- q.model if q.modelID === ml.id && ml.model.inSet(values.map(_.model.toString).toSet)
            ie <- q.interface if q.interfaceID === ie.id
            ue <- q.use if q.useID === ue.id
          } yield (b.size, r.id, r.number, s.id, s.number, p.row, p.col, q.id, q.tags, at.id, am.id, ml.id, ie.id, ue.id)

        case _ =>

          for {
            r <- rounds if r.boardID === boardId && r.tag === tag
            b <- r.board if r.boardID === b.id
            q <- r.queen if r.queenID === q.id
            s <- solutions if s.roundID === r.id
            p <- solutionPoints if p.solutionID === s.id
            at <- q.aspect if q.aspectID === at.id && at.aspect.inSet(values.map(_.aspect.toString).toSet)
            am <- q.algorithm if q.algorithmID === am.id && am.algorithm.inSet(values.map(_.algorithm.toString).toSet)
            ml <- q.model if q.modelID === ml.id && ml.model.inSet(values.map(_.model.toString).toSet)
            ie <- q.interface if q.interfaceID === ie.id
            ue <- q.use if q.useID === ue.id
          } yield (b.size, r.id, r.number, s.id, s.number, p.row, p.col, q.id, q.tags, at.id, am.id, ml.id, ie.id, ue.id)

      }

      case _ => `rounds?` match {

        case Some(numbers) =>

          for {
            r <- rounds if r.boardID === boardId && r.tag === tag && r.number.inSet(numbers.toSet)
            b <- r.board if r.boardID === b.id
            q <- r.queen if r.queenID === q.id
            s <- solutions if s.roundID === r.id
            p <- solutionPoints if p.solutionID === s.id
            at <- q.aspect if q.aspectID === at.id
            am <- q.algorithm if q.algorithmID === am.id
            ml <- q.model if q.modelID === ml.id
            ie <- q.interface if q.interfaceID === ie.id
            ue <- q.use if q.useID === ue.id
          } yield (b.size, r.id, r.number, s.id, s.number, p.row, p.col, q.id, q.tags, at.id, am.id, ml.id, ie.id, ue.id)

        case _ =>

          for {
            r <- rounds if r.boardID === boardId && r.tag === tag
            b <- r.board if r.boardID === b.id
            q <- r.queen if r.queenID === q.id
            s <- solutions if s.roundID === r.id
            p <- solutionPoints if p.solutionID === s.id
            at <- q.aspect if q.aspectID === at.id
            am <- q.algorithm if q.algorithmID === am.id
            ml <- q.model if q.modelID === ml.id
            ie <- q.interface if q.interfaceID === ie.id
            ue <- q.use if q.useID === ue.id
          } yield (b.size, r.id, r.number, s.id, s.number, p.row, p.col, q.id, q.tags, at.id, am.id, ml.id, ie.id, ue.id)

      }

    }

  }

  object Implicits {

    implicit class Sort(it: Future[(Seq[S], Seq[T])]) {

      def sort(implicit ec: ExecutionContext) = it
        .map { case (b, s) => b
             .sortBy(_._3)
             .sortBy(_._2) -> s
             .sortBy(_._7)
             .sortBy(_._6)
             .sortBy(_._5)
             .sortBy(_._3)
        }

    }

    implicit class DbMap(it: Seq[T]) {
      def dbMap[
        P <: JdbcProfile,
        DAO <: SlickDatabaseDAO[P]
      ](
        filter: Option[Nest => Option[Nest]]
      )(implicit
        boardId: Long,
        tag: String,
        dao: DAO,
        r: MutableMap[DbSolution, MutableList[DbSolutionPoint]]
      ) = it
        .foreach { case (size, rid, rno, sid, no, row, col, qid, tags, atid, amid, mlid, ieid, ueid) =>

          val b = DbBoard(size, boardId.?)
          val q = DbQueen(dao.mapAspect(atid), atid
                         ,dao.mapAlgorithm(amid), amid
                         ,dao.mapModel(mlid), mlid
                         ,dao.mapInterface(ieid), ieid
                         ,dao.mapUse(ueid), ueid
                         ,tags
                         ,qid.?
                  )
          val s = DbSolution(DbRound(b, boardId, q, q.id, tag, rno, rid.?), rid, no, sid.?)

          filter.fold[Option[DbSolution]](Some(s)) { f =>
            f(q.nest_mk) match {
              case None => None
              case _ => Some(s)
            }
          } match {

            case None =>

            case _ =>

              if (!r.contains(s))
                r(s) = MutableList()

              r(s) += DbSolutionPoint(s, s.id, row, col)

          }

        }

    }

  }

}
