package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package delete
package plainsql

import scala.concurrent.{ Await, ExecutionContext }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile

import axis.fifth.output.database.Mappings._
import axis.fifth.output.database.Implicits._


abstract trait `PlainSQL Slick delete Query *'dao`[
  E <: DbEntity,
  P <: JdbcProfile
] extends slick.dao.delete.`Slick delete Query *'dao`[E, P]
    with dao.plainsql.`PlainSQL Slick Query *'dao`[P] {

  import dao.tables.profile.api._

  protected def deleteQuery_plainSQL(it: E) = {
    val id: Long = it.id
    it match {
      case _: DbQueen =>
        sqlu"""delete from "QUEENS" where "ID" = $id"""
      case _: DbRound =>
        sqlu"""delete from "ROUNDS" where "ID" = $id"""
      case _: DbSolution =>
        sqlu"""delete from "SOLUTIONS" where "ID" = $id"""
      case _ =>
        ???
    }
  }

  protected def delete_plainSQL(it: E): Boolean = { Await
    .result(db
      .run(
        deleteQuery_plainSQL(it)
      )
      ,Duration.Inf
    )
    true
  }

}
