package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package plainsql

import _root_.slick.jdbc.JdbcProfile


abstract trait `PlainSQL Slick Query *'dao`[P <: JdbcProfile]
    extends slick.dao.`Slick Query *'dao`[P]
