package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao

import scala.concurrent.{ ExecutionContext, Future, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile

import dimensions.fifth.output.database.slick.dao.plainsql.PlainSQLSlickDatabaseDAO


class `Impl.Slick Query Rounds'dao`[
  P <: JdbcProfile,
  DAO <: PlainSQLSlickDatabaseDAO[P]
](
  _id: Long,
  _tag: String
)(implicit
  _dao: DAO,
  _ec: ExecutionContext
) extends `Impl.Slick Query *'dao`[P, DAO](_id, _tag)
    with plainsql.`PlainSQL Slick Query Rounds'dao`[P]


import axis.fifth.output.database.Mappings.{ DbBoard, DbQueen, DbRound }
import axis.fifth.output.database.Implicits._

abstract trait `Slick Query Rounds'dao`[P <: JdbcProfile]
    extends `Slick Query *'dao`[P] {

  import `Slick Query Rounds'dao`.itemQuery
  import `Slick Query Rounds'dao`.Implicits._
  import dao.tables.profile.api._

  def mapRounds(
    `rounds?`: Option[Iterable[Long]]
  ): Seq[DbRound] = Await
    .result(db
      .run(itemQuery[P, DAO](`rounds?`).result)
      .sort
      ,Duration.Inf
    )
    .dbSeq[P, DAO]

}


object `Slick Query Rounds'dao` {

  import dimensions.fifth.output.database.slick.dao.SlickDatabaseDAO

  type T = (Int, Long, Long, Long, String, Long, Long, Long, Long, Long)

  def itemQuery[
    P <: JdbcProfile,
    DAO <: SlickDatabaseDAO[P]
  ](
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    dao: DAO,
    ec: ExecutionContext
  ) = {

    import dao.tables.rounds
    import dao.tables.profile.api._

    `rounds?` match {

      case Some(numbers) =>

        for {
          r <- rounds if r.boardID === boardId && r.tag === tag && r.number.inSet(numbers.toSet)
          b <- r.board if r.boardID === b.id
          q <- r.queen if r.queenID === q.id
          at <- q.aspect if q.aspectID === at.id
          am <- q.algorithm if q.algorithmID === am.id
          ml <- q.model if q.modelID === ml.id
          ie <- q.interface if q.interfaceID === ie.id
          ue <- q.use if q.useID === ue.id
        } yield (b.size, r.id, r.number, q.id, q.tags, at.id, am.id, ml.id, ie.id, ue.id)

      case _ =>

        for {
          r <- rounds if r.boardID === boardId && r.tag === tag
          b <- r.board if r.boardID === b.id
          q <- r.queen if r.queenID === q.id
          at <- q.aspect if q.aspectID === at.id
          am <- q.algorithm if q.algorithmID === am.id
          ml <- q.model if q.modelID === ml.id
          ie <- q.interface if q.interfaceID === ie.id
          ue <- q.use if q.useID === ue.id
        } yield (b.size, r.id, r.number, q.id, q.tags, at.id, am.id, ml.id, ie.id, ue.id)

    }

  }

  object Implicits {

    implicit class Sort(it: Future[Seq[T]]) {

      def sort(implicit ec: ExecutionContext) = it
        .map(_
          .sortBy(_._3)
        )

    }

    implicit class DbSeq(it: Seq[T]) {

      def dbSeq[
        P <: JdbcProfile,
        DAO <: SlickDatabaseDAO[P]
      ](implicit
        boardId: Long,
        tag: String,
        dao: DAO
      ) = it
        .map { case (size, rid, no, qid, tags, atid, amid, mlid, ieid, ueid) =>

          val b = DbBoard(size, boardId.?)
          val q = DbQueen(dao.mapAspect(atid), atid
                         ,dao.mapAlgorithm(amid), amid
                         ,dao.mapModel(mlid), mlid
                         ,dao.mapInterface(ieid), ieid
                         ,dao.mapUse(ueid), ueid
                         ,tags
                         ,qid.?
                  )

          DbRound(b, boardId, q, qid, tag, no, rid.?)

        }

    }

  }

}
