package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package delete

import _root_.slick.jdbc.JdbcProfile

import axis.fifth.output.database.Mappings.DbEntity


abstract trait `Slick delete Query *'dao`[
  E <: DbEntity,
  P <: JdbcProfile
] extends `Slick Query *'dao`[P] {

  def delete(it: E): Boolean

}
