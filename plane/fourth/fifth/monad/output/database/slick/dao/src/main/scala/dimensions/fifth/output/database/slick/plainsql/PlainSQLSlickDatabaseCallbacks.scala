package queens
package dimensions.fifth
package output.database
package slick
package plainsql

import scala.concurrent.{ ExecutionContext, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile
import _root_.slick.jdbc.JdbcBackend.backend.Database

import common.geom.Implicits.`Coord*`

import queens.base.Board
import dimensions.third.Queens

import slick.dao.plainsql.PlainSQLSlickDatabaseDAO
import slick.dao.SlickDatabaseDAO.Implicits._


abstract class PlainSQLSlickDatabaseCallbacks[
  P <: JdbcProfile,
  DAO <: PlainSQLSlickDatabaseDAO[P]
](implicit
  override protected val dao: DAO,
  override protected val executionContext: ExecutionContext
) extends SlickDatabaseCallbacks[P] {

  private val db: Database = dao

  import dao.tables.profile.api.{ Database => _, _ }

  override def apply(board: Board): Long = {
    if (!dao.plainSQL)
      return super.apply(board)

    val auto: Long = boardId

    val insertAction = DBIO.seq(
      sqlu"""insert into "BOARDS"("ID", "SIZE")
             values ($auto, ${board.N})
          """
      +:
      (for {
        row <- 0 until board.N
        col <- 0 until board.N
      } yield
        sqlu"""insert into "SQUARES"("BOARD_ID", "ROW_NO", "COLUMN", "IS_FREE")
               values ($auto, $row, $col, ${!board(row)(col)})
            """
      ): _*
    )

    Await.result(db.run(insertAction), Duration.Inf)

    auto
  }


  import dimensions.Dimension.Use.{ output$ }
  import dimensions.Dimension.Use.Output.{ database$ }
  import dimensions.Dimension.Use.Output.Database.{ slick$ }
  import dimensions.Dimension.Interface.monadProgram
  import _root_.queens.Implicits._

  override def apply(queens: Queens[?]): Long = {
    if (!dao.plainSQL)
      return super.apply(queens)

    val auto: Long = queenId

    val aspect = queens.aspect.toString()
    val algorithm = queens.algorithm.toString()
    val model = queens.model.toString()
    val interface = monadProgram.toString()
    val use = output$(database$(slick$(dao.slickProfile.toString, true)))

    val tags: List[String] = queens.toString()

    val filterQuery =
      sql"""select at."ID", am."ID", ml."ID", ie."ID", ue."ID"
            from "ASPECTS" at, "ALGORITHMS" am, "MODELS" ml, "INTERFACES" ie, "USES" ue
            where at."ASPECT" = $aspect
            and am."ALGORITHM" = $algorithm
            and ml."MODEL" = $model
            and ie."INTERFACE" = $interface
            and ue."USE" = $use
         """.as[(Long, Long, Long, Long, Long)]

    Await.result(db
      .run(filterQuery.head)
      .flatMap { ids =>
        val (aspectId, algorithmId, modelId, interfaceId, useId) = ids

        val insertAction =
          sqlu"""insert into "QUEENS"("ID", "ASPECT_ID", "ALGORITHM_ID", "MODEL_ID", "INTERFACE_ID", "USE_ID", "TAGS")
                 values ($auto, $aspectId, $algorithmId, $modelId, $interfaceId, $useId, ${tags mkString " "})
              """

        db.run(insertAction)
      }
      ,Duration.Inf
    )

    auto
  }


  override def apply(boardId: Long, tag: String, queenId: Long, roundNo: Option[Long] = None): Long = {
    if (!dao.plainSQL)
      return super.apply(boardId, tag, queenId, roundNo)

    val auto: Long = roundId
    val number: Long = this.roundNo

    val insertAction = DBIO.seq(
      sqlu"""insert into "ROUNDS"("ID", "BOARD_ID", "QUEEN_ID", "TAG", "NUMBER")
             values ($auto, $boardId, $queenId, $tag, $number)
          """
    )

    Await.result(db.run(insertAction), Duration.Inf)

    auto
  }


  override def apply(roundId: Long, solution: Solution, number: Long): Boolean = {
    if (!dao.plainSQL) {
      return super.apply(roundId, solution, number)
    }

    val auto: Long = solutionId

    val dbsolution = (auto, roundId, number)

    val insertAction = DBIO.seq(
      sqlu"""insert into "SOLUTIONS"("ID", "ROUND_ID", "NUMBER")
             values (${dbsolution._1}, ${dbsolution._2}, ${dbsolution._3})
          """
      +:
      solution
        .map { it =>
          sqlu"""insert into "SOLUTION_POINTS"("SOLUTION_ID", "ROW_NO", "COLUMN")
                 values (${dbsolution._1}, ${it.row}, ${it.col})
              """
        }: _*
    )

    Await.result(db.run(insertAction), Duration.Inf)

    true
  }

}
