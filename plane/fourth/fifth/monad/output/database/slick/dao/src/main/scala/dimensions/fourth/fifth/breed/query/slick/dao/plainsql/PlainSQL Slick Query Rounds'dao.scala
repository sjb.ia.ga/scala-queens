package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package plainsql

import scala.concurrent.{ ExecutionContext, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile

import axis.fifth.output.database.Mappings.DbRound


abstract trait `PlainSQL Slick Query Rounds'dao`[P <: JdbcProfile]
    extends `Slick Query Rounds'dao`[P]
    with `PlainSQL Slick Query *'dao`[P] {

  override def mapRounds(
    `rounds?`: Option[Iterable[Long]]
  ): Seq[DbRound] =
    if (dao.plainSQL)
      mapRounds_plainSQL(`rounds?`)
    else
      super.mapRounds(`rounds?`)

  import `PlainSQL Slick Query Rounds'dao`.itemQuery
  import `Slick Query Rounds'dao`.Implicits._
  import dao.tables.profile.api._

  protected def mapRounds_plainSQL(
    `rounds?`: Option[Iterable[Long]]
  ): Seq[DbRound] = Await
    .result(db
      .run(itemQuery[P, DAO](`rounds?`))
      .sort
      ,Duration.Inf
    )
    .dbSeq[P, DAO]

}


object `PlainSQL Slick Query Rounds'dao` {

  import dimensions.fifth.output.database.slick.dao.SlickDatabaseDAO

  type T = slick.dao.`Slick Query Rounds'dao`.T

  def itemQuery[
    P <: JdbcProfile,
    DAO <: SlickDatabaseDAO[P]
  ](
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    dao: DAO,
    ec: ExecutionContext
  ) = {

    import dao.tables.profile.api._

    `rounds?` match {

      case Some(numbers) =>

        sql"""select b."SIZE", r."ID", r."NUMBER", q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
              from "ROUNDS" r
              join "BOARDS" b on r."BOARD_ID" = b."ID"
              join "QUEENS" q on r."QUEEN_ID" = q."ID"
              join "ASPECTS" at on q."ASPECT_ID" = at."ID"
              join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
              join "MODELS" ml on q."MODEL_ID" = ml."ID"
              join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
              join "USES" ue on q."USE_ID" = ue."ID"
              where b."ID" = $boardId and r."TAG" = $tag
              and r."NUMBER" in (${numbers.mkString(", ")})
           """.as[T]

        case _ =>

        sql"""select b."SIZE", r."ID", r."NUMBER", q."ID", q."TAGS", at."ID", am."ID", ml."ID", ie."ID", ue."ID"
              from "ROUNDS" r
              join "BOARDS" b on r."BOARD_ID" = b."ID"
              join "QUEENS" q on r."QUEEN_ID" = q."ID"
              join "ASPECTS" at on q."ASPECT_ID" = at."ID"
              join "ALGORITHMS" am on q."ALGORITHM_ID" = am."ID"
              join "MODELS" ml on q."MODEL_ID" = ml."ID"
              join "INTERFACES" ie on q."INTERFACE_ID" = ie."ID"
              join "USES" ue on q."USE_ID" = ue."ID"
              where b."ID" = $boardId and r."TAG" = $tag
           """.as[T]

    }

  }

}
