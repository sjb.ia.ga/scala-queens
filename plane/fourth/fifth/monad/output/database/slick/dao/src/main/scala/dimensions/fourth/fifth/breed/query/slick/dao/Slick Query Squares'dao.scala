package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao

import scala.concurrent.{ ExecutionContext, Future, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile


import dimensions.fifth.output.database.slick.dao.plainsql.PlainSQLSlickDatabaseDAO

class `Impl.Slick Query Squares'dao`[
  P <: JdbcProfile,
  DAO <: PlainSQLSlickDatabaseDAO[P]
](
  _id: Long,
  _tag: String
)(implicit
  _dao: DAO,
  _ec: ExecutionContext
) extends `Impl.Slick Query *'dao`[P, DAO](_id, _tag)
    with plainsql.`PlainSQL Slick Query Squares'dao`[P]


import axis.fifth.output.database.Mappings.{ DbBoard, DbSquare }
import axis.fifth.output.database.Implicits._

abstract trait `Slick Query Squares'dao`[P <: JdbcProfile]
    extends `Slick Query *'dao`[P] {

  import `Slick Query Squares'dao`.itemQuery
  import `Slick Query Squares'dao`.Implicits._
  import dao.tables.profile.api._

  def mapSquares(): Seq[DbSquare] = Await
    .result(db
      .run(itemQuery[P, DAO].result)
      .sort
      ,Duration.Inf
    )
    .dbSeq

}


object `Slick Query Squares'dao` {

  import dimensions.fifth.output.database.slick.dao.SlickDatabaseDAO

  type T = (Int, Int, Int, Boolean)

  def itemQuery[
    P <: JdbcProfile,
    DAO <: SlickDatabaseDAO[P]
  ](implicit
    boardId: Long,
    dao: DAO,
    ec: ExecutionContext
  ) = {

    import dao.tables.squares
    import dao.tables.profile.api._

    for {
      s <- squares if s.boardID === boardId
      b <- s.board if s.boardID === b.id
    } yield (b.size, s.row, s.col, s.isFree)

  }

  object Implicits {

    implicit class Sort(it: Future[Seq[T]]) {

      def sort(implicit ec: ExecutionContext) = it
        .map(_
          .sortBy(_._3)
          .sortBy(_._2)
        )

    }

    implicit class DbSeq(it: Seq[T]) {

      def dbSeq(implicit boardId: Long) = it
        .map { case (size, row, col, isFree) =>

          val b = DbBoard(size, boardId.?)

          DbSquare(b, boardId, row, col, isFree)

        }

    }

  }

}
