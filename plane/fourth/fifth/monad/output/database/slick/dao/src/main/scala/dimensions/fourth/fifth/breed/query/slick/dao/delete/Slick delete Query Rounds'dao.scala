package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package dao
package delete

import scala.concurrent.{ ExecutionContext, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile


import dimensions.fifth.output.database.slick.dao.plainsql.PlainSQLSlickDatabaseDAO

class `Impl.Slick delete Query Rounds'dao`[
  P <: JdbcProfile,
  DAO <: PlainSQLSlickDatabaseDAO[P]
](
  _id: Long,
  _tag: String
)(implicit
  _dao: DAO,
  _ec: ExecutionContext
) extends `Impl.Slick Query Rounds'dao`[P, DAO](_id, _tag)
    with plainsql.`PlainSQL Slick delete Query Rounds'dao`[P]


import axis.fifth.output.database.Mappings.DbRound
import axis.fifth.output.database.Implicits._

abstract trait `Slick delete Query Rounds'dao`[P <: JdbcProfile]
    extends `Slick Query Rounds'dao`[P]
    with `Slick delete Query *'dao`[DbRound, P] {

  import dao.tables.profile.api._
  import dao.tables.rounds

  protected def deleteQuery(it: DbRound) = rounds
    .filter(_.id === it.id)

  override def delete(it: DbRound): Boolean = { Await
    .result(db
      .run(
        deleteQuery(it)
          .delete
      )
      ,Duration.Inf
    )
    true
  }

}
