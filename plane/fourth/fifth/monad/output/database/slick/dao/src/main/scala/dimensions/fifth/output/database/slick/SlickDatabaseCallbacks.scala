package queens
package dimensions.fifth
package output.database
package slick

import java.util.concurrent.atomic.AtomicLong

import scala.concurrent.{ ExecutionContext, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile
import _root_.slick.jdbc.JdbcBackend.backend.Database

import common.geom.Implicits.`Coord*`

import queens.base.Board
import dimensions.third.Queens

import axis.fifth.output.database.Implicits._
import axis.fifth.output.database.Mappings.DbBoard

import dao.SlickDatabaseDAO
import dao.SlickDatabaseDAO.Implicits._


abstract trait SlickDatabaseCallbacks[P <: JdbcProfile] {

  protected val dao: SlickDatabaseDAO[P]

  private val db: Database = dao

  implicit protected val executionContext: ExecutionContext

  implicit protected def atl2long(it: AtomicLong): Long =
    it.addAndGet(1)


  protected val boardId = new AtomicLong(0)

  def apply(board: Board): Long = {
    import dao.tables.profile.api._

    import dao.tables.{ boards, squares }

    val auto: Long = boardId

    val insertAction = DBIO.seq(
      boards += DbBoard(board.N, auto.?),
      squares ++= (for {
        row <- 0 until board.N
        col <- 0 until board.N
      } yield (auto, row, col, !board(row)(col))
      )
    )

    Await.result(db.run(insertAction), Duration.Inf)

    auto
  }


  import dimensions.Dimension.Use.{ output$ }
  import dimensions.Dimension.Use.Output.{ database$ }
  import dimensions.Dimension.Use.Output.Database.{ slick$ }
  import dimensions.Dimension.Interface.monadProgram
  import _root_.queens.Implicits._

  protected val queenId = new AtomicLong(0)

  def apply(queens3: Queens[?]): Long = {
    import dao.tables.profile.api._

    import dao.tables.queens

    import dao.tables.d.{ aspects, algorithms, models, interfaces, uses }

    val auto: Long = queenId

    val aspect = queens3.aspect.toString()
    val algorithm = queens3.algorithm.toString()
    val model = queens3.model.toString()
    val interface = monadProgram.toString()
    val use = output$(database$(slick$(dao.slickProfile.toString, false)))

    val tags: List[String] = queens3.toString()

    val filterQuery = for {
      at <- aspects if at.aspect === aspect
      am <- algorithms if am.algorithm === algorithm
      ml <- models if ml.model === model
      ie <- interfaces if ie.interface === interface
      ue <- uses if ue.use === use.toString
    } yield (at.id, am.id, ml.id, ie.id, ue.id)


    Await.result(db
      .run(filterQuery.result.head)
      .flatMap { ids =>
        val (aspectId, algorithmId, modelId, interfaceId, useId) = ids

        val insertAction =
          queens += (auto,
            aspectId,
            algorithmId,
            modelId,
            interfaceId,
            useId,
            tags mkString " "
          )

        db.run(insertAction)
      }
      ,Duration.Inf
    )

    auto
  }


  protected val roundId = new AtomicLong(0)
  protected val roundNo = new AtomicLong(0)

  def apply(boardId: Long, tag: String, queenId: Long, roundNo: Option[Long] = None): Long = {
    roundNo.map { no => this.roundNo.set(no - 1) }

    import dao.tables.profile.api._

    import dao.tables.rounds

    val auto: Long = roundId
    val number: Long = this.roundNo

    val insertAction = DBIO.seq(
      rounds += (auto, boardId, queenId, tag, number)
    )

    Await.result(db.run(insertAction), Duration.Inf)

    auto
  }


  protected val solutionId = new AtomicLong(0)

  def apply(roundId: Long, solution: Solution, number: Long): Boolean = {
    import dao.tables.profile.api._

    import dao.tables.{ solutions, solutionPoints }

    val auto: Long = solutionId

    val dbsolution = (auto, roundId, number)

    val insertAction = DBIO.seq(
      solutions += dbsolution,
      solutionPoints ++= solution
        .map { it => (dbsolution._1, it.row, it.col ) }
    )

    Await.result(db.run(insertAction), Duration.Inf)

    true
  }

}


object SlickDatabaseCallbacks {

  object Implicits {

    implicit def dcb2dao[P <: JdbcProfile, DCB <: SlickDatabaseCallbacks[P], DAO <: SlickDatabaseDAO[P]](it: DCB): DAO =
      it.dao.asInstanceOf[DAO]

  }

}
