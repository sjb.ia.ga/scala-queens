package queens
package dimensions.fourth.fifth
package breed
package query
package slick

import _root_.slick.jdbc.JdbcProfile

import dao.`Slick Query Squares'dao`

import axis.fifth.output.database.Mappings.DbRound

import conf.query.slick.{ Conf => Slick }

import conf.query.squares.Conf


abstract trait `Slick Query Squares'q`[
  P <: Conf & Slick,
  JP <: JdbcProfile,
  DAO <: `Slick Query Squares'dao`[JP]
](using
  override protected val * : P
) extends `Query Squares'q`[P]
    with `Slick Query *'q`[P, JP, DAO] { this: flow.`Slick Query *'q by Flow`[P, JP, DAO] =>

  override protected type * <: `Slick Query Squares'q`[P, JP, DAO]

  override protected def `map*`[R](
    block: ((QueryItem, Int)) => Option[R]
  ): Seq[Option[R]] = dao
    .mapSquares()
    .zipWithIndex
    .map(block(_))

}
