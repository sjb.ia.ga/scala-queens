package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package delete

import _root_.slick.jdbc.JdbcProfile

import dao.delete.`Slick delete Query Rounds'dao`

import axis.fifth.output.database.Mappings.DbRound

import conf.query.slick.{ Conf => Slick }

import conf.query.rounds.Conf


abstract trait `Slick delete Query Rounds'q`[
  P <: Conf & Slick,
  JP <: JdbcProfile,
  DAO <: `Slick delete Query Rounds'dao`[JP]
](using
  override protected val * : P
) extends `Slick delete Query *'q`[DbRound, P, JP, DAO]:

  override protected type * <: `Slick delete Query Rounds'q`[P, JP, DAO]
