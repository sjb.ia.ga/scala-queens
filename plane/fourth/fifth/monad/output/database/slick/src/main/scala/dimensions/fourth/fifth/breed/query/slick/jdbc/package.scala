package queens
package dimensions.fourth.fifth
package breed
package query
package slick

import _root_.slick.jdbc.JdbcProfile


package jdbc {

  package callback {

    import dao.{
      `Impl.Jdbc Slick Query Rounds'dao`,
      `Impl.Jdbc Slick Query Squares'dao`,
      `Impl.Jdbc Slick Query Queens'dao`,
      `Impl.Jdbc Slick Query Solutions'dao`
    }

    import slick.callback.flow.{
      `Impl.Slick callback Query Rounds'q by Flow`,
      `Slick callback Query Rounds'q by Flow`,
      `Impl.Slick callback Query Squares'q by Flow`,
      `Slick callback Query Squares'q by Flow`,
      `Impl.Slick callback Query Queens'q by Flow`,
      `Slick callback Query Queens'q by Flow`,
      `Impl.Slick callback Query Solutions'q by Flow`,
      `Slick callback Query Solutions'q by Flow`
    }

    package object flow {

      type `Impl.Jdbc Slick callback Query Rounds'q by Flow`[P <: JdbcProfile] = `Impl.Slick callback Query Rounds'q by Flow`[P, `Impl.Jdbc Slick Query Rounds'dao`[P]]
      type `Jdbc Slick callback Query Rounds'q by Flow`[P <: JdbcProfile] = `Slick callback Query Rounds'q by Flow`[P, `Impl.Jdbc Slick Query Rounds'dao`[P]]
      type `Impl.Jdbc Slick callback Query Squares'q by Flow`[P <: JdbcProfile] = `Impl.Slick callback Query Squares'q by Flow`[P, `Impl.Jdbc Slick Query Squares'dao`[P]]
      type `Jdbc Slick callback Query Squares'q by Flow`[P <: JdbcProfile] = `Slick callback Query Squares'q by Flow`[P, `Impl.Jdbc Slick Query Squares'dao`[P]]
      type `Impl.Jdbc Slick callback Query Queens'q by Flow`[P <: JdbcProfile] = `Impl.Slick callback Query Queens'q by Flow`[P, `Impl.Jdbc Slick Query Queens'dao`[P]]
      type `Jdbc Slick callback Query Queens'q by Flow`[P <: JdbcProfile] = `Slick callback Query Queens'q by Flow`[P, `Impl.Jdbc Slick Query Queens'dao`[P]]
      type `Impl.Jdbc Slick callback Query Solutions'q by Flow`[P <: JdbcProfile] = `Impl.Slick callback Query Solutions'q by Flow`[P, `Impl.Jdbc Slick Query Solutions'dao`[P]]
      type `Jdbc Slick callback Query Solutions'q by Flow`[P <: JdbcProfile] = `Slick callback Query Solutions'q by Flow`[P, `Impl.Jdbc Slick Query Solutions'dao`[P]]
    }

  }

  package delete {

    import jdbc.dao.delete.{
      `Impl.Jdbc Slick delete Query Rounds'dao`,
      `Impl.Jdbc Slick delete Query Squares'dao`,
      `Impl.Jdbc Slick delete Query Queens'dao`,
      `Impl.Jdbc Slick delete Query Solutions'dao`
    }

    import slick.delete.flow.{
      `Impl.Slick delete Query Rounds'q by Flow`,
      `Slick delete Query Rounds'q by Flow`,
      `Impl.Slick delete Query Squares'q by Flow`,
      `Slick delete Query Squares'q by Flow`,
      `Impl.Slick delete Query Queens'q by Flow`,
      `Slick delete Query Queens'q by Flow`,
      `Impl.Slick delete Query Solutions'q by Flow`,
      `Slick delete Query Solutions'q by Flow`
    }

    package object flow {
      type `Impl.Jdbc Slick delete Query Rounds'q by Flow`[P <: JdbcProfile] = `Impl.Slick delete Query Rounds'q by Flow`[P, `Impl.Jdbc Slick delete Query Rounds'dao`[P]]
      type `Jdbc Slick delete Query Rounds'q by Flow`[P <: JdbcProfile] = `Slick delete Query Rounds'q by Flow`[P, `Impl.Jdbc Slick delete Query Rounds'dao`[P]]
      type `Impl.Jdbc Slick delete Query Squares'q by Flow`[P <: JdbcProfile] = `Impl.Slick delete Query Squares'q by Flow`[P, `Impl.Jdbc Slick delete Query Squares'dao`[P]]
      type `Jdbc Slick delete Query Squares'q by Flow`[P <: JdbcProfile] = `Slick delete Query Squares'q by Flow`[P, `Impl.Jdbc Slick delete Query Squares'dao`[P]]
      type `Impl.Jdbc Slick delete Query Queens'q by Flow`[P <: JdbcProfile] = `Impl.Slick delete Query Queens'q by Flow`[P, `Impl.Jdbc Slick delete Query Queens'dao`[P]]
      type `Jdbc Slick delete Query Queens'q by Flow`[P <: JdbcProfile] = `Slick delete Query Queens'q by Flow`[P, `Impl.Jdbc Slick delete Query Queens'dao`[P]]
      type `Impl.Jdbc Slick delete Query Solutions'q by Flow`[P <: JdbcProfile] = `Impl.Slick delete Query Solutions'q by Flow`[P, `Impl.Jdbc Slick delete Query Solutions'dao`[P]]
      type `Jdbc Slick delete Query Solutions'q by Flow`[P <: JdbcProfile] = `Slick delete Query Solutions'q by Flow`[P, `Impl.Jdbc Slick delete Query Solutions'dao`[P]]
    }

  }

  package println {

    import jdbc.dao.{
      `Impl.Jdbc Slick Query Rounds'dao`,
      `Impl.Jdbc Slick Query Squares'dao`,
      `Impl.Jdbc Slick Query Queens'dao`,
      `Impl.Jdbc Slick Query Solutions'dao`
    }

    import slick.println.flow.{
      `Impl.Slick println Query Rounds'q by Flow`,
      `Slick println Query Rounds'q by Flow`,
      `Impl.Slick println Query Squares'q by Flow`,
      `Slick println Query Squares'q by Flow`,
      `Impl.Slick println Query Queens'q by Flow`,
      `Slick println Query Queens'q by Flow`,
      `Impl.Slick println Query Solutions'q by Flow`,
      `Slick println Query Solutions'q by Flow`
    }

    package object flow {
      type `Impl.Jdbc Slick println Query Rounds'q by Flow`[P <: JdbcProfile] = `Impl.Slick println Query Rounds'q by Flow`[P, `Impl.Jdbc Slick Query Rounds'dao`[P]]
      type `Jdbc Slick println Query Rounds'q by Flow`[P <: JdbcProfile] = `Slick println Query Rounds'q by Flow`[P, `Impl.Jdbc Slick Query Rounds'dao`[P]]
      type `Impl.Jdbc Slick println Query Squares'q by Flow`[P <: JdbcProfile] = `Impl.Slick println Query Squares'q by Flow`[P, `Impl.Jdbc Slick Query Squares'dao`[P]]
      type `Jdbc Slick println Query Squares'q by Flow`[P <: JdbcProfile] = `Slick println Query Squares'q by Flow`[P, `Impl.Jdbc Slick Query Squares'dao`[P]]
      type `Impl.Jdbc Slick println Query Queens'q by Flow`[P <: JdbcProfile] = `Impl.Slick println Query Queens'q by Flow`[P, `Impl.Jdbc Slick Query Queens'dao`[P]]
      type `Jdbc Slick println Query Queens'q by Flow`[P <: JdbcProfile] = `Slick println Query Queens'q by Flow`[P, `Impl.Jdbc Slick Query Queens'dao`[P]]
      type `Impl.Jdbc Slick println Query Solutions'q by Flow`[P <: JdbcProfile] = `Impl.Slick println Query Solutions'q by Flow`[P, `Impl.Jdbc Slick Query Solutions'dao`[P]]
      type `Jdbc Slick println Query Solutions'q by Flow`[P <: JdbcProfile] = `Slick println Query Solutions'q by Flow`[P, `Impl.Jdbc Slick Query Solutions'dao`[P]]
    }

  }

}
