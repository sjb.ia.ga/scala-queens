package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package println

import _root_.slick.jdbc.JdbcProfile

import dao.`Slick Query Solutions'dao`

import conf.query.slick.{ Conf => Slick }

import conf.query.solutions.println.Conf


abstract trait `Slick println Query Solutions'q`[
  P <: Conf & Slick,
  JP <: JdbcProfile,
  DAO <: `Slick Query Solutions'dao`[JP]
](using
  override protected val * : P
) extends query.println.`println Query Solutions'q`[P]
    with `Slick Query Solutions'q`[P, JP, DAO] { this: slick.flow.`Slick Query *'q by Flow`[P, JP, DAO] =>

  override protected type * <: `Slick println Query Solutions'q`[P, JP, DAO]

}
