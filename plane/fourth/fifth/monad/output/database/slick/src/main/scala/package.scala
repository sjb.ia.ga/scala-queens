package object queens:

  import java.util.UUID

  import base.Board

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]


  type Simulate = dimensions.fifth.cycle.Simulate

  type IO = dimensions.fifth.cycle.IO


  type Identity_[A] = A

  type Iterable_[X[_], A] = Iterable[A]
