package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package flow

import _root_.slick.jdbc.JdbcProfile

import conf.query.slick.Conf


abstract trait `Slick Query *'q by Flow`[
  P <: Conf,
  JP <: JdbcProfile,
  DAO <: dao.`Slick Query *'dao`[JP]
](using
  override protected val * : P
) extends query.flow.`Query *'q by Flow`[P]
    with `Slick Query *'q`[P, JP, DAO]:

  override protected type * <: `Slick Query *'q by Flow`[P, JP, DAO]
