package queens
package version.v1
package vector
package breed
package query

import java.util.UUID
import java.util.concurrent.{ ConcurrentHashMap => Map }

import scala.concurrent.ExecutionContext

import common.Flag

import common.monad.Item

import conf.mdo.slick.Conf

import _root_.slick.jdbc.JdbcProfile

import dimensions.fourth.fifth.breed.query.slick.jdbc.dao.{
  `Impl.Jdbc Slick Query Rounds'dao`,
  `Impl.Jdbc Slick Query Squares'dao`,
  `Impl.Jdbc Slick Query Queens'dao`,
  `Impl.Jdbc Slick Query Solutions'dao`
}
import dimensions.fourth.fifth.breed.query.slick.jdbc.dao.delete.{
  `Impl.Jdbc Slick delete Query Rounds'dao`,
  `Impl.Jdbc Slick delete Query Squares'dao`,
  `Impl.Jdbc Slick delete Query Queens'dao`,
  `Impl.Jdbc Slick delete Query Solutions'dao`
}

import dimensions.fourth.fifth.breed.query.slick.jdbc.callback.flow.{
  `Impl.Jdbc Slick callback Query Rounds'q by Flow`,
  `Jdbc Slick callback Query Rounds'q by Flow`,
  `Impl.Jdbc Slick callback Query Squares'q by Flow`,
  `Jdbc Slick callback Query Squares'q by Flow`,
  `Impl.Jdbc Slick callback Query Queens'q by Flow`,
  `Jdbc Slick callback Query Queens'q by Flow`,
  `Impl.Jdbc Slick callback Query Solutions'q by Flow`,
  `Jdbc Slick callback Query Solutions'q by Flow`
}

import dimensions.fourth.fifth.breed.query.slick.jdbc.delete.flow.{
  `Impl.Jdbc Slick delete Query Rounds'q by Flow`,
  `Jdbc Slick delete Query Rounds'q by Flow`,
  `Impl.Jdbc Slick delete Query Squares'q by Flow`,
  `Jdbc Slick delete Query Squares'q by Flow`,
  `Impl.Jdbc Slick delete Query Queens'q by Flow`,
  `Jdbc Slick delete Query Queens'q by Flow`,
  `Impl.Jdbc Slick delete Query Solutions'q by Flow`,
  `Jdbc Slick delete Query Solutions'q by Flow`
}

import dimensions.fourth.fifth.breed.query.slick.jdbc.println.flow.{
  `Impl.Jdbc Slick println Query Rounds'q by Flow`,
  `Jdbc Slick println Query Rounds'q by Flow`,
  `Impl.Jdbc Slick println Query Squares'q by Flow`,
  `Jdbc Slick println Query Squares'q by Flow`,
  `Impl.Jdbc Slick println Query Queens'q by Flow`,
  `Jdbc Slick println Query Queens'q by Flow`,
  `Impl.Jdbc Slick println Query Solutions'q by Flow`,
  `Jdbc Slick println Query Solutions'q by Flow`
}

import dimensions.fifth.output.database.slick.jdbc.JdbcSlickDatabaseCallbacks

import version.less.nest.Nest


final class `Hatch'em, Spawn'it, Jdbc Slick Store'o Query'q`[
  P <: Conf,
  JP <: JdbcProfile
](
  _id: Long,
  _em: Iterable[Hive[P]],
  _tag: Any,
  _its: Seq[UUID] = UUID.randomUUID :: Nil
)(using
  override protected val * : P
)(
  override protected val fs: Item[*.T] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)] = new Map()
)(using
  _d: JdbcSlickDatabaseCallbacks[JP],
  _ec: ExecutionContext
) extends
    `Base.Hatch'em, Spawn'it, Slick Store'o Query'q`[P](_id, _em, _tag, _its):

  override protected type * = `Hatch'em, Spawn'it, Jdbc Slick Store'o Query'q`[P, JP]

  override def `#`(t: Any): * =
    new `Hatch'em, Spawn'it, Jdbc Slick Store'o Query'q`[P, JP](_id, _em, t, _its)(fs*)

  override protected def `apply°`(s: *.Item => Boolean*): * =
    new `Hatch'em, Spawn'it, Jdbc Slick Store'o Query'q`[P, JP](_id, _em, _tag, _its)(s*)

  import `Hatch'em, Spawn'it, Jdbc Slick Store'o Query'q`.given


  class `flow'q`(params: conf.mdo.slick.flow.Parameters)
      extends `By Flow Query`:

    override protected type `sq.P` = conf.query.slick.flow.squares.Parameters

    implicit override protected val `sq.P`: `sq.P` = {
      import conf.query.slick.flow.squares.Parameters.given
      params
    }

    override protected type `r.P` = conf.query.slick.flow.rounds.Parameters

    implicit override protected val `r.P` = {
      import conf.query.slick.flow.rounds.Parameters.given
      params
    }

    override protected type `q.P` = conf.query.slick.flow.queens.Parameters

    implicit override protected val `q.P` = {
      import conf.query.slick.flow.queens.Parameters.given
      params
    }

    override protected type `s.cb.P` = conf.query.slick.flow.solutions.callback.Parameters

    implicit override protected val `s.cb.P` = {
      import conf.query.slick.flow.solutions.callback.Parameters.given
      params
    }

    override object `callback'q`
        extends `By Flow callback Query`:

      override def squares(
        callback: `sq.P`.T => Boolean
      ): `Jdbc Slick callback Query Squares'q by Flow`[JP] =
        new `Impl.Jdbc Slick callback Query Squares'q by Flow`(
          callback.asInstanceOf[Any => Boolean],
          new `Impl.Jdbc Slick Query Squares'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def rounds(
        callback: `r.P`.T => Boolean,
        `rounds?`: Option[Iterable[Long]] = None
      ): `Jdbc Slick callback Query Rounds'q by Flow`[JP] =
        new `Impl.Jdbc Slick callback Query Rounds'q by Flow`(
          callback.asInstanceOf[Any => Boolean],
          `rounds?`,
          new `Impl.Jdbc Slick Query Rounds'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def queens(
        callback: `q.P`.T => Boolean,
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `Jdbc Slick callback Query Queens'q by Flow`[JP] =
        new `Impl.Jdbc Slick callback Query Queens'q by Flow`(
          callback.asInstanceOf[Any => Boolean],
          `nests?`, `rounds?`,
          new `Impl.Jdbc Slick Query Queens'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def solutions(
        callback: `s.cb.P`.T => Boolean,
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `Jdbc Slick callback Query Solutions'q by Flow`[JP] =
        new `Impl.Jdbc Slick callback Query Solutions'q by Flow`(
          callback.asInstanceOf[Any => Boolean],
          `nests?`, `rounds?`,
          new `Impl.Jdbc Slick Query Solutions'dao`(_id, _tag.toString)
        )(
          _tag
        )()


    override protected type `s.del.P` = conf.query.slick.flow.solutions.delete.Parameters

    implicit override protected val `s.del.P` = {
      import conf.query.slick.flow.solutions.delete.Parameters.given
      params
    }

    override object `delete'q`
        extends `By Flow delete Query`:

      override def squares(
      ): `Jdbc Slick delete Query Squares'q by Flow`[JP] =
        new `Impl.Jdbc Slick delete Query Squares'q by Flow`(
          new `Impl.Jdbc Slick delete Query Squares'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def rounds(
        `rounds?`: Option[Iterable[Long]] = None
      ): `Jdbc Slick delete Query Rounds'q by Flow`[JP] =
        new `Impl.Jdbc Slick delete Query Rounds'q by Flow`(
          `rounds?`,
          new `Impl.Jdbc Slick delete Query Rounds'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def queens(
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `Jdbc Slick delete Query Queens'q by Flow`[JP] =
        new `Impl.Jdbc Slick delete Query Queens'q by Flow`(
          `nests?`, `rounds?`,
          new `Impl.Jdbc Slick delete Query Queens'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def solutions(
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `Jdbc Slick delete Query Solutions'q by Flow`[JP] =
        new `Impl.Jdbc Slick delete Query Solutions'q by Flow`(
          `nests?`, `rounds?`,
          new `Impl.Jdbc Slick delete Query Solutions'dao`(_id, _tag.toString)
        )(
          _tag
        )()


    override protected type `s.co.P` = conf.query.slick.flow.solutions.println.Parameters

    implicit override protected val `s.co.P` = {
      import conf.query.slick.flow.solutions.println.Parameters.given
      params
    }

    override object `console'o'q`
        extends `By Flow println Query`:

      override def squares(
      ): `Jdbc Slick println Query Squares'q by Flow`[JP] =
        new `Impl.Jdbc Slick println Query Squares'q by Flow`(
          new `Impl.Jdbc Slick Query Squares'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def rounds(
        `rounds?`: Option[Iterable[Long]] = None
      ): `Jdbc Slick println Query Rounds'q by Flow`[JP] =
        new `Impl.Jdbc Slick println Query Rounds'q by Flow`(
          `rounds?`,
          new `Impl.Jdbc Slick Query Rounds'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def queens(
        `nests?`: Option[Iterable[Nest]] = None,
        `rounds?`: Option[Iterable[Long]] = None
      ): `Jdbc Slick println Query Queens'q by Flow`[JP] =
        new `Impl.Jdbc Slick println Query Queens'q by Flow`(
          `nests?`, `rounds?`,
          new `Impl.Jdbc Slick Query Queens'dao`(_id, _tag.toString)
        )(
          _tag
        )()

      override def solutions(
        `nests?`: Option[Iterable[Nest]],
        `rounds?`: Option[Iterable[Long]]
      ): `Jdbc Slick println Query Solutions'q by Flow`[JP] =
        new `Impl.Jdbc Slick println Query Solutions'q by Flow`(
          `nests?`, `rounds?`,
          new `Impl.Jdbc Slick Query Solutions'dao`(_id, _tag.toString)
        )(
          _tag
        )()


  override def `by Flow`: `By Flow Query` = * match

    case params: conf.mdo.slick.flow.Parameters => `flow'q`(params)

    case _ => ???


object `Hatch'em, Spawn'it, Jdbc Slick Store'o Query'q`:

  import dimensions.fifth.output.database.slick.jdbc.dao.JdbcSlickDatabaseDAO
  import dimensions.fifth.output.database.slick.SlickDatabaseCallbacks.Implicits.dcb2dao

  given dao[P <: JdbcProfile](using dcb: JdbcSlickDatabaseCallbacks[P]): JdbcSlickDatabaseDAO[P] = dcb2dao(dcb)

  import common.NoTag

  def apply[
    P <: Conf,
    JP <: JdbcProfile
  ](
    em: Iterable[Hive[P]]
  )(using
    params: P,
    dbCallbacks: JdbcSlickDatabaseCallbacks[JP],
    _ec: ExecutionContext
  ): `Hatch'em, Spawn'it, Store'o Query'q`[P] =
    val board = params.board
    new `Hatch'em, Spawn'it, Jdbc Slick Store'o Query'q`[P, JP](dbCallbacks(board), em, NoTag)(using params)()
