package queens
package version.v1
package vector
package breed
package query

import java.util.UUID
import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.Flag

import conf.mdo.slick.Conf

import dimensions.fifth.output.database.slick.SlickDatabaseCallbacks


abstract class `Base.Hatch'em, Spawn'it, Slick Store'o Query'q`[
  P <: Conf
](
  _id: Long,
  _em: Iterable[Hive[P]],
  _tag: Any,
  _its: Seq[UUID]
)(using
  override protected val * : P
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
)(using
  _d: SlickDatabaseCallbacks[?]
) extends
    `Base.Hatch'em, Spawn'it, Output'o`[P](_em, _tag, _its),
    `Hatch'em, Spawn'it, Store'o Query'q`[P],
    `Base.Hatch'em, Spawn'it, Slick Store'o`[P](_id):

  override protected type * <: `Base.Hatch'em, Spawn'it, Slick Store'o Query'q`[P]
