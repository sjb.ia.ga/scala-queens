package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package delete

import _root_.slick.jdbc.JdbcProfile

import dao.delete.`Slick delete Query Queens'dao`

import axis.fifth.output.database.Mappings.DbQueen

import conf.query.slick.{ Conf => Slick }

import conf.query.queens.Conf


abstract trait `Slick delete Query Queens'q`[
  P <: Conf & Slick,
  JP <: JdbcProfile,
  DAO <: `Slick delete Query Queens'dao`[JP]
](using
  override protected val * : P
) extends `Slick delete Query *'q`[DbQueen, P, JP, DAO]:

  override protected type * <: `Slick delete Query Queens'q`[P, JP, DAO]
