package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package delete
package flow

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import _root_.slick.jdbc.JdbcProfile

import conf.query.slick.flow.rounds.{ Parameters => P }

import common.Flag
import common.monad.Item


final class `Impl.Slick delete Query Rounds'q by Flow`[
  JP <: JdbcProfile,
  DAO <: dao.delete.`Slick delete Query Rounds'dao`[JP]
](using
  override protected val * : P
)(
  _rs: Option[Iterable[Long]],
  _dao: DAO
)(
  override protected val tag: Any
)(
  override protected val fs: Item[*.T] => Boolean*
)(using
  override protected val iterations: Seq[UUID] = Nil,
  override protected val `iterations*`: Map[UUID, (Boolean, Flag)] = new Map()
) extends
    `Base.Query Rounds'q`[P](_rs),
    `Base.Slick Query *'q`[P, JP, DAO](_dao),
    `Slick delete Query Rounds'q by Flow`[JP, DAO]:

  override protected type * = `Impl.Slick delete Query Rounds'q by Flow`[JP, DAO]

  override protected def `apply°`(s: *.Item => Boolean*): * =
    new `Impl.Slick delete Query Rounds'q by Flow`(_rs, _dao)(tag)(s*)


abstract trait `Slick delete Query Rounds'q by Flow`[
  JP <: JdbcProfile,
  DAO <: dao.delete.`Slick delete Query Rounds'dao`[JP]
](using
  override protected val * : P
) extends slick.delete.`Slick delete Query Rounds'q`[P, JP, DAO]
    with slick.flow.`Slick Query *'q by Flow`[P, JP, DAO]
    with `Slick Query Rounds'q`[P, JP, DAO]:

  override protected type * <: `Slick delete Query Rounds'q by Flow`[JP, DAO]
