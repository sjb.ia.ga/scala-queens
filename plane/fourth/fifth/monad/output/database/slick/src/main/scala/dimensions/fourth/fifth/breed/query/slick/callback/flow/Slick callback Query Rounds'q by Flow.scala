package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package callback
package flow

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import _root_.slick.jdbc.JdbcProfile

import conf.query.slick.flow.rounds.{ Parameters => P }

import common.Flag
import common.monad.Item


final class `Impl.Slick callback Query Rounds'q by Flow`[
  JP <: JdbcProfile,
  DAO <: dao.`Slick Query Rounds'dao`[JP]
](using
  override protected val * : P
)(
  _cb: Any => Boolean,
  _rs: Option[Iterable[Long]],
  _dao: DAO
)(
  override protected val tag: Any
)(
  override protected val fs: Item[*.T] => Boolean*
)(using
  override protected val iterations: Seq[UUID] = Nil,
  override protected val `iterations*`: Map[UUID, (Boolean, Flag)] = new Map()
) extends
    `Base.Query Rounds'q`[P](_rs),
    query.callback.`Base.callback Query *'q`[P](_cb),
    `Base.Slick Query *'q`[P, JP, DAO](_dao),
    `Slick callback Query Rounds'q by Flow`[JP, DAO]:

  override protected type * = `Impl.Slick callback Query Rounds'q by Flow`[JP, DAO]

  override protected def `apply°`(s: *.Item => Boolean*): * =
    new `Impl.Slick callback Query Rounds'q by Flow`(_cb, _rs, _dao)(tag)(s*)


abstract trait `Slick callback Query Rounds'q by Flow`[
  JP <: JdbcProfile,
  DAO <: dao.`Slick Query Rounds'dao`[JP]
](using
  override protected val * : P
) extends query.callback.`callback Query *'q`[P]
    with slick.flow.`Slick Query *'q by Flow`[P, JP, DAO]
    with `Slick Query Rounds'q`[P, JP, DAO]:

  override protected type * <: `Slick callback Query Rounds'q by Flow`[JP, DAO]
