package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package delete

import _root_.slick.jdbc.JdbcProfile

import dao.delete.`Slick delete Query Squares'dao`

import axis.fifth.output.database.Mappings.DbSquare

import conf.query.slick.{ Conf => Slick }

import conf.query.squares.Conf


abstract trait `Slick delete Query Squares'q`[
  P <: Conf & Slick,
  JP <: JdbcProfile,
  DAO <: `Slick delete Query Squares'dao`[JP]
](using
  override protected val * : P
) extends `Slick delete Query *'q`[DbSquare, P, JP, DAO]:

  override protected type * <: `Slick delete Query Squares'q`[P, JP, DAO]
