package queens
package dimensions.fourth.fifth
package breed
package query
package slick

import _root_.slick.jdbc.JdbcProfile

import dao.`Slick Query Queens'dao`

import conf.query.slick.{ Conf => Slick }

import conf.query.queens.Conf


abstract trait `Slick Query Queens'q`[
  P <: Conf & Slick,
  JP <: JdbcProfile,
  DAO <: `Slick Query Queens'dao`[JP]
](using
  override protected val * : P
) extends `Query Queens'q`[P]
    with `Slick Query *'q`[P, JP, DAO] { this: flow.`Slick Query *'q by Flow`[P, JP, DAO] =>

  override protected type * <: `Slick Query Queens'q`[P, JP, DAO]

  override protected def `map*`[R](
    block: ((QueryItem, Int)) => Option[R]
  ): Seq[Option[R]] = dao
    .mapQueens(nests, rounds)
    .zipWithIndex
    .map(block(_))

}
