package queens
package dimensions.fifth
package output.database
package slick

import scala.Function.const


abstract trait QueensSlickDatabaseOutput
    extends QueensDatabaseOutput:

  protected var dbCallbacks: SlickDatabaseCallbacks[?] = _
  protected var roundId: Long = _

  def apply(dbCallbacks: SlickDatabaseCallbacks[?], roundId: Long): Unit =
    this.dbCallbacks = dbCallbacks
    this.roundId = roundId

  final override protected val callback =
    { (solution: Solution, number: Long) => dbCallbacks(roundId, solution, number) }

  override def toString(): String = "Slick " + super.toString
