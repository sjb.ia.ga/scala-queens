package queens
package dimensions.fourth.fifth
package breed
package query
package slick

import _root_.slick.jdbc.JdbcProfile

import dao.`Slick Query *'dao`

import conf.query.slick.Conf


abstract trait `Base.Slick Query *'q`[
  P <: Conf,
  JP <: JdbcProfile,
  DAO <: `Slick Query *'dao`[JP]
](using
  override protected val * : P
)(
  override protected val dao: DAO
) extends `Slick Query *'q`[P, JP, DAO]:

  override protected type * <: `Base.Slick Query *'q`[P, JP, DAO]


abstract trait `Slick Query *'q`[
  P <: Conf,
  JP <: JdbcProfile,
  DAO <: dao.`Slick Query *'dao`[JP]
](using
  override protected val * : P
) extends `Query *'q`[P]:

  override protected type * <: `Slick Query *'q`[P, JP, DAO]

  protected val dao: DAO

  override def toString(): String = dao.toString() +  " Slick " + super.toString
