package queens
package dimensions.fourth.fifth
package breed
package query
package slick

import _root_.slick.jdbc.JdbcProfile

import dao.`Slick Query Solutions'dao`

import conf.query.slick.{ Conf => Slick }

import conf.query.solutions.Conf


abstract trait `Slick Query Solutions'q`[
  P <: Conf & Slick,
  JP <: JdbcProfile,
  DAO <: `Slick Query Solutions'dao`[JP]
](using
  override protected val * : P
) extends `Query Solutions'q`[P]
    with `Slick Query *'q`[P, JP, DAO] { this: flow.`Slick Query *'q by Flow`[P, JP, DAO] =>

  override protected type * <: `Slick Query Solutions'q`[P, JP, DAO]

  override protected def `map*`[R](
    block: ((QueryItem, Int)) => Option[R]
  ): Seq[Option[R]] = dao
    .mapSolutions(nests, rounds)
    .values
    .toSeq
    .zipWithIndex
    .map(block(_))

}
