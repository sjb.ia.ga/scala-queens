package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package delete

import _root_.slick.jdbc.JdbcProfile

import dao.delete.`Slick delete Query Solutions'dao`

import conf.query.slick.{ Conf => Slick }

import conf.query.solutions.delete.Conf


abstract trait `Slick delete Query Solutions'q`[
  P <: Conf & Slick,
  JP <: JdbcProfile,
  DAO <: `Slick delete Query Solutions'dao`[JP]
](using
  override protected val * : P
) extends query.delete.`delete Query Solutions'q`[P]
    with `Slick Query *'q`[P, JP, DAO] { this: slick.flow.`Slick Query *'q by Flow`[P, JP, DAO] =>

  override protected type * <: `Slick delete Query Solutions'q`[P, JP, DAO]

  import axis.fifth.output.database.Mappings.DbSolution

  import `Query Solutions'db`.given

  override protected val >> = query.delete.`delete Query *'q`[DbSolution](dao.delete)

}
