package queens
package dimensions.fourth.fifth
package breed
package query
package slick

import _root_.slick.jdbc.JdbcProfile

import dao.`Slick Query Rounds'dao`

import conf.query.slick.{ Conf => Slick }

import conf.query.rounds.Conf


abstract trait `Slick Query Rounds'q`[
  P <: Conf & Slick,
  JP <: JdbcProfile,
  DAO <: `Slick Query Rounds'dao`[JP]
](using
  override protected val * : P
) extends `Query Rounds'q`[P]
    with `Slick Query *'q`[P, JP, DAO] { this: flow.`Slick Query *'q by Flow`[P, JP, DAO] =>

  override protected type * <: `Slick Query Rounds'q`[P, JP, DAO]

  override protected def `map*`[R](
    block: ((QueryItem, Int)) => Option[R]
  ): Seq[Option[R]] = dao
    .mapRounds(rounds)
    .zipWithIndex
    .map(block(_))

}
