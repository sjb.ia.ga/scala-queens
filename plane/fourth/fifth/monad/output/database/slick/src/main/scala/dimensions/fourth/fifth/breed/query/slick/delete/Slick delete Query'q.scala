package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package delete

import _root_.slick.jdbc.JdbcProfile

import dao.delete.`Slick delete Query *'dao`

import conf.query.slick.Conf

import axis.fifth.output.database.Mappings.DbEntity


abstract trait `Slick delete Query *'q`[
  E <: DbEntity,
  P <: Conf,
  JP <: JdbcProfile,
  DAO <: `Slick delete Query *'dao`[E, JP]
](using
  override protected val * : P
) extends query.delete.`delete Query *'q`[P]
    with `Slick Query *'q`[P, JP, DAO]:

  override protected type * <: `Slick delete Query *'q`[E, P, JP, DAO]

  override protected val >> = query.delete.`delete Query *'q`[E](dao.delete)
