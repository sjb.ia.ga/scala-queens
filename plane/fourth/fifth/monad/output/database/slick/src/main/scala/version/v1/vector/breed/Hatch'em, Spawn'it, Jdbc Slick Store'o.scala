package queens
package version.v1
package vector
package breed

import java.util.UUID
import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.Flag
import common.monad.Item

import conf.mdo.slick.Conf

import _root_.slick.jdbc.JdbcProfile

import dimensions.fifth.output.database.slick.jdbc.JdbcSlickDatabaseCallbacks


final class `Hatch'em, Spawn'it, Jdbc Slick Store'o`[
  P <: Conf,
  JP <: JdbcProfile
](
  _id: Long,
  _em: Iterable[Hive[P]],
  _tag: Any,
  _its: Seq[UUID] = UUID.randomUUID :: Nil
)(using
  override protected val * : P
)(
  override protected val fs: *.Item => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)] = new Map()
)(using
  _d: JdbcSlickDatabaseCallbacks[JP]
) extends
    `Base.Hatch'em, Spawn'it, Output'o`[P](_em, _tag, _its),
    `Base.Hatch'em, Spawn'it, Slick Store'o`[P](_id):

  override protected type * = `Hatch'em, Spawn'it, Jdbc Slick Store'o`[P, JP]

  override def `#`(t: Any): * =
    new `Hatch'em, Spawn'it, Jdbc Slick Store'o`[P, JP](_id, _em, t, _its)(fs*)

  override protected def `apply°`(s: *.Item => Boolean*): * =
    new `Hatch'em, Spawn'it, Jdbc Slick Store'o`[P, JP](_id, _em, _tag, _its)(s*)

  override def toString(): String = "Jdbc " + super.toString


object `Hatch'em, Spawn'it, Jdbc Slick Store'o`:

  import common.NoTag

  def apply[
    P <: Conf,
    JP <: JdbcProfile
  ](
    em: Iterable[Hive[P]]
  )(
    using params: P,
    dbCallbacks: JdbcSlickDatabaseCallbacks[JP]
  ): `Hatch'em, Spawn'it, Output'o`[P] =
    val board = params.board
    new `Hatch'em, Spawn'it, Jdbc Slick Store'o`[P, JP](dbCallbacks(board), em, NoTag)(using params)()
