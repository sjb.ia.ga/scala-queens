package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package println
package flow

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import _root_.slick.jdbc.JdbcProfile

import base.Board

import conf.query.slick.flow.queens.{ Parameters => P }

import common.Flag
import common.monad.Item

import version.less.nest.Nest


final class `Impl.Slick println Query Queens'q by Flow`[
  JP <: JdbcProfile,
  DAO <: dao.`Slick Query Queens'dao`[JP]
](using
  override protected val * : P
)(
  _ns: Option[Iterable[Nest]],
  _rs: Option[Iterable[Long]],
  _dao: DAO
)(
  override protected val tag: Any
)(
  override protected val fs: Item[*.T] => Boolean*
)(using
  override protected val iterations: Seq[UUID] = Nil,
  override protected val `iterations*`: Map[UUID, (Boolean, Flag)] = new Map()
) extends
    `Base.Query Queens'q`[P](_ns, _rs),
    `Base.Slick Query *'q`[P, JP, DAO](_dao),
    `Slick println Query Queens'q by Flow`[JP, DAO]:

  override protected type * = `Impl.Slick println Query Queens'q by Flow`[JP, DAO]

  override protected def `apply°`(s: *.Item => Boolean*): * =
    new `Impl.Slick println Query Queens'q by Flow`(_ns, _rs, _dao)(tag)(s*)


abstract trait `Slick println Query Queens'q by Flow`[
  JP <: JdbcProfile,
  DAO <: dao.`Slick Query Queens'dao`[JP]
](using
  override protected val * : P
) extends query.println.`println Query *'q`[P]
    with slick.flow.`Slick Query *'q by Flow`[P, JP, DAO]
    with `Slick Query Queens'q`[P, JP, DAO]:

  override protected type * <: `Slick println Query Queens'q by Flow`[JP, DAO]
