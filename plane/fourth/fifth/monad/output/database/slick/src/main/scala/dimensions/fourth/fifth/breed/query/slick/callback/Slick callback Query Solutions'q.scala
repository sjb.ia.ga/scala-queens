package queens
package dimensions.fourth.fifth
package breed
package query
package slick
package callback

import _root_.slick.jdbc.JdbcProfile

import dao.`Slick Query Solutions'dao`

import conf.query.slick.{ Conf => Slick }

import conf.query.solutions.callback.Conf


abstract trait `Slick callback Query Solutions'q`[
  P <: Conf & Slick,
  JP <: JdbcProfile,
  DAO <: `Slick Query Solutions'dao`[JP]
](using
  override protected val * : P
) extends query.callback.`callback Query Solutions'q`[P]
    with `Slick Query Solutions'q`[P, JP, DAO] { this: slick.flow.`Slick Query *'q by Flow`[P, JP, DAO] =>

  override protected type * <: `Slick callback Query Solutions'q`[P, JP, DAO]

}
