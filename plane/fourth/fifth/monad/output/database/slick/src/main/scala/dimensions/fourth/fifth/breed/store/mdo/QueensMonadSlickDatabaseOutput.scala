package queens
package dimensions.fourth.fifth
package breed
package store
package mdo

import conf.mdo.slick.Conf

import dimensions.fourth.fifth.breed.mdo.QueensMonadDatabaseOutput
import dimensions.fifth.output.database.slick.QueensSlickDatabaseOutput


abstract trait QueensMonadSlickDatabaseOutput[
  P <: Conf
] extends QueensMonadDatabaseOutput[P]
    with QueensSlickDatabaseOutput
