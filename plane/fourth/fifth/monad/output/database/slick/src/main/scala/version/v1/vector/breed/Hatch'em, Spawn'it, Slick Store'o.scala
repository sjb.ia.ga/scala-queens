package queens
package version.v1
package vector
package breed

import common.monad.Item

import conf.mdo.slick.Conf

import dimensions.fifth.output.database.slick.SlickDatabaseCallbacks


abstract trait `Base.Hatch'em, Spawn'it, Slick Store'o`[
  P <: Conf
](
  protected val boardId: Long
)(using
  override protected val * : P
)(using
  protected val dbCallbacks: SlickDatabaseCallbacks[?]
) extends `Hatch'em, Spawn'it, Store'o`[P]:

  override protected type * <: `Base.Hatch'em, Spawn'it, Slick Store'o`[P]

  override protected def apply[R](
    it: *.Item,
    block: *.Item => R
  ): R =
    var queenId = 0L
    it.the { queens =>
      queenId = dbCallbacks(queens)
      false
    }
    val roundId = dbCallbacks(queenId
                             ,tag.toString
                             ,queenId
                             ,*.`Slick Store'o Round`)
    it(dbCallbacks, roundId)
    block(it)

  override def toString(): String = "Slick " + super.toString
