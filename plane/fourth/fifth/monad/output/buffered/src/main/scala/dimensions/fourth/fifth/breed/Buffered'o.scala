package queens
package dimensions.fourth.fifth
package breed

import java.util.UUID

import conf.mbo.Conf

import mbo.QueensMonadBufferedOutput


abstract trait `Buffered'o`[
  P <: Conf
](using
  override protected val * : P
) extends QueensMonadBufferedOutput[P]
    with `Output *'o`[P]:

  override protected type * <: `Buffered'o`[P]

  override def toString(): String = "Buffered'o " + super.toString


/**
  * @see [[queens.version.patch.v1.mbo.MonadBO]] @{code case Output(Buffered) => `Buffered'o`(model)}
  */
object `Buffered'o`
    extends Spawner:

  import java.util.concurrent.{ ConcurrentHashMap => Map }

  import dimensions.Dimension.Model
  import Model.{ Flow, Parallel, Messaging }
  import Parallel.{ Actors, Futures }
  import Messaging.{ Kafka, RabbitMQ, AWSSQS }

  import common.Flag

  import common.pipeline.Context
  import dimensions.fifth.QueensUseSolution

  override def apply[
    P <: conf.emito.Conf
  ](model: Model
  )(using
    params: P
  ): Spawn[P] = new Spawn[P] {

    override def apply(next: Option[Context => QueensUseSolution]): * =

      new * :

        override protected def `apply*`(queens: *.Q,
                                        tag: Any,
                                        its: Seq[UUID],
                                        dryrun: Either[Simulate, Boolean]
        ): *.T =
          assert(model eq queens.model)

          given Map[UUID, (Boolean, Flag)] = new Map()

          ((model, params) match

            case (Parallel(Actors), p: conf.mbo.actors.Parameters) =>

              new actors.`Impl.Buffered'o by Actors`(using p)(queens, tag, its, dryrun)(next)

            case (Parallel(Futures), p: conf.mbo.futures.Parameters) =>

              new futures.`Impl.Buffered'o by Futures`(using p)(queens, tag, its, dryrun)(next)

            case (Messaging(Kafka), p: conf.mbo.kafka.Parameters) =>

              new kafka.`Impl.Buffered'o by Kafka`(using p)(queens, tag, its, dryrun)(next)

            case (Messaging(RabbitMQ), p: conf.mbo.rabbitMQ.Parameters) =>

              new rabbitMQ.`Impl.Buffered'o by RabbitMQ`(using p)(queens, tag, its, dryrun)(next)

            case (Messaging(AWSSQS), p: conf.mbo.awsSQS.Parameters) =>

              new awsSQS.`Impl.Buffered'o by AWSSQS`(using p)(queens, tag, its, dryrun)(next)

            case (Flow, p: conf.mbo.flow.Parameters) =>

              new flow.`Impl.Buffered'o by Flow`(using p)(queens, tag, its, dryrun)(next)

          ).asInstanceOf[*.T]

    }
