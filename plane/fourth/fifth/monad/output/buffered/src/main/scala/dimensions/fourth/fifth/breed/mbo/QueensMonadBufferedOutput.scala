package queens
package dimensions.fourth.fifth
package breed
package mbo

import conf.mbo.Conf

import axis.fifth.output.buffered.QueensBufferedOutput


abstract trait QueensMonadBufferedOutput[
  P <: Conf
] extends QueensMonadOutput[P]
    with QueensBufferedOutput
