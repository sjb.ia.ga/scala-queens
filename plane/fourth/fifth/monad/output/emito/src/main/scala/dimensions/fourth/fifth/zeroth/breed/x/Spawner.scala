package queens
package dimensions.fourth.fifth
package zeroth
package breed
package x

import cats.effect.Sync

import conf.emito.`0`.x.Conf


/**
  * @see [[queens.dimensions.fourth.fifth.zeroth.breed.x.sync.`I'o`]]
  * @see [[queens.dimensions.fourth.fifth.zeroth.breed.x.async.`I'o`]]
  */
abstract trait Spawner[E[_]: Sync]:

  import dimensions.Dimension.Model

  def apply[
    P <: Conf[E]
  ](model: Model
  )(using
    params: P
  ): Spawn[E, P]
