package queens
package version.v0
package vector
package breed
package s

import conf.emito.`0`.s.Conf

import dimensions.fourth.fifth.zeroth.breed.s.Spawn

import dimensions.three.breed.Hatch


class Hive[
  P <: Conf
] private (using
  protected val params: P
)(
  val spawn: Spawn[P]
)(
  val hatch: Hatch[params.Q],
  val `spawn*`: spawn.* = spawn(None)
)


object Hive:

  import common.pipeline.Context
  import dimensions.fifth.QueensUseSolution

  def apply[
    P <: Conf
  ](using
    params: P
  )(hatch: Hatch[params.Q],
    spawn: Spawn[P]
  )(next: Option[Context => QueensUseSolution] = None
  ) = new Hive[P](using params)(spawn)(hatch, spawn(next))
