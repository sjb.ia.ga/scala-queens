package queens
package dimensions.fourth.fifth
package breed
package rabbitMQ

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import conf.emito.rabbitMQ.Conf

import common.Flag
import common.monad.Item
import dimensions.third.first.rabbitMQ.{ Iteration, `Queens*` }

import dimensions.third.Queens

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution


abstract class `Base.Output *'o by RabbitMQ`[
  P <: Conf
] private (
  _qs: `Queens*`[?],
  _tag: Any,
  _its: Seq[UUID],
  _run: Either[Simulate, Boolean],
  _fs: Item[Solution] => Boolean*
)(
  override protected val next: Option[Context => QueensUseSolution]
)(using
  override protected val * : P
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.Output *'o`[P](_tag, _its, _run, _fs*)
    with `Output *'o by RabbitMQ`[P]:

  protected def this(
    _qs: dimensions.third.Queens[?],
    _tag: Any,
    _its: Seq[UUID],
    _run: Either[Simulate, Boolean],
    _fs: Item[Solution] => Boolean*
  )(
    _next: Option[Context => QueensUseSolution]
  )(using
    `_*`: P
  )(using
    `_its*`: Map[UUID, (Boolean, Flag)]
  ) = this(_qs.asInstanceOf[`Queens*`[?]], _tag, _its, _run, _fs*)(_next)

  override protected val queens: Q =
    init(_qs)


abstract trait `Output *'o by RabbitMQ`[
  P <: Conf
](using
  override protected val * : P
) extends `Output *'o`[P]:

  override protected type It = Iteration

  override protected type Q = `Queens*`[?]

  override protected type T56 = String

  override protected def apply(flg: Flag, end: () => Unit)(using preemption: Boolean): It =
    Iteration(*.board, tag, preemption, dryrun, flg, end, *.it)

  override def toString(): String = "RabbitMQ " + super.toString
