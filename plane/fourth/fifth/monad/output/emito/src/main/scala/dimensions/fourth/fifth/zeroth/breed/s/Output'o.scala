package queens
package dimensions.fourth.fifth
package zeroth
package breed
package s

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import cats.effect.IO

import fs2.Stream

import conf.emito.`0`.s.Conf

import common.monad.Item
import common.monad.`0`.`MonadWithFilter'`

import dimensions.fourth.fifth.breed.{ `Base.Breed *'"`, `Breed *'"` }

import dimensions.third.zeroth.Queens

import common.Flag


abstract class `Base.Output *'o`[
  P <: Conf
] protected (
  override protected val tag: Any,
  _its: Seq[UUID],
  override protected val fs: Item[Solution] => Boolean*
)(using
  override protected val * : P
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.Breed *'"`[Solution](_its)
    with `Output *'o`[P]:

  override protected type * <: `Base.Output *'o`[P]

  protected def init(qs: Q): Q =
    fs.foldLeft(qs)(_.withFilter(_).asInstanceOf[Q])


abstract trait `Output *'o`[
  P <: Conf
](using
  override protected val * : P
) extends zeroth.s.QueensMonadOutput[P]
    with `MonadWithFilter'`[IO, Stream, Solution, Long]
    with `Breed *'"`[Solution]:

  override protected type * <: `Output *'o`[P]

  protected type Q <: Queens[IO, Stream, ?, It]

  import scala.Function.const

  import Item.given

  import common.pipeline.Context.Round

  override def apply[E](it: Item[Solution])(expr: (UUID, java.lang.Long) ?=> E): Option[E] =
    `just get*`(it) {
      case false => const(None)
      case _ => {
        case _ if !it => None
        case (_, (board, tag, uuid, n, _, _)) if it.isEmpty || ||(it, Round(board, tag, queens, n), Some(uuid)) =>
          Some(expr(using uuid, n))
        case _ =>
          None
      }
    }

  final override def stop(it: Item[?]): Boolean = ???


  protected def apply(flg: Flag, end: () => Unit): It
  protected def apply[R](r: R)(using It): R = r

  protected def `for*`[T, R](block: Item[Solution] => T)
                            (fun: (Item[Solution] => T) => (Long, It) ?=> R)
                            (using Long): R =
    val id = UUID.randomUUID
    val flg = `flag*`(id)

    `iterations*`.put(id, (true, flg))

    given It = this(flg, { () => `iterations*`.remove(id) })

    fun(block)

  inline override def foreach(block: Item[Solution] => Unit)
                             (using Long): Stream[IO, Unit] =
    `for*`(block)(queens.foreach)

  inline override def map[R](block: Item[Solution] => R)
                            (using Long): Stream[IO, R] =
    `for*`(block)(queens.map)

  inline override def flatMap[R](block: Item[Solution] => Stream[IO, R])
                                (using Long): Stream[IO, R] =
    `for*`(block)(queens.flatMap)


  override def toString(): String = "Output'o :: " + super.toString
