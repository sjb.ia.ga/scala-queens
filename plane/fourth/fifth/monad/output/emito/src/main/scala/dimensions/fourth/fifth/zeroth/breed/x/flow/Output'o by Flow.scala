package queens
package dimensions.fourth.fifth
package zeroth
package breed
package flow
package x

import java.util.UUID
import java.util.Date

import java.util.concurrent.{ ConcurrentHashMap => Map }

import cats.effect.Sync

import conf.emito.`0`.x.flow.Conf

import common.Flag
import common.monad.Item
import dimensions.third.first.zeroth.flow.Iteration0

import dimensions.third.Queens

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution

import dimensions.third.first.zeroth.flow.x.`Queens*`


abstract class `Base.Output *'o by Flow`[E[_]: Sync,
  P <: Conf[E]
] private (
  _qs: `Queens*`[E, ?],
  _tag: Any,
  _its: Seq[UUID],
  _fs: Item[Solution] => Boolean*
)(
  override protected val next: Option[Context => QueensUseSolution]
)(using
  override protected val * : P
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends breed.x.`Base.Output *'o`[E, P](_tag, _its, _fs*)
    with `Output *'o by Flow`[E, P]:

  protected def this(
    _qs: Queens[?],
    _tag: Any,
    _its: Seq[UUID],
    _fs: Item[Solution] => Boolean*
  )(
    _next: Option[Context => QueensUseSolution]
  )(using
    `_*`: P
  )(using
    `_its*`: Map[UUID, (Boolean, Flag)]
  ) = this(_qs.asInstanceOf[`Queens*`[E, ?]], _tag, _its, _fs*)(_next)

  override protected val queens: Q =
    init(_qs)


abstract trait `Output *'o by Flow`[E[_]: Sync,
  P <: Conf[E]
](using
  override protected val * : P
) extends breed.x.`Output *'o`[E, P]:

  override protected type It = Iteration0

  override protected type Q = `Queens*`[E, ?]

  override protected type T56 = Date

  override protected def apply(flg: Flag, end: () => Unit): It =
    Iteration0(*.board, tag, flg, end)

  override def toString(): String = "Flow " + super.toString
