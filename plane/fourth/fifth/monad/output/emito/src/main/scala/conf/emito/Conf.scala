package queens
package conf.emito

import base.Board


abstract trait Conf
    extends conf.monad.Conf:

  def board: Board

  import dimensions.third.`Queens*`

  protected type It <: AnyVal

  type Q <: `Queens*`[?, It]

  import dimensions.fourth.fifth.breed.`Output *'o`

  type P <: Conf

  override type T <: `Output *'o`[P]


package `0`:

  import java.nio.file.Path

  abstract trait Conf
      extends conf.monad.Conf:

    def board: Board

    def tmpdir: Path

    protected type It <: AnyVal

    type P <: Conf

    import dimensions.third.zeroth.Queens

    type Q <: Queens[?, ?, ?, It]

  package x:

    import cats.effect.Sync

    abstract trait Conf[E[_]: Sync]
        extends `0`.Conf:

      import dimensions.fourth.fifth.zeroth.breed.x.`Output *'o`

      override type P <: Conf[E]

      override type T <: `Output *'o`[E, P]

  package s:

    abstract trait Conf
        extends `0`.Conf:

      import dimensions.fourth.fifth.zeroth.breed.s.`Output *'o`

      override type P <: Conf

      override type T <: `Output *'o`[P]
