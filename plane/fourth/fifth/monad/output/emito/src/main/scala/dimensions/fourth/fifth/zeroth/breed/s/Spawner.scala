package queens
package dimensions.fourth.fifth
package zeroth
package breed
package s

import conf.emito.`0`.s.Conf


/**
  * @see [[queens.dimensions.fourth.fifth.zeroth.breed.fs.`I'o`]]
  */
abstract trait Spawner:

  import dimensions.Dimension.Model

  def apply[
    P <: Conf
  ](model: Model
  )(using
    params: P
  ): Spawn[P]
