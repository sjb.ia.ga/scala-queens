package queens
package version.v1
package vector
package breed

import conf.emito.Conf

import dimensions.three.breed.Hatch

import dimensions.fourth.fifth.breed.Spawn


class Hive[
  P <: Conf
] private (using
  protected val params: P
)(
  val spawn: Spawn[P]
)(
  val hatch: Hatch[params.Q],
  val `spawn*`: spawn.* = spawn(None)
)


object Hive:

  import common.pipeline.Context
  import dimensions.fifth.QueensUseSolution

  def apply[
    P <: Conf
  ](using
    params: P
  )(hatch: Hatch[params.Q],
    spawn: Spawn[P]
  )(next: Option[Context => QueensUseSolution] = None
  ) = new Hive[P](using params)(spawn)(hatch, spawn(next))
