package queens
package dimensions.fourth.fifth
package breed

import java.util.UUID

import scala.Function.const

import base.breed.Breed

import conf.emito.Conf

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution

import dimensions.third.Queens


abstract trait Spawn[
  P <: Conf
](using
  protected val * : P
) extends Breed:

  def apply(next: Option[Context => QueensUseSolution] = None): *

  type * = $.`Spawn*`

  object $:

    abstract trait `Spawn*`:

      @inline def apply[
        Q <: Queens[?],
        O <: `Output *'o`[P]
      ](queens: Q,
        tag: Any,
        its: Seq[UUID],
        dryrun: Either[Simulate, Boolean] = Right(false)
      ): O = `apply*`(queens.asInstanceOf[*.Q], tag, its, dryrun)
        .asInstanceOf[O]

      protected def `apply*`(queens: *.Q,
                             tag: Any,
                             iterations: Seq[UUID],
                             dryrun: Either[Simulate, Boolean]
      ): *.T
