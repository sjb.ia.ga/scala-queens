package queens
package version.v1
package vector
package breed

import java.util.UUID
import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.Flag
import common.Flag.given
import common.given

import common.Macros.tryc

import common.monad.`MonadWithFilter'°`

import conf.emito.Conf

import dimensions.fourth.fifth.breed.{ `Base.Breed *'°`, `Breed *'°` }

import dimensions.three.breed.Hatch
import Hatch.given


abstract class `Base.Hatch'em, Spawn'it, Output'o`[
  P <: Conf
](
  override protected val em: Iterable[Hive[P]],
  override protected val tag: Any,
  _its: Seq[UUID]
)(using
  override protected val * : P
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.Breed *'°`[P](_its)
    with `Hatch'em, Spawn'it, Output'o`[P]:

  override protected type * <: `Base.Hatch'em, Spawn'it, Output'o`[P]


abstract trait `Hatch'em, Spawn'it, Output'o`[
  P <: Conf
](using
  override protected val * : P
) extends `MonadWithFilter'°`[P, Boolean]
    with `Breed *'°`[P]:

  override protected type * <: `Hatch'em, Spawn'it, Output'o`[P]

  override protected type T56 = Unit

  protected val em: Iterable[Hive[P]]

  protected def apply[R](
    it: *.Item,
    block: *.Item => R
  ): R

  override protected def `for*`[R](block: *.Item => R)(using
                                   preemption: Boolean): Iterable[R] =
    val id = UUID.randomUUID
    val flg = `flag*`(id)

    try
      `iterations*`.put(id, (true, flg))

      val `block°` = `apply°`(block)

      val board = *.board

      val r = em
        .zipWithIndex
        .map { case (hive, n) =>
          if flg then
            val hatch = hive.hatch
            val spawn = hive.`spawn*`

            val vi = (board, tag, UUID.randomUUID, n+1L, (), ())

            if preemption then
              val o: *.T = spawn(hatch(), tag, iterations, Right(preemption))
              this(new *.Item(o, flg.slot), block)

            val o: *.T = spawn(hatch(), tag, iterations)
            this(new *.Item(o, flg.slot, Some(vi)), `block°`)
          else
            None
        }
      r
    finally
      tryc(`iterations*`.remove(id, (true, flg)))

  def foreach(block: *.Item => Unit)(using
              preemption: Boolean = false): Iterable[Unit] =
    `for*`(block)

  def map[R](block: *.Item => R)(using
             preemption: Boolean = false): Iterable[R] =
    `for*`(block)

  def flatMap[R](block: *.Item => Iterable[R])(using
                 preemption: Boolean = false): Iterable[R] =
    `for*`(block).flatten


  override def toString(): String = s"Output Hive [ #${em.size} Nest${if em.size == 1 then "" else "s"} ]"
