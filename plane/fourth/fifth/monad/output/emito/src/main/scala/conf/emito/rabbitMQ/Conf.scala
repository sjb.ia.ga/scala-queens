package queens
package conf.emito.rabbitMQ


abstract trait Conf
    extends conf.emito.Conf
    with conf.rabbitMQ.Conf:

  import dimensions.third.first.rabbitMQ.{ Iteration, `Queens*` }

  override protected type It = Iteration

  override type Q = `Queens*`[?]
