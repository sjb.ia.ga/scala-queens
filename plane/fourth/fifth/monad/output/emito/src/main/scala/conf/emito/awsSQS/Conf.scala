package queens
package conf.emito.awsSQS


abstract trait Conf
    extends conf.emito.Conf
    with conf.awsSQS.Conf:

  import dimensions.third.first.awsSQS.{ Iteration, `Queens*` }

  override protected type It = Iteration

  override type Q = `Queens*`[?]
