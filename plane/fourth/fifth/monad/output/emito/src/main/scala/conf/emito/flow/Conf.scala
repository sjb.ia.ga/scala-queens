package queens
package conf.emito


package flow:

  abstract trait Conf
      extends conf.emito.Conf
      with conf.flow.Conf:

    import dimensions.third.first.flow.{ Iteration, `Queens*` }

    override protected type It = Iteration

    override type Q = `Queens*`[?]


package `0`:

  package x:

    import cats.effect.Sync

    package flow:

      abstract trait Conf[E[_]: Sync]
          extends conf.emito.`0`.x.Conf[E]
          with conf.flow.Conf:

        import dimensions.third.first.zeroth.flow.Iteration0

        override protected type It = Iteration0

        override type P <: Conf[E]

        import dimensions.third.first.zeroth.flow.x.`Queens*`

        override type Q = `Queens*`[E, ?]

  package s:

    package flow:

      abstract trait Conf
          extends conf.emito.`0`.s.Conf
          with conf.flow.Conf:

        import dimensions.third.first.zeroth.flow.Iteration0

        override protected type It = Iteration0

        override type P <: Conf

        import dimensions.third.first.zeroth.flow.s.`Queens*`

        override type Q = `Queens*`[?]
