package queens
package version.v0
package vector
package breed
package x

import cats.effect.Sync

import conf.emito.`0`.x.Conf

import dimensions.fourth.fifth.zeroth.breed.x.Spawn

import dimensions.three.breed.Hatch


class Hive[E[_]: Sync,
  P <: Conf[E]
] private (using
  protected val params: P
)(
  val spawn: Spawn[E, P]
)(
  val hatch: Hatch[params.Q],
  val `spawn*`: spawn.* = spawn(None)
)


object Hive:

  import monix.eval.{ Coeval, Task }

  import common.pipeline.Context
  import dimensions.fifth.QueensUseSolution

  object sync:

    def apply[
      P <: Conf[Coeval]
    ](using
      params: P
    )(hatch: Hatch[params.Q],
      spawn: Spawn[Coeval, P]
    )(next: Option[Context => QueensUseSolution] = None
    ) = new Hive[Coeval, P](using params)(spawn)(hatch, spawn(next))

  object async:

    def apply[
      P <: Conf[Task]
    ](using
      params: P
    )(hatch: Hatch[params.Q],
      spawn: Spawn[Task, P]
    )(next: Option[Context => QueensUseSolution] = None
    ) = new Hive[Task, P](using params)(spawn)(hatch, spawn(next))
