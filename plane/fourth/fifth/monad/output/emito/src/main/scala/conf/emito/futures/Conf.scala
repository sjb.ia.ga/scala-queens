package queens
package conf.emito.futures

import scala.concurrent.ExecutionContext

import conf.futures.Callbacks


abstract trait Conf
    extends conf.emito.Conf
    with conf.futures.Conf:

  import dimensions.third.first.futures.{ Iteration, `Queens*` }

  override protected type It = Iteration

  override type Q = `Queens*`[?]
