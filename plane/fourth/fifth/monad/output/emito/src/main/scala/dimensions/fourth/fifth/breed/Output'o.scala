package queens
package dimensions.fourth.fifth
package breed

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.monad.{ Item, `MonadWithFilter"` }

import dimensions.third.`Queens*`

import conf.emito.Conf

import common.Flag


abstract class `Base.Output *'o`[
  P <: Conf
] protected (
  override protected val tag: Any,
  _its: Seq[UUID],
  override protected val dryrun: Either[Simulate, Boolean],
  override protected val fs: Item[Solution] => Boolean*
)(using
  override protected val * : P
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.Breed *'"`[Solution](_its)
    with `Output *'o`[P]:

  override protected type * <: `Base.Output *'o`[P]

  protected def init(qs: Q): Q =
    fs.foldLeft(qs)(_.withFilter(_).asInstanceOf[Q])


abstract trait `Output *'o`[
  P <: Conf
](using
  override protected val * : P
) extends QueensMonadOutput[P]
    with `MonadWithFilter"`[Solution, Long, Boolean]
    with `Breed *'"`[Solution]:

  override protected type * <: `Output *'o`[P]

  protected type Q <: `Queens*`[?, It]

  protected val dryrun: Either[Simulate, Boolean]

  import scala.Function.const

  import Item.given

  import common.pipeline.Context.Round

  override def apply[E](it: Item[Solution])(expr: (UUID, java.lang.Long) ?=> E): Option[E] =
    `just get*`(it) {
      case false => const(None)
      case _ => {
        case _ if !it => None
        case (_, (board, tag, uuid, n, _, _)) if it.isEmpty || ||(it, Round(board, tag, queens, n), Some(uuid)) =>
          val r = expr(using uuid, n)
          r match
            case false => this stop it
            case _ =>
          Some(r)
        case _ =>
          this stop it
          None
      }
    }

  protected def apply(flg: Flag, end: () => Unit)(using Boolean): It
  protected def apply[R](r: R)(using It): R = r

  private def `for*`[T, R](block: Item[Solution] => T)
                          (fun: (Item[Solution] => T) => (Long, It) ?=> R)
                          (using Long, Boolean): R =
    val id = UUID.randomUUID
    val flg = `flag*`(id)

    `iterations*`.put(id, (true, flg))

    given It = this(flg, { () => `iterations*`.remove(id) })

    fun(block)

  inline override def foreach(block: Item[Solution] => Unit)
                             (using Long, Boolean): Iterable[Unit] =
    `for*`(block)(queens.foreach)

  inline override def map[R](block: Item[Solution] => R)
                            (using Long, Boolean): Iterable[R] =
    `for*`(block)(queens.map)

  inline override def flatMap[R](block: Item[Solution] => Iterable[R])
                                (using Long, Boolean): Iterable[R] =
    `for*`(block)(queens.flatMap)


  override def toString(): String = "Output'o :: " + super.toString
