package queens
package conf.emito.kafka


abstract trait Conf
    extends conf.emito.Conf
    with conf.kafka.Conf:

  import dimensions.third.first.kafka.{ Iteration, `Queens*` }

  override protected type It = Iteration

  override type Q = `Queens*`[?]
