package queens
package dimensions.fourth.fifth
package breed

import conf.emito.Conf


/**
  *
  * @see [[queens.dimensions.fourth.fifth.breed.`Console'o`]]
  * @see [[queens.dimensions.fourth.fifth.breed.`Buffered'o`]]
  * @see [[queens.dimensions.fourth.fifth.breed.store.`Slick Store'o`]]
  * @see [[queens.dimensions.fourth.fifth.breed.store.`Scalikejdbc Store'o`]]
  *
  */
abstract trait Spawner:

  import dimensions.Dimension.Model

  def apply[
    P <: Conf
  ](model: Model
  )(using
    params: P
  ): Spawn[P]
