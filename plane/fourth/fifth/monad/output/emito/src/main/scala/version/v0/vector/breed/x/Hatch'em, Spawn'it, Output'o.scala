package queens
package version.v0
package vector
package breed
package flow
package x

import java.util.UUID
import java.util.concurrent.{ ConcurrentHashMap => Map }

import cats.effect.Sync

import monix.tail.Iterant
import Iterant.{ fromIterable, resource }

import breed.x.Hive

import conf.emito.`0`.x.Conf

import common.Flag
import common.Flag.given
import common.given

import common.monad.Item
import common.monad.`0`.`MonadWithFilter'°`

import dimensions.fourth.fifth.breed.{ `Base.Breed *'°`, `Breed *'°` }

import dimensions.three.breed.Hatch
import Hatch.given



abstract class `Base.Hatch'em, Spawn'it, Output'o`[E[_]: Sync,
  P <: Conf[E]
](
  override protected val em: Iterable[Hive[E, P]],
  override protected val tag: Any,
  _its: Seq[UUID]
)(using
  override protected val * : P
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.Breed *'°`[P](_its)
    with `Hatch'em, Spawn'it, Output'o`[E, P]:

  override protected type * <: `Base.Hatch'em, Spawn'it, Output'o`[E, P]


abstract trait `Hatch'em, Spawn'it, Output'o`[E[_]: Sync,
  P <: Conf[E]
](using
  override protected val * : P
) extends `MonadWithFilter'°`[E, Iterant, P, Long]
    with `Breed *'°`[P]:

  override protected type * <: `Hatch'em, Spawn'it, Output'o`[E, P]

  override protected type T56 = Unit

  protected val em: Iterable[Hive[E, P]]

  private def `for*`(using Long): Iterant[E, *.Item] =
    ( resource {
        val id = UUID.randomUUID
        val flg = `flag*`(id)
        `iterations*`.put(id, (true, flg))
        Sync[E].pure(id -> flg)
      } { (id, flg) =>
        Sync[E].pure(`iterations*`.remove(id, (true, flg)))
      }
    ).flatMap { (id, flg) =>
      fromIterable(em)
        .zipWithIndex
        .map { case (hive, n) =>
          val hatch = hive.hatch
          val spawn = hive.`spawn*`

          val o: *.T = spawn(hatch(), tag, iterations)

          val vi = (*.board, tag, UUID.randomUUID, n+1L, (), ())

          new *.Item(o, flg.slot, Some(vi))
        }
    }

  def foreach(block: *.Item => Unit)(using Long): Iterant[E, Unit] =
    val `block°` = `apply°`(block)
    `for*`
      .map(`block°`)
      .filter(_ ne None)
      .map(_.get)

  def map[R](block: *.Item => R)(using Long): Iterant[E, R] =
    val `block°` = `apply°`(block)
    `for*`
      .map(`block°`)
      .filter(_ ne None)
      .map(_.get)

  def flatMap[R](block: *.Item => Iterant[E, R])(using Long): Iterant[E, R] =
    val `block°` = `apply°`(block)
    `for*`
      .map(`block°`)
      .filter(_ ne None)
      .map(_.get)
      .flatten


  override def toString(): String = s"Output Monix Hive [ #${em.size} Nest${if em.size == 1 then "" else "s"} ]"
