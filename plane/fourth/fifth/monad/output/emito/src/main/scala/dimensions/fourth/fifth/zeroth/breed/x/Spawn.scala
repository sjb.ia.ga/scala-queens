package queens
package dimensions.fourth.fifth
package zeroth
package breed
package x

import java.util.UUID

import scala.Function.const

import cats.effect.Sync

import base.breed.Breed

import conf.emito.`0`.x.Conf

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution

import dimensions.third.Queens


abstract trait Spawn[E[_]: Sync,
  P <: Conf[E]
](using
  protected val * : P
) extends Breed:

  def apply(next: Option[Context => QueensUseSolution] = None): *

  type * = $.`Spawn*`

  object $:

    abstract trait `Spawn*`:

      @inline def apply[
        Q <: Queens[?],
        O <: `Output *'o`[E, P]
      ](queens: Q,
        tag: Any,
        its: Seq[UUID]
      )(using Long): O = `apply*`(queens.asInstanceOf[*.Q], tag, its)
        .asInstanceOf[O]

      protected def `apply*`(queens: *.Q,
                             tag: Any,
                             iterations: Seq[UUID]
      )(using Long): *.T
