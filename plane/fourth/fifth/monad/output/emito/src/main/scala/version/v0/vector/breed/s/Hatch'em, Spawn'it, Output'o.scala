package queens
package version.v0
package vector
package breed
package flow
package s

import java.util.UUID
import java.util.concurrent.{ ConcurrentHashMap => Map }

import cats.effect.IO

import fs2.Stream
import Stream.{ bracket, emits }

import breed.s.Hive

import conf.emito.`0`.s.Conf

import common.Flag
import common.Flag.given
import common.given

import common.monad.Item
import common.monad.`0`.`MonadWithFilter'°`

import dimensions.fourth.fifth.breed.{ `Base.Breed *'°`, `Breed *'°` }

import dimensions.three.breed.Hatch
import Hatch.given



abstract class `Base.Hatch'em, Spawn'it, Output'o`[
  P <: Conf
](
  override protected val em: Iterable[Hive[P]],
  override protected val tag: Any,
  _its: Seq[UUID]
)(using
  override protected val * : P
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.Breed *'°`[P](_its)
    with `Hatch'em, Spawn'it, Output'o`[P]:

  override protected type * <: `Base.Hatch'em, Spawn'it, Output'o`[P]


abstract trait `Hatch'em, Spawn'it, Output'o`[
  P <: Conf
](using
  override protected val * : P
) extends `MonadWithFilter'°`[IO, Stream, P, Long]
    with `Breed *'°`[P]:

  override protected type * <: `Hatch'em, Spawn'it, Output'o`[P]

  override protected type T56 = Unit

  protected val em: Iterable[Hive[P]]

  private def `for*`(using Long): Stream[IO, *.Item] =
    ( bracket {
        val id = UUID.randomUUID
        val flg = `flag*`(id)
        `iterations*`.put(id, (true, flg))
        IO(id -> flg)
      } { (id, flg) =>
        IO(`iterations*`.remove(id, (true, flg)))
      }
    ).flatMap { (id, flg) =>
      emits(em.toSeq)
        .zipWithIndex
        .map { case (hive, n) =>
          val hatch = hive.hatch
          val spawn = hive.`spawn*`

          val o: *.T = spawn(hatch(), tag, iterations)

          val vi = (*.board, tag, UUID.randomUUID, n+1L, (), ())

          new *.Item(o, flg.slot, Some(vi))
        }
      }

  def foreach(block: *.Item => Unit)(using Long): Stream[IO, Unit] =
    val `block°` = `apply°`(block)
    `for*`
      .map(`block°`)
      .filter(_ ne None)
      .map(_.get)

  def map[R](block: *.Item => R)(using Long): Stream[IO, R] =
    val `block°` = `apply°`(block)
    `for*`
      .map(`block°`)
      .filter(_ ne None)
      .map(_.get)

  def flatMap[R](block: *.Item => Stream[IO, R])(using Long): Stream[IO, R] =
    val `block°` = `apply°`(block)
    `for*`
      .map(`block°`)
      .filter(_ ne None)
      .map(_.get)
      .flatten


  override def toString(): String = s"Output FS2 Hive [ #${em.size} Nest${if em.size == 1 then "" else "s"} ]"
