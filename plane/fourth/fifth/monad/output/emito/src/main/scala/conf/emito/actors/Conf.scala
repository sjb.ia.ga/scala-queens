package queens
package conf.emito.actors

import akka.actor.typed.scaladsl.ActorContext

import conf.actors.Callbacks


abstract trait Conf
    extends conf.emito.Conf
    with conf.actors.Conf:

  import dimensions.third.first.actors.{ Iteration, `Queens*` }

  override protected type It = Iteration

  override type Q = `Queens*`[?]
