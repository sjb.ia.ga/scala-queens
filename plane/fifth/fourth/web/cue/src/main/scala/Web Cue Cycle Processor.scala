package queens
package plane.fifth.fourth
package web.cue

import java.util.Date
import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.ConcurrentHashMap

import scala.reflect.ClassTag

import common.bis.Cycle

import dimensions.three.breed.Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import pojo.{ Cue2, Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }

import dimensions.fifth.fourth.cue.cycle.`Base.Cue Cycle Program`


abstract trait `Base.Web Cue Cycle Processor`[It <: AnyRef,
  O <: Queens[R, B, P, S, N, Q],
  B <: Board2[R, B],
  R <: Square2,
  S <: Solution2[R, B, P, S],
  P <: Point2,
  Q <: Queens2[R, B, P, S, N, Q],
  N <: Nest2,
  C <: Cue2[R, B, N]
](using
  ct_o: ClassTag[O],
  ct_b: ClassTag[B],
  ct_r: ClassTag[R],
  ct_s: ClassTag[S],
  ct_p: ClassTag[P],
  ct_q: ClassTag[Q],
  ct_n: ClassTag[N],
  ct_c: ClassTag[C]
) extends `Base.Cue Cycle Program`[It]:

  override protected def apply(wildcard: Wildcard,
                               multiplier: Multiplier[Nest]
                             )(using
                               iteration: It
                             )(using Flavor.Value): Long = ???

  import `Base.Web Cue Cycle Processor`._

  protected  val inMemoryDatabase = ConcurrentHashMap[Row, Long]()
                                 -> ConcurrentHashMap[Long, ((Date, Date, Long), Row)]()

  protected val auto = AtomicLong(0)


object `Base.Web Cue Cycle Processor`:

  case class Row(wildcard: Wildcard,
                 multiplier: Multiplier[Nest],
                 cycle: Cycle,
                 flavor: Flavor.Value
  ):
    private val exp = wildcard(multiplier).sorted

    override def hashCode(): Int = (exp, cycle, flavor).##

    override def equals(any: Any): Boolean = any match
      case that: Row =>
        this.flavor == that.flavor &&
        this.exp == that.exp &&
        this.cycle == that.cycle
      case _ => false


    override def toString(): String = exp.toString() + cycle.toString() + flavor.toString()
