package queens
// https://github.com/hantsy/spring-reactive-sample/blob/master/routes/src/main/java/com/example/demo/Application.java
package plane.fifth.fourth
package web.cue.springboot

import org.springframework.context.annotation.AnnotationConfigApplicationContext

import reactor.netty.http.server.HttpServer


object Main:

  {
    import sys.Prop.StringProp
    sys.Prop("scala.concurrent.context.numThreads").set("x32")
  }

  import sys.Prop.IntProp
  sys.Prop("scala.concurrent.context.maxThreads").set("8192")
  sys.Prop("scala.concurrent.context.maxExtraThreads").set("8192")
  sys.Prop("scala.concurrent.context.minThreads").set("512")
  sys.Prop("java.util.concurrent.ForkJoinPool.common.parallelism").set("4096")

  def main(args: Array[String]): Unit =

    sys.Prop
       .StringProp("server.port")
       .set("8458")

    sys.Prop
       .StringProp("debug")
       .set("false")

    sys.BooleanProp
      .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
      .enable()

    var context: AnnotationConfigApplicationContext = null
    try
      context = new AnnotationConfigApplicationContext(classOf[Application])
      context.getBean(classOf[HttpServer]).bindNow().onDispose().block()
    finally
      if context ne null then context.close()
