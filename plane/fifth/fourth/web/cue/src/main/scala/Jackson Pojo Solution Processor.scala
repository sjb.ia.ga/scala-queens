package queens
package plane.fifth.fourth
package web.cue

import pojo.jackson.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }
import pojo.jackson.jPojoJackson.given

import dimensions.fifth.fourth.third.cue.cycle.`Base.Pojo Solution Processor`


class `Jackson Pojo Solution Processor`(using Boolean)
    extends `Base.Pojo Solution Processor`[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]:

  override def toString(): String = "Jackson " + super.toString
