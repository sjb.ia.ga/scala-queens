package queens
package plane.fifth.fourth
package web.cue
package controller

import scala.concurrent.ExecutionContext

import scala.reflect.ClassTag

import _root_.reactor.core.publisher.Flux

import base.Board

import pojo.{ Cue2, Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }
import pojo.parser.date.jDate.apply
import pojo.jPojo.given


abstract trait `Reactor Base Cue Controller`[
  O <: Queens[R, B, P, S, N, Q],
  B <: Board2[R, B],
  R <: Square2,
  S <: Solution2[R, B, P, S],
  P <: Point2,
  Q <: Queens2[R, B, P, S, N, Q],
  N <: Nest2,
  C <: Cue2[R, B, N]
](using
  ct_o: ClassTag[O],
  ct_b: ClassTag[B],
  ct_r: ClassTag[R],
  ct_s: ClassTag[S],
  ct_p: ClassTag[P],
  ct_q: ClassTag[Q],
  ct_n: ClassTag[N],
  ct_c: ClassTag[C],
  b2b2: Conversion[Board, B]
) extends `Base Cue Controller`[Flux, O, B, R, S, P, Q, N, C]:

  protected def apply(cycleId: Long)(timeout: Long)
                     (using ExecutionContext): Flux[C] =
    val ((started, ended, idle), _) = inMemoryDatabase._2.get(cycleId)

    this(cycleId, timeout) {
      Flux
        .from(_)
        .map { cue =>
          val pojo: C = cue
          pojo.getBoard()(started, ended, Some(idle))
          pojo
        }
    }
