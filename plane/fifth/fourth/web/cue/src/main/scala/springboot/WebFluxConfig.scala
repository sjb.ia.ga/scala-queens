package queens
package plane.fifth.fourth
package web.cue.springboot

import org.springframework.context.annotation.{ Bean, Configuration }
import org.springframework.web.reactive.config.{ EnableWebFlux, WebFluxConfigurer }

import org.springframework.http.MediaType.{ APPLICATION_JSON, APPLICATION_NDJSON }

import org.springframework.web.reactive.function.server.RequestPredicates.{ accept, contentType }
import org.springframework.web.reactive.function.server.{ RouterFunction, ServerResponse }
import org.springframework.web.reactive.function.server.RouterFunctions.route

import dimensions.fifth.output.console.QueensConsoleOutput


@Configuration
@EnableWebFlux
class WebFluxConfig
    extends WebFluxConfigurer:

  private val handler = controller.`WebFlux SpringBoot Cue Handler`(Some(QueensConsoleOutput(_)))

  private val cue: RouterFunction[ServerResponse] = route()
    .path("/queens/cue/solve",
          _
            .POST("/model/{model}", contentType(APPLICATION_JSON), handler.solve(_))
            .GET("/{id}/record", accept(APPLICATION_NDJSON), handler.record(_))
            .POST("/{id}/replay", contentType(APPLICATION_NDJSON).and(accept(APPLICATION_NDJSON)), handler.replay(_))
    ).build()

  @Bean
  def routerFunctionCue(): RouterFunction[?] = cue
