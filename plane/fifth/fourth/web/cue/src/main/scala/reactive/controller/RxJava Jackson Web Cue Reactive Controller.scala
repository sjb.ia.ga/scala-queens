package queens
package plane.fifth.fourth
package web.cue
package reactive
package controller

import scala.concurrent.ExecutionContext

import io.reactivex.rxjava3.core.Flowable

import common.Flag

import common.bis.Cycle

import conf.futures.bis.Callbacks

import dimensions.fifth.output.console.QueensConsoleOutput

import dimensions.third.first.Iteration1

import pojo.jackson.{ Cue2, Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }
import pojo.jPojo.given

import dimensions.fifth.fourth.third.cue.cycle.`Cue Cycle Processor`

import `Base.Web Cue Cycle Processor`.Row

import web.cue.controller.`RxJava Base Cue Controller`


abstract trait `RxJava Jackson Web Cue Reactive Controller`
    extends `RxJava Base Cue Controller`[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2, Cue2]:

  protected def apply(cycleId: Long, timeout: Long)
                     (cues: Flowable[Cue2])
               (using ec: ExecutionContext): Flowable[Queens] =
      implicit val (_, Row(wildcard, multiplier, Cycle(((board, _, max, uuid), _), _), flavor)) = inMemoryDatabase._2.get(cycleId)

      val pub = Flowable
        .fromPublisher(cues)
        .map { pojo =>
          val it: Cue = pojo
          it
        }

      given Boolean = false

      Flowable.fromPublisher {
        (new `Jackson Pojo Solution Processor`()) { idling =>
          val proc = new `Cue Cycle Processor`(pub)(idling)(Some(QueensConsoleOutput(_)))

          given Iteration1 = new Iteration1(Cycle(), uuid, Flag(), { () => }, ec -> Callbacks())

          proc(wildcard, multiplier)

          proc
        }
      }

  override def toString(): String = super.toString + " :: Jackson"
