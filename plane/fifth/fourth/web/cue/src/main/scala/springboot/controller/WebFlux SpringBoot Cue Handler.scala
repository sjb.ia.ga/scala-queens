package queens
package plane.fifth.fourth
package web.cue.springboot
package controller

import java.net.URI

import scala.concurrent.ExecutionContext.Implicits._

import scala.collection.mutable.{ HashMap => MutableMap }
import scala.collection.JavaConverters.mapAsScalaMapConverter
import scala.collection.JavaConverters.iterableAsScalaIterableConverter

import org.springframework.http.HttpStatus
import org.springframework.http.MediaType.{ APPLICATION_JSON, APPLICATION_NDJSON }

import org.springframework.web.reactive.function.server.{ ServerRequest, ServerResponse }
import ServerResponse.{ badRequest, created, ok, status }

import _root_.reactor.core.publisher.{ Flux, Mono }

import common.pipeline.Context

import dimensions.fifth.QueensUseSolution

import pojo.jackson.{ Cue2, Queens, Matrix }
import pojo.jackson.jPojoJackson.given

import web.cue.reactive.controller.`Reactor Jackson Web Cue Reactive Controller`


class `WebFlux SpringBoot Cue Handler`(
  override protected val next: Option[Context => QueensUseSolution] = None
) extends `Reactor Jackson Web Cue Reactive Controller`:

  override protected def request(data: Any): (String, Map[String, List[String]]) =
    val (model, matrix) = data.asInstanceOf[(String, Matrix)]
    model -> (matrix.getAspects.asScala.map { (at, n) => at -> List(n.toString) }.toMap ++
              matrix.getAlgorithms.asScala.map { (am, n) => am -> List(n.toString) }.toMap)

  import `WebFlux SpringBoot Cue Handler`._

  /**
    * /queens/cue/solve/model/{model}
    */
  def solve(request: ServerRequest): Mono[ServerResponse] =
    val model = request.pathVariable("model")

    val query = MutableMap.from(request.queryParams().asScala.map(_ -> _.get(0)))

    val qs = query.keySet
    val ps = params.keySet

    if (qs -- ps).isEmpty
    then

      try
        query.addAll(params.removedAll(ps & qs))

        val size = query("size").toInt
        val empty = java.lang.Boolean.parseBoolean(query("empty"))
        val seed = query("seed").toLong
        val max = query("max").toLong
        val flavor = query("flavor")

        request
          .bodyToMono(classOf[Matrix])
          .map(matrix => this(model -> matrix, Left(size -> (empty -> seed)), max, flavor))
          .flatMap { id => created(URI.create(request.requestPath.subPath(0, 7).value + id)).build }

      catch
        case _: NumberFormatException =>
          badRequest().build()
        case _: Exception =>
          status(HttpStatus.INTERNAL_SERVER_ERROR).build()

    else
      badRequest().build()


  /**
    * /queens/cue/solve/{id}/record
    */
  def record(request: ServerRequest): Mono[ServerResponse] =
    try
      val cycleId = java.lang.Long.parseLong(request.pathVariable("id"))

      val cues = this(cycleId)(timeout(request))

      ok().contentType(APPLICATION_NDJSON).body(cues, classOf[Cue2])
    catch
      case ex: NumberFormatException =>
        badRequest().body(Flux.error(ex), classOf[Cue2])
      case ex: Exception =>
        status(HttpStatus.INTERNAL_SERVER_ERROR).body(Flux.error(ex), classOf[Cue2])

  /**
    * /queens/cue/solve/{id}/replay
    */
  def replay(request: ServerRequest): Mono[ServerResponse] =
    try
      val cycleId = java.lang.Long.parseLong(request.pathVariable("id"))

      val queens = this(cycleId, timeout(request))(request.bodyToFlux(classOf[Cue2]))

      ok().contentType(APPLICATION_NDJSON).body(queens, classOf[Queens])
    catch
      case ex: NumberFormatException =>
        badRequest().body(Flux.error(ex), classOf[Queens])
      case ex: Exception =>
        status(HttpStatus.INTERNAL_SERVER_ERROR).body(Flux.error(ex), classOf[Queens])

  override def toString(): String = super.toString + " :: SpringBoot Cue WebFlux"


object `WebFlux SpringBoot Cue Handler`:

  import scala.collection.mutable.HashMap

  def timeout(request: ServerRequest) =
    request.queryParam("timeout").map(_.toInt).orElse {
      sys.Prop
        .IntProp(_root_.queens.main.Properties.WebCue.timeout)
        .value
    }

  val params = Map[String, String](
    "size"   -> "6",                    // int
    "empty"  -> "true",                 // boolean
    "seed"   -> "-9223372036854775808", // long
    "max"    -> "9223372036854775807",  // long
    "flavor" -> "build",                // string
    "square" -> "",                     // string
    "piece"  -> "",                     // string
    "queen"  -> "",                     // string
    "indent" -> "2",                    // int
  )
