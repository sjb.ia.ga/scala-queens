package object queens {

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]


  type IO = dimensions.fifth.cycle.IO


  type Identity_[A] = A

  type Iterable_[X[_], A] = Iterable[A]


  type `Queens3*` = dimensions.third.`Queens*`[Point, Boolean]


  import java.util.UUID

  import base.Board

  import version.less.nest.Nest

  type Cue = ((Board, Nest, Long, UUID), Long, Long, Long, Long)


  type Ctx = dimensions.fifth.fourth.third.publish.actors.Macros.Ctx


  given Conversion[String, List[String]] = _
    .split("[\\[{(<.: ;,>)}\\]]")
    .filterNot(_.isEmpty)
    .map(_.replace('_', ' '))
    .toList

}
