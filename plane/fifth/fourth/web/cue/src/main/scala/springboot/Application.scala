package queens
// https://github.com/hantsy/spring-reactive-sample/blob/master/routes/src/main/java/com/example/demo/Application.java
package plane.fifth.fourth
package web.cue.springboot

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.context.annotation.PropertySource
import org.springframework.http.server.reactive.HttpHandler
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter
import org.springframework.web.server.adapter.WebHttpHandlerBuilder
import reactor.netty.http.server.HttpServer


@Configuration
@ComponentScan
@PropertySource(value = Array("classpath:application.properties"), ignoreResourceNotFound = true)
class Application:

    @Value("${server.port:8458}")
    private val port: Int = 8458

    @Profile(Array("default"))
    @Bean
    def nettyHttpServer(context: ApplicationContext): HttpServer =
      val handler: HttpHandler = WebHttpHandlerBuilder.applicationContext(context).build()
      val adapter: ReactorHttpHandlerAdapter = new ReactorHttpHandlerAdapter(handler)
      val httpServer: HttpServer = HttpServer.create().host("localhost").port(this.port)
      httpServer.handle(adapter)
