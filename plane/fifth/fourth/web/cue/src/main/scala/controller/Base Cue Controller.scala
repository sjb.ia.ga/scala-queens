package queens
package plane.fifth.fourth
package web.cue
package controller

import java.util.Date
import java.util.UUID

import scala.concurrent.ExecutionContext

import scala.reflect.ClassTag

import org.reactivestreams.Publisher

import base.{ Board, EmptyBoard }

import common.bis.Cycle

import dimensions.third.first.Iteration1

import dimensions.three.breed.Hatch.Flavor

import dimensions.Dimension.{ Monad, Algorithm, Model }
import Monad.Iterable
import Model.Parallel
import Parallel.Futures

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import dimensions.fifth.publish.QueensPublisher

import pojo.{ Cue2, Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }
import pojo.parser.date.jDate.spanFrom
import pojo.parser.date.jDate.apply

import `Base.Web Cue Cycle Processor`.Row

import plane.fifth.fourth.publish.subscribe.cycle.cue.futures.`Cues' Solution Processor`
import web.simple.controller.`Base Controller`


abstract trait `Base Cue Controller`[
  T[_] <: Publisher[_],
  O <: Queens[R, B, P, S, N, Q],
  B <: Board2[R, B],
  R <: Square2,
  S <: Solution2[R, B, P, S],
  P <: Point2,
  Q <: Queens2[R, B, P, S, N, Q],
  N <: Nest2,
  C <: Cue2[R, B, N]
](using
  ct_o: ClassTag[O],
  ct_b: ClassTag[B],
  ct_r: ClassTag[R],
  ct_s: ClassTag[S],
  ct_p: ClassTag[P],
  ct_q: ClassTag[Q],
  ct_n: ClassTag[N],
  ct_c: ClassTag[C],
  b2b2: Conversion[Board, B]
) extends `Base Controller`[Long]
    with `Base.Web Cue Cycle Processor`[Iteration1, O, B, R, S, P, Q, N, C]:

  override protected def apply(monad: Monad, model: Model)
                              (wildcard: Wildcard, multiplier: Multiplier[Nest])
                              (started: Date, ended: Date)
                        (using Board, Long, Flavor.Value): Long =
    (monad, model) match
      case (Iterable, Parallel(Futures)) =>
      case _ => ???

    val cycle = Cycle(wildcard, multiplier)
    val key = Row(wildcard, multiplier, cycle, implicitly[Flavor.Value])

    synchronized {
      if inMemoryDatabase._1.containsKey(key)
      then
        inMemoryDatabase._1.get(key)
      else
        val id = auto.incrementAndGet
        inMemoryDatabase._1.put(key, id)
        inMemoryDatabase._2.put(id, (started, ended, 0L.spanFrom(ended)) -> key)
        id
    }


  protected def apply(cycleId: Long, timeout: Long)
                     (block: QueensPublisher[Cue] => T[C])
               (using ec: ExecutionContext): T[C] =
    val (_, Row(wildcard, multiplier, cycle, flavor)) = inMemoryDatabase._2.get(cycleId)

    given Flavor.Value = flavor

    given Boolean = false

    val cues = new `Cues' Solution Processor`(legacy = false)()

    cues(wildcard, multiplier)(cycle, timeout)(block)._1
