package queens
package plane.fifth.fourth
package web.cue

import java.util.UUID

import scala.collection.JavaConverters.iterableAsScalaIterableConverter

import org.reactivestreams.{ Subscriber, Subscription }

import org.scalatest.flatspec.AnyFlatSpec

import base.{ Board, EmptyBoard }

import common.Flag

import common.bis.Cycle

import dimensions.fifth.output.console.QueensConsoleOutput

import dimensions.third.first.Iteration1

import dimensions.three.breed.Hatch.Flavor

import dimensions.Dimension.Aspect
import Aspect._
import Classic._
import Stepper._
import dimensions.Dimension.Algorithm
import Algorithm._
import Recursive._
import dimensions.Dimension.Model
import Model.Flow

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import pojo.jackson.Queens
import pojo.jPojo.given
import pojo.last

import plane.fifth.fourth.publish.subscribe.cycle.cue.flow.`Cues' Solution Processor`
import dimensions.fifth.fourth.third.cue.cycle.`Cue Cycle Processor`
import plane.fifth.fourth.web.cue.`Jackson Pojo Solution Processor`


class ReactorJacksonPojoProcessorByFlowSpec
    extends AnyFlatSpec
    with Subscriber[Queens]:

  private val TEST = Object()


  given Option[Long] = Some(0)

  given Flavor.Value = Flavor.build


  val wildcard = Wildcard (
    (_, _) match
      case (Aspect, Classic(Straight)) => true
      case (Aspect, Classic(Callback)) => true
      case (Aspect, Stepper(OneIncrement)) => true
      case (Aspect, Stepper(Permutations)) => true
      case (Algorithm, Iterative) => true
      case (Algorithm, Recursive(Extern)) => true
      case (Model, Flow) => true
      case _ => false
  )

  val multiplier = Multiplier[Nest] {
    // case Nest(_, Recursive(Extern), _) => 1
    // case Nest(_, Iterative, _) => 1
    case _ => 1
  }


  val exp = wildcard(multiplier)

  var subscription: Subscription = _

  var counter = 0L

  var offset = 0L


  "Board 3x3 selected for 1 solution" should "replay 1 solution" in {
    Synchronized(TEST) {
      given Long = 2

      val o = false
      val x = true

      given Board = new Board(List(
        List(x, o, x),
        List(x, x, x),
        List(o, x, o)
      ))

      apply
    }
  }

  "Board 4x4 selected for 2 solutions" should "replay 2 solutions" in {
    Synchronized(TEST) {
      given Long = 3

      val o = false
      val x = true

      given Board = new Board(List(
        List(x, o, x, x),
        List(x, o, x, o),
        List(x, x, x, x),
        List(o, x, o, x)
      ))

      apply
    }
  }


  "Board 6x6 selected for 4 solutions" should "replay 4 solutions" in {
    Synchronized(TEST) {
      given Long = 5

      given Board = EmptyBoard(6)

      apply
    }
  }

  def apply(using Board)
           (using Long, Flavor.Value): Unit =

    given Boolean = false

    val cues = new `Cues' Solution Processor`(legacy = false)()

    val (_, total) = cues(wildcard, multiplier)(Cycle(wildcard, multiplier), 0L) { pub =>

      `Jackson Pojo Solution Processor`() { idling =>
        val proc = new `Cue Cycle Processor`(pub)(idling)(Some(QueensConsoleOutput(_)))

        given Iteration1 = new Iteration1(Cycle(), UUID.randomUUID, Flag(), { () => })

        proc(wildcard, multiplier)

        proc
      }.subscribe(this)

    }

    val count = implicitly[Long] - 1

    assert(counter - offset == exp.size)
    assert(total == (counter - offset) * count)

    offset = counter


  override def onSubscribe(subscription: Subscription): Unit =
    subscription.request(1)

  override def onNext(qs: Queens): Unit =
    counter += 1

    for
      q2 <- qs.getQueens.asScala
      _ = println(s"@${q2.getTag}.${q2.getUUID} [${q2.getStarted}, ${q2.getEnded}] (${q2.getElapsed}, ${q2.getIdle})")
      s2 <- q2.getSolutions.asScala
    do
      val it: Solution = s2
      println(s"#${s2.getNumber}: $it [${s2.getStarted}, ${s2.getEnded}] (${s2.getElapsed}, ${s2.getIdle})")

    subscription.request(1)

  override def onComplete(): Unit = {}
  override def onError(t: Throwable): Unit = {}
