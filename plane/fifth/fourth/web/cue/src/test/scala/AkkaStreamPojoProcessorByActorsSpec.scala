package queens
package plane.fifth.fourth
package web.cue

import java.util.UUID

import scala.concurrent.duration._

import akka.actor.typed.{ ActorRef, Behavior }
import akka.actor.typed.scaladsl.Behaviors

import akka.stream.scaladsl.{ JavaFlowSupport, Sink }

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit

import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.BeforeAndAfterAll

import base.{ Board, EmptyBoard }

import common.Flag

import common.bis.Cycle

import conf.actors.bis.Callbacks

import dimensions.fourth.subscribe.QueensSubscription._

import dimensions.fifth.output.console.QueensConsoleOutput

import dimensions.third.first.Iteration1

import dimensions.three.breed.Hatch.Flavor

import dimensions.Dimension.Aspect
import Aspect._
import Classic._
import Stepper._
import dimensions.Dimension.Algorithm
import Algorithm._
import Recursive._
import dimensions.Dimension.Model
import Model.Parallel
import Parallel.Actors

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import pojo.`4s`.{ Queens, Queens2, Solution2 }
import Solution2.given

import plane.fifth.fourth.publish.subscribe.cycle.cue.actors.`Cues' Solution Processor`
import dimensions.fifth.fourth.third.cue.cycle.`Cue Cycle Processor`
import dimensions.fifth.fourth.third.cue.cycle.`4s`.`Pojo Solution Processor`


class AkkaStreamPojoProcessorByActorsSpec
    extends ScalaTestWithActorTestKit
    with AnyWordSpecLike
    with BeforeAndAfterAll:

  import AkkaStreamPojoProcessorByActorsSpec._


  private val TEST = Object()


  given Option[Long] = Some(0)

  given Flavor.Value = Flavor.build


  val wildcard = Wildcard (
    (_, _) match
      case (Aspect, Classic(Straight)) => true
      case (Aspect, Classic(Callback)) => true
      case (Aspect, Stepper(OneIncrement)) => true
      case (Aspect, Stepper(Permutations)) => true
      case (Algorithm, Iterative) => true
      case (Algorithm, Recursive(Extern)) => true
      case (Model, Parallel(Actors)) => true
      case _ => false
  )

  val multiplier = Multiplier[Nest] {
    // case Nest(_, Recursive(Extern), _) => 1
    // case Nest(_, Iterative, _) => 1
    case _ => 1
  }


  val exp = wildcard(multiplier)


  "Board 3x3 selected for 1 solution" should {
    "replay 1 solution" in {
      Synchronized(TEST) {
        given Long = 2

        val o = false
        val x = true

        given Board = new Board(List(
          List(x, o, x),
          List(x, x, x),
          List(o, x, o)
        ))

        apply(20)
      }
    }
  }


  "Board 4x4 selected for 2 solutions" should {
    "replay 2 solutions" in {
      Synchronized(TEST) {
        given Long = 3

        val o = false
        val x = true

        given Board = new Board(List(
          List(x, o, x, x),
          List(x, o, x, o),
          List(x, x, x, x),
          List(o, x, o, x)
        ))

        apply(20)
      }
    }
  }


  "Board 6x6 selected for 4 solutions" should {
    "replay 4 solutions" in {
      Synchronized(TEST) {
        given Long = 5

        given Board = EmptyBoard(6)

        apply(40)
      }
    }
  }


  def apply(sec: Int)
           (using Board)
           (using Long, Flavor.Value): Unit =

    val `13`: Behavior[Test] = Behaviors
      .receive {

        case (ctx, Test(Left(gen))) =>

          val cbs = Callbacks()

          ctx.spawnAnonymous {
            given Boolean = false

            val cues = new `Cues' Solution Processor`(legacy = false)()

            val (_, r) = cues(wildcard, multiplier)(Cycle(wildcard, multiplier), 0L) { pub =>

              JavaFlowSupport.Source
                .fromPublisher {
                  `Pojo Solution Processor`() { idling =>
                    val proc = new `Cue Cycle Processor`(pub)(idling)(Some(QueensConsoleOutput(_)))

                    given Iteration1 = new Iteration1(Cycle(), UUID.randomUUID, Flag(), { () => }, ctx -> cbs)

                    proc(wildcard, multiplier)

                    proc
                  }
                }
                .runWith {
                  Sink
                    .foreach {
                      case Queens(_, _, queens, _, _, _, _) =>
                        for
                          Queens2(_, _, tag, uuid, _, solutions, started, ended, elapsed, idle) <- queens
                          _ = println(s"@$tag.$uuid [$started, $ended] ($elapsed, $idle)")
                          it @ Solution2(number, _, _, started, ended, elapsed, idle) <- solutions
                        do
                          println(s"#$number: $it [$started, $ended] ($elapsed, $idle)")
                    }
                }

            }

            r
          } ! Some(cbs) -> gen

          Behaviors.same

        case (ctx, Test(Right(ref))) =>

          ref ! Stop

          Behaviors.stopped
      }

    val count = implicitly[Long] - 1

    val actor = spawn(`13`, "Queens-Test")
    val gen = createTestProbe[Long]()
    val stop = createTestProbe[Stop.type]()

    actor ! Test(Left(gen.ref))

    gen.expectMessage(sec.seconds, exp.size * count)

    actor ! Test(Right(stop.ref))

    stop.expectMessage(Stop)


object AkkaStreamPojoProcessorByActorsSpec:

  case class Test(response: Either[ActorRef[Long], ActorRef[Stop.type]])
  case object Stop
