val AkkaVersion = "2.8.5"
val springBootVersion = "3.3.0"

libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",

  "com.typesafe.akka" %% "akka-actor-testkit-typed" % AkkaVersion % Test,

  // reactive
  "org.reactivestreams" % "reactive-streams" % "1.0.4",
  "io.reactivex.rxjava3" % "rxjava" % "3.1.8",
  "io.projectreactor" % "reactor-core" % "3.6.7",
  "org.springframework.boot" % "spring-boot-starter-webflux" % springBootVersion
    exclude("org.springframework.boot", "spring-boot-starter-tomcat"),

  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
