val AkkaVersion = "2.8.5"
val AkkaHttpVersion = "10.5.3"
val springBootVersion = "3.3.0"
val jacksonVersion = "2.17.1"

libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",

  // akkahttp
  "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % AkkaHttpVersion,

  // dropwizard
  "io.dropwizard" % "dropwizard-core" % "4.0.7",
  "io.dropwizard" % "dropwizard-jackson" % "4.0.7",
  "io.dropwizard" % "dropwizard-jetty" % "4.0.7",
  "jakarta.ws.rs" % "all" % "3.1.0",
  "javax.servlet" % "javax.servlet-api" % "4.0.1",
  "com.fasterxml.jackson.module" % "jackson-module-afterburner" % "2.17.1",

  // micronaut
  "io.micronaut" % "micronaut-runtime" % "4.5.3",
  "io.micronaut" % "micronaut-http" % "4.5.3",
  "io.micronaut" % "micronaut-http-server-netty" % "4.5.3",
  "io.micronaut" % "micronaut-jackson-databind" % "4.5.3",
  "io.micronaut" % "micronaut-inject-java" % "4.5.3",

  // springboot
  "org.springframework.boot" % "spring-boot-starter-web" % springBootVersion
    exclude("org.springframework.boot", "spring-boot-starter-tomcat"),
  "org.springframework.boot" % "spring-boot-starter-jetty" % springBootVersion
    exclude("jakarta.servlet", "jakarta.servlet-api"),
  "jakarta.servlet" % "jakarta.servlet-api" % "6.1.0",
  "com.fasterxml.jackson.core" % "jackson-databind" % jacksonVersion,
  "com.fasterxml.jackson.dataformat" % "jackson-dataformat-xml" % jacksonVersion,

  // vertx
  "io.vertx" % "vertx-web" % "4.5.8",
  "com.fasterxml.jackson.core" % "jackson-databind" % jacksonVersion,
  "com.fasterxml.jackson.dataformat" % "jackson-dataformat-xml" % jacksonVersion,

  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
