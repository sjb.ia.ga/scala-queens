package queens
package plane.fifth.fourth
package web.simple
package servlet.container
package controller

import pojo.jackson.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }

import web.simple.controller.`Base Solve Controller`


abstract trait `Jackson Web Servlet Container Controller`
    extends `Base Solve Controller`[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]
    with flow.`Jackson Web Servlet Container Simple Program by Flow`:

  override def toString(): String = super.toString + " :: Jackson"
