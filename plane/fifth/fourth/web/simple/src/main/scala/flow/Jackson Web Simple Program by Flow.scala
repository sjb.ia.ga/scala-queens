package queens
package plane.fifth.fourth
package web.simple
package flow

import dimensions.third.first.Iteration1

import pojo.jackson.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }

import pojo.jackson.jPojoJackson.given


abstract trait `Jackson Web Simple Program by Flow`
    extends `Base.Web Simple Program by Flow`[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]
    with `Jackson Web Simple Program`[Iteration1]
