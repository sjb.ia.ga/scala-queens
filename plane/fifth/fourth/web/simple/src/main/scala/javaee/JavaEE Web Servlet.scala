package queens
package plane.fifth.fourth
package web.simple.javaee

import javax.servlet.annotation.WebServlet
import javax.servlet.http.{ HttpServlet, HttpServletRequest, HttpServletResponse }

import scala.collection.mutable.{ HashMap => MutableMap }
import scala.collection.JavaConverters.mapAsScalaMapConverter

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution
import dimensions.fifth.output.console.QueensConsoleOutput

import pojo.jackson.jPojoJackson.given
import pojo.parser.text.jText.toText
import pojo.parser.write.jWrite.toData

import web.simple.servlet.container.controller.`Jackson Web Servlet Container Controller`


@WebServlet(urlPatterns = Array("/queens/solve/model/*"))
class `JavaEE Web Servlet`
    extends HttpServlet
    with `Jackson Web Servlet Container Controller`:

  final override protected val plainJAVA: Boolean = false

  final override protected val webXML: Boolean = false

  override protected def next: Option[Context => QueensUseSolution] = Some(QueensConsoleOutput(_))

  override protected def request(data: Any): (String, Map[String, List[String]]) =
    val List(_, model, matrix*) = data.asInstanceOf[String].split("/").toList

    val vars = matrix.map(_.split("=").take(2))

    assert(vars.map(_.size == 2).forall(identity))

    val pairs = vars.map(_(0)) zip vars.map(_(1))

    val planes = MutableMap[String, List[String]]()

    pairs.foreach { case (k, _) => planes(k) = Nil }
    pairs.foreach { case (k, v) => planes(k) = v :: planes(k) }

    model -> planes.toMap

  import `JavaEE Web Servlet`._

  override def doGet(
    request: HttpServletRequest,
    response: HttpServletResponse
  ): Unit =
    if request.getHeader("Accept").toLowerCase != "text/plain"
    then
      response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE)
      return

    val query = MutableMap.from(request.getParameterMap().asScala.map(_ -> _(0)))

    val qs = query.keySet
    val ps = params.keySet

    if (qs -- ps).nonEmpty
    then
      response.sendError(HttpServletResponse.SC_BAD_REQUEST)
      return

    try
      query.addAll(params.removedAll(ps & qs))

      val size = query("size").toInt
      val empty = java.lang.Boolean.parseBoolean(query("empty"))
      val seed = query("seed").toLong
      val max = query("max").toLong
      val flavor = query("flavor")

      val square = query("square")
      val piece = query("piece")
      val queen = query("queen")
      val indent = query("indent")

      val responseBody = this(request.getPathInfo(), Left(size -> (empty -> seed)), max, flavor)
        .toData(square, piece, queen)
        .toText(try indent.toInt catch _ => 1, " ")

      response.setContentType("text/plain")
      response.getWriter.write(responseBody)

    catch
      case _: NumberFormatException =>
        response.sendError(HttpServletResponse.SC_BAD_REQUEST)
      case _: Exception =>
        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)


object `JavaEE Web Servlet`:

  val params = Map[String, String](
    "size"   -> "6",                    // int
    "empty"  -> "true",                 // boolean
    "seed"   -> "-9223372036854775808", // long
    "max"    -> "9223372036854775807",  // long
    "flavor" -> "build",                // string
    "square" -> "",                     // string
    "piece"  -> "",                     // string
    "queen"  -> "",                     // string
    "indent" -> "2",                    // int
  )
