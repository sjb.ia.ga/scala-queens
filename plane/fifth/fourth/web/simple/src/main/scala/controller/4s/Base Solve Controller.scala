package queens
package plane.fifth.fourth
package web.simple
package controller

import java.util.Date

import base.Board

import dimensions.three.breed.Hatch
import Hatch.Flavor

import dimensions.Dimension.{ Monad, Algorithm, Model }
import Monad.Iterable
import Model.Flow

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import pojo.parser.date.jDate.spanFrom

import plane.fourth.fifth.third.TryBoard

import common.given

import pojo.`4s`.Queens


package `4s`:

  abstract trait `Base Solve Controller`
      extends `Base Controller`[Queens]
      with flow.`4s`.`Base.Web Simple Program by Flow`:

    override protected def apply(monad: Monad, model: Model)
                                (wildcard: Wildcard, multiplier: Multiplier[Nest])
                                (started: Date, ended: Date)
                          (using Board, Long, Flavor.Value): Queens =
      val idle = 0L.spanFrom(ended)

      (monad, model) match
        case (Iterable, Flow) =>
        case _ => ???

      val ((response, idleSolving), endedSolving) = byFlow(wildcard, multiplier) -> new Date()

      response.board = response.board.copy(started = started,
                                           ended = ended,
                                           elapsed = ended.getTime - started.getTime,
                                           idle = 0L)

      val elapsed = Date().getTime - started.getTime

      response.copy(started = started,
                    ended = Date(started.getTime + elapsed),
                    elapsed = elapsed,
                    idle = idle.spanFrom(endedSolving) + idleSolving)


  object `Base Solve Controller`:

    import scala.collection.mutable.{ HashMap => MutableMap }

    def apply(data: Any, monad: Option[Monad] = None): (String, Map[String, List[String]]) =
      val List(model, matrix*) = data.asInstanceOf[String].split(";").toList

      val vars = matrix.map(_.split("=").take(2))

      assert(vars.map(_.size == 2).forall(identity))

      val pairs = vars.map(_(0)) zip vars.map(_(1))

      val planes = MutableMap[String, List[String]]()

      pairs.foreach { case (k, _) => planes(k) = Nil }
      pairs.foreach { case (k, v) => planes(k) = v :: planes(k) }

      monad.foreach { it => planes(it.toString) = List("1") }

      model -> planes.toMap
