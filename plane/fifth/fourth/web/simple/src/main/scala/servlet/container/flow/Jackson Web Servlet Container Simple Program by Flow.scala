package queens
package plane.fifth.fourth
package web.simple
package servlet.container
package flow

import pojo.jackson.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }

import dimensions.fifth.fourth.third.web.simple.servlet.container.flow.QueensWebServletContainerSimpleProgramByFlow

import web.simple.flow.`Jackson Web Simple Program by Flow`


abstract trait `Jackson Web Servlet Container Simple Program by Flow`
    extends `Jackson Web Simple Program by Flow`
    with QueensWebServletContainerSimpleProgramByFlow[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]
