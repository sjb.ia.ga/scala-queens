package queens
package plane.fifth.fourth
package web.simple

import scala.reflect.ClassTag

import pojo.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }

import dimensions.fifth.fourth.web.simple.QueensWebSimpleProgram


abstract trait `Base.Web Simple Program`[It <: AnyRef,
  O <: Queens[R, B, P, S, N, Q],
  B <: Board2[R, B],
  R <: Square2,
  S <: Solution2[R, B, P, S],
  P <: Point2,
  Q <: Queens2[R, B, P, S, N, Q],
  N <: Nest2
](using
  ct_o: ClassTag[O],
  ct_b: ClassTag[B],
  ct_r: ClassTag[R],
  ct_s: ClassTag[S],
  ct_p: ClassTag[P],
  ct_q: ClassTag[Q],
  ct_n: ClassTag[N]
) extends QueensWebSimpleProgram[It, O, B, R, S, P, Q, N]


package `4s`:

  abstract trait `Base.Web Simple Program`[It <: AnyRef]
      extends dimensions.fifth.fourth.web.simple.`4s`.QueensWebSimpleProgram[It]
