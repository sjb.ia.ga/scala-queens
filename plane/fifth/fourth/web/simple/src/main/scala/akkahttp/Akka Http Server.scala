package queens
package plane.fifth.fourth
package web.simple
package akkahttp

import scala.collection.mutable.{ HashMap => MutableMap }
import scala.concurrent.{ ExecutionContext, Future }
import scala.io.StdIn

import akka.actor.typed.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ MediaRange, StatusCodes }
import akka.http.scaladsl.model.MediaTypes.{ `application/json`, `text/plain` }
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ MediaTypeNegotiator, Route }
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution
import dimensions.fifth.output.console.QueensConsoleOutput

import pojo.`4s`.Queens
import pojo.parser.`4s`.SprayJson._

import pojo.parser.text.jText.toText
import pojo.parser.write.`4s`.toData

import plane.fifth.fourth.web.simple.controller.`4s`.`Base Solve Controller`


object `Akka Http Server`
    extends `Base Solve Controller`
    with SprayJsonSupport:

  final override protected val plainJAVA: Boolean = false

  val _next: Context => QueensUseSolution = QueensConsoleOutput(_)

  override protected def next: Option[Context => QueensUseSolution] =
    if _next eq null then None else Some(_next)


  private val encodings = Seq(MediaRange(`text/plain`), MediaRange(`application/json`))

  def route(using ExecutionContext): Route =
    ( get
    & path("queens" / "solve" / "model" / RemainingPath)
    & parameters("size".?(6),
                 "empty".?(true),
                 "seed".?(Long.MinValue),
                 "max".?(Long.MaxValue),
                 "flavor".?("build"),
                 "square".?(""),
                 "piece".?(""),
                 "queen".?(""),
                 "indent".?(2))
    & extract(_.request.headers)
    ) { (planes, size, empty, seed, max, flavor, square, piece, queen, indent, headers) =>
      onSuccess {
        Future {
          this(planes, Left(size -> (empty -> seed)), max, flavor)
        }
      } { queens =>

        // https://stackoverflow.com/questions/32187858/akka-http-accept-and-content-type-handling
        val encoding = MediaTypeNegotiator(headers)
          .acceptedMediaRanges
          .intersect(encodings)
          .headOption
          .getOrElse(MediaRange(`application/json`))

        if encoding.matches(`application/json`)
        then
          complete(queens)
        else if encoding.matches(`text/plain`)
        then
          complete {
            queens
              .toData(square, piece, queen)
              .toText(indent)
        }
        else
          complete(StatusCodes.BadRequest)

      }
    }


  override protected def request(data: Any): (String, Map[String, List[String]]) =
    `Base Solve Controller`(data.toString)

  override def toString(): String = super.toString + " :: AkkaHttp"


  def apply(): Unit =
    given system: ActorSystem[?] = ActorSystem(Behaviors.empty, "queens")
    given ExecutionContext = system.executionContext

    val bindingFuture = Http().newServerAt("localhost", 8044).bind(route)

    StdIn.readLine()

    bindingFuture
      .flatMap(_.unbind())
      .onComplete(_ => system.terminate())
