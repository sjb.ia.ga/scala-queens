package queens
package plane.fifth.fourth
package web.simple
package flow

import java.util.Date

import base.Board

import common.given

import dimensions.third.first.Iteration1

import dimensions.three.breed.Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import pojo.`4s`.{ Queens, Board2, Square2 }
import pojo.parser.date.span

import plane.fifth.fourth.web.simple.`4s`.`Base.Web Simple Program`


package `4s`:

  import dimensions.fifth.fourth.third.web.simple.flow.`4s`.QueensWebSimpleProgramByFlow

  abstract trait `Base.Web Simple Program by Flow`
      extends `Base.Web Simple Program`[Iteration1]
      with QueensWebSimpleProgramByFlow:

    import Board2.given

    protected def byFlow(wildcard: Wildcard,
                         multiplier: Multiplier[Nest])
                        (using board: Board)
                        (using Long, Flavor.Value): (Queens, Long) =
      val started = Date()

      val squares: List[Square2] = board
      val board2 = Board2(squares.sorted, board.seed.getOrElse(0L))

      var response = Queens(board2)

      val tag = this(response)

      given Iteration1 = new Iteration1(board, tag, false, Right(false), common.Flag(), { () => })

      val init: Long = 0L.span(from = started)

      val idle = this(wildcard, multiplier)

      val ended = Date()

      response.count = response.queens.size

      this(tag, response)

      response = response.copy(queens = response.queens.sorted)

      val done: Long = idle.span(from = ended)

      response -> (init + done)
