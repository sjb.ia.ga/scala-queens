package queens
package plane.fifth.fourth
package web.simple.micronaut
package controller

import scala.collection.mutable.{ HashMap => MutableMap }

import io.micronaut.http.MediaType.{ APPLICATION_JSON, APPLICATION_XML, TEXT_PLAIN }
import io.micronaut.http.annotation.{ Controller, Get, PathVariable, QueryValue, Produces }

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution

import dimensions.fifth.output.console.QueensConsoleOutput

import pojo.jackson.Queens
import pojo.jackson.jPojoJackson.given

import pojo.parser.text.jText.toText
import pojo.parser.write.jWrite.toData

import web.simple.servlet.container.controller.`Jackson Web Servlet Container Controller`


@Controller("/queens/solve")
class `Http Micronaut Controller`
    extends `Jackson Web Servlet Container Controller` {

//  @Value("${micronaut.application.name}")
  val appName: String = null

  final override protected val plainJAVA: Boolean = false

  final override protected val webXML: Boolean = false

  val _next: Context => QueensUseSolution = QueensConsoleOutput.apply

  override protected def next: Option[Context => QueensUseSolution] =
    if _next eq null then None else Some(_next)

  @Get("/model/{data}")
  @Produces(Array(TEXT_PLAIN))
  def solve(@PathVariable("data")                              data: String,
            @QueryValue(defaultValue = "6")                    size: Int,
            @QueryValue(defaultValue = "true")                 empty: Boolean,
            @QueryValue(defaultValue = "-9223372036854775808") seed: Long,
            @QueryValue(defaultValue = "9223372036854775807")  max: Long,
            @QueryValue(defaultValue = "build")                flavor: String,
            @QueryValue(defaultValue = "")                     square: String,
            @QueryValue(defaultValue = "")                     piece: String,
            @QueryValue(defaultValue = "")                     queen: String,
            @QueryValue(defaultValue = "2")                    indent: String
  ): String =
    this(data, Left(size -> (empty -> seed)), max, flavor)
      .toData(square, piece, queen)
      .toText(try indent.toInt catch _ => 1, " ")

  @Get("/model/{data}")
  @Produces(Array(APPLICATION_JSON, APPLICATION_XML))
  def solve(@PathVariable("data")                              data: String,
            @QueryValue(defaultValue = "6")                    size: Int,
            @QueryValue(defaultValue = "true")                 empty: Boolean,
            @QueryValue(defaultValue = "-9223372036854775808") seed: Long,
            @QueryValue(defaultValue = "9223372036854775807")  max: Long,
            @QueryValue(defaultValue = "build")                flavor: String
  ): Queens = this(data, Left(size -> (empty -> seed)), max, flavor)


  override protected def request(data: Any): (String, Map[String, List[String]]) = {
    val List(model, matrix*) = data.asInstanceOf[String].split(";").toList

    val vars = matrix.map(_.split("=").take(2))

    assert(vars.map(_.size == 2).forall(identity))

    val pairs = vars.map(_(0)) zip vars.map(_(1))

    val planes = MutableMap[String, List[String]]()

    pairs.foreach { case (k, _) => planes(k) = Nil }
    pairs.foreach { case (k, v) => planes(k) = v :: planes(k) }

    model -> planes.toMap
  }

  override def toString(): String = super.toString + " :: Micronaut"

}
