package queens
package plane.fifth.fourth
package web.simple.springboot
package controller

import scala.collection.mutable.{ HashMap => MutableMap }
import scala.collection.JavaConverters.iterableAsScalaIterableConverter

import org.springframework.util.MultiValueMap

import org.springframework.http.MediaType.{ APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE, TEXT_PLAIN_VALUE }

import org.springframework.beans.factory.annotation.{ Autowired, Value }
import org.springframework.web.bind.annotation.{ RequestMapping, RestController }
import org.springframework.web.bind.annotation.{ PathVariable, MatrixVariable, GetMapping, RequestParam, ResponseBody }
import org.springframework.web.bind.annotation.ExceptionHandler

import org.springframework.http.ResponseEntity

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution
import dimensions.fifth.output.console.QueensConsoleOutput

import pojo.jackson.Queens
import pojo.jackson.jPojoJackson.given

import pojo.parser.text.jText.toText
import pojo.parser.write.jWrite.toData

import dimensions.fourth.webapp.`Web*`.FlavorException

import web.simple.servlet.container.controller.`Jackson Web Servlet Container Controller`


@RestController
@RequestMapping(path = Array("/queens/solve/*"))
class `Rest SpringBoot Controller`
    extends `Jackson Web Servlet Container Controller`:

  @Value("${spring.application.name}")
  val appName: String = null

  final override protected val plainJAVA: Boolean = false

  final override protected val webXML: Boolean = false

  val _next: Context => QueensUseSolution = QueensConsoleOutput(_)

  override protected def next: Option[Context => QueensUseSolution] =
    if _next eq null then None else Some(_next)

  @GetMapping(path = Array("/model/{model}"), produces = Array(TEXT_PLAIN_VALUE))
  def solve(@PathVariable(value = "model")                                         model: String,
            @MatrixVariable(pathVar = "model")                                     planes: MultiValueMap[String, String],
            @RequestParam(required = false, defaultValue = "6")                    size: Int,
            @RequestParam(required = false, defaultValue = "true")                 empty: Boolean,
            @RequestParam(required = false, defaultValue = "-9223372036854775808") seed: Long,
            @RequestParam(required = false, defaultValue = "9223372036854775807")  max: Long,
            @RequestParam(required = false, defaultValue = "build")                flavor: String,
            @RequestParam(required = false, defaultValue = "")                     square: String,
            @RequestParam(required = false, defaultValue = "")                     piece: String,
            @RequestParam(required = false, defaultValue = "")                     queen: String,
            @RequestParam(required = false, defaultValue = "2")                    indent: String
  ): String = this(model -> planes, Left(size -> (empty -> seed)), max, flavor)
    .toData(square, piece, queen)
    .toText(try indent.toInt catch _ => 1, " ")

  @GetMapping(path = Array("/model/{model}"), produces = Array(APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE))
  @ResponseBody
  def solve(@PathVariable(value = "model")                                         model: String,
            @MatrixVariable(pathVar = "model")                                     planes: MultiValueMap[String, String],
            @RequestParam(required = false, defaultValue = "6")                    size: Int,
            @RequestParam(required = false, defaultValue = "true")                 empty: Boolean,
            @RequestParam(required = false, defaultValue = "-9223372036854775808") seed: Long,
            @RequestParam(required = false, defaultValue = "9223372036854775807")  max: Long,
            @RequestParam(required = false, defaultValue = "build")                flavor: String
  ): Queens = this(model -> planes, Left(size -> (empty -> seed)), max, flavor)


  @ExceptionHandler(value = Array(classOf[FlavorException]))
  def error(ex: base.QueensException): ResponseEntity[String] =
    ResponseEntity.badRequest().build()


  override protected def request(data: Any): (String, Map[String, List[String]]) =
    val (model, matrix) = data.asInstanceOf[(String, MultiValueMap[String, String])]

    val planes = MutableMap[String, List[String]]()

    matrix.keySet.asScala.foreach { key =>
      planes(key) = matrix.get(key).asScala.toList
    }

    model -> planes.toMap

  override def toString(): String = super.toString + " :: SpringBoot WebMvc"
