package queens
package plane.fifth.fourth
package web.simple.micronaut

import io.micronaut.runtime.Micronaut


object Main:

  sys.Prop
     .StringProp("micronaut.server.port")
     .set("8037")

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  def main(args: Array[String]): Unit =
    Micronaut.run(classOf[MicronautApp])
