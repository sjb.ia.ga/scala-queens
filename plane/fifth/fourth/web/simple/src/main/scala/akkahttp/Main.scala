package queens
package plane.fifth.fourth
package web.simple
package akkahttp


object Main:

  def main(args: Array[String]): Unit =

    sys.BooleanProp
      .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
      .enable()

    `Akka Http Server`()
