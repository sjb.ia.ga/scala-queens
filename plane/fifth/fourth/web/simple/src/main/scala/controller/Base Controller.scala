package queens
package plane.fifth.fourth
package web.simple
package controller

import scala.reflect.ClassTag

import java.util.Date

import base.Board

import dimensions.three.breed.{ Hatch, Hatcher }
import Hatch.Flavor

import dimensions.Dimension.{ Monad, Aspect, Algorithm, Model }
import Monad.iterable

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import pojo.parser.date.span

import plane.fourth.fifth.third.TryBoard

import dimensions.fourth.webapp.`Web*`.FlavorException

import common.given


abstract trait `Base Controller`[T]:

  {
    given Hatch.Flavor.Value = Hatch.Flavor.`di-guice`

    Hatcher()
  }

  {
    given Hatch.Flavor.Value = Hatch.Flavor.`di-macwire`

    Hatcher()
  }

  protected def request(data: Any): (String, Map[String, List[String]]) =
    data.asInstanceOf[(String, Map[String, List[String]])]


  private def preprocess(data: (String, Map[String, List[String]])): (Monad, Model, Wildcard, Multiplier[Nest]) =
    val (model, planes) = data

    val params = planes.keys

    val monads: Iterable[Monad] = { val its = params.map(Monad(_)); its }
    val aspects: Iterable[Aspect] = { val its = params.map(Aspect(_)); its }
    val algorithms: Iterable[Algorithm] = { val its = params.map(Algorithm(_)); its }
    val models: Iterable[Model] = { val its = Set(model).map(Model(_)); its }

    val nests =
      for
        at <- aspects
        am <- algorithms
        ml <- models
      yield
        Nest(at, am, ml)

    val wildcard = Wildcard(Wildcard(nests.toSeq*))

    val multiplier = Multiplier[Nest] {
      case Nest(at, am, _) =>
        planes(s"$at")
          .map { n => try n.toInt catch _ => 0 }
          .sum
        .min
        planes(s"$am")
          .map { n => try n.toInt catch _ => 0 }
          .sum
    }

    require(monads.size <= 1)

    (monads.headOption.getOrElse(iterable), models.head, wildcard, multiplier)

  protected def apply(monad: Monad, model: Model)
                     (wildcard: Wildcard, multiplier: Multiplier[Nest])
                     (started: Date, ended: Date)
               (using Board, Long, Flavor.Value): T

  protected def apply(data: Any,
                      board: Either[(Int, (Boolean, Long)), Board],
                      max: Long,
                      flavor: String
  ): T =
    val started = new Date()

    given Board = board.getOrElse {
      val (size, (empty, seed)) = board.left.get
      TryBoard(size, empty, 1)(using if seed == Long.MinValue then None else Some(seed))
    }

    var ended = new Date()

    given Long = max

    given Flavor.Value = Flavor(flavor) match
      case Some(it) => it
      case _ => throw FlavorException(flavor, None)

    val (model, planes) = request(data)
    val (monad, ml, wildcard, multiplier) = preprocess(model, planes)

    this(monad, ml)(wildcard, multiplier)(started, ended)
