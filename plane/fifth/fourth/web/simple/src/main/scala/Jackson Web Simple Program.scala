package queens
package plane.fifth.fourth
package web.simple

import base.Board

import pojo.jackson.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }


abstract trait `Jackson Web Simple Program`[It <: AnyRef]
    extends `Base.Web Simple Program`[It, Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]
