package object queens {

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]


  type Identity_[A] = A

  type Iterable_[X[_], A] = Iterable[A]


  import java.util.UUID

  import base.Board

  import version.less.nest.Nest

  type Cue = ((Board, Nest, Long, UUID), Long, Long, Long, Long)


  given Conversion[String, List[String]] = _
    .split("[\\[{(<.: ;,>)}\\]]")
    .filterNot(_.isEmpty)
    .map(_.replace('_', ' '))
    .toList

}
