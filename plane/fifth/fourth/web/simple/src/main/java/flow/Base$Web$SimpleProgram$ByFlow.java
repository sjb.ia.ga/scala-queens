package queens.plane.fifth.fourth.web.simple.flow;

import java.util.UUID;
import java.util.Date;
import java.util.LinkedList;
import static java.util.Arrays.asList;
import static java.util.Collections.sort;

import scala.Conversion;
import scala.Enumeration;
import scala.Option;
import scala.Tuple2;
import scala.util.Right;
import scala.collection.immutable.Seq;
import scala.reflect.ClassTag;
import static scala.runtime.BoxedUnit.UNIT;
import static scala.collection.JavaConverters.asScala;

import queens.base.Board;

import queens.common.package$;

import queens.dimensions.third.first.Iteration1;

import queens.version.less.nest.Nest;
import queens.version.less.nest.Wildcard;
import queens.version.base.Multiplier;

import static queens.Util.fun0;

import queens.pojo.parser.date.jDate;

import queens.pojo.base.Queens;
import queens.pojo.base.Board2;
import queens.pojo.base.Square2;
import queens.pojo.base.Solution2;
import queens.pojo.base.Point2;
import queens.pojo.base.Queens2;
import queens.pojo.base.Nest2;

import queens.dimensions.fifth.fourth.third.web.simple.flow.QueensWebSimpleProgramByFlow;

import queens.plane.fifth.fourth.web.simple.Base$Web$SimpleProgram;


public interface Base$Web$SimpleProgram$ByFlow<R extends Square2<R>, B extends Board2<R, B>, P extends Point2<P>, S extends Solution2<R, B, P, S>, N extends Nest2<N>, Q extends Queens2<R, B, P, S, N, Q>, O extends Queens<R, B, P, S, N, Q, O>>
    extends Base$Web$SimpleProgram<Iteration1, R, B, P, S, N, Q, O>,
            QueensWebSimpleProgramByFlow<O, B, R, S, P, Q, N> {

    B b2b2(Board given_Board);

    default Tuple2<O, Long> byFlow(Wildcard wildcard,
                                   Multiplier<Nest> multiplier,
                                   Board given_Board,
                                   Long given_Long,
                                   Enumeration.Value given_Flavor) {
        Date started = new Date();

        O response = package$.MODULE$.<O>given_Conversion_ClassTag_T().apply(queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_o());

        response.setBoard(b2b2(given_Board));

        UUID tag = apply(response);

        Iteration1 given_Iteration = new Iteration1(asScala(asList(given_Board, tag, false, Right.apply(false), new queens.common.Flag(null), fun0(UNIT))).toSeq());

        response.setQueens(new LinkedList<Q>());

        long idle = jDate.spanFrom(0L, started);

        idle += (long) apply(wildcard, multiplier, given_Iteration, given_Long, given_Flavor);

        Date ended = new Date();

        response.setCount(response.getQueens().size());

        sort(response.getBoard().getSquares());

        sort(response.getQueens());

        apply(tag, response);

        idle = jDate.spanFrom(idle, ended);

        return Tuple2.apply(response, idle);
    }

}
