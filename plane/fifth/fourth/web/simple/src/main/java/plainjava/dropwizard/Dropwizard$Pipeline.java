package queens.plane.fifth.fourth.web.simple.plainjava.dropwizard;

import java.util.UUID;
import java.util.List;

import scala.Tuple2;

import queens.common.pipeline.Context.Round;

import queens.dimensions.fifth.plainjava.PlainJAVA$Solution$Pipeline;


public class Dropwizard$Pipeline
    extends PlainJAVA$Solution$Pipeline {
    @Override
    protected boolean apply(List<Tuple2<Object, Object>> it,
                            Round round,
                            UUID uuid) {
        StringBuilder sb = new StringBuilder();
        for (Tuple2<Object, Object> p : it)
            sb
                .append(" ")
                .append(p._1())
                .append(" x ")
                .append(p._2())
                .append(" ;");

        System.out.println("Solution: " + sb.toString());
        System.out.println("Nest: "
                           + round.queens().aspect().toString()
                           + round.queens().algorithm().toString()
                           + round.queens().model().toString());
        System.out.println("Number: " + round.number());

        return true;
    }
}
