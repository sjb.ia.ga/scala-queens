package queens.plane.fifth.fourth.web.simple.plainjava.springboot;

import org.springframework.boot.SpringApplication;

import static queens.main.Properties.Validator$;


public class Main {

    static {
        System.setProperty("server.port", "8758");

        System.setProperty("debug", "false");

        System.setProperty(Validator$.MODULE$.plainjava(), "true");
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringBoot$App.class);
    }

}
