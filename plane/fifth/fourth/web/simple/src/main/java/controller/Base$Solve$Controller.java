package queens.plane.fifth.fourth.web.simple.controller;

import java.util.UUID;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import scala.Enumeration;
import scala.Option;
import scala.Function1;
import scala.Tuple2;
import scala.NotImplementedError;
import scala.collection.immutable.List;

import queens.base.Board;

import queens.dimensions.Dimension.Model;

import queens.common.Pipeline;
import queens.common.Pipeline.Noop;
import queens.common.pipeline.Context;
import queens.common.pipeline.Context.Round;

import queens.version.less.nest.Nest;
import queens.version.less.nest.Wildcard;
import queens.version.base.Multiplier;

import queens.pojo.parser.date.jDate;

import queens.pojo.base.Queens;
import queens.pojo.base.Board2;
import queens.pojo.base.Square2;
import queens.pojo.base.Solution2;
import queens.pojo.base.Point2;
import queens.pojo.base.Queens2;
import queens.pojo.base.Nest2;

import queens.plane.fifth.fourth.web.simple.flow.Base$Web$SimpleProgram$ByFlow;


public abstract class Base$Solve$Controller<R extends Square2<R>, B extends Board2<R, B>, P extends Point2<P>, S extends Solution2<R, B, P, S>, N extends Nest2<N>, Q extends Queens2<R, B, P, S, N, Q>, O extends Queens<R, B, P, S, N, Q, O>>
    extends Base$Controller<O>
    implements Base$Web$SimpleProgram$ByFlow<R, B, P, S, N, Q, O> {

    private static queens.dimensions.Dimension.Model$ Model$ = queens.dimensions.Dimension.Model$.MODULE$;

    protected Base$Solve$Controller(Option<Function1<Context, Pipeline>>... next) {
        __pipeline_next = Option
            .apply(
                  new Function1<Context, Pipeline>() {
                      @Override
                      public Pipeline apply(Context ctxt) {
                          Round round = (Round) ctxt;
                          if (round.queens().model().equals(Model$.flow()))
                              if (next.length > 0 && next[0].nonEmpty())
                                  return next[0].get().apply(ctxt);
                          else
                              throw new NotImplementedError();
                          return new Noop<List<Tuple2<Object, Object>>>(ctxt);
                      }
                  }
                  );
    }

    @Override
    protected O apply(Model model,
                      Wildcard wildcard,
                      Multiplier<Nest> multiplier,
                      Date started, Date ended, final long idle,
                      Board given_Board, Long given_Long, Enumeration.Value given_Flavor_Value) {
        O response;
        long idleSolving;

        {
            if (model.equals(Model$.actors()))
                throw new NotImplementedError();
            else if (model.equals(Model$.futures()))
                throw new NotImplementedError();
            else if (model.equals(Model$.flow())) {
                Tuple2<O, Long> byFlow = byFlow(wildcard, multiplier, given_Board, given_Long, given_Flavor_Value);
                response = byFlow._1();
                idleSolving = byFlow._2();
            } else
                throw new NotImplementedError();
        }

        Date endedSolving = new Date();

        jDate.apply(response.getBoard(), started, ended, Option.empty());

        jDate.apply(response, started, new Date(), Option.apply(jDate.spanFrom(idle, endedSolving) + idleSolving));

        return response;
    }

    private Option<Function1<Context, Pipeline>> __pipeline_next;
    private Tuple2<ConcurrentHashMap<UUID, O>, ConcurrentHashMap<Tuple2<UUID, UUID>, Q>> __webuse_data = Tuple2.apply(new ConcurrentHashMap<>(), new ConcurrentHashMap<>());

    @Override
    public Option<Function1<Context, Pipeline>> next() {
        return __pipeline_next;
    }

    public void queens$dimensions$fifth$web$QueensWebUseSolution$_setter_$queens$dimensions$fifth$web$QueensWebUseSolution$$data_$eq(Tuple2 data) {
        this.__webuse_data = data;
    }

    public Tuple2<ConcurrentHashMap<UUID, O>, ConcurrentHashMap<Tuple2<UUID, UUID>, Q>> queens$dimensions$fifth$web$QueensWebUseSolution$$data() {
        return this.__webuse_data;
    }
}
