package queens.plane.fifth.fourth.web.simple.plainjava.dropwizard.resources;

import java.util.List;
import java.util.LinkedList;
import java.util.HashMap;

import scala.Option;
import scala.Function1;
import scala.Tuple2;
import scala.Tuple4;
import scala.util.Left;
import scala.collection.Map;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.PathSegment;
import jakarta.ws.rs.core.MultivaluedMap;
import static jakarta.ws.rs.core.MediaType.TEXT_PLAIN;
import static jakarta.ws.rs.core.MediaType.APPLICATION_JSON;

import queens.common.Pipeline;
import queens.common.pipeline.Context;
import queens.dimensions.fifth.QueensUseSolution;

import queens.pojo.jackson.Queens;

import static queens.pojo.parser.text.jText.toText;
import static queens.pojo.parser.write.jWrite.toData;

import static queens.dimensions.fourth.webapp.Web$times.FlavorException;

import queens.plane.fifth.fourth.web.simple.servlet.container.controller.Jackson$Web$ServletContainer$Controller;


@Path("/queens/solve")
public class Resource$Dropwizard$Controller
    extends Jackson$Web$ServletContainer$Controller {

    @Override
    final public boolean plainJAVA() {
        return true;
    }

    @Override
    final public boolean webXML() {
        return false;
    }

    public Resource$Dropwizard$Controller(Option<Function1<Context, Pipeline>> nextByFlow) {
        super(nextByFlow);
    }

    public Resource$Dropwizard$Controller() {
        this(Option.empty());
    }


    @GET
    @Path("model/{model}")
    @Produces(TEXT_PLAIN)
    public String solve(@PathParam("model")                                         PathSegment segment,
                        @QueryParam("size")   @DefaultValue("6")                    int size,
                        @QueryParam("empty")  @DefaultValue("true")                 boolean empty,
                        @QueryParam("seed")   @DefaultValue("-9223372036854775808") long seed,
                        @QueryParam("max")    @DefaultValue("9223372036854775807")  long max,
                        @QueryParam("flavor") @DefaultValue("build")                String flavor,
                        @QueryParam("square") @DefaultValue("")                     String square,
                        @QueryParam("piece")  @DefaultValue("")                     String piece,
                        @QueryParam("queen")  @DefaultValue("")                     String queen,
                        @QueryParam("indent") @DefaultValue("2")                    int indent
                        ) {
        String model = segment.getPath();
        java.util.Map<String, List<String>> planes = segment.getMatrixParameters();
        Queens pojo = this.apply(Tuple2.apply(model, planes), Left.apply(Tuple2.apply(size, Tuple2.apply(empty, seed))), max, flavor);
        Map<String, Object> data = toData(pojo, square, piece, queen);
        return toText(data, indent, " ", "\n", Tuple4.apply(":", "-", "[", "]"));
    }

    @GET
    @Path("model/{model}")
    @Produces(APPLICATION_JSON)
    public Queens solve(@PathParam("model")                                         PathSegment segment,
                        @QueryParam("size")   @DefaultValue("6")                    int size,
                        @QueryParam("empty")  @DefaultValue("true")                 boolean empty,
                        @QueryParam("seed")   @DefaultValue("-9223372036854775808") long seed,
                        @QueryParam("max")    @DefaultValue("9223372036854775807")  long max,
                        @QueryParam("flavor") @DefaultValue("build")                String flavor
                        ) {
        String model = segment.getPath();
        java.util.Map<String, List<String>> planes = segment.getMatrixParameters();
        return this.apply(Tuple2.apply(model, planes), Left.apply(Tuple2.apply(size, Tuple2.apply(empty, seed))), max, flavor);
    }


    @Override
    public String toString() {
        return super.toString() + " :: Dropwizard";
    }
}
