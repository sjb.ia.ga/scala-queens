package queens.plane.fifth.fourth.web.simple.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

import scala.Enumeration;
import scala.Option;
import scala.Function1;
import scala.Tuple2;
import scala.Tuple3;
import scala.util.Either;
import scala.collection.immutable.Seq;
import static scala.collection.JavaConverters.asScala;

import queens.base.Board;

import queens.dimensions.Dimension.Aspect;
import queens.dimensions.Dimension.Algorithm;
import queens.dimensions.Dimension.Model;

import queens.version.less.nest.Nest;
import queens.version.less.nest.Wildcard;
import queens.version.base.Multiplier;

import queens.pojo.parser.date.jDate;

import queens.plane.fourth.fifth.third.TryBoard;

import static queens.dimensions.fourth.webapp.Web$times.FlavorException;


public abstract class Base$Controller<T> {

    private static queens.dimensions.Dimension.Aspect$ Aspect$ = queens.dimensions.Dimension.Aspect$.MODULE$;
    private static queens.dimensions.Dimension.Algorithm$ Algorithm$ = queens.dimensions.Dimension.Algorithm$.MODULE$;
    private static queens.dimensions.Dimension.Model$ Model$ = queens.dimensions.Dimension.Model$.MODULE$;
    private static queens.dimensions.three.breed.Hatch.Flavor$ Flavor$ = queens.dimensions.three.breed.Hatch.Flavor$.MODULE$;
    private static queens.version.less.nest.Wildcard$ Wildcard$ = queens.version.less.nest.Wildcard$.MODULE$;

    protected Tuple2<String, Map<String, List<String>>> request(Object data) {
        return (Tuple2<String, Map<String, List<String>>>) data;
    }

    private Tuple3<Model, Wildcard, Multiplier<Nest>> preprocess(String model, Map<String, List<String>> planes) {
        Set<String> params = planes.keySet();

        HashSet<Aspect> aspects = new HashSet<>();
        HashSet<Algorithm> algorithms = new HashSet<>();
        HashSet<Model> models = new HashSet<>();

        {
            String param = model;
            Option<Model> opt = Model$.apply(param);
            if (opt.nonEmpty())
                models.add(opt.get());
        }

        for (String param: params) {
            {
                Option<Aspect> opt = Aspect$.apply(param);
                if (opt.nonEmpty()) {
                    aspects.add(opt.get());
                    continue;
                }
            }
            {
                Option<Algorithm> opt = Algorithm$.apply(param);
                if (opt.nonEmpty()) {
                    algorithms.add(opt.get());
                    continue;
                }
            }
        }

        HashSet<Nest> nests = new HashSet<>();

        for (Aspect at: aspects)
            for (Algorithm am: algorithms)
                for (Model ml: models)
                    nests.add(new Nest(at, am, ml));

        Wildcard wildcard;
        {
            Seq<Nest> seq = asScala(nests).toSeq();
            wildcard = Wildcard$.apply(Wildcard$.apply(seq), Wildcard$.$times());
        }

        Multiplier<Nest> multiplier =
            new Multiplier<Nest>(
                new Function1<Nest, Object>() {
                    @Override
                    public Object apply(Nest nest) {
                        String at = nest.aspect().toString();
                        String am = nest.algorithm().toString();
                        int sum = 0;
                        for (String n: planes.get(at))
                            try { sum += Integer.parseInt(n); } catch (NumberFormatException __) {}
                        int min = sum;
                        sum = 0;
                        for (String n: planes.get(am))
                            try { sum += Integer.parseInt(n); } catch (NumberFormatException __) {}
                        return Math.min(min, sum);
                    }
                }
            );

        return Tuple3.apply(models.iterator().next(), wildcard, multiplier);
    }

    protected abstract T apply(Model model,
                               Wildcard wildcard,
                               Multiplier<Nest> multiplier,
                               Date started, Date ended, long idle,
                               Board given_Board, Long given_Long, Enumeration.Value given_Flavor_Value);

    protected T apply(Object data,
                      Either<Tuple2<Integer, Tuple2<Boolean, Long>>, Board> board,
                      long max,
                      String flavor) {
        Date started = new Date();

        Board given_Board = board.isRight() ? board.right().get() : TryBoard.apply(board.left().get()._1(), board.left().get()._2()._1(), 1, board.left().get()._2()._2() == Long.MIN_VALUE ? Option.empty() : Option.apply(board.left().get()._2()._2()));

        Date ended = new Date();

        long idle = 0L;

        long given_Long = max;

        Option<Enumeration.Value> given_Flavor_Value = Flavor$.apply(flavor);

        if (given_Flavor_Value.isEmpty() ||
            !(given_Flavor_Value.get().equals(Flavor$.build()) ||
              given_Flavor_Value.get().equals(Flavor$.config())))
            throw new FlavorException(flavor, given_Flavor_Value);

        Tuple2<String, Map<String, List<String>>> request = request(data);
        String model = request._1();
        Map<String, List<String>> planes = request._2();

        Tuple3<Model, Wildcard, Multiplier<Nest>> result = preprocess(model, planes);

        Model ml = result._1();
        Wildcard wildcard = result._2();
        Multiplier<Nest> multiplier = result._3();

        return apply(ml,
                     wildcard, multiplier,
                     started, ended, jDate.spanFrom(idle, ended),
                     given_Board, given_Long, given_Flavor_Value.get());
    }
}
