package queens.plane.fifth.fourth.web.simple.flow;

import scala.Conversion;
import scala.reflect.ClassTag;

import queens.base.Board;

import queens.dimensions.third.first.Iteration1;

import queens.pojo.moshi.Queens;
import queens.pojo.moshi.Board2;
import queens.pojo.moshi.Square2;
import queens.pojo.moshi.Solution2;
import queens.pojo.moshi.Point2;
import queens.pojo.moshi.Queens2;
import queens.pojo.moshi.Nest2;

import static queens.pojo.moshi.jPojoMoshi.classTagQueens;
import static queens.pojo.moshi.jPojoMoshi.classTagBoard2;
import static queens.pojo.moshi.jPojoMoshi.classTagSquare2;
import static queens.pojo.moshi.jPojoMoshi.classTagSolution2;
import static queens.pojo.moshi.jPojoMoshi.classTagPoint2;
import static queens.pojo.moshi.jPojoMoshi.classTagQueens2;
import static queens.pojo.moshi.jPojoMoshi.classTagNest2;

import queens.pojo.moshi.jPojoMoshi$;

import queens.plane.fifth.fourth.web.simple.Moshi$Web$SimpleProgram;


public interface Moshi$Web$SimpleProgram$ByFlow
    extends Base$Web$SimpleProgram$ByFlow<Square2, Board2, Point2, Solution2, Nest2, Queens2, Queens>,
            Moshi$Web$SimpleProgram<Iteration1> {

    default Board2 b2b2(Board given_Board) {
        Conversion<Board, Board2> given_Conversion_Board_Board2 = jPojoMoshi$.MODULE$.given_Conversion_Board_Board2();
        return given_Conversion_Board_Board2.apply(given_Board);
    }

    default ClassTag<Queens> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_o() {
        return classTagQueens();
    }

    default ClassTag<Board2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_b() {
        return classTagBoard2();
    }

    default ClassTag<Square2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_r() {
        return classTagSquare2();
    }

    default ClassTag<Solution2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_s() {
        return classTagSolution2();
    }

    default ClassTag<Point2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_p() {
        return classTagPoint2();
    }

    default ClassTag<Queens2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_q() {
        return classTagQueens2();
    }

    default ClassTag<Nest2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_n() {
        return classTagNest2();
    }

}
