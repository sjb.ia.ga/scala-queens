package queens.plane.fifth.fourth.web.simple.servlet.container.flow;

import scala.reflect.ClassTag;

import queens.dimensions.third.first.Iteration1;

import queens.pojo.gson.Queens;
import queens.pojo.gson.Board2;
import queens.pojo.gson.Square2;
import queens.pojo.gson.Solution2;
import queens.pojo.gson.Point2;
import queens.pojo.gson.Queens2;
import queens.pojo.gson.Nest2;

import static queens.pojo.gson.jPojoGson.classTagQueens;
import static queens.pojo.gson.jPojoGson.classTagBoard2;
import static queens.pojo.gson.jPojoGson.classTagSquare2;
import static queens.pojo.gson.jPojoGson.classTagSolution2;
import static queens.pojo.gson.jPojoGson.classTagPoint2;
import static queens.pojo.gson.jPojoGson.classTagQueens2;
import static queens.pojo.gson.jPojoGson.classTagNest2;

import queens.dimensions.fifth.fourth.third.web.simple.servlet.container.flow.QueensWebServletContainerSimpleProgramByFlow;
import queens.plane.fifth.fourth.web.simple.flow.Gson$Web$SimpleProgram$ByFlow;
import queens.plane.fifth.fourth.web.simple.servlet.container.Gson$Web$ServletContainer$SimpleProgram;


public interface Gson$Web$ServletContainer$SimpleProgram$ByFlow
    extends QueensWebServletContainerSimpleProgramByFlow<Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2>,
            Gson$Web$ServletContainer$SimpleProgram<Iteration1>,
            Gson$Web$SimpleProgram$ByFlow {

    default ClassTag<Queens> queens$dimensions$fifth$fourth$third$web$simple$servlet$container$flow$QueensWebServletContainerSimpleProgramByFlow$$ct_o() {
        return classTagQueens();
    }

    default ClassTag<Board2> queens$dimensions$fifth$fourth$third$web$simple$servlet$container$flow$QueensWebServletContainerSimpleProgramByFlow$$ct_b() {
        return classTagBoard2();
    }

    default ClassTag<Square2> queens$dimensions$fifth$fourth$third$web$simple$servlet$container$flow$QueensWebServletContainerSimpleProgramByFlow$$ct_r() {
        return classTagSquare2();
    }

    default ClassTag<Solution2> queens$dimensions$fifth$fourth$third$web$simple$servlet$container$flow$QueensWebServletContainerSimpleProgramByFlow$$ct_s() {
        return classTagSolution2();
    }

    default ClassTag<Point2> queens$dimensions$fifth$fourth$third$web$simple$servlet$container$flow$QueensWebServletContainerSimpleProgramByFlow$$ct_p() {
        return classTagPoint2();
    }

    default ClassTag<Queens2> queens$dimensions$fifth$fourth$third$web$simple$servlet$container$flow$QueensWebServletContainerSimpleProgramByFlow$$ct_q() {
        return classTagQueens2();
    }

    default ClassTag<Nest2> queens$dimensions$fifth$fourth$third$web$simple$servlet$container$flow$QueensWebServletContainerSimpleProgramByFlow$$ct_n() {
        return classTagNest2();
    }

}
