package queens.plane.fifth.fourth.web.simple.flow;

import scala.Conversion;
import scala.reflect.ClassTag;

import queens.base.Board;

import queens.dimensions.third.first.Iteration1;

import queens.pojo.gson.Queens;
import queens.pojo.gson.Board2;
import queens.pojo.gson.Square2;
import queens.pojo.gson.Solution2;
import queens.pojo.gson.Point2;
import queens.pojo.gson.Queens2;
import queens.pojo.gson.Nest2;

import static queens.pojo.gson.jPojoGson.classTagQueens;
import static queens.pojo.gson.jPojoGson.classTagBoard2;
import static queens.pojo.gson.jPojoGson.classTagSquare2;
import static queens.pojo.gson.jPojoGson.classTagSolution2;
import static queens.pojo.gson.jPojoGson.classTagPoint2;
import static queens.pojo.gson.jPojoGson.classTagQueens2;
import static queens.pojo.gson.jPojoGson.classTagNest2;

import queens.pojo.gson.jPojoGson$;

import queens.plane.fifth.fourth.web.simple.Gson$Web$SimpleProgram;


public interface Gson$Web$SimpleProgram$ByFlow
    extends Base$Web$SimpleProgram$ByFlow<Square2, Board2, Point2, Solution2, Nest2, Queens2, Queens>,
            Gson$Web$SimpleProgram<Iteration1> {

    default Board2 b2b2(Board given_Board) {
        Conversion<Board, Board2> given_Conversion_Board_Board2 = jPojoGson$.MODULE$.given_Conversion_Board_Board2();
        return given_Conversion_Board_Board2.apply(given_Board);
    }

    default ClassTag<Queens> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_o() {
        return classTagQueens();
    }

    default ClassTag<Board2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_b() {
        return classTagBoard2();
    }

    default ClassTag<Square2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_r() {
        return classTagSquare2();
    }

    default ClassTag<Solution2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_s() {
        return classTagSolution2();
    }

    default ClassTag<Point2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_p() {
        return classTagPoint2();
    }

    default ClassTag<Queens2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_q() {
        return classTagQueens2();
    }

    default ClassTag<Nest2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_n() {
        return classTagNest2();
    }

}
