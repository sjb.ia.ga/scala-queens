package queens.plane.fifth.fourth.web.simple.reactive.flow;

import scala.reflect.ClassTag;

import queens.dimensions.third.first.Iteration1;

import queens.pojo.jackson.Queens;
import queens.pojo.jackson.Board2;
import queens.pojo.jackson.Square2;
import queens.pojo.jackson.Solution2;
import queens.pojo.jackson.Point2;
import queens.pojo.jackson.Queens2;
import queens.pojo.jackson.Nest2;

import static queens.pojo.jackson.jPojoJackson.classTagQueens;
import static queens.pojo.jackson.jPojoJackson.classTagBoard2;
import static queens.pojo.jackson.jPojoJackson.classTagSquare2;
import static queens.pojo.jackson.jPojoJackson.classTagSolution2;
import static queens.pojo.jackson.jPojoJackson.classTagPoint2;
import static queens.pojo.jackson.jPojoJackson.classTagQueens2;
import static queens.pojo.jackson.jPojoJackson.classTagNest2;

import queens.dimensions.fifth.fourth.third.web.simple.reactive.flow.QueensWebReactiveSimpleProgramByFlow;
import queens.plane.fifth.fourth.web.simple.flow.Jackson$Web$SimpleProgram$ByFlow;
import queens.plane.fifth.fourth.web.simple.reactive.Jackson$Web$Reactive$SimpleProgram;


public interface Jackson$Web$Reactive$SimpleProgram$ByFlow
    extends QueensWebReactiveSimpleProgramByFlow<Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2>,
            Jackson$Web$Reactive$SimpleProgram<Iteration1>,
            Jackson$Web$SimpleProgram$ByFlow {

    default ClassTag<Queens> queens$dimensions$fifth$fourth$third$web$simple$reactive$flow$QueensWebReactiveSimpleProgramByFlow$$ct_o() {
        return classTagQueens();
    }

    default ClassTag<Board2> queens$dimensions$fifth$fourth$third$web$simple$reactive$flow$QueensWebReactiveSimpleProgramByFlow$$ct_b() {
        return classTagBoard2();
    }

    default ClassTag<Square2> queens$dimensions$fifth$fourth$third$web$simple$reactive$flow$QueensWebReactiveSimpleProgramByFlow$$ct_r() {
        return classTagSquare2();
    }

    default ClassTag<Solution2> queens$dimensions$fifth$fourth$third$web$simple$reactive$flow$QueensWebReactiveSimpleProgramByFlow$$ct_s() {
        return classTagSolution2();
    }

    default ClassTag<Point2> queens$dimensions$fifth$fourth$third$web$simple$reactive$flow$QueensWebReactiveSimpleProgramByFlow$$ct_p() {
        return classTagPoint2();
    }

    default ClassTag<Queens2> queens$dimensions$fifth$fourth$third$web$simple$reactive$flow$QueensWebReactiveSimpleProgramByFlow$$ct_q() {
        return classTagQueens2();
    }

    default ClassTag<Nest2> queens$dimensions$fifth$fourth$third$web$simple$reactive$flow$QueensWebReactiveSimpleProgramByFlow$$ct_n() {
        return classTagNest2();
    }

}
