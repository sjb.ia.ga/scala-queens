package queens.plane.fifth.fourth.web.simple.flow;

import scala.Conversion;
import scala.reflect.ClassTag;

import queens.base.Board;

import queens.dimensions.third.first.Iteration1;

import queens.pojo.jackson.Queens;
import queens.pojo.jackson.Board2;
import queens.pojo.jackson.Square2;
import queens.pojo.jackson.Solution2;
import queens.pojo.jackson.Point2;
import queens.pojo.jackson.Queens2;
import queens.pojo.jackson.Nest2;

import static queens.pojo.jackson.jPojoJackson.classTagQueens;
import static queens.pojo.jackson.jPojoJackson.classTagBoard2;
import static queens.pojo.jackson.jPojoJackson.classTagSquare2;
import static queens.pojo.jackson.jPojoJackson.classTagSolution2;
import static queens.pojo.jackson.jPojoJackson.classTagPoint2;
import static queens.pojo.jackson.jPojoJackson.classTagQueens2;
import static queens.pojo.jackson.jPojoJackson.classTagNest2;

import queens.pojo.jackson.jPojoJackson$;

import queens.plane.fifth.fourth.web.simple.Jackson$Web$SimpleProgram;


public interface Jackson$Web$SimpleProgram$ByFlow
    extends Base$Web$SimpleProgram$ByFlow<Square2, Board2, Point2, Solution2, Nest2, Queens2, Queens>,
            Jackson$Web$SimpleProgram<Iteration1> {

    default Board2 b2b2(Board given_Board) {
        Conversion<Board, Board2> given_Conversion_Board_Board2 = jPojoJackson$.MODULE$.given_Conversion_Board_Board2();
        return given_Conversion_Board_Board2.apply(given_Board);
    }

    default ClassTag<Queens> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_o() {
        return classTagQueens();
    }

    default ClassTag<Board2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_b() {
        return classTagBoard2();
    }

    default ClassTag<Square2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_r() {
        return classTagSquare2();
    }

    default ClassTag<Solution2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_s() {
        return classTagSolution2();
    }

    default ClassTag<Point2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_p() {
        return classTagPoint2();
    }

    default ClassTag<Queens2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_q() {
        return classTagQueens2();
    }

    default ClassTag<Nest2> queens$dimensions$fifth$fourth$third$web$simple$flow$QueensWebSimpleProgramByFlow$$ct_n() {
        return classTagNest2();
    }
}
