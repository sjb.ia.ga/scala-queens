package queens.plane.fifth.fourth.web.simple;

import scala.reflect.ClassTag;

import queens.pojo.moshi.Queens;
import queens.pojo.moshi.Board2;
import queens.pojo.moshi.Square2;
import queens.pojo.moshi.Solution2;
import queens.pojo.moshi.Point2;
import queens.pojo.moshi.Queens2;
import queens.pojo.moshi.Nest2;

import static queens.pojo.moshi.jPojoMoshi.classTagQueens;
import static queens.pojo.moshi.jPojoMoshi.classTagBoard2;
import static queens.pojo.moshi.jPojoMoshi.classTagSquare2;
import static queens.pojo.moshi.jPojoMoshi.classTagSolution2;
import static queens.pojo.moshi.jPojoMoshi.classTagPoint2;
import static queens.pojo.moshi.jPojoMoshi.classTagQueens2;
import static queens.pojo.moshi.jPojoMoshi.classTagNest2;


public interface Moshi$Web$SimpleProgram<It>
    extends Base$Web$SimpleProgram<It, Square2, Board2, Point2, Solution2, Nest2, Queens2, Queens> {

    default ClassTag<Queens> queens$dimensions$fifth$fourth$web$simple$QueensWebSimpleProgram$$ct_o() {
        return classTagQueens();
    }

    default ClassTag<Board2> queens$dimensions$fifth$fourth$web$simple$QueensWebSimpleProgram$$ct_b() {
        return classTagBoard2();
    }

    default ClassTag<Square2> queens$dimensions$fifth$fourth$web$simple$QueensWebSimpleProgram$$ct_r() {
        return classTagSquare2();
    }

    default ClassTag<Solution2> queens$dimensions$fifth$fourth$web$simple$QueensWebSimpleProgram$$ct_s() {
        return classTagSolution2();
    }

    default ClassTag<Point2> queens$dimensions$fifth$fourth$web$simple$QueensWebSimpleProgram$$ct_p() {
        return classTagPoint2();
    }

    default ClassTag<Queens2> queens$dimensions$fifth$fourth$web$simple$QueensWebSimpleProgram$$ct_q() {
        return classTagQueens2();
    }

    default ClassTag<Nest2> queens$dimensions$fifth$fourth$web$simple$QueensWebSimpleProgram$$ct_n() {
        return classTagNest2();
    }

}
