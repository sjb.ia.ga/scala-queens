package queens.plane.fifth.fourth.web.simple.servlet.container.controller;

import scala.Option;
import scala.Function1;

import queens.common.Pipeline;
import queens.common.pipeline.Context;

import queens.pojo.jackson.Queens;
import queens.pojo.jackson.Board2;
import queens.pojo.jackson.Square2;
import queens.pojo.jackson.Solution2;
import queens.pojo.jackson.Point2;
import queens.pojo.jackson.Queens2;
import queens.pojo.jackson.Nest2;

import queens.plane.fifth.fourth.web.simple.controller.Base$Solve$Controller;
import queens.plane.fifth.fourth.web.simple.servlet.container.flow.Jackson$Web$ServletContainer$SimpleProgram$ByFlow;


public abstract class Jackson$Web$ServletContainer$Controller
    extends Base$Solve$Controller<Square2, Board2, Point2, Solution2, Nest2, Queens2, Queens>
    implements Jackson$Web$ServletContainer$SimpleProgram$ByFlow {

    protected Jackson$Web$ServletContainer$Controller(Option<Function1<Context, Pipeline>>... next) {
        super(next);
    }
}
