package queens.plane.fifth.fourth.web.simple.plainjava.dropwizard;

import io.dropwizard.core.Application;
import io.dropwizard.core.setup.Bootstrap;
import io.dropwizard.core.setup.Environment;

import static queens.main.Properties.Validator$;

import queens.plane.fifth.fourth.web.simple.plainjava.dropwizard.health.Dropwizard$HealthCheck;
import queens.plane.fifth.fourth.web.simple.plainjava.dropwizard.config.Dropwizard$Controller$Configuration;
import queens.plane.fifth.fourth.web.simple.plainjava.dropwizard.resources.Resource$Dropwizard$Controller;


public class Dropwizard$App
    extends Application<Dropwizard$Controller$Configuration> {

    @Override
    public String getName() {
        return "Queens Dropwizard Web Servlet";
    }

    @Override
    public void run(final Dropwizard$Controller$Configuration configuration, final Environment environment) throws Exception {
        configuration.setNextByFlow(new Dropwizard$Pipeline());

        environment.healthChecks().register("health", new Dropwizard$HealthCheck());
        environment.jersey().register(new Resource$Dropwizard$Controller(configuration.getNextByFlow()));
    }
 }
