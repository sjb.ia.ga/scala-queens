package queens.plane.fifth.fourth.web.simple;

import queens.pojo.base.Queens;
import queens.pojo.base.Board2;
import queens.pojo.base.Square2;
import queens.pojo.base.Solution2;
import queens.pojo.base.Point2;
import queens.pojo.base.Queens2;
import queens.pojo.base.Nest2;

import queens.dimensions.fifth.fourth.web.simple.QueensWebSimpleProgram;


public interface Base$Web$SimpleProgram<It, R extends Square2<R>, B extends Board2<R, B>, P extends Point2<P>, S extends Solution2<R, B, P, S>, N extends Nest2<N>, Q extends Queens2<R, B, P, S, N, Q>, O extends Queens<R, B, P, S, N, Q, O>>
    extends QueensWebSimpleProgram<It, O, B, R, S, P, Q, N> {
}
