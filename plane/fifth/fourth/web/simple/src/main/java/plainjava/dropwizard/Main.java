package queens.plane.fifth.fourth.web.simple.plainjava.dropwizard;

import static queens.main.Properties.Validator$;


public class Main {

    static {
        System.setProperty(Validator$.MODULE$.plainjava(), "true");
    }

    public static void main(final String[] args) throws Exception {
        new Dropwizard$App().run(args);
    }
 }
