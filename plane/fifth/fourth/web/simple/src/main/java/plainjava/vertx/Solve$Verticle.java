package queens.plane.fifth.fourth.web.simple.plainjava.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Router;

import queens.plane.fifth.fourth.web.simple.plainjava.vertx.controller.Handler$Vertx$Controller;


public class Solve$Verticle
    extends AbstractVerticle {
    @Override
    public void start(Promise<Void> startPromise) {
        Router router = Router.router(vertx);

        Handler$Vertx$Controller controller = new Handler$Vertx$Controller();

        router
            .route("/queens/solve/model/:matrix;")
            .method(HttpMethod.GET)
            .produces("application/json")
            .handler(controller::json);

        router
            .route("/queens/solve/model/:matrix")
            .method(HttpMethod.GET)
            .produces("application/json")
            .handler(controller::json);

        router
            .route("/queens/solve/model/:matrix;")
            .method(HttpMethod.GET)
            .produces("text/plain")
            .handler(controller::text);

        router
            .route("/queens/solve/model/:matrix")
            .method(HttpMethod.GET)
            .produces("text/plain")
            .handler(controller::text);

        vertx.createHttpServer()
                .requestHandler(router)
                .listen(8704, asyncStart -> {
                    if (asyncStart.succeeded()) {
                        startPromise.complete();
                        System.out.println("HTTP server running on port 8704");
                    } else {
                        System.out.printf("Woops %s", asyncStart.cause().toString());
                        startPromise.fail(asyncStart.cause());
                    }
                });
    }
}
