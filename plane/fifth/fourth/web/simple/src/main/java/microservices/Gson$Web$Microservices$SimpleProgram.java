package queens.plane.fifth.fourth.web.simple.microservices;

import scala.reflect.ClassTag;

import queens.pojo.gson.Queens;
import queens.pojo.gson.Board2;
import queens.pojo.gson.Square2;
import queens.pojo.gson.Solution2;
import queens.pojo.gson.Point2;
import queens.pojo.gson.Queens2;
import queens.pojo.gson.Nest2;

import static queens.pojo.gson.jPojoGson.classTagQueens;
import static queens.pojo.gson.jPojoGson.classTagBoard2;
import static queens.pojo.gson.jPojoGson.classTagSquare2;
import static queens.pojo.gson.jPojoGson.classTagSolution2;
import static queens.pojo.gson.jPojoGson.classTagPoint2;
import static queens.pojo.gson.jPojoGson.classTagQueens2;
import static queens.pojo.gson.jPojoGson.classTagNest2;

import queens.dimensions.fifth.fourth.web.simple.microservices.QueensWebMicroservicesSimpleProgram;
import queens.plane.fifth.fourth.web.simple.Gson$Web$SimpleProgram;


public interface Gson$Web$Microservices$SimpleProgram<It>
    extends QueensWebMicroservicesSimpleProgram<It, Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2>,
            Gson$Web$SimpleProgram<It> {

    default ClassTag<Queens> queens$dimensions$fifth$fourth$web$simple$microservices$QueensWebMicroservicesSimpleProgram$$ct_o() {
        return classTagQueens();
    }

    default ClassTag<Board2> queens$dimensions$fifth$fourth$web$simple$microservices$QueensWebMicroservicesSimpleProgram$$ct_b() {
        return classTagBoard2();
    }

    default ClassTag<Square2> queens$dimensions$fifth$fourth$web$simple$microservices$QueensWebMicroservicesSimpleProgram$$ct_r() {
        return classTagSquare2();
    }

    default ClassTag<Solution2> queens$dimensions$fifth$fourth$web$simple$microservices$QueensWebMicroservicesSimpleProgram$$ct_s() {
        return classTagSolution2();
    }

    default ClassTag<Point2> queens$dimensions$fifth$fourth$web$simple$microservices$QueensWebMicroservicesSimpleProgram$$ct_p() {
        return classTagPoint2();
    }

    default ClassTag<Queens2> queens$dimensions$fifth$fourth$web$simple$microservices$QueensWebMicroservicesSimpleProgram$$ct_q() {
        return classTagQueens2();
    }

    default ClassTag<Nest2> queens$dimensions$fifth$fourth$web$simple$microservices$QueensWebMicroservicesSimpleProgram$$ct_n() {
        return classTagNest2();
    }

}
