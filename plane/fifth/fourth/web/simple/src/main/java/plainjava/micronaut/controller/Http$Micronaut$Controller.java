package queens.plane.fifth.fourth.web.simple.plainjava.micronaut.controller;

import java.util.Arrays;
import java.util.List;

import scala.Option;
import scala.Function1;
import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;
import scala.util.Left;
import scala.collection.Map;
import static scala.collection.JavaConverters.asScala;

import static io.micronaut.http.MediaType.APPLICATION_JSON;
import static io.micronaut.http.MediaType.APPLICATION_XML;
import static io.micronaut.http.MediaType.TEXT_PLAIN;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.http.annotation.Produces;

import queens.common.Pipeline;
import queens.common.pipeline.Context;
import queens.dimensions.fifth.QueensUseSolution;

import queens.pojo.jackson.Queens;

import static queens.pojo.parser.text.jText.toText;
import static queens.pojo.parser.write.jWrite.toData;
import static queens.Util.matrix;

import queens.plane.fifth.fourth.web.simple.servlet.container.controller.Jackson$Web$ServletContainer$Controller;


@Controller("/queens/solve")
class Http$Micronaut$Controller
    extends Jackson$Web$ServletContainer$Controller {

    //    @Value("${spring.application.name}")
    String appName = null;

    @Override
    final public boolean plainJAVA() {
        return true;
    }

    @Override
    final public boolean webXML() {
        return false;
    }

    //@Autowired
    private Function1<Context, Pipeline> _next = null;

    @Override
    public Option<Function1<Context, Pipeline>> next() {
        return _next == null ? Option.empty() : Option.apply(_next);
    }


    @Get("/model/{data}")
    @Produces(TEXT_PLAIN)
    public String solve(@PathVariable("data")                              String data,
                        @QueryValue(defaultValue = "6")                    int size,
                        @QueryValue(defaultValue = "true")                 Boolean empty,
                        @QueryValue(defaultValue = "-9223372036854775808") long seed,
                        @QueryValue(defaultValue = "9223372036854775807")  long max,
                        @QueryValue(defaultValue = "build")                String flavor,
                        @QueryValue(defaultValue = "")                     String square,
                        @QueryValue(defaultValue = "")                     String piece,
                        @QueryValue(defaultValue = "")                     String queen,
                        @QueryValue(defaultValue = "2")                    int indent
                        ) {
        Queens pojo = this.apply(data, Left.apply(Tuple2.apply(size, Tuple2.apply(empty, seed))), max, flavor);
        Map<String, Object> map = toData(pojo, square, piece, queen);
        return toText(map, indent, " ", "\n", Tuple4.apply(":", "-", "[", "]"));
    }

    @Get("/model/{data}")
    @Produces({APPLICATION_JSON, APPLICATION_XML})
    public Queens solve(@PathVariable("data")                              String data,
                        @QueryValue(defaultValue = "6")                    int size,
                        @QueryValue(defaultValue = "true")                 Boolean empty,
                        @QueryValue(defaultValue = "-9223372036854775808") long seed,
                        @QueryValue(defaultValue = "9223372036854775807")  long max,
                        @QueryValue(defaultValue = "build")                String flavor
                        ) {
        return this.apply(data, Left.apply(Tuple2.apply(size, Tuple2.apply(empty, seed))), max, flavor);
    }


    @Override
    protected Tuple2<String, java.util.Map<String, List<String>>> request(Object obj) {
        List<String> data = Arrays.asList(((String) obj).split(";"));
        return Tuple2.apply(data.get(0), matrix(asScala(data).tail().toList()));
    }

    @Override
    public String toString() {
        return super.toString() + " :: Micronaut";
    }
}
