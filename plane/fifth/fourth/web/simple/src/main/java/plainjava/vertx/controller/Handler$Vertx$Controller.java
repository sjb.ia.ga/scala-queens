package queens.plane.fifth.fourth.web.simple.plainjava.vertx.controller;

import java.util.List;
import java.util.HashMap;
import java.util.Map.Entry;
import static java.util.Arrays.asList;

import scala.Option;
import scala.Function1;
import scala.Tuple2;
import scala.Tuple4;
import scala.util.Left;
import scala.collection.Map;
import static scala.collection.JavaConverters.asScala;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.vertx.ext.web.RoutingContext;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;
import io.vertx.core.json.jackson.DatabindCodec;

import queens.common.Pipeline;
import queens.common.pipeline.Context;
import queens.dimensions.fifth.QueensUseSolution;

import queens.pojo.jackson.Queens;

import static queens.pojo.parser.text.jText.toText;
import static queens.pojo.parser.write.jWrite.toData;

import static queens.Util.matrix;

import queens.plane.fifth.fourth.web.simple.reactive.controller.Jackson$Web$Reactive$Controller;


public class Handler$Vertx$Controller
    extends Jackson$Web$Reactive$Controller {

    public final static int OK = 200;
    public final static int BAD_REQUEST = 400;
    public final static int NOT_ACCEPTABLE = 406;
    public final static int INTERNAL_SERVER_ERROR = 500;

    public final static HashMap<String, String> params = new HashMap<>(8);

    static {
        params.put("size",   "6");                    // int
        params.put("empty",  "true");                 // boolean
        params.put("seed",   "-9223372036854775808"); // long
        params.put("max",    "9223372036854775807");  // long
        params.put("flavor", "build");                // string
        params.put("square", "");                     // string
        params.put("piece",  "");                     // string
        params.put("queen",  "");                     // string
        params.put("indent", "2");                    // int
    }

    @Override
    final public boolean plainJAVA() {
        return true;
    }

    //@Autowired
    private Function1<Context, Pipeline> _next = null;

    @Override
    public Option<Function1<Context, Pipeline>> next() {
        return _next == null ? Option.empty() : Option.apply(_next);
    }

    public void json(RoutingContext context) {
        Option<Queens> pojo = solve(context)._2();

        if (pojo.isEmpty())
            return;

        try {
            context.response()
                .setStatusCode(OK)
                // https://stackoverflow.com/questions/65234796/where-is-the-place-to-register-a-jackson-codec-in-vertx-4-0-using-custom-object
                .end(DatabindCodec.mapper().writeValueAsString(pojo.get()));
        } catch (JsonProcessingException __) {
            context.response()
                .setStatusCode(INTERNAL_SERVER_ERROR)
                .end();
        }
    }

    public void text(RoutingContext context) {
        Tuple2<HashMap<String, String>, Option<Queens>> tuple = solve(context);
        HashMap<String, String> queryParams = tuple._1();
        Option<Queens> pojo = tuple._2();

        if (pojo.isEmpty())
            return;

        String square = queryParams.get("square");
        String piece = queryParams.get("piece");
        String queen = queryParams.get("queen");

        Map<String, Object> data = toData(pojo.get(), square, piece, queen);

        int indent = 2;
        try { indent = Integer.parseInt(queryParams.get("indent")); } catch (NumberFormatException __) {}

        context.response()
            .setStatusCode(OK)
            .end(toText(data, indent, " ", "\n", Tuple4.apply(":", "-", "[", "]")));
    }

    private Tuple2<HashMap<String, String>, Option<Queens>> solve(RoutingContext context) {
        java.util.Map<String, String> pathParams = context.pathParams();
        String model_matrix = pathParams.get("matrix");
        int semicolon = model_matrix.indexOf(';');
        List<String> vars = asList(model_matrix.substring(semicolon+1).split(";"));
        String model = model_matrix.substring(0, semicolon);
        java.util.Map<String, List<String>> planes = matrix(asScala(vars).toList());

        MultiMap query = context.queryParams();
        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.putAll(params);

        for (Entry<String, String> param: query) {
            if (!params.containsKey(param.getKey())) {
                context.response()
                    .setStatusCode(BAD_REQUEST)
                    .end();
                return Tuple2.apply(queryParams, Option.empty());
            }
            queryParams.put(param.getKey(), param.getValue());
        }

        try {
            int size = Integer.parseInt(queryParams.get("size"));
            boolean empty = Boolean.parseBoolean(params.get("empty"));
            long seed = Long.parseLong(queryParams.get("seed"));
            long max = Long.parseLong(queryParams.get("max"));

            String flavor = queryParams.get("flavor");
            try {
                Queens pojo = this.apply(Tuple2.apply(model, planes), Left.apply(Tuple2.apply(size, Tuple2.apply(empty, seed))), max, flavor);
                return Tuple2.apply(queryParams, Option.apply(pojo));
            } catch (Exception __) {
                context.response()
                    .setStatusCode(INTERNAL_SERVER_ERROR)
                    .end();
                return Tuple2.apply(queryParams, Option.empty());
            }
        } catch (NumberFormatException __) {
            context.response()
                .setStatusCode(BAD_REQUEST)
                .end();
            return Tuple2.apply(queryParams, Option.empty());
        }
    }

    @Override
    public String toString() {
        return super.toString() + " :: Vert.x";
    }

}
