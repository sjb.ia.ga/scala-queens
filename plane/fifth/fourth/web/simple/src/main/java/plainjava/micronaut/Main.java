package queens.plane.fifth.fourth.web.simple.plainjava.micronaut;

import io.micronaut.runtime.Micronaut;

import static queens.main.Properties.Validator$;


public class Main {

    static {
        System.setProperty("micronaut.server.port", "8737");

        System.setProperty(Validator$.MODULE$.plainjava(), "true");
    }

    public static void main(String[] args) {

        Micronaut.run(Micronaut$App.class, args);

    }

}
