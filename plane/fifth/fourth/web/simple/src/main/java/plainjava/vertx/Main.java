package queens.plane.fifth.fourth.web.simple.plainjava.vertx;

import io.vertx.core.Vertx;

import static queens.main.Properties.Validator$;


public class Main {

    static {
        System.setProperty(Validator$.MODULE$.plainjava(), "true");
    }

    public static void main(String[] args) {
        Vertx.vertx().deployVerticle(new Solve$Verticle());
    }

}
