package queens.plane.fifth.fourth.web.simple.plainjava.dropwizard.config;

import scala.Option;
import scala.Function1;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.core.Configuration;

import queens.common.Pipeline;
import queens.common.pipeline.Context;

import static queens.Util.const1;

import queens.plane.fifth.fourth.web.simple.plainjava.dropwizard.Dropwizard$Pipeline;


public class Dropwizard$Controller$Configuration
    extends Configuration {

    private Option<Function1<Context, Pipeline>> nextByFlow;

    public void setNextByFlow(Dropwizard$Pipeline nextByFlow) {
        this.nextByFlow = Option.apply(const1(nextByFlow));
    }

    @JsonProperty("nextByFlow")
    public Option<Function1<Context, Pipeline>> getNextByFlow() {
        return nextByFlow;
    }
}
