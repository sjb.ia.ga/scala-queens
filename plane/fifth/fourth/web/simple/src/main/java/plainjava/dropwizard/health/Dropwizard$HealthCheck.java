package queens.plane.fifth.fourth.web.simple.plainjava.dropwizard.health;

import com.codahale.metrics.health.HealthCheck;


public class Dropwizard$HealthCheck extends HealthCheck {
    public Dropwizard$HealthCheck() {
    }

    @Override
    protected Result check() throws Exception {
        return Result.healthy();
    }
}
