package queens.plane.fifth.fourth.web.simple.servlet.container;

import scala.reflect.ClassTag;

import queens.pojo.jackson.Queens;
import queens.pojo.jackson.Board2;
import queens.pojo.jackson.Square2;
import queens.pojo.jackson.Solution2;
import queens.pojo.jackson.Point2;
import queens.pojo.jackson.Queens2;
import queens.pojo.jackson.Nest2;

import static queens.pojo.jackson.jPojoJackson.classTagQueens;
import static queens.pojo.jackson.jPojoJackson.classTagBoard2;
import static queens.pojo.jackson.jPojoJackson.classTagSquare2;
import static queens.pojo.jackson.jPojoJackson.classTagSolution2;
import static queens.pojo.jackson.jPojoJackson.classTagPoint2;
import static queens.pojo.jackson.jPojoJackson.classTagQueens2;
import static queens.pojo.jackson.jPojoJackson.classTagNest2;


import queens.dimensions.fifth.fourth.web.simple.servlet.container.QueensWebServletContainerSimpleProgram;
import queens.plane.fifth.fourth.web.simple.Jackson$Web$SimpleProgram;


public interface Jackson$Web$ServletContainer$SimpleProgram<It>
    extends QueensWebServletContainerSimpleProgram<It, Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2>,
            Jackson$Web$SimpleProgram<It> {

    default ClassTag<Queens> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_o() {
        return classTagQueens();
    }

    default ClassTag<Board2> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_b() {
        return classTagBoard2();
    }

    default ClassTag<Square2> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_r() {
        return classTagSquare2();
    }

    default ClassTag<Solution2> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_s() {
        return classTagSolution2();
    }

    default ClassTag<Point2> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_p() {
        return classTagPoint2();
    }

    default ClassTag<Queens2> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_q() {
        return classTagQueens2();
    }

    default ClassTag<Nest2> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_n() {
        return classTagNest2();
    }

}
