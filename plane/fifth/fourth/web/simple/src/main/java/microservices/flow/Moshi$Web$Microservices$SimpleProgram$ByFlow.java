package queens.plane.fifth.fourth.web.simple.microservices.flow;

import scala.reflect.ClassTag;

import queens.dimensions.third.first.Iteration1;

import queens.pojo.moshi.Queens;
import queens.pojo.moshi.Board2;
import queens.pojo.moshi.Square2;
import queens.pojo.moshi.Solution2;
import queens.pojo.moshi.Point2;
import queens.pojo.moshi.Queens2;
import queens.pojo.moshi.Nest2;

import static queens.pojo.moshi.jPojoMoshi.classTagQueens;
import static queens.pojo.moshi.jPojoMoshi.classTagBoard2;
import static queens.pojo.moshi.jPojoMoshi.classTagSquare2;
import static queens.pojo.moshi.jPojoMoshi.classTagSolution2;
import static queens.pojo.moshi.jPojoMoshi.classTagPoint2;
import static queens.pojo.moshi.jPojoMoshi.classTagQueens2;
import static queens.pojo.moshi.jPojoMoshi.classTagNest2;

import queens.dimensions.fifth.fourth.third.web.simple.microservices.flow.QueensWebMicroservicesSimpleProgramByFlow;
import queens.plane.fifth.fourth.web.simple.flow.Moshi$Web$SimpleProgram$ByFlow;
import queens.plane.fifth.fourth.web.simple.microservices.Moshi$Web$Microservices$SimpleProgram;


public interface Moshi$Web$Microservices$SimpleProgram$ByFlow
    extends QueensWebMicroservicesSimpleProgramByFlow<Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2>,
            Moshi$Web$Microservices$SimpleProgram<Iteration1>,
            Moshi$Web$SimpleProgram$ByFlow {

    default ClassTag<Queens> queens$dimensions$fifth$fourth$third$web$simple$microservices$flow$QueensWebMicroservicesSimpleProgramByFlow$$ct_o() {
        return classTagQueens();
    }

    default ClassTag<Board2> queens$dimensions$fifth$fourth$third$web$simple$microservices$flow$QueensWebMicroservicesSimpleProgramByFlow$$ct_b() {
        return classTagBoard2();
    }

    default ClassTag<Square2> queens$dimensions$fifth$fourth$third$web$simple$microservices$flow$QueensWebMicroservicesSimpleProgramByFlow$$ct_r() {
        return classTagSquare2();
    }

    default ClassTag<Solution2> queens$dimensions$fifth$fourth$third$web$simple$microservices$flow$QueensWebMicroservicesSimpleProgramByFlow$$ct_s() {
        return classTagSolution2();
    }

    default ClassTag<Point2> queens$dimensions$fifth$fourth$third$web$simple$microservices$flow$QueensWebMicroservicesSimpleProgramByFlow$$ct_p() {
        return classTagPoint2();
    }

    default ClassTag<Queens2> queens$dimensions$fifth$fourth$third$web$simple$microservices$flow$QueensWebMicroservicesSimpleProgramByFlow$$ct_q() {
        return classTagQueens2();
    }

    default ClassTag<Nest2> queens$dimensions$fifth$fourth$third$web$simple$microservices$flow$QueensWebMicroservicesSimpleProgramByFlow$$ct_n() {
        return classTagNest2();
    }

}
