package queens.plane.fifth.fourth.web.simple.plainjava.springboot.controller;

import java.util.List;

import scala.Option;
import scala.Function1;
import scala.Tuple2;
import scala.Tuple4;
import scala.util.Left;
import scala.collection.Map;

import org.springframework.util.MultiValueMap;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.bind.annotation.ExceptionHandler;

import org.springframework.http.ResponseEntity;

import queens.common.Pipeline;
import queens.common.pipeline.Context;
import queens.dimensions.fifth.QueensUseSolution;

import queens.pojo.jackson.Queens;

import static queens.pojo.parser.text.jText.toText;
import static queens.pojo.parser.write.jWrite.toData;

import static queens.dimensions.fourth.webapp.Web$times.FlavorException;

import queens.plane.fifth.fourth.web.simple.servlet.container.controller.Jackson$Web$ServletContainer$Controller;


@RestController
@RequestMapping(path = "/queens/solve/*")
public class Rest$SpringBoot$Controller
    extends Jackson$Web$ServletContainer$Controller {

    @Value("${spring.application.name}")
    String appName = null;

    @Override
    final public boolean plainJAVA() {
        return true;
    }

    @Override
    final public boolean webXML() {
        return false;
    }

    //@Autowired
    private Function1<Context, Pipeline> _next = null;

    @Override
    public Option<Function1<Context, Pipeline>> next() {
        return _next == null ? Option.empty() : Option.apply(_next);
    }

    @GetMapping(path = "/model/{model}", produces = TEXT_PLAIN_VALUE)
    public String solve(@PathVariable(value = "model")                                                          String model,
                        @MatrixVariable(pathVar = "model")                                                      MultiValueMap<String, String> planes,
                        @RequestParam(required = false, defaultValue = "6",                    name = "size")   int size,
                        @RequestParam(required = false, defaultValue = "true",                 name = "empty")  boolean empty,
                        @RequestParam(required = false, defaultValue = "-9223372036854775808", name = "seed")   long seed,
                        @RequestParam(required = false, defaultValue = "9223372036854775807",  name = "max")    long max,
                        @RequestParam(required = false, defaultValue = "build",                name = "flavor") String flavor,
                        @RequestParam(required = false, defaultValue = "",                     name = "square") String square,
                        @RequestParam(required = false, defaultValue = "",                     name = "piece")  String piece,
                        @RequestParam(required = false, defaultValue = "",                     name = "queen")  String queen,
                        @RequestParam(required = false, defaultValue = "2",                    name = "indent") int indent
                        ) {
        Queens pojo = this.apply(Tuple2.apply(model, planes), Left.apply(Tuple2.apply(size, Tuple2.apply(empty, seed))), max, flavor);
        Map<String, Object> data = toData(pojo, square, piece, queen);
        return toText(data, indent, " ", "\n", Tuple4.apply(":", "-", "[", "]"));
    }

    @GetMapping(path = "/model/{model}", produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE})
    @ResponseBody
    public Queens solve(@PathVariable(value = "model")                                                          String model,
                        @MatrixVariable(pathVar = "model")                                                      MultiValueMap<String, String> planes,
                        @RequestParam(required = false, defaultValue = "6",                    name = "size")   int size,
                        @RequestParam(required = false, defaultValue = "true",                 name = "empty")  boolean empty,
                        @RequestParam(required = false, defaultValue = "-9223372036854775808", name = "seed")   long seed,
                        @RequestParam(required = false, defaultValue = "9223372036854775807",  name = "max")    long max,
                        @RequestParam(required = false, defaultValue = "build",                name = "flavor") String flavor
                        ) {
        return this.apply(Tuple2.apply(model, planes), Left.apply(Tuple2.apply(size, Tuple2.apply(empty, seed))), max, flavor);
    }

    @ExceptionHandler({FlavorException.class})
    public ResponseEntity<String> error(queens.base.QueensException ex) {
        return ResponseEntity.badRequest().build();
    }

    @Override
    public String toString() {
        return super.toString() + " :: SpringBoot";
    }
}
