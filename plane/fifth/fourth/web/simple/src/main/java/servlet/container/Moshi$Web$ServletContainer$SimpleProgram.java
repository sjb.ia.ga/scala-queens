package queens.plane.fifth.fourth.web.simple.servlet.container;

import scala.reflect.ClassTag;

import queens.pojo.moshi.Queens;
import queens.pojo.moshi.Board2;
import queens.pojo.moshi.Square2;
import queens.pojo.moshi.Solution2;
import queens.pojo.moshi.Point2;
import queens.pojo.moshi.Queens2;
import queens.pojo.moshi.Nest2;

import static queens.pojo.moshi.jPojoMoshi.classTagQueens;
import static queens.pojo.moshi.jPojoMoshi.classTagBoard2;
import static queens.pojo.moshi.jPojoMoshi.classTagSquare2;
import static queens.pojo.moshi.jPojoMoshi.classTagSolution2;
import static queens.pojo.moshi.jPojoMoshi.classTagPoint2;
import static queens.pojo.moshi.jPojoMoshi.classTagQueens2;
import static queens.pojo.moshi.jPojoMoshi.classTagNest2;

import queens.dimensions.fifth.fourth.web.simple.servlet.container.QueensWebServletContainerSimpleProgram;
import queens.plane.fifth.fourth.web.simple.Moshi$Web$SimpleProgram;


public interface Moshi$Web$ServletContainer$SimpleProgram<It>
    extends QueensWebServletContainerSimpleProgram<It, Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2>,
            Moshi$Web$SimpleProgram<It> {

    default ClassTag<Queens> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_o() {
        return classTagQueens();
    }

    default ClassTag<Board2> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_b() {
        return classTagBoard2();
    }

    default ClassTag<Square2> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_r() {
        return classTagSquare2();
    }

    default ClassTag<Solution2> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_s() {
        return classTagSolution2();
    }

    default ClassTag<Point2> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_p() {
        return classTagPoint2();
    }

    default ClassTag<Queens2> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_q() {
        return classTagQueens2();
    }

    default ClassTag<Nest2> queens$dimensions$fifth$fourth$web$simple$servlet$container$QueensWebServletContainerSimpleProgram$$ct_n() {
        return classTagNest2();
    }

}
