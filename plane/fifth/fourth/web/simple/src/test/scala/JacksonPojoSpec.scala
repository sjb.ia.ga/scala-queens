package queens
package plane.fifth.fourth
package web.simple

import java.util.Date

import org.scalatest.flatspec.AnyFlatSpec

import queens.base.Board

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Classic._
import Stepper._
import Algorithm._
import Recursive._
import Model.Flow

import dimensions.three.breed.Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import pojo.parser.text.jText.toText
import pojo.parser.write.jWrite.toData
import pojo.jackson.jPojoJackson.toPojo
import pojo.parser.date.jDate.apply

import pojo.jackson.jPojoJackson.given

import plane.fourth.fifth.third.TryBoard

import plane.fifth.fourth.web.simple.servlet.container.flow.`Jackson Web Servlet Container Simple Program by Flow`


class JacksonPojoSpec
    extends AnyFlatSpec
    with `Jackson Web Servlet Container Simple Program by Flow`:

  final override protected def plainJAVA: Boolean = ???

  final override protected def webXML: Boolean = ???

  "response write" should "equal read" in {
    val N = 6

    val M = 2

    given Long = M

    given Flavor.Value = Flavor.build

    val started = new Date()

    given Board = TryBoard(N, true, M)

    val ended = new Date()

    val wildcard = Wildcard (
      (_, _) match
        // case (Aspect, Stepper(OneIncrement)) => true
        // case (Aspect, Stepper(Permutations)) => true
        // case (Algorithm, Iterative) => true
        case (Algorithm, Recursive(Native)) => true
        case (Aspect, Classic(Straight)) => true
        case (Model, Flow) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case Nest(_, Recursive(Native), _) => 2
      case Nest(_, Iterative, _) => 1
      case _ => 0
    }

    val (response, _) = byFlow(wildcard, multiplier)

    response.getBoard()(started, ended)

    response(started, ended)

    val indent = 2

    assert(response.toData("_", "#", "*").toText(indent, ".").toPojo(".").map(response.==).getOrElse(false))

  }
