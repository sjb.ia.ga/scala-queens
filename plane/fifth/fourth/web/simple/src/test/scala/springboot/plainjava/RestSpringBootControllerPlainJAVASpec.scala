package queens
package plane.fifth.fourth
package web.simple.plainjava.springboot
package controller

import org.scalatest.flatspec.AnyFlatSpec

import org.springframework.util.{ LinkedMultiValueMap => MultiValueMap }

import dimensions.three.breed.Hatch.Flavor

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Classic._
import Stepper._
import Algorithm._
import Recursive._
import Model._

import pojo.jPojo.given


class RestSpringBootControllerPlainJAVASpec
      extends AnyFlatSpec:

  val controller = new Rest$SpringBoot$Controller()

  "solve" should "not fail" in {
    val N = 6

    val M = 2

    val planes = MultiValueMap[String, String]()

    planes.put(s"$classic", List("1"))
    planes.put(s"$native", List("1"))

    controller
      .solve(s"$flow", planes,
             N,
             false,
             Long.MinValue,
             M,
             s"${Flavor.build}",
             "",
             "",
             "",
             2)

    assert(true)

  }
