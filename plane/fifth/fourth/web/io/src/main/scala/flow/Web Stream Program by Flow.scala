package queens
package plane.fifth.fourth
package zeroth
package web.io
package flow

import java.util.Date
import java.util.Collections.sort
import java.util.LinkedList

import scala.reflect.ClassTag

import base.Board

import common.given

import dimensions.third.first.Iteration1

import dimensions.three.breed.Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import pojo.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }

import pojo.parser.date.span

import plane.fifth.fourth.web.simple.`Base.Web Simple Program`


package s:

  import cats.effect.IO

  import dimensions.fifth.fourth.third.zeroth.web.io.flow.s.QueensWebStreamProgramByFlow

  abstract trait `Base.Web Stream Program by Flow`[
    O <: Queens[R, B, P, S, N, Q],
    B <: Board2[R, B],
    R <: Square2,
    S <: Solution2[R, B, P, S],
    P <: Point2,
    Q <: Queens2[R, B, P, S, N, Q],
    N <: Nest2
  ](using
    ct_o: ClassTag[O],
    ct_b: ClassTag[B],
    ct_r: ClassTag[R],
    ct_s: ClassTag[S],
    ct_p: ClassTag[P],
    ct_q: ClassTag[Q],
    ct_n: ClassTag[N],
    b2b2: Conversion[Board, B]
  ) extends `Base.Web Simple Program`[Iteration1, O, B, R, S, P, Q, N]
    with QueensWebStreamProgramByFlow[O, B, R, S, P, Q, N]:

    protected def byFlow(wildcard: Wildcard,
                         multiplier: Multiplier[Nest])
                        (using board: Board)
                        (using Long, Flavor.Value): IO[(O, Long)] =
      val started = Date()

      val response: O = ct_o

      response.setBoard(board)

      val tag = this(response)

      given Iteration1 = new Iteration1(board, tag, common.Flag(), { () => })

      response.setQueens(LinkedList[Q]())

      val init: Long = 0L.span(from = started)

      for
        idle <- this(wildcard, multiplier)
        ended <- IO(Date())
        _ <- IO {
          response.setCount(response.getQueens.size)

          sort(response.getBoard.getSquares)

          sort(response.getQueens)

          this(tag, response)
        }
        done: Long = idle.span(from = ended)
      yield
        response -> (init + done)
