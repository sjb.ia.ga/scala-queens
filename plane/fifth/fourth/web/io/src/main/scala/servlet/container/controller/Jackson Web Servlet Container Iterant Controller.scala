package queens
package plane.fifth.fourth
package zeroth
package web.io
package servlet.container
package controller

import pojo.jackson.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }


package x:

  package sync:

    abstract trait `Jackson Web Servlet Container Iterant Controller`
        extends web.io.controller.x.sync.`Base Solve Iterant Controller`[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]
        with flow.x.sync.`Jackson Web Servlet Container Iterant Program by Flow`:

      override def toString(): String = super.toString + " :: Jackson"

  package async:

    abstract trait `Jackson Web Servlet Container Iterant Controller`
        extends web.io.controller.x.async.`Base Solve Iterant Controller`[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]
        with flow.x.async.`Jackson Web Servlet Container Iterant Program by Flow`:

      override def toString(): String = super.toString + " :: Jackson"
