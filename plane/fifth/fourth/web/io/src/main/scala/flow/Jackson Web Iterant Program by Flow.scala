package queens
package plane.fifth.fourth
package zeroth
package web.io
package flow

import dimensions.third.first.Iteration1

import pojo.jackson.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }

import pojo.jackson.jPojoJackson.given

import plane.fifth.fourth.web.simple.`Jackson Web Simple Program`


package x:

  package sync:

    abstract trait `Jackson Web Iterant Program by Flow`
        extends `Base.Web Iterant Program by Flow`[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]
        with `Jackson Web Simple Program`[Iteration1]

  package async:

    abstract trait `Jackson Web Iterant Program by Flow`
        extends `Base.Web Iterant Program by Flow`[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]
        with `Jackson Web Simple Program`[Iteration1]
