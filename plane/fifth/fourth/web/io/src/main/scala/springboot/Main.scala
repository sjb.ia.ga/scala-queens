package queens
package plane.fifth.fourth
package zeroth
package web.io.springboot

import org.springframework.boot.SpringApplication


object Main:

  def main(args: Array[String]): Unit =

    sys.Prop
       .StringProp("server.port")
       .set("8358")

    sys.Prop
       .StringProp("debug")
       .set("false")

    sys.BooleanProp
      .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
      .enable()

    SpringApplication.run(classOf[SpringBootApp])
