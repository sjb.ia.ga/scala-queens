package queens
package plane.fifth.fourth
package zeroth
package web.io
package controller

import java.util.Date

import base.Board

import dimensions.three.breed.Hatch
import Hatch.Flavor

import dimensions.Dimension.{ Monad, Algorithm, Model }
import Monad.Effect
import Model.Flow

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import pojo.parser.date.jDate.spanFrom
import pojo.parser.date.jDate.apply

import plane.fourth.fifth.third.TryBoard

import common.given

import pojo.`4s`.Queens

import plane.fifth.fourth.web.simple.controller.`Base Controller`


package `4s`:

  import cats.effect.IO

  import Effect.FS2StreamIO

  abstract trait `Base Solve Stream Controller`
      extends `Base Controller`[IO[Queens]]
      with flow.`4s`.`Base.Web Stream Program by Flow`:

    override protected def apply(monad: Monad, model: Model)
                                (wildcard: Wildcard, multiplier: Multiplier[Nest])
                                (started: Date, ended: Date)
                          (using Board, Long, Flavor.Value): IO[Queens] =
      val idle = 0L.spanFrom(ended)

      (monad, model) match
        case (Effect(FS2StreamIO), Flow) =>
        case _ => ???

      for
        result <- byFlow(wildcard, multiplier)
        response = result._1
        idleSolving = result._2
        endedSolving <- IO(Date())
        _ <- IO {
          response.board = response.board.copy(started = started,
                                               ended = ended,
                                               elapsed = ended.getTime - started.getTime,
                                               idle = 0L)
        }
        ended = Date()
      yield
        response.copy(started = started,
                      ended = ended,
                      elapsed = ended.getTime - started.getTime,
                      idle = idle.spanFrom(endedSolving) + idleSolving)
