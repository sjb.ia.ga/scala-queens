package queens
package plane.fifth.fourth
package zeroth
package web.io
package servlet.container
package flow

import pojo.jackson.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }


package s:

   import web.io.flow.s.`Jackson Web Stream Program by Flow`
   import dimensions.fifth.fourth.third.zeroth.web.io.servlet.container.flow.s.QueensWebServletContainerStreamProgramByFlow

   abstract trait `Jackson Web Servlet Container Stream Program by Flow`
       extends `Jackson Web Stream Program by Flow`
       with QueensWebServletContainerStreamProgramByFlow[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]
