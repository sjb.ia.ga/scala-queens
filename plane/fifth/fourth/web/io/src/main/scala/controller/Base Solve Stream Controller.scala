package queens
package plane.fifth.fourth
package zeroth
package web.io
package controller

import java.util.Date

import scala.reflect.ClassTag

import base.Board

import dimensions.three.breed.Hatch
import Hatch.Flavor

import dimensions.Dimension.{ Monad, Algorithm, Model }
import Monad.Effect
import Model.Flow

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import pojo.parser.date.jDate.spanFrom
import pojo.parser.date.jDate.apply

import plane.fourth.fifth.third.TryBoard

import common.given

import pojo.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }

import plane.fifth.fourth.web.simple.controller.`Base Controller`


package s:

  import cats.effect.IO

  import Effect.FS2StreamIO

  abstract trait `Base Solve Stream Controller`[
    O <: Queens[R, B, P, S, N, Q],
    B <: Board2[R, B],
    R <: Square2,
    S <: Solution2[R, B, P, S],
    P <: Point2,
    Q <: Queens2[R, B, P, S, N, Q],
    N <: Nest2
  ](using
    ct_o: ClassTag[O],
    ct_b: ClassTag[B],
    ct_r: ClassTag[R],
    ct_s: ClassTag[S],
    ct_p: ClassTag[P],
    ct_q: ClassTag[Q],
    ct_n: ClassTag[N],
    b2b2: Conversion[Board, B]
  ) extends `Base Controller`[IO[O]]
      with flow.s.`Base.Web Stream Program by Flow`[O, B, R, S, P, Q, N]:

    override protected def apply(monad: Monad, model: Model)
                                (wildcard: Wildcard, multiplier: Multiplier[Nest])
                                (started: Date, ended: Date)
                          (using Board, Long, Flavor.Value): IO[O] =
      val idle = 0L.spanFrom(ended)

      (monad, model) match
        case (Effect(FS2StreamIO), Flow) =>
        case _ => ???

      for
        result <- byFlow(wildcard, multiplier)
        response = result._1
        idleSolving = result._2
        endedSolving <- IO(Date())
        _ <- IO {
          response.getBoard()(started, ended)

          response(started, Date(), Some(idle.spanFrom(endedSolving) + idleSolving))
        }
      yield
        response
