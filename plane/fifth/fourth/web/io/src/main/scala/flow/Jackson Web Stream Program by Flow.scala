package queens
package plane.fifth.fourth
package zeroth
package web.io
package flow

import dimensions.third.first.Iteration1

import pojo.jackson.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }

import pojo.jackson.jPojoJackson.given

import plane.fifth.fourth.web.simple.`Jackson Web Simple Program`


package s:

  abstract trait `Jackson Web Stream Program by Flow`
      extends `Base.Web Stream Program by Flow`[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]
      with `Jackson Web Simple Program`[Iteration1]
