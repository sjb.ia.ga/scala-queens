package queens
package plane.fifth.fourth
package zeroth
package web.io
package servlet.container
package flow

import pojo.jackson.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }


package x:

  package sync:

    import web.io.flow.x.sync.`Jackson Web Iterant Program by Flow`
    import dimensions.fifth.fourth.third.zeroth.web.io.servlet.container.flow.x.sync.QueensWebServletContainerIterantProgramByFlow

    abstract trait `Jackson Web Servlet Container Iterant Program by Flow`
        extends `Jackson Web Iterant Program by Flow`
        with QueensWebServletContainerIterantProgramByFlow[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]

  package async:

    import web.io.flow.x.async.`Jackson Web Iterant Program by Flow`
    import dimensions.fifth.fourth.third.zeroth.web.io.servlet.container.flow.x.async.QueensWebServletContainerIterantProgramByFlow

    abstract trait `Jackson Web Servlet Container Iterant Program by Flow`
        extends `Jackson Web Iterant Program by Flow`
        with QueensWebServletContainerIterantProgramByFlow[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]
