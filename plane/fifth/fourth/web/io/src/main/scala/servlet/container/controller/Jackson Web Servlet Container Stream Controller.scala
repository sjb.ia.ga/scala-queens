package queens
package plane.fifth.fourth
package zeroth
package web.io
package servlet.container
package controller

import pojo.jackson.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }


package s:

  abstract trait `Jackson Web Servlet Container Stream Controller`
      extends web.io.controller.s.`Base Solve Stream Controller`[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2]
      with flow.s.`Jackson Web Servlet Container Stream Program by Flow`:

    override def toString(): String = super.toString + " :: Jackson"
