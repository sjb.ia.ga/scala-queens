package object queens:

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]


  type IO = dimensions.fifth.cycle.IO


  type Identity_[A] = A

  type Iterable_[X[_], A] = Iterable[A]


  type `Queens3*` = dimensions.third.`Queens*`[Point, Boolean]


  given Conversion[String, List[String]] = _
    .split("[\\[{(<.: ;,>)}\\]]")
    .filterNot(_.isEmpty)
    .map(_.replace('_', ' '))
    .toList
