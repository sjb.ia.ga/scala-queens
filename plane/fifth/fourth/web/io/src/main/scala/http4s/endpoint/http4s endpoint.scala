package queens
package plane.fifth.fourth
package zeroth
package web.io.http4s
package endpoint

import scala.collection.mutable.{ HashMap => MutableMap }

import cats.effect.IO

import io.circe.syntax._

import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.headers.`Content-Type`
import org.http4s.{ EntityEncoder, HttpRoutes, Header }

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution
import dimensions.fifth.output.console.QueensConsoleOutput

import dimensions.Dimension.Monad.fs2StreamIO

import pojo.`4s`.Queens
import pojo.parser.`4s`.Circe.given

import pojo.parser.text.jText.toText
import pojo.parser.write.`4s`.toData


object `http4s endpoint`
    extends web.io.controller.`4s`.`Base Solve Stream Controller`
    with Http4sDsl[IO]:

  final override protected val plainJAVA: Boolean = false

  val _next: Context => QueensUseSolution = QueensConsoleOutput(_)

  override protected def next: Option[Context => QueensUseSolution] =
    if _next eq null then None else Some(_next)


  given EntityEncoder[IO, Queens] = jsonEncoderOf

  object SizeQueryParamMatcher extends OptionalQueryParamDecoderMatcher[Int]("size")
  object EmptyQueryParamMatcher extends OptionalQueryParamDecoderMatcher[Boolean]("empty")
  object SeedQueryParamMatcher extends OptionalQueryParamDecoderMatcher[Long]("seed")
  object MaxQueryParamMatcher extends OptionalQueryParamDecoderMatcher[Long]("max")
  object FlavorQueryParamMatcher extends OptionalQueryParamDecoderMatcher[String]("flavor")

  object SquareQueryParamMatcher extends OptionalQueryParamDecoderMatcher[String]("square")
  object PieceQueryParamMatcher extends OptionalQueryParamDecoderMatcher[String]("piece")
  object QueenQueryParamMatcher extends OptionalQueryParamDecoderMatcher[String]("queen")
  object IndentQueryParamMatcher extends OptionalQueryParamDecoderMatcher[Int]("indent")


  def apply(): HttpRoutes[IO] = HttpRoutes.of[IO] {

    case req @ GET -> Root / "model" / planes
        :? SizeQueryParamMatcher(size)
        +& EmptyQueryParamMatcher(empty)
        +& SeedQueryParamMatcher(seed)
        +& MaxQueryParamMatcher(max)
        +& FlavorQueryParamMatcher(flavor)
        if req.headers.headers.contains(Header("Accept", "application/json")) =>
      for
        queens <- this(planes,
                       Left(size.getOrElse(6) -> (empty.getOrElse(true) -> seed.getOrElse(Long.MinValue))),
                       max.getOrElse(Long.MaxValue),
                       flavor.getOrElse("build"))
        response <- Ok(queens.asJson)
      yield
        response

    case req @ GET -> Root / "model" / planes
        :? SizeQueryParamMatcher(size)
        +& EmptyQueryParamMatcher(empty)
        +& SeedQueryParamMatcher(seed)
        +& MaxQueryParamMatcher(max)
        +& FlavorQueryParamMatcher(flavor)
        +& SquareQueryParamMatcher(square)
        +& PieceQueryParamMatcher(piece)
        +& QueenQueryParamMatcher(queen)
        +& IndentQueryParamMatcher(indent)
        if req.headers.headers.contains(Header("Accept", "text/plain")) =>
      for
        queens <- this(planes,
                       Left(size.getOrElse(6) -> (empty.getOrElse(true) -> seed.getOrElse(Long.MinValue))),
                       max.getOrElse(Long.MaxValue),
                       flavor.getOrElse("build"))
                  .map(_
                    .toData(square.getOrElse(""), piece.getOrElse(""), queen.getOrElse(""))
                    .toText(indent.getOrElse(2)))
        response <- Ok(queens)
      yield
        response
  }


  override protected def request(data: Any): (String, Map[String, List[String]]) =
    import plane.fifth.fourth.web.simple.controller.`4s`.`Base Solve Controller`
    `Base Solve Controller`(data, Some(fs2StreamIO))

  override def toString(): String = super.toString + " :: http4s"
