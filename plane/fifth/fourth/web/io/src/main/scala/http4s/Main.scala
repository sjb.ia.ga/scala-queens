package queens
package plane.fifth.fourth
package zeroth
package web.io.http4s

import scala.concurrent.ExecutionContext.Implicits.global

import cats.effect.{ ExitCode, IOApp, IO, Resource }

import io.circe.config.parser

import org.http4s.implicits._
import org.http4s.server.{ Router, Server }
import org.http4s.server.blaze.BlazeServerBuilder

import endpoint.`http4s endpoint`


object Main extends IOApp:


  case class ServerConfig(host: String, port: Int)
  case class QueensConfig(server: ServerConfig)

  object QueensConfig:

    import io.circe.Decoder
    import io.circe.generic.semiauto._

    given Decoder[ServerConfig] = deriveDecoder
    given Decoder[QueensConfig] = deriveDecoder


  def createServer: Resource[IO, Server] =
    import QueensConfig.given

    for
      conf <- Resource.eval(parser.decodePathF[IO, QueensConfig]("queens"))
      httpApp = Router[IO]("/queens/solve/" -> `http4s endpoint`()).orNotFound
      server <- BlazeServerBuilder[IO](global)
        .bindHttp(conf.server.port, conf.server.host)
        .withHttpApp(httpApp)
        .resource
    yield
      server


  override def run(args: List[String]): IO[ExitCode] =
    createServer.use(_ => IO.never).as(ExitCode.Success)
