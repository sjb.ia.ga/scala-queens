val springBootVersion = "3.3.0"
val CirceVersion = "0.14.7"
val CirceConfigVersion = "0.10.1"
val http4sVersion = "0.22.15"

libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",

  // reactive
  "org.reactivestreams" % "reactive-streams" % "1.0.4",
  "io.projectreactor" % "reactor-core" % "3.6.7",
  "org.springframework.boot" % "spring-boot-starter-webflux" % springBootVersion
    exclude("org.springframework.boot", "spring-boot-starter-tomcat"),
  "jakarta.servlet" % "jakarta.servlet-api" % "6.1.0",

  "co.fs2" %% "fs2-core" % "2.5.12",
  "co.fs2" %% "fs2-reactive-streams" % "2.5.12",

  // http4s
  "io.circe" %% "circe-generic" % CirceVersion,
  "io.circe" %% "circe-parser" % CirceVersion,
  "io.circe" %% "circe-config" % CirceConfigVersion,
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,
  "org.http4s" %% "http4s-dsl" % http4sVersion,

  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
