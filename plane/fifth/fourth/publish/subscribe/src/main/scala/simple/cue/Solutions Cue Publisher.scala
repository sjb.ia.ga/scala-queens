package queens
package plane.fifth.fourth
package publish.subscribe
package cycle.cue

import scala.collection.mutable.{ LongMap => Lengthy }

import dimensions.fifth.fourth.third.publish.cycle.QueensCoupledPublisher

import `Solutions' Cue Publisher`.Recording


class `Solutions' Cue Publisher`(using Boolean)
    extends QueensCoupledPublisher[Cue]:

  protected var fidelity = 1L
  var recording: Recording = null

  inline final override protected def span(_number: Long, _prev: Long): Option[Long] =
    Some(fidelity)

  override protected def at(io: IO, marker: Long): Cue =
    val (fidelity, (counter, _)) = recording(marker)
    val (_, (counter1, _)) = recording(marker-1)
    (io._1, number, counter - counter1, fidelity, (this.counter - offset) - counter)

  override protected def on(marker: Long): Unit =
    if marker == 1
    then
      fidelity = 1L
      recording = Lengthy(0L -> (fidelity -> (0L -> 0L)))
    recording(marker) = fidelity -> ((counter - offset) -> 0L)

  override protected def no(marker: Long): Boolean =
    val (_, (counter, _)) = recording(marker)
    val (_, (counter1, _)) = recording(marker-1)
    counter - counter1 == 0

  override def emit0(io0: IO): Unit  =
    emit(Some((io0._1, 0L, 0L, -1L, -1L)))

  override def toString = "Solutions' Cue Publisher"


object `Solutions' Cue Publisher`:

  private type Recording = Lengthy[(Long, (Long, Long))]

  import dimensions.fifth.fourth.third.publish.cycle.QueensCoupledPublisher.QueensCoupledSubscriber

  import dimensions.fourth.subscribe.QueensSubscription

  type `Solutions' Cue Subscriber` = QueensCoupledSubscriber[Cue]

  type `Solutions' Cue Subscription` = QueensSubscription[Cue]
