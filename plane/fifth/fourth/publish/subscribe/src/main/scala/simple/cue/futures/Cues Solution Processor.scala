package queens
package plane.fifth.fourth
package publish.subscribe
package cycle.cue
package futures

import common.pipeline.Context

import dimensions.fifth.QueensUseSolution

import dimensions.fifth.third.publish.futures.given

import dimensions.fifth.fourth.third.publish.cycle.futures.`Base.Coupled Publisher Cycle Program by Futures`


class `Cues' Solution Processor`(
  legacy: Boolean = true
)(
  override protected val next: Option[Context => QueensUseSolution] = None
)(using
  Boolean
) extends `Base.Coupled Publisher Cycle Program by Futures`[Cue, `Solutions' Cue Publisher`]("Cue", legacy):

  override protected val preemption = new `Solutions' Cue Publisher`()

  override def toString(): String = "Cues' Solution Processor " + super.toString
