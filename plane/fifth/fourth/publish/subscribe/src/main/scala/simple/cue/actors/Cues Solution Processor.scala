package queens
package plane.fifth.fourth
package publish.subscribe
package cycle.cue
package actors

import common.pipeline.Context

import dimensions.fifth.QueensUseSolution

import dimensions.fifth.third.publish.actors.given

import dimensions.fifth.fourth.third.publish.cycle.actors.`Base.Coupled Publisher Cycle Program by Actors`


class `Cues' Solution Processor`(
  legacy: Boolean = true
)(
  override protected val next: Option[Context => QueensUseSolution] = None
)(using
  Boolean
) extends `Base.Coupled Publisher Cycle Program by Actors`[Cue, `Solutions' Cue Publisher`]("Cue", legacy):

  override protected val preemption = new `Solutions' Cue Publisher`()

  override def toString(): String = "Cues' Solution Processor " + super.toString
