package queens
package plane.fifth.fourth
package publish.subscribe
package reactor
package actors

import java.util.UUID
import java.util.concurrent.atomic.AtomicLong

import scala.concurrent.duration._

import org.reactivestreams.{ Subscriber, Subscription }

import akka.actor.typed.{ ActorRef, Behavior }
import akka.actor.typed.scaladsl.Behaviors

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit

import akka.util.ReentrantGuard

import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.matchers.should.Matchers

import _root_.reactor.core.publisher.Flux

import base.{ Board, EmptyBoard }

import common.{ Flag, NoTag }

import common.bis.Cycle

import common.pipeline.Context.Round

import conf.actors.bis.Callbacks

import dimensions.fourth.subscribe.QueensSubscription._

import dimensions.fifth.output.console.QueensConsoleOutput

import dimensions.third.first.Iteration1

import dimensions.three.breed.Hatch.Flavor

import dimensions.Dimension.Aspect
import Aspect._
import Classic._
import Stepper._
import dimensions.Dimension.Algorithm
import Algorithm._
import Recursive._
import dimensions.Dimension.Model
import Model.Parallel
import Parallel.Actors

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import plane.fifth.fourth.publish.subscribe.cycle.cue.actors.`Cues' Solution Processor`
import dimensions.fifth.fourth.third.cue.cycle.`Cue Cycle Processor`


class ReactorCueProcessorReplayByActorsSpec
    extends ScalaTestWithActorTestKit
    with AnyWordSpecLike
    with Subscriber[(IO, (Solution, Round))]:

  import ReactorCueProcessorReplayByActorsSpec._


  private val TEST = new ReentrantGuard()

  private val total = AtomicLong()

  private var subscription: Subscription = null


  given Option[Long] = Some(0)

  given Flavor.Value = Flavor.build


  val wildcard = Wildcard (
    (_, _) match
      case (Algorithm, Recursive(Extern)) => true
      case (Algorithm, Recursive(_)) => false
      case (Model, Parallel(Actors)) => true
      case (Model, _) => false
      case _ => true
  )

  val multiplier = Multiplier.Once[Nest]()

  // val wildcard = Wildcard (
  //   (_, _) match
  //     case (Aspect, Classic(Straight)) => true
  //     case (Aspect, Classic(Callback)) => true
  //     case (Aspect, Stepper(OneIncrement)) => true
  //     case (Aspect, Stepper(Permutations)) => true
  //     case (Algorithm, Iterative) => true
  //     case (Algorithm, Recursive(Extern)) => true
  //     case (Model, Parallel(Actors)) => true
  //     case _ => false
  // )

  // val multiplier = Multiplier[Nest] {
  //   case Nest(_, Iterative, _) => 1
  //   case Nest(_, Recursive(Extern), _) => 1
  //   case _ => 0
  // }

  val exp = wildcard(multiplier)


  "Board 3x3 selected for 1 solution" should {
    "replay 1 solution" in {
      TEST.withGuard {
        given Long = 2

        val o = false
        val x = true

        given Board = new Board(List(
          List(x, o, x),
          List(x, x, x),
          List(o, x, o)
        ))

        apply(20)
      }
    }
  }


  "Board 4x4 selected for 2 solutions" should {
    "replay 2 solutions" in {
      TEST.withGuard {
        given Long = 3

        val o = false
        val x = true

        given Board = new Board(List(
          List(x, o, x, x),
          List(x, o, x, o),
          List(x, x, x, x),
          List(o, x, o, x)
        ))

        apply(20)
      }
    }
  }


  "Board 6x6 selected for 4 solutions" should {
    "replay 4 solutions" in {
      TEST.withGuard {

        given Long = 5

        given Board = EmptyBoard(6)

        apply(40)
      }
    }
  }


  def apply(sec: Int)
           (using Board)
           (using Long, Flavor.Value): Unit =

    total.set(0)

    val `13`: Behavior[Test] = Behaviors
      .receive {

        case (ctx, Test(Left(gen))) =>

          val cbs = Callbacks()

          ctx.spawnAnonymous {
            given Boolean = false

            val cues = new `Cues' Solution Processor`(legacy = false)()

            val (_, r) = cues(wildcard, multiplier)(Cycle(wildcard, multiplier), 0L) { pub =>

              val cues = new `Cue Cycle Processor`(pub)()(Some(QueensConsoleOutput(_)))

              given Iteration1 = new Iteration1(Cycle(), NoTag, Flag(), { () => }, ctx -> cbs)

              cues(wildcard, multiplier)

              cues.subscribe(this)
            }

            r
          } ! Some(cbs) -> gen

          Behaviors.same

        case (_, Test(Right(ref))) =>

          ref ! Stop

          Behaviors.stopped
      }

    val actor = spawn(`13`, "Queens-Test")
    val gen = createTestProbe[Long]()
    val stop = createTestProbe[Stop.type]()

    actor ! Test(Left(gen.ref))

    gen.expectMessage(sec.seconds, exp.size * (implicitly[Long] - 1))

    actor ! Test(Right(stop.ref))

    stop.expectMessage(Stop)

    assert(total.get == exp.size * (implicitly[Long] - 1))


  override def onSubscribe(s: Subscription): Unit =
    subscription = s
    subscription.request(Long.MaxValue)

  override def onNext(t: (IO, (Solution, Round))): Unit =
    t match
      case (_, (Nil, _)) =>
      case (((_, nest, _, uuid), _), (it, Round(_, _, _, n))) =>
        total.incrementAndGet()
        println(s"[$uuid] @ $nest #$n : $it")

  override def onComplete(): Unit =
    subscription.cancel()
    subscription = null

  override def onError(t: Throwable): Unit =
    subscription.cancel()
    subscription = null


object ReactorCueProcessorReplayByActorsSpec:

  case class Test(response: Either[ActorRef[Long], ActorRef[Stop.type]])
  case object Stop
