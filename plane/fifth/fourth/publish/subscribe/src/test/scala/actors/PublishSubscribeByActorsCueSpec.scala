package queens
package plane.fifth.fourth
package publish.subscribe
package actors

import scala.concurrent.duration._

import akka.actor.typed.{ ActorRef, Behavior }
import akka.actor.typed.scaladsl.Behaviors

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit

import akka.util.ReentrantGuard

import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.matchers.should.Matchers

import org.reactivestreams.Subscriber

import base.{ Board, EmptyBoard }

import common.bis.Cycle

import common.pipeline.Context

import dimensions.fifth.output.console.QueensConsoleOutput

import dimensions.three.breed.Hatch.Flavor

import dimensions.Dimension.Aspect
import Aspect._
import Classic._
import Stepper._
import dimensions.Dimension.Algorithm
import Algorithm._
import Recursive._
import dimensions.Dimension.Model
import Model.Parallel
import Parallel.Actors

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import plane.fifth.fourth.publish.subscribe.cycle.cue.`Solutions' Cue Publisher`.`Solutions' Cue Subscriber`
import plane.fifth.fourth.publish.subscribe.cycle.cue.actors.`Cues' Solution Processor`


class PublishSubscribeByActorsCueSpec
    extends ScalaTestWithActorTestKit
    with AnyWordSpecLike:

  import PublishSubscribeByActorsCueSpec._


  private val TEST = new ReentrantGuard()


  given Option[Long] = Some(0)

  given Flavor.Value = Flavor.build


  val wildcard = Wildcard (
    (_, _) match
      case (Aspect, Classic(Straight)) => true
      case (Aspect, Classic(Callback)) => true
      case (Aspect, Stepper(OneIncrement)) => true
      case (Aspect, Stepper(Permutations)) => true
      case (Algorithm, Iterative) => true
      case (Algorithm, Recursive(Native)) => true
      case (Algorithm, Recursive(Extern)) => true
      case (Model, Parallel(Actors)) => true
      case _ => false
  )

  val multiplier = Multiplier[Nest] {
    case Nest(_, Recursive(Native), _) => 1
    case Nest(_, Recursive(Extern), _) => 1
    case Nest(_, Iterative, _) => 0
    case _ => 0
  }


  "Board 3x3 selected for 1 solution" should {
    "record 1 solution" in {
      TEST.withGuard {
         given Long = 2

         val o = false
         val x = true

         given Board = new Board(List(
           List(x, o, x),
           List(x, x, x),
           List(o, x, o)
         ))

         apply(10)
      }
    }
  }


  "Board 4x4 selected for 2 solutions" should {
    "record 2 solutions" in {
      TEST.withGuard {
        given Long = 3

        val o = false
        val x = true

        given Board = new Board(List(
          List(x, o, x, x),
          List(x, o, x, o),
          List(x, x, x, x),
          List(o, x, o, x)
        ))

        apply(20)
      }
    }
  }


  "Board 4x4 transposed selected for 2 solutions" should {
    "record 2 solutions" in {
      TEST.withGuard {
        given Long = 3

        val o = false
        val x = true

        given Board = new Board(List(
          List(x, x, x, o),
          List(o, o, x, x),
          List(x, x, x, o),
          List(x, o, x, x)
        ))

        apply(20)
      }
    }
  }


  "Board 6x6 selected for 4 solutions" should {
    "record 4 solutions" in {
      TEST.withGuard {
        given Long = 5

        given Board = EmptyBoard(6)

        apply(40)
      }
    }
  }


  def apply(sec: Int)(using Board)
                     (using Long, Flavor.Value): Unit =

    val cycle = Cycle(wildcard, multiplier)

    val `13`: Behavior[Either[ActorRef[Long], ActorRef[Stop.type]]] = Behaviors
      .receive {

        case (ctx, Left(ref)) =>

          given Boolean = false

          val cues = new `Cues' Solution Processor`()(Some(QueensConsoleOutput.apply))

          ctx.spawnAnonymous {
            val (_, r) = cues(wildcard, multiplier)(cycle, 5000L)(_.subscribe(PublishSubscribeByActorsCueSpec.asInstanceOf[Subscriber[Cue]]))
            r
          } ! None -> ref

          Behaviors.same

        case (ctx, Right(ref)) =>

          ref ! Stop

          Behaviors.stopped
      }

    val actor = spawn(`13`, "Queens-Test")
    val done = createTestProbe[Long]()
    val stop = createTestProbe[Stop.type]()

    actor ! Left(done.ref)

    done.expectMessage(sec.seconds, wildcard(multiplier).size * (implicitly[Long] - 1))

    actor ! Right(stop.ref)

    stop.expectMessage(Stop)


given Boolean = false

object PublishSubscribeByActorsCueSpec
    extends `Solutions' Cue Subscriber`:

  case object Stop

  import java.util.concurrent.Flow.{ Subscription => JavaSubscription }
  import org.reactivestreams.{ Subscription => OrgSubscription }

  var default: Long = 10
  var capacity: Long = _

  private def resubmit =
    capacity = default
    subscription.request(capacity)

  override protected def onSubscription(subscription: Either[JavaSubscription, OrgSubscription]): Unit =
    super.onSubscription(subscription)
    resubmit

  override def onNext(cue: Cue): Unit =
    val ((_, _, _, uuid), n, c, f, d) = cue
    println(s"[$uuid] $n => $c * $f (delta $d)")
    capacity -= 1
    if capacity == 0 then resubmit
