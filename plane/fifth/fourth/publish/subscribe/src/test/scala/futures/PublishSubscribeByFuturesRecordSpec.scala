package queens
package plane.fifth.fourth
package publish.subscribe
package futures

import java.util.UUID

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits._

import java.util.concurrent.Flow.{ Subscription => JavaSubscription }
import org.reactivestreams.{ Subscription => OrgSubscription }

import org.scalatest.flatspec.AnyFlatSpec

import base.{ Board, EmptyBoard }

import dimensions.three.breed.Hatch.Flavor

import dimensions.Dimension.Aspect
import Aspect._
import Classic._
import Stepper._
import dimensions.Dimension.Algorithm
import Algorithm._
import Recursive._
import dimensions.Dimension.Model
import Model.Parallel
import Parallel.Futures

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import dimensions.fourth.subscribe.QueensSubscription._

import dimensions.fifth.fourth.publish.cycle.{ QueensPreemptionPublisher, QueensSolutionSubscriber }

import dimensions.fifth.third.publish.futures.given

import dimensions.fifth.fourth.third.publish.cycle.futures.`Base.Publisher Cycle Program by Futures`


class PublishSubscribeByFuturesRecordSpec
    extends AnyFlatSpec
    with `Base.Publisher Cycle Program by Futures`(())
    with QueensSolutionSubscriber:

  {
    import sys.Prop.StringProp
    sys.Prop("scala.concurrent.context.numThreads").set("x32")
  }

  import sys.Prop.IntProp
  sys.Prop("scala.concurrent.context.maxThreads").set("8192")
  sys.Prop("scala.concurrent.context.maxExtraThreads").set("8192")
  sys.Prop("scala.concurrent.context.minThreads").set("512")
  sys.Prop("java.util.concurrent.ForkJoinPool.common.parallelism").set("4096")


  import PublishSubscribeByFuturesRecordSpec._

  private val TEST = Object()

  override val self = if implicitly[Boolean] then Left(this) else Right(this)


  override protected val preemption: QueensPreemptionPublisher[Unit] = new QueensPreemptionPublisher[Unit] {}

  var recorder: RecordSubscriber = _


  override protected def onSubscription(subscription: Either[JavaSubscription, OrgSubscription]): Unit =
    super.onSubscription(subscription)
    subscription.request(Long.MaxValue)

  inline override protected def span(_n: Long, _p: Long): Option[Long] =
    Some(1L)

  inline override def onComplete(): Unit =
    suspend()

  inline override def onNext(_it: IO): Unit =
    !recorder


  given Option[Long] = Some(0)

  given Flavor.Value = Flavor.build


  val wildcard = Wildcard (
    (_, _) match
      case (Aspect, Classic(Straight)) => true
      //case (Aspect, Classic(Callback)) => true
      //case (Aspect, Stepper(OneIncrement)) => true
      //case (Aspect, Stepper(Permutations)) => true
      case (Algorithm, Iterative) => true
      case (Algorithm, Recursive(Native)) => true
      case (Algorithm, Recursive(Extern)) => true
      case (Model, Parallel(Futures)) => true
      case _ => false
  )

  val multiplier = Multiplier[Nest] {
    case Nest(_, Recursive(Native), _) => 1
    case Nest(_, Recursive(Extern), _) => 0
    case Nest(_, Iterative, _) => 0
    case _ => 0
  }


  require(wildcard(multiplier).size == 1)


  "Board 3x3 selected for 1 solution" should "record 1 solution" in {
    Synchronized(TEST) {
      given Long = 2

      val o = false
      val x = true

      given Board = new Board(List(
        List(x, o, x),
        List(x, x, x),
        List(o, x, o)
      ))

      apply
    }
  }


  "Board 4x4 selected for 2 solutions" should "record 2 solutions" in {
    Synchronized(TEST) {
      given Long = 3

      val o = false
      val x = true

      given Board = new Board(List(
        List(x, o, x, x),
        List(x, o, x, o),
        List(x, x, x, x),
        List(o, x, o, x)
      ))

      apply
    }
  }


  "Board 4x4 transposed selected for 2 solutions" should "record 2 solutions" in {
    Synchronized(TEST) {
      given Long = 3

      val o = false
      val x = true

      given Board = new Board(List(
        List(x, x, x, o),
        List(o, o, x, x),
        List(x, x, x, o),
        List(x, o, x, x)
      ))

      apply
    }
  }


  "Board 6x6 selected for 4 solutions" should "record 4 solutions" in {
    Synchronized(TEST) {
      given Long = 5

      given Board = EmptyBoard(6)

      apply
    }
  }


  "Board 3x3 selected for 1 solution and delayed subscriber" should "record 1 solution" in {
    Synchronized(TEST) {
      import conf.futures.Callbacks.await

      recorder = new RecordSubscriber(1L)
      preemption.subscribe(recorder.asInstanceOf[org.reactivestreams.Subscriber[Unit]])

      val o = false
      val x = true

      given Board = new Board(List(
        List(x, o, x),
        List(x, x, x),
        List(o, x, o)
      ))

      given Long = 2

      val f = Future {
        Thread.sleep(1000L)

        subscribe(this.asInstanceOf[org.reactivestreams.Subscriber[IO]])
      }

      byFutures(wildcard, multiplier)

      await(f)

      val rr = relative(recorder.recording)
      rr.foreach { case (n, (f, (m, d))) =>
        println(s"$n => $m [* $f] (delta $d)")
      }

      reset

      unsubscribe()
    }
  }


  def apply(using Board)
           (using Long, Flavor.Value): Unit =
    subscribe(this.asInstanceOf[org.reactivestreams.Subscriber[IO]])

    recorder = new RecordSubscriber(1L)
    preemption.subscribe(recorder.asInstanceOf[org.reactivestreams.Subscriber[Unit]])

    byFutures(wildcard, multiplier)

    val rr = relative(recorder.recording)
    rr.foreach { case (n, (f, (m, d))) =>
      println(s"$n => $m [* $f] (delta $d)")
    }

    reset

    unsubscribe()


  import common.pipeline.Context

  import dimensions.fifth.output.console.QueensConsoleOutput

  override protected val next: Option[Context => `-|-`] = Some(QueensConsoleOutput.apply)


object PublishSubscribeByFuturesRecordSpec:

  import scala.collection.mutable.{ LongMap => Lengthy, Queue }

  type Recording = Lengthy[(Long, (Long, Long))]

  def relative(recording: Recording): Recording =
    val result = new Recording()
    var (_, (prev, _)) = recording(0)
    for
      i <- 1 until recording.size
    do
      val (fidelity, (next, delta)) = recording(i)
      result(i-1) = fidelity -> ((next - prev) -> delta)
      prev = next
    result


  import dimensions.fifth.fourth.publish.cycle.QueensPreemptionSubscriber

  class RecordSubscriber(private var fidelity: Long)(using Boolean)
      extends QueensPreemptionSubscriber[Unit]:
    var marker: Long = _
    var passes = Queue[Long]()

    val recording: Recording = Lengthy(marker -> (fidelity -> (counter -> 0L)))

    inline override protected def span(_number: Long, _prev: Long): Option[Long] =
      Some(fidelity)

    def unary_! : Unit =
      val marker = passes.dequeue
      val (_, (counter, _)) = recording(marker)
      recording(marker) = fidelity -> (counter -> (this.counter - counter))

    inline override def onMark(_it: IO): Unit =
      marker += 1L
      recording(marker) = fidelity -> (counter -> 0L)
      passes.enqueue(marker)
