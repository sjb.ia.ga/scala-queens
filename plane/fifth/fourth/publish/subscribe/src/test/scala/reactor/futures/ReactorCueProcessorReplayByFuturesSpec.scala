package queens
package plane.fifth.fourth
package publish.subscribe
package reactor
package futures

import java.util.UUID
import java.util.concurrent.atomic.AtomicLong

import scala.concurrent.{ Await, ExecutionContext, Future }
import scala.concurrent.duration.Duration
import ExecutionContext.Implicits._

import org.reactivestreams.{ Subscriber, Subscription }

import org.scalatest.flatspec.AnyFlatSpec

import _root_.reactor.core.publisher.{ Flux, Mono }
import _root_.reactor.core.scheduler.Schedulers

import base.{ Board, EmptyBoard }

import common.{ Flag, NoTag }

import common.bis.Cycle

import common.pipeline.Context.Round

import common.Macros.busywaitWhile

import conf.futures.bis.Callbacks

import dimensions.fourth.subscribe.QueensSubscription._

import dimensions.fifth.output.console.QueensConsoleOutput

import dimensions.third.first.Iteration1

import dimensions.three.breed.Hatch.Flavor

import dimensions.Dimension.Aspect
import Aspect._
import Classic._
import Stepper._
import dimensions.Dimension.Algorithm
import Algorithm._
import Recursive._
import dimensions.Dimension.Model
import Model.Parallel
import Parallel.Futures

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import plane.fifth.fourth.publish.subscribe.cycle.cue.futures.`Cues' Solution Processor`
import dimensions.fifth.fourth.third.cue.cycle.`Cue Cycle Processor`
import dimensions.fifth.fourth.third.cue.cycle.QueensCueCycleProcessor.given


class ReactorCueProcessorReplayByFuturesSpec
    extends AnyFlatSpec
    with Subscriber[(IO, (Solution, Round))]:

  {
    import sys.Prop.StringProp
    sys.Prop("scala.concurrent.context.numThreads").set("x32")
  }

  import sys.Prop.IntProp
  sys.Prop("scala.concurrent.context.maxThreads").set("8192")
  sys.Prop("scala.concurrent.context.maxExtraThreads").set("8192")
  sys.Prop("scala.concurrent.context.minThreads").set("512")
  sys.Prop("java.util.concurrent.ForkJoinPool.common.parallelism").set("4096")


  private val TEST = Object()

  private val total = AtomicLong()

  private var subscription: Subscription = null


  given Option[Long] = Some(0)

  given Flavor.Value = Flavor.build


  val wildcard = Wildcard (
    (_, _) match
      case (Algorithm, Recursive(Extern)) => true
      case (Algorithm, Recursive(_)) => false
      case (Model, Parallel(Futures)) => true
      case (Model, _) => false
      case _ => true
  )

  val multiplier = Multiplier.Once[Nest]()

  // val wildcard = Wildcard (
  //   (_, _) match
  //     case (Aspect, Classic(Straight)) => true
  //     case (Aspect, Classic(Callback)) => true
  //     case (Aspect, Stepper(OneIncrement)) => true
  //     case (Aspect, Stepper(Permutations)) => true
  //     case (Algorithm, Iterative) => true
  //     case (Algorithm, Recursive(Extern)) => true
  //     case (Model, Parallel(Futures)) => true
  //     case _ => false
  // )

  // val multiplier = Multiplier[Nest] {
  //   case Nest(_, Iterative, _) => 1
  //   case Nest(_, Recursive(Extern), _) => 1
  //   case _ => 0
  // }


  "Board 3x3 selected for 1 solution" should "replay 1 solution" in {
    Synchronized(TEST) {
      given Long = 2

      val o = false
      val x = true

      given Board = new Board(List(
        List(x, o, x),
        List(x, x, x),
        List(o, x, o)
      ))

      apply
    }
  }


  "Board 4x4 selected for 2 solutions" should "replay 2 solutions" in {
    Synchronized(TEST) {
      given Long = 3

      val o = false
      val x = true

      given Board = new Board(List(
        List(x, o, x, x),
        List(x, o, x, o),
        List(x, x, x, x),
        List(o, x, o, x)
      ))

      apply
    }
  }


  "Board 6x6 selected for 4 solutions" should "replay 4 solutions" in {
    Synchronized(TEST) {
      given Long = 5

      given Board = EmptyBoard(6)

      apply
    }
  }


  def apply(using Board)
           (using Long, Flavor.Value)
           (implicit ec: ExecutionContext): Unit =

    total.set(0)

    given Boolean = false

    val cues = new `Cues' Solution Processor`(legacy = false)()

    val (_, f) = cues(wildcard, multiplier)(Cycle(wildcard, multiplier), 9000L) { pub =>

      val proc = new `Cue Cycle Processor`(pub)()(Some(QueensConsoleOutput(_)))

      given Iteration1 = new Iteration1(Cycle(), NoTag, Flag(), { () => }, ec -> Callbacks())

      proc(wildcard, multiplier)

      proc.subscribe(this)
    }

    Await.ready(f, Duration.Inf)

    busywaitWhile(subscription ne null)(60000L, 10L)()

    assert(total.get == wildcard(multiplier).size * (implicitly[Long] - 1))


  override def onSubscribe(s: Subscription): Unit =
    subscription = s
    subscription.request(Long.MaxValue)

  override def onNext(t: (IO, (Solution, Round))): Unit =
    t match
      case (_, (Nil, _)) =>
      case (((_, nest, _, uuid), _), (it, Round(_, _, _, n))) =>
        total.incrementAndGet()
        println(s"[$uuid] @ $nest #$n : $it")

  override def onComplete(): Unit =
    subscription.cancel()
    subscription = null

  override def onError(t: Throwable): Unit =
    subscription.cancel()
    subscription = null
