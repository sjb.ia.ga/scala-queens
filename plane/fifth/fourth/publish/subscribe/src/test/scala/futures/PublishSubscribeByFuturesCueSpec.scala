package queens
package plane.fifth.fourth
package publish.subscribe
package futures

import scala.concurrent.{ Await, ExecutionContext }
import scala.concurrent.duration.Duration
import ExecutionContext.Implicits._

import org.scalatest.flatspec.AnyFlatSpec

import org.reactivestreams.Subscriber

import base.{ Board, EmptyBoard }

import common.bis.Cycle

import common.pipeline.Context

import dimensions.fourth.subscribe.QueensSubscription._

import dimensions.fifth.output.console.QueensConsoleOutput

import dimensions.three.breed.Hatch.Flavor

import dimensions.Dimension.Aspect
import Aspect._
import Classic._
import Stepper._
import dimensions.Dimension.Algorithm
import Algorithm._
import Recursive._
import dimensions.Dimension.Model
import Model.Parallel
import Parallel.Futures

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import plane.fifth.fourth.publish.subscribe.cycle.cue.futures.`Cues' Solution Processor`
import plane.fifth.fourth.publish.subscribe.cycle.cue.`Solutions' Cue Publisher`.`Solutions' Cue Subscriber`


class PublishSubscribeByFuturesCueSpec
    extends AnyFlatSpec:

  {
    import sys.Prop.StringProp
    sys.Prop("scala.concurrent.context.numThreads").set("x32")
  }

  import sys.Prop.IntProp
  sys.Prop("scala.concurrent.context.maxThreads").set("8192")
  sys.Prop("scala.concurrent.context.maxExtraThreads").set("8192")
  sys.Prop("scala.concurrent.context.minThreads").set("512")
  sys.Prop("java.util.concurrent.ForkJoinPool.common.parallelism").set("4096")


  private val TEST = Object()


  given Option[Long] = Some(0)

  given Flavor.Value = Flavor.build

  val wildcard = Wildcard (
    (_, _) match
      case (Aspect, Classic(Straight)) => true
      case (Aspect, Classic(Callback)) => true
      case (Aspect, Stepper(OneIncrement)) => true
      case (Aspect, Stepper(Permutations)) => true
      case (Algorithm, Iterative) => true
      case (Algorithm, Recursive(Native)) => true
      case (Algorithm, Recursive(Extern)) => true
      case (Model, Parallel(Futures)) => true
      case _ => false
  )

  val multiplier = Multiplier[Nest] {
    case Nest(_, Recursive(Native), _) => 1
    case Nest(_, Recursive(Extern), _) => 1
    case Nest(_, Iterative, _) => 0
    case _ => 0
  }


  "Board 3x3 selected for 1 solution" should "record 1 solution" in {
    Synchronized(TEST) {
      given Long = 2

      val o = false
      val x = true

      given Board = new Board(List(
        List(x, o, x),
        List(x, x, x),
        List(o, x, o)
      ))

      apply
    }
  }


  "Board 4x4 selected for 2 solutions" should "record 2 solutions" in {
    Synchronized(TEST) {
      given Long = 3

      val o = false
      val x = true

      given Board = new Board(List(
        List(x, o, x, x),
        List(x, o, x, o),
        List(x, x, x, x),
        List(o, x, o, x)
      ))

      apply
    }
  }


  "Board 4x4 transposed selected for 2 solutions" should "record 2 solutions" in {
    Synchronized(TEST) {
      given Long = 3

      val o = false
      val x = true

      given Board = new Board(List(
        List(x, x, x, o),
        List(o, o, x, x),
        List(x, x, x, o),
        List(x, o, x, x)
      ))

      apply
    }
  }


  "Board 6x6 selected for 4 solutions" should "record 4 solutions" in {
    Synchronized(TEST) {
      given Long = 5

      given Board = EmptyBoard(6)

      apply
    }
  }


  def apply(using Board)
           (using Long, Flavor.Value): Unit =
    val cycle = Cycle(wildcard, multiplier)

    given Boolean = false

    val cues = new `Cues' Solution Processor`()(Some(QueensConsoleOutput.apply))

    val (_, f) = cues(wildcard, multiplier)(cycle, 5000L)(_.subscribe(PublishSubscribeByFuturesCueSpec.asInstanceOf[Subscriber[Cue]]))

    assert(Await.result(f, Duration.Inf) == wildcard(multiplier).size * (implicitly[Long] - 1))


object PublishSubscribeByFuturesCueSpec
    extends `Solutions' Cue Subscriber`:

  import java.util.concurrent.Flow.{ Subscription => JavaSubscription }
  import org.reactivestreams.{ Subscription => OrgSubscription }

  var default: Long = 10
  var capacity: Long = _

  private def resubmit =
    capacity = default
    subscription.request(capacity)

  override protected def onSubscription(subscription: Either[JavaSubscription, OrgSubscription]): Unit =
    super.onSubscription(subscription)
    resubmit

  override def onNext(cue: Cue): Unit =
    val ((_, _, _, uuid), n, c, f, d) = cue
    println(s"[$uuid] $n => $c * $f (delta $d)")
    capacity -= 1
    if capacity == 0 then resubmit

