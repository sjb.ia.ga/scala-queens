package queens
package plane.fifth.fourth
package publish.subscribe
package rxjava
package futures

import java.util.UUID
import java.util.concurrent.atomic.AtomicLong

import scala.concurrent.{ Await, ExecutionContext, Future }
import scala.concurrent.duration.Duration
import ExecutionContext.Implicits._

import org.scalatest.flatspec.AnyFlatSpec

import io.reactivex.rxjava3.core.Flowable

import base.{ Board, EmptyBoard }

import common.bis.Cycle

import dimensions.three.breed.Hatch.Flavor

import dimensions.Dimension.Aspect
import Aspect._
import Classic._
import Stepper._
import dimensions.Dimension.Algorithm
import Algorithm._
import Recursive._
import dimensions.Dimension.Model
import Model.Parallel
import Parallel.Futures

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import plane.fifth.fourth.publish.subscribe.cycle.cue.futures.`Cues' Solution Processor`
import dimensions.fifth.fourth.third.cue.cycle.futures.`Base.Cue Cycle Program by Futures`


class RxJavaCueReplayByFuturesSpec
    extends AnyFlatSpec
    with `Base.Cue Cycle Program by Futures`:

  {
    import sys.Prop.StringProp
    sys.Prop("scala.concurrent.context.numThreads").set("x32")
  }

  import sys.Prop.IntProp
  sys.Prop("scala.concurrent.context.maxThreads").set("8192")
  sys.Prop("scala.concurrent.context.maxExtraThreads").set("8192")
  sys.Prop("scala.concurrent.context.minThreads").set("512")
  sys.Prop("java.util.concurrent.ForkJoinPool.common.parallelism").set("4096")


  private val TEST = Object()

  private val total = AtomicLong(0)


  given Option[Long] = Some(0)

  given Flavor.Value = Flavor.build


  val wildcard = Wildcard (
    (_, _) match
      case (Model, Parallel(Futures)) => true
      case (Model, _) => false
      case _ => true
  )

  val multiplier = Multiplier.Once[Nest]()

  // val wildcard = Wildcard (
  //   (_, _) match
  //     case (Aspect, Classic(Straight)) => true
  //     case (Aspect, Classic(Callback)) => true
  //     case (Aspect, Stepper(OneIncrement)) => true
  //     case (Aspect, Stepper(Permutations)) => true
  //     case (Algorithm, Iterative) => true
  //     case (Algorithm, Recursive(Native)) => true
  //     case (Algorithm, Recursive(Extern)) => true
  //     case (Model, Parallel(Futures)) => true
  //     case _ => false
  // )

  // val multiplier = Multiplier[Nest] {
  //   case Nest(_, Recursive(Native), _) => 1
  //   case Nest(_, Recursive(Extern), _) => 1
  //   case Nest(_, Iterative, _) => 1
  //   case _ => 0
  // }


  "Board 3x3 selected for 1 solution" should "replay 1 solution" in {
    Synchronized(TEST) {
      given Long = 2

      val o = false
      val x = true

      given Board = new Board(List(
        List(x, o, x),
        List(x, x, x),
        List(o, x, o)
      ))

      apply
    }
  }


  "Board 4x4 selected for 2 solutions" should "replay 2 solutions" in {
    Synchronized(TEST) {
      given Long = 3

      val o = false
      val x = true

      given Board = new Board(List(
        List(x, o, x, x),
        List(x, o, x, o),
        List(x, x, x, x),
        List(o, x, o, x)
      ))

      apply
    }
  }


  "Board 6x6 selected for 4 solutions" should "replay 4 solutions" in {
    Synchronized(TEST) {
      given Long = 5

      given Board = EmptyBoard(6)

      apply
    }
  }


  def apply(using Board)
           (using Long, Flavor.Value): Unit =

    total.set(0)

    val cycle = Cycle(wildcard, multiplier)

    var f: Future[?] = null

    val s = Flowable.just(()).onBackpressureBuffer()

    byFutures()(wildcard, multiplier)(s)(cycle, 5000L) { q =>

      f = Future {
        given Boolean = false

        val cues = new `Cues' Solution Processor`(legacy = false)()

        val (_, f) = cues(wildcard, multiplier)(cycle, 0L) {
          Flowable
            .fromPublisher(_)
            //.takeUntil(s)
            .subscribe(
              { it =>
                val ((_, _, _, uuid), number, cue, f, d) = it
                //println(s"[$uuid] $number => $cue * $f (delta $d)")
                q(uuid -> (number -> cue))
              }
            )
        }

        Await.ready(f, Duration.Inf)
      }

    }

    Await.ready(f, Duration.Inf)

    assert(total.get == wildcard(multiplier).size * (implicitly[Long] - 1))


  import common.pipeline.Context

  import dimensions.fifth.output.console.QueensConsoleOutput

  override protected val next: Option[Context => `-|-`] = Some { ctxt => total.incrementAndGet(); QueensConsoleOutput(ctxt) }
