package queens
package plane.fifth.fourth
package publish.subscribe
package rxjava
package actors

import java.util.UUID
import java.util.concurrent.atomic.AtomicLong

import scala.concurrent.duration._

import akka.actor.typed.{ ActorRef, Behavior }
import akka.actor.typed.scaladsl.Behaviors

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit

import akka.util.ReentrantGuard

import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.matchers.should.Matchers

import io.reactivex.rxjava3.core.Flowable

import base.{ Board, EmptyBoard }

import common.bis.Cycle

import dimensions.fifth.output.console.QueensConsoleOutput

import dimensions.three.breed.Hatch.Flavor

import dimensions.Dimension.Aspect
import Aspect._
import Classic._
import Stepper._
import dimensions.Dimension.Algorithm
import Algorithm._
import Recursive._
import dimensions.Dimension.Model
import Model.Parallel
import Parallel.Actors

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import plane.fifth.fourth.publish.subscribe.cycle.cue.actors.`Cues' Solution Processor`


class RxJavaCueRawByActorsSpec
    extends ScalaTestWithActorTestKit
    with AnyWordSpecLike:

  import RxJavaCueRawByActorsSpec._

  private val total = AtomicLong(0)


  private val TEST = new ReentrantGuard()


  given Option[Long] = Some(0)

  given Flavor.Value = Flavor.build

  val wildcard = Wildcard (
    (_, _) match
      case (Model, Parallel(Actors)) => true
      case (Model, _) => false
      case _ => true
  )

  val multiplier = Multiplier.Once[Nest]()

  // val wildcard = Wildcard (
  //   (_, _) match
  //     case (Aspect, Classic(Straight)) => true
  //     case (Aspect, Classic(Callback)) => true
  //     case (Aspect, Stepper(OneIncrement)) => true
  //     case (Aspect, Stepper(Permutations)) => true
  //     case (Algorithm, Iterative) => true
  //     case (Algorithm, Recursive(Native)) => true
  //     case (Algorithm, Recursive(Extern)) => true
  //     case (Model, Parallel(Actors)) => true
  //     case _ => false
  // )

  // val multiplier = Multiplier[Nest] {
  //   case Nest(_, Recursive(Native), _) => 1
  //   case Nest(_, Recursive(Extern), _) => 1
  //   case Nest(_, Iterative, _) => 1
  //   case _ => 0
  // }


  "Board 3x3 selected for 1 solution" should {
    "record 1 solution" in {
      TEST.withGuard {
        given Long = 2

        val o = false
        val x = true

        given Board = new Board(List(
          List(x, o, x),
          List(x, x, x),
          List(o, x, o)
        ))

        apply
      }
    }
  }


  "Board 4x4 selected for 2 solutions" should {
    "record 2 solutions" in {
      TEST.withGuard {
        given Long = 3

        val o = false
        val x = true

        given Board = new Board(List(
          List(x, o, x, x),
          List(x, o, x, o),
          List(x, x, x, x),
          List(o, x, o, x)
        ))

        apply
      }
    }
  }


  "Board 6x6 selected for 4 solutions" should {
    "record 4 solutions" in {
      TEST.withGuard {
        given Long = 5

        given Board = EmptyBoard(6)

        apply
      }
    }
  }


  def apply(using Board)
           (using Long, Flavor.Value): Unit =

    total.set(0)

    val cycle = Cycle(wildcard, multiplier)

    val `13`: Behavior[Test] = Behaviors
      .receive {

        case (ctx, Test(Left(ref))) =>

          given Boolean = false

          val cues = new `Cues' Solution Processor`(legacy = false)(Some { ctxt => total.incrementAndGet(); QueensConsoleOutput(ctxt) })

          ctx.spawnAnonymous {
            val (f, r) = cues(wildcard, multiplier)(cycle, 0L)(Flowable.fromPublisher(_))
            f.subscribe { it =>
              val ((_, _, _, uuid), number, cue, f, d) = it
              println(s"[$uuid] $number => $cue * $f (delta $d)")
            }
            r
          } ! None -> ref

          Behaviors.same

        case (ctx, Test(Right(ref))) =>

          ref ! Stop

          Behaviors.stopped
      }

    val actor = spawn(`13`, "Queens-Test")
    val done = createTestProbe[Long]()
    val stop = createTestProbe[Stop.type]()

    actor ! Test(Left(done.ref))

    done.expectMessage(10.seconds, wildcard(multiplier).size * (implicitly[Long] - 1))

    actor ! Test(Right(stop.ref))

    stop.expectMessage(Stop)

    assert(total.get == wildcard(multiplier).size * (implicitly[Long] - 1))


object RxJavaCueRawByActorsSpec:

  case class Test(response: Either[ActorRef[Long], ActorRef[Stop.type]])
  case object Stop
