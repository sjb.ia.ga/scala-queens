package object queens {

  import java.util.UUID

  import base.Board

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]


  type Simulate = dimensions.fifth.cycle.Simulate

  type IO = dimensions.fifth.cycle.IO


  import java.util.UUID

  import base.Board

  import version.less.nest.Nest

  type Cue = ((Board, Nest, Long, UUID), Long, Long, Long, Long)


  type Ctx = dimensions.fifth.fourth.third.publish.actors.Macros.Ctx


  // ScalaTest

}
