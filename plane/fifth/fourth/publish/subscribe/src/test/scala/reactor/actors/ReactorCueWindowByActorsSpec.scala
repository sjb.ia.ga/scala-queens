package queens
package plane.fifth.fourth
package publish.subscribe
package reactor
package actors

import java.util.UUID

import scala.collection.mutable.{ LongMap => Lengthy }

import scala.concurrent.duration._

import akka.actor.typed.{ ActorRef, Behavior }
import akka.actor.typed.scaladsl.Behaviors

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit

import akka.util.ReentrantGuard

import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.matchers.should.Matchers

import _root_.reactor.core.publisher.Flux

import base.{ Board, EmptyBoard }

import common.bis.Cycle

import dimensions.fifth.output.console.QueensConsoleOutput

import dimensions.three.breed.Hatch.Flavor

import dimensions.Dimension.Aspect
import Aspect._
import Classic._
import Stepper._
import dimensions.Dimension.Algorithm
import Algorithm._
import Recursive._
import dimensions.Dimension.Model
import Model.Parallel
import Parallel.Actors

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import plane.fifth.fourth.publish.subscribe.cycle.cue.actors.`Cues' Solution Processor`


class ReactorCueWindowByActorsSpec
    extends ScalaTestWithActorTestKit
    with AnyWordSpecLike:

  import ReactorCueWindowByActorsSpec._

  private val TEST = new ReentrantGuard()


  given Option[Long] = Some(0)

  given Flavor.Value = Flavor.build


  val wildcard = Wildcard (
    (_, _) match
      case (Model, Parallel(Actors)) => true
      case (Model, _) => false
      case _ => true
  )

  val multiplier = Multiplier.Once[Nest]()

  // val wildcard = Wildcard (
  //   (_, _) match
  //     case (Aspect, Classic(Straight)) => true
  //     case (Aspect, Classic(Callback)) => true
  //     case (Aspect, Stepper(OneIncrement)) => true
  //     case (Aspect, Stepper(Permutations)) => true
  //     case (Algorithm, Iterative) => true
  //     case (Algorithm, Recursive(Native)) => true
  //     case (Algorithm, Recursive(Extern)) => true
  //     case (Model, Parallel(Actors)) => true
  //     case _ => false
  // )

  // val multiplier = Multiplier[Nest] {
  //   case Nest(_, Recursive(Native), _) => 1
  //   case Nest(_, Recursive(Extern), _) => 1
  //   case Nest(_, Iterative, _) => 1
  //   case _ => 0
  // }


  "Board 3x3 selected for 1 solution" should {
    "record 1 solution" in {
      TEST.withGuard {
        given Long = 2

        val o = false
        val x = true

        given Board = new Board(List(
          List(x, o, x),
          List(x, x, x),
          List(o, x, o)
        ))


        val cycle = Cycle(wildcard, multiplier)


        val `13`: Behavior[Test] = Behaviors
          .receive {

            case (ctx, Test(Left(ref))) =>

              given Boolean = false

              val cues = new `Cues' Solution Processor`(legacy = false)() //(Some(QueensConsoleOutput.apply))

              ctx.spawnAnonymous {
                val (ws, r) = cues(wildcard, multiplier)(cycle, 5000L)(Flux.from(_).windowWhile(_._3 > 0))
                ws
                  .subscribe { w =>
                    w
                      .map { it => it._1._2 -> (it._2 -> it._3) }
                      .subscribe { it =>
                        val (Nest(at, am, _), (num, cue)) = it
                        //println(s"$at $am $num $cue")
                        (at, am) match
                          case (Stepper(OneIncrement | Permutations), Iterative) =>
                            assert(Lengthy(0L->38L, 1L->2L)(num) == cue)
                          case (_, Iterative) =>
                            assert(Lengthy(0L->13L, 1L->27L)(num) == cue)
                          case _ =>
                            assert(Lengthy(0L->40L)(num) == cue)
                      }
                  }
                r
              } ! None -> ref

              Behaviors.same

            case (ctx, Test(Right(ref))) =>

              ref ! Stop

              Behaviors.stopped
          }

        val actor = spawn(`13`, "Queens-Test")
        val done = createTestProbe[Long]()
        val stop = createTestProbe[Stop.type]()

        actor ! Test(Left(done.ref))

        done.expectMessage(10.seconds, wildcard(multiplier).size * (implicitly[Long] - 1))

        actor ! Test(Right(stop.ref))

        stop.expectMessage(Stop)
      }
    }
  }


  "Board 4x4 selected for 2 solutions" should {
    "record 2 solutions" in {
      TEST.withGuard {
        given Long = 3

        val o = false
        val x = true

        given Board = new Board(List(
          List(x, o, x, x),
          List(x, o, x, o),
          List(x, x, x, x),
          List(o, x, o, x)
        ))


        val cycle = Cycle(wildcard, multiplier)


        val `13`: Behavior[Test] = Behaviors
          .receive {

            case (ctx, Test(Left(ref))) =>

              given Boolean = false

              val cues = new `Cues' Solution Processor`(legacy = false)() //(Some(QueensConsoleOutput.apply))

              ctx.spawnAnonymous {
                val (ws, r) = cues(wildcard, multiplier)(cycle, 5000L)(Flux.from(_).windowWhile(_._3 > 0))
                ws
                  .subscribe { w =>
                    w
                      .map { it => it._1._2 -> (it._2 -> it._3) }
                      .subscribe { it =>
                        val (Nest(at, am, _), (num, cue)) = it
                        //println(s"$at $am $num $cue")
                        (at, am) match
                          case (Stepper(OneIncrement | Permutations), Iterative) =>
                            assert(Lengthy(0L->170L, 1L->5L, 2L->15L)(num) == cue)
                          case (_, Iterative) =>
                            assert(Lengthy(0L->57L, 1L->51L, 2L->82L)(num) == cue)
                          case _ =>
                            assert(Lengthy(0L->102L, 1L->51L, 2L->37L)(num) == cue)
                      }
                  }
                r
              } ! None -> ref

              Behaviors.same

            case (ctx, Test(Right(ref))) =>

              ref ! Stop

              Behaviors.stopped
          }

        val actor = spawn(`13`, "Queens-Test")
        val done = createTestProbe[Long]()
        val stop = createTestProbe[Stop.type]()

        actor ! Test(Left(done.ref))

        done.expectMessage(30.seconds, wildcard(multiplier).size * (implicitly[Long] - 1))

        actor ! Test(Right(stop.ref))

        stop.expectMessage(Stop)
      }
    }
  }


  "Board 6x6 selected for 4 solutions" should {
    "record 4 solutions" in {
      TEST.withGuard {
        given Long = 5

        given Board = new EmptyBoard(6)


        val cycle = Cycle(wildcard, multiplier)


        val `13`: Behavior[Test] = Behaviors
          .receive {

            case (ctx, Test(Left(ref))) =>

              given Boolean = false

              val cues = new `Cues' Solution Processor`(legacy = false)() //(Some(QueensConsoleOutput.apply))

              ctx.spawnAnonymous {
                val (ws, r) = cues(wildcard, multiplier)(cycle, 5000L)(Flux.from(_).windowWhile(_._3 > 0))
                ws
                  .subscribe { w =>
                    w
                      .map { it => it._1._2 -> (it._2 -> it._3) }
                      .subscribe { it =>
                        val (Nest(at, am, _), (num, cue)) = it
                        //println(s"$at $am $num $cue")
                        (at, am) match
                          case (Stepper(OneIncrement), Iterative) =>
                            assert(Lengthy(0L->600235L, 1L->9472L, 2L->4114L, 3L->7452L, 4L->39983L)(num) == cue)
                          case (Stepper(Permutations), Iterative) =>
                            assert(Lengthy(0L->600854L, 1L->15898L, 2L->3066L, 3L->6715L, 4L->34723L)(num) == cue)
                          case (Stepper(Permutations), Recursive(_)) =>
                            assert(Lengthy(0L->276755L, 1L->57534L, 2L->33892L, 3L->136221L, 4L->156854L)(num) == cue)
                          case (_, Iterative) =>
                            assert(Lengthy(0L->152837L, 1L->78564L, 2L->42315L, 3L->63719L, 4L->323821L)(num) == cue)
                          case _ =>
                            assert(Lengthy(0L->323860L, 1L->63720L, 2L->42316L, 3L->78565L, 4L->152795L)(num) == cue)
                      }
                  }
                r
              } ! None -> ref

              Behaviors.same

            case (ctx, Test(Right(ref))) =>

              ref ! Stop

              Behaviors.stopped
          }

        val actor = spawn(`13`, "Queens-Test")
        val done = createTestProbe[Long]()
        val stop = createTestProbe[Stop.type]()

        actor ! Test(Left(done.ref))

        done.expectMessage(50.seconds, wildcard(multiplier).size * (implicitly[Long] - 1))

        actor ! Test(Right(stop.ref))

        stop.expectMessage(Stop)
      }
    }
  }


object ReactorCueWindowByActorsSpec:

  case class Test(response: Either[ActorRef[Long], ActorRef[Stop.type]])
  case object Stop
