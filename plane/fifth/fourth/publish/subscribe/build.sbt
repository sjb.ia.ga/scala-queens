val AkkaVersion = "2.8.5"

libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",
  "org.reactivestreams" % "reactive-streams" % "1.0.4",
  "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
  "com.typesafe.akka" %% "akka-actor-testkit-typed" % AkkaVersion % Test,
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "io.reactivex.rxjava3" % "rxjava" % "3.1.8",
  "io.projectreactor" % "reactor-core" % "3.6.7",
  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
