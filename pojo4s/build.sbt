val jacksonVersion = "2.17.1"

libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",

  "io.circe" %% "circe-core" % "0.14.7",
  "io.circe" %% "circe-generic" % "0.14.7",
  "io.circe" %% "circe-parser" % "0.14.7",
  "com.lihaoyi" %% "upickle" % "3.3.1",
  "io.spray" %% "spray-json" % "1.3.6",

  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
