package object queens:

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]


  import java.util.UUID

  import base.Board

  import version.less.nest.Nest

  type Cue = ((Board, Nest, Long, UUID), Long, Long, Long, Long)


  type `Queens3*` = dimensions.third.`Queens*`[Point, Boolean]
