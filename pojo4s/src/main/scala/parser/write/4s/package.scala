package queens
package pojo
package parser
package write

import scala.collection.mutable.{ ListBuffer => MutableList, HashMap }
import scala.collection.Map
import scala.Function.const

import queens.base.Board

import pojo.parser.write.{ mkSquares, mkBoard0, mkBoard1 }
import pojo.`4s`.{ Queens, Board2, Queens2, Solution2, Nest2 }
import date.`4s`.getDated

import version.less.nest.Nest

import Board2.given
import Nest2.given
import Solution2.given


package object `4s`:

  extension(self: Queens)
    def toData(square: String = "",
               piece: String = "",
               queen: String = ""
    ): Map[String, Any] =

      val response = HashMap[String, Any](
        "count" -> self.count
      ) ++ self.getDated()

      val board: Board = self.board

      response("board") = Map[String, Any](
        "squares" -> board.mkSquares(),
        "seed" -> board.seed.getOrElse(Long.MinValue)
      ) ++ (
        if square.length == piece.length &&
           piece.length == queen.length &&
           square.nonEmpty
        then
          Map[String, Any]("graphics" -> Map[String, Any]("asciiart" -> board.mkBoard0(square, piece)))
        else
          Map.empty
      ) ++ self.board.getDated()

      if self.count == 0 then
        response("queens") = Nil
        return response

      given Option[Solution => List[String]] =
        if square.length == piece.length &&
           piece.length == queen.length &&
           square.nonEmpty
        then
          Some(board.mkBoard1(square, piece, queen))
        else
          None

      given Board = board

      val q = MutableList[Map[String, Any]]()

      self.queens.foreach { it =>
        q += it.mkQueens ++ it.getDated()
      }

      response("queens") = q.toList

      response


  extension(self: Queens2)
    private def mkQueens(using
                         board: Board,
                         mkBoard: Option[Solution => List[String]]
    ): Map[String, Any] =
      val nest: Nest = self.nest
      Map[String, Any](
        "tags" -> self.tags,
        "validator" -> self.validator,
        "tag" -> self.tag,
        "uuid" -> self.uuid,
        "nest" -> Map[String, Any]("aspect" -> nest.aspect,
                                   "algorithm" -> nest.algorithm,
                                   "model" -> nest.model),
        "solutions" -> {

          import Board.apply

          self.solutions
            .map { it =>
              val solution: Solution = it

              Map[String, Any](
                "number" -> it.number,
                "queens" -> solution.map(mkPoint),
                "board" -> (
                  Map[String, Any](
                    "squares" -> board(it).mkSquares(),
                    "seed" -> board.seed.getOrElse(Long.MinValue)
                  ) ++ (
                    mkBoard match
                      case Some(mk) => Map[String, Any]("graphics" -> Map[String, Any]("asciiart" -> mk(it)))
                      case _ => Map.empty
                  ) ++ it.board.getDated()
                )
              ) ++ it.getDated()
            }.toList
        }
    )

    private def mkPoint(it: Point): Map[String, Any] =
      import common.geom.{ row, col }
      Map("row" -> it.row, "column" -> it.col)
