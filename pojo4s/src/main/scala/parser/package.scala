package queens
package pojo


package parser:

  package object `4s`:

    import java.util.Date

    import pojo.`4s`.{ Queens, Board2, Square2, Queens2, Nest2, Solution2, Point2 }


    object Circe:

      import java.util.UUID

      import scala.util.Try

      import io.circe.{ Decoder, Encoder }

      import io.circe.generic.semiauto._

      given Encoder[Date] = Encoder.encodeString.contramap[Date](df.format)

      given Decoder[Date] = Decoder.decodeString.emapTry { it => Try(df.parse(it)) }

      given Encoder[UUID] = Encoder.encodeString.contramap[UUID](_.toString)

      given Decoder[UUID] = Decoder.decodeString.emapTry { it => Try(UUID.fromString(it)) }


      given Decoder[Point2] = deriveDecoder
      given Decoder[Square2] = deriveDecoder
      given Decoder[Board2] = deriveDecoder
      given Decoder[Nest2] = deriveDecoder
      given Decoder[Solution2] = deriveDecoder
      given Decoder[Queens2] = deriveDecoder
      given Decoder[Queens] = deriveDecoder

      given Encoder[Point2] = deriveEncoder
      given Encoder[Square2] = deriveEncoder
      given Encoder[Board2] = deriveEncoder
      given Encoder[Nest2] = deriveEncoder
      given Encoder[Solution2] = deriveEncoder
      given Encoder[Queens2] = deriveEncoder
      given Encoder[Queens] = deriveEncoder


    object uPickle:

      import upickle.default._

      given ReadWriter[Queens] = macroRW[Queens]
      given ReadWriter[Board2] = macroRW[Board2]
      given ReadWriter[Square2] = macroRW[Square2]
      given ReadWriter[Queens2] = macroRW[Queens2]
      given ReadWriter[Nest2] = macroRW[Nest2]
      given ReadWriter[Solution2] = macroRW[Solution2]
      given ReadWriter[Point2] = macroRW[Point2]

      given ReadWriter[Date] = readwriter[String].bimap[Date](df.format(_), df.parse(_))

    object SprayJson:

      import spray.json.DefaultJsonProtocol._
      import spray.json.RootJsonFormat

      import DateMarshalling.DateFormat
      import UUIDMarshalling.UUIDFormat

      implicit val squareFormat: RootJsonFormat[Square2] = jsonFormat3(Square2.apply)
      implicit val boardFormat: RootJsonFormat[Board2] = jsonFormat6(Board2.apply)
      implicit val pointFormat: RootJsonFormat[Point2] = jsonFormat2(Point2.apply)
      implicit val solutionFormat: RootJsonFormat[Solution2] = jsonFormat7(Solution2.apply)
      implicit val nestFormat: RootJsonFormat[Nest2] = jsonFormat3(Nest2.apply)
      implicit val queensFormat: RootJsonFormat[Queens2] = jsonFormat10(Queens2.apply)
      implicit val responseFormat: RootJsonFormat[Queens] = jsonFormat7(Queens.apply)


      // https://gist.githubusercontent.com/owainlewis/ba6e6ed3eb64fd5d83e7/raw/c5fdc80b0dc228c98ee552e991049ebd96e9081a/DateFormatter.scala
      object DateMarshalling:

        import java.text.SimpleDateFormat
        import java.util.Date
        import scala.util.Try
        import spray.json._

          implicit object DateFormat extends JsonFormat[Date] {
            def write(date: Date) = JsString(dateToIsoString(date))
            def read(json: JsValue) = json match {
              case JsString(rawDate) =>
                parseIsoDateString(rawDate)
                  .fold(deserializationError(s"Expected ISO Date format, got $rawDate"))(identity)
              case error => deserializationError(s"Expected JsString, got $error")
            }
          }

          private val localIsoDateFormatter = new ThreadLocal[SimpleDateFormat] {
            override def initialValue() = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
          }

          private def dateToIsoString(date: Date) =
            localIsoDateFormatter.get().format(date)

          private def parseIsoDateString(date: String): Option[Date] =
            Try{ localIsoDateFormatter.get().parse(date) }.toOption


      object UUIDMarshalling:

        import java.util.UUID
        import scala.util.Try
        import spray.json._

          implicit object UUIDFormat extends JsonFormat[UUID] {
            def write(uuid: UUID) = JsString(uuid.toString)
            def read(json: JsValue) = json match {
              case JsString(rawUUID) =>
                Try{ UUID.fromString(rawUUID) }.toOption
                  .fold(deserializationError(s"Expected UUID, got $rawUUID"))(identity)
              case error => deserializationError(s"Expected JsString, got $error")
            }
          }
