package queens
package pojo


package parser:

  import java.util.Date

  import scala.collection.immutable.ListMap


  package date:

    package object `4s`:

      import scala.collection.Map

      case class Dated(var started: Date = null,
                       var ended: Date = null,
                       var elapsed: Long = 0,
                       var idle: Long = 0)

      extension(self: Dated)
        inline def setDated(): Map[String, Any => Unit] = Map(
          "started" -> { case it: String => self.started = df.parse(it) },
          "ended" -> { case it: String => self.ended = df.parse(it) },
          "elapsed" -> { case it: String => self.elapsed = it.toLong },
          "idle" -> { case it: String => self.idle = it.toLong }
        )

      extension[T <: pojo.`4s`.Dated](self: T)
        inline def getDated(quote: String = "'"): Map[String, Any] =
          ListMap[String, Any](
            "started" -> s"$quote${df.format(self.started)}$quote",
            "ended" -> s"$quote${df.format(self.ended)}$quote",
            "elapsed" -> self.elapsed,
            "idle" -> self.idle
          )
