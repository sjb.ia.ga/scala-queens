package queens
package pojo
package parser
package read
package `4s`

import java.util.UUID
import java.util.Date

import scala.collection.mutable.{ ListBuffer => MutableList }
import scala.collection.mutable.HashMap
import scala.collection.Map

import common.given

import pojo.`4s`.{ Queens, Board2, Square2, Queens2, Solution2, Point2, Nest2 }
import date.`4s`.{ Dated, setDated }


extension(self: String)
  inline def toPojo(spacing: String = " ",
                    separator: String = "\n",
                    delimiters: (String, String, String, String) = (":", "-", "[", "]") // yaml
  ): Option[Queens] =
    `toPojo*`(spacing, separator, delimiters)
  private def `toPojo*`(spacing: String,
                        separator: String,
                        delimiters: (String, String, String, String)
  ): Option[Queens] =
    val quoting = HashMap[String, String]()

    val l = MutableList[String]()

    var i = 0

    for
      it <- self.split("[']")
    do
      i = i + 1
      if i % 2 == 0 then
        val uuid = UUID.randomUUID.toString
        quoting(uuid) = it
        l += uuid
      else l += it

    import text.fromText

    val (colon, item, opened, closed) = delimiters
    val spacing1 = if spacing.isEmpty then " " else spacing
    val item1 = if item.isEmpty then "-" else item
    val spacing2 = spacing1.take(item1.length.min(spacing1.length))
    val item2 = item1.take(spacing1.length.min(item1.length))

    given (String, String, String) = (item2, opened, closed)

    l
      .mkString
      .replace(s"$colon$spacing", colon)
      .replace(s"$item2$spacing$opened", item2+opened)
      .replace(s"$spacing$spacing$closed", item2+closed)
      .replace(s"$item2$spacing", item2)
      .split(s"[$colon$separator]")
      .filterNot(_.isEmpty)
      .map(_.replace('_', ' '))
      .toList
      .fromText(spacing2, quoting)
      .flatMap(_.toQueens)



extension(self: Map[String, AnyRef])
  private def ifObj(using fs: Map[String, AnyRef => Unit]): Option[Unit] =
    if fs.keys.toList.sorted == self.keys.toList.sorted
    then
      try
        fs.foreach { case (f, s) => s(self(f)) }
        Some(())
      catch _ =>
        None
    else
      None


extension(self: Map[String, AnyRef])
  private def toQueens: Option[Queens] =
    var board: Board2 = null
    var count = 0L
    var queens: List[Queens2] = null
    val dated = Dated()
    given Map[String, AnyRef => Unit] =
      Map[String, AnyRef => Unit](
        "board" ->  { case it: Map[String, AnyRef] =>
                        it.toBoard2.foreach(board = _) },
        "count" -> { case it: String => count = it.toLong },
        "queens" -> { case it: List[Map[String, AnyRef]] =>
                        val qs = MutableList[Queens2]()
                        it.foreach(qs += _.toQueens2.get)
                        queens = qs.toList.sorted
                      case None =>
                        queens = Nil
                    }
      ) ++ dated.setDated()

    self.ifObj.map(_ => Queens(board, count, queens, dated.started, dated.ended, dated.elapsed, dated.idle))


extension(self: Map[String, AnyRef])
  private def toBoard2: Option[Board2] =
    if self.contains("graphics") then
      return (self - "graphics").toBoard2
    var squares: List[Square2] = Nil
    var seed: Long = Long.MinValue
    val dated = Dated()
    given Map[String, AnyRef => Unit] =
      Map[String, AnyRef => Unit](
        "squares" ->  { case it: List[Map[String, AnyRef]] =>
                          val sqs = MutableList[Square2]()
                          it.foreach(sqs += _.toSquare2.get)
                          squares = sqs.toList
                        case None =>
                          squares = Nil
                      },
        "seed" -> { case it: String => seed = it.toLong },
      ) ++ dated.setDated()

    self.ifObj.map(_ => Board2(squares, seed, dated.started, dated.ended, dated.elapsed, dated.idle))



extension(self: Map[String, AnyRef])
  private def toSquare2: Option[Square2] =
    var row = 0
    var col = 0
    var free = false
    given Map[String, AnyRef => Unit] =
      Map(
        "row" -> { case it: String => row = it.toInt },
        "column" -> { case it: String => col = it.toInt },
        "free" -> { case it: String => free = java.lang.Boolean.parseBoolean(it) }
      )

    self.ifObj.map(_ => Square2(row, col, free))


extension(self: Map[String, AnyRef])
  private def toQueens2: Option[Queens2] =
    var tags: List[String] = null
    var validator = ""
    var tag = ""
    var uuid: UUID = null
    var nest: Nest2 = null
    var solutions: List[Solution2] = null
    val dated = Dated()
    given Map[String, AnyRef => Unit] =
      Map[String, AnyRef => Unit](
        "tags" -> { case it: List[String] => tags = it.sorted },
        "validator" -> { case it: String => validator = it },
        "tag" -> { case it: String => tag = it },
//        "tag" -> { case it: String => tag = UUID.fromString(it.toString) },
        "uuid" -> { case it: String => uuid = UUID.fromString(it) },
        "nest" -> { case it: Map[String, AnyRef] =>
                      it.toNest2.foreach(nest = _) },
        "solutions" ->  { case it: List[Map[String, AnyRef]] =>
                            val qs = MutableList[Solution2]()
                            it.foreach(qs += _.toSolution2.get)
                            solutions = qs.toList.sorted
                          case None =>
                            solutions = Nil
                        }
      ) ++ dated.setDated()

    self.ifObj.map(_ => Queens2(tags, validator, tag, uuid, nest, solutions, dated.started, dated.ended, dated.elapsed, dated.idle))


extension(self: Map[String, AnyRef])
  private def toNest2: Option[Nest2] =
    import dimensions.Dimension.{ Aspect, Algorithm, Model }
    var aspect = ""
    var algorithm = ""
    var model = ""
    given Map[String, AnyRef => Unit] =
      Map(
        "aspect" -> { case it: String => aspect = Aspect(it).get.toString },
        "algorithm" -> { case it: String => algorithm = Algorithm(it).get.toString },
        "model" -> { case it: String => model = Model(it).get.toString }
      )

    self.ifObj.map(_ => Nest2(aspect, algorithm, model))


extension(self: Map[String, AnyRef])
  private def toSolution2: Option[Solution2] =
    var number = 0L
    var board: Board2 = null
    var queens: List[Point2] = null
    val dated = Dated()
    given Map[String, AnyRef => Unit] =
      Map[String, AnyRef => Unit](
        "number" -> { case it: String => number = it.toLong },
        "board" ->  { case it: Map[String, AnyRef] =>
                        board = it.toBoard2.get },
        "queens" -> { case it: List[Map[String, AnyRef]] =>
                        val qs = MutableList[Point2]()
                        it.foreach(qs += _.toPoint2.get)
                        queens = qs.toList.sorted
                      case None =>
                        queens = Nil
                    }
      ) ++ dated.setDated()

    self.ifObj.map(_ => Solution2(number, queens, board, dated.started, dated.ended, dated.elapsed, dated.idle))


extension(self: Map[String, AnyRef])
  private def toPoint2: Option[Point2] =
    var row = 0
    var col = 0
    given Map[String, AnyRef => Unit] =
      Map(
        "row" -> { case it: String => row = it.toInt },
        "column" -> { case it: String => col = it.toInt }
      )

    self.ifObj.map(_ => Point2(row, col))
