package queens
package pojo
package `4s`

import java.util.Date


case class Solution2(number: Long,
                     queens: List[Point2],
                     var board: Board2,
                     override val started: Date = null,
                     override val ended: Date = null,
                     override val elapsed: Long = -1L,
                     override val idle: Long = -1L)
  extends Dated, Comparable[Solution2]:

  override def compareTo(that: Solution2): Int =
    if this.number < that.number then -1
    else if this.number > that.number then 1
    else 0

  override def toString(): String = {
    import Solution2.given
    val it: Solution = this
    it.toString
  }

object Solution2:

  import Point2.given

  inline given Conversion[Solution2, Solution] = _.queens.map { it => val p: Point = it; p }

  inline given Conversion[Solution, List[Point2]] = _.map(Point2.apply.tupled)
