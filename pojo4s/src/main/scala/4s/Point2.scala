package queens
package pojo
package `4s`


abstract trait `Point2*`[P2 <: `Point2*`[P2]]
      extends Comparable[P2]:

  val row: Int
  val column: Int

  override def compareTo(that: P2): Int =
    if this.row == that.row
    then this.column - that.column
    else this.row - that.row


case class Point2(override val row: Int,
                  override val column: Int)
    extends `Point2*`[Point2]


object Point2:

  import common.geom.x

  inline given Conversion[Point2, Point] = { it => it.row x it.column }
