package queens
package pojo
package `4s`


case class Nest2(aspect: String, algorithm: String, model: String)


object Nest2:

  import dimensions.Dimension.{ Aspect, Algorithm, Model }

  import version.less.nest.Nest

  given Conversion[Nest2, Nest] with
    inline def apply(nest: Nest2): Nest =
      Nest(Aspect(nest.aspect).get,
           Algorithm(nest.algorithm).get,
           Model(nest.model).get)

  inline given Conversion[Nest, Nest2] = { nest =>
    Nest2(nest.aspect.toString, nest.algorithm.toString, nest.model.toString)
  }
