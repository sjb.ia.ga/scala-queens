package queens
package pojo
package `4s`

import java.util.Date


case class Board2(squares: List[Square2],
                  seed: Long,
                  override val started: Date = null,
                  override val ended: Date = null,
                  override val elapsed: Long = -1L,
                  override val idle: Long = -1L)
    extends Dated


object Board2:

  import scala.collection.mutable.{ ListBuffer => MutableList }

  import queens.base.Board

  given Conversion[Board2, Board] = { self =>
    given Option[Long] = if self.seed == Long.MinValue then None else Some(self.seed)

    if self.squares.isEmpty
    then
      new Board(Nil)
    else
      val squares = MutableList[Boolean]()

      var N = 0
      var p: Option[Square2] = None

      for
        sq <- self.squares.sorted
      do
        if N == 0 then
          if sq.row == 1 then N = p.get.column + 1
          else p = Some(sq)
        squares += !sq.free

      new Board(squares.toList.sliding(N, N).toList)
  }

  given Conversion[Board, List[Square2]] with
    def apply(self: Board): List[Square2] =
      val squares = MutableList[Square2]()
      for
        i <- 0 until self.N
        j <- 0 until self.N
      do
        squares += Square2(i, j, !self(i)(j))
      squares.toList
