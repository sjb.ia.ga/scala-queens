package queens
package pojo
package `4s`

import java.util.UUID
import java.util.Date


case class Queens2(tags: List[String],
                   validator: String,
                   tag: String,
                   uuid: UUID,
                   nest: Nest2,
                   var solutions: List[Solution2] = Nil,
                   override val started: Date = null,
                   override val ended: Date = null,
                   override val elapsed: Long = -1L,
                   override val idle: Long = -1L)
    extends Dated, Comparable[Queens2]:

  override def compareTo(that: Queens2): Int =
    if this.tag == that.tag
    then this.uuid.compareTo(that.uuid)
    else this.tag.compareTo(that.tag)


object Queens2:

  import Solution2.given

  inline given Conversion[List[Solution2], Seq[Solution]] = _.map { it => val s: Solution = it; s }.toSeq
