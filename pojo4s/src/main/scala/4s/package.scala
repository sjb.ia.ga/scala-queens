package queens
package pojo
package `4s`


object FromResponseToQueens
  extends pojo.FromResponseToQueens[Queens]:

  import dimensions.third.`Queens*`

  import Board2.given
  import Nest2.given
  import Queens2.given

  override def apply(queens: Queens, tag: Any, key: String): Iterable[`Queens3*`] =
    val board = queens.board
    for
      it <- queens.queens
    yield
      new Queens3(board, it.tags)(tag, key)(it.nest, it.solutions)()
