package queens
package pojo
package `4s`

import java.util.UUID


case class Cue2(board: Board2,
                nest: Nest2,
                max: Long,
                uuid: UUID,
                number: Long,
                cue: Long)
