package queens
package pojo
package `4s`

import java.util.Date


case class Queens(var board: Board2,
                  var count: Long = 0,
                  var queens: List[Queens2] = Nil,
                  override val started: Date = null,
                  override val ended: Date = null,
                  override val elapsed: Long = -1L,
                  override val idle: Long = -1L)
    extends Dated
