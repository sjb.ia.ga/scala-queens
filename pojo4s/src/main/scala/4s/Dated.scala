package queens
package pojo
package `4s`

import java.util.Date


abstract trait Dated:
  val started: Date
  val ended: Date
  val elapsed: Long
  val idle: Long
