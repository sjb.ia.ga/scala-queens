package queens
package pojo
package parser
package `4s`

import java.util.Date
import java.util.UUID

import org.scalatest.flatspec.AnyFlatSpec

import pojo.`4s`.{ Queens, Board2, Square2, Queens2, Nest2, Solution2, Point2 }


class CirceSpec
    extends AnyFlatSpec:

  import CirceSpec._
  import Person._

  import io.circe._
  import io.circe.parser._
  import io.circe.syntax._
  import io.circe.generic.auto._
  import pojo.parser.`4s`.Circe._


  "asJson" should "equal json" in {

    val json = """{"board":{"squares":[],"seed":0,"started":"1970-01-01T01:00:00.000+0100","ended":"1970-01-01T01:00:00.000+0100","elapsed":0,"idle":0},"count":0,"queens":[],"started":"1970-01-01T01:00:00.000+0100","ended":"1970-01-01T01:00:00.000+0100","elapsed":0,"idle":0}"""

    val queens = Queens(Board2(Nil, 0L, `70s`, `70s`, 0L, 0L), 0, Nil, `70s`, `70s`, 0L, 0L)

    assert(json == queens.asJson.noSpaces)

  }


  "queens" should "equal as" in {

    val nest2 = Nest2("classic", "recursive", "flow")

    val squares2 = List(Square2(0, 0, false), Square2(0, 1, true), Square2(1, 0, true), Square2(1, 1, false))

    val board2 = Board2(squares2, 0L, `70s`, `70s`, 0L, 0L)

    val solution2 = Solution2(1, List(Point2(0, 0), Point2(1, 1)), board2, `70s`, `70s`, 0L, 0L)

    val queens2 = Queens2(List(""), "opt", "1", UUID.randomUUID, nest2, List(solution2), `70s`, `70s`, 0L, 0L)

    val squares = List(Square2(0, 0, true), Square2(0, 1, true), Square2(1, 0, true), Square2(1, 1, true))

    val board = Board2(squares, 0L, `70s`, `70s`, 0L, 0L)

    val queens = Queens(board, 1, List(queens2), `70s`, `70s`, 0L, 0L)

    val `13` = queens.asJson.as[Queens]

    assert(queens == `13`.right.get)

  }

  "person json" should "equal asJson" in {

    assert("""{"name":"James","birth":"1970-01-01T01:00:00.000+0100"}""" == Person("James", `70s`).asJson.noSpaces)

  }

  "json person" should "equal as" in {

    import Person._

    val `13` = encodeSimplePerson(Map("name" -> "James")).as[SimplePerson]

    assert(`13`.right.get == SimplePerson("James"))

  }

  "social person obj" should "equal as" in {

    import Person._

    val her = SocialPerson("Jane", `70s`, Nil)
    val him = SocialPerson("James", `70s`, List(her))
    val she = SocialPerson("Joan", `70s`, List(him, her))

    assert(she.asJson.as[SocialPerson].right.get == she)

  }


object CirceSpec:

  val `70s` = Date(0L)

  case class SimplePerson(name: String)

  case class Person(name: String, birth: Date)

  case class SocialPerson(name: String, birth: Date, friends: List[Person])

  object Person:
   import io.circe._

   import Encoder.{ encodeMap, encodeString }

   val encodeSimplePerson = encodeMap[String, String]

   given Conversion[Person, SimplePerson] = { it => SimplePerson(it.name) }
   given Conversion[SocialPerson, Person] = { it => Person(it.name, it.birth) }
