ThisBuild / scalaVersion := "3.5.0-RC1"

Global / resolvers += "scala-integration" at "https://scala-ci.typesafe.com/artifactory/scala-integration/"

val scala2Opts = Seq("-feature", "-language:implicitConversions", "-deprecation", "-Ytasty-reader")
val scala3Opts = Seq("-feature", "-language:implicitConversions", "-indent", "-Xwiki-syntax", "-Xmax-inlines", "128", "-new-syntax")
// val scala2Opts = Seq("-feature", "-language:implicitConversions", "-explaintypes", "-deprecation", "-Ytasty-reader")
// val scala3Opts = Seq("-feature", "-language:implicitConversions", "-explain-types", "-indent", "-new-syntax")

lazy val root = (project in file("."))
  .aggregate(
    `plane-fifth-fourth-publish-subscribe`,
    `plane-fifth-fourth-web-simple`,
    `plane-fifth-fourth-web-cue`,
    `plane-fifth-fourth-web-io`,
    `plane-fourth-fifth-webapp-console`,
    `plane-fourth-fifth-webapp-console-cue`,
    `plane-fourth-fifth-third-program-monad-output-buffered-actors`,
    `plane-fourth-fifth-third-program-monad-output-buffered-futures`,
    `plane-fourth-fifth-third-program-monad-output-buffered-flow`,
    `plane-fourth-fifth-third-program-monad-output-console-actors`,
    `plane-fourth-fifth-third-program-monad-output-console-futures`,
    `plane-fourth-fifth-third-program-monad-output-console-flow`,
    `plane-fourth-fifth-third-program-monad-output-console-kafka`,
    `plane-fourth-fifth-third-program-monad-output-console-rabbitMQ`,
    `plane-fourth-fifth-third-program-monad-output-console-awsSQS`,
    `plane-fourth-fifth-third-program-monad-output-io-flow`,
    `plane-fourth-fifth-third-program-monad-output-database-slick-jdbc-flow`,
    `plane-fourth-fifth-third-program-monad-output-database-scalikejdbc-flow`,
    cipher
  )
  .settings(
    name := "queens",
    organization := "sjbiaga",
    version := "1.0",
  )

lazy val monad = (project in file("monad"))
  .settings(
    name := "queens.monad",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val board = (project in file("board"))
  .dependsOn(monad)
  .settings(
    name := "queens.nest.board",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val base = (project in file("base"))
  .dependsOn(board)
  .settings(
    name := "queens.base",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val nest = (project in file("nest"))
  .dependsOn(base)
  .settings(
    name := "queens.nest",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 36,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val comb = (project in file("comb"))
  .dependsOn(nest)
  .settings(
    name := "queens.comb",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 36,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `di-guice` = (project in file("di/guice"))
  .dependsOn(comb)
  .settings(
    name := "queens.comb.di.guice",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `di-macwire` = (project in file("di/macwire"))
  .dependsOn(comb)
  .settings(
    name := "queens.comb.di.macwire",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-monad-output-emito` = (project in file("plane/fourth/fifth/monad/output/emito"))
  .dependsOn(comb)
  .settings(
    name := "queens.hatch'em spawn'it output'o",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `axis-fifth-output-buffered` = (project in file("axis/fifth/output/buffered"))
  .dependsOn(comb)
  .settings(
    name := "queens.buffered",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-monad-output-buffered` = (project in file("plane/fourth/fifth/monad/output/buffered"))
  .dependsOn(
    `axis-fifth-output-buffered`,
    `plane-fourth-fifth-monad-output-emito`
  )
  .settings(
    name := "queens.monad.output.buffered",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-monad-output-database` = (project in file("plane/fourth/fifth/monad/output/database"))
  .dependsOn(
    `plane-fourth-fifth-monad-output-emito`
  )
  .settings(
    name := "queens.hatch'em spawn'it store'o",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `axis-fifth-output-database` = (project in file("axis/fifth/output/database"))
  .dependsOn(comb)
  .settings(
    name := "queens.database",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "2.13.14",
    scalacOptions ++= scala2Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-monad-output-database-query` = (project in file("plane/fourth/fifth/monad/output/database/query"))
  .dependsOn(
    `axis-fifth-output-database`,
    `plane-fourth-fifth-monad-output-database`
  )
  .settings(
    name := "queens.hatch'em spawn'it store'o query'q",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* ScalikeJDBC */

lazy val `axis-fifth-output-database-scalikejdbc` = (project in file("axis/fifth/output/database/scalikejdbc"))
  .dependsOn(`axis-fifth-output-database`)
  .settings(
    name := "queens.database.scalikejdbc",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "2.13.14",
    scalacOptions ++= scala2Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-monad-output-database-scalikejdbc-dao` = (project in file("plane/fourth/fifth/monad/output/database/scalikejdbc/dao"))
  .dependsOn(
    `axis-fifth-output-database-scalikejdbc`,
    `plane-fourth-fifth-monad-output-database`
  )
  .settings(
    name := "queens.monad.output.database.scalikejdbc.dao",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "2.13.14",
    scalacOptions ++= scala2Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-monad-output-database-scalikejdbc` = (project in file("plane/fourth/fifth/monad/output/database/scalikejdbc"))
  .dependsOn(
    `plane-fourth-fifth-monad-output-database-scalikejdbc-dao`,
    `plane-fourth-fifth-monad-output-database-query`
  )
  .settings(
    name := "queens.monad.output.database.scalikejdbc",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Slick */

lazy val `axis-fifth-output-database-slick` = (project in file("axis/fifth/output/database/slick"))
  .dependsOn(`axis-fifth-output-database`)
  .settings(
    name := "queens.database.slick",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "2.13.14",
    scalacOptions ++= scala2Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-monad-output-database-slick-dao` = (project in file("plane/fourth/fifth/monad/output/database/slick/dao"))
  .dependsOn(
    `axis-fifth-output-database-slick`,
    `plane-fourth-fifth-monad-output-database`
  )
  .settings(
    name := "queens.monad.output.database.slick.dao",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "2.13.14",
    scalacOptions ++= scala2Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-monad-output-database-slick` = (project in file("plane/fourth/fifth/monad/output/database/slick"))
  .dependsOn(
    `plane-fourth-fifth-monad-output-database-slick-dao`,
    `plane-fourth-fifth-monad-output-database-query`
  )
  .settings(
    name := "queens.monad.output.database.slick",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Plain Old Java Object */  ////////////

lazy val pojo = (project in file("pojo"))
  .dependsOn(comb)
  .settings(
    name := "queens.pojo",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val pojo4s = (project in file("pojo4s"))
  .dependsOn(pojo)
  .settings(
    name := "queens.pojo4s",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Hive */  /////////////////////////////

lazy val hive = (project in file("hive"))
  .dependsOn(`di-guice`, `di-macwire`,
    `plane-fourth-fifth-monad-output-emito`,
    `plane-fourth-fifth-monad-output-buffered`,
    `plane-fourth-fifth-monad-output-database-scalikejdbc`,
    `plane-fourth-fifth-monad-output-database-slick`,
    `pojo4s`
  )
  .settings(
    name := "queens.hive",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val wax = (project in file("wax"))
  .dependsOn(hive)
  .settings(
    name := "queens.wax",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Monad Program (Output) */  ///////////////////////////////////////////////////////////////////////////////////////////////

lazy val `plane-fourth-fifth-third-program-monad-output` = (project in file("plane/fourth/fifth/third/program/monad/output"))
  .dependsOn(hive,
    `axis-fifth-output-buffered`,
    `axis-fifth-output-database-scalikejdbc`,
    `axis-fifth-output-database-slick`,
    `plane-fourth-fifth-monad-output-database-scalikejdbc`,
    `plane-fourth-fifth-monad-output-database-slick`
  )
  .settings(
    name := "queens.app",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Publish/Subscribe (Stream) */  /////////////////////////////////////////////////////////////////////////

lazy val `plane-fifth-fourth-publish-subscribe` = (project in file("plane/fifth/fourth/publish/subscribe"))
  .dependsOn(wax)
  .settings(
    name := "queens.publish.subscribe",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    javacOptions ++= Seq("-source", "16"),
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Web (Simple) */  /////////////////////////////////////////////////////////////////////////

lazy val `plane-fifth-fourth-web-simple` = (project in file("plane/fifth/fourth/web/simple"))
  .dependsOn(wax)
  .enablePlugins(TomcatPlugin)
  .settings(
    name := "queens.server.web",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    javacOptions ++= Seq("-source", "16"),
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Web (Cue) */  //////////////////////////////////////////////////////////////////////

lazy val `plane-fifth-fourth-web-cue` = (project in file("plane/fifth/fourth/web/cue"))
  .dependsOn(wax, `plane-fifth-fourth-web-simple`, `plane-fifth-fourth-publish-subscribe`)
  .settings(
    name := "queens.server.cue.web",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    javacOptions ++= Seq("-source", "16"),
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Web (IO) */  /////////////////////////////////////////////////////////////////////

lazy val `plane-fifth-fourth-web-io` = (project in file("plane/fifth/fourth/web/io"))
  .dependsOn(wax, `plane-fifth-fourth-web-simple`)
  .settings(
    name := "queens.server.io.web",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    javacOptions ++= Seq("-source", "16"),
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Web App */  ////////////////////////////////////////////////////////////////////////////

lazy val `axis-fourth-webapp` = (project in file("axis/fourth/webapp"))
  .dependsOn(wax)
  .settings(
    name := "queens.client.webapp",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    javacOptions ++= Seq("-source", "16"),
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-webapp-console` = (project in file("plane/fourth/fifth/webapp/console"))
  .dependsOn(`axis-fourth-webapp`)
  .settings(
    name := "queens.webapp.console",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-webapp-console-cue` = (project in file("plane/fourth/fifth/webapp/console/cue"))
  .dependsOn(`axis-fourth-webapp`)
  .settings(
    name := "queens.webapp.console.cue",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Monad Buffered */

lazy val `plane-fourth-fifth-third-program-monad-output-buffered-actors` = (project in file("plane/fourth/fifth/third/program/monad/output/buffered/actors"))
  .dependsOn(`plane-fourth-fifth-third-program-monad-output`)
  .settings(
    name := "queens.app.buffered.actors",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-third-program-monad-output-buffered-futures` = (project in file("plane/fourth/fifth/third/program/monad/output/buffered/futures"))
  .dependsOn(`plane-fourth-fifth-third-program-monad-output`)
  .settings(
    name := "queens.app.buffered.futures",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-third-program-monad-output-buffered-flow` = (project in file("plane/fourth/fifth/third/program/monad/output/buffered/flow"))
  .dependsOn(`plane-fourth-fifth-third-program-monad-output`)
  .settings(
    name := "queens.app.buffered.flow",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Monad Console */

lazy val `plane-fourth-fifth-third-program-monad-output-console-actors` = (project in file("plane/fourth/fifth/third/program/monad/output/console/actors"))
  .dependsOn(`plane-fourth-fifth-third-program-monad-output`)
  .settings(
    name := "queens.app.console.actors",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-third-program-monad-output-console-futures` = (project in file("plane/fourth/fifth/third/program/monad/output/console/futures"))
  .dependsOn(`plane-fourth-fifth-third-program-monad-output`)
  .settings(
    name := "queens.app.console.futures",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-third-program-monad-output-console-flow` = (project in file("plane/fourth/fifth/third/program/monad/output/console/flow"))
  .dependsOn(`plane-fourth-fifth-third-program-monad-output`)
  .settings(
    name := "queens.app.console.flow",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-third-program-monad-output-console-kafka` = (project in file("plane/fourth/fifth/third/program/monad/output/console/kafka"))
  .dependsOn(`plane-fourth-fifth-third-program-monad-output`)
  .settings(
    name := "queens.app.console.kafka",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-third-program-monad-output-console-rabbitMQ` = (project in file("plane/fourth/fifth/third/program/monad/output/console/rabbitMQ"))
  .dependsOn(`plane-fourth-fifth-third-program-monad-output`)
  .enablePlugins(DockerPlugin)
  .settings(
    name := "queens.app.console.rabbitMQ",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val `plane-fourth-fifth-third-program-monad-output-console-awsSQS` = (project in file("plane/fourth/fifth/third/program/monad/output/console/awsSQS"))
  .dependsOn(`plane-fourth-fifth-third-program-monad-output`)
  .enablePlugins(DockerPlugin)
  .settings(
    name := "queens.app.console.awsSQS",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Monad IO */  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

lazy val `plane-fourth-fifth-third-program-monad-output-io-flow` = (project in file("plane/fourth/fifth/third/program/monad/output/io/flow"))
  .dependsOn(`plane-fourth-fifth-third-program-monad-output`)
  .settings(
    name := "queens.app.io.flow",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Monad Scalikejdbc */  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

lazy val `plane-fourth-fifth-third-program-monad-output-database-scalikejdbc-flow` = (project in file("plane/fourth/fifth/third/program/monad/output/database/scalikejdbc/flow"))
  .dependsOn(`plane-fourth-fifth-third-program-monad-output`)
  .settings(
    name := "queens.app.database.scalikejdbc.flow",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

/* Monad Slick Jdbc */

lazy val `plane-fourth-fifth-third-program-monad-output-database-slick-jdbc-flow` = (project in file("plane/fourth/fifth/third/program/monad/output/database/slick/jdbc/flow"))
  .dependsOn(`plane-fourth-fifth-third-program-monad-output`)
  .settings(
    name := "queens.app.database.slick.jdbc.flow",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "3.5.0-RC1",
    crossScalaVersions ++= Seq("2.13.14", "3.5.0-RC1"),
    scalacOptions ++= scala3Opts, // :+ "-Xprint:typer",
  )

lazy val cipher = (project in file("cipher"))
  .settings(
    name := "queens.cipher",
    organization := "sjbiaga",
    version := "1.0",
    maxErrors := 5,
    scalaVersion := "2.11.12",
    scalacOptions ++= Seq("-feature", "-language:implicitConversions", "-explaintypes", "-deprecation"), // :+ "-Xprint:typer",
  )

// ThisBuild / evictionErrorLevel := Level.Info

Global / bloopExportJarClassifiers := Some(Set("sources"))
Global / onChangedBuildSource := ReloadOnSourceChanges
//Global / onChangedBuildSource := IgnoreSourceChanges
