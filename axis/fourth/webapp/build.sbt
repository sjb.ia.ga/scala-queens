val springBootVersion = "3.3.0"
val jacksonVersion = "2.17.1"
val http4sVersion = "0.22.15"

libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",
  // micronaut
  "io.micronaut" % "micronaut-http-client" % "4.5.3",
  "io.micronaut" % "micronaut-jackson-databind" % "4.5.3",

  //okhttp
  "com.squareup.okhttp3" % "okhttp" % "4.12.0",
  "com.squareup.moshi" % "moshi" % "1.15.1", // % "runtime",
  "com.squareup.moshi" % "moshi-adapters" % "1.15.1",

  // retrofit
  "com.squareup.retrofit2" % "retrofit" % "2.11.0",
  "com.squareup.retrofit2" % "converter-gson" % "2.11.0",

  // spring
  "org.springframework.boot" % "spring-boot-starter-web" % springBootVersion
    exclude("org.springframework.boot", "spring-boot-starter-tomcat"),

  // sttp
  "com.softwaremill.sttp.client3" %% "core" % "3.9.7",
  "com.softwaremill.sttp.client3" %% "circe" % "3.9.7",
  "io.circe" %% "circe-generic" % "0.14.7",
  //"com.softwaremill.sttp.client3" %% "upickle" % "3.9.7",
  "com.softwaremill.sttp.client3" %% "slf4j-backend" % "3.9.7",
  "com.lihaoyi" %% "upickle" % "3.3.1",

  // vertx
  "io.vertx" % "vertx-web-client" % "4.5.8",
  "io.vertx" % "vertx-rx-java3" % "4.5.8",
  "com.fasterxml.jackson.core" % "jackson-databind" % jacksonVersion,

  // reactive
  "io.reactivex.rxjava3" % "rxjava" % "3.1.8",
  "io.projectreactor" % "reactor-core" % "3.6.7",
  "org.springframework.boot" % "spring-boot-starter-webflux" % springBootVersion
    exclude("org.springframework.boot", "spring-boot-starter-tomcat"),

  // http4s
  "org.http4s" %% "http4s-blaze-client" % http4sVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,
  "org.http4s" %% "http4s-dsl" % http4sVersion,

  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
