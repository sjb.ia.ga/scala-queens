package queens
package axis.fourth.webapp
package http4s

import java.util.LinkedList

import scala.util.{ Try, Failure }
import scala.collection.Map
import scala.concurrent.ExecutionContext.global

import cats.effect.{ ContextShift, IO }

import org.http4s.circe._
import org.http4s.client.blaze._
import org.http4s.client._
import org.http4s.client.dsl.io._
import org.http4s.headers._
import org.http4s.{ EntityDecoder, MediaType, Method, Uri }

import common.pipeline.Context
import common.pipeline.Metadata
import Metadata.{ Boot, Media }

import pojo.`4s`.Queens
import pojo.parser.`4s`.Circe.given
import pojo.parser.read.`4s`.toPojo

import dimensions.fourth.app.Parameter

import dimensions.fourth.webapp.QueensWebAppInterface.Web
import dimensions.fourth.webapp.`Web Client`


class `http4s Web Client`(
  override protected val next: Option[Context => `Web Client`] = None
) extends `Web Client`:

  import `http4s Web Client`.GET

  given ContextShift[IO] = IO.contextShift(global)

  given EntityDecoder[IO, Queens] = jsonOf

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(_, web @ Web[Queens, GET](Right(url), media, params*), Some(key: String)) =>
        val effect = BlazeClientBuilder[IO](global).resource.use { httpClient =>
          media match {
            case "application/json" =>
              val request = Method.GET(Uri.unsafeFromString(url), Accept(MediaType.application.json))
              httpClient.expect[Queens](request)
            case _ =>
              val request = Method.GET(Uri.unsafeFromString(url), Accept(MediaType.text.plain))
              httpClient.expect[String](request)
          }
        }
        val method = GET(effect, url, media, params*)
        web(key -> Some(method))


  override protected def >> = {
    case Media(_, Some(it @ GET(request, _, media, _*))) => { _ =>
      it.result = Try {
        media match {
          case "application/json" => request.asInstanceOf[IO[Queens]].unsafeRunSync()
          case _ => request.asInstanceOf[IO[String]].unsafeRunSync().toPojo(" ").get
        }
      }

      it.result.isSuccess
    }
  }

  override def toString(): String = "http4s " + super.toString


object `http4s Web Client`:

  import dimensions.fourth.app.Parameter
  import dimensions.fourth.webapp.WebMethod

  case class GET(override protected[http4s] val request: IO[?],
                 url: String,
                 media: String,
                 override val params: Parameter[?]*)
      extends WebMethod[IO[?], Queens, GET]
