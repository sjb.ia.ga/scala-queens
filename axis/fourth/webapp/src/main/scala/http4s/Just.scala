package queens
package axis.fourth.webapp
package http4s

import java.util.UUID

import scala.util.Try

import base.Board

import common.monad.Grow

import dimensions.third.`Queens*`

import pojo.`4s`.Queens

import `http4s Web Client`.GET


final case class Just[T](override val board: Board,
                         override val tag: T,
                         override val uuid: UUID,
                         override val number: Long,
                         responses: Iterable[(Int, GET, Try[Queens], Queens => Iterable[`Queens3*`])],
                         results: (Int, GET, Queens, `Queens3*`)*)
    extends Grow.Just[T]
