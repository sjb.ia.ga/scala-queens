package queens
package axis.fourth.webapp
package http4s

import scala.collection.Map

import org.http4s._
import org.http4s.implicits._

import dimensions.Dimension.{ Aspect, Algorithm, Model }

import pojo.`4s`.Queens

import dimensions.fourth.webapp.`Web*`

import `http4s Web Client`.GET

import axis.fourth.webapp.spring.`Spring Web*`


object `http4s Web*`
    extends `Web*`[Queens, GET]:

  override def apply(baseUrl: String,
                     matrix: Map[Model, Map[Aspect, Map[Algorithm, Int]]],
                     query: Map[String, String]
  ): Iterable[Either[Any, String]] = `Spring Web*`(baseUrl, matrix, query)
