package queens
package axis.fourth.webapp
package http4s

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.Flag
import common.monad.Item

import pojo.`4s`.Queens
import pojo.`4s`.FromResponseToQueens

import dimensions.fourth.webapp.`Web Client`
import dimensions.fourth.webapp.QueensWebAppInterface
import QueensWebAppInterface.Web
import dimensions.fourth.webapp.`Base.QueensWebAppInterface`

import `http4s Web Client`.GET


class `http4s Web App`(
  webs: Iterable[Web[Queens, GET]]
)(
  _tag: Any,
  _its: Seq[UUID]
)(
  _fs: Item[`Web Client`] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.QueensWebAppInterface`[
  Queens, GET
](FromResponseToQueens, webs)(_tag, _its)(_fs*):

  override protected type * = `http4s Web App`

  override def `#`(t: Any): * =
    new `http4s Web App`(webs)(t, _its)(_fs*)

  override protected def `apply*`(s: Item[`Web Client`] => Boolean*): * =
    new `http4s Web App`(webs)(_tag, _its)(s*)

  override def toString(): String = "http4s " + super.toString


object `http4s Web App`:

  def apply(webs: Iterable[Web[Queens, GET]]): `http4s Web App` =

    val its = UUID.randomUUID :: Nil

    given Map[UUID, (Boolean, Flag)] = new Map()

    import common.NoTag

    new `http4s Web App`(webs)(NoTag, its)()
