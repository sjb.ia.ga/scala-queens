package object queens:

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]


  type Identity_[A] = A

  type Iterable_[X[_], A] = Iterable[A]


  import java.util.UUID

  import base.Board

  import version.less.nest.Nest

  type Cue = ((Board, Nest, Long, UUID), Long, Long, Long, Long)


  import dimensions.third.`Queens*`

  type `Queens3*` = `Queens*`[Point, Boolean]
