package queens
package axis.fourth.webapp
package retrofit

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.Flag
import common.monad.Item

import pojo.gson.Queens
import pojo.gson.FromResponseToQueens

import dimensions.fourth.webapp.`Web Client`
import dimensions.fourth.webapp.QueensWebAppInterface
import QueensWebAppInterface.Web
import dimensions.fourth.webapp.`Base.QueensWebAppInterface`

import `Retrofit Web Client`.Get


class `Retrofit Web App`(
  webs: Iterable[Web[Queens, Get]]
)(
  _tag: Any,
  _its: Seq[UUID]
)(
  _fs: Item[`Web Client`] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.QueensWebAppInterface`[
  Queens, Get
](FromResponseToQueens, webs)(_tag, _its)(_fs*):

  override protected type * = `Retrofit Web App`

  override def `#`(t: Any): * =
    new `Retrofit Web App`(webs)(t, _its)(_fs*)

  override protected def `apply*`(s: Item[`Web Client`] => Boolean*): * =
    new `Retrofit Web App`(webs)(_tag, _its)(s*)

  override def toString(): String = "Retrofit " + super.toString


object `Retrofit Web App`:

  def apply(webs: Iterable[Web[Queens, Get]]): `Retrofit Web App` =

    val its = UUID.randomUUID :: Nil

    given Map[UUID, (Boolean, Flag)] = new Map()

    import common.NoTag

    new `Retrofit Web App`(webs)(NoTag, its)()
