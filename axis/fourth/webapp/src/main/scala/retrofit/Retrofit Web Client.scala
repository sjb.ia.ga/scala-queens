package queens
package axis.fourth.webapp
package retrofit

import scala.util.Try
import scala.collection.JavaConverters.asJava

import common.pipeline.Context
import common.pipeline.Metadata
import Metadata.{ Boot, Media }

import pojo.gson.Queens
import pojo.gson.jPojoGson.toPojo

import dimensions.fourth.webapp.QueensWebAppInterface.Web
import dimensions.fourth.webapp.`Web Client`


class `Retrofit Web Client`(
  override protected val next: Option[Context => `Web Client`] = None
) extends `Web Client`:

  import `Retrofit Web Client`.Get

  import retrofit2.Response

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(_, web @ Web[Queens, Get](Left(baseUrl, matrix: String, query), media, params*), Some(key: String)) =>
        val service = api.`Retrofit Client`(baseUrl)
        val call = service.solve(matrix, asJava(query), media)
        val method = Get(call, call.request.url, media, params*)
        web(key -> Some(method))

  override protected def >> = {
    case Media(_, Some(it @ Get(call, _, _, _*))) => { _ =>
      it.result = Try {
        val response = call.execute
        if response.isSuccessful
        then
          response.body
        else
          throw java.io.IOException(response.errorBody.string)
      }
      it.result.isSuccess
    }
  }

override def toString(): String = "Retrofit " + super.toString


object `Retrofit Web Client`:

  import retrofit2.Call
  import okhttp3.HttpUrl

  import dimensions.fourth.app.Parameter
  import dimensions.fourth.webapp.WebMethod

  case class Get(override protected[retrofit] val request: Call[Queens],
                 url: HttpUrl,
                 media: String,
                 override val params: Parameter[?]*)
      extends WebMethod[Call[Queens], Queens, Get]
