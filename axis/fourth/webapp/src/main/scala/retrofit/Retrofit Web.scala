package queens
package axis.fourth.webapp
package retrofit

import scala.collection.Map

import dimensions.Dimension.{ Aspect, Algorithm, Model }

import pojo.gson.Queens

import dimensions.fourth.webapp.`Web*`

import `Retrofit Web Client`.Get


object `Retrofit Web*`
    extends `Web*`[Queens, Get]:

  override def apply(baseUrl: String,
                     matrix: Map[Model, Map[Aspect, Map[Algorithm, Int]]],
                     query: Map[String, String]
  ): Iterable[Either[Any, String]] =
    for
      (ml, mat) <- matrix
      (at, mam) <- mat
    yield
      Left {
        List(ml.toString
            ,at.toString+"=1"
            ,mam.map(_.toString+"="+_).mkString(";")
        )
        .mkString(";") + ";"
      }
