package queens
package axis.fourth.webapp
package retrofit
package api

import java.util.Map

import retrofit2.Call
import retrofit2.http.{ GET, Header, Path, QueryMap }

import pojo.gson.Queens


trait `Queens Service` {

    @GET("model/{matrix}")
    def solve(@Path("matrix")   matrix: String,
              @QueryMap         query: Map[String, String],
              @Header("Accept") media: String
    ): Call[Queens]

}
