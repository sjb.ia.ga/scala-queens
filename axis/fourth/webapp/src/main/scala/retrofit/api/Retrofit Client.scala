package queens
package axis.fourth.webapp
package retrofit
package api

import scala.reflect.ClassTag

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object `Retrofit Client`:

  def retrofit(baseUrl: String): Retrofit = Retrofit.Builder()
    .baseUrl(baseUrl)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

  def apply(baseUrl: String): `Queens Service` = retrofit(baseUrl)
    .create(classOf[`Queens Service`])
