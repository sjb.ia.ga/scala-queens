package queens
package axis.fourth.webapp
package spring

import scala.collection.Map

import dimensions.Dimension.{ Aspect, Algorithm, Model }

import pojo.jackson.Queens

import dimensions.fourth.webapp.`Web*`

import `Spring Web Client`.Get


object `Spring Web*`
    extends `Web*`[Queens, Get]:

  override def apply(baseUrl: String,
                     matrix: Map[Model, Map[Aspect, Map[Algorithm, Int]]],
                     query: Map[String, String]
  ): Iterable[Either[Any, String]] =
    for
      (ml, mat) <- matrix
      (at, mam) <- mat
    yield
      Right {
        List(s"${baseUrl}model/$ml"
            ,s"$at=1"
            ,mam.map(_.toString+"="+_).mkString(";")
        )
        .mkString(";") + ";"
      }
