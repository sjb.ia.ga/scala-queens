package queens
package axis.fourth.webapp
package spring

import java.util.LinkedList

import scala.collection.Map
import scala.util.Try

import org.springframework.web.client.RestClientException

import org.springframework.http.{ HttpHeaders, HttpEntity }
import org.springframework.http.HttpMethod.GET

import org.springframework.http.MediaType
import org.springframework.http.MediaType.{ APPLICATION_JSON, APPLICATION_JSON_VALUE
                                          , APPLICATION_XML, APPLICATION_XML_VALUE
                                          , TEXT_PLAIN, TEXT_PLAIN_VALUE }

import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.StringHttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter

import org.springframework.web.client.RestTemplate

import common.pipeline.Context
import common.pipeline.Metadata
import Metadata.{ Boot, Media }

import pojo.jackson.Queens
import pojo.jackson.jPojoJackson.toPojo

import dimensions.fourth.webapp.QueensWebAppInterface.Web
import dimensions.fourth.webapp.`Web Client`


class `Spring Web Client`(
  override protected val next: Option[Context => `Web Client`] = None
) extends `Web Client`:

  import `Spring Web Client`.Get

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(_, web @ Web[Queens, Get](Right(url), media, params*), Some(key: String)) =>
        val restTemplate = RestTemplate()
        val requestHeaders = HttpHeaders()
        val mediaType = `Spring Web Client`(media)
        val (converters, acceptableMediaTypes) = `Spring Web Client`(mediaType)
        restTemplate.setMessageConverters(converters)
        requestHeaders.setAccept(acceptableMediaTypes)
        val requestEntity = HttpEntity(requestHeaders)
        val method = Get((restTemplate, requestEntity), url, media, params*)
        web(key -> Some(method))

  override protected def >> = {
    case Media(_, Some(it @ Get((restTemplate, requestEntity), url, media, _*))) => { _ =>
      it.result = Try {
        val mediaType = `Spring Web Client`(media)
        val response = mediaType match
          case APPLICATION_JSON | APPLICATION_XML =>
            val responseEntity = restTemplate.exchange(url, GET, requestEntity, classOf[Queens])
            responseEntity.getBody()
          case _ =>
            val responseEntity = restTemplate.exchange(url, GET, requestEntity, classOf[String])
            responseEntity.getBody().toPojo(" ").get
        response
      }
      it.result.isSuccess
    }
  }

  override def toString(): String = "Spring " + super.toString


object `Spring Web Client`:

  lazy val cs = Map[MediaType, HttpMessageConverter[?]](
    APPLICATION_JSON -> MappingJackson2HttpMessageConverter(),
    APPLICATION_XML -> MappingJackson2XmlHttpMessageConverter(),
    TEXT_PLAIN -> StringHttpMessageConverter()
  )

  private def apply(mediaType: MediaType): (LinkedList[HttpMessageConverter[?]], LinkedList[MediaType]) =
    val converters = LinkedList[HttpMessageConverter[?]]()
    converters.add(cs(mediaType))
    val acceptableMediaTypes = LinkedList[MediaType]()
    acceptableMediaTypes.add(mediaType)
    converters -> acceptableMediaTypes

  private def apply(media: String): MediaType = MediaType.valueOf(media)


  import dimensions.fourth.app.Parameter
  import dimensions.fourth.webapp.WebMethod

  case class Get(override protected[spring] val request: (RestTemplate, HttpEntity[?]),
                 url: String,
                 media: String,
                 override val params: Parameter[?]*)
      extends WebMethod[(RestTemplate, HttpEntity[?]), Queens, Get]
