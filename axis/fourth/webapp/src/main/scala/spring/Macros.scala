package queens
package axis.fourth.webapp
package spring

import scala.quoted.{ Expr, Type, Quotes }


object Macros:

  import queens.base.Board

  import queens.base.Macros.`co*Code`

  import dimensions.third.`Queens*`

  import axis.fourth.webapp.spring.`Spring Web Client`.Get

  import pojo.jackson.Queens
  import pojo.jPojo.given

  def coCode(
    tag: Expr[Any],
    results: Expr[Seq[(Int, Get, Queens, `Queens3*`)]],
    cb: Expr[(Queens, Get) => Boolean],
    fun: Expr[(`Queens3*`, Solution) => Unit],
    max: Expr[Long], preemption: Expr[Boolean])
   (using Quotes): Expr[Unit] =
   '{
      val cb2 = { (_: `Queens3*`, _: Solution, _: Long) => true }
      for
        (_, method, response, queens) <- $results
        if $cb(response, method)
      do
        val board: Board = response.getBoard
        ${ `co*Code`[Boolean](tag, 'board, 'queens, 'cb2, fun, max, preemption) }
    }

  inline def co(
    inline tag: Any,
    inline results: Seq[(Int, Get, Queens, `Queens3*`)])
   (inline cb: (Queens, Get) => Boolean = { (_: Queens, _: Get) => true })
   (using inline max: Long, inline preemption: Boolean): Unit =
    ${ coCode('tag, 'results, 'cb, '{ { (_: `Queens3*`, _: Solution) => } }, 'max, 'preemption) }


  def coCode2(
    tag: Expr[Any],
    results: Expr[Seq[(Int, Get, Queens, `Queens3*`)]],
    cb: Expr[(Queens, Get) => Boolean],
    cb2: Expr[(`Queens3*`, Solution, Long) => Boolean],
    fun: Expr[(`Queens3*`, Solution) => Unit],
    max: Expr[Long], preemption: Expr[Boolean])
   (using Quotes): Expr[Unit] =
   '{
      for
        (_, method, response, queens) <- $results
        if $cb(response, method)
      do
        val board: Board = response.getBoard
        ${ `co*Code`(tag, 'board, 'queens, cb2, fun, max, preemption) }
    }

  inline def `co*`(
    inline tag: Any,
    inline results: Seq[(Int, Get, Queens, `Queens3*`)])
   (inline cb: (Queens, Get) => Boolean = { (_: Queens, _: Get) => true })
   (inline cb2: (`Queens3*`, Solution, Long) => Boolean = { (_: `Queens*`[Point, Boolean], _: Solution, _: Long) => true })
   (inline fun: (`Queens3*`, Solution) => Unit = { (_: `Queens*`[Point, Boolean], _: Solution) => })
   (using inline max: Long, inline preemption: Boolean): Unit =
   ${ coCode2('tag, 'results, 'cb, 'cb2, 'fun, 'max, 'preemption) }
