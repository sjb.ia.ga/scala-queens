package queens
package axis.fourth.webapp
package spring

import java.util.UUID

import scala.util.Try

import base.Board

import common.monad.Grow
import Grow.ItemValue

import dimensions.fourth.app.AppData
import dimensions.fourth.webapp.WebMethod

import dimensions.third.`Queens*`

import pojo.jackson.Queens

import `Spring Web Client`.Get


implicit class ToJust[T](using tag: Any => T)
    extends Grow.ToJust[T, ItemValue[?]]:
  override type J = Just[T]
  inline def apply(r: (Board, T, UUID, Long, ItemValue[?], ItemValue[?])): J =
    val responses = r._5.it.asInstanceOf[Iterable[(Int, Get, Try[Queens], Queens => Iterable[`Queens3*`])]]
    val results = r._6.it.asInstanceOf[Seq[(Int, Get, Queens, `Queens3*`)]]
    Just(r._1, r._2, r._3, r._4, responses, results*)
