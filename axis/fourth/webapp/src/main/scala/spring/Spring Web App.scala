package queens
package axis.fourth.webapp
package spring

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.Flag
import common.monad.Item

import pojo.jackson.Queens
import pojo.jackson.FromResponseToQueens

import dimensions.fourth.webapp.`Web Client`
import dimensions.fourth.webapp.QueensWebAppInterface
import QueensWebAppInterface.Web
import dimensions.fourth.webapp.`Base.QueensWebAppInterface`

import `Spring Web Client`.Get


class `Spring Web App`(
  webs: Iterable[Web[Queens, Get]]
)(
  _tag: Any,
  _its: Seq[UUID]
)(
  _fs: Item[`Web Client`] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.QueensWebAppInterface`[
  Queens, Get
](FromResponseToQueens, webs)(_tag, _its)(_fs*):

  override protected type * = `Spring Web App`

  override def `#`(t: Any): * =
    new `Spring Web App`(webs)(t, _its)(_fs*)

  override protected def `apply*`(s: Item[`Web Client`] => Boolean*): * =
    new `Spring Web App`(webs)(_tag, _its)(s*)

  override def toString(): String = "Spring " + super.toString


object `Spring Web App`:

  def apply(webs: Iterable[Web[Queens, Get]]): `Spring Web App` =

    val its = UUID.randomUUID :: Nil

    given Map[UUID, (Boolean, Flag)] = new Map()

    import common.NoTag

    new `Spring Web App`(webs)(NoTag, its)()
