package queens
package axis.fourth.webapp
package spring

import java.util.UUID

import scala.util.Try

import base.Board

import common.monad.Grow

import dimensions.third.`Queens*`

import pojo.jackson.Queens

import `Spring Web Client`.Get


final case class Just[T](override val board: Board,
                         override val tag: T,
                         override val uuid: UUID,
                         override val number: Long,
                         responses: Iterable[(Int, Get, Try[Queens], Queens => Iterable[`Queens3*`])],
                         results: (Int, Get, Queens, `Queens3*`)*)
    extends Grow.Just[T]
