package queens
package axis.fourth.webapp
package okhttp

import scala.util.Try

import common.pipeline.Context
import common.pipeline.Metadata
import Metadata.{ Boot, Media }

import pojo.moshi.Queens

import dimensions.fourth.webapp.QueensWebAppInterface.Web
import dimensions.fourth.webapp.`Web Client`


class `OkHttp Web Client`(
  override protected val next: Option[Context => `Web Client`] = None
) extends `Web Client`:

  import `OkHttp Web Client`.Get

  import okhttp3.Response

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(_, web @ Web[Queens, Get](Right(url), media, params*), Some(key: String)) =>
        val request = api.`OkHttp Client`(url, media)
        val call = api.`OkHttp Client`(request)
        val method = Get(call, call.request.url, media, params*)
        web(key -> Some(method))

  override protected def >> = {
    case Media(_, Some(it @ Get(call, _, media, _*))) => { _ =>
      it.result = Try {
        val response = call.execute
        if response.isSuccessful
        then
          api.`OkHttp Client`(response, media)
        else
          throw java.io.IOException("Unexpected code " + response)
      }
      it.result.isSuccess
    }
  }

  override def toString(): String = "OkHttp " + super.toString


object `OkHttp Web Client`:

  import okhttp3.{ Call, HttpUrl }

  import dimensions.fourth.app.Parameter
  import dimensions.fourth.webapp.WebMethod

  case class Get(override protected[okhttp] val request: Call,
                 url: HttpUrl,
                 media: String,
                 override val params: Parameter[?]*)
      extends WebMethod[Call, Queens, Get]
