package queens
package axis.fourth.webapp
package okhttp
package api

import java.util.UUID
import java.util.Date

import okhttp3.{ Call, OkHttpClient, Request, Response }
import com.squareup.moshi.{ JsonAdapter, Moshi }
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter

import pojo.moshi.Queens
import pojo.moshi.jPojoMoshi.toPojo


object `OkHttp Client`:

  private val client: OkHttpClient = new OkHttpClient.Builder()
    .build()

  def apply(url: String, media: String): Request = new Request.Builder()
    .url(url)
    .header("User-Agent", "OkHttp Queens")
    .addHeader("Accept", media)
    .build()

  def apply(request: Request): Call = client.newBuilder()
    .build()
    .newCall(request)


  private val moshi: Moshi = new Moshi.Builder()
    .add(classOf[Date], Rfc3339DateJsonAdapter())
    .add(classOf[UUID], UUIDJsonAdapter)
    .build()

  private val queensJsonAdapter: JsonAdapter[Queens] = moshi.adapter(classOf[Queens])

  def apply(response: Response, media: String): Queens =
    media match
      case "application/json" =>
        val json = response.body.source()
        queensJsonAdapter.fromJson(json)

      case "text/plain" =>
        val text = response.body.string()
        text.toPojo(" ").get

      case _ => ???


  private object UUIDJsonAdapter extends JsonAdapter[UUID]:

    import com.squareup.moshi.{ JsonReader, JsonWriter }

    override def fromJson(reader: JsonReader): UUID = synchronized {
      if reader.peek eq null
      then
        reader.nextNull
      else
        val string = reader.nextString
        UUID.fromString(string)
    }

    override def toJson(writer: JsonWriter, value: UUID): Unit = synchronized {
      if value eq null
      then
        writer.nullValue
      else
        val string = value.toString
        writer.value(string)
    }
