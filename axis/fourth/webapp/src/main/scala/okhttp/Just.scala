package queens
package axis.fourth.webapp
package okhttp

import java.util.UUID

import scala.util.Try

import base.Board

import common.monad.Grow

import dimensions.third.`Queens*`

import pojo.moshi.Queens

import `OkHttp Web Client`.Get


final case class Just[T](override val board: Board,
                         override val tag: T,
                         override val uuid: UUID,
                         override val number: Long,
                         responses: Iterable[(Int, Get, Try[Queens], Queens => Iterable[`Queens3*`])],
                         results: (Int, Get, Queens, `Queens3*`)*)
    extends Grow.Just[T]
