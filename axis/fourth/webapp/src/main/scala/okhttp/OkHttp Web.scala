package queens
package axis.fourth.webapp
package okhttp

import scala.collection.Map

import dimensions.Dimension.{ Aspect, Algorithm, Model }

import pojo.moshi.Queens

import dimensions.fourth.webapp.`Web*`

import `OkHttp Web Client`.Get

import axis.fourth.webapp.spring.`Spring Web*`


object `OkHttp Web*`
    extends `Web*`[Queens, Get]:

  override def apply(baseUrl: String,
                     matrix: Map[Model, Map[Aspect, Map[Algorithm, Int]]],
                     query: Map[String, String]
  ): Iterable[Either[Any, String]] = `Spring Web*`(baseUrl, matrix, query)
