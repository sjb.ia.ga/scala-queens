package queens
package axis.fourth.webapp
package okhttp

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.Flag
import common.monad.Item

import pojo.moshi.Queens
import pojo.moshi.FromResponseToQueens

import dimensions.fourth.webapp.`Web Client`
import dimensions.fourth.webapp.QueensWebAppInterface
import QueensWebAppInterface.Web
import dimensions.fourth.webapp.`Base.QueensWebAppInterface`

import `OkHttp Web Client`.Get


class `OkHttp Web App`(
  webs: Iterable[Web[Queens, Get]]
)(
  _tag: Any,
  _its: Seq[UUID]
)(
  _fs: Item[`Web Client`] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.QueensWebAppInterface`[
  Queens, Get
](FromResponseToQueens, webs)(_tag, _its)(_fs*):

  override protected type * = `OkHttp Web App`

  override def `#`(t: Any): * =
    new `OkHttp Web App`(webs)(t, _its)(_fs*)

  override protected def `apply*`(s: Item[`Web Client`] => Boolean*): * =
    new `OkHttp Web App`(webs)(_tag, _its)(s*)

  override def toString(): String = "OkHttp " + super.toString


object `OkHttp Web App`:

  def apply(webs: Iterable[Web[Queens, Get]]): `OkHttp Web App` =

    val its = UUID.randomUUID :: Nil

    given Map[UUID, (Boolean, Flag)] = new Map()

    import common.NoTag

    new `OkHttp Web App`(webs)(NoTag, its)()
