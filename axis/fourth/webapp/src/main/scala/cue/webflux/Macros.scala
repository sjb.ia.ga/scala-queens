package queens
package axis.fourth.webapp
package cue.webflux

import scala.quoted.{ Expr, Type, Quotes }

import scala.util.Try


object Macros:

  import scala.collection.JavaConverters.iterableAsScalaIterableConverter

  import _root_.reactor.core.publisher.Flux

  import queens.base.Board

  import queens.base.Macros.`co*Code`

  import dimensions.third.`Queens*`

  import queens.dimensions.fourth.webapp.TagKeyEmptyIterable

  import axis.fourth.webapp.cue.webflux.`WebFlux Web Client`.Get

  import pojo.jackson.FromResponseToQueens

  import pojo.jackson.Queens
  import pojo.jPojo.given

  def coCode(
    tag: Expr[Any],
    responses: Expr[Iterable[(Int, Get, Try[Flux[Queens]], Flux[Queens] => Iterable[`Queens3*`])]],
    cb: Expr[(Queens, Get) => Boolean],
    fun: Expr[(`Queens3*`, Solution) => Unit],
    max: Expr[Long], preemption: Expr[Boolean])
   (using Quotes): Expr[Unit] =
   '{
      val cb2 = { (_: `Queens3*`, _: Solution, _: Long) => true }
      for
        (_, method, response, frtq) <- $responses
        if response.isSuccess
      do
        val tk = frtq(response.get).asInstanceOf[TagKeyEmptyIterable[`Queens3*`]]
        for
          pojo <- response.get.collectList.block.asScala
          if $cb(pojo, method)
        do
          val board: Board = pojo.getBoard
          for
            queens <- FromResponseToQueens(pojo, tk.tag, tk.key)
          do
           ${ `co*Code`[Boolean](tag, 'board, 'queens, 'cb2, fun, max, preemption) }
  }

  inline def co(
    inline tag: Any,
    inline responses: Iterable[(Int, Get, Try[Flux[Queens]], Flux[Queens] => Iterable[`Queens3*`])])
   (inline cb: (Queens, Get) => Boolean = { (_: Queens, _: Get) => true })
   (using inline max: Long, inline preemption: Boolean): Unit =
    ${ coCode('tag, 'responses, 'cb, '{ { (_: `Queens3*`, _: Solution) => } }, 'max, 'preemption) }


  def coCode2(
    tag: Expr[Any],
    responses: Expr[Iterable[(Int, Get, Try[Flux[Queens]], Flux[Queens] => Iterable[`Queens3*`])]],
    cb: Expr[(Queens, Get) => Boolean],
    cb2: Expr[(`Queens3*`, Solution, Long) => Boolean],
    fun: Expr[(`Queens3*`, Solution) => Unit],
    max: Expr[Long], preemption: Expr[Boolean])
   (using Quotes): Expr[Unit] =
   '{
      for
        (_, method, response, frtq) <- $responses
        if response.isSuccess
      do
        val tk = frtq(response.get).asInstanceOf[TagKeyEmptyIterable[`Queens3*`]]
        for
          pojo <- response.get.collectList.block.asScala
          if $cb(pojo, method)
        do
          val board: Board = pojo.getBoard
          for
            queens <- FromResponseToQueens(pojo, tk.tag, tk.key)
          do
           ${ `co*Code`(tag, 'board, 'queens, cb2, fun, max, preemption) }
    }

  inline def `co*`(
    inline tag: Any,
    inline responses: Iterable[(Int, Get, Try[Flux[Queens]], Flux[Queens] => Iterable[`Queens3*`])])
   (inline cb: (Queens, Get) => Boolean = { (_: Queens, _: Get) => true })
   (inline cb2: (`Queens3*`, Solution, Long) => Boolean = { (_: `Queens*`[Point, Boolean], _: Solution, _: Long) => true })
   (inline fun: (`Queens3*`, Solution) => Unit = { (_: `Queens*`[Point, Boolean], _: Solution) => })
   (using inline max: Long, inline preemption: Boolean): Unit =
   ${ coCode2('tag, 'responses, 'cb, 'cb2, 'fun, 'max, 'preemption) }
