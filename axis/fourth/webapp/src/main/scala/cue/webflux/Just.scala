package queens
package axis.fourth.webapp
package cue.webflux

import java.util.UUID

import scala.util.Try

import _root_.reactor.core.publisher.Flux

import base.Board

import common.monad.Grow

import dimensions.third.`Queens*`

import pojo.jackson.Queens

import `WebFlux Web Client`.Get


final case class Just[T](override val board: Board,
                         override val tag: T,
                         override val uuid: UUID,
                         override val number: Long,
                         responses: Iterable[(Int, Get, Try[Flux[Queens]], Flux[Queens] => Iterable[`Queens3*`])],
                         results: (Int, Get, Queens, `Queens3*`)*)
    extends Grow.Just[T]
