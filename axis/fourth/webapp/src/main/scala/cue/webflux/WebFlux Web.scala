package queens
package axis.fourth.webapp
package cue.webflux

import scala.collection.Map
import scala.collection.JavaConverters.mapAsJavaMapConverter

import _root_.reactor.core.publisher.Flux

import dimensions.Dimension.{ Aspect, Algorithm, Model }

import pojo.jackson.{ Queens, Matrix }

import dimensions.fourth.webapp.`Web*`

import `WebFlux Web Client`.Get


object `WebFlux Web*`
    extends `Web*`[Flux[Queens], Get]:

  override protected def apply(scheme: String,
                               host: String,
                               port: Int,
                               infra: String
  ): String =
    if infra.isEmpty
    then
      s"$scheme://$host:$port/queens/cue/solve"
    else
      s"$scheme://$host:$port/queens/cue/solve/$infra"

  override def apply(baseUrl: String,
                     matrix: Map[Model, Map[Aspect, Map[Algorithm, Int]]],
                     _query: Map[String, String]
  ): Iterable[Either[Any, String]] =
    for
      (ml, mat) <- matrix
      (at, mam) <- mat
    yield
      Left {
        val pojo = Matrix()
        pojo.setAspects(Map(at.toString -> Integer(1)).asJava)
        pojo.setAlgorithms(mam.map(_.toString -> Integer(_)).asJava)
        ml -> pojo
      }
