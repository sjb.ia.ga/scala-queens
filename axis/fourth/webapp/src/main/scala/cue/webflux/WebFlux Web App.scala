package queens
package axis.fourth.webapp
package cue.webflux

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import _root_.reactor.core.publisher.Flux

import common.Flag
import common.monad.Item

import pojo.jackson.Queens

import dimensions.fourth.webapp.`Web Client`
import dimensions.fourth.webapp.QueensWebAppInterface
import QueensWebAppInterface.Web
import dimensions.fourth.webapp.`Base.QueensWebAppInterface`

import `WebFlux Web Client`.Get


class `WebFlux Web App`(
  webs: Iterable[Web[Flux[Queens], Get]]
)(
  _tag: Any,
  _its: Seq[UUID]
)(
  _fs: Item[`Web Client`] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.QueensWebAppInterface`[
  Flux[Queens], Get
](FromResponseToQueens, webs)(_tag, _its)(_fs*):

  override protected type * = `WebFlux Web App`

  override def `#`(t: Any): * =
    new `WebFlux Web App`(webs)(t, _its)(_fs*)

  override protected def `apply*`(s: Item[`Web Client`] => Boolean*): * =
    new `WebFlux Web App`(webs)(_tag, _its)(s*)

  override def toString(): String = "WebFlux " + super.toString


object `WebFlux Web App`:

  def apply(webs: Iterable[Web[Flux[Queens], Get]]): `WebFlux Web App` =

    val its = UUID.randomUUID :: Nil

    given Map[UUID, (Boolean, Flag)] = new Map()

    import common.NoTag

    new `WebFlux Web App`(webs)(NoTag, its)()
