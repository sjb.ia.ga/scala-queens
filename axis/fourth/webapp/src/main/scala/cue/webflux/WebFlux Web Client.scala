package queens
package axis.fourth.webapp
package cue.webflux

import java.net.{ URI, URISyntaxException }
import java.util.{ HashMap, Optional }

import scala.util.Try

import org.springframework.http.MediaType
import MediaType.{ APPLICATION_JSON, APPLICATION_NDJSON }

import org.springframework.web.reactive.function.client.WebClient
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.util.{ MultiValueMap, MultiValueMapAdapter }

import reactor.netty.http.client.HttpClient
import io.netty.channel.ChannelOption

import org.springframework.http.HttpStatus

import _root_.reactor.core.publisher.{ Flux, Mono }

import common.pipeline.Context
import common.pipeline.Metadata
import Metadata.{ Boot, Media }

import dimensions.Dimension.Model
import Model.Parallel
import Parallel.Futures

import pojo.jackson.{ Cue2, Queens, Matrix }
import pojo.jPojo.given

import dimensions.fourth.app.Parameter.name
import dimensions.fourth.app.Parameter.Cue.timeout

import dimensions.fourth.webapp.QueensWebAppInterface.Web
import dimensions.fourth.webapp.`Web Client`


class `WebFlux Web Client`(
  override protected val next: Option[Context => `Web Client`] = None
) extends `Web Client`:

  import `WebFlux Web Client`.Get

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(_, web @ Web[Flux[Queens], Get](Left(baseUrl, (model: Model, matrix: Matrix), queries), media, params*), Some(key: String)) =>
        val mediaType = MediaType.valueOf(media)
        val query = new MultiValueMapAdapter[String, String](new HashMap[String, java.util.List[String]]())
        (queries - "timeout").foreach(query.add(_, _))
        try
          val uri = URI(baseUrl)
          val url = uri.getScheme + "://" + uri.getHost + ":" + uri.getPort
          val method = Get((model, matrix, query), (url, uri.getPath), mediaType, params*)
          web(key -> Some(method))
        catch
          case _: URISyntaxException =>
            None

  override protected def >> = {
    case Media(_, Some(it @ Get((model, matrix, query), (root, path), mediaType, params*))) => { _ =>
      it.result = Try {
        (model, mediaType) match
          case (Parallel(Futures), APPLICATION_JSON) =>
            val httpClient = HttpClient
              .create()

            var webClient: WebClient = null

            var url: String = null

            webClient = WebClient.builder()
              .baseUrl(root + path)
              .build()

            // cycleId: Long

            webClient
              .post()
              .uri(_
                     .pathSegment("model", "{model}")
                     .queryParams(query)
                     .build(model)
              )
              .contentType(mediaType)
              .body(Mono.just(matrix), classOf[Matrix])
              .exchangeToMono { response =>
                if response.statusCode == HttpStatus.CREATED
                then
                  url = response.headers.header("Location").get(0)
                  response.bodyToMono(classOf[Void])
                else
                  response.createException().flatMap(Mono.error)
              }
              .block()

            // cues: Flux[Cue2]

            var timeoutOpt = Optional.empty[Int]()

            params.find {
              case timeout(Some(millis)) =>
                timeoutOpt = Optional.of(millis)
                httpClient.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, millis)
                true
              case _ => false
            }

            webClient = WebClient.builder()
              .clientConnector(new ReactorClientHttpConnector(httpClient))
              .baseUrl(root + url)
              .build()

            val cues = webClient
              .get()
              .uri(_
                     .pathSegment("record")
                     .queryParamIfPresent(name[timeout], timeoutOpt)
                     .build()
              )
              .accept(APPLICATION_NDJSON)
              .exchangeToFlux { response =>
                if response.statusCode == HttpStatus.OK
                then
                  response.bodyToFlux(classOf[Cue2])
                else
                  response.createException().flatMap(Mono.error[Cue2](_)).flux()
              }

            // queens: Flux[Queens]

            val response = webClient
              .post()
              .uri(_
                     .pathSegment("replay")
                     .queryParamIfPresent(name[timeout], timeoutOpt)
                     .build()
              )
              .accept(APPLICATION_NDJSON)
              .contentType(APPLICATION_NDJSON)
              .body(cues, classOf[Cue2])
              .retrieve()
              .bodyToFlux(classOf[Queens])

            response

          case _ => ???
      }
      it.result.isSuccess
    }
  }

  override def toString(): String = "WebFlux " + super.toString


object `WebFlux Web Client`:

  import dimensions.fourth.app.Parameter
  import dimensions.fourth.webapp.WebMethod

  case class Get(override protected[webflux] val request: (Model, Matrix, MultiValueMap[String, String]),
                 baseUrl: (String, String),
                 mediaType: MediaType,
                 override val params: Parameter[?]*)
      extends WebMethod[(Model, Matrix, MultiValueMap[String, String]), Flux[Queens], Get]
