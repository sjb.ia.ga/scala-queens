package queens
package axis.fourth.webapp
package cue.webflux

import _root_.reactor.core.publisher.Flux

import queens.dimensions.fourth.webapp.TagKeyEmptyIterable

import queens.pojo.jackson.Queens


object FromResponseToQueens
  extends pojo.FromResponseToQueens[Flux[Queens]]:

  override def apply(_queens: Flux[Queens], tag: Any, key: String): Iterable[`Queens3*`] =
    TagKeyEmptyIterable[`Queens3*`](tag, key);
