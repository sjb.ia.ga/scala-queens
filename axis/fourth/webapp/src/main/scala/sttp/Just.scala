package queens
package axis.fourth.webapp
package sttp

import java.util.UUID

import scala.util.Try

import base.Board

import common.monad.Grow

import dimensions.third.`Queens*`

import pojo.`4s`.Queens

import `sttp Web Client`.get


final case class Just[T](override val board: Board,
                         override val tag: T,
                         override val uuid: UUID,
                         override val number: Long,
                         responses: Iterable[(Int, get, Try[Queens], Queens => Iterable[`Queens3*`])],
                         results: (Int, get, Queens, `Queens3*`)*)
    extends Grow.Just[T]
