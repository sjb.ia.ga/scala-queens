package queens
package axis.fourth.webapp
package sttp

import java.util.LinkedList

import scala.util.{ Try, Failure }
import scala.collection.Map

import _root_.sttp.client3._
import _root_.sttp.model.MediaType
import MediaType.{ ApplicationJson
                 , TextPlain }
import _root_.sttp.model.Uri

import common.pipeline.Context
import common.pipeline.Metadata
import Metadata.{ Boot, Media }

import pojo.`4s`.Queens
import pojo.parser.read.`4s`.toPojo

import dimensions.fourth.app.Parameter
import Parameter.Json.{ circe => Circe, upickle => uPickle }
import Parameter.apply

import dimensions.fourth.webapp.QueensWebAppInterface.Web
import dimensions.fourth.webapp.`Web Client`

class `sttp Web Client`(
  override protected val next: Option[Context => `Web Client`] = None
) extends `Web Client`:

  import `sttp Web Client`._

  override protected def *(slot: Option[Int]): Boolean =
    slot.map(_+1 <= 2).getOrElse(true)

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(Some(slot), web @ Web[Queens, get](Left(baseUrl, matrix, query), media, params*), Some(key: String)) =>
        val _url = s"$baseUrl$matrix;"
        val url = uri"$_url?$query"

        val List(main, sub) = media.split("/").toList

        val mediaType = MediaType
          .safeApply(main, sub)
          .toOption

        val requestTemplate = emptyRequest
          .header("Accept", media)

        var method: Option[get] = None

        mediaType.foreach {

          case ApplicationJson =>

            method = method.orElse {
              params[Circe](slot == 1) {
                import _root_.sttp.client3.circe._
                import io.circe.generic.auto._
                import pojo.parser.`4s`.Circe.given

                val request = requestTemplate
                  .header("User-Agent", "Circe")
                  .get(url)

                get(request.response(asJson[Queens]), params*)
              }
            }

            method = method.orElse {
              params[uPickle](slot == 2) {
                //import _root_.sttp.client3.upicklejson._
                import UpickleApi._
                import pojo.parser.`4s`.uPickle.given

                val request = requestTemplate
                  .header("User-Agent", "uPickle")
                  .get(url)

                get(request.response(asJson[Queens]), params*)
              }
            }

          case _ =>

            method = mediaType.map { _ =>
              val request = requestTemplate
                .get(url)

              get(request.response(asString), params*)
            }

        }

        web(key -> method)


  override protected def >> = {
    case Media(_, Some(it @ get(request, _*))) => { _ =>
      var backend: SttpBackend[Identity, Any] = null
      try
        backend = HttpClientSyncBackend()
        val response = request.send(backend)

        if response.isSuccess
        then
          it.result = Try {
            response.body match
              case Right(it: Queens) => it
              case Right(it: String) => it.toPojo(" ").get
              case Left(it: ResponseException[String, Exception]) => throw it
              case Left(it: String) => throw java.io.IOException(it)
              case _ => ???
          }
        else
          it.result = Failure(java.io.IOException(response.code.toString))
      catch t => it.result = Failure(t)
      finally
        if backend ne null then backend.close()

      it.result.isSuccess
    }
  }

  override def toString(): String = "sttp " + super.toString


object `sttp Web Client`:

  import dimensions.fourth.app.Parameter
  import dimensions.fourth.webapp.WebMethod

  case class get(override protected[sttp] val request: Request[Either[?, ?], Any],
                 override val params: Parameter[?]*)
      extends WebMethod[Request[Either[?, ?], Any], Queens, get]:
    val url = request.uri
    val media = request.headers.find(_.name == "Accept").get.value
    val header = { (it: String) => request.headers.find(_.name == it).map(_.value) }
