package queens
//https://raw.githubusercontent.com/softwaremill/sttp/master/json/upickle/src/main/scala/sttp/client4/upicklejson/SttpUpickleApi.scala
package axis.fourth.webapp
package sttp

import upickle.default.{Reader, Writer, read, write}
import _root_.sttp.client3._
import _root_.sttp.model.MediaType
import _root_.sttp.client3.json._


object UpickleApi {

  implicit def upickleBodySerializer[B](implicit encoder: Writer[B]): BodySerializer[B] =
    b => StringBody(write(b), "utf-8", MediaType.ApplicationJson)

  /** If the response is successful (2xx), tries to deserialize the body from a string into JSON. Returns:
    *   - `Right(b)` if the parsing was successful
    *   - `Left(HttpError(String))` if the response code was other than 2xx (deserialization is not attempted)
    *   - `Left(DeserializationException)` if there's an error during deserialization
    */
  def asJson[B: Reader: IsOption]: ResponseAs[Either[ResponseException[String, Exception], B], Any] =
    asString.mapWithMetadata(ResponseAs.deserializeRightWithError(deserializeJson)).showAsJson

  /** Tries to deserialize the body from a string into JSON, regardless of the response code. Returns:
    *   - `Right(b)` if the parsing was successful
    *   - `Left(DeserializationException)` if there's an error during deserialization
    */
  def asJsonAlways[B: Reader: IsOption]: ResponseAs[Either[DeserializationException[Exception], B], Any] =
    asStringAlways.map(ResponseAs.deserializeWithError(deserializeJson)).showAsJsonAlways

  /** Tries to deserialize the body from a string into JSON, using different deserializers depending on the status code.
    * Returns:
    *   - `Right(B)` if the response was 2xx and parsing was successful
    *   - `Left(HttpError(E))` if the response was other than 2xx and parsing was successful
    *   - `Left(DeserializationException)` if there's an error during deserialization
    */
  def asJsonEither[E: Reader: IsOption, B: Reader: IsOption]
      : ResponseAs[Either[ResponseException[E, Exception], B], Any] = {
    asJson[B].mapLeft {
      case HttpError(e, code) => deserializeJson[E].apply(e).fold(DeserializationException(e, _), HttpError(_, code))
      case de @ DeserializationException(_, _) => de
    }.showAsJsonEither
  }

  def deserializeJson[B: Reader: IsOption]: String => Either[Exception, B] = { (s: String) =>
    try {
      Right(read[B](JsonInput.sanitize[B].apply(s)))
    } catch {
      case e: Exception => Left(e)
      case t: Throwable =>
        // in ScalaJS, ArrayIndexOutOfBoundsException exceptions are wrapped in org.scalajs.linker.runtime.UndefinedBehaviorError
        t.getCause match {
          case e: ArrayIndexOutOfBoundsException => Left(e)
          case _                                 => throw t
        }
    }
  }

  //https://raw.githubusercontent.com/softwaremill/sttp/master/json/common/src/main/scala/sttp/client4/JsonInput.scala
  object JsonInput {
    def sanitize[T: IsOption]: String => String = { s =>
      if implicitly[IsOption[T]].isOption && s.trim.isEmpty then "null" else s
    }
  }

}
