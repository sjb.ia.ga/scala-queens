package queens
package axis.fourth.webapp
package sttp

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.Flag
import common.monad.Item

import pojo.`4s`.Queens
import pojo.`4s`.FromResponseToQueens

import dimensions.fourth.webapp.`Web Client`
import dimensions.fourth.webapp.QueensWebAppInterface
import QueensWebAppInterface.Web
import dimensions.fourth.webapp.`Base.QueensWebAppInterface`

import `sttp Web Client`.get


class `sttp Web App`(
  webs: Iterable[Web[Queens, get]]
)(
  _tag: Any,
  _its: Seq[UUID]
)(
  _fs: Item[`Web Client`] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.QueensWebAppInterface`[
  Queens, get
](FromResponseToQueens, webs)(_tag, _its)(_fs*):

  override protected type * = `sttp Web App`

  override def `#`(t: Any): * =
    new `sttp Web App`(webs)(t, _its)(_fs*)

  override protected def `apply*`(s: Item[`Web Client`] => Boolean*): * =
    new `sttp Web App`(webs)(_tag, _its)(s*)

  override def toString(): String = "sttp " + super.toString


object `sttp Web App`:

  def apply(webs: Iterable[Web[Queens, get]]): `sttp Web App` =

    val its = UUID.randomUUID :: Nil

    given Map[UUID, (Boolean, Flag)] = new Map()

    import common.NoTag

    new `sttp Web App`(webs)(NoTag, its)()
