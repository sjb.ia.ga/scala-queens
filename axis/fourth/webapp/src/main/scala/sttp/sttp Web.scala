package queens
package axis.fourth.webapp
package sttp

import scala.collection.Map

import _root_.sttp.client3._
import _root_.sttp.model.Uri

import dimensions.Dimension.{ Aspect, Algorithm, Model }

import pojo.`4s`.Queens

import dimensions.fourth.webapp.`Web*`

import `sttp Web Client`.get


object `sttp Web*`
    extends `Web*`[Queens, get]:

  override protected def apply(scheme: String,
                               host: String,
                               port: Int,
                               infra: String
  ): String =
    if infra.isEmpty
    then
      uri"$scheme://$host:$port/queens/solve/".toString
    else
      uri"$scheme://$host:$port/queens/solve/$infra/".toString

  override def apply(baseUrl: String,
                     matrix: Map[Model, Map[Aspect, Map[Algorithm, Int]]],
                     query: Map[String, String]
  ): Iterable[Either[Any, String]] =
    for
      (ml, mat) <- matrix
      (at, _mam) <- mat
      mam = _mam.map { case (am, n) => am.toString -> n }.toMap
    yield
      Left( -uri"/model/$ml;" + !uri"/$at=1?$mam" )

  extension(uri: Uri)
    def unary_- : String = uri.toString.substring(1)
    def unary_! : String = (-uri).replace("?", "&").replace("&", ";")
