package queens.axis.fourth.webapp.plainjava.retrofit.api;

import java.util.Map;

import queens.pojo.gson.Queens;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Header;


public interface Queens$Service {

    @GET("model/{matrix}")
    Call<Queens> solve(@Path("matrix")   String matrix,
                       @QueryMap         Map<String, String> query,
                       @Header("Accept") String media);

}
