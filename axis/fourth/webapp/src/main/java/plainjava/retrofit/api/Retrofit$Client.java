package queens.axis.fourth.webapp.plainjava.retrofit.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Retrofit$Client {

    public static Retrofit retrofit(String baseUrl) {
        return new Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    }

    public static Queens$Service $(String baseUrl) {
        return retrofit(baseUrl).create(Queens$Service.class);
    }

}
