package queens.axis.fourth.webapp.plainjava.retrofit;

import scala.Option;
import scala.Function1;
import scala.util.Failure$;
import scala.util.Success$;
import scala.util.Try;
import scala.NotImplementedError;
import scala.collection.Map;
import scala.collection.immutable.Seq;
import static scala.collection.JavaConverters.asJava;

import okhttp3.HttpUrl;
import retrofit2.Call;
import retrofit2.Response;

import queens.common.Pipeline;
import queens.common.pipeline.Metadata;
import queens.common.pipeline.Context;

import queens.pojo.gson.Queens;
import queens.pojo.gson.jPojoGson$;

import queens.dimensions.fourth.app.Parameter;
import queens.dimensions.fourth.webapp.WebMethod;

import queens.dimensions.fourth.webapp.plainjava.Base$PlainJAVA$Web$Client;

import queens.axis.fourth.webapp.plainjava.retrofit.api.Queens$Service;
import queens.axis.fourth.webapp.plainjava.retrofit.api.Retrofit$Client;
import static queens.axis.fourth.webapp.plainjava.retrofit.Retrofit$Web$Client.Get;


public class Retrofit$Web$Client
    extends Base$PlainJAVA$Web$Client<Call<Queens>, Queens, Get> {

    private static jPojoGson$ j$ = jPojoGson$.MODULE$;

    //@Autowired
    private Function1<Context, Pipeline> _next = null;

    @Override
    public Option<Function1<Context, Pipeline>> next() {
        return _next == null ? Option.empty() : Option.apply(_next);
    }

    protected Get set(Option<Map<String, String>> query,
                      String media,
                      Seq<Parameter<?>> params,
                      String... url) {
        String baseUrl = url[0];
        String matrix = url[1];

        Queens$Service service = Retrofit$Client.$(baseUrl);

        Call<Queens> call = service.solve(matrix, asJava(query.get()), media);

        return new Get(call, call.request().url(), media, params);
    }

    protected Try<Queens> get(Get it) {
        try {
            Response<Queens> response = it.request().execute();
            if (!response.isSuccessful())
                throw new java.io.IOException(response.errorBody().string());
            Queens pojo = response.body();
            return Success$.MODULE$.apply(pojo);
        }
        catch (Throwable t) {
            return Failure$.MODULE$.apply(t);
        }
    }


    public static class Get implements WebMethod<Call<Queens>, Queens, Get> {
        private final Call<Queens> request;
        private Try<Queens> result;
        private final HttpUrl url;
        private final String media;
        private final Seq<Parameter<?>> params;

        public Get(Call<Queens> request,
                   HttpUrl url,
                   String media,
                   Seq<Parameter<?>> params) {
            this.request = request;
            this.url = url;
            this.media = media;
            this.params = params;
        }

        public Call<Queens> request() {
            return request;
        }

        public Try<Queens> result() {
            return result;
        }

        public HttpUrl url() {
            return url;
        }

        public String media() {
            return media;
        }

        public Seq<Parameter<?>> params() {
            return params;
        }

        {
            result_$eq(Failure$.MODULE$.apply(new NotImplementedError()));
        }

        public void result_$eq(Try<Queens> result) {
            this.result = result;
        }
    }
}
