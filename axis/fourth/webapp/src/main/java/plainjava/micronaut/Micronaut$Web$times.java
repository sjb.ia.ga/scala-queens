package queens.axis.fourth.webapp.plainjava.micronaut;

import java.net.URI;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Map.Entry;
import static java.util.Collections.singletonMap;

import scala.util.Either;
import scala.util.Left;
import scala.collection.Iterable;
import scala.collection.Map;

import static scala.collection.JavaConverters.asJava;
import static scala.collection.JavaConverters.asScala;

import io.micronaut.http.uri.UriBuilder;

import queens.dimensions.Dimension.Aspect;
import queens.dimensions.Dimension.Algorithm;
import queens.dimensions.Dimension.Model;

import static queens.Util.uri2;

import queens.pojo.jackson.Queens;

import queens.dimensions.fourth.webapp.Web$times;

import static queens.axis.fourth.webapp.plainjava.micronaut.Micronaut$Web$Client.Get;


public final class Micronaut$Web$times
        implements Web$times<Queens, Get> {

    private static volatile Micronaut$Web$times instance = null;

    private Micronaut$Web$times() {}

    synchronized public static Micronaut$Web$times $() {
        if (instance == null)
            instance = new Micronaut$Web$times();

        return instance;
    }

    @Override
    public String apply(String scheme,
                        String host,
                        int port,
                        String infra) {
        UriBuilder builder = UriBuilder
            .of("")
            .scheme(scheme)
            .host(host)
            .port(port)
            .path("queens")
            .path("solve")
            .path(infra);

        return builder.build().toString();
    }

    public Iterable<Either<Object, String>> apply(String baseUrl,
                                                  Map<Model, Map<Aspect, Map<Algorithm, Object>>> matrix,
                                                  Map<String, String> query) {

        LinkedList<Either<Object, String>> urls = new LinkedList<>();

        for (Entry<Model, Map<Aspect, Map<Algorithm, Object>>> ml_mat : asJava(matrix).entrySet()) {
            String ml = ml_mat.getKey().toString();
            Map<Aspect, Map<Algorithm, Object>> mat = ml_mat.getValue();

            for (Entry<String, java.util.Map<String, String>> at_mam : uri2(mat).entrySet()) {
                String at = at_mam.getKey();
                java.util.Map<String, String> mam = at_mam.getValue();

                HashMap<String, Object> exp = new HashMap<>(4);

                exp.put("ml", ml);
                exp.put("at", singletonMap(at, "1"));
                exp.put("mam", mam);

                UriBuilder builder = UriBuilder
                    .of("")
                    .path("{ml}")
                    .path("{;at*}")
                    .path("{;mam*}")
                    .path(";");

                if (!query.isEmpty()) {
                    exp.put("q", asJava(query));
                    builder.path("{?q*}");
                }

                URI uri = builder.expand(exp);

                uri = UriBuilder
                    .of("")
                    .path("model")
                    .path(uri.getPath().replace("/", ""))
                    .build();

                String url = uri.getPath();
                int q = url.indexOf("&");
                if (q >= 0)
                    url = url.substring(0, q)
                        + "?"
                        + url.substring(q+1, url.length());

                urls.add(Left.apply(url));
            }
        }

        return asScala(urls);
    }

}
