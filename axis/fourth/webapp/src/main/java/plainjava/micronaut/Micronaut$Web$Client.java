package queens.axis.fourth.webapp.plainjava.micronaut;

import java.net.URL;
import java.util.LinkedList;
import java.util.HashMap;

import scala.Option;
import scala.Function1;
import scala.Tuple2;
import scala.util.Try;
import scala.util.Failure$;
import scala.util.Success$;
import scala.NotImplementedError;
import scala.collection.Map;
import scala.collection.immutable.Seq;

import static io.micronaut.http.MediaType.APPLICATION_JSON;
import static io.micronaut.http.MediaType.APPLICATION_XML;
import static io.micronaut.http.MediaType.TEXT_PLAIN;

import io.micronaut.http.client.HttpClient;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;

import queens.common.Pipeline;
import queens.common.pipeline.Metadata;
import queens.common.pipeline.Context;

import queens.pojo.jackson.Queens;
import queens.pojo.jackson.jPojoJackson$;

import static queens.Util.fun0;

import queens.dimensions.fourth.app.Parameter;
import queens.dimensions.fourth.webapp.WebMethod;

import queens.dimensions.fourth.webapp.plainjava.Base$PlainJAVA$Web$Client;

import static queens.axis.fourth.webapp.plainjava.micronaut.Micronaut$Web$Client.Get;


public class Micronaut$Web$Client
    extends Base$PlainJAVA$Web$Client<Tuple2<URL, HttpRequest<?>>, Queens, Get> {

    private static jPojoJackson$ j$ = jPojoJackson$.MODULE$;

    private Function1<Context, Pipeline> _next = null;

    @Override
    public Option<Function1<Context, Pipeline>> next() {
        return _next == null ? Option.empty() : Option.apply(_next);
    }

    protected Get set(Option<Map<String, String>> __,
                      String media,
                      Seq<Parameter<?>> params,
                      String... urls) {
        try {
            URL url = new URL(urls[0]);
            HttpRequest<?> request = HttpRequest.GET(urls[0] + urls[1]).accept(media);
            return new Get(Tuple2.apply(url, request), media, params);
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    protected Try<Queens> get(Get it) {
        URL url = it.request._1();
        HttpRequest<?> request = it.request._2();
        try (HttpClient client = HttpClient.create(url)) {
            if (it.media().equals(TEXT_PLAIN)) {
                String queens = client.toBlocking().retrieve(request);

                Option<Queens> pojo = j$.toPojo(queens,
                                                j$.toPojo$default$2(queens),
                                                j$.toPojo$default$3(queens),
                                                j$.toPojo$default$4(queens));
                return Success$.MODULE$.apply(pojo.get());
            } else {
                HttpResponse<Queens> response = client.toBlocking().exchange(request, Queens.class);
                Queens pojo = response.body();
                return Success$.MODULE$.apply(pojo);
            }
        } catch (Throwable t) {
            return Failure$.MODULE$.apply(t);
        }
    }


    public static class Get implements WebMethod<Tuple2<URL, HttpRequest<?>>, Queens, Get> {
        private final Tuple2<URL, HttpRequest<?>> request;
        private Try<Queens> result;
        private final String media;
        private final Seq<Parameter<?>> params;

        public Get(Tuple2<URL, HttpRequest<?>> request,
                   String media,
                   Seq<Parameter<?>> params) {
            this.request = request;
            this.media = media;
            this.params = params;
        }

        public Tuple2<URL, HttpRequest<?>> request() {
            return request;
        }

        public String url() {
            return request._1().toString() + request._2().getUri().toString();
        }

        public Try<Queens> result() {
            return result;
        }

        public String media() {
            return media;
        }

        public Seq<Parameter<?>> params() {
            return params;
        }

        {
            result_$eq(Failure$.MODULE$.apply(new NotImplementedError()));
        }

        public void result_$eq(Try<Queens> result) {
            this.result = result;
        }
    }
}
