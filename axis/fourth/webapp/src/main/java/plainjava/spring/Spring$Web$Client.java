package queens.axis.fourth.webapp.plainjava.spring;

import java.util.LinkedList;
import java.util.HashMap;

import scala.Option;
import scala.Function1;
import scala.Tuple2;
import scala.util.Try;
import scala.util.Failure$;
import scala.util.Success$;
import scala.NotImplementedError;
import scala.collection.Map;
import scala.collection.immutable.Seq;

import org.springframework.web.client.RestClientException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.HttpMethod.GET;

import org.springframework.http.MediaType;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;

import org.springframework.web.client.RestTemplate;

import queens.common.Pipeline;
import queens.common.pipeline.Metadata;
import queens.common.pipeline.Context;

import queens.pojo.jackson.Queens;
import queens.pojo.jackson.jPojoJackson$;

import static queens.Util.fun0;

import queens.dimensions.fourth.app.Parameter;
import queens.dimensions.fourth.webapp.WebMethod;

import queens.dimensions.fourth.webapp.plainjava.Base$PlainJAVA$Web$Client;

import static queens.axis.fourth.webapp.plainjava.spring.Spring$Web$Client.Get;


public class Spring$Web$Client
    extends Base$PlainJAVA$Web$Client<Tuple2<RestTemplate, HttpEntity<?>>, Queens, Get> {

    private static jPojoJackson$ j$ = jPojoJackson$.MODULE$;

    //@Autowired
    private Function1<Context, Pipeline> _next = null;

    @Override
    public Option<Function1<Context, Pipeline>> next() {
        return _next == null ? Option.empty() : Option.apply(_next);
    }

    protected Get set(Option<Map<String, String>> __,
                      String media,
                      Seq<Parameter<?>> params,
                      String... url) {
        RestTemplate restTemplate = new RestTemplate();

        MediaType mediaType = Spring$Web$Client.apply(media);
        LinkedList<HttpMessageConverter<?>> converters = new LinkedList<>();
        converters.add(Spring$Web$Client.apply(mediaType));
        restTemplate.setMessageConverters(converters);

        HttpHeaders requestHeaders = new HttpHeaders();
        LinkedList<MediaType> acceptableMediaTypes = new LinkedList<>();
        acceptableMediaTypes.add(mediaType);
        requestHeaders.setAccept(acceptableMediaTypes);

        HttpEntity<?> requestEntity = new HttpEntity<>(requestHeaders);

        Tuple2<RestTemplate, HttpEntity<?>> request = Tuple2.apply(restTemplate, requestEntity);
        return new Get(request, url[0], media, params);
    }

    protected Try<Queens> get(Get it) {
        try {
            MediaType mediaType = Spring$Web$Client.apply(it.media());

            RestTemplate restTemplate = it.request()._1();
            HttpEntity<?> requestEntity = it.request()._2();

            if (mediaType.equals(TEXT_PLAIN)) {
                ResponseEntity<String> responseEntity = restTemplate.exchange(it.url(), GET, requestEntity, String.class);
                String queens = responseEntity.getBody();
                Option<Queens> pojo = j$.toPojo(queens,
                                                j$.toPojo$default$2(queens),
                                                j$.toPojo$default$3(queens),
                                                j$.toPojo$default$4(queens));
                return Success$.MODULE$.apply(pojo.get());
            } else {
                ResponseEntity<Queens> responseEntity = restTemplate.exchange(it.url(), GET, requestEntity, Queens.class);
                Queens pojo = responseEntity.getBody();
                return Success$.MODULE$.apply(pojo);
            }
        } catch (Throwable t) {
            return Failure$.MODULE$.apply(t);
        }
    }


    public static class Get implements WebMethod<Tuple2<RestTemplate, HttpEntity<?>>, Queens, Get> {
        private final Tuple2<RestTemplate, HttpEntity<?>> request;
        private Try<Queens> result;
        private final String url;
        private final String media;
        private final Seq<Parameter<?>> params;

        public Get(Tuple2<RestTemplate, HttpEntity<?>> request,
                   String url,
                   String media,
                   Seq<Parameter<?>> params) {
            this.request = request;
            this.url = url;
            this.media = media;
            this.params = params;
        }

        public Tuple2<RestTemplate, HttpEntity<?>> request() {
            return request;
        }

        public Try<Queens> result() {
            return result;
        }

        public String url() {
            return url;
        }

        public String media() {
            return media;
        }

        public Seq<Parameter<?>> params() {
            return params;
        }

        {
            result_$eq(Failure$.MODULE$.apply(new NotImplementedError()));
        }

        public void result_$eq(Try<Queens> result) {
            this.result = result;
        }
    }


    private static HashMap<MediaType, HttpMessageConverter<?>> converters = new HashMap<>();

    static {
        converters.put(APPLICATION_JSON, new MappingJackson2HttpMessageConverter());
        converters.put(APPLICATION_XML, new MappingJackson2XmlHttpMessageConverter());
        converters.put(TEXT_PLAIN, new StringHttpMessageConverter());
    }

    private static HttpMessageConverter<?> apply(MediaType mediaType) {
        return converters.get(mediaType);
    }

    private static MediaType apply(String media) {
        return MediaType.valueOf(media);
    }
}
