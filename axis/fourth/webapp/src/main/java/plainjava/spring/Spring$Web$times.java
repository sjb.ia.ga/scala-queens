package queens.axis.fourth.webapp.plainjava.spring;

import java.util.LinkedList;
import java.util.Map.Entry;

import scala.util.Either;
import scala.util.Right;
import scala.collection.Iterable;
import scala.collection.Map;

import static scala.collection.JavaConverters.asJava;
import static scala.collection.JavaConverters.asScala;

import queens.dimensions.Dimension.Aspect;
import queens.dimensions.Dimension.Algorithm;
import queens.dimensions.Dimension.Model;

import queens.pojo.jackson.Queens;

import queens.dimensions.fourth.webapp.Web$times;

import static queens.axis.fourth.webapp.plainjava.spring.Spring$Web$Client.Get;


public final class Spring$Web$times
        implements Web$times<Queens, Get> {

    private static volatile Spring$Web$times instance = null;

    private Spring$Web$times() {}

    synchronized public static Spring$Web$times $() {
        if (instance == null)
            instance = new Spring$Web$times();

        return instance;
    }

    public Iterable<Either<Object, String>> apply(String baseUrl,
                                                  Map<Model, Map<Aspect, Map<Algorithm, Object>>> matrix,
                                                  Map<String, String> __) {
        LinkedList<Either<Object, String>> urls = new LinkedList<>();

        for (Entry<Model, Map<Aspect, Map<Algorithm, Object>>> ml_mat : asJava(matrix).entrySet()) {
            Model ml = ml_mat.getKey();
            Map<Aspect, Map<Algorithm, Object>> mat = ml_mat.getValue();

            for (Entry<Aspect, Map<Algorithm, Object>> at_mam : asJava(mat).entrySet()) {
                Aspect at = at_mam.getKey();
                Map<Algorithm, Object> mam = at_mam.getValue();

                StringBuilder sb = new StringBuilder(baseUrl + "model/" + ml.toString() + ";");
                sb.append(at.toString() + "=1;");

                for (Entry<Algorithm, Object> am_n : asJava(mam).entrySet()) {
                    Algorithm am = am_n.getKey();
                    Integer n = (Integer) am_n.getValue();
                    sb.append(am.toString() + "="+n+";");
                }

                urls.add(Right.apply(sb.toString()));
            }
        }

        return asScala(urls);
    }

}
