package queens.axis.fourth.webapp.plainjava.okhttp;

import scala.util.Either;
import scala.collection.Iterable;
import scala.collection.Map;

import queens.dimensions.Dimension.Aspect;
import queens.dimensions.Dimension.Algorithm;
import queens.dimensions.Dimension.Model;

import queens.pojo.moshi.Queens;

import queens.dimensions.fourth.webapp.Web$times;

import static queens.axis.fourth.webapp.plainjava.okhttp.OkHttp$Web$Client.Get;

import queens.axis.fourth.webapp.plainjava.spring.Spring$Web$times;


public final class OkHttp$Web$times
        implements Web$times<Queens, Get> {

    private static volatile OkHttp$Web$times instance = null;

    private OkHttp$Web$times() {}

    synchronized public static OkHttp$Web$times $() {
        if (instance == null)
            instance = new OkHttp$Web$times();

        return instance;
    }

    public Iterable<Either<Object, String>> apply(String baseUrl,
                                                  Map<Model, Map<Aspect, Map<Algorithm, Object>>> matrix,
                                                  Map<String, String> __) {
        return Spring$Web$times.$().apply(baseUrl, matrix, __);
    }

}
