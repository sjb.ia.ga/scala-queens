package queens.axis.fourth.webapp.plainjava.okhttp;

import java.util.UUID;

import java.util.concurrent.ConcurrentHashMap;

import scala.Function1;
import scala.Function2;
import scala.Function3;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.immutable.Seq;
import scala.runtime.BoxedUnit;

import queens.common.Flag;

import queens.common.monad.Item;
import queens.common.NoTag$;
import queens.common.monad.MonadWithFilter$u0022;
import queens.common.monad.WithFilter$times;

import queens.dimensions.third.Queens$times;

import queens.pojo.moshi.Queens;
import queens.pojo.moshi.FromResponseToQueens$;

import queens.dimensions.fourth.app.QueensAppInterface;
import queens.dimensions.fourth.webapp.QueensWebAppInterface.Web;

import queens.dimensions.fourth.webapp.Base$u002EQueensWebAppInterface;
import queens.dimensions.fourth.webapp.Web$u0020Client;

import static queens.axis.fourth.webapp.plainjava.okhttp.OkHttp$Web$Client.Get;

import static queens.Util.*;


@SuppressWarnings("unchecked")
public class OkHttp$Web$App
    extends Base$u002EQueensWebAppInterface<Queens, Get> {

    private Iterable<Web<Queens, Get>> webs;
    private Object tag;
    private Seq<UUID> its;
    private Seq<Function1<Item<Web$u0020Client>, Object>> fs;
    private ConcurrentHashMap<UUID, Tuple2<Object, Flag>> its$;

    private OkHttp$Web$App(Iterable<Web<Queens, Get>> webs,
                           Object tag,
                           Seq<UUID> its,
                           Seq<Function1<Item<Web$u0020Client>, Object>> fs,
                           ConcurrentHashMap<UUID, Tuple2<Object, Flag>> its$) {
        super(FromResponseToQueens$.MODULE$, webs, tag, its, fs, its$);
        this.webs = webs;
        this.tag = tag;
        this.its = its;
        this.fs = fs;
        this.its$ = its$;
    }

    public static OkHttp$Web$App apply(Iterable<Web<Queens, Get>> webs) {
        Seq<UUID> its = singleton(UUID.randomUUID());
        Seq<Function1<Item<Web$u0020Client>, Object>> fs = empty();

        ConcurrentHashMap<UUID, Tuple2<Object, Flag>> given_Map = new ConcurrentHashMap<>();

        return new OkHttp$Web$App(webs, NoTag$.MODULE$, its, fs, given_Map);
    }

    public Function3<Queens, Object, String, Iterable<Queens$times<Tuple2<Object, Object>, Object>>> queens$dimensions$fourth$webapp$QueensWebAppInterface$$from() {
        return FromResponseToQueens$.MODULE$;
    }

    public QueensAppInterface $hash(Object t) {
        return new OkHttp$Web$App(webs, t, its, fs, its$);
    }

    public WithFilter$times apply$times(Seq<Function1<Item<Web$u0020Client>, Object>> s) {
        return new OkHttp$Web$App(webs, tag, its, s, its$);
    }

    public Seq<Function1<Item<Web$u0020Client>, Object>> fs() {
        return this.fs;
    }

    @Override
    public String toString() {
        return "OkHttp " + super.toString();
    }
}
