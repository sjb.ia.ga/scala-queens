package queens.axis.fourth.webapp.plainjava.okhttp.api;

import java.util.UUID;
import java.util.Date;

import scala.Option;
import scala.NotImplementedError;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter;

import com.squareup.moshi.JsonReader;
import com.squareup.moshi.JsonWriter;

import queens.pojo.moshi.Queens;
import queens.pojo.moshi.jPojoMoshi$;


public class OkHttp$Client {

    private static jPojoMoshi$ j$ = jPojoMoshi$.MODULE$;

    private static OkHttpClient client = new OkHttpClient.Builder()
      .build();

    public static Request $(String url, String media) {
        return new Request.Builder()
            .url(url)
            .header("User-Agent", "OkHttp Queens")
            .addHeader("Accept", media)
            .build();
    }

    public static Call $(Request request) {
        return client.newBuilder()
            .build()
            .newCall(request);
    }

    private static Moshi moshi = new Moshi.Builder()
        .add(Date.class, new Rfc3339DateJsonAdapter())
        .add(UUID.class, new UUIDJsonAdapter())
        .build();

    private static JsonAdapter<Queens> queensJsonAdapter = moshi.adapter(Queens.class);

    public static Queens $(Response response, String media)
            throws java.io.IOException {
        if (media.equals("text/plain")) {
            String queens = response.body().string();
            Option<Queens> pojo = j$.toPojo(queens,
                                            j$.toPojo$default$2(queens),
                                            j$.toPojo$default$3(queens),
                                            j$.toPojo$default$4(queens));
            return pojo.get();
        } else if (media.equals("application/json"))
            return queensJsonAdapter.fromJson(response.body().source());

        throw new NotImplementedError();
    }

    private static class UUIDJsonAdapter extends JsonAdapter<UUID> {
        @Override
        public synchronized UUID fromJson(JsonReader reader)
            throws java.io.IOException {
            if (reader.peek() == null)
                return reader.nextNull();
            else
                return UUID.fromString(reader.nextString());
        }

        @Override
        public synchronized void toJson(JsonWriter writer, UUID value)
            throws java.io.IOException {
            if (value == null)
                writer.nullValue();
            else
                writer.value(value.toString());
        }
    }

}
