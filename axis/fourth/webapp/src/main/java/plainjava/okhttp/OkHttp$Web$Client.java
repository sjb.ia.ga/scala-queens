package queens.axis.fourth.webapp.plainjava.okhttp;

import scala.Option;
import scala.Function1;
import scala.util.Failure$;
import scala.util.Success$;
import scala.util.Try;
import scala.NotImplementedError;
import scala.collection.Map;
import scala.collection.immutable.Seq;
import static scala.collection.JavaConverters.asJava;

import okhttp3.HttpUrl;
import okhttp3.Call;
import okhttp3.Request;
import okhttp3.Response;

import queens.common.Pipeline;
import queens.common.pipeline.Metadata;
import queens.common.pipeline.Context;

import queens.pojo.moshi.Queens;
import queens.pojo.moshi.jPojoMoshi$;

import queens.dimensions.fourth.app.Parameter;
import queens.dimensions.fourth.webapp.WebMethod;

import queens.dimensions.fourth.webapp.plainjava.Base$PlainJAVA$Web$Client;

import queens.axis.fourth.webapp.plainjava.okhttp.api.OkHttp$Client;
import static queens.axis.fourth.webapp.plainjava.okhttp.OkHttp$Web$Client.Get;


public class OkHttp$Web$Client
    extends Base$PlainJAVA$Web$Client<Call, Queens, Get> {

    private static jPojoMoshi$ j$ = jPojoMoshi$.MODULE$;

    //@Autowired
    private Function1<Context, Pipeline> _next = null;

    @Override
    public Option<Function1<Context, Pipeline>> next() {
        return _next == null ? Option.empty() : Option.apply(_next);
    }

    protected Get set(Option<Map<String, String>> __,
                      String media,
                      Seq<Parameter<?>> params,
                      String... url) {
        Request request = OkHttp$Client.$(url[0], media);
        Call call = OkHttp$Client.$(request);
        return new Get(call, call.request().url(), media, params);
    }

    protected Try<Queens> get(Get it) {
        try {
            Response response = it.request().execute();
            if (!response.isSuccessful())
                throw new java.io.IOException("Unexpected code " + response);
            Queens pojo = OkHttp$Client.$(response, it.media());
            return Success$.MODULE$.apply(pojo);
        }
        catch (Throwable t) {
            return Failure$.MODULE$.apply(t);
        }
    }


    public static class Get implements WebMethod<Call, Queens, Get> {
        private final Call request;
        private Try<Queens> result;
        private final HttpUrl url;
        private final String media;
        private final Seq<Parameter<?>> params;

        public Get(Call request,
                   HttpUrl url,
                   String media,
                   Seq<Parameter<?>> params) {
            this.request = request;
            this.url = url;
            this.media = media;
            this.params = params;
        }

        public Call request() {
            return request;
        }

        public Try<Queens> result() {
            return result;
        }

        public HttpUrl url() {
            return url;
        }

        public String media() {
            return media;
        }

        public Seq<Parameter<?>> params() {
            return params;
        }

        {
            result_$eq(Failure$.MODULE$.apply(new NotImplementedError()));
        }

        public void result_$eq(Try<Queens> result) {
            this.result = result;
        }
    }
}
