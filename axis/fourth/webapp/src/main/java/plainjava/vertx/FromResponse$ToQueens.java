package queens.axis.fourth.webapp.plainjava.vertx;

import java.util.LinkedList;

import scala.Function3;
import scala.Tuple2;
import scala.collection.Iterable;

import io.reactivex.rxjava3.core.Single;

import queens.dimensions.third.Queens$times;

import queens.dimensions.fourth.webapp.TagKeyEmptyIterable;

import queens.pojo.jackson.Queens;


public final class FromResponse$ToQueens
    implements Function3<Single<Queens>, Object, String, Iterable<Queens$times<Tuple2<Object, Object>, Object>>> {

    private static volatile FromResponse$ToQueens instance = null;

    private FromResponse$ToQueens() {}

    synchronized public static FromResponse$ToQueens $() {
        if (instance == null)
            instance = new FromResponse$ToQueens();

        return instance;
    }

    @Override
    public Iterable<Queens$times<Tuple2<Object, Object>, Object>> apply(Single<Queens> queens, Object tag, String key) {
        return new TagKeyEmptyIterable<Queens$times<Tuple2<Object, Object>, Object>>(tag, key);
    }
}
