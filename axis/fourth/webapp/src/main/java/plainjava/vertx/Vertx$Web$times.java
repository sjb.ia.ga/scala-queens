package queens.axis.fourth.webapp.plainjava.vertx;

import scala.util.Either;
import scala.collection.Iterable;
import scala.collection.Map;

import io.reactivex.rxjava3.core.Single;

import queens.dimensions.Dimension.Aspect;
import queens.dimensions.Dimension.Algorithm;
import queens.dimensions.Dimension.Model;

import queens.pojo.jackson.Queens;

import queens.dimensions.fourth.webapp.Web$times;

import static queens.axis.fourth.webapp.plainjava.vertx.Vertx$Web$Client.Get;

import queens.axis.fourth.webapp.plainjava.spring.Spring$Web$times;


public final class Vertx$Web$times
        implements Web$times<Single<Queens>, Get> {

    private static volatile Vertx$Web$times instance = null;

    private Vertx$Web$times() {}

    synchronized public static Vertx$Web$times $() {
        if (instance == null)
            instance = new Vertx$Web$times();

        return instance;
    }

    public Iterable<Either<Object, String>> apply(String baseUrl,
                                                  Map<Model, Map<Aspect, Map<Algorithm, Object>>> matrix,
                                                  Map<String, String> __) {
        return Spring$Web$times.$().apply(baseUrl, matrix, __);
    }

}
