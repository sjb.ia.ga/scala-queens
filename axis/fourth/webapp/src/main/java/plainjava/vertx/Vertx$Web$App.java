package queens.axis.fourth.webapp.plainjava.vertx;

import java.util.UUID;

import java.util.concurrent.ConcurrentHashMap;

import scala.Function1;
import scala.Function2;
import scala.Function3;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.immutable.Seq;
import scala.runtime.BoxedUnit;

import io.reactivex.rxjava3.core.Single;

import queens.common.Flag;

import queens.common.monad.Item;
import queens.common.NoTag$;
import queens.common.monad.MonadWithFilter$u0022;
import queens.common.monad.WithFilter$times;

import queens.dimensions.third.Queens$times;

import queens.pojo.jackson.Queens;

import queens.dimensions.fourth.app.QueensAppInterface;
import queens.dimensions.fourth.webapp.QueensWebAppInterface.Web;

import queens.dimensions.fourth.webapp.Base$u002EQueensWebAppInterface;
import queens.dimensions.fourth.webapp.Web$u0020Client;

import static queens.axis.fourth.webapp.plainjava.vertx.Vertx$Web$Client.Get;

import static queens.Util.*;


@SuppressWarnings("unchecked")
public class Vertx$Web$App
    extends Base$u002EQueensWebAppInterface<Single<Queens>, Get> {

    private Iterable<Web<Single<Queens>, Get>> webs;
    private Object tag;
    private Seq<UUID> its;
    private Seq<Function1<Item<Web$u0020Client>, Object>> fs;
    private ConcurrentHashMap<UUID, Tuple2<Object, Flag>> its$;

    private Vertx$Web$App(Iterable<Web<Single<Queens>, Get>> webs,
                          Object tag,
                          Seq<UUID> its,
                          Seq<Function1<Item<Web$u0020Client>, Object>> fs,
                          ConcurrentHashMap<UUID, Tuple2<Object, Flag>> its$) {
        super(FromResponse$ToQueens.$(), webs, tag, its, fs, its$);
        this.webs = webs;
        this.tag = tag;
        this.its = its;
        this.fs = fs;
        this.its$ = its$;
    }

    public static Vertx$Web$App apply(Iterable<Web<Single<Queens>, Get>> webs) {
        Seq<UUID> its = singleton(UUID.randomUUID());
        Seq<Function1<Item<Web$u0020Client>, Object>> fs = empty();

        ConcurrentHashMap<UUID, Tuple2<Object, Flag>> given_Map = new ConcurrentHashMap<>();

        return new Vertx$Web$App(webs, NoTag$.MODULE$, its, fs, given_Map);
    }

    public Function3<Single<Queens>, Object, String, Iterable<Queens$times<Tuple2<Object, Object>, Object>>> queens$dimensions$fourth$webapp$QueensWebAppInterface$$from() {
        return FromResponse$ToQueens.$();
    }

    public QueensAppInterface $hash(Object t) {
        return new Vertx$Web$App(webs, t, its, fs, its$);
    }

    public WithFilter$times apply$times(Seq<Function1<Item<Web$u0020Client>, Object>> s) {
        return new Vertx$Web$App(webs, tag, its, s, its$);
    }

    public Seq<Function1<Item<Web$u0020Client>, Object>> fs() {
        return this.fs;
    }

    @Override
    public String toString() {
        return "Vertx " + super.toString();
    }
}
