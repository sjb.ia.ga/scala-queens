package queens.axis.fourth.webapp.plainjava.vertx;

import java.net.URL;
import java.util.LinkedList;
import java.util.HashMap;

import scala.Option;
import scala.Function1;
import scala.Tuple2;
import scala.util.Try;
import scala.util.Failure$;
import scala.util.Success$;
import scala.NotImplementedError;
import scala.collection.Map;
import scala.collection.immutable.Seq;
import scala.runtime.BoxedUnit;

import io.reactivex.rxjava3.core.Single;

import io.vertx.core.Promise;

import io.vertx.rxjava3.core.AbstractVerticle;
import io.vertx.rxjava3.core.Vertx;
import io.vertx.rxjava3.ext.web.client.WebClient;
import io.vertx.rxjava3.ext.web.client.HttpRequest;
import io.vertx.rxjava3.ext.web.client.HttpResponse;
import io.vertx.rxjava3.ext.web.codec.BodyCodec;
import io.vertx.rxjava3.core.buffer.Buffer;

import queens.common.Pipeline;
import queens.common.pipeline.Metadata;
import queens.common.pipeline.Context;

import queens.pojo.jackson.Queens;
import queens.pojo.jackson.jPojoJackson$;

import static queens.Util.fun0;

import queens.dimensions.fourth.app.Parameter;
import queens.dimensions.fourth.webapp.WebMethod;

import queens.dimensions.fourth.webapp.plainjava.PlainJAVA$Web$Client;

import static queens.axis.fourth.webapp.plainjava.vertx.Vertx$Web$Client.Get;


public class Vertx$Web$Client
    extends AbstractVerticle
    implements PlainJAVA$Web$Client<Tuple2<String, HttpRequest<Buffer>>, Single<Queens>, Get> {

    private static jPojoJackson$ j$ = jPojoJackson$.MODULE$;

    private WebClient client;

    @Override
    public void start(Promise<Void> startPromise) {
        client = WebClient.create(vertx);
        startPromise.complete();
    }

    @Override
    public void stop(Promise<Void> stopPromise) {
        client.close();
        stopPromise.complete();
    }

    @Override
    public Option<Metadata> $less$less(Metadata metadata) {
        return $less$less$(metadata, it -> set(it._1(), it._2(), it._3(), it._4()));
    }

    @Override
    public Function1<Metadata, Function1<BoxedUnit, Object>> $greater$greater() {
        return $greater$greater$(it -> get(it));
    }

    private Function1<Context, Pipeline> _next = null;

    @Override
    public Option<Function1<Context, Pipeline>> next() {
        return _next == null ? Option.empty() : Option.apply(_next);
    }

    private Get set(Option<Map<String, String>> __,
                    String media,
                    Seq<Parameter<?>> params,
                    String... url) {
        try {
            HttpRequest<Buffer> request = client
                .getAbs(url[0])
                .putHeader("Accept", media);
            return new Get(Tuple2.apply(url[0], request), media, params);
        } catch (Throwable t) {
            throw new RuntimeException(t);
        }
    }

    protected Try<Single<Queens>> get(Get it) {
        try {
            String url = it.request._1();
            HttpRequest<Buffer> request = it.request._2();
            if (it.media().equals("text/plain")) {
                Single<Queens> pojo = request
                    .rxSend()
                    .map(HttpResponse::bodyAsString)
                    .map(queens -> j$.toPojo(queens,
                                             j$.toPojo$default$2(queens),
                                             j$.toPojo$default$3(queens),
                                             j$.toPojo$default$4(queens)).get());
                return Success$.MODULE$.apply(pojo);
            } else if (it.media().equals("application/json")) {
                Single<Queens> pojo = request
                    .as(BodyCodec.json(Queens.class))
                    .rxSend()
                    .map(HttpResponse::body);
                return Success$.MODULE$.apply(pojo);
            } else {
                return Failure$.MODULE$.apply(new NotImplementedError());
            }
        } catch (Throwable t) {
            return Failure$.MODULE$.apply(t);
        }
    }


    public static class Get implements WebMethod<Tuple2<String, HttpRequest<Buffer>>, Single<Queens>, Get> {
        private final Tuple2<String, HttpRequest<Buffer>> request;
        private Try<Single<Queens>> result;
        private final String media;
        private final Seq<Parameter<?>> params;

        public Get(Tuple2<String, HttpRequest<Buffer>> request,
                   String media,
                   Seq<Parameter<?>> params) {
            this.request = request;
            this.media = media;
            this.params = params;
        }

        public Tuple2<String, HttpRequest<Buffer>> request() {
            return request;
        }

        public String url() {
            return request._1();
        }

        public Try<Single<Queens>> result() {
            return result;
        }

        public String media() {
            return media;
        }

        public Seq<Parameter<?>> params() {
            return params;
        }

        {
            result_$eq(Failure$.MODULE$.apply(new NotImplementedError()));
        }

        public void result_$eq(Try<Single<Queens>> result) {
            this.result = result;
        }
    }
}
