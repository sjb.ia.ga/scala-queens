package queens.axis.fourth.webapp.cue.plainjava.webflux;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Optional;

import org.springframework.util.MultiValueMap;
import org.springframework.util.MultiValueMapAdapter;

import org.springframework.http.MediaType;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_NDJSON;

import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;

import reactor.netty.http.client.HttpClient;
import io.netty.channel.ChannelOption;

import org.springframework.http.HttpStatus;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import scala.Option;
import scala.Function1;
import scala.Tuple2;
import scala.Tuple3;
import scala.util.Try;
import scala.util.Failure$;
import scala.util.Success$;
import scala.NotImplementedError;
import scala.collection.Map;
import scala.collection.immutable.Seq;
import scala.runtime.BoxedUnit;

import static scala.collection.JavaConverters.asJava;

import queens.common.Pipeline;
import queens.common.pipeline.Metadata;
import queens.common.pipeline.Metadata.Boot;
import queens.common.pipeline.Context;

import queens.dimensions.Dimension.Model;
import static queens.dimensions.Dimension.Model.futures;

import queens.pojo.jackson.Cue2;
import queens.pojo.jackson.Queens;
import queens.pojo.jackson.Matrix;
import queens.pojo.jackson.jPojoJackson$;

import queens.dimensions.fourth.app.Parameter;
import queens.dimensions.fourth.webapp.QueensWebAppInterface.Web;
import queens.dimensions.fourth.webapp.WebMethod;

import queens.dimensions.fourth.webapp.plainjava.PlainJAVA$Web$Client;

import static queens.axis.fourth.webapp.cue.plainjava.webflux.WebFlux$Web$Client.Get;


public class WebFlux$Web$Client
    implements PlainJAVA$Web$Client<Tuple3<Model, Matrix, MultiValueMap<String, String>>, Flux<Queens>, Get> {

    private static jPojoJackson$ j$ = jPojoJackson$.MODULE$;

    @Override
    public Option<Metadata> $less$less(Metadata metadata) {
        if (metadata instanceof Boot boot)
            if (boot.context() instanceof Web<?, ?>) {
                Web<Flux<Queens>, Get> web = (Web<Flux<Queens>, Get>) boot.context();
                String media = web.media();
                Seq<Parameter<?>> params = web.params();

                if (boot.data().nonEmpty()) {
                    String key = (String) boot.data().get();
                    Tuple3<String, Object, Map<String, String>> url = web.url().left().get();
                    String baseUrl = url._1();
                    Tuple2<Model, Matrix> ml_matrix = (Tuple2<Model, Matrix>) url._2();
                    Model model = ml_matrix._1();
                    Matrix matrix = ml_matrix._2();

                    MediaType mediaType = MediaType.valueOf(media);

                    MultiValueMapAdapter<String, String> query = new MultiValueMapAdapter<>(new HashMap<String, List<String>>());
                    for (Entry<String, String> it : asJava(url._3()).entrySet()) {
                        if (it.getKey().equals("timeout"))
                            continue;
                        query.add(it.getKey(), it.getValue());
                    }

                    try {
                        URI uri = new URI(baseUrl);

                        baseUrl = uri.getScheme() + "://" + uri.getHost() + ":" + uri.getPort();

                        Get it = new Get(Tuple3.apply(model, matrix, query), Tuple2.apply(baseUrl, uri.getPath()), mediaType, params);

                        return web.apply(Tuple2.apply(key, Option.apply(it)));
                    } catch (URISyntaxException __) {
                        return Option.empty();
                    }
                } else
                    return Option.empty();
            } else
                return Option.empty();
        else
            return Option.empty();
    }

    @Override
    public Function1<Metadata, Function1<BoxedUnit, Object>> $greater$greater() {
        return $greater$greater$(it -> get(it));
    }

    private Function1<Context, Pipeline> _next = null;

    @Override
    public Option<Function1<Context, Pipeline>> next() {
        return _next == null ? Option.empty() : Option.apply(_next);
    }

    protected Try<Flux<Queens>> get(Get it) {
        try {
            Model model = it.request()._1();
            Matrix matrix = it.request()._2();
            MultiValueMap<String, String> query = it.request()._3();

            String root = it.baseUrl()._1();
            String path = it.baseUrl()._2();

            if (it.mediaType().equals(APPLICATION_JSON)) {
                HttpClient httpClient = HttpClient.create();

                WebClient webClient;

                final String[] url = {null};

                webClient = WebClient.builder()
                    .baseUrl(root + path)
                    .build();

                // cycleId: Long

                webClient
                    .post()
                    .uri(b -> b
                         .pathSegment("model", "{model}")
                         .queryParams(query)
                         .build(model)
                         )
                    .contentType(it.mediaType())
                    .body(Mono.just(matrix), Matrix.class)
                    .exchangeToMono(response -> {
                                        if (response.statusCode().equals(HttpStatus.CREATED)) {
                                            url[0] = response.headers().header("Location").get(0);
                                            return response.bodyToMono(Void.class);
                                        } else
                                            return response.createException().flatMap(Mono::error);
                                    })
                    .block();

                // cues: Flux<Cue2>

                Optional<Integer> timeoutOpt = Optional.of(60000);

                httpClient.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 60000);

                webClient = WebClient.builder()
                    .clientConnector(new ReactorClientHttpConnector(httpClient))
                    .baseUrl(root + url[0])
                    .build();

                Flux<Cue2> cues = webClient
                    .get()
                    .uri(b -> b
                         .pathSegment("record")
                         .queryParamIfPresent("timeout", timeoutOpt)
                         .build()
                         )
                    .accept(APPLICATION_NDJSON)
                    .retrieve()
                    .bodyToFlux(Cue2.class);

                // queens: Flux<Queens>

                Flux<Queens> response = webClient
                    .post()
                    .uri(b -> b
                         .pathSegment("replay")
                         .queryParamIfPresent("timeout", timeoutOpt)
                         .build()
                         )
                    .accept(APPLICATION_NDJSON)
                    .contentType(APPLICATION_NDJSON)
                    .body(cues, Cue2.class)
                    .retrieve()
                    .bodyToFlux(Queens.class);

                return Success$.MODULE$.apply(response);
            } else {
                return Failure$.MODULE$.apply(new NotImplementedError());
            }
        } catch (Throwable t) {
            return Failure$.MODULE$.apply(t);
        }
    }


    public static class Get implements WebMethod<Tuple3<Model, Matrix, MultiValueMap<String, String>>, Flux<Queens>, Get> {
        private final Tuple3<Model, Matrix, MultiValueMap<String, String>> request;
        private final Tuple2<String, String> baseUrl;
        private final MediaType mediaType;
        private final Seq<Parameter<?>> params;
        private Try<Flux<Queens>> result;

        public Get(Tuple3<Model, Matrix, MultiValueMap<String, String>> request,
                   Tuple2<String, String> baseUrl,
                   MediaType mediaType,
                   Seq<Parameter<?>> params) {
            this.request = request;
            this.baseUrl = baseUrl;
            this.mediaType = mediaType;
            this.params = params;
        }

        public Tuple3<Model, Matrix, MultiValueMap<String, String>> request() {
            return request;
        }

        public Tuple2<String, String> baseUrl() {
            return baseUrl;
        }

        public MediaType mediaType() {
            return mediaType;
        }

        public Seq<Parameter<?>> params() {
            return params;
        }

        public Try<Flux<Queens>> result() {
            return result;
        }

        {
            result_$eq(Failure$.MODULE$.apply(new NotImplementedError()));
        }

        public void result_$eq(Try<Flux<Queens>> result) {
            this.result = result;
        }
    }
}
