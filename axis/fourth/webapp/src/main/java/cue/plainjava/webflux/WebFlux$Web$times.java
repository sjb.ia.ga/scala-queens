package queens.axis.fourth.webapp.cue.plainjava.webflux;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import scala.Tuple2;
import scala.util.Either;
import scala.util.Left;
import scala.collection.Iterable;
import scala.collection.Map;

import static scala.collection.JavaConverters.asJava;
import static scala.collection.JavaConverters.asScala;

import reactor.core.publisher.Flux;

import queens.dimensions.Dimension.Aspect;
import queens.dimensions.Dimension.Algorithm;
import queens.dimensions.Dimension.Model;

import queens.pojo.jackson.Queens;
import queens.pojo.jackson.Matrix;

import queens.dimensions.fourth.webapp.Web$times;

import static queens.axis.fourth.webapp.cue.plainjava.webflux.WebFlux$Web$Client.Get;


public final class WebFlux$Web$times
        implements Web$times<Flux<Queens>, Get> {

    private static volatile WebFlux$Web$times instance = null;

    private WebFlux$Web$times() {}

    synchronized public static WebFlux$Web$times $() {
        if (instance == null)
            instance = new WebFlux$Web$times();

        return instance;
    }

    @Override
    public String apply(String scheme,
                        String host,
                        int port,
                        String infra) {
        if (infra == null || infra.isEmpty())
            return scheme + "://" + host + ":" + port + "/queens/cue/solve";
        else
            return scheme + "://" + host + ":" + port + "/queens/cue/solve/" + infra;
    }

    public Iterable<Either<Object, String>> apply(String baseUrl,
                                                  Map<Model, Map<Aspect, Map<Algorithm, Object>>> matrix,
                                                  Map<String, String> __) {
        LinkedList<Either<Object, String>> pojos = new LinkedList<>();

        for (Entry<Model, Map<Aspect, Map<Algorithm, Object>>> ml_mat : asJava(matrix).entrySet()) {
            Model ml = ml_mat.getKey();
            Map<Aspect, Map<Algorithm, Object>> mat = ml_mat.getValue();

            for (Entry<Aspect, Map<Algorithm, Object>> at_mam : asJava(mat).entrySet()) {
                Aspect at = at_mam.getKey();
                Map<Algorithm, Object> mam = at_mam.getValue();

                Matrix pojo = new Matrix();

                HashMap<String, Integer> ats = new HashMap<>(1);
                pojo.setAspects(ats);

                HashMap<String, Integer> ams = new HashMap<>(mam.size());
                pojo.setAlgorithms(ams);

                ats.put(at.toString(), 1);

                for (Entry<Algorithm, Object> am_n : asJava(mam).entrySet()) {
                    Algorithm am = am_n.getKey();
                    Integer n = (Integer) am_n.getValue();
                    ams.put(am.toString(), n);
                }

                pojos.add(Left.apply(Tuple2.apply(ml, pojo)));
            }
        }

        return asScala(pojos);
    }

}
