package queens.axis.fourth.webapp.cue.plainjava.webflux;

import java.util.UUID;

import java.util.concurrent.ConcurrentHashMap;

import scala.Function1;
import scala.Function2;
import scala.Function3;
import scala.Tuple2;
import scala.collection.Iterable;
import scala.collection.immutable.Seq;
import scala.runtime.BoxedUnit;

import reactor.core.publisher.Flux;

import queens.common.Flag;

import queens.common.monad.Item;
import queens.common.NoTag$;
import queens.common.monad.MonadWithFilter$u0022;
import queens.common.monad.WithFilter$times;

import queens.dimensions.third.Queens$times;

import queens.pojo.jackson.Queens;

import queens.dimensions.fourth.app.QueensAppInterface;
import queens.dimensions.fourth.webapp.QueensWebAppInterface.Web;

import queens.dimensions.fourth.webapp.Base$u002EQueensWebAppInterface;
import queens.dimensions.fourth.webapp.Web$u0020Client;

import static queens.axis.fourth.webapp.cue.plainjava.webflux.WebFlux$Web$Client.Get;

import static queens.Util.*;


@SuppressWarnings("unchecked")
public class WebFlux$Web$App
    extends Base$u002EQueensWebAppInterface<Flux<Queens>, Get> {

    private Iterable<Web<Flux<Queens>, Get>> webs;
    private Object tag;
    private Seq<UUID> its;
    private Seq<Function1<Item<Web$u0020Client>, Object>> fs;
    private ConcurrentHashMap<UUID, Tuple2<Object, Flag>> its$;

    private WebFlux$Web$App(Iterable<Web<Flux<Queens>, Get>> webs,
                          Object tag,
                          Seq<UUID> its,
                          Seq<Function1<Item<Web$u0020Client>, Object>> fs,
                          ConcurrentHashMap<UUID, Tuple2<Object, Flag>> its$) {
        super(FromResponse$ToQueens.$(), webs, tag, its, fs, its$);
        this.webs = webs;
        this.tag = tag;
        this.its = its;
        this.fs = fs;
        this.its$ = its$;
    }

    public static WebFlux$Web$App apply(Iterable<Web<Flux<Queens>, Get>> webs) {
        Seq<UUID> its = singleton(UUID.randomUUID());
        Seq<Function1<Item<Web$u0020Client>, Object>> fs = empty();

        ConcurrentHashMap<UUID, Tuple2<Object, Flag>> given_Map = new ConcurrentHashMap<>();

        return new WebFlux$Web$App(webs, NoTag$.MODULE$, its, fs, given_Map);
    }

    public Function3<Flux<Queens>, Object, String, Iterable<Queens$times<Tuple2<Object, Object>, Object>>> queens$dimensions$fourth$webapp$QueensWebAppInterface$$from() {
        return FromResponse$ToQueens.$();
    }

    public QueensAppInterface $hash(Object t) {
        return new WebFlux$Web$App(webs, t, its, fs, its$);
    }

    public WithFilter$times apply$times(Seq<Function1<Item<Web$u0020Client>, Object>> s) {
        return new WebFlux$Web$App(webs, tag, its, s, its$);
    }

    public Seq<Function1<Item<Web$u0020Client>, Object>> fs() {
        return this.fs;
    }

    @Override
    public String toString() {
        return "WebFlux " + super.toString();
    }
}
