package queens.axis.fourth.webapp.cue.plainjava.webflux;

import java.util.UUID;

import scala.Function1;
import scala.Tuple2;
import scala.Tuple4;
import scala.util.Try;
import scala.collection.Iterable;
import scala.collection.immutable.Seq;

import reactor.core.publisher.Flux;

import queens.base.Board;

import queens.dimensions.third.Queens$times;

import queens.pojo.jackson.Queens;

import queens.axis.fourth.webapp.cue.plainjava.webflux.WebFlux$Web$Client.Get;


public final class Just<T>
    implements queens.common.monad.Grow.Just<T> {
    private final Board board;
    private final T tag;
    private final UUID uuid;
    private final long number;
    private final Iterable<Tuple4<Object, Get, Try<Flux<Queens>>, Function1<Flux<Queens>, Iterable<Queens$times<Tuple2<Object, Object>, Object>>>>> responses;
    private final Seq<Tuple4<Object, Get, Queens, Queens$times<Tuple2<Object, Object>, Object>>> results;

    public Just(Board board,
                T tag,
                UUID uuid,
                long number,
                Iterable<Tuple4<Object, Get, Try<Flux<Queens>>, Function1<Flux<Queens>, Iterable<Queens$times<Tuple2<Object, Object>, Object>>>>> responses,
                Seq<Tuple4<Object, Get, Queens, Queens$times<Tuple2<Object, Object>, Object>>> results) {
        this.board = board;
        this.tag = tag;
        this.uuid = uuid;
        this.number = number;
        this.responses = responses;
        this.results = results;
    }

    public Board board() {
        return board;
    }

    public T tag() {
        return tag;
    }

    public UUID uuid() {
        return uuid;
    }

    public long number() {
        return number;
    }

    public Iterable<Tuple4<Object, Get, Try<Flux<Queens>>, Function1<Flux<Queens>, Iterable<Queens$times<Tuple2<Object, Object>, Object>>>>> responses() {
        return responses;
    }

    public Seq<Tuple4<Object, Get, Queens, Queens$times<Tuple2<Object, Object>, Object>>> results() {
        return results;
    }
}
