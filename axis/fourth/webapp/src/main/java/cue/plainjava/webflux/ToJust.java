package queens.axis.fourth.webapp.cue.plainjava.webflux;

import java.util.UUID;

import scala.Function1;
import scala.Tuple2;
import scala.Tuple4;
import scala.Tuple6;
import scala.util.Try;
import scala.collection.Iterable;
import scala.collection.immutable.Seq;

import reactor.core.publisher.Flux;

import queens.base.Board;

import queens.common.monad.Grow.ItemValue;

import queens.dimensions.third.Queens$times;

import queens.pojo.jackson.Queens;

import queens.axis.fourth.webapp.cue.plainjava.webflux.WebFlux$Web$Client.Get;


public class ToJust<T>
    implements queens.common.monad.Grow.ToJust<T, ItemValue<?>> {

    private final Function1<Object, T> given_Tag;

    public ToJust(Function1<Object, T> given_Tag) {
        this.given_Tag = given_Tag;
    }

    @Override
    public Just<T> apply(Tuple6<Board, T, UUID, Object, ItemValue<?>, ItemValue<?>> r) {
        Iterable<Tuple4<Object, Get, Try<Flux<Queens>>, Function1<Flux<Queens>, Iterable<Queens$times<Tuple2<Object, Object>, Object>>>>> responses = (Iterable<Tuple4<Object, Get, Try<Flux<Queens>>, Function1<Flux<Queens>, Iterable<Queens$times<Tuple2<Object, Object>, Object>>>>>) r._5().it();
        Seq<Tuple4<Object, Get, Queens, Queens$times<Tuple2<Object, Object>, Object>>> results = (Seq<Tuple4<Object, Get, Queens, Queens$times<Tuple2<Object, Object>, Object>>>) r._6().it();
        return new Just<T>(r._1(), r._2(), r._3(), (long) r._4(), responses, results);
    }

    public Function1<Object, T> queens$common$monad$Grow$ToJust$$tag() {
        return this.given_Tag;
    }
}
