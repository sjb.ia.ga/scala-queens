package queens
package axis.fifth
package output.buffered

import scala.collection.mutable.{ ListBuffer => MutableList }

import common.pipeline.Context.Empty
import common.pipeline.Metadata
import Metadata.{ Boot, Copy }

import dimensions.fifth.output.QueensOutputUseSolution


abstract trait QueensBufferedOutput
    extends QueensOutputUseSolution:

  protected val buffer: MutableList[Solution] = MutableList()

  def length: Int = buffer.length

  def apply(i: Int): Solution = buffer(i)

  def toList = buffer.toList

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(_, _, _) => Some(Copy(Empty, None))

  override protected val >> = QueensBufferedOutput(buffer)

  override def toString(): String = "Buffered :: " + super.toString


object QueensBufferedOutput:

  private def apply(buffer: MutableList[Solution])
                   (metadata: Metadata)
                   (solution: Solution): Boolean =
    metadata match
      case Copy(Empty, None) =>
        buffer += solution
        true
      case _ =>
        false
