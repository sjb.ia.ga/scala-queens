package queens
package axis.fifth.output
package database

import dimensions.Dimension.{ Aspect, Algorithm, Model }

import version.less.nest.Nest

import dbdimensions.Mappings._


object Mappings {

  abstract class DbEntity {
    val id: Option[Long] = None
  }


  case class DbBoard(size_db: Int
                    ,override val id: Option[Long] = None)
      extends DbEntity


  case class DbSquare(board_db: DbBoard = null
                     ,boardID: Long = -1
                     ,row_db: Int, col_db: Int
                     ,isFree_db: Boolean)
      extends DbEntity


  case class DbQueen(aspect_db: DbAspect
                    ,aspectID: Long
                    ,algorithm_db: DbAlgorithm
                    ,algorithmID: Long
                    ,model_db: DbModel
                    ,modelID: Long
                    ,interface_db: DbInterface
                    ,interfaceID: Long
                    ,use_db: DbUse
                    ,useID: Long
                    ,tags_db: String
                    ,override val id: Option[Long] = None)
      extends DbEntity {

    def nest_mk: Nest =
      Nest(Aspect(aspect_db.aspect).get, Algorithm(algorithm_db.algorithm).get, Model(model_db.model).get)

  }


  case class DbRound(board_db: DbBoard
                    ,boardID: Long
                    ,queen_db: DbQueen
                    ,queenID: Long
                    ,tag_db: String
                    ,number_db: Long
                    ,override val id: Option[Long] = None)
      extends DbEntity


  case class DbSolution(round_db: DbRound = null
                       ,roundID: Long = -1
                       ,number_db: Long
                       ,override val id: Option[Long] = None)
      extends DbEntity


  case class DbSolutionPoint(solution_db: DbSolution = null
                            ,solutionID: Long = -1
                            ,row_db: Int, col_db: Int)
      extends DbEntity {
    override val id: Option[Long] = None
  }


  object Implicits {

    import scala.collection.mutable.{ ListBuffer => MutableList }

    import queens.base.Board

    implicit def s2b(self: Seq[DbSquare]): Board = {
      implicit val seed: Option[Long] = None

      if (self.isEmpty)
        new Board(Nil)
      else {
        val N = self.head.board_db.size_db
        assert(self.size == N * N)

        val squares: List[MutableList[Boolean]] = List.fill(N)(MutableList.fill(N)(true))

        self.foreach { it => squares(it.row_db)(it.col_db) = !it.isFree_db }

        new Board(squares.map(_.toList))
      }
    }

    implicit def s2s(self: Seq[DbSolutionPoint]): Solution = self
      .foldLeft[Solution](Nil){ case (it, DbSolutionPoint(_, _, row, col)) => (row -> col) :: it }

  }

}
