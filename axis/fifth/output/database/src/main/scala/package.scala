package queens
package axis.fifth.output


package object database {

  class SomeId(it: Long) {
    def ? = Some(it)
  }


  object Implicits {

    implicit def long2opt(it: Long): SomeId = new SomeId(it)

    implicit def opt2long(it: Option[Long]): Long = it.getOrElse(0L)

    import scala.collection.Map
    import scala.collection.mutable.{ ListBuffer => MutableList, LinkedHashMap => MutableMap }

    import axis.fifth.output.database.Mappings.{ DbSquare, DbSolution, DbSolutionPoint }

    implicit def rsr2map(
      rs: Seq[DbSquare]
    )(implicit
      r: MutableMap[DbSolution, MutableList[DbSolutionPoint]]
    ): Map[DbSolution, Seq[(DbSquare, Option[DbSolutionPoint])]] = {
      val N = rs.headOption.map(_.board_db.size_db).getOrElse(0)
      val ns: List[Option[DbSolutionPoint]] = List.fill(N*N-N)(None)

      val result = MutableMap[DbSolution, Seq[(DbSquare, Option[DbSolutionPoint])]]()
      r.foreach { case (it, ls) => result(it) = rs zip ls.map(Some(_)) ++ ns }
      result
    }

  }

}
