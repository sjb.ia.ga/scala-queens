package queens
package axis.fifth.output
package database
package dbdimensions

import Implicits._


object Mappings {

  import database.Mappings.DbEntity


  // aspect

  object Classic {

    case class DbLook(look: String
                     ,override val id: Option[Long] = None)
        extends DbEntity

    case class DbClassic(look: DbLook = null
                        ,lookID: Long
                        ,override val id: Option[Long] = None)
        extends DbEntity

  }

  object Stepper {

    case class DbNext(next: String
                     ,override val id: Option[Long] = None)
        extends DbEntity

    case class DbStepper(next: DbNext = null
                        ,nextID: Long
                        ,override val id: Option[Long] = None)
        extends DbEntity

  }

  case class DbClassicAspect(classic: Classic.DbClassic
                            ,override val id: Option[Long] = None)
      extends DbEntity

  case class DbStepperAspect(stepper: Stepper.DbStepper
                            ,override val id: Option[Long] = None)
      extends DbEntity

  case class DbAspect(aspect: String
                     ,classicAspect: Option[DbClassicAspect] = None
                     ,classicID: Option[Long] = None
                     ,stepperAspect: Option[DbStepperAspect] = None
                     ,stepperID: Option[Long] = None
                     ,override val id: Option[Long] = None)
      extends DbEntity


  // algorithm

  object Recursive {

    case class DbNature(nature: String
                       ,override val id: Option[Long] = None)
        extends DbEntity

    case class DbRecursive(nature: DbNature = null
                          ,natureID: Long
                          ,override val id: Option[Long] = None)
        extends DbEntity

  }

  case class DbRecursiveAlgorithm(recursive: Recursive.DbRecursive
                                 ,override val id: Option[Long] = None)
      extends DbEntity

  case class DbAlgorithm(algorithm: String
                        ,recursiveAlgorithm: Option[DbRecursiveAlgorithm] = None
                        ,recursiveID: Option[Long] = None
                        ,override val id: Option[Long] = None
  ) extends DbEntity


  // model

  object Parallel {

    case class DbParadigm(paradigm: String
                         ,override val id: Option[Long] = None)
        extends DbEntity

    case class DbParallel(paradigm: DbParadigm = null
                         ,paradigmID: Long
                         ,override val id: Option[Long] = None)
        extends DbEntity

  }

  case class DbParallelModel(parallel: Parallel.DbParallel
                            ,override val id: Option[Long] = None)
      extends DbEntity

  case class DbModel(model: String
                    ,parallelModel: Option[DbParallelModel] = None
                    ,parallelID: Option[Long] = None
                    ,override val id: Option[Long] = None)
      extends DbEntity


  // interface

  object Program {

    case class DbStyle(style: String
                      ,override val id: Option[Long] = None)
        extends DbEntity

    case class DbProgram(style: DbStyle = null
                        ,styleID: Long
                        ,override val id: Option[Long] = None)
        extends DbEntity

  }

  case class DbProgramInterface(program: Program.DbProgram
                               ,override val id: Option[Long] = None)
      extends DbEntity

  case class DbInterface(interface: String
                        ,programInterface: DbProgramInterface = null
                        ,programID: Long = 0
                        ,override val id: Option[Long] = None)
      extends DbEntity


  // use

  object Output {

    object Database {

      object Slick {

        case class DbSlickProfile(profile: String
                                 ,plainSQL: Boolean
                                 ,override val id: Option[Long] = None)
            extends DbEntity

      }

      object Scalike {

        case class DbScalikeDriver(driver: String
                                  ,plainSQL: Boolean
                                  ,override val id: Option[Long] = None)
            extends DbEntity

      }

      case class DbDatabase(storage: String
                           ,slickProfile: Option[Slick.DbSlickProfile] = None
                           ,slickProfileID: Option[Long] = None
                           ,scalikeDriver: Option[Scalike.DbScalikeDriver] = None
                           ,scalikeDriverID: Option[Long] = None
                           ,override val id: Option[Long] = None)
          extends DbEntity

    }

    case class DbDatabaseKind(database: Database.DbDatabase
                             ,databaseID: Long
                             ,override val id: Option[Long] = None)
        extends DbEntity

    case class DbKind(kind: String
                     ,databaseID: Option[Long] = None
                     ,override val id: Option[Long] = None)
        extends DbEntity

    case class DbDatabaseOutput(databaseKind: DbDatabaseKind
                               ,override val id: Option[Long] = None)
        extends DbEntity

    case class DbOutput(kind: DbKind
                       ,kindID: Long
                       ,override val id: Option[Long] = None)
        extends DbEntity

  }

  case class DbDatabaseOutputUse(databaseOutput: Output.DbDatabaseOutput
                                ,override val id: Option[Long] = None)
      extends DbEntity

  case class DbOutputUse(output: Output.DbOutput
                        ,databaseOutputUse: Option[DbDatabaseOutputUse] = None
                        ,override val id: Option[Long] = None)
      extends DbEntity

  case class DbUse(use: String
                  ,outputUse: DbOutputUse
                  ,outputID: Long
                  ,override val id: Option[Long] = None)
      extends DbEntity

}
