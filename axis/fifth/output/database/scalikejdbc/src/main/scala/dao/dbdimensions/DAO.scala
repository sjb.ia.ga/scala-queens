package queens
package axis.fifth.output
package database.scalikejdbc
package dao
package dbdimensions

import _root_.scalikejdbc._


final class DAO(SQL5: dao.dbdimensions.SQL) {

  import database.scalikejdbc.dbdimensions.X


  // DAO //////////////////////////////////////////////////////////// ASPECTS //

  object Aspects {

    import database.dbdimensions.Mappings.DbAspect

    def mapAspect(
      id: Long
    )(implicit
      session: DBSession
    ): DbAspect = {

      var aspect: Option[DbAspect] = None

      import SQL5.Classic.{ SQLLook, SQLClassic }
      import SQL5.Stepper.{ SQLNext, SQLStepper }
      import SQL5.SQLAspect

      val lk = SQLLook.syntax("lk")
      val cl = SQLClassic.syntax("cl")
      val nx = SQLNext.syntax("nx")
      val st = SQLStepper.syntax("st")
      val at = SQLAspect.syntax("at")

      if (aspect.isEmpty)
        aspect = withSQL {
          select
            .from(SQLAspect as at)
            .join(SQLClassic as cl).on(at.classicID, cl.id)
            .join(SQLLook as lk).on(cl.lookID, lk.id)
            .where.eq(at.id, id).and.eq(at.stepperID, X)
        }
          .map(SQLAspect.Classic(at, cl, lk))
          .list()()
          .headOption

      if (aspect.isEmpty)
        aspect = withSQL {
          select
            .from(SQLAspect as at)
            .join(SQLStepper as st).on(at.stepperID, st.id)
            .join(SQLNext as nx).on(st.nextID, nx.id)
            .where.eq(at.id, id).and.eq(at.classicID, X)
        }
          .map(SQLAspect.Stepper(at, st, nx))
          .list()()
          .headOption

      assert(aspect.isDefined)

      aspect.get
    }

  }


  // DAO ///////////////////////////////////////////////////////// ALGORITHMS //

  object Algorithms {

    import database.dbdimensions.Mappings.DbAlgorithm

    def mapAlgorithm(
      id: Long
    )(implicit
      session: DBSession
    ): DbAlgorithm = {

      var algorithm: Option[DbAlgorithm] = None

      import SQL5.Recursive.{ SQLNature, SQLRecursive }
      import SQL5.SQLAlgorithm

      val nt = SQLNature.syntax("nt")
      val rc = SQLRecursive.syntax("rc")
      val am = SQLAlgorithm.syntax("am")

      if (algorithm.isEmpty)
        algorithm = withSQL {
          select
            .from(SQLAlgorithm as am)
            .where.eq(am.id, id).and.eq(am.recursiveID, X)
        }
          .map(SQLAlgorithm(am))
          .list()()
          .headOption

      if (algorithm.isEmpty)
        algorithm = withSQL {
          select
            .from(SQLAlgorithm as am)
            .join(SQLRecursive as rc).on(am.recursiveID, rc.id)
            .join(SQLNature as nt).on(rc.natureID, nt.id)
            .where.eq(am.id, id).and.not.eq(am.recursiveID, X)
        }
          .map(SQLAlgorithm.Recursive(am, rc, nt))
          .list()()
          .headOption

        assert(algorithm.isDefined)

        algorithm.get
      }

  }


  // DAO ///////////////////////////////////////////////////////////// MODELS //

  object Models {

    import database.dbdimensions.Mappings.DbModel

    def mapModel(
      id: Long
    )(implicit
      session: DBSession
    ): DbModel = {

      var model: Option[DbModel] = None

      import SQL5.Parallel.{ SQLParadigm, SQLParallel }
      import SQL5.SQLModel

      val pd = SQLParadigm.syntax("pd")
      val pl = SQLParallel.syntax("pl")
      val ml = SQLModel.syntax("ml")

      if (model.isEmpty)
        model = withSQL {
          select
            .from(SQLModel as ml)
            .where.eq(ml.id, id).and.eq(ml.parallelID, X)
        }
          .map(SQLModel(ml))
          .list()()
          .headOption

      if (model.isEmpty)
        model = withSQL {
          select
            .from(SQLModel as ml)
            .join(SQLParallel as pl).on(ml.parallelID, pl.id)
            .join(SQLParadigm as pd).on(pl.paradigmID, pd.id)
            .where.eq(ml.id, id).and.not.eq(ml.parallelID, X)
        }
          .map(SQLModel.Parallel(ml, pl, pd))
          .list()()
          .headOption

      assert(model.isDefined)

      model.get
    }

  }


  // DAO ///////////////////////////////////////////////////////// INTERFACES //

  object Interfaces {

    import database.dbdimensions.Mappings.DbInterface

    def mapInterface(
      id: Long
    )(implicit
      session: DBSession
    ): DbInterface = {

      var interface: Option[DbInterface] = None

      import SQL5.Program.{ SQLStyle, SQLProgram }
      import SQL5.SQLInterface

      val st = SQLStyle.syntax("st")
      val pr = SQLProgram.syntax("pr")
      val ie = SQLInterface.syntax("ie")

      if (interface.isEmpty)
        interface = withSQL {
          select
            .from(SQLInterface as ie)
            .join(SQLProgram as pr).on(ie.programID, pr.id)
            .join(SQLStyle as st).on(pr.styleID, st.id)
            .where.eq(ie.id, id).and.not.eq(ie.programID, X)
        }
          .map(SQLInterface(ie, pr, st))
          .list()()
          .headOption

      assert(interface.isDefined)

      interface.get
    }

  }


  // DAO /////////////////////////////////////////////////////////////// USES //

  object Uses {

    import database.dbdimensions.Mappings.DbUse

    def mapUse(
      id: Long
    )(implicit
      session: DBSession
    ): DbUse = {

      var use: Option[DbUse] = None

      import SQL5.Output.Database.{ SQLSlickProfile, SQLScalikeDriver }
      import SQL5.Output.{ SQLKind, SQLDatabase, SQLOutput }
      import SQL5.SQLUse

      val sp = SQLSlickProfile.syntax("sp")
      val sd = SQLScalikeDriver.syntax("sd")
      val db = SQLDatabase.syntax("db")
      val ki = SQLKind.syntax("ki")
      val ot = SQLOutput.syntax("ot")
      val ue = SQLUse.syntax("ue")

      if (use.isEmpty)
        use = withSQL {
          select
            .from(SQLUse as ue)
            .join(SQLOutput as ot).on(ue.outputID, ot.id)
            .join(SQLKind as ki).on(ot.kindID, ki.id)
            .where.eq(ue.id, id).and.eq(ki.databaseID, X)
        }
          .map(SQLUse(ue, ot, ki))
          .list()()
          .headOption

      if (use.isEmpty)
        use = withSQL {
          select
            .from(SQLUse as ue)
            .join(SQLOutput as ot).on(ue.outputID, ot.id)
            .join(SQLKind as ki).on(ot.kindID, ki.id)
            .join(SQLDatabase as db).on(ki.databaseID, db.id)
            .join(SQLSlickProfile as sp).on(db.slickProfileID, sp.id)
            .where.eq(ue.id, id)
            .and.not.eq(ki.databaseID, X)
            .and.not.eq(db.slickProfileID, X)
        }
          .map(SQLUse.Slick(ue, ot, ki, db, sp))
          .list()()
          .headOption

      if (use.isEmpty)
        use = withSQL {
          select
            .from(SQLUse as ue)
            .join(SQLOutput as ot).on(ue.outputID, ot.id)
            .join(SQLKind as ki).on(ot.kindID, ki.id)
            .join(SQLDatabase as db).on(ki.databaseID, db.id)
            .join(SQLScalikeDriver as sd).on(db.scalikeDriverID, sd.id)
            .where.eq(ue.id, id)
            .and.not.eq(ki.databaseID, X)
            .and.not.eq(db.scalikeDriverID, X)
        }
          .map(SQLUse.Scalike(ue, ot, ki, db, sd))
          .list()()
          .headOption

      assert(use.isDefined)

      use.get
    }

  }

}
