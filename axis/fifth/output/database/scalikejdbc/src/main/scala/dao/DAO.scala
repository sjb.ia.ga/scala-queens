package queens
package axis.fifth.output
package database.scalikejdbc
package dao

import scala.collection.Map
import scala.collection.mutable.{ ListBuffer => MutableList, LinkedHashMap => MutableMap }

import _root_.scalikejdbc._

import common.geom.Implicits.`Coord*`

import database.Mappings._

import queens.base.Board

import version.less.nest.Nest

import database.Implicits._


final class DAO(
  SQL5: dao.dbdimensions.SQL,
  SQL6: dao.SQL,
  DAO5: dbdimensions.DAO
) {

  import database.scalikejdbc.dbdimensions.X

  import SQL6._


  // DAO ////////////////////////////////////////////////////////////// BOARD //

  def addBoard(
    board: Board
  )(implicit
    session: DBSession
  ): Long = {

    val bd = SQLBoard.column

    val boardId =
      withSQL {
        insertInto(SQLBoard).namedValues(bd.size_db -> board.N)
      }
        .updateAndReturnGeneratedKey()()
        .toLong

    for {
      row <- 0 until board.N
      col <- 0 until board.N
    } {
      applyUpdate {
        insertInto(SQLSquare).values(boardId, row, col, !board(row)(col))
      }
    }

    boardId

  }

  // not used
  def mapBoard(implicit
    boardId: Long,
    session: DBSession
  ): DbBoard = {

    val bd = SQLBoard.syntax("bd")

    withSQL {
      select
        .from(SQLBoard as bd)
        .where.eq(bd.id, boardId)
    }
      .map(SQLBoard(bd))
      .single()()
      .get

  }


  // DAO ///////////////////////////////////////////////////////////// SQUARE //

  def mapSquares(implicit
    boardId: Long,
    session: DBSession
  ): Seq[DbSquare] = {

    val bd = SQLBoard.syntax("bd")
    val sq = SQLSquare.syntax("sq")

    withSQL {
      select
        .from(SQLSquare as sq)
        .join(SQLBoard as bd).on(sq.boardID, bd.id)
        .where.eq(bd.id, boardId)
        .orderBy(sq.col_db, sq.row_db)
    }
      .map(SQLSquare(sq, bd))
      .list()()

  }

  def deleteSquare(
    it: DbSquare
  )(implicit
    session: DBSession
  ): Unit = {

    val sq = SQLSquare.column

    applyUpdate {
      deleteFrom(SQLSquare)
      .where.eq(sq.boardID, it.boardID)
        .and.eq(sq.row_db, it.row_db)
        .and.eq(sq.col_db, it.col_db)
    }

  }


  // DAO ////////////////////////////////////////////////////////////// QUEEN //

  def addQueens(aspect: String
               ,algorithm: String
               ,model: String
               ,interface: String
               ,use: String
               ,tags: String
              )(implicit
                session: DBSession): Long = {

    import SQL5.SQLAspect
    import SQL5.SQLAlgorithm
    import SQL5.SQLModel
    import SQL5.SQLInterface
    import SQL5.SQLUse

    val qn = SQLQueen.column

    val at = SQLAspect.syntax("at")
    val am = SQLAlgorithm.syntax("am")
    val ml = SQLModel.syntax("ml")
    val ie = SQLInterface.syntax("ie")
    val ue = SQLUse.syntax("ue")

    var aspectId = -1
    var algorithmId = -1
    var modelId = -1
    var interfaceId = -1
    var useId = -1

    withSQL {
      select
        .from(SQLAspect as at)
        .join(SQLAlgorithm as am)
        .join(SQLModel as ml)
        .join(SQLInterface as ie)
        .join(SQLUse as ue)
      .where.eq(at.aspect, aspect)
        .and.eq(am.algorithm, algorithm)
        .and.eq(ml.model, model)
        .and.eq(ie.interface, interface)
        .and.eq(ue.use, use)
    }
      .foreach { rs =>
        aspectId = rs.int("i_on_at")
        algorithmId = rs.int("i_on_am")
        modelId = rs.int("i_on_ml")
        interfaceId = rs.int("i1_on_ie")
        useId = rs.int("i_on_ue")
      }

    withSQL {
      insert
        .into(SQLQueen)
        .namedValues(
          qn.aspectID -> aspectId,
          qn.algorithmID -> algorithmId,
          qn.modelID -> modelId,
          qn.interfaceID -> interfaceId,
          qn.useID -> useId,
          qn.tags_db -> tags
        )
    }
      .updateAndReturnGeneratedKey()()
      .toLong

  }

  def mapQueens(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    session: DBSession
  ): Seq[DbQueen] = {

    import SQL5.SQLAspect
    import SQL5.SQLAlgorithm
    import SQL5.SQLModel
    import SQL5.SQLInterface
    import SQL5.SQLUse

    val qn = SQLQueen.syntax("qn")
    val rn = SQLRound.syntax("rn")

    val at = SQLAspect.syntax("at")
    val am = SQLAlgorithm.syntax("am")
    val ml = SQLModel.syntax("ml")
    val ie = SQLInterface.syntax("ie")
    val ue = SQLUse.syntax("ue")


    implicit val filter: Option[Nest => Option[Nest]] = `nests?`.map { ns => { it => ns.find(it.==) } }
    import DAO.Implicits.Queens._


    def queen_mk(rs: WrappedResultSet) = {
      val aspectId = rs.int("ai1_on_qn")
      val algorithmId = rs.int("ai2_on_qn")
      val modelId = rs.int("mi_on_qn")
      val interfaceId = rs.int("ii_on_qn")
      val useId = rs.int("ui_on_qn")

      val dbAspect = DAO5.Aspects.mapAspect(aspectId)
      val dbAlgorithm = DAO5.Algorithms.mapAlgorithm(algorithmId)
      val dbModel = DAO5.Models.mapModel(modelId)
      val dbInterface = DAO5.Interfaces.mapInterface(interfaceId)
      val dbUse = DAO5.Uses.mapUse(useId)

      SQLQueen(qn
              ,dbAspect
              ,dbAlgorithm
              ,dbModel
              ,dbInterface
              ,dbUse
             )(rs)
    }


    (

      `nests?` match {

        case Some(values) => `rounds?` match {

          case Some(numbers) =>

            withSQL {
              select
                .from(SQLRound as rn)
                .join(SQLQueen as qn).on(rn.queenID, qn.id)
                .join(SQLAspect as at).on(qn.aspectID, at.id)
                .join(SQLAlgorithm as am).on(qn.algorithmID, am.id)
                .join(SQLModel as ml).on(qn.modelID, ml.id)
                .join(SQLInterface as ie).on(qn.interfaceID, ie.id)
                .join(SQLUse as ue).on(qn.useID, ue.id)
              .where.eq(rn.boardID, boardId).and.eq(rn.tag_db, tag)
                .and.in(rn.number_db, numbers.toSeq)
                .and.in(at.aspect, values.map(_.aspect.toString).toSeq)
                .and.in(am.algorithm, values.map(_.algorithm.toString).toSeq)
                .and.in(ml.model, values.map(_.model.toString).toSeq)
                .orderBy(qn.id)
            }

          case _ =>

            withSQL {
              select
                .from(SQLRound as rn)
                .join(SQLQueen as qn).on(rn.queenID, qn.id)
                .join(SQLAspect as at).on(qn.aspectID, at.id)
                .join(SQLAlgorithm as am).on(qn.algorithmID, am.id)
                .join(SQLModel as ml).on(qn.modelID, ml.id)
                .join(SQLInterface as ie).on(qn.interfaceID, ie.id)
                .join(SQLUse as ue).on(qn.useID, ue.id)
              .where.eq(rn.boardID, boardId).and.eq(rn.tag_db, tag)
                .and.in(at.aspect, values.map(_.aspect.toString).toSeq)
                .and.in(am.algorithm, values.map(_.algorithm.toString).toSeq)
                .and.in(ml.model, values.map(_.model.toString).toSeq)
                .orderBy(qn.id)
            }
        }

        case _ => `rounds?` match {

          case Some(numbers) =>

            withSQL {
              select
                .from(SQLRound as rn)
                .join(SQLQueen as qn).on(rn.queenID, qn.id)
                .join(SQLAspect as at).on(qn.aspectID, at.id)
                .join(SQLAlgorithm as am).on(qn.algorithmID, am.id)
                .join(SQLModel as ml).on(qn.modelID, ml.id)
                .join(SQLInterface as ie).on(qn.interfaceID, ie.id)
                .join(SQLUse as ue).on(qn.useID, ue.id)
              .where.eq(rn.boardID, boardId).and.eq(rn.tag_db, tag)
                .and.in(rn.number_db, numbers.toSeq)
                .orderBy(qn.id)
            }

          case _ =>

            withSQL {
              select
                .from(SQLRound as rn)
                .join(SQLQueen as qn).on(rn.queenID, qn.id)
                .join(SQLAspect as at).on(qn.aspectID, at.id)
                .join(SQLAlgorithm as am).on(qn.algorithmID, am.id)
                .join(SQLModel as ml).on(qn.modelID, ml.id)
                .join(SQLInterface as ie).on(qn.interfaceID, ie.id)
                .join(SQLUse as ue).on(qn.useID, ue.id)
              .where.eq(rn.boardID, boardId).and.eq(rn.tag_db, tag)
                .orderBy(qn.id)
            }

        }

      }

    )
      .map(queen_mk)
      .list()()
      .matching
      .toList

  }

  def deleteQueen(
    it: DbQueen
  )(implicit
    session: DBSession
  ): Unit = {

    val qn = SQLQueen.column

    applyUpdate { deleteFrom(SQLQueen).where.eq(qn.id, it.id) }

  }


  // DAO ////////////////////////////////////////////////////////////// ROUND //

  def addRound(
    boardId: Long,
    queenId: Long,
    tag: String,
    number: Long
  )(implicit
    session: DBSession
  ): Long = {

    val rn = SQLRound.column

    withSQL {
      insert
        .into(SQLRound).namedValues(
          rn.boardID -> boardId,
          rn.queenID -> queenId,
          rn.tag_db -> tag,
          rn.number_db -> number
        )
    }
      .updateAndReturnGeneratedKey()()
      .toLong

  }

  def mapRounds(
    `rounds?`: Option[Iterable[Long]] = None
  )(implicit
    boardId: Long,
    tag: String,
    session: DBSession
  ): Seq[DbRound] = {

    import SQL5.SQLAspect
    import SQL5.SQLAlgorithm
    import SQL5.SQLModel
    import SQL5.SQLInterface
    import SQL5.SQLUse

    val rn = SQLRound.syntax("rn")
    val bd = SQLBoard.syntax("bd")

    val qn = SQLQueen.syntax("qn")

    val at = SQLAspect.syntax("at")
    val am = SQLAlgorithm.syntax("am")
    val ml = SQLModel.syntax("ml")
    val ie = SQLInterface.syntax("ie")
    val ue = SQLUse.syntax("ue")


    def queen_mk(rs: WrappedResultSet) = {
      val aspectId = rs.int("ai1_on_qn")
      val algorithmId = rs.int("ai2_on_qn")
      val modelId = rs.int("mi_on_qn")
      val interfaceId = rs.int("ii_on_qn")
      val useId = rs.int("ui_on_qn")

      val dbAspect = DAO5.Aspects.mapAspect(aspectId)
      val dbAlgorithm = DAO5.Algorithms.mapAlgorithm(algorithmId)
      val dbModel = DAO5.Models.mapModel(modelId)
      val dbInterface = DAO5.Interfaces.mapInterface(interfaceId)
      val dbUse = DAO5.Uses.mapUse(useId)

      SQLQueen(qn
              ,dbAspect
              ,dbAlgorithm
              ,dbModel
              ,dbInterface
              ,dbUse
             )(rs)
    }


    (

    `rounds?` match {

      case Some(numbers) =>

        withSQL {
          select
            .from(SQLRound as rn)
            .join(SQLBoard as bd).on(rn.boardID, bd.id)
            .join(SQLQueen as qn).on(rn.queenID, qn.id)
            .join(SQLAspect as at).on(qn.aspectID, at.id)
            .join(SQLAlgorithm as am).on(qn.algorithmID, am.id)
            .join(SQLModel as ml).on(qn.modelID, ml.id)
            .join(SQLInterface as ie).on(qn.interfaceID, ie.id)
            .join(SQLUse as ue).on(qn.useID, ue.id)
          .where.eq(rn.boardID, boardId).and.eq(rn.tag_db, tag)
            .and.in(rn.number_db, numbers.toSeq)
            .orderBy(rn.number_db)
        }

      case _ =>

        withSQL {
          select
            .from(SQLRound as rn)
            .join(SQLBoard as bd).on(rn.boardID, bd.id)
            .join(SQLQueen as qn).on(rn.queenID, qn.id)
            .join(SQLAspect as at).on(qn.aspectID, at.id)
            .join(SQLAlgorithm as am).on(qn.algorithmID, am.id)
            .join(SQLModel as ml).on(qn.modelID, ml.id)
            .join(SQLInterface as ie).on(qn.interfaceID, ie.id)
            .join(SQLUse as ue).on(qn.useID, ue.id)
          .where.eq(rn.boardID, boardId).and.eq(rn.tag_db, tag)
            .orderBy(rn.number_db)
        }

    }

    )
      .map { rs => SQLRound(rn, bd, queen_mk(rs))(rs) }
      .list()()

  }

  def deleteRound(
    it: DbRound
  )(implicit
    session: DBSession
  ): Unit = {

    val rn = SQLRound.column

    applyUpdate { deleteFrom(SQLRound).where.eq(rn.id, it.id) }

  }


  // DAO /////////////////////////////////////////////////////////// SOLUTION //

  def addSolution(
    roundId: Long,
    solution: Solution,
    number: Long
  )(implicit
    session: DBSession
  ): Unit = {

    val sn = SQLSolution.column
    val sp = SQLSolutionPoint.column

    val solutionId =
      withSQL {
        insertInto(SQLSolution).namedValues(
          sn.roundID -> roundId,
          sn.number_db -> number
        )
      }
        .updateAndReturnGeneratedKey()()
        .toLong

    solution
      .map { it =>
        applyUpdate {
          insertInto(SQLSolutionPoint).values(solutionId, it.row, it.col)
        }
      }

  }

  def mapSolutions(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    session: DBSession
  ): Map[DbSolution, Seq[(DbSquare, Option[DbSolutionPoint])]] = {
    implicit val r = MutableMap[DbSolution, MutableList[DbSolutionPoint]]()
    mapSolutionPoints(`nests?`, `rounds?`)
    mapSquares
  }

  private def mapSolutionPoints(
    `nests?`: Option[Iterable[Nest]],
    `rounds?`: Option[Iterable[Long]]
  )(implicit
    boardId: Long,
    tag: String,
    r: MutableMap[DbSolution, MutableList[DbSolutionPoint]],
    session: DBSession
  ): Unit = {

    import SQL5.SQLAspect
    import SQL5.SQLAlgorithm
    import SQL5.SQLModel
    import SQL5.SQLInterface
    import SQL5.SQLUse

    val sn = SQLSolution.syntax("sn")
    val sp = SQLSolutionPoint.syntax("sp")

    val rn = SQLRound.syntax("rn")
    val bd = SQLBoard.syntax("bd")

    val qn = SQLQueen.syntax("qn")

    val at = SQLAspect.syntax("at")
    val am = SQLAlgorithm.syntax("am")
    val ml = SQLModel.syntax("ml")
    val ie = SQLInterface.syntax("ie")
    val ue = SQLUse.syntax("ue")


    implicit val filter: Option[Nest => Option[Nest]] = `nests?`.map { ns => { it => ns.find(it.==) } }
    import DAO.Implicits.Solutions._


    def solution_point_mk(rs: WrappedResultSet) = {
      val aspectId = rs.int("ai1_on_qn")
      val algorithmId = rs.int("ai2_on_qn")
      val modelId = rs.int("mi_on_qn")
      val interfaceId = rs.int("ii_on_qn")
      val useId = rs.int("ui_on_qn")

      val dbAspect = DAO5.Aspects.mapAspect(aspectId)
      val dbAlgorithm = DAO5.Algorithms.mapAlgorithm(algorithmId)
      val dbModel = DAO5.Models.mapModel(modelId)
      val dbInterface = DAO5.Interfaces.mapInterface(interfaceId)
      val dbUse = DAO5.Uses.mapUse(useId)

      val dbQueen = SQLQueen(qn
                            ,dbAspect
                            ,dbAlgorithm
                            ,dbModel
                            ,dbInterface
                            ,dbUse
                           )(rs)

      val dbRound = SQLRound(rn
                            ,bd
                            ,dbQueen
                           )(rs)

      val dbSolution = SQLSolution(sn
                                  ,dbRound
                                 )(rs)

      SQLSolutionPoint(sp
                      ,dbSolution
                     )(rs)
    }


    (

      `nests?` match {

        case Some(values) => `rounds?` match {

          case Some(numbers) =>

            withSQL {
              select
                .from(SQLRound as rn)
                .join(SQLBoard as bd).on(rn.boardID, bd.id)
                .join(SQLQueen as qn).on(rn.queenID, qn.id)
                .join(SQLSolution as sn).on(sn.roundID, rn.id)
                .join(SQLSolutionPoint as sp).on(sp.solutionID, sn.id)
                .join(SQLAspect as at).on(qn.aspectID, at.id)
                .join(SQLAlgorithm as am).on(qn.algorithmID, am.id)
                .join(SQLModel as ml).on(qn.modelID, ml.id)
                .join(SQLInterface as ie).on(qn.interfaceID, ie.id)
                .join(SQLUse as ue).on(qn.useID, ue.id)
              .where.eq(rn.id, boardId).and.eq(rn.tag_db, tag)
                .and.in(rn.number_db, numbers.toSeq)
                .and.in(at.aspect, values.map(_.aspect.toString).toSeq)
                .and.in(am.algorithm, values.map(_.algorithm.toString).toSeq)
                .and.in(ml.model, values.map(_.model.toString).toSeq)
                .orderBy(sp.col_db, sp.row_db, sn.id, rn.number_db)
            }

          case _ =>

            withSQL {
              select
                .from(SQLRound as rn)
                .join(SQLBoard as bd).on(rn.boardID, bd.id)
                .join(SQLQueen as qn).on(rn.queenID, qn.id)
                .join(SQLSolution as sn).on(sn.roundID, rn.id)
                .join(SQLSolutionPoint as sp).on(sp.solutionID, sn.id)
                .join(SQLAspect as at).on(qn.aspectID, at.id)
                .join(SQLAlgorithm as am).on(qn.algorithmID, am.id)
                .join(SQLModel as ml).on(qn.modelID, ml.id)
                .join(SQLInterface as ie).on(qn.interfaceID, ie.id)
                .join(SQLUse as ue).on(qn.useID, ue.id)
              .where.eq(rn.id, boardId).and.eq(rn.tag_db, tag)
                .and.in(at.aspect, values.map(_.aspect.toString).toSeq)
                .and.in(am.algorithm, values.map(_.algorithm.toString).toSeq)
                .and.in(ml.model, values.map(_.model.toString).toSeq)
                .orderBy(sp.col_db, sp.row_db, sn.id, rn.number_db)
            }

        }

        case _ => `rounds?` match {

          case Some(numbers) =>

            withSQL {
              select
                .from(SQLRound as rn)
                .join(SQLBoard as bd).on(rn.boardID, bd.id)
                .join(SQLQueen as qn).on(rn.queenID, qn.id)
                .join(SQLSolution as sn).on(sn.roundID, rn.id)
                .join(SQLSolutionPoint as sp).on(sp.solutionID, sn.id)
                .join(SQLAspect as at).on(qn.aspectID, at.id)
                .join(SQLAlgorithm as am).on(qn.algorithmID, am.id)
                .join(SQLModel as ml).on(qn.modelID, ml.id)
                .join(SQLInterface as ie).on(qn.interfaceID, ie.id)
                .join(SQLUse as ue).on(qn.useID, ue.id)
              .where.eq(rn.id, boardId).and.eq(rn.tag_db, tag)
                .and.in(rn.number_db, numbers.toSeq)
                .orderBy(sp.col_db, sp.row_db, sn.id, rn.number_db)
            }

          case _ =>

            withSQL {
              select
                .from(SQLRound as rn)
                .join(SQLBoard as bd).on(rn.boardID, bd.id)
                .join(SQLQueen as qn).on(rn.queenID, qn.id)
                .join(SQLSolution as sn).on(sn.roundID, rn.id)
                .join(SQLSolutionPoint as sp).on(sp.solutionID, sn.id)
                .join(SQLAspect as at).on(qn.aspectID, at.id)
                .join(SQLAlgorithm as am).on(qn.algorithmID, am.id)
                .join(SQLModel as ml).on(qn.modelID, ml.id)
                .join(SQLInterface as ie).on(qn.interfaceID, ie.id)
                .join(SQLUse as ue).on(qn.useID, ue.id)
              .where.eq(rn.id, boardId).and.eq(rn.tag_db, tag)
                .orderBy(sp.col_db, sp.row_db, sn.id, rn.number_db)
            }

        }

      }

    )
      .map(solution_point_mk)
      .list()()
      .matching
      .appending

  }

  def deleteSolution(
    it: DbSolution
  )(implicit
    session: DBSession
  ): Unit = {

    val sn = SQLSolution.column
    val sp = SQLSolutionPoint.column

    applyUpdate {
      deleteFrom(SQLSolutionPoint).where.eq(sp.solutionID, it.id)
    }
    applyUpdate {
      deleteFrom(SQLSolution).where.eq(sn.id, it.id)
    }

  }

}


object DAO {

  object Implicits {

    object Queens {

      implicit class Matching(it: Seq[DbQueen]) {

        def matching(implicit
                     filter: Option[Nest => Option[Nest]]) = filter match {

          case Some(f) => it
              .map { q =>
                f(q.nest_mk) match {
                  case None => None
                  case _ => Some(q)
                }
              }
              .filter(_.nonEmpty)
              .map(_.get)

          case _ => it

        }

      }

    }

    object Solutions {

      implicit class Matching(it: Seq[DbSolutionPoint]) {

        def matching(implicit filter: Option[Nest => Option[Nest]]) = filter match  {

          case Some(f) => it
              .map { sp =>
                val q = sp.solution_db.round_db.queen_db

                f(q.nest_mk) match {
                  case None => None
                  case _ => Some(sp)
                }
              }
              .filter(_.nonEmpty)
              .map(_.get)

          case _ => it

        }

      }

      implicit class Appending(it: Seq[DbSolutionPoint]) {

        def appending(implicit r: MutableMap[DbSolution, MutableList[DbSolutionPoint]]) = it
          .foreach { sp =>
            val s = sp.solution_db

            if (!r.contains(s))
              r(s) = MutableList()

            r(s) += sp
          }

      }

    }

  }

}
