package queens
package axis.fifth.output
package database.scalikejdbc


package object dao {

  def init(
    driver: Symbol
  ): Unit =
    plainsql.init(driver)


  def drop(
    driver: Symbol
  ): Unit =
    plainsql.drop(driver)

}
