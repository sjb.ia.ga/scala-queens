package queens
package axis.fifth.output
package database.scalikejdbc


package object dbdimensions {

  val X = 0L


  def init(
    driver: Symbol,
    plainSQL: Boolean = false
  ): Unit =
    if (plainSQL)
      plainsql.dbdimensions.init(driver)
    else
      dao.dbdimensions.init(driver)


  def drop(
    driver: Symbol,
    plainSQL: Boolean = false
  ): Unit =
    if (plainSQL)
      plainsql.dbdimensions.drop(driver)
    else
      dao.dbdimensions.drop(driver)

}


package plainsql {

  import _root_.scalikejdbc._


  package object dbdimensions {

    val X = 0


    def init(
      driver: Symbol,
      create: Boolean = true,
      populate: Boolean = true
    ): Unit = {


      def init_aspects: Unit = {

        if (create) {

          NamedDB(driver) autoCommit { implicit session =>

            sql"""create table "LOOKS"(
                    "ID" int generated by default as identity,
                    "LOOK" varchar,
                    primary key ("ID")
                  )
               """.execute()()
            sql"""create table "CLASSICS"(
                    "ID" int generated by default as identity,
                    "LOOK_ID" int,
                    primary key ("ID"),
                    constraint "LOOK_FK" foreign key ("LOOK_ID") references "LOOKS"("ID")
                  )
               """.execute()()
            sql"""create table "NEXTS"(
                    "ID" int generated by default as identity,
                    "NEXT" varchar,
                    primary key ("ID")
                  )
               """.execute()()
            sql"""create table "STEPPERS"(
                    "ID" int generated by default as identity,
                    "NEXT_ID" int,
                    primary key ("ID"),
                    constraint "NEXT_FK" foreign key ("NEXT_ID") references "NEXTS"("ID")
                  )
               """.execute()()
            sql"""create table "ASPECTS"(
                    "ID" int generated by default as identity,
                    "ASPECT" varchar,
                    "CLASSIC_ID" int,
                    "STEPPER_ID" int,
                    primary key ("ID"),
                    constraint "CLASSIC_FK" foreign key ("CLASSIC_ID") references "CLASSICS"("ID"),
                    constraint "STEPPER_FK" foreign key ("STEPPER_ID") references "STEPPERS"("ID")
                  )
               """.execute()()

          }

        }

        if (!populate) return

        import dimensions.Dimension.Aspect.Classic.{ Straight, Callback }
        import dimensions.Dimension.Aspect.Stepper.{ OneIncrement, Permutations }
        import dimensions.Dimension.Aspect.{ classic$, stepper$ }

        val obls = List(Straight, Callback).map(_.toString)
        val obns = List(OneIncrement, Permutations).map(_.toString)

        NamedDB(driver) localTx { implicit session =>

          sql"""insert into "LOOKS"("ID", "LOOK") values ($X, '')""".update()()
          sql"""insert into "CLASSICS"("ID", "LOOK_ID") values ($X, $X)""".update()()
          sql"""insert into "NEXTS"("ID", "NEXT") values ($X, '')""".update()()
          sql"""insert into "STEPPERS"("ID", "NEXT_ID") values ($X, $X)""".update()()

          obls.foreach { it =>
            sql"""insert into "LOOKS"("LOOK") values ($it)""".update()()
          }

          obns.map { it =>
            sql"""insert into "NEXTS"("NEXT") values ($it)""".update()()
          }

          sql"""select "ID" from "LOOKS" where not "ID" = $X"""
            .map(rs => rs.int("ID"))
            .list()()
            .foreach { it =>
              sql"""insert into "CLASSICS"("LOOK_ID") values ($it)""".update()()
            }

          sql"""select "ID" from "NEXTS" where not "ID" = $X"""
            .map(rs => rs.int("ID"))
            .list()()
            .foreach { it =>
              sql"""insert into "STEPPERS"("NEXT_ID") values ($it)""".update()()
            }

          {
            val ls = sql"""select l."LOOK", c."ID"
                           from "CLASSICS" c
                           join "LOOKS" l
                           on c."LOOK_ID" = l."ID"
                           where not c."ID" = $X
                        """
              .map(rs => (rs.string("LOOK"), rs.int("ID")))
              .list()()
              .toSeq

            val ns = sql"""select n."NEXT", s."ID"
                           from "STEPPERS" s
                           join "NEXTS" n
                           on s."NEXT_ID" = n."ID"
                           where not s."ID" = $X
                        """
              .map(rs => (rs.string("NEXT"), rs.int("ID")))
              .list()()
              .toSeq

            val params =
              ls.map { it =>
                Seq(
                  'ASPECT -> classic$(it._1),
                  'CLASSIC_ID -> it._2,
                  'STEPPER_ID -> X
                )
              } ++
              ns.map { it =>
                Seq(
                  'ASPECT -> stepper$(it._1),
                  'STEPPER_ID -> it._2,
                  'CLASSIC_ID -> X
                )
              }

            sql"""insert into "ASPECTS"("ASPECT", "CLASSIC_ID", "STEPPER_ID")
                  values ({ASPECT}, {CLASSIC_ID}, {STEPPER_ID})
               """
              .batchByName(params: _*)
              .apply[List]()

          }

        }

      }


      def init_algorithms: Unit = {

        if (create) {

          NamedDB(driver) autoCommit { implicit session =>

            sql"""create table "NATURES"(
                    "ID" int generated by default as identity,
                    "NATURE" varchar,
                    primary key ("ID")
                  )
               """.execute()()
            sql"""create table "RECURSIVES"(
                    "ID" int generated by default as identity,
                    "NATURE_ID" int,
                    primary key ("ID"),
                    constraint "NATURE_FK" foreign key ("NATURE_ID") references "NATURES"("ID")
                  )
               """.execute()()
            sql"""create table "ALGORITHMS"(
                    "ID" int generated by default as identity,
                    "ALGORITHM" varchar,
                    "RECURSIVE_ID" int,
                    primary key ("ID"),
                    constraint "RECURSIVE_FK" foreign key ("RECURSIVE_ID") references "RECURSIVES"("ID")
                  )
               """.execute()()

          }

        }

        if (!populate) return

        import dimensions.Dimension.Algorithm.{ Iterative, iterative } // FIXME: Iterative
        import dimensions.Dimension.Algorithm.Recursive.{ Native, Extern }
        import dimensions.Dimension.Algorithm.{ recursive$ }

        val obrs = List(Native, Extern).map(_.toString)
        val obas = List(iterative).map(_.toString) // FIXME: Iterative

        NamedDB(driver) localTx { implicit session =>

          sql"""insert into "NATURES"("ID", "NATURE") values ($X, '')""".update()()
          sql"""insert into "RECURSIVES"("ID", "NATURE_ID") values ($X, $X)""".update()()

          obrs.foreach { it =>
            sql"""insert into "NATURES"("NATURE") values ($it)""".update()()
          }

          obas.map { it =>
            sql"""insert into "ALGORITHMS"("ALGORITHM", "RECURSIVE_ID") values ($it, $X)""".update()()
          }

          sql"""select "ID" from "NATURES" where not "ID" = $X"""
            .map(rs => rs.int("ID"))
            .list()()
            .foreach { it =>
              sql"""insert into "RECURSIVES"("NATURE_ID") values ($it)""".update()()
            }

          {
            val ls = sql"""select n."NATURE", r."ID"
                           from "RECURSIVES" r
                           join "NATURES" n
                           on r."NATURE_ID" = n."ID"
                           where not r."ID" = $X
                        """
              .map(rs => (rs.string("NATURE"), rs.int("ID")))
              .list()()
              .toSeq

            val params =
              ls.map { it =>
                Seq(
                  'ALGORITHM -> recursive$(it._1),
                  'RECURSIVE_ID -> it._2
                )
              }

            sql"""insert into "ALGORITHMS"("ALGORITHM", "RECURSIVE_ID")
                  values ({ALGORITHM}, {RECURSIVE_ID})
               """
              .batchByName(params: _*)
              .apply[List]()

          }

        }

      }


      def init_models: Unit = {

        if (create) {

          NamedDB(driver) autoCommit { implicit session =>

            sql"""create table "PARADIGMS"(
                    "ID" int generated by default as identity,
                    "PARADIGM" varchar,
                    primary key ("ID")
                  )
               """.execute()()
            sql"""create table "PARALLELS"(
                    "ID" int generated by default as identity,
                    "PARADIGM_ID" int,
                    primary key ("ID"),
                    constraint "PARADIGM_FK" foreign key ("PARADIGM_ID") references "PARADIGMS"("ID")
                  )
               """.execute()()
            sql"""create table "MODELS"(
                    "ID" int generated by default as identity,
                    "MODEL" varchar,
                    "PARALLEL_ID" int,
                    primary key ("ID"),
                    constraint "PARALLEL_FK" foreign key ("PARALLEL_ID") references "PARALLELS"("ID")
                  )
               """.execute()()
          }

        }

        if (!populate) return

        import dimensions.Dimension.Model.{ Flow, flow } // FIXME: Flow
        import dimensions.Dimension.Model.Parallel.{ Actors, Futures }
        import dimensions.Dimension.Model.{ parallel$ }

        val obps = List(Actors, Futures).map(_.toString)
        val obms = List(flow).map(_.toString) // FIXME: Flow

        NamedDB(driver) localTx { implicit session =>

          sql"""insert into "PARADIGMS"("ID", "PARADIGM") values ($X, '')""".update()()
          sql"""insert into "PARALLELS"("ID", "PARADIGM_ID") values ($X, $X)""".update()()

          obps.foreach { it =>
            sql"""insert into "PARADIGMS"("PARADIGM") values ($it)""".update()()
          }

          obms.foreach { it =>
            sql"""insert into "MODELS"("MODEL", "PARALLEL_ID") values ($it, $X)""".update()()
          }

          sql"""select "ID" from "PARADIGMS" where not "ID" = $X"""
            .map(rs => rs.int("ID"))
            .list()()
            .foreach { it =>
              sql"""insert into "PARALLELS"("PARADIGM_ID") values ($it)""".update()()
            }

          {
            val ls = sql"""select pp."PARADIGM", p."ID"
                           from "PARALLELS" p
                           join "PARADIGMS" pp
                           on p."PARADIGM_ID" = pp."ID"
                           where not p."ID" = $X
                        """
              .map(rs => (rs.string("PARADIGM"), rs.int("ID")))
              .list()()
              .toSeq

            val params =
              ls.map { it =>
                Seq(
                  'MODEL -> parallel$(it._1),
                  'PARALLEL_ID -> it._2
                )
              }

            sql"""insert into "MODELS"("MODEL", "PARALLEL_ID")
                  values ({MODEL}, {PARALLEL_ID})
               """
              .batchByName(params: _*)
              .apply[List]()

          }

        }

      }


      def init_interfaces: Unit = {

        if (create) {

          NamedDB(driver) autoCommit { implicit session =>

            sql"""create table "STYLES"(
                    "ID" int generated by default as identity,
                    "STYLE" varchar,
                    primary key ("ID")
                  )
               """.execute()()
            sql"""create table "PROGRAMS"(
                    "ID" int generated by default as identity,
                    "STYLE_ID" int,
                    primary key ("ID"),
                    constraint "STYLE_FK" foreign key ("STYLE_ID") references "STYLES"("ID")
                  )
               """.execute()()
            sql"""create table "INTERFACES"(
                    "ID" int generated by default as identity,
                    "INTERFACE" varchar,
                    "PROGRAM_ID" int,
                    primary key ("ID"),
                    constraint "PROGRAM_FK" foreign key ("PROGRAM_ID") references "PROGRAMS"("ID")
                  )
               """.execute()()

          }

        }

        if (!populate) return

        import dimensions.Dimension.Interface.Program.{ Monad, Simple }
        import dimensions.Dimension.Interface.{ program$ }

        import database.dbdimensions.Mappings.Program._

        val obss = List("", Monad, Simple).map(_.toString).tail

        NamedDB(driver) localTx { implicit session =>

          obss.foreach { it =>
            sql"""insert into "STYLES"("STYLE") values ($it)""".update()()
          }

          sql"""select "ID" from "STYLES""""
            .map(rs => rs.int("ID"))
            .list()()
            .foreach { it =>
              sql"""insert into "PROGRAMS"("STYLE_ID") values ($it)""".update()()
            }

          {
            val ls = sql"""select s."STYLE", p."ID"
                           from "PROGRAMS" p
                           join "STYLES" s
                           on p."STYLE_ID" = s."ID"
                        """
              .map(rs => (rs.string("STYLE"), rs.int("ID")))
              .list()()
              .toSeq

            val params =
              ls.map { it =>
                Seq(
                  'INTERFACE -> program$(it._1),
                  'PROGRAM_ID -> it._2
                )
              }

            sql"""insert into "INTERFACES"("INTERFACE", "PROGRAM_ID")
                  values ({INTERFACE}, {PROGRAM_ID})
               """
              .batchByName(params: _*)
              .apply[List]()

          }

        }

      }


      def init_uses: Unit = {

        if (create) {

          NamedDB(driver) autoCommit { implicit session =>

            sql"""create table "SLICK_PROFILES"(
                    "ID" int generated by default as identity,
                    "PROFILE" varchar,
                    "PLAIN_SQL" bool,
                    primary key ("ID")
                  )
               """.execute()()
            sql"""create table "SCALIKE_DRIVERS"(
                    "ID" int generated by default as identity,
                    "DRIVER" varchar,
                    "PLAIN_SQL" bool,
                    primary key ("ID")
                  )
               """.execute()()
            sql"""create table "DATABASES"(
                    "ID" int generated by default as identity,
                    "STORAGE" varchar,
                    "SLICK_PROFILE_ID" int,
                    "SCALIKE_DRIVER_ID" int,
                    primary key ("ID"),
                    constraint "SLICK_PROFILE_FK" foreign key ("SLICK_PROFILE_ID") references "SLICK_PROFILES"("ID"),
                    constraint "SCALIKE_DRIVER_FK" foreign key ("SCALIKE_DRIVER_ID") references "SCALIKE_DRIVERS"("ID")
                  )
               """.execute()()
            sql"""create table "KINDS"(
                    "ID" int generated by default as identity,
                    "KIND" varchar,
                    "DATABASE_ID" int,
                    primary key ("ID"),
                    constraint "DATABASE_FK" foreign key ("DATABASE_ID") references "DATABASES"("ID")
                  )
               """.execute()()
            sql"""create table "OUTPUTS"(
                    "ID" int generated by default as identity,
                    "KIND_ID" int,
                    primary key ("ID"),
                    constraint "KIND_FK" foreign key ("KIND_ID") references "KINDS"("ID")
                  )
               """.execute()()
            sql"""create table "USES"(
                    "ID" int generated by default as identity,
                    "USE" varchar,
                    "OUTPUT_ID" int,
                    primary key ("ID"),
                    constraint "OUTPUT_FK" foreign key ("OUTPUT_ID") references "OUTPUTS"("ID")
                  )
               """.execute()()

          }

        }

        if (!populate) return

        import dimensions.Dimension.Use.Output.{ Buffered, Console }
        import dimensions.Dimension.Use.Output.Database.Slick
        import dimensions.Dimension.Use.Output.Database.Scalike
        import dimensions.Dimension.Use.{ output$ }
        import dimensions.Dimension.Use.Output.{ database$ }
        import dimensions.Dimension.Use.Output.Database.{ slick$, scalike$ }

        val obps = List("", Slick.Profile.H2, Slick.Profile.SQLite).map(_.toString).tail
        val obds = List("", Scalike.Driver.H2, Scalike.Driver.SQLite).map(_.toString).tail
        val obks = List("", Buffered, Console).map(_.toString).tail

        NamedDB(driver) localTx { implicit session =>

          sql"""insert into "SLICK_PROFILES"("ID", "PROFILE", "PLAIN_SQL") values ($X, '', false)""".update()()
          sql"""insert into "SCALIKE_DRIVERS"("ID", "DRIVER", "PLAIN_SQL") values ($X, '', false)""".update()()
          sql"""insert into "DATABASES"("ID", "STORAGE", "SLICK_PROFILE_ID", "SCALIKE_DRIVER_ID") values ($X, '', $X, $X)""".update()()

          (obps.flatMap(List.fill(2)(_) zip List(false, true))).foreach { case (it, plainSQL) =>
            sql"""insert into "SLICK_PROFILES"("PROFILE", "PLAIN_SQL") values ($it, $plainSQL)""".update()()
          }

          (obds.flatMap(List.fill(2)(_) zip List(false, true))).foreach { case (it, plainSQL) =>
            sql"""insert into "SCALIKE_DRIVERS"("DRIVER", "PLAIN_SQL") values ($it, $plainSQL)""".update()()
          }

          obks.foreach { it =>
            sql"""insert into "KINDS"("KIND", "DATABASE_ID") values ($it, $X)""".update()()
          }

          {
            val ps = sql"""select * from "SLICK_PROFILES"
                           where not "ID" = $X
                        """
              .map(rs => (rs.int("ID"), rs.string("PROFILE"), rs.boolean("PLAIN_SQL")))
              .list()()
              .toSeq

            val ds = sql"""select * from "SCALIKE_DRIVERS"
                           where not "ID" = $X
                        """
              .map(rs => (rs.int("ID"), rs.string("DRIVER"), rs.boolean("PLAIN_SQL")))
              .list()()
              .toSeq

            val params =
              ps.map { it =>
                Seq(
                  'STORAGE -> slick$(it._2, it._3),
                  'SLICK_PROFILE_ID -> it._1,
                  'SCALIKE_DRIVER_ID -> X
                )
              } ++
              ds.map { it =>
                Seq(
                  'STORAGE -> scalike$(it._2, it._3),
                  'SLICK_PROFILE_ID -> X,
                  'SCALIKE_DRIVER_ID -> it._1
                )
              }

            sql"""insert into "DATABASES"("STORAGE", "SLICK_PROFILE_ID", "SCALIKE_DRIVER_ID")
                  values ({STORAGE}, {SLICK_PROFILE_ID}, {SCALIKE_DRIVER_ID})
               """
              .batchByName(params: _*)
              .apply[List]()

          }

          sql"""select "STORAGE", "ID" from "DATABASES" where not "ID" = $X"""
            .map(rs => (rs.string("STORAGE"), rs.int("ID")))
            .list()()
            .foreach { case (it, did) =>
              sql"""insert into "KINDS"("KIND", "DATABASE_ID") values (${database$(it)}, $did)""".update()()
            }

          sql"""select "ID" from "KINDS""""
            .map(rs => rs.int("ID"))
            .list()()
            .foreach { it =>
              sql"""insert into "OUTPUTS"("KIND_ID") values ($it)""".update()()
            }

          {
            val ls = sql"""select k."KIND", o."ID"
                           from "OUTPUTS" o
                           join "KINDS" k
                           on o."KIND_ID" = k."ID"
                        """
              .map(rs => (rs.string("KIND"), rs.int("ID")))
              .list()()
              .toSeq

            val params =
              ls.map { it =>
                Seq(
                  'USE -> output$(it._1),
                  'OUTPUT_ID -> it._2
                )
              }

            sql"""insert into "USES"("USE", "OUTPUT_ID")
                  values ({USE}, {OUTPUT_ID})
               """
              .batchByName(params: _*)
              .apply[List]()

          }

        }

      }


      init_aspects
      init_algorithms
      init_models
      init_interfaces
      init_uses

    }


    def drop(
      driver: Symbol
    ): Unit = {

      NamedDB(driver) autoCommit { implicit session =>

        sql"""drop table "USES"""".execute()()
        sql"""drop table "OUTPUTS"""".execute()()
        sql"""drop table "KINDS"""".execute()()
        sql"""drop table "DATABASES"""".execute()()
        sql"""drop table "SLICK_PROFILES"""".execute()()
        sql"""drop table "SCALIKE_DRIVERS"""".execute()()

        sql"""drop table "INTERFACES"""".execute()()
        sql"""drop table "PROGRAMS"""".execute()()
        sql"""drop table "STYLES"""".execute()()

        sql"""drop table "MODELS"""".execute()()
        sql"""drop table "PARALLELS"""".execute()()
        sql"""drop table "PARADIGMS"""".execute()()

        sql"""drop table "ALGORITHMS"""".execute()()
        sql"""drop table "RECURSIVES"""".execute()()
        sql"""drop table "NATURES"""".execute()()

        sql"""drop table "ASPECTS"""".execute()()
        sql"""drop table "STEPPERS"""".execute()()
        sql"""drop table "NEXTS"""".execute()()
        sql"""drop table "CLASSICS"""".execute()()
        sql"""drop table "LOOKS"""".execute()()

      }

    }

  }

}


package h2 {

  import _root_.scalikejdbc._


  package object dbdimensions {

    def init(
      driver: Symbol,
      plainSQL: Boolean = false
    ): Unit =
      database.scalikejdbc.dbdimensions.init(driver, plainSQL)


    def drop(
      driver: Symbol,
      plainSQL: Boolean = false
    ): Unit =
      database.scalikejdbc.dbdimensions.drop(driver, plainSQL)

  }

}


package sqlite {

  import _root_.scalikejdbc._


  package object dbdimensions {

    def init(
      driver: Symbol,
      plainSQL: Boolean
    ): Unit = {

      def init_aspects: Unit = {

        NamedDB(driver) autoCommit { implicit session =>

          sql"""create table "LOOKS"(
                  "ID" integer primary key autoincrement,
                  "LOOK" text
                )
             """.execute()()
          sql"""create table "CLASSICS"(
                  "ID" integer primary key autoincrement,
                  "LOOK_ID" integer,
                  constraint "LOOK_FK" foreign key ("LOOK_ID") references "LOOKS"("ID")
                )
             """.execute()()
          sql"""create table "NEXTS"(
                  "ID" integer primary key autoincrement,
                  "NEXT" text
                )
             """.execute()()
          sql"""create table "STEPPERS"(
                  "ID" integer primary key autoincrement,
                  "NEXT_ID" integer,
                  constraint "NEXT_FK" foreign key ("NEXT_ID") references "NEXTS"("ID")
                )
             """.execute()()
          sql"""create table "ASPECTS"(
                  "ID" integer primary key autoincrement,
                  "ASPECT" text,
                  "CLASSIC_ID" integer,
                  "STEPPER_ID" integer,
                  constraint "CLASSIC_FK" foreign key ("CLASSIC_ID") references "CLASSICS"("ID"),
                  constraint "STEPPER_FK" foreign key ("STEPPER_ID") references "STEPPERS"("ID")
                )
             """.execute()()

        }

      }


      def init_algorithms: Unit = {

        NamedDB(driver) autoCommit { implicit session =>

          sql"""create table "NATURES"(
                  "ID" integer primary key autoincrement,
                  "NATURE" text
                )
             """.execute()()
          sql"""create table "RECURSIVES"(
                  "ID" integer primary key autoincrement,
                  "NATURE_ID" integer,
                  constraint "NATURE_FK" foreign key ("NATURE_ID") references "NATURES"("ID")
                )
             """.execute()()
          sql"""create table "ALGORITHMS"(
                  "ID" integer primary key autoincrement,
                  "ALGORITHM" text,
                  "RECURSIVE_ID" integer,
                  constraint "RECURSIVE_FK" foreign key ("RECURSIVE_ID") references "RECURSIVES"("ID")
                )
             """.execute()()

        }

      }


      def init_models: Unit = {

        NamedDB(driver) autoCommit { implicit session =>

          sql"""create table "PARADIGMS"(
                  "ID" integer primary key autoincrement,
                  "PARADIGM" text
                )
             """.execute()()
          sql"""create table "PARALLELS"(
                  "ID" integer primary key autoincrement,
                  "PARADIGM_ID" integer,
                  constraint "PARADIGM_FK" foreign key ("PARADIGM_ID") references "PARADIGMS"("ID")
                )
             """.execute()()
          sql"""create table "MODELS"(
                  "ID" integer primary key autoincrement,
                  "MODEL" text,
                  "PARALLEL_ID" integer,
                  constraint "PARALLEL_FK" foreign key ("PARALLEL_ID") references "PARALLELS"("ID")
                )
             """.execute()()

        }

      }


      def init_interfaces: Unit = {

        NamedDB(driver) autoCommit { implicit session =>

          sql"""create table "STYLES"(
                  "ID" integer primary key autoincrement,
                  "STYLE" text
                )
             """.execute()()
          sql"""create table "PROGRAMS"(
                  "ID" integer primary key autoincrement,
                  "STYLE_ID" integer,
                  constraint "STYLE_FK" foreign key ("STYLE_ID") references "STYLES"("ID")
                )
             """.execute()()
          sql"""create table "INTERFACES"(
                  "ID" integer primary key autoincrement,
                  "INTERFACE" text,
                  "PROGRAM_ID" integer,
                  constraint "PROGRAM_FK" foreign key ("PROGRAM_ID") references "PROGRAMS"("ID")
                )
             """.execute()()

        }

      }


      def init_uses: Unit = {

        NamedDB(driver) autoCommit { implicit session =>

          sql"""create table "SLICK_PROFILES"(
                  "ID" integer primary key autoincrement,
                  "PROFILE" text,
                  "PLAIN_SQL" int
                )
             """.execute()()
          sql"""create table "SCALIKE_DRIVERS"(
                  "ID" integer primary key autoincrement,
                  "DRIVER" text,
                  "PLAIN_SQL" int
                )
             """.execute()()
          sql"""create table "DATABASES"(
                  "ID" integer primary key autoincrement,
                  "STORAGE" text,
                  "SLICK_PROFILE_ID" integer,
                  "SCALIKE_DRIVER_ID" integer,
                  constraint "SLICK_PROFILE_FK" foreign key ("SLICK_PROFILE_ID") references "SLICK_PROFILES"("ID"),
                  constraint "SCALIKE_DRIVER_FK" foreign key ("SCALIKE_DRIVER_ID") references "SCALIKE_DRIVERS"("ID")
                )
             """.execute()()
          sql"""create table "KINDS"(
                  "ID" integer primary key autoincrement,
                  "KIND" text,
                  "DATABASE_ID" integer,
                  constraint "DATABASE_FK" foreign key ("DATABASE_ID") references "DATABASES"("ID")
                )
             """.execute()()
          sql"""create table "OUTPUTS"(
                  "ID" integer primary key autoincrement,
                  "KIND_ID" integer,
                  constraint "KIND_FK" foreign key ("KIND_ID") references "KINDS"("ID")
                )
             """.execute()()
          sql"""create table "USES"(
                  "ID" integer primary key autoincrement,
                  "USE" text,
                  "OUTPUT_ID" integer,
                  constraint "OUTPUT_FK" foreign key ("OUTPUT_ID") references "OUTPUTS"("ID")
                )
             """.execute()()

        }

      }


      init_aspects
      init_algorithms
      init_models
      init_interfaces
      init_uses

      if (plainSQL)
        plainsql.dbdimensions.init(driver, create = false)
      else
        database.scalikejdbc.dao.dbdimensions.init(driver, create = false)

    }


    def drop(
      driver: Symbol,
      plainSQL: Boolean
    ): Unit =
      database.scalikejdbc.dbdimensions.drop(driver, plainSQL)

  }

}
