package queens
package axis.fifth.output
package database.scalikejdbc
package dao

import _root_.scalikejdbc._

import database.Implicits._


class SQL(driver: Symbol) {

  import database.scalikejdbc.dbdimensions.X


  // SQL ////////////////////////////////////////////////////////////// BOARD //

  import database.Mappings.DbBoard

  object SQLBoard extends SQLSyntaxSupport[DbBoard] {

    override val connectionPoolName = driver

    override val autoSession = NamedAutoSession(connectionPoolName)

    override val tableName = "BOARDS"

    override val nameConverters = Map(
      "_db" -> ""
    )

    def apply(bd: SyntaxProvider[DbBoard]
            )(rs: WrappedResultSet): DbBoard =
      apply(bd.resultName
          )(rs)

    def apply(bd: ResultName[DbBoard]
            )(rs: WrappedResultSet): DbBoard =
      autoConstruct(rs, bd)

  }


  // SQL ///////////////////////////////////////////////////////////// SQUARE //

  import database.Mappings.DbSquare

  object SQLSquare extends SQLSyntaxSupport[DbSquare] {

    override val connectionPoolName = driver

    override val autoSession = NamedAutoSession(connectionPoolName)

    override val tableName = "SQUARES"

    override val nameConverters = Map(
      "_db" -> "",
      "row" -> "row_no",
      "col" -> "column"
    )

    def apply(sq: SyntaxProvider[DbSquare]
             ,bd: SyntaxProvider[DbBoard]
            )(rs: WrappedResultSet): DbSquare =
      apply(sq.resultName
           ,bd.resultName
          )(rs)

    def apply(sq: ResultName[DbSquare]
             ,bd: ResultName[DbBoard]
            )(rs: WrappedResultSet): DbSquare = {
      val dbBoard = SQL.this.SQLBoard(bd)(rs)

      autoConstruct(rs, sq, "board_db", "boardID")
        .copy(board_db = dbBoard
             ,boardID = dbBoard.id
        )
    }

  }


  // SQL ////////////////////////////////////////////////////////////// QUEEN //

  import database.Mappings.DbQueen

  object SQLQueen extends SQLSyntaxSupport[DbQueen] {

    override val connectionPoolName = driver

    override val autoSession = NamedAutoSession(connectionPoolName)

    override val tableName = "QUEENS"

    override val nameConverters = Map(
      "_db" -> ""
    )

    import axis.fifth.output.database.dbdimensions.Mappings.{
      DbAspect,
      DbAlgorithm,
      DbModel,
      DbInterface,
      DbUse
    }

    def apply(qn: SyntaxProvider[DbQueen]
             ,dbAspect: DbAspect
             ,dbAlgorithm: DbAlgorithm
             ,dbModel: DbModel
             ,dbInterface: DbInterface
             ,dbUse: DbUse
            )(rs: WrappedResultSet): DbQueen =
      apply(qn.resultName
           ,dbAspect
           ,dbAlgorithm
           ,dbModel
           ,dbInterface
           ,dbUse
          )(rs)

    def apply(qn: ResultName[DbQueen]
             ,dbAspect: DbAspect
             ,dbAlgorithm: DbAlgorithm
             ,dbModel: DbModel
             ,dbInterface: DbInterface
             ,dbUse: DbUse
            )(rs: WrappedResultSet): DbQueen = {
      DbQueen(dbAspect, dbAspect.id
             ,dbAlgorithm, dbAlgorithm.id
             ,dbModel, dbModel.id
             ,dbInterface, dbInterface.id
             ,dbUse, dbUse.id
             ,rs.string(qn.tags_db)
             ,rs.longOpt(qn.id)
      )

    }

  }


  // SQL ////////////////////////////////////////////////////////////// ROUND //

  import database.Mappings.DbRound

  object SQLRound extends SQLSyntaxSupport[DbRound] {

    override val connectionPoolName = driver

    override val autoSession = NamedAutoSession(connectionPoolName)

    override val tableName = "ROUNDS"

    override val nameConverters = Map(
      "_db" -> ""
    )

    def apply(rn: SyntaxProvider[DbRound]
             ,bd: SyntaxProvider[DbBoard]
             ,dbQueen: DbQueen
            )(rs: WrappedResultSet): DbRound =
      apply(rn.resultName
           ,bd.resultName
           ,dbQueen
          )(rs)

    def apply(rn: ResultName[DbRound]
             ,bd: ResultName[DbBoard]
             ,dbQueen: DbQueen
            )(rs: WrappedResultSet): DbRound = {
      val dbBoard = SQL.this.SQLBoard(bd)(rs)

      DbRound(dbBoard
             ,dbBoard.id
             ,dbQueen
             ,dbQueen.id
             ,rs.string(rn.tag_db)
             ,rs.long(rn.number_db)
             ,rs.longOpt(rn.id)
      )

    }

  }


  // SQL /////////////////////////////////////////////////////////// SOLUTION //

  import database.Mappings.DbSolution

  object SQLSolution extends SQLSyntaxSupport[DbSolution] {

    override val connectionPoolName = driver

    override val autoSession = NamedAutoSession(connectionPoolName)

    override val tableName = "SOLUTIONS"

    override val nameConverters = Map(
      "_db" -> ""
    )

    def apply(sn: SyntaxProvider[DbSolution]
             ,dbRound: DbRound
            )(rs: WrappedResultSet): DbSolution =
      apply(sn.resultName
           ,dbRound
          )(rs)

    def apply(sn: ResultName[DbSolution]
             ,dbRound: DbRound
            )(rs: WrappedResultSet): DbSolution =
      autoConstruct(rs, sn, "round_db", "roundID")
        .copy(round_db = dbRound
             ,roundID = dbRound.id
        )

  }


  // SQL ///////////////////////////////////////////////////// SOLUTION_POINT //

  import database.Mappings.DbSolutionPoint

  object SQLSolutionPoint extends SQLSyntaxSupport[DbSolutionPoint] {

    override val connectionPoolName = driver

    override val autoSession = NamedAutoSession(connectionPoolName)

    override val tableName = "SOLUTION_POINTS"

    override val nameConverters = Map(
      "_db" -> "",
      "row" -> "row_no",
      "col" -> "column"
    )

    def apply(sp: SyntaxProvider[DbSolutionPoint]
             ,dbSolution: DbSolution
            )(rs: WrappedResultSet): DbSolutionPoint =
      apply(sp.resultName
           ,dbSolution
          )(rs)

    def apply(sp: ResultName[DbSolutionPoint]
             ,dbSolution: DbSolution
            )(rs: WrappedResultSet): DbSolutionPoint =
      autoConstruct(rs, sp, "solution_db", "solutionID")
        .copy(solution_db = dbSolution
             ,solutionID = dbSolution.id
        )

  }

}
