package queens
package axis.fifth.output
package database.scalikejdbc
package dao
package dbdimensions

import _root_.scalikejdbc._

import database.Implicits._


class SQL(driver: Symbol) {

  import database.scalikejdbc.dbdimensions.X


  // SQL ///////////////////////////////////////////////////////////// ASPECT //

  protected[dbdimensions] object Classic {

    import database.dbdimensions.Mappings.Classic._

    object SQLLook extends SQLSyntaxSupport[DbLook] {

      override val connectionPoolName = driver

      override val autoSession = NamedAutoSession(connectionPoolName)

      override val tableName = "LOOKS"

      def apply(lk: SyntaxProvider[DbLook]
              )(rs: WrappedResultSet): DbLook =
        apply(lk.resultName
            )(rs)

      def apply(lk: ResultName[DbLook]
              )(rs: WrappedResultSet): DbLook =
        autoConstruct(rs, lk)

    }

    object SQLClassic extends SQLSyntaxSupport[DbClassic] {

      override val connectionPoolName = driver

      override val autoSession = NamedAutoSession(connectionPoolName)

      override val tableName = "CLASSICS"

      def apply(cl: SyntaxProvider[DbClassic]
               ,lk: SyntaxProvider[DbLook]
              )(rs: WrappedResultSet): DbClassic =
        apply(cl.resultName
             ,lk.resultName
            )(rs)

      def apply(cl: ResultName[DbClassic]
               ,lk: ResultName[DbLook]
              )(rs: WrappedResultSet): DbClassic =
        autoConstruct(rs, cl, "look")
          .copy(look = SQLLook(lk)(rs))

    }

  }

  protected[dbdimensions] object Stepper {

    import database.dbdimensions.Mappings.Stepper._

    object SQLNext extends SQLSyntaxSupport[DbNext] {

      override val connectionPoolName = driver

      override val autoSession = NamedAutoSession(connectionPoolName)

      override val tableName = "NEXTS"

      def apply(nx: SyntaxProvider[DbNext]
              )(rs: WrappedResultSet): DbNext =
        apply(nx.resultName
            )(rs)

      def apply(nx: ResultName[DbNext]
              )(rs: WrappedResultSet): DbNext =
        autoConstruct(rs, nx)

    }

    object SQLStepper extends SQLSyntaxSupport[DbStepper] {

      override val connectionPoolName = driver

      override val autoSession = NamedAutoSession(connectionPoolName)

      override val tableName = "STEPPERS"

      def apply(st: SyntaxProvider[DbStepper]
               ,nx: SyntaxProvider[DbNext]
              )(rs: WrappedResultSet): DbStepper =
        apply(st.resultName
             ,nx.resultName
            )(rs)

      def apply(st: ResultName[DbStepper]
               ,nx: ResultName[DbNext]
              )(rs: WrappedResultSet): DbStepper =
        autoConstruct(rs, st, "next")
          .copy(next = SQLNext(nx)(rs))

    }

  }

  import database.dbdimensions.Mappings.{ DbClassicAspect, DbStepperAspect, DbAspect }

  object SQLAspect extends SQLSyntaxSupport[DbAspect] {

    override val connectionPoolName = driver

    override val autoSession = NamedAutoSession(connectionPoolName)

    override val tableName = "ASPECTS"

    object Classic {

      import database.dbdimensions.Mappings.Classic.DbLook
      import database.dbdimensions.Mappings.Classic.DbClassic

      def apply(at: SyntaxProvider[DbAspect]
               ,cl: SyntaxProvider[DbClassic]
               ,lk: SyntaxProvider[DbLook]
              )(rs: WrappedResultSet): DbAspect =
        apply(at.resultName
             ,cl.resultName
             ,lk.resultName
            )(rs)

      def apply(at: ResultName[DbAspect]
               ,cl: ResultName[DbClassic]
               ,lk: ResultName[DbLook]
              )(rs: WrappedResultSet): DbAspect =
        DbAspect(rs.string(at.aspect)
                ,Some(
                  DbClassicAspect(SQL.this.Classic.SQLClassic(cl, lk)(rs)
                                 ,rs.longOpt(at.id)
                  )
                )
                ,rs.longOpt(cl.id)
                ,None
                ,None
                ,rs.longOpt(at.id)
        )

    }

    object Stepper {

      import database.dbdimensions.Mappings.Stepper.DbNext
      import database.dbdimensions.Mappings.Stepper.DbStepper

      def apply(at: SyntaxProvider[DbAspect]
               ,st: SyntaxProvider[DbStepper]
               ,nx: SyntaxProvider[DbNext]
              )(rs: WrappedResultSet): DbAspect =
        apply(at.resultName
             ,st.resultName
             ,nx.resultName
            )(rs)

      def apply(at: ResultName[DbAspect]
               ,st: ResultName[DbStepper]
               ,nx: ResultName[DbNext]
              )(rs: WrappedResultSet): DbAspect =
        DbAspect(rs.string(at.aspect)
                ,None
                ,None
                ,Some(
                  DbStepperAspect(SQL.this.Stepper.SQLStepper(st, nx)(rs)
                                 ,rs.longOpt(at.id)
                  )
                )
                ,rs.longOpt(st.id)
                ,rs.longOpt(at.id)
        )

    }

  }


  // SQL ////////////////////////////////////////////////////////// ALGORITHM //

  protected[dbdimensions] object Recursive {

    import database.dbdimensions.Mappings.Recursive._

    object SQLNature extends SQLSyntaxSupport[DbNature] {

      override val connectionPoolName = driver

      override val autoSession = NamedAutoSession(connectionPoolName)

      override val tableName = "NATURES"

      def apply(nt: SyntaxProvider[DbNature]
              )(rs: WrappedResultSet): DbNature =
        apply(nt.resultName
            )(rs)

      def apply(nt: ResultName[DbNature]
              )(rs: WrappedResultSet): DbNature =
        autoConstruct(rs, nt)

    }

    object SQLRecursive extends SQLSyntaxSupport[DbRecursive] {

      override val connectionPoolName = driver

      override val autoSession = NamedAutoSession(connectionPoolName)

      override val tableName = "RECURSIVES"

      def apply(rc: SyntaxProvider[DbRecursive]
               ,nt: SyntaxProvider[DbNature]
              )(rs: WrappedResultSet): DbRecursive =
        apply(rc.resultName
             ,nt.resultName
            )(rs)

      def apply(rc: ResultName[DbRecursive]
               ,nt: ResultName[DbNature]
              )(rs: WrappedResultSet): DbRecursive =
        autoConstruct(rs, rc, "nature")
          .copy(nature = SQLNature(nt)(rs))

    }

  }

  import database.dbdimensions.Mappings.{ DbRecursiveAlgorithm, DbAlgorithm }

  object SQLAlgorithm extends SQLSyntaxSupport[DbAlgorithm] {

    override val connectionPoolName = driver

    override val autoSession = NamedAutoSession(connectionPoolName)

    override val tableName = "ALGORITHMS"

    def apply(am: SyntaxProvider[DbAlgorithm]
            )(rs: WrappedResultSet): DbAlgorithm =
        apply(am.resultName
            )(rs)

    def apply(am: ResultName[DbAlgorithm]
            )(rs: WrappedResultSet): DbAlgorithm =
        autoConstruct(rs, am, "recursiveAlgorithm", "recursiveID")

    object Recursive {

      import database.dbdimensions.Mappings.Recursive.DbNature
      import database.dbdimensions.Mappings.Recursive.DbRecursive

      def apply(am: SyntaxProvider[DbAlgorithm]
               ,rc: SyntaxProvider[DbRecursive]
               ,nt: SyntaxProvider[DbNature]
              )(rs: WrappedResultSet): DbAlgorithm =
        apply(am.resultName
             ,rc.resultName
             ,nt.resultName
            )(rs)

      def apply(am: ResultName[DbAlgorithm]
               ,rc: ResultName[DbRecursive]
               ,nt: ResultName[DbNature]
              )(rs: WrappedResultSet): DbAlgorithm =
        SQLAlgorithm(am)(rs)
          .copy(recursiveAlgorithm = Some(
                                       DbRecursiveAlgorithm(SQL.this.Recursive.SQLRecursive(rc, nt)(rs)
                                                           ,rs.longOpt(am.id)
                                       )
                                     )
               ,recursiveID = rs.longOpt(rc.id)
          )

    }

  }


  // SQL ////////////////////////////////////////////////////////////// MODEL //

  protected[dbdimensions] object Parallel {

    import database.dbdimensions.Mappings.Parallel._

    object SQLParadigm extends SQLSyntaxSupport[DbParadigm] {

      override val connectionPoolName = driver

      override val autoSession = NamedAutoSession(connectionPoolName)

      override val tableName = "PARADIGMS"

      def apply(pd: SyntaxProvider[DbParadigm]
              )(rs: WrappedResultSet): DbParadigm =
        apply(pd.resultName
            )(rs)

      def apply(pd: ResultName[DbParadigm]
              )(rs: WrappedResultSet): DbParadigm =
        autoConstruct(rs, pd)

    }

    protected[dbdimensions] object SQLParallel extends SQLSyntaxSupport[DbParallel] {

      override val connectionPoolName = driver

      override val autoSession = NamedAutoSession(connectionPoolName)

      override val tableName = "PARALLELS"

      def apply(pl: SyntaxProvider[DbParallel]
               ,pd: SyntaxProvider[DbParadigm]
              )(rs: WrappedResultSet): DbParallel =
        apply(pl.resultName
             ,pd.resultName
            )(rs)

      def apply(pl: ResultName[DbParallel]
               ,pd: ResultName[DbParadigm]
              )(rs: WrappedResultSet): DbParallel =
        autoConstruct(rs, pl, "paradigm")
          .copy(paradigm = SQLParadigm(pd)(rs))

    }

  }

  import database.dbdimensions.Mappings.{ DbParallelModel, DbModel }

  object SQLModel extends SQLSyntaxSupport[DbModel] {

    override val connectionPoolName = driver

    override val autoSession = NamedAutoSession(connectionPoolName)

    override val tableName = "MODELS"

    def apply(ml: SyntaxProvider[DbModel]
            )(rs: WrappedResultSet): DbModel =
      apply(ml.resultName
          )(rs)

    def apply(ml: ResultName[DbModel]
            )(rs: WrappedResultSet): DbModel =
        autoConstruct(rs, ml, "parallelModel", "parallelID")

    object Parallel {

      import database.dbdimensions.Mappings.Parallel.DbParadigm
      import database.dbdimensions.Mappings.Parallel.DbParallel

      def apply(ml: SyntaxProvider[DbModel]
               ,pl: SyntaxProvider[DbParallel]
               ,pd: SyntaxProvider[DbParadigm]
              )(rs: WrappedResultSet): DbModel =
        apply(ml.resultName
             ,pl.resultName
             ,pd.resultName
            )(rs)

      def apply(ml: ResultName[DbModel]
               ,pl: ResultName[DbParallel]
               ,pd: ResultName[DbParadigm]
              )(rs: WrappedResultSet): DbModel =
        SQLModel(ml)(rs)
          .copy(parallelModel = Some(
                                  DbParallelModel(SQL.this.Parallel.SQLParallel(pl, pd)(rs)
                                                 ,rs.longOpt(ml.id)
                                  )
                                )
               ,parallelID = rs.longOpt(pl.id)
          )

    }

  }


  // SQL ////////////////////////////////////////////////////////// INTERFACE //

  protected[dbdimensions] object Program {

    import database.dbdimensions.Mappings.Program._

    object SQLStyle extends SQLSyntaxSupport[DbStyle] {

      override val connectionPoolName = driver

      override val autoSession = NamedAutoSession(connectionPoolName)

      override val tableName = "STYLES"

      def apply(st: SyntaxProvider[DbStyle]
              )(rs: WrappedResultSet): DbStyle =
        apply(st.resultName
            )(rs)

      def apply(st: ResultName[DbStyle]
              )(rs: WrappedResultSet): DbStyle =
        autoConstruct(rs, st)

    }

    object SQLProgram extends SQLSyntaxSupport[DbProgram] {

      override val connectionPoolName = driver

      override val autoSession = NamedAutoSession(connectionPoolName)

      override val tableName = "PROGRAMS"

      def apply(pr: SyntaxProvider[DbProgram]
               ,st: SyntaxProvider[DbStyle]
              )(rs: WrappedResultSet): DbProgram =
        apply(pr.resultName
             ,st.resultName
            )(rs)

      def apply(pr: ResultName[DbProgram]
               ,st: ResultName[DbStyle]
              )(rs: WrappedResultSet): DbProgram =
        autoConstruct(rs, pr, "style")
          .copy(style = SQLStyle(st)(rs))

    }

  }

  import database.dbdimensions.Mappings.{ DbProgramInterface, DbInterface }

  object SQLInterface extends SQLSyntaxSupport[DbInterface] {

    override val connectionPoolName = driver

    override val autoSession = NamedAutoSession(connectionPoolName)

    override val tableName = "INTERFACES"

    import database.dbdimensions.Mappings.Program.DbStyle
    import database.dbdimensions.Mappings.Program.DbProgram

    def apply(ie: SyntaxProvider[DbInterface]
             ,pr: SyntaxProvider[DbProgram]
             ,st: SyntaxProvider[DbStyle]
            )(rs: WrappedResultSet): DbInterface =
      apply(ie.resultName
           ,pr.resultName
           ,st.resultName
          )(rs)

    def apply(ie: ResultName[DbInterface]
             ,pr: ResultName[DbProgram]
             ,st: ResultName[DbStyle]
            )(rs: WrappedResultSet): DbInterface =
      autoConstruct(rs, ie, "programInterface", "programID")
        .copy(programInterface = DbProgramInterface(SQL.this.Program.SQLProgram(pr, st)(rs)
                                                   ,rs.longOpt(ie.id)
                                 )
             ,programID = rs.int(pr.id)
        )

  }


  // SQL //////////////////////////////////////////////////////////////// USE //

  protected[dbdimensions] object Output {

    object Database {

      import database.dbdimensions.Mappings.Output.Database.Slick._

      object SQLSlickProfile extends SQLSyntaxSupport[DbSlickProfile] {

        override val connectionPoolName = driver

        override val autoSession = NamedAutoSession(connectionPoolName)

        override val tableName = "SLICK_PROFILES"

        def apply(sp: SyntaxProvider[DbSlickProfile]
                )(rs: WrappedResultSet): DbSlickProfile =
          apply(sp.resultName
              )(rs)

        def apply(sp: ResultName[DbSlickProfile]
                )(rs: WrappedResultSet): DbSlickProfile =
          autoConstruct(rs, sp)

      }

      import database.dbdimensions.Mappings.Output.Database.Scalike._

      object SQLScalikeDriver extends SQLSyntaxSupport[DbScalikeDriver] {

        override val connectionPoolName = driver

        override val autoSession = NamedAutoSession(connectionPoolName)

        override val tableName = "SCALIKE_DRIVERS"

        def apply(sd: SyntaxProvider[DbScalikeDriver]
                )(rs: WrappedResultSet): DbScalikeDriver =
          apply(sd.resultName
              )(rs)

        def apply(sd: ResultName[DbScalikeDriver]
                )(rs: WrappedResultSet): DbScalikeDriver =
          autoConstruct(rs, sd)

      }

    }

    import database.dbdimensions.Mappings.Output.Database.DbDatabase

    object SQLDatabase extends SQLSyntaxSupport[DbDatabase] {

      override val connectionPoolName = driver

      override val autoSession = NamedAutoSession(connectionPoolName)

      override val tableName = "DATABASES"

      object Slick {

        import database.dbdimensions.Mappings.Output.Database.Slick._

        def apply(db: SyntaxProvider[DbDatabase]
                 ,sp: SyntaxProvider[DbSlickProfile]
                )(rs: WrappedResultSet): DbDatabase =
          apply(db.resultName
               ,sp.resultName
              )(rs)

        def apply(db: ResultName[DbDatabase]
                 ,sp: ResultName[DbSlickProfile]
                )(rs: WrappedResultSet): DbDatabase = {
          val dbSlickProfile = SQL.this.Output.Database.SQLSlickProfile(sp)(rs)

          autoConstruct(rs, db, "slickProfile", "slickProfileID", "scalikeDriver", "scalikeDriverID")
            .copy(slickProfile = Some(dbSlickProfile), slickProfileID = dbSlickProfile.id)
        }

      }

      object Scalike {

        import database.dbdimensions.Mappings.Output.Database.Scalike._

        def apply(db: SyntaxProvider[DbDatabase]
                 ,sd: SyntaxProvider[DbScalikeDriver]
                )(rs: WrappedResultSet): DbDatabase =
          apply(db.resultName
               ,sd.resultName
              )(rs)

        def apply(db: ResultName[DbDatabase]
                 ,sd: ResultName[DbScalikeDriver]
                )(rs: WrappedResultSet): DbDatabase = {
          val dbScalikeDriver = SQL.this.Output.Database.SQLScalikeDriver(sd)(rs)

          autoConstruct(rs, db, "slickProfile", "slickProfileID", "scalikeDriver", "scalikeDriverID")
            .copy(scalikeDriver = Some(dbScalikeDriver), scalikeDriverID = dbScalikeDriver.id)
        }

      }

    }

    import database.dbdimensions.Mappings.Output.DbKind

    object SQLKind extends SQLSyntaxSupport[DbKind] {

      override val connectionPoolName = driver

      override val autoSession = NamedAutoSession(connectionPoolName)

      override val tableName = "KINDS"

      def apply(ki: SyntaxProvider[DbKind]
              )(rs: WrappedResultSet): DbKind =
        apply(ki.resultName
            )(rs)

      def apply(ki: ResultName[DbKind]
              )(rs: WrappedResultSet): DbKind =
        autoConstruct(rs, ki, "databaseID")

      object Database {

        import database.dbdimensions.Mappings.Output.DbDatabaseKind
        import database.dbdimensions.Mappings.Output.Database.DbDatabase

        def apply(ki: SyntaxProvider[DbKind]
                 ,dbDatabase: DbDatabase
                )(rs: WrappedResultSet): (DbDatabaseKind, DbKind) =
          apply(ki.resultName
               ,dbDatabase
              )(rs)

        def apply(ki: ResultName[DbKind]
                 ,dbDatabase: DbDatabase
                )(rs: WrappedResultSet): (DbDatabaseKind, DbKind) =
          ( DbDatabaseKind(dbDatabase
                          ,dbDatabase.id
                          ,rs.longOpt(ki.id))
          , SQLKind.apply(ki)(rs).copy(databaseID = dbDatabase.id) )

      }

    }

    import database.dbdimensions.Mappings.Output.DbOutput

    object SQLOutput extends SQLSyntaxSupport[DbOutput] {

      override val connectionPoolName = driver

      override val autoSession = NamedAutoSession(connectionPoolName)

      override val tableName = "OUTPUTS"

      def apply(ot: SyntaxProvider[DbOutput]
               ,ki: SyntaxProvider[DbKind]
              )(rs: WrappedResultSet): DbOutput =
        apply(ot.resultName
             ,ki.resultName
            )(rs)

      def apply(ot: ResultName[DbOutput]
               ,ki: ResultName[DbKind]
              )(rs: WrappedResultSet): DbOutput =
        DbOutput(SQLKind(ki)(rs)
                ,rs.int(ki.id)
                ,rs.longOpt(ot.id)
        )

      object Database {

        import database.dbdimensions.Mappings.Output.DbDatabaseOutput
        import database.dbdimensions.Mappings.Output.Database.DbDatabase

        def apply(ot: SyntaxProvider[DbOutput]
                 ,ki: SyntaxProvider[DbKind]
                 ,dbDatabase: DbDatabase
                )(rs: WrappedResultSet): (DbDatabaseOutput, DbOutput) =
          apply(ot.resultName
               ,ki.resultName
               ,dbDatabase
              )(rs)

        def apply(ot: ResultName[DbOutput]
                 ,ki: ResultName[DbKind]
                 ,dbDatabase: DbDatabase
                )(rs: WrappedResultSet): (DbDatabaseOutput, DbOutput) = {
          val (dbDatabaseKind, dbKind) = SQLKind.Database(ki, dbDatabase)(rs)

          ( DbDatabaseOutput(dbDatabaseKind
                            ,dbKind.id)
          , SQLOutput.apply(ot, ki)(rs).copy(kindID = dbKind.id) )
        }

      }

    }

  }

  import database.dbdimensions.Mappings.DbUse

  object SQLUse extends SQLSyntaxSupport[DbUse] {

    override val connectionPoolName = driver

    override val autoSession = NamedAutoSession(connectionPoolName)

    override val tableName = "USES"

    import database.dbdimensions.Mappings.Output.{ DbKind, DbOutput }
    import database.dbdimensions.Mappings.DbOutputUse

    def apply(ue: SyntaxProvider[DbUse]
             ,ot: SyntaxProvider[DbOutput]
             ,ki: SyntaxProvider[DbKind]
            )(rs: WrappedResultSet): DbUse =
      apply(ue.resultName
           ,ot.resultName
           ,ki.resultName
          )(rs)

    def apply(ue: ResultName[DbUse]
             ,ot: ResultName[DbOutput]
             ,ki: ResultName[DbKind]
            )(rs: WrappedResultSet): DbUse = {
      val dbOutput = SQL.this.Output.SQLOutput(ot, ki)(rs)
      val id = rs.longOpt(ue.id)

      DbUse(rs.string(ue.use)
           ,DbOutputUse(dbOutput
                       ,None
                       ,id
           )
           ,dbOutput.id.get
           ,id
      )
    }

    object Slick {

      import database.dbdimensions.Mappings.Output.Database.Slick._

      import database.dbdimensions.Mappings.Output.Database.DbDatabase
      import database.dbdimensions.Mappings.Output.DbDatabaseKind
      import database.dbdimensions.Mappings.Output.DbDatabaseOutput
      import database.dbdimensions.Mappings.DbDatabaseOutputUse

      def apply(ue: SyntaxProvider[DbUse]
               ,ot: SyntaxProvider[DbOutput]
               ,ki: SyntaxProvider[DbKind]
               ,db: SyntaxProvider[DbDatabase]
               ,sp: SyntaxProvider[DbSlickProfile]
              )(rs: WrappedResultSet): DbUse =
        apply(ue.resultName
             ,ot.resultName
             ,ki.resultName
             ,db.resultName
             ,sp.resultName
            )(rs)

      def apply(ue: ResultName[DbUse]
               ,ot: ResultName[DbOutput]
               ,ki: ResultName[DbKind]
               ,db: ResultName[DbDatabase]
               ,sp: ResultName[DbSlickProfile]
              )(rs: WrappedResultSet): DbUse = {
        val dbDatabase = SQL.this.Output.SQLDatabase.Slick(db, sp)(rs)
        val (dbDatabaseOutput, dbOutput) = SQL.this.Output.SQLOutput.Database(ot, ki, dbDatabase)(rs)
        val (dbDatabaseKind, dbKind) = (dbDatabaseOutput.databaseKind, dbOutput.kind)

        val id = rs.longOpt(ue.id)

        DbUse(rs.string(ue.use)
             ,DbOutputUse(dbOutput
                         ,Some(
                           DbDatabaseOutputUse(dbDatabaseOutput
                                              ,id
                           )
                         )
                         ,id
             )
             ,dbOutput.id.get
             ,id
        )
      }

    }

    object Scalike {

      import database.dbdimensions.Mappings.Output.Database.Scalike._

      import database.dbdimensions.Mappings.Output.Database.DbDatabase
      import database.dbdimensions.Mappings.Output.DbDatabaseKind
      import database.dbdimensions.Mappings.Output.DbDatabaseOutput
      import database.dbdimensions.Mappings.DbDatabaseOutputUse

      def apply(ue: SyntaxProvider[DbUse]
               ,ot: SyntaxProvider[DbOutput]
               ,ki: SyntaxProvider[DbKind]
               ,db: SyntaxProvider[DbDatabase]
               ,sd: SyntaxProvider[DbScalikeDriver]
              )(rs: WrappedResultSet): DbUse =
        apply(ue.resultName
             ,ot.resultName
             ,ki.resultName
             ,db.resultName
             ,sd.resultName
            )(rs)

      def apply(ue: ResultName[DbUse]
               ,ot: ResultName[DbOutput]
               ,ki: ResultName[DbKind]
               ,db: ResultName[DbDatabase]
               ,sd: ResultName[DbScalikeDriver]
              )(rs: WrappedResultSet): DbUse = {
        val dbDatabase = SQL.this.Output.SQLDatabase.Scalike(db, sd)(rs)
        val (dbDatabaseOutput, dbOutput) = SQL.this.Output.SQLOutput.Database(ot, ki, dbDatabase)(rs)
        val (dbDatabaseKind, dbKind) = (dbDatabaseOutput.databaseKind, dbOutput.kind)

        val id = rs.longOpt(ue.id)

        DbUse(rs.string(ue.use)
             ,DbOutputUse(dbOutput
                         ,Some(
                           DbDatabaseOutputUse(dbDatabaseOutput
                                              ,id
                           )
                         )
                         ,id
             )
             ,dbOutput.id.get
             ,id
        )
      }

    }

  }

}
