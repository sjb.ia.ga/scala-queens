package queens
package axis.fifth.output
package database.scalikejdbc
package dao

import _root_.scalikejdbc._


package object dbdimensions {

  val X = 0


  def init(
    driver: Symbol,
    create: Boolean = true
  ): Unit = {
    plainsql.dbdimensions.init(driver, create = create, populate = false)


    val SQL = new dao.dbdimensions.SQL(driver)


    def init_aspects = {

      import dimensions.Dimension.Aspect.Classic.{ Straight, Callback }
      import dimensions.Dimension.Aspect.Stepper.{ OneIncrement, Permutations }
      import dimensions.Dimension.Aspect.{ classic$, stepper$ }

      val obls = List(Straight, Callback).map(_.toString)
      val obns = List(OneIncrement, Permutations).map(_.toString)

      import SQL.Classic.{ SQLLook, SQLClassic }
      import SQL.Stepper.{ SQLNext, SQLStepper }
      import SQL.SQLAspect

      NamedDB(driver) localTx { implicit session =>

        val lk = SQLLook.syntax("lk")
        val cl = SQLClassic.syntax("cl")
        val nx = SQLNext.syntax("nx")
        val st = SQLStepper.syntax("st")

        val at = SQLAspect.syntax("at")

        applyUpdate { insertInto(SQLLook).values(X, "") }


        obls.foreach { it =>
          val lk = SQLLook.column
          applyUpdate { insertInto(SQLLook).namedValues(lk.look -> it) }
        }

        applyUpdate { insertInto(SQLClassic).values(X, X) }

        withSQL {
          select
            .from(SQLLook as lk)
            .where.not.eq(lk.id, X)
        }
          .map(SQLLook(lk))
          .list()()
          .foreach { it =>
            val cl = SQLClassic.column
            applyUpdate { insertInto(SQLClassic).namedValues(cl.lookID -> it.id) }
          }

        applyUpdate { insertInto(SQLNext).values(X, "") }

        obns.foreach { it =>
          val nx = SQLNext.column
          applyUpdate { insertInto(SQLNext).namedValues(nx.next -> it) }
        }

        applyUpdate { insertInto(SQLStepper).values(X, X) }

        withSQL {
          select
            .from(SQLNext as nx)
            .where.not.eq(nx.id, X)
        }
          .map(SQLNext(nx))
          .list()()
          .foreach { it =>
            val st = SQLStepper.column
            applyUpdate { insertInto(SQLStepper).namedValues(st.nextID -> it.id) }
          }

        withSQL {
          select
            .from(SQLLook as lk)
            .join(SQLClassic as cl).on(cl.lookID, lk.id)
            .where.not.eq(cl.id, X)
        }
          .map(SQLClassic(cl, lk))
          .list()()
          .foreach { it =>
            val at = SQLAspect.column
            applyUpdate {
              insert.into(SQLAspect)
                .namedValues(
                  at.aspect -> classic$(it.look.look),
                  at.classicID -> it.id,
                  at.stepperID -> X
                )
            }
          }

        withSQL {
          select
            .from(SQLNext as nx)
            .join(SQLStepper as st).on(st.nextID, nx.id)
            .where.not.eq(st.id, X)
        }
          .map(SQLStepper(st, nx))
          .list()()
          .foreach { it =>
            val at = SQLAspect.column
            applyUpdate {
              insert.into(SQLAspect)
                .namedValues(
                  at.aspect -> stepper$(it.next.next),
                  at.classicID -> X,
                  at.stepperID -> it.id
                )
            }
          }

      }

    }


    def init_algorithms = {

      import dimensions.Dimension.Algorithm.{ Iterative, iterative } // FIXME: Iterative
      import dimensions.Dimension.Algorithm.Recursive.{ Native, Extern }
      import dimensions.Dimension.Algorithm.{ recursive$ }

      val obrs = List(Native, Extern).map(_.toString)
      val obas = List(iterative).map(_.toString) // FIXME: Iterative

      import SQL.Recursive.{ SQLNature, SQLRecursive }
      import SQL.SQLAlgorithm

      NamedDB(driver) localTx { implicit session =>

        val nt = SQLNature.syntax("nt")
        val rc = SQLRecursive.syntax("rc")

        val am = SQLAlgorithm.syntax("am")

        applyUpdate { insertInto(SQLNature).values(X, "") }

        obrs.foreach { it =>
          val nt = SQLNature.column
          applyUpdate { insertInto(SQLNature).namedValues(nt.nature -> it) }
        }

        applyUpdate { insertInto(SQLRecursive).values(X, X) }

        obas.foreach { it =>
          val am = SQLAlgorithm.column
          applyUpdate {
            insert.into(SQLAlgorithm)
              .namedValues(
                am.algorithm -> it,
                am.recursiveID -> X
              )
          }
        }

        withSQL {
          select
            .from(SQLNature as nt)
            .where.not.eq(nt.id, X)
        }
          .map(SQLNature(nt))
          .list()()
          .foreach { it =>
            val rc = SQLRecursive.column
            applyUpdate { insertInto(SQLRecursive).namedValues(rc.natureID -> it.id) }
          }

        withSQL {
          select
            .from(SQLNature as nt)
            .join(SQLRecursive as rc).on(rc.natureID, nt.id)
            .where.not.eq(rc.id, X)
        }
          .map(SQLRecursive(rc, nt))
          .list()()
          .foreach { it =>
            val am = SQLAlgorithm.column
            applyUpdate {
              insert.into(SQLAlgorithm)
                .namedValues(
                  am.algorithm -> recursive$(it.nature.nature),
                  am.recursiveID -> it.id
                )
            }
          }

      }

    }


    def init_models = {

      import dimensions.Dimension.Model.{ Flow, flow } // FIXME: Flow
      import dimensions.Dimension.Model.Parallel.{ Actors, Futures }
      import dimensions.Dimension.Model.{ parallel$ }

      val obps = List(Actors, Futures).map(_.toString)
      val obms = List(flow).map(_.toString) // FIXME: Flow

      import SQL.Parallel.{ SQLParadigm, SQLParallel }
      import SQL.SQLModel

      NamedDB(driver) localTx { implicit session =>

        val pd = SQLParadigm.syntax("pd")
        val pl = SQLParallel.syntax("pl")

        val ml = SQLModel.syntax("ml")

        applyUpdate { insertInto(SQLParadigm).values(X, "") }

        obps.foreach { it =>
          val pd = SQLParadigm.column
          applyUpdate { insertInto(SQLParadigm).namedValues(pd.paradigm -> it) }
        }

        applyUpdate { insertInto(SQLParallel).values(X, X) }

        obms.foreach { it =>
          val ml = SQLModel.column
          applyUpdate {
            insert.into(SQLModel)
              .namedValues(
                ml.model -> it,
                ml.parallelID -> X
              )
          }
        }

        withSQL {
          select
            .from(SQLParadigm as pd)
            .where.not.eq(pd.id, X)
        }
          .map(SQLParadigm(pd))
          .list()()
          .foreach { it =>
            val pl = SQLParallel.column
            applyUpdate { insertInto(SQLParallel).namedValues(pl.paradigmID -> it.id) }
          }

        withSQL {
          select
            .from(SQLParadigm as pd)
            .join(SQLParallel as pl).on(pl.paradigmID, pd.id)
            .where.not.eq(pl.id, X)
        }
          .map(SQLParallel(pl, pd))
          .list()()
          .foreach { it =>
            val ml = SQLModel.column
            applyUpdate {
              insert.into(SQLModel)
                .namedValues(
                  ml.model -> parallel$(it.paradigm.paradigm),
                  ml.parallelID -> it.id
                )
            }
          }

      }

    }


    def init_interfaces = {

      import dimensions.Dimension.Interface.Program.{ Monad, Simple }
      import dimensions.Dimension.Interface.{ program$ }

      import database.dbdimensions.Mappings.Program._

      val obss = List("", Monad, Simple).map(_.toString).tail

      import SQL.Program.{ SQLStyle, SQLProgram }
      import SQL.SQLInterface

      NamedDB(driver) localTx { implicit session =>

        val st = SQLStyle.syntax("st")
        val pr = SQLProgram.syntax("pr")

        val ie = SQLInterface.syntax("ie")

        obss.foreach { it =>
          val st = SQLStyle.column
          applyUpdate { insertInto(SQLStyle).namedValues(st.style -> it) }
        }

        withSQL {
          select
            .from(SQLStyle as st)
        }
          .map(SQLStyle(st))
          .list()()
          .foreach { it =>
            val pr = SQLProgram.column
            applyUpdate { insertInto(SQLProgram).namedValues(pr.styleID -> it.id) }
          }

        withSQL {
          select
            .from(SQLStyle as st)
            .join(SQLProgram as pr).on(pr.styleID, st.id)
        }
          .map(SQLProgram(pr, st))
          .list()()
          .foreach { it =>
            val ie = SQLInterface.column
            applyUpdate {
              insert.into(SQLInterface)
                .namedValues(
                  ie.interface -> program$(it.style.style),
                  ie.programID -> it.id
                )
            }
          }

      }

    }


    def init_uses = {

      import dimensions.Dimension.Use.Output.{ Buffered, Console }
      import dimensions.Dimension.Use.Output.Database.Slick
      import dimensions.Dimension.Use.Output.Database.Scalike
      import dimensions.Dimension.Use.{ output$ }
      import dimensions.Dimension.Use.Output.{ database$ }
      import dimensions.Dimension.Use.Output.Database.{ slick$, scalike$ }

      val obps = List("", Slick.Profile.H2, Slick.Profile.SQLite).map(_.toString).tail
      val obds = List("", Scalike.Driver.H2, Scalike.Driver.SQLite).map(_.toString).tail
      val obks = List("", Buffered, Console).map(_.toString).tail

      import SQL.Output.Database.{ SQLSlickProfile, SQLScalikeDriver }
      import SQL.Output.{ SQLKind, SQLDatabase, SQLOutput }
      import SQL.SQLUse

      NamedDB(driver) localTx { implicit session =>

        val sp = SQLSlickProfile.syntax("sp")
        val sd = SQLScalikeDriver.syntax("sd")

        val db = SQLDatabase.syntax("db")
        val ki = SQLKind.syntax("ki")
        val ot = SQLOutput.syntax("ot")

        val ue = SQLUse.syntax("ue")

        applyUpdate { insertInto(SQLSlickProfile).values(X, "", false) }

        (obps.flatMap(List.fill(2)(_) zip List(false, true))).foreach { case (it, plainSQL) =>
          val sp = SQLSlickProfile.column
          applyUpdate {
            insert
              .into(SQLSlickProfile)
              .namedValues(
                sp.profile -> it,
                sp.plainSQL -> plainSQL
              )
          }
        }

        applyUpdate { insertInto(SQLScalikeDriver).values(X, "", false) }

        (obds.flatMap(List.fill(2)(_) zip List(false, true))).foreach { case (it, plainSQL) =>
          val sd = SQLScalikeDriver.column
          applyUpdate {
            insert
              .into(SQLScalikeDriver)
              .namedValues(
                sd.driver -> it,
                sd.plainSQL -> plainSQL
              )
          }
        }

        applyUpdate { insertInto(SQLDatabase).values(X, "", X, X) }

        obks.foreach { it =>
          val ki = SQLKind.column
          applyUpdate {
            insert.into(SQLKind)
              .namedValues(
                ki.kind -> it,
                ki.databaseID -> X
              )
          }
        }

        withSQL {
          select
            .from(SQLSlickProfile as sp)
            .where.not.eq(sp.id, X)
        }
          .map(SQLSlickProfile(sp))
          .list()()
          .foreach { it =>
            val db = SQLDatabase.column
            applyUpdate {
              insert.into(SQLDatabase)
                .namedValues(
                  db.storage -> slick$(it.profile, it.plainSQL),
                  db.slickProfileID -> it.id,
                  db.scalikeDriverID -> X
                )
            }
          }

        withSQL {
          select
            .from(SQLDatabase as db)
            .join(SQLSlickProfile as sp).on(db.slickProfileID, sp.id)
            .where.not.eq(db.id, X)
            .and.not.eq(sp.id, X)
        }
          .map(SQLDatabase.Slick(db, sp))
          .list()()
          .foreach { it =>
            val ki = SQLKind.column
            applyUpdate {
              insert.into(SQLKind)
                .namedValues(
                  ki.kind -> database$(it.storage),
                  ki.databaseID -> it.id
                )
            }
          }

        withSQL {
          select
            .from(SQLScalikeDriver as sd)
            .where.not.eq(sd.id, X)
        }
          .map(SQLScalikeDriver(sd))
          .list()()
          .foreach { it =>
            val db = SQLDatabase.column
            applyUpdate {
              insert.into(SQLDatabase)
                .namedValues(
                  db.storage -> scalike$(it.driver, it.plainSQL),
                  db.slickProfileID -> X,
                  db.scalikeDriverID -> it.id
                )
            }
          }

        withSQL {
          select
            .from(SQLDatabase as db)
            .join(SQLScalikeDriver as sd).on(db.scalikeDriverID, sd.id)
            .where.not.eq(db.id, X)
            .and.not.eq(sd.id, X)
        }
          .map(SQLDatabase.Scalike(db, sd))
          .list()()
          .foreach { it =>
            val ki = SQLKind.column
            applyUpdate {
              insert.into(SQLKind)
                .namedValues(
                  ki.kind -> database$(it.storage),
                  ki.databaseID -> it.id
                )
            }
          }

        withSQL {
          select
            .from(SQLKind as ki)
        }
          .map(SQLKind(ki))
          .list()()
          .foreach { it =>
            val ot = SQLOutput.column
            applyUpdate { insertInto(SQLOutput).namedValues(ot.kindID -> it.id) }
          }


        withSQL {
          select
            .from(SQLOutput as ot)
            .join(SQLKind as ki).on(ot.kindID, ki.id)
        }
          .map(SQLOutput(ot, ki))
          .list()()
          .foreach { it =>
            val ue = SQLUse.column
            applyUpdate {
              insert.into(SQLUse)
                .namedValues(
                  ue.use -> output$(it.kind.kind),
                  ue.outputID -> it.id
                )
            }
          }

      }

    }


    init_aspects
    init_algorithms
    init_models
    init_interfaces
    init_uses

  }


  def drop(
    driver: Symbol
  ): Unit =
    plainsql.dbdimensions.drop(driver)

}
