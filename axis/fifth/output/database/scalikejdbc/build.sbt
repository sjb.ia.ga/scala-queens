libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",
  "org.scalikejdbc" %% "scalikejdbc" % "3.5.0",
  "org.scalikejdbc" %% "scalikejdbc-config" % "3.5.0",
  "org.scalikejdbc" %% "scalikejdbc-syntax-support-macro" % "3.5.0",
  "com.h2database" % "h2" % "2.2.224",
  "org.xerial" % "sqlite-jdbc" % "3.46.0.0",
  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
