package queens
package axis.fifth.output
package database.slick
package h2.dbdimensions

import scala.collection.mutable.{ ListBuffer => MutableList }

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits._

import org.scalatest.flatspec.AnyFlatSpec

import _root_.slick.basic.DatabaseConfig
import _root_.slick.jdbc.H2Profile
import _root_.slick.jdbc.H2Profile.backend.Database

import database.dbdimensions.Mappings._


class AlgorithmsSpec extends AnyFlatSpec {

  import dao.dbdimensions.X

  def apply(): List[String] = {

    val dc = DatabaseConfig.forConfig[H2Profile]("h2mem_dc")
    val db = dc.db.asInstanceOf[Database]

    val tables =
      new axis.fifth.output.database.slick.dao.dbdimensions.Tables[H2Profile](dc.profile)

    init(db, tables)

    try {

      import tables.profile.api._
      import tables.algorithms

      val result = MutableList[String]()

      def iterative(_u: Unit) = {
        val joinQuery = for {
          a <- algorithms if a.recursiveID === X
        } yield Tuple1(a.algorithm)

        db.run(joinQuery.result)
      }

      def recursiveNatures(_u: Unit) = {
        val joinQuery = for {
          a <- algorithms if a.recursiveID =!= X
          r <- a.recursive if a.recursiveID === r.id
          n <- r.nature
        } yield (a.algorithm, n.nature)

        db.run(joinQuery.result)
      }

      val noop: DBIO[Unit] = DBIO.seq()

      import dimensions.Dimension.Algorithm.{ recursive$ }

      Await.result(db
        .run(noop)
        .flatMap(iterative(_)).map(
          _.foreach { it => result += s"algorithm: ${it._1} == ${it._1}" }
        )
        .flatMap(recursiveNatures(_)).map(
          _.foreach { it => result += s"algorithm: ${it._1} == ${recursive$(it._2)}" }
        )
        ,Duration.Inf
      )

      result.toList

    } finally {
      drop(db, tables)
      db.close()
    }

  }

  "Algorithms table" should "contain all" in {

    Synchronized(X) {

      import dimensions.Dimension.Algorithm.{ iterative, native, extern }
      import dimensions.Dimension.Algorithm.{ Iterative }
      import dimensions.Dimension.Algorithm.Recursive.{ Native, Extern }
      import dimensions.Dimension.Algorithm.{ recursive$ }

      val ls = List(
  //      (iterative, Iterative)
        (iterative, iterative)
      ).map { it => s"algorithm: ${it._1} == ${it._2}" }
      .concat(List(
        (native, recursive$(Native)),
        (extern, recursive$(Extern))
      ).map { it => s"algorithm: ${it._1} == ${it._2}" }
      )

      assert(ls.sorted == this().sorted)

    }

  }

}
