package queens
package axis.fifth.output
package database.slick
package h2.dbdimensions

import scala.collection.mutable.{ ListBuffer => MutableList }

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits._

import org.scalatest.flatspec.AnyFlatSpec

import _root_.slick.basic.DatabaseConfig
import _root_.slick.jdbc.H2Profile
import _root_.slick.jdbc.H2Profile.backend.Database

import database.dbdimensions.Mappings._


class ModelsSpec extends AnyFlatSpec {

  import dao.dbdimensions.X

  def apply(): List[String] = {

    val dc = DatabaseConfig.forConfig[H2Profile]("h2mem_dc")
    val db = dc.db.asInstanceOf[Database]

    val tables =
      new axis.fifth.output.database.slick.dao.dbdimensions.Tables[H2Profile](dc.profile)

    init(db, tables)

    try {

      import tables.profile.api._
      import tables.models

      val result = MutableList[String]()

      def flow(_u: Unit) = {
        val joinQuery = for {
          m <- models if m.parallelID === X
        } yield Tuple1(m.model)

        db.run(joinQuery.result)
      }

      def parallelParadigms(_u: Unit) = {
        val joinQuery = for {
          m <- models if m.parallelID =!= X
          p <- m.parallel if m.parallelID === p.id
          pp <- p.paradigm
        } yield (m.model, pp.paradigm)

        db.run(joinQuery.result)
      }

      val noop: DBIO[Unit] = DBIO.seq()

      import dimensions.Dimension.Model.{ parallel$ }

      Await.result(db
        .run(noop)
        .flatMap(flow(_)).map(
          _.foreach { it => result += s"model: ${it._1} == ${it._1}" }
        )
        .flatMap(parallelParadigms(_)).map(
          _.foreach { it => result += s"model: ${it._1} == ${parallel$(it._2)}" }
        )
        ,Duration.Inf
      )

      result.toList

    } finally {
      drop(db, tables)
      db.close()
    }

  }

  "Models table" should "contain all" in {

    Synchronized(X) {

      import dimensions.Dimension.Model.{ flow, actors, futures }
      import dimensions.Dimension.Model.{ Flow }
      import dimensions.Dimension.Model.Parallel.{ Actors, Futures }
      import dimensions.Dimension.Model.{ parallel$ }

      val ls = List(
  //      (flow, Flow)
        (flow, flow)
      ).map { it => s"model: ${it._1} == ${it._2}" }
      .concat(List(
        (actors, parallel$(Actors)),
        (futures, parallel$(Futures))
      ).map { it => s"model: ${it._1} == ${it._2}" }
      )

      assert(ls.sorted == this().sorted)

    }

  }

}
