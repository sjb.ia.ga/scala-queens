package queens
package axis.fifth.output
package database.slick
package h2.dbdimensions

import scala.collection.mutable.{ ListBuffer => MutableList }

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits._

import org.scalatest.flatspec.AnyFlatSpec

import _root_.slick.basic.DatabaseConfig
import _root_.slick.jdbc.H2Profile
import _root_.slick.jdbc.H2Profile.backend.Database

import database.dbdimensions.Mappings._


class UsesSpec extends AnyFlatSpec {

  import dao.dbdimensions.X

  def apply(): List[String] = {

    val dc = DatabaseConfig.forConfig[H2Profile]("h2mem_dc")
    val db = dc.db.asInstanceOf[Database]

    val tables =
      new axis.fifth.output.database.slick.dao.dbdimensions.Tables[H2Profile](dc.profile)

    init(db, tables)

    try {

      import tables.profile.api._
      import tables.uses

      val result = MutableList[String]()

      def outputKinds(_u: Unit) = {
        val joinQuery = for {
          u <- uses
          o <- u.output if u.outputID === o.id
          k <- o.kind if k.databaseID === X
        } yield (u.use, k.kind)

        db.run(joinQuery.result)
      }

      def databaseOutputs(_u: Unit) = {
        val joinQuery = for {
          u <- uses
          o <- u.output if u.outputID === o.id
          k <- o.kind if o.kindID === k.id && k.databaseID =!= X
          db <- k.database if k.databaseID === db.id && db.slickProfileID =!= X
          sp <- db.slickProfile if db.slickProfileID === sp.id
        } yield (u.use, k.kind, sp.profile, sp.plainSQL)

        db.run(joinQuery.result)
      }

      val noop: DBIO[Unit] = DBIO.seq()

      import dimensions.Dimension.Use.{ output$ }
      import dimensions.Dimension.Use.Output.{ database$ }
      import dimensions.Dimension.Use.Output.Database.{ slick$ }

      Await.result(db
        .run(noop)
        .flatMap(outputKinds(_)).map(
          _.foreach { it => result += s"use: ${it._1} == ${output$(it._2)}" }
        )
        .flatMap(databaseOutputs(_)).map(
          _.foreach { it => result += s"use: ${it._1} == ${output$(it._2)} == ${output$(database$(slick$(it._3, it._4)))}" }
        )
        ,Duration.Inf
      )

      result.toList

    } finally {
      drop(db, tables)
      db.close()
    }

  }

  "Uses table" should "contain all" in {

    Synchronized(X) {

      import dimensions.Dimension.Use.{
        H2SlickDatabaseOutput, H2PlainSQLSlickDatabaseOutput,
        SQLiteSlickDatabaseOutput, SQLitePlainSQLSlickDatabaseOutput
      }
      import dimensions.Dimension.Use.Output.{ Console, Buffered }
      import dimensions.Dimension.Use.Output.Database.Slick.Profile.{ H2, SQLite }
      import dimensions.Dimension.Use.{ output$ }
      import dimensions.Dimension.Use.Output.{ database$ }
      import dimensions.Dimension.Use.Output.Database.{ slick$ }

      val ls = List(
        output$(Console),
        output$(Buffered)
      ).map { it => s"use: $it == $it" }
      .concat(List(
        (H2SlickDatabaseOutput, output$(database$(slick$(H2, false)))),
        (H2PlainSQLSlickDatabaseOutput, output$(database$(slick$(H2, true)))),
        (SQLiteSlickDatabaseOutput, output$(database$(slick$(SQLite, false)))),
        (SQLitePlainSQLSlickDatabaseOutput, output$(database$(slick$(SQLite, true))))
      ).map { it => s"use: ${it._1} == ${it._2} == ${it._2}" }
      )

      assert(ls.sorted == this().sorted)

    }

  }

}
