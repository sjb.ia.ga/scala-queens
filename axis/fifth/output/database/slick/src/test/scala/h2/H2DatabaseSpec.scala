package queens
package axis.fifth.output
package database.slick.h2

import scala.concurrent.ExecutionContext.Implicits._

import org.scalatest.flatspec.AnyFlatSpec

import _root_.slick.basic.DatabaseConfig
import _root_.slick.jdbc.H2Profile
import _root_.slick.jdbc.H2Profile.backend.Database

import axis.fifth.output.database.slick.dao.Tables


class H2DatabaseSpec extends AnyFlatSpec {

  import database.slick.dao.dbdimensions.X

  def apply(): (Database, Tables[H2Profile]) = {

    val dc = DatabaseConfig.forConfig[H2Profile]("h2mem_dc")
    val db = dc.db.asInstanceOf[Database]

    val tables =
      new Tables[H2Profile](
        dc.profile,
        new axis.fifth.output.database.slick.dao.dbdimensions.Tables[H2Profile](dc.profile)
      )

    (db, tables)

  }

  "create tables plain SQL" should "NOT throw exception" in {

    Synchronized(X) {

      val (db, tables) = this()

      import tables.profile.api._

      var result = true

      try {
        init(db, tables, true)
      } catch {
        case _ => result = false
      } finally {
        try { drop(db, tables, true) } catch { case _ => }
        db.close()
      }

      assert(result)

    }

  }


  "drop tables plain SQL" should "NOT throw exception" in {

    Synchronized(X) {

      val (db, tables) = this()

      import tables.profile.api._

      var result = true

      init(db, tables, true)

      try {
        drop(db, tables, true)
      } catch {
        case _ => result = false
      } finally {
        db.close()
      }

      assert(result)

    }

  }

  "create tables" should "NOT throw exception" in {

    Synchronized(X) {

      val (db, tables) = this()

      import tables.profile.api._

      var result = true

      try {
        init(db, tables)
      } catch {
        case _ => result = false
      } finally {
        try { drop(db, tables) } catch { case _ => }
        db.close()
      }

      assert(result)

    }

  }


  "drop tables" should "NOT throw exception" in {

    Synchronized(X) {

      val (db, tables) = this()

      import tables.profile.api._

      var result = true

      init(db, tables)

      try {
        drop(db, tables)
      } catch {
        case _ => result = false
      } finally {
        db.close()
      }

      assert(result)

    }

  }

}
