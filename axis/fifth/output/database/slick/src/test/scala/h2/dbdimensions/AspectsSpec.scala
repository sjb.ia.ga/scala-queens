package queens
package axis.fifth.output
package database.slick
package h2.dbdimensions

import scala.collection.mutable.{ ListBuffer => MutableList }

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits._

import org.scalatest.flatspec.AnyFlatSpec

import _root_.slick.basic.DatabaseConfig
import _root_.slick.jdbc.H2Profile
import _root_.slick.jdbc.H2Profile.backend.Database

import database.dbdimensions.Mappings._


class AspectsSpec extends AnyFlatSpec {

  import dao.dbdimensions.X

  def apply(): List[String] = {

    val dc = DatabaseConfig.forConfig[H2Profile]("h2mem_dc")
    val db = dc.db.asInstanceOf[Database]

    val tables =
      new axis.fifth.output.database.slick.dao.dbdimensions.Tables[H2Profile](dc.profile)

    init(db, tables)

    try {

      import tables.profile.api._
      import tables.aspects

      val result = MutableList[String]()

      def classicLooks(_u: Unit) = {
        val joinQuery = for {
          a <- aspects if a.classicID =!= X
          c <- a.classic if a.classicID === c.id
          l <- c.look
        } yield (a.aspect, l.look)

        db.run(joinQuery.result)
      }

      def stepperNexts(_u: Unit) = {
        val joinQuery = for {
          a <- aspects if a.stepperID =!= X
          s <- a.stepper if a.stepperID === s.id
          n <- s.next
        } yield (a.aspect, n.next)

        db.run(joinQuery.result)
      }

      val noop: DBIO[Unit] = DBIO.seq()

      import dimensions.Dimension.Aspect.{ classic$, stepper$ }

      Await.result(db
        .run(noop)
        .flatMap(classicLooks(_)).map(
          _.foreach { it => result += s"aspect: ${it._1} == ${classic$(it._2)}" }
        )
        .flatMap(stepperNexts(_)).map(
          _.foreach { it => result += s"aspect: ${it._1} == ${stepper$(it._2)}" }
        )
        ,Duration.Inf
      )

      result.toList

    } finally {
      drop(db, tables)
      db.close()
    }

  }

  "Aspects table" should "contain all" in {

    Synchronized(X) {

      import dimensions.Dimension.Aspect.Classic.{ Straight, Callback }
      import dimensions.Dimension.Aspect.Stepper.{ OneIncrement, Permutations }
      import dimensions.Dimension.Aspect.{ classic$, stepper$ }

      val ls = List(
        classic$(Straight),
        classic$(Callback),
        stepper$(OneIncrement),
        stepper$(Permutations)
      ).map { it => s"aspect: $it == $it" }

      assert(ls.sorted == this().sorted)

    }

  }

}
