package queens
package axis.fifth.output
package database.slick
package h2.dbdimensions

import scala.collection.mutable.{ ListBuffer => MutableList }

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits._

import org.scalatest.flatspec.AnyFlatSpec

import slick.jdbc.H2Profile.api._

import _root_.slick.basic.DatabaseConfig
import _root_.slick.jdbc.H2Profile
import _root_.slick.jdbc.H2Profile.backend.Database

import database.dbdimensions.Mappings._


class InterfacesSpec extends AnyFlatSpec {

  import dao.dbdimensions.X

  def apply(): List[String] = {

    val dc = DatabaseConfig.forConfig[H2Profile]("h2mem_dc")
    val db = dc.db.asInstanceOf[Database]

    val tables =
      new axis.fifth.output.database.slick.dao.dbdimensions.Tables[H2Profile](dc.profile)

    init(db, tables)

    try {

      import tables.profile.api._
      import tables.interfaces

      val result = MutableList[String]()

      def programStyles(_u: Unit) = {
        val joinQuery = for {
          i <- interfaces
          p <- i.program if i.programID === p.id
          s <- p.style
        } yield (i.interface, s.style)

        db.run(joinQuery.result)
      }

      val noop: DBIO[Unit] = DBIO.seq()

      import dimensions.Dimension.Interface.{ program$ }

      Await.result(db
        .run(noop)
        .flatMap(programStyles(_)).map(
          _.foreach { it => result += s"interface: ${it._1} == ${program$(it._2)}" }
        )
        ,Duration.Inf
      )

      result.toList

    } finally {
      drop(db, tables)
      db.close()
    }

  }

  "Interfaces table" should "contain all" in {

    Synchronized(X) {

      import dimensions.Dimension.Interface.Program.{ Monad, Simple }
      import dimensions.Dimension.Interface.{ program$ }

      val ls = List(
        "",
        program$(Monad),
        program$(Simple)
      ).map { it => s"interface: $it == $it" }.tail

      assert(ls.sorted == this().sorted)
    }

  }

}
