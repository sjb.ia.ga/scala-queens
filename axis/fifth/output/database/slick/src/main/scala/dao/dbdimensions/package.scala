package queens
package axis.fifth.output
package database.slick
package dao

import scala.concurrent.{ ExecutionContext, Future, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile
import _root_.slick.jdbc.JdbcBackend.backend.Database

import database.Implicits._


package object dbdimensions {

  val X = 0L


  def init[P <: JdbcProfile](
    db: Database,
    tables: Tables[P]
  )(implicit
    ec: ExecutionContext
  ): Unit = {

    import tables.profile.api._

    def init_aspects(_u: Unit): Future[Unit] = {

      import dimensions.Dimension.Aspect.Classic.{ Straight, Callback }
      import dimensions.Dimension.Aspect.Stepper.{ OneIncrement, Permutations }
      import dimensions.Dimension.Aspect.{ classic$, stepper$ }

      import database.dbdimensions.Mappings.Classic._
      import database.dbdimensions.Mappings.Stepper._

      import tables.{ looks, classics, nexts, steppers, aspects }

      val obls = List(Straight, Callback).map(_.toString)
      val obns = List(OneIncrement, Permutations).map(_.toString)

      var id: Long = -1
      def auto = Some{id+=1;id}

      val setupAction: DBIO[Unit] = DBIO.seq(
        (looks.schema ++ classics.schema ++ nexts.schema ++ steppers.schema ++ aspects.schema).create
      )

      db.run(setupAction)
        .flatMap { _ =>
          val insertAction: (DBIO[Unit], DBIO[Unit]) = (
            {
              id = 0
              DBIO.seq(
                looks += DbLook("", X.?),
                classics += (X, X),
                looks ++= obls.map(DbLook(_, auto))
              )
            },

            {
              id = 0
              DBIO.seq(
                nexts += DbNext("", X.?),
                steppers += (X, X),
                nexts ++= obns.map(DbNext(_, auto))
              )
            }
          )

          db.run(insertAction._1 andThen insertAction._2)
        }.flatMap { _ =>
          val allLooks: DBIO[Seq[DbLook]] =
            looks.filter(_.id =!= X).result

          db.run(allLooks)
        }.flatMap { ls =>
          id = X
          val insertAction: DBIO[Unit] = DBIO.seq(
            classics ++= ls.map { it => (auto, it.id) }
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val allNexts: DBIO[Seq[DbNext]] =
            nexts.filter(_.id =!= X).result

          db.run(allNexts)
        }.flatMap { ns =>
          id = X
          val insertAction: DBIO[Unit] = DBIO.seq(
            steppers ++= ns.map { it => (auto, it.id) }
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val joinQuery: Query[(Rep[String], Rep[Long], Rep[Long]), (String, Long, Long), Seq] = for {
            c <- classics if c.id =!= X
            cl <- c.look
          } yield (cl.look, cl.id, c.id)

          db.run(joinQuery.result)
        }.map(
          _.map { it => DbClassic(DbLook(it._1, it._2.?), it._2.?, it._3.?) }
        ).flatMap { cs =>
          id = 0 // both CLASSIC & STEPPER
          val insertAction: DBIO[Unit] = DBIO.seq(
            aspects ++= cs.map { it => (auto, classic$(it.look.look), it.id, X) }
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val joinQuery: Query[(Rep[String], Rep[Long], Rep[Long]), (String, Long, Long), Seq] = for {
            s <- steppers if s.id =!= X
            sn <- s.next
          } yield (sn.next, sn.id, s.id)

          db.run(joinQuery.result)
        }.map(
          _.map { it => DbStepper(DbNext(it._1, it._2.?), it._2.?, it._3.?) }
        ).flatMap { ss =>
          val insertAction: DBIO[Unit] = DBIO.seq(
            aspects ++= ss.map { it => (auto, stepper$(it.next.next), X, it.id) }
          )

          db.run(insertAction)
        }

    }


    def init_algorithms(_u: Unit): Future[Unit] = {

      import dimensions.Dimension.Algorithm.{ Iterative, iterative } // FIXME: Iterative
      import dimensions.Dimension.Algorithm.Recursive.{ Native, Extern }
      import dimensions.Dimension.Algorithm.{ recursive$ }

      import database.dbdimensions.Mappings.Recursive._

      import tables.{ natures, recursives, algorithms }

      val obrs = List(Native, Extern).map(_.toString)
      val obas = List(iterative).map(_.toString) // FIXME: Iterative

      var id: Long = -1
      def auto = Some{id+=1;id}

      val setupAction: DBIO[Unit] = DBIO.seq(
        (natures.schema ++ recursives.schema ++ algorithms.schema).create
      )

      db.run(setupAction)
        .flatMap { _ =>
          id = 0
          val insertAction: DBIO[Unit] = DBIO.seq(
            natures += DbNature("", X.?),
            recursives += (X, X),
            natures ++= obrs.map(DbNature(_, auto)),
          )

          db.run(insertAction)
        }.flatMap { _ =>
          id = 0
          val insertAction: DBIO[Unit] = DBIO.seq(
            algorithms ++= obas.map((auto, _, X)),
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val allNatures: DBIO[Seq[DbNature]] =
            natures.filter(_.id =!= X).result

          db.run(allNatures)
        }.flatMap { rs =>
          id = X
          val insertAction: DBIO[Unit] = DBIO.seq(
            recursives ++= rs.map { it => (auto, it.id) }
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val joinQuery: Query[(Rep[String], Rep[Long], Rep[Long]), (String, Long, Long), Seq] = for {
            r <- recursives if r.id =!= X
            rn <- r.nature
          } yield (rn.nature, rn.id, r.id)

          db.run(joinQuery.result)
        }.map(
          _.map { it => DbRecursive(DbNature(it._1, it._2.?), it._2.?, it._3.?) }
        ).flatMap { rs =>
          id = obas.length
          val insertAction: DBIO[Unit] = DBIO.seq(
            algorithms ++= rs.map { it => (auto, recursive$(it.nature.nature), it.id) }
          )

          db.run(insertAction)
        }

    }


    def init_models(_u: Unit): Future[Unit] = {

      import dimensions.Dimension.Model.{ Flow, flow } // FIXME: Flow
      import dimensions.Dimension.Model.Parallel.{ Actors, Futures }
      import dimensions.Dimension.Model.{ parallel$ }

      import database.dbdimensions.Mappings.Parallel._

      import tables.{ paradigms, parallels, models }

      val obps = List(Actors, Futures).map(_.toString)
      val obms = List(flow).map(_.toString) // FIXME: Flow

      var id: Long = -1
      def auto = Some{id+=1;id}

      val setupAction: DBIO[Unit] = DBIO.seq(
        (paradigms.schema ++ parallels.schema ++ models.schema).create
      )

      db.run(setupAction)
        .flatMap { _ =>
          id = 0
          val insertAction: DBIO[Unit] = DBIO.seq(
            paradigms += DbParadigm("", X.?),
            parallels += (X, X),
            paradigms ++= obps.map(DbParadigm(_, auto)),
          )

          db.run(insertAction)
        }.flatMap { _ =>
          id = 0
          val insertAction: DBIO[Unit] = DBIO.seq(
            models ++= obms.map((auto, _, X)),
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val allParadigms: DBIO[Seq[DbParadigm]] =
            paradigms.filter(_.id =!= X).result

          db.run(allParadigms)
        }.flatMap { ps =>
          id = X
          val insertAction: DBIO[Unit] = DBIO.seq(
            parallels ++= ps.map { it => (auto, it.id) }
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val joinQuery: Query[(Rep[String], Rep[Long], Rep[Long]), (String, Long, Long), Seq] = for {
            p <- parallels if p.id =!= X
            pp <- p.paradigm
          } yield (pp.paradigm, pp.id, p.id)

          db.run(joinQuery.result)
        }.map(
          _.map { it => DbParallel(DbParadigm(it._1, it._2.?), it._2.?, it._3.?) }
        ).flatMap { ps =>
          id = obms.length
          val insertAction: DBIO[Unit] = DBIO.seq(
            models ++= ps.map { it => (auto, parallel$(it.paradigm.paradigm), it.id) }
          )

          db.run(insertAction)
        }

    }


    def init_interfaces(_u: Unit): Future[Unit] = {

      import dimensions.Dimension.Interface.Program.{ Monad, Simple }
      import dimensions.Dimension.Interface.{ program$ }

      import database.dbdimensions.Mappings.Program._

      import tables.{ styles, programs, interfaces }

      val obss = List("", Monad, Simple).map(_.toString).tail

      var id: Long = -1
      def auto = Some{id+=1;id}

      val setupAction: DBIO[Unit] = DBIO.seq(
        (styles.schema ++ programs.schema ++ interfaces.schema).create
      )

      db.run(setupAction)
        .flatMap { _ =>
          id = 0
          val insertAction: DBIO[Unit] = DBIO.seq(
            styles ++= obss.map(DbStyle(_, auto))
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val allStyles: DBIO[Seq[DbStyle]] =
            styles.result

          db.run(allStyles)
        }.flatMap { ss =>
          id = 0
          val insertAction: DBIO[Unit] = DBIO.seq(
            programs ++= ss.map { it => (auto, it.id) }
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val joinQuery: Query[(Rep[DbStyle], Rep[Long]), (DbStyle, Long), Seq] = for {
            ps <- programs
            s <- ps.style
          } yield (s, ps.id)

          db.run(joinQuery.result)
        }.map(
          _.map { it => DbProgram(it._1, it._1.id, it._2.?) }
        ).flatMap { ps =>
          id = 0
          val insertAction: DBIO[Unit] = DBIO.seq(
            interfaces ++= ps.map { it => (auto, program$(it.style.style), it.id) }
          )

          db.run(insertAction)
        }

    }


    def init_uses(_u: Unit): Future[Unit] = {

      import dimensions.Dimension.Use.Output.{ Buffered, Console }
      import dimensions.Dimension.Use.Output.Database.Slick
      import dimensions.Dimension.Use.Output.Database.Scalike
      import dimensions.Dimension.Use.{ output$ }
      import dimensions.Dimension.Use.Output.{ database$ }
      import dimensions.Dimension.Use.Output.Database.{ slick$, scalike$ }

      import database.dbdimensions.Mappings.Output._
      import database.dbdimensions.Mappings.Output.Database._
      import database.dbdimensions.Mappings.Output.Database.Slick._
      import database.dbdimensions.Mappings.Output.Database.Scalike._

      import tables.{ slickProfiles, scalikeDrivers, databases, kinds, outputs, uses }

      val obps = List("", Slick.Profile.H2, Slick.Profile.SQLite).map(_.toString).tail
      val obds = List("", Scalike.Driver.H2, Scalike.Driver.SQLite).map(_.toString).tail
      val obks = List("", Buffered, Console).map(_.toString).tail

      var id: Long = -1
      def auto = Some{id+=1;id}

      val setupAction: DBIO[Unit] = DBIO.seq(
        (slickProfiles.schema ++ scalikeDrivers.schema ++ databases.schema ++ kinds.schema ++ outputs.schema ++ uses.schema).create
      )

      db.run(setupAction)
        .flatMap { _ =>
          id = 0
          val insertAction: DBIO[Unit] = DBIO.seq(
            slickProfiles += DbSlickProfile("", false, X.?),

            slickProfiles ++= (obps.flatMap(List.fill(2)(_) zip List(false, true)))
              .map { case (it, plainSQL) => DbSlickProfile(it, plainSQL, auto) }
          )

          db.run(insertAction)
        }.flatMap { _ =>
          id = 0
          val insertAction: DBIO[Unit] = DBIO.seq(
            scalikeDrivers += DbScalikeDriver("", false, X.?),

            scalikeDrivers ++= (obds.flatMap(List.fill(2)(_) zip List(false, true)))
              .map { case (it, plainSQL) => DbScalikeDriver(it, plainSQL, auto) }
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val insertAction: DBIO[Unit] = DBIO.seq(
            databases += (X, "", X, X),
          )

          db.run(insertAction)
        }.flatMap { _ =>
          id = 0
          val insertAction: DBIO[Unit] = DBIO.seq(
            kinds ++= obks.map(DbKind(_, X.?, auto))
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val allSlickProfiles: DBIO[Seq[DbSlickProfile]] =
            slickProfiles.filter(_.id =!= X).result

          db.run(allSlickProfiles)
        }.flatMap { ps =>
          id = X // both SLICK & SCALIKEJDBC
          val ds = ps.map { it => DbDatabase(slick$(it.profile, it.plainSQL), Some(it), it.id, None, None, auto) }
          val insertAction: DBIO[Unit] = DBIO.seq(
            databases ++= ds.map { it => (it.id, it.storage, it.slickProfile.get.id, X) }
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val allScalikeDrivers: DBIO[Seq[DbScalikeDriver]] =
            scalikeDrivers.filter(_.id =!= X).result

          db.run(allScalikeDrivers)
        }.flatMap { ps =>
          val ds = ps.map { it => DbDatabase(scalike$(it.driver, it.plainSQL), None, None, Some(it), it.id, auto) }
          val insertAction: DBIO[Unit] = DBIO.seq(
            databases ++= ds.map { it => (it.id, it.storage, X, it.scalikeDriver.get.id) }
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val allDatabases: DBIO[Seq[(Long, String, Long, Long)]] =
            databases.filter(_.id =!= X).result

          db.run(allDatabases)
        }.flatMap { ds =>
          id = obks.length
          val insertAction: DBIO[Unit] = DBIO.seq(
            kinds ++= ds.map { it => DbKind(database$(it._2), it._1.?, auto) }
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val allKinds: DBIO[Seq[DbKind]] =
            kinds.result

          db.run(allKinds)
        }.flatMap { ks =>
          id = 0
          val insertAction: DBIO[Unit] = DBIO.seq(
            outputs ++= ks.map { it => (auto, it.id) }
          )

          db.run(insertAction)
        }.flatMap { _ =>
          val joinQuery: Query[(Rep[DbKind], Rep[Long]), (DbKind, Long), Seq] = for {
            os <- outputs
            k <- os.kind
          } yield (k, os.id)

          db.run(joinQuery.result)
        }.map(
          _.map { it => DbOutput(it._1, it._1.id, it._2.?) }
        ).flatMap { os =>
          id = 0
          val insertAction: DBIO[Unit] = DBIO.seq(
            uses ++= os.map { it => (auto, output$(it.kind.kind), it.id) }
          )

          db.run(insertAction)
        }

    }


    val noop: DBIO[Unit] = DBIO.seq()

    Await.result(
      db.run(noop)
        .flatMap(init_aspects(_))
        .flatMap(init_algorithms(_))
        .flatMap(init_models(_))
        .flatMap(init_interfaces(_))
        .flatMap(init_uses(_))
        ,Duration.Inf
    )

  }


  def drop[P <: JdbcProfile](
    db: Database,
    tables: Tables[P]
  )(implicit
    ec: ExecutionContext
  ): Unit = {

    import tables.profile.api._

    def drop_uses(_u: Unit): Future[Unit] = {

      import tables.{ uses, outputs, kinds, databases, slickProfiles, scalikeDrivers }

      val finalizeAction: DBIO[Unit] = DBIO.seq(
        (uses.schema ++ outputs.schema ++ kinds.schema ++ databases.schema ++ scalikeDrivers.schema ++ slickProfiles.schema).drop
      )

      db.run(finalizeAction)
    }

    def drop_interfaces(_u: Unit): Future[Unit] = {

      import tables.{ styles, programs, interfaces }

      val finalizeAction: DBIO[Unit] = DBIO.seq(
        (interfaces.schema ++ programs.schema ++ styles.schema).drop
      )

      db.run(finalizeAction)
    }

    def drop_models(_u: Unit): Future[Unit] = {

      import tables.{ paradigms, parallels, models }

      val finalizeAction: DBIO[Unit] = DBIO.seq(
        (models.schema ++ parallels.schema ++ paradigms.schema).drop
      )

      db.run(finalizeAction)
    }

    def drop_algorithms(_u: Unit): Future[Unit] = {

      import tables.{ natures, recursives, algorithms }

      val finalizeAction: DBIO[Unit] = DBIO.seq(
        (algorithms.schema ++ recursives.schema ++ natures.schema).drop
      )

      db.run(finalizeAction)
    }

    def drop_aspects(_u: Unit): Future[Unit] = {

      import tables.{ looks, classics, nexts, steppers, aspects }

      val finalizeAction: DBIO[Unit] = DBIO.seq(
        (aspects.schema ++ steppers.schema ++ nexts.schema ++ classics.schema ++ looks.schema).drop
      )

      db.run(finalizeAction)
    }


    val noop: DBIO[Unit] = DBIO.seq()

    Await.result(
      db.run(noop)
        .flatMap(drop_uses(_))
        .flatMap(drop_interfaces(_))
        .flatMap(drop_models(_))
        .flatMap(drop_algorithms(_))
        .flatMap(drop_aspects(_))
        ,Duration.Inf
    )
  }

}
