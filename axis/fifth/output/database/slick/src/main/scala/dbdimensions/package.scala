package queens
package axis.fifth.output
package database.slick


package jdbc {

  import scala.concurrent.ExecutionContext

  import _root_.slick.jdbc.JdbcProfile
  import slick.jdbc.JdbcBackend.backend.Database


  package object dbdimensions {

    def init[P <: JdbcProfile](
      db: Database,
      tables: dao.dbdimensions.Tables[P],
      plainSQL: Boolean = false
    )(implicit
      ec: ExecutionContext
    ): Unit =
      if (plainSQL)
        plainsql.dbdimensions.init(db, tables)
      else
        dao.dbdimensions.init(db, tables)


    def drop[P <: JdbcProfile](
      db: Database,
      tables: dao.dbdimensions.Tables[P],
      plainSQL: Boolean = false
    )(implicit
      ec: ExecutionContext
    ): Unit =
      if (plainSQL)
        plainsql.dbdimensions.drop(db, tables)
      else
        dao.dbdimensions.drop(db, tables)

  }

}


package plainsql {

  import scala.concurrent.{ ExecutionContext, Future, Await }
  import scala.concurrent.duration.Duration

  import _root_.slick.jdbc.JdbcProfile
  import _root_.slick.jdbc.JdbcBackend.backend.Database

  import database.Implicits._


  package object dbdimensions {

    import dao.dbdimensions.X


    def init[
      P <: JdbcProfile
    ](
      db: Database,
      tables: dao.dbdimensions.Tables[P],
      create: Boolean = true
    )(implicit
      ec: ExecutionContext
    ): Unit = {

      import tables.profile.api._

      def init_aspects(_u: Unit): Future[Unit] = {

        import dimensions.Dimension.Aspect.Classic.{ Straight, Callback }
        import dimensions.Dimension.Aspect.Stepper.{ OneIncrement, Permutations }
        import dimensions.Dimension.Aspect.{ classic$, stepper$ }

        val obls = List(Straight, Callback).map(_.toString)
        val obns = List(OneIncrement, Permutations).map(_.toString)

        var id: Long = -1
        def auto = {id+=1;id}

        val setupAction = if (!create) DBIO.seq() else DBIO.seq(
          sqlu"""create table "LOOKS"(
                   "ID" int primary key,
                   "LOOK" varchar
                 )
              """,
          sqlu"""create table "CLASSICS"(
                   "ID" int primary key,
                   "LOOK_ID" int,
                   constraint "LOOK_FK" foreign key ("LOOK_ID") references "LOOKS"("ID")
                 )
              """,
          sqlu"""create table "NEXTS"(
                   "ID" int primary key,
                   "NEXT" varchar
                 )
              """,
          sqlu"""create table "STEPPERS"(
                   "ID" int primary key,
                   "NEXT_ID" int,
                   constraint "NEXT_FK" foreign key ("NEXT_ID") references "NEXTS"("ID")
                 )
              """,
          sqlu"""create table "ASPECTS"(
                   "ID" int primary key,
                   "ASPECT" varchar,
                   "CLASSIC_ID" int,
                   "STEPPER_ID" int,
                   constraint "CLASSIC_FK" foreign key ("CLASSIC_ID") references "CLASSICS"("ID"),
                   constraint "STEPPER_FK" foreign key ("STEPPER_ID") references "STEPPERS"("ID")
                 )
              """
        )

        db.run(setupAction)
          .flatMap { _ =>
            val insertAction = (
              DBIO.seq(
                sqlu"""insert into "LOOKS"("ID", "LOOK") values ($X, '')""",
                sqlu"""insert into "CLASSICS"("ID", "LOOK_ID") values ($X, $X)""",
                sqlu"""insert into "NEXTS"("ID", "NEXT") values ($X, '')""",
                sqlu"""insert into "STEPPERS"("ID", "NEXT_ID") values ($X, $X)"""
              ),
              {
                id = 0
                DBIO.seq( obls.map { it =>
                  sqlu"""insert into "LOOKS"("ID", "LOOK") values ($auto, $it)"""
                }: _*)
              },
              {
                id = 0
                DBIO.seq( obns.map { it =>
                  sqlu"""insert into "NEXTS"("ID", "NEXT") values ($auto, $it)"""
                }: _*)
              }
            )

            db.run(insertAction._1 andThen insertAction._2 andThen insertAction._3)
          }.flatMap { _ =>
            val allLooks =
              sql"""select "ID" from "LOOKS" where not "ID" = $X""".as[Int]

            db.run(allLooks)
          }.flatMap { ls =>
            id = X
            val insertAction = DBIO.seq( ls.map { it =>
              sqlu"""insert into "CLASSICS"("ID", "LOOK_ID") values ($auto, $it)"""
            }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val allNexts =
              sql"""select "ID" from "NEXTS" where not "ID" = $X""".as[Int]

            db.run(allNexts)
          }.flatMap { ls =>
            id = X
            val insertAction = DBIO.seq( ls.map { it =>
              sqlu"""insert into "STEPPERS"("ID", "NEXT_ID") values ($auto, $it)"""
            }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val joinQuery =
              sql"""select l."LOOK", c."ID" from "CLASSICS" c join "LOOKS" l on c."LOOK_ID" = l."ID" where not c."ID" = $X
                 """.as[(String, Long)]

            db.run(joinQuery)
          }.flatMap { ls =>
            id = 0 // both CLASSIC & STEPPER
            val insertAction = DBIO.seq( ls.map { case (it, cid) =>
              sqlu"""insert into "ASPECTS"("ID", "ASPECT", "CLASSIC_ID", "STEPPER_ID") values ($auto, ${classic$(it)}, $cid, $X)"""
            }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val joinQuery =
              sql"""select n."NEXT", s."ID" from "STEPPERS" s join "NEXTS" n on s."NEXT_ID" = n."ID" where not s."ID" = $X
                 """.as[(String, Long)]

            db.run(joinQuery)
          }.flatMap { ls =>
            val insertAction = DBIO.seq( ls.map { case (it, sid) =>
              sqlu"""insert into "ASPECTS"("ID", "ASPECT", "STEPPER_ID", "CLASSIC_ID") values ($auto, ${stepper$(it)}, $sid, $X)"""
            }: _*)

            db.run(insertAction)
          }

      }


      def init_algorithms(_u: Unit): Future[Unit] = {

        import dimensions.Dimension.Algorithm.{ Iterative, iterative } // FIXME: Iterative
        import dimensions.Dimension.Algorithm.Recursive.{ Native, Extern }
        import dimensions.Dimension.Algorithm.{ recursive$ }

        val obrs = List(Native, Extern).map(_.toString)
        val obas = List(iterative).map(_.toString) // FIXME: Iterative

        var id: Long = -1
        def auto = Some{id+=1;id}

        val setupAction = if (!create) DBIO.seq() else DBIO.seq(
          sqlu"""create table "NATURES"(
                   "ID" int primary key,
                   "NATURE" varchar
                 )
              """,
          sqlu"""create table "RECURSIVES"(
                   "ID" int primary key,
                   "NATURE_ID" int,
                   constraint "NATURE_FK" foreign key ("NATURE_ID") references "NATURES"("ID")
                 )
              """,
          sqlu"""create table "ALGORITHMS"(
                   "ID" int primary key,
                   "ALGORITHM" varchar,
                   "RECURSIVE_ID" int,
                   constraint "RECURSIVE_FK" foreign key ("RECURSIVE_ID") references "RECURSIVES"("ID")
                 )
              """
        )

        db.run(setupAction)
          .flatMap { _ =>
            val insertAction = (
              DBIO.seq(
                sqlu"""insert into "NATURES"("ID", "NATURE") values ($X, '')""",
                sqlu"""insert into "RECURSIVES"("ID", "NATURE_ID") values ($X, $X)"""
              ),
              {
                id = 0
                DBIO.seq( obrs.map { it =>
                  sqlu"""insert into "NATURES"("ID", "NATURE") values ($auto, $it)"""
                }: _*)
              }
            )

            db.run(insertAction._1 andThen insertAction._2)
          }.flatMap { _ =>
            id = 0
            val insertAction = DBIO.seq( obas.map { it =>
              sqlu"""insert into "ALGORITHMS"("ID", "ALGORITHM", "RECURSIVE_ID") values ($auto, $it, $X)"""
            }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val allNatures =
              sql"""select "ID" from "NATURES" where not "ID" = $X""".as[Int]

            db.run(allNatures)
          }.flatMap { ls =>
            id = X
            val insertAction = DBIO.seq( ls.map { it =>
              sqlu"""insert into "RECURSIVES"("ID", "NATURE_ID") values ($auto, $it)"""
            }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val joinQuery =
              sql"""select n."NATURE", r."ID" from "RECURSIVES" r join "NATURES" n on r."NATURE_ID" = n."ID" where not r."ID" = $X
                 """.as[(String, Long)]

            db.run(joinQuery)
          }.flatMap { ls =>
            id = obas.size
            val insertAction = DBIO.seq( ls.map { case (it, rid) =>
              sqlu"""insert into "ALGORITHMS"("ID", "ALGORITHM", "RECURSIVE_ID") values ($auto, ${recursive$(it)}, $rid)"""
            }: _*)

            db.run(insertAction)
          }

      }


      def init_models(_u: Unit): Future[Unit] = {

        import dimensions.Dimension.Model.{ Flow, flow } // FIXME: Flow
        import dimensions.Dimension.Model.Parallel.{ Actors, Futures }
        import dimensions.Dimension.Model.{ parallel$ }

        val obps = List(Actors, Futures).map(_.toString)
        val obms = List(flow).map(_.toString) // FIXME: Flow

        var id: Long = -1
        def auto = Some{id+=1;id}

        val setupAction = if (!create) DBIO.seq() else DBIO.seq(
          sqlu"""create table "PARADIGMS"(
                   "ID" int primary key,
                   "PARADIGM" varchar
                 )
              """,
          sqlu"""create table "PARALLELS"(
                   "ID" int primary key,
                   "PARADIGM_ID" int,
                   constraint "PARADIGM_FK" foreign key ("PARADIGM_ID") references "PARADIGMS"("ID")
                 )
              """,
          sqlu"""create table "MODELS"(
                   "ID" int primary key,
                   "MODEL" varchar,
                   "PARALLEL_ID" int,
                   constraint "PARALLEL_FK" foreign key ("PARALLEL_ID") references "PARALLELS"("ID")
                 )
              """
        )

        db.run(setupAction)
          .flatMap { _ =>
            val insertAction = (
              DBIO.seq(
                sqlu"""insert into "PARADIGMS"("ID", "PARADIGM") values ($X, '')""",
                sqlu"""insert into "PARALLELS"("ID", "PARADIGM_ID") values ($X, $X)"""
              ),
              {
                id = 0
                DBIO.seq( obps.map { it =>
                  sqlu"""insert into "PARADIGMS"("ID", "PARADIGM") values ($auto, $it)"""
                }: _*)
              }
            )

            db.run(insertAction._1 andThen insertAction._2)
          }.flatMap { _ =>
            id = 0
            val insertAction = DBIO.seq( obms.map { it =>
              sqlu"""insert into "MODELS"("ID", "MODEL", "PARALLEL_ID") values ($auto, $it, $X)"""
            }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val allParadigms =
              sql"""select "ID" from "PARADIGMS" where not "ID" = $X""".as[Int]

            db.run(allParadigms)
          }.flatMap { ls =>
            id = X
            val insertAction = DBIO.seq( ls.map { it =>
              sqlu"""insert into "PARALLELS"("ID", "PARADIGM_ID") values ($auto, $it)"""
            }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val joinQuery =
              sql"""select pp."PARADIGM", p."ID" from "PARALLELS" p join "PARADIGMS" pp on p."PARADIGM_ID" = pp."ID" where not p."ID" = $X
                 """.as[(String, Long)]

            db.run(joinQuery)
          }.flatMap { ls =>
            id = obms.size
            val insertAction = DBIO.seq( ls.map { case (it, pid) =>
              sqlu"""insert into "MODELS"("ID", "MODEL", "PARALLEL_ID") values ($auto, ${parallel$(it)}, $pid)"""
            }: _*)

            db.run(insertAction)
          }

      }


      def init_interfaces(_u: Unit): Future[Unit] = {

        import dimensions.Dimension.Interface.Program.{ Monad, Simple }
        import dimensions.Dimension.Interface.{ program$ }

        import database.dbdimensions.Mappings.Program._

        val obss = List("", Monad, Simple).map(_.toString).tail

        var id: Long = -1
        def auto = {id+=1;id}

        val setupAction = if (!create) DBIO.seq() else DBIO.seq(
          sqlu"""create table "STYLES"(
                   "ID" int primary key,
                   "STYLE" varchar
                 )
              """,
          sqlu"""create table "PROGRAMS"(
                   "ID" int primary key,
                   "STYLE_ID" int,
                   constraint "STYLE_FK" foreign key ("STYLE_ID") references "STYLES"("ID")
                 )
              """,
          sqlu"""create table "INTERFACES"(
                   "ID" int primary key,
                   "INTERFACE" varchar,
                   "PROGRAM_ID" int,
                   constraint "PROGRAM_FK" foreign key ("PROGRAM_ID") references "PROGRAMS"("ID")
                 )
              """
        )

        db.run(setupAction)
          .flatMap { _ =>
            id = 0
            val insertAction = DBIO.seq( obss.map { it =>
              sqlu"""insert into "STYLES"("ID", "STYLE") values ($auto, $it)"""
            }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val allStyles =
              sql"""select "ID" from "STYLES"""".as[Int]

            db.run(allStyles)
          }.flatMap { ls =>
            id = 0
            val insertAction = DBIO.seq( ls.map { it =>
              sqlu"""insert into "PROGRAMS"("ID", "STYLE_ID") values ($auto, $it)"""
            }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val joinQuery =
              sql"""select s."STYLE", p."ID" from "PROGRAMS" p join "STYLES" s on p."STYLE_ID" = s."ID"
                 """.as[(String, Long)]

            db.run(joinQuery)
          }.flatMap { ls =>
            id = 0
            val insertAction = DBIO.seq( ls.map { case (it, pid) =>
              sqlu"""insert into "INTERFACES"("ID", "INTERFACE", "PROGRAM_ID") values ($auto, ${program$(it)}, $pid)"""
            }: _*)

            db.run(insertAction)
          }

      }


      def init_uses(_u: Unit): Future[Unit] = {

        import dimensions.Dimension.Use.Output.{ Buffered, Console }
        import dimensions.Dimension.Use.Output.Database.Slick
        import dimensions.Dimension.Use.Output.Database.Scalike
        import dimensions.Dimension.Use.{ output$ }
        import dimensions.Dimension.Use.Output.{ database$ }
        import dimensions.Dimension.Use.Output.Database.{ slick$, scalike$ }

        val obps = List("", Slick.Profile.H2, Slick.Profile.SQLite).map(_.toString).tail
        val obds = List("", Scalike.Driver.H2, Scalike.Driver.SQLite).map(_.toString).tail
        val obks = List("", Buffered, Console).map(_.toString).tail

        var id: Long = -1
        def auto = {id+=1;id}

        val setupAction = if (!create) DBIO.seq() else DBIO.seq(
          sqlu"""create table "SLICK_PROFILES"(
                   "ID" int primary key,
                   "PROFILE" varchar,
                   "PLAIN_SQL" bool
                 )
              """,
          sqlu"""create table "SCALIKE_DRIVERS"(
                   "ID" int primary key,
                   "DRIVER" varchar,
                   "PLAIN_SQL" bool
                 )
              """,
          sqlu"""create table "DATABASES"(
                   "ID" int primary key,
                   "STORAGE" varchar,
                   "SLICK_PROFILE_ID" int,
                   "SCALIKE_DRIVER_ID" int,
                   constraint "SLICK_PROFILE_FK" foreign key ("SLICK_PROFILE_ID") references "SLICK_PROFILES"("ID"),
                   constraint "SCALIKE_DRIVER_FK" foreign key ("SCALIKE_DRIVER_ID") references "SCALIKE_DRIVERS"("ID")
                 )
              """,
          sqlu"""create table "KINDS"(
                   "ID" int primary key,
                   "KIND" varchar,
                   "DATABASE_ID" int,
                   constraint "DATABASE_FK" foreign key ("DATABASE_ID") references "DATABASES"("ID")
                 )
              """,
          sqlu"""create table "OUTPUTS"(
                   "ID" int primary key,
                   "KIND_ID" int,
                   constraint "KIND_FK" foreign key ("KIND_ID") references "KINDS"("ID")
                 )
              """,
          sqlu"""create table "USES"(
                   "ID" int primary key,
                   "USE" varchar,
                   "OUTPUT_ID" int,
                   constraint "OUTPUT_FK" foreign key ("OUTPUT_ID") references "OUTPUTS"("ID")
                 )
              """
        )

        db.run(setupAction)
          .flatMap { _ =>
            val insertAction = (
              DBIO.seq(
                sqlu"""insert into "SLICK_PROFILES"("ID", "PROFILE", "PLAIN_SQL") values ($X, '', false)""",
                sqlu"""insert into "SCALIKE_DRIVERS"("ID", "DRIVER", "PLAIN_SQL") values ($X, '', false)""",
                sqlu"""insert into "DATABASES"("ID", "STORAGE", "SLICK_PROFILE_ID", "SCALIKE_DRIVER_ID") values ($X, '', $X, $X)"""
              ),
              {
                id = 0
                DBIO.seq((obps.flatMap(List.fill(2)(_) zip List(false, true))).map { case (it, plainSQL) =>
                  sqlu"""insert into "SLICK_PROFILES"("ID", "PROFILE", "PLAIN_SQL") values ($auto, $it, $plainSQL)"""
                }: _*)
              },
              {
                id = 0
                DBIO.seq((obds.flatMap(List.fill(2)(_) zip List(false, true))).map { case (it, plainSQL) =>
                  sqlu"""insert into "SCALIKE_DRIVERS"("ID", "DRIVER", "PLAIN_SQL") values ($auto, $it, $plainSQL)"""
                }: _*)
              }
            )

            db.run(insertAction._1 andThen insertAction._2)
          }.flatMap { _ =>
            id = 0
            val insertAction = DBIO.seq( obks.map { it =>
              sqlu"""insert into "KINDS"("ID", "KIND", "DATABASE_ID") values ($auto, $it, $X)"""
            }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val allSlickProfiles =
              sql"""select * from "SLICK_PROFILES" where not "ID" = $X""".as[(Int, String, Boolean)]

            db.run(allSlickProfiles)
          }.flatMap { ls =>
            id = X // both SLICK & SCALIKEJDBC
            val insertAction = DBIO.seq( ls.map { it =>
              sqlu"""insert into "DATABASES"("ID", "STORAGE", "SLICK_PROFILE_ID", "SCALIKE_DRIVER_ID") values ($auto, ${slick$(it._2, it._3)}, ${it._1}, $X)"""
              }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val allScalikeDrivers =
              sql"""select * from "SCALIKE_DRIVERS" where not "ID" = $X""".as[(Int, String, Boolean)]

            db.run(allScalikeDrivers)
          }.flatMap { ls =>
            val insertAction = DBIO.seq( ls.map { it =>
              sqlu"""insert into "DATABASES"("ID", "STORAGE", "SCALIKE_PROFILE_ID", "SCALIKE_DRIVER_ID") values ($auto, ${scalike$(it._2, it._3)}, $X, ${it._1})"""
              }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val joinQuery =
              sql"""select "STORAGE", "ID" from "DATABASES" where not "ID" = $X""".as[(String, Long)]

            db.run(joinQuery)
          }.flatMap { ls =>
            id = obks.length
            val insertAction = DBIO.seq( ls.map { case (it, did) =>
              sqlu"""insert into "KINDS"("ID", "KIND", "DATABASE_ID") values ($auto, ${database$(it)}, $did)"""
            }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val allKinds =
              sql"""select "ID" from "KINDS"""".as[Int]

            db.run(allKinds)
          }.flatMap { ls =>
            id = 0
            val insertAction = DBIO.seq( ls.map { it =>
              sqlu"""insert into "OUTPUTS"("ID", "KIND_ID") values ($auto, $it)"""
            }: _*)

            db.run(insertAction)
          }.flatMap { _ =>
            val joinQuery =
              sql"""select k."KIND", o."ID" from "OUTPUTS" o join "KINDS" k on o."KIND_ID" = k."ID"
                 """.as[(String, Long)]

            db.run(joinQuery)
          }.flatMap { ls =>
            id = 0
            val insertAction = DBIO.seq( ls.map { case (it, oid) =>
              sqlu"""insert into "USES"("ID", "USE", "OUTPUT_ID") values ($auto, ${output$(it)}, $oid)"""
            }: _*)

            db.run(insertAction)
          }

      }


      val noop: DBIO[Unit] = DBIO.seq()

      Await.result(
        db.run(noop)
          .flatMap(init_aspects(_))
          .flatMap(init_algorithms(_))
          .flatMap(init_models(_))
          .flatMap(init_interfaces(_))
          .flatMap(init_uses(_))
          ,Duration.Inf
      )

    }

    def drop[
      P <: JdbcProfile
    ](
      db: Database,
      tables: dao.dbdimensions.Tables[P]
    )(implicit
      ec: ExecutionContext
    ): Unit = {

      import tables.profile.api._

      val finalizeAction = DBIO.seq(
        sqlu"""drop table "USES"""",
        sqlu"""drop table "OUTPUTS"""",
        sqlu"""drop table "KINDS"""",
        sqlu"""drop table "DATABASES"""",
        sqlu"""drop table "SLICK_PROFILES"""",
        sqlu"""drop table "SCALIKE_DRIVERS"""",

        sqlu"""drop table "INTERFACES"""",
        sqlu"""drop table "PROGRAMS"""",
        sqlu"""drop table "STYLES"""",

        sqlu"""drop table "MODELS"""",
        sqlu"""drop table "PARALLELS"""",
        sqlu"""drop table "PARADIGMS"""",

        sqlu"""drop table "ALGORITHMS"""",
        sqlu"""drop table "RECURSIVES"""",
        sqlu"""drop table "NATURES"""",

        sqlu"""drop table "ASPECTS"""",
        sqlu"""drop table "STEPPERS"""",
        sqlu"""drop table "NEXTS"""",
        sqlu"""drop table "CLASSICS"""",
        sqlu"""drop table "LOOKS""""
      )

      Await.result(db.run(finalizeAction), Duration.Inf)

    }

  }

}


package h2 {

  import scala.concurrent.ExecutionContext

  import _root_.slick.jdbc.H2Profile
  import slick.jdbc.H2Profile.backend.Database


  package object dbdimensions {

    import h2.Implicits._


    def init(
      db: Database,
      tables: dao.dbdimensions.Tables[H2Profile],
      plainSQL: Boolean = false
    )(implicit
      ec: ExecutionContext
    ): Unit =
      jdbc.dbdimensions.init(db, tables, plainSQL)


    def drop(
      db: Database,
      tables: dao.dbdimensions.Tables[H2Profile],
      plainSQL: Boolean = false
    )(implicit
      ec: ExecutionContext
    ): Unit =
      jdbc.dbdimensions.drop(db, tables, plainSQL)

  }

}


package sqlite {

  import scala.concurrent.{ ExecutionContext, Future, Await }
  import scala.concurrent.duration.Duration

  import _root_.slick.jdbc.SQLiteProfile
  import slick.jdbc.SQLiteProfile.backend.Database


  package object dbdimensions {

    import sqlite.Implicits._


    def init(
      db: Database,
      tables: dao.dbdimensions.Tables[SQLiteProfile],
      plainSQL: Boolean
    )(implicit
      ec: ExecutionContext
    ): Unit = {

      if (!plainSQL) {
        jdbc.dbdimensions.init(db, tables, plainSQL)
        return
      }

      import tables.profile.api._

      def init_aspects(_u: Unit): Future[Unit] = {

        val setupAction = DBIO.seq(
          sqlu"""create table "LOOKS"(
                   "ID" int primary key,
                   "LOOK" text
                 )
              """,
          sqlu"""create table "CLASSICS"(
                   "ID" int primary key,
                   "LOOK_ID" int,
                   constraint "LOOK_FK" foreign key ("LOOK_ID") references "LOOKS"("ID")
                 )
              """,
          sqlu"""create table "NEXTS"(
                   "ID" int primary key,
                   "NEXT" text
                 )
              """,
          sqlu"""create table "STEPPERS"(
                   "ID" int primary key,
                   "NEXT_ID" int,
                   constraint "NEXT_FK" foreign key ("NEXT_ID") references "NEXTS"("ID")
                 )
              """,
          sqlu"""create table "ASPECTS"(
                   "ID" int primary key,
                   "ASPECT" text,
                   "CLASSIC_ID" int,
                   "STEPPER_ID" int,
                   constraint "CLASSIC_FK" foreign key ("CLASSIC_ID") references "CLASSICS"("ID"),
                   constraint "STEPPER_FK" foreign key ("STEPPER_ID") references "STEPPERS"("ID")
                 )
              """
        )

        db.run(setupAction)
      }


      def init_algorithms(_u: Unit): Future[Unit] = {

        val setupAction = DBIO.seq(
          sqlu"""create table "NATURES"(
                   "ID" int primary key,
                   "NATURE" text
                 )
              """,
          sqlu"""create table "RECURSIVES"(
                   "ID" int primary key,
                   "NATURE_ID" int,
                   constraint "NATURE_FK" foreign key ("NATURE_ID") references "NATURES"("ID")
                 )
              """,
          sqlu"""create table "ALGORITHMS"(
                   "ID" int primary key,
                   "ALGORITHM" text,
                   "RECURSIVE_ID" int,
                   constraint "RECURSIVE_FK" foreign key ("RECURSIVE_ID") references "RECURSIVES"("ID")
                 )
              """
        )

        db.run(setupAction)
      }


      def init_models(_u: Unit): Future[Unit] = {

        val setupAction = DBIO.seq(
          sqlu"""create table "PARADIGMS"(
                   "ID" int primary key,
                   "PARADIGM" text
                 )
              """,
          sqlu"""create table "PARALLELS"(
                   "ID" int primary key,
                   "PARADIGM_ID" int,
                   constraint "PARADIGM_FK" foreign key ("PARADIGM_ID") references "PARADIGMS"("ID")
                 )
              """,
          sqlu"""create table "MODELS"(
                   "ID" int primary key,
                   "MODEL" text,
                   "PARALLEL_ID" int,
                   constraint "PARALLEL_FK" foreign key ("PARALLEL_ID") references "PARALLELS"("ID")
                 )
              """
        )

        db.run(setupAction)
      }


      def init_interfaces(_u: Unit): Future[Unit] = {

        val setupAction = DBIO.seq(
          sqlu"""create table "STYLES"(
                   "ID" int primary key,
                   "STYLE" text
                 )
              """,
          sqlu"""create table "PROGRAMS"(
                   "ID" int primary key,
                   "STYLE_ID" int,
                   constraint "STYLE_FK" foreign key ("STYLE_ID") references "STYLES"("ID")
                 )
              """,
          sqlu"""create table "INTERFACES"(
                   "ID" int primary key,
                   "INTERFACE" text,
                   "PROGRAM_ID" int,
                   constraint "PROGRAM_FK" foreign key ("PROGRAM_ID") references "PROGRAMS"("ID")
                 )
              """
        )

        db.run(setupAction)
      }


      def init_uses(_u: Unit): Future[Unit] = {

        val setupAction = DBIO.seq(
          sqlu"""create table "SLICK_PROFILES"(
                   "ID" int primary key,
                   "PROFILE" text,
                   "PLAIN_SQL" int
                 )
              """,
          sqlu"""create table "SCALIKE_DRIVERS"(
                   "ID" int primary key,
                   "DRIVER" text,
                   "PLAIN_SQL" int
                 )
              """,
          sqlu"""create table "DATABASES"(
                   "ID" int primary key,
                   "STORAGE" text,
                   "SLICK_PROFILE_ID" int,
                   "SCALIKE_DRIVER_ID" int,
                   constraint "SLICK_PROFILE_FK" foreign key ("SLICK_PROFILE_ID") references "SLICK_PROFILES"("ID"),
                   constraint "SCALIKE_DRIVER_FK" foreign key ("SCALIKE_DRIVER_ID") references "SCALIKE_DRIVERS"("ID")
                 )
              """,
          sqlu"""create table "KINDS"(
                   "ID" int primary key,
                   "KIND" text,
                   "DATABASE_ID" int,
                   constraint "DATABASE_FK" foreign key ("DATABASE_ID") references "DATABASES"("ID")
                 )
              """,
          sqlu"""create table "OUTPUTS"(
                   "ID" int primary key,
                   "KIND_ID" int,
                   constraint "KIND_FK" foreign key ("KIND_ID") references "KINDS"("ID")
                 )
              """,
          sqlu"""create table "USES"(
                   "ID" int primary key,
                   "USE" text,
                   "OUTPUT_ID" int,
                   constraint "OUTPUT_FK" foreign key ("OUTPUT_ID") references "OUTPUTS"("ID")
                 )
              """
        )

        db.run(setupAction)
      }


      val noop: DBIO[Unit] = DBIO.seq()

      Await.result(
        db.run(noop)
          .flatMap(init_aspects(_))
          .flatMap(init_algorithms(_))
          .flatMap(init_models(_))
          .flatMap(init_interfaces(_))
          .flatMap(init_uses(_))
          ,Duration.Inf
      )


      plainsql.dbdimensions.init(db, tables, create = false)
    }


    def drop(
      db: Database,
      tables: dao.dbdimensions.Tables[SQLiteProfile],
      plainSQL: Boolean
    )(implicit
      ec: ExecutionContext
    ): Unit =
      jdbc.dbdimensions.drop(db, tables, plainSQL)

  }

}
