package queens
package axis.fifth.output
package database.slick


package object jdbc {

  import scala.concurrent.ExecutionContext

  import _root_.slick.jdbc.JdbcProfile
  import _root_.slick.jdbc.JdbcBackend.backend.Database

  def init[P <: JdbcProfile](
    db: Database,
    tables: dao.Tables[P],
    plainSQL: Boolean = false
  )(implicit
    ec: ExecutionContext
  ): Unit =
    if (plainSQL)
      plainsql.init(db, tables)
    else
      dao.init(db, tables)

  def drop[P <: JdbcProfile](
    db: Database,
    tables: dao.Tables[P],
    plainSQL: Boolean = false
  )(implicit
    ec: ExecutionContext
  ): Unit =
    if (plainSQL)
      plainsql.drop(db, tables)
    else
      dao.drop(db, tables)

}


package object plainsql {

  import scala.concurrent.{ ExecutionContext, Await }
  import scala.concurrent.duration.Duration

  import _root_.slick.jdbc.JdbcProfile
  import _root_.slick.jdbc.JdbcBackend.backend.Database

  def init[P <: JdbcProfile](
    db: Database,
    tables: dao.Tables[P]
  )(implicit
    ec: ExecutionContext
  ): Unit = {

    import tables.profile.api._

    val setupAction = DBIO.seq(
      sqlu"""create table "BOARDS"(
               "ID" int primary key,
               "SIZE" int
             )
          """,
      sqlu"""create table "SQUARES"(
               "BOARD_ID" int,
               "ROW_NO" int,
               "COLUMN" int,
               "IS_FREE" bool,
               constraint "SQUARES_BOARD_FK" foreign key ("BOARD_ID") references "BOARDS"("ID")
             )
          """,
      sqlu"""create table "QUEENS"(
               "ID" int primary key,
               "ASPECT_ID" int,
               "ALGORITHM_ID" int,
               "MODEL_ID" int,
               "INTERFACE_ID" int,
               "USE_ID" int,
               "TAGS" varchar,
               constraint "ASPECT_FK" foreign key ("ASPECT_ID") references "ASPECTS"("ID"),
               constraint "ALGORITHM_FK" foreign key ("ALGORITHM_ID") references "ALGORITHMS"("ID"),
               constraint "MODEL_FK" foreign key ("MODEL_ID") references "MODELS"("ID"),
               constraint "INTERFACE_FK" foreign key ("INTERFACE_ID") references "INTERFACES"("ID"),
               constraint "USE_FK" foreign key ("USE_ID") references "USES"("ID")
             )
          """,
      sqlu"""create table "ROUNDS"(
               "ID" int primary key,
               "BOARD_ID" int,
               "QUEEN_ID" int,
               "TAG" varchar NOT NULL,
               "NUMBER" int,
               constraint "ROUNDS_BOARD_FK" foreign key ("BOARD_ID") references "BOARDS"("ID"),
               constraint "QUEEN_FK" foreign key ("QUEEN_ID") references "QUEENS"("ID")
             )
          """,
      sqlu"""create table "SOLUTIONS"(
               "ID" int primary key,
               "ROUND_ID" int,
               "NUMBER" int,
               constraint "ROUND_FK" foreign key ("ROUND_ID") references "ROUNDS"("ID")
             )
          """,
      sqlu"""create table "SOLUTION_POINTS"(
               "SOLUTION_ID" int,
               "ROW_NO" int,
               "COLUMN" int,
               constraint "SOLUTION_FK" foreign key ("SOLUTION_ID") references "SOLUTIONS"("ID")
             )
          """
    )

    Await.result(db.run(setupAction), Duration.Inf)

  }


  def drop[P <: JdbcProfile](
    db: Database,
    tables: dao.Tables[P]
  )(implicit
    ec: ExecutionContext
  ): Unit = {

    import tables.profile.api._

    val finalizeAction = DBIO.seq(
      sqlu"""drop table "SOLUTION_POINTS"""",
      sqlu"""drop table "SOLUTIONS"""",
      sqlu"""drop table "ROUNDS"""",
      sqlu"""drop table "QUEENS"""",
      sqlu"""drop table "SQUARES"""",
      sqlu"""drop table "BOARDS""""
    )

    Await.result(db.run(finalizeAction), Duration.Inf)

  }

}


package object h2 {

  import scala.concurrent.ExecutionContext

  import _root_.slick.jdbc.H2Profile
  import _root_.slick.jdbc.H2Profile.backend.Database

  object Implicits {

    import _root_.slick.jdbc.JdbcBackend.backend.{ Database => Database2 }

    implicit def db2db(db: Database): Database2 =
      db.asInstanceOf[Database2]

  }


  import Implicits._


  def init(
    db: Database,
    tables: dao.Tables[H2Profile],
    plainSQL: Boolean = false
  )(implicit
    ec: ExecutionContext
  ): Unit = {
    dbdimensions.init(db, tables.d, plainSQL)

    jdbc.init(db, tables, plainSQL)
  }

  def drop(
    db: Database,
    tables: dao.Tables[H2Profile],
    plainSQL: Boolean = false
  )(implicit
    ec: ExecutionContext
  ): Unit = {
    jdbc.drop(db, tables, plainSQL)

    dbdimensions.drop(db, tables.d, plainSQL)
  }

}



package object sqlite {

  import scala.concurrent.{ ExecutionContext, Await }
  import scala.concurrent.duration.Duration

  import _root_.slick.jdbc.SQLiteProfile
  import _root_.slick.jdbc.SQLiteProfile.backend.Database

  object Implicits {

    import _root_.slick.jdbc.JdbcBackend.backend.{ Database => Database2 }

    implicit def db2db(db: Database): Database2 =
      db.asInstanceOf[Database2]

  }


  import Implicits._


  def init(
    db: Database,
    tables: dao.Tables[SQLiteProfile],
    plainSQL: Boolean = false
  )(implicit
    ec: ExecutionContext
  ): Unit = {
    dbdimensions.init(db, tables.d, plainSQL)

    if (!plainSQL) {
      jdbc.init(db, tables, plainSQL)
      return
    }

    import tables.profile.api._

    val setupAction = DBIO.seq(
      sqlu"""create table "BOARDS"(
               "ID" int primary key,
               "SIZE" int
             )
          """,
      sqlu"""create table "SQUARES"(
               "BOARD_ID" int,
               "ROW_NO" int,
               "COLUMN" int,
               "IS_FREE" int,
               constraint "SQUARES_BOARD_FK" foreign key ("BOARD_ID") references "BOARDS"("ID")
             )
          """,
      sqlu"""create table "QUEENS"(
               "ID" int primary key,
               "ASPECT_ID" int,
               "ALGORITHM_ID" int,
               "MODEL_ID" int,
               "INTERFACE_ID" int,
               "USE_ID" int,
               "TAGS" text,
               constraint "ASPECT_FK" foreign key ("ASPECT_ID") references "ASPECTS"("ID"),
               constraint "ALGORITHM_FK" foreign key ("ALGORITHM_ID") references "ALGORITHMS"("ID"),
               constraint "MODEL_FK" foreign key ("MODEL_ID") references "MODELS"("ID"),
               constraint "INTERFACE_FK" foreign key ("INTERFACE_ID") references "INTERFACES"("ID"),
               constraint "USE_FK" foreign key ("USE_ID") references "USES"("ID")
             )
          """,
      sqlu"""create table "ROUNDS"(
               "ID" int primary key,
               "BOARD_ID" int,
               "QUEEN_ID" int,
               "TAG" text NOT NULL,
               "NUMBER" int,
               constraint "ROUNDS_BOARD_FK" foreign key ("BOARD_ID") references "BOARDS"("ID"),
               constraint "QUEEN_FK" foreign key ("QUEEN_ID") references "QUEENS"("ID")
             )
          """,
      sqlu"""create table "SOLUTIONS"(
               "ID" int primary key,
               "ROUND_ID" int,
               "NUMBER" int,
               constraint "ROUND_FK" foreign key ("ROUND_ID") references "ROUNDS"("ID")
             )
          """,
      sqlu"""create table "SOLUTION_POINTS"(
               "SOLUTION_ID" int,
               "ROW_NO" int,
               "COLUMN" int,
               constraint "SOLUTION_FK" foreign key ("SOLUTION_ID") references "SOLUTIONS"("ID")
             )
          """
    )

    Await.result(db.run(setupAction), Duration.Inf)
  }

  def drop(
    db: Database,
    tables: dao.Tables[SQLiteProfile],
    plainSQL: Boolean = false
  )(implicit
    ec: ExecutionContext
  ): Unit = {
    jdbc.drop(db, tables, plainSQL)

    dbdimensions.drop(db, tables.d, plainSQL)
  }

}
