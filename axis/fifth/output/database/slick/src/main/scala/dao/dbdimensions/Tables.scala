package queens
package axis.fifth.output
package database.slick
package dao
package dbdimensions

import slick.jdbc.JdbcProfile
import slick.lifted.{ ProvenShape, ForeignKeyQuery }


final class Tables[P <: JdbcProfile](val profile: P) {

  import profile.api._


  import this.Classic._
  import this.Stepper._

  val looks: TableQuery[Looks] = TableQuery[Looks]

  val classics: TableQuery[Classics] = TableQuery[Classics]

  val nexts: TableQuery[Nexts] = TableQuery[Nexts]

  val steppers: TableQuery[Steppers] = TableQuery[Steppers]

  val aspects: TableQuery[Aspects] = TableQuery[Aspects]


  import this.Recursive._

  val natures: TableQuery[Natures] = TableQuery[Natures]

  val recursives: TableQuery[Recursives] = TableQuery[Recursives]

  val algorithms: TableQuery[Algorithms] = TableQuery[Algorithms]


  import this.Parallel._

  val paradigms: TableQuery[Paradigms] = TableQuery[Paradigms]

  val parallels: TableQuery[Parallels] = TableQuery[Parallels]

  val models: TableQuery[Models] = TableQuery[Models]


  import this.Program._

  val styles: TableQuery[Styles] = TableQuery[Styles]

  val programs: TableQuery[Programs] = TableQuery[Programs]

  val interfaces: TableQuery[Interfaces] = TableQuery[Interfaces]


  import this.Output._
  import this.Output.Database._
  import this.Output.Database.Slick._
  import this.Output.Database.Scalike._

  val slickProfiles: TableQuery[SlickProfiles] = TableQuery[SlickProfiles]

  val scalikeDrivers: TableQuery[ScalikeDrivers] = TableQuery[ScalikeDrivers]

  val databases: TableQuery[Databases] = TableQuery[Databases]

  val kinds: TableQuery[Kinds] = TableQuery[Kinds]

  val outputs: TableQuery[Outputs] = TableQuery[Outputs]

  val uses: TableQuery[Uses] = TableQuery[Uses]


  // aspect

  object Classic {

    import database.dbdimensions.Mappings.Classic._

    class Looks(tag: Tag)
        extends Table[DbLook](tag, "LOOKS") {

      def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
      def look: Rep[String] = column[String]("LOOK")

      def * = (look, id.?).mapTo[DbLook]

    }

    class Classics(tag: Tag)
        extends Table[(Long, Long)](tag, "CLASSICS") {

      def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
      def lookID: Rep[Long] = column[Long]("LOOK_ID")

      def * : ProvenShape[(Long, Long)] =
        (id, lookID)

      def look: ForeignKeyQuery[Looks, DbLook] =
        foreignKey("LOOK_FK", lookID, TableQuery[Looks])(_.id)

    }

  }

  object Stepper {

    import database.dbdimensions.Mappings.Stepper._

    class Nexts(tag: Tag)
        extends Table[DbNext](tag, "NEXTS") {

      def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
      def next: Rep[String] = column[String]("NEXT")

      def * = (next, id.?).mapTo[DbNext]

    }

    class Steppers(tag: Tag)
        extends Table[(Long, Long)](tag, "STEPPERS") {

      def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
      def nextID: Rep[Long] = column[Long]("NEXT_ID")

      def * : ProvenShape[(Long, Long)] =
        (id, nextID)

      def next: ForeignKeyQuery[Nexts, DbNext] =
        foreignKey("NEXT_FK", nextID, TableQuery[Nexts])(_.id)

    }

  }

  class Aspects(tag: Tag)
      extends Table[(Long, String, Long, Long)](tag, "ASPECTS") {

    def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
    def aspect: Rep[String] = column[String]("ASPECT")
    def classicID: Rep[Long] = column[Long]("CLASSIC_ID")
    def stepperID: Rep[Long] = column[Long]("STEPPER_ID")

    def * : ProvenShape[(Long, String, Long, Long)] =
      (id, aspect, classicID, stepperID)

    def classic: ForeignKeyQuery[Classic.Classics, (Long, Long)] =
      foreignKey("CLASSIC_FK", classicID, TableQuery[Classic.Classics])(_.id)

    def stepper: ForeignKeyQuery[Stepper.Steppers, (Long, Long)] =
      foreignKey("STEPPER_FK", stepperID, TableQuery[Stepper.Steppers])(_.id)

  }


  // algorithm

  object Recursive {

    import database.dbdimensions.Mappings.Recursive._

    class Natures(tag: Tag)
        extends Table[DbNature](tag, "NATURES") {

      def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
      def nature: Rep[String] = column[String]("NATURE")

      def * = (nature, id.?).mapTo[DbNature]

    }

    class Recursives(tag: Tag)
        extends Table[(Long, Long)](tag, "RECURSIVES") {

      def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
      def natureID: Rep[Long] = column[Long]("NATURE_ID")

      def * : ProvenShape[(Long, Long)] =
        (id, natureID)

      def nature: ForeignKeyQuery[Natures, DbNature] =
        foreignKey("NATURE_FK", natureID, TableQuery[Natures])(_.id)

    }

  }

  class Algorithms(tag: Tag)
      extends Table[(Long, String, Long)](tag, "ALGORITHMS") {

    def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
    def algorithm: Rep[String] = column[String]("ALGORITHM")
    def recursiveID: Rep[Long] = column[Long]("RECURSIVE_ID")

    def * : ProvenShape[(Long, String, Long)] =
      (id, algorithm, recursiveID)

    def recursive: ForeignKeyQuery[Recursive.Recursives, (Long, Long)] =
      foreignKey("RECURSIVE_FK", recursiveID, TableQuery[Recursive.Recursives])(_.id)

  }


  // model

  object Parallel {

    import database.dbdimensions.Mappings.Parallel._

    class Paradigms(tag: Tag)
        extends Table[DbParadigm](tag, "PARADIGMS") {

      def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
      def paradigm: Rep[String] = column[String]("PARADIGM")

      def * = (paradigm, id.?).mapTo[DbParadigm]

    }

    class Parallels(tag: Tag)
        extends Table[(Long, Long)](tag, "PARALLELS") {

      def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
      def paradigmID: Rep[Long] = column[Long]("PARADIGM_ID")

      def * : ProvenShape[(Long, Long)] =
        (id, paradigmID)

      def paradigm: ForeignKeyQuery[Paradigms, DbParadigm] =
        foreignKey("PARADIGM_FK", paradigmID, TableQuery[Paradigms])(_.id)

    }

  }

  class Models(tag: Tag)
      extends Table[(Long, String, Long)](tag, "MODELS") {

    def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
    def model: Rep[String] = column[String]("MODEL")
    def parallelID: Rep[Long] = column[Long]("PARALLEL_ID")

    def * : ProvenShape[(Long, String, Long)] =
      (id, model, parallelID)

    def parallel: ForeignKeyQuery[Parallel.Parallels, (Long, Long)] =
      foreignKey("PARALLEL_FK", parallelID, TableQuery[Parallel.Parallels])(_.id)

  }


  // interface

  object Program {

    import database.dbdimensions.Mappings.Program._

    class Styles(tag: Tag)
        extends Table[DbStyle](tag, "STYLES") {

      def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
      def style: Rep[String] = column[String]("STYLE")

      def * = (style, id.?).mapTo[DbStyle]

    }

    class Programs(tag: Tag)
        extends Table[(Long, Long)](tag, "PROGRAMS") {

      def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
      def styleID: Rep[Long] = column[Long]("STYLE_ID")

      def * : ProvenShape[(Long, Long)] =
        (id, styleID)

      def style: ForeignKeyQuery[Styles, DbStyle] =
        foreignKey("STYLE_FK", styleID, TableQuery[Styles])(_.id)

    }

  }

  class Interfaces(tag: Tag)
      extends Table[(Long, String, Long)](tag, "INTERFACES") {

    def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
    def interface: Rep[String] = column[String]("INTERFACE")
    def programID: Rep[Long] = column[Long]("PROGRAM_ID")

    def * : ProvenShape[(Long, String, Long)] =
      (id, interface, programID)

    def program: ForeignKeyQuery[Program.Programs, (Long, Long)] =
      foreignKey("PROGRAM_FK", programID, TableQuery[Program.Programs])(_.id)

  }


  // use

  object Output {

    object Database {

      object Slick {

        import database.dbdimensions.Mappings.Output.Database.Slick._

        class SlickProfiles(tag: Tag)
            extends Table[DbSlickProfile](tag, "SLICK_PROFILES") {

          def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
          def profile: Rep[String] = column[String]("PROFILE")
          def plainSQL: Rep[Boolean] = column[Boolean]("PLAIN_SQL")

          def * = (profile, plainSQL, id.?).mapTo[DbSlickProfile]

        }

      }

      object Scalike {

        import database.dbdimensions.Mappings.Output.Database.Scalike._

        class ScalikeDrivers(tag: Tag)
            extends Table[DbScalikeDriver](tag, "SCALIKE_DRIVERS") {

          def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
          def driver: Rep[String] = column[String]("DRIVER")
          def plainSQL: Rep[Boolean] = column[Boolean]("PLAIN_SQL")

          def * = (driver, plainSQL, id.?).mapTo[DbScalikeDriver]

        }

      }

      import database.dbdimensions.Mappings.Output.Database._
      import database.dbdimensions.Mappings.Output.Database.Slick._
      import database.dbdimensions.Mappings.Output.Database.Scalike._

      class Databases(tag: Tag)
        extends Table[(Long, String, Long, Long)](tag, "DATABASES") {

        def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
        def storage: Rep[String] = column[String]("STORAGE")
        def slickProfileID: Rep[Long] = column[Long]("SLICK_PROFILE_ID")
        def scalikeDriverID: Rep[Long] = column[Long]("SCALIKE_DRIVER_ID")

        def * : ProvenShape[(Long, String, Long, Long)] =
          (id, storage, slickProfileID, scalikeDriverID)

        def slickProfile: ForeignKeyQuery[Slick.SlickProfiles, DbSlickProfile] =
          foreignKey("SLICK_PROFILE_FK", slickProfileID, TableQuery[Slick.SlickProfiles])(_.id)

        def scalikeDriver: ForeignKeyQuery[Scalike.ScalikeDrivers, DbScalikeDriver] =
          foreignKey("SCALIKE_DRIVER_FK", scalikeDriverID, TableQuery[Scalike.ScalikeDrivers])(_.id)

      }

    }

    import database.dbdimensions.Mappings.Output._

    class Kinds(tag: Tag)
        extends Table[DbKind](tag, "KINDS") {

      def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
      def kind: Rep[String] = column[String]("KIND")
      def databaseID: Rep[Long] = column[Long]("DATABASE_ID")

      def * = (kind, databaseID.?, id.?).mapTo[DbKind]

      def database: ForeignKeyQuery[Database.Databases, (Long, String, Long, Long)] =
        foreignKey("DATABASE_FK", databaseID, TableQuery[Database.Databases])(_.id)

    }

    class Outputs(tag: Tag)
        extends Table[(Long, Long)](tag, "OUTPUTS") {

      def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
      def kindID: Rep[Long] = column[Long]("KIND_ID")

      def * : ProvenShape[(Long, Long)] =
        (id, kindID)

      def kind: ForeignKeyQuery[Kinds, DbKind] =
        foreignKey("KIND_FK", kindID, TableQuery[Kinds])(_.id)

    }

  }

  class Uses(tag: Tag)
      extends Table[(Long, String, Long)](tag, "USES") {

    def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
    def use: Rep[String] = column[String]("USE")
    def outputID: Rep[Long] = column[Long]("OUTPUT_ID")

    def * : ProvenShape[(Long, String, Long)] =
      (id, use, outputID)

    def output: ForeignKeyQuery[Output.Outputs, (Long, Long)] =
      foreignKey("OUTPUT_FK", outputID, TableQuery[Output.Outputs])(_.id)

  }

}
