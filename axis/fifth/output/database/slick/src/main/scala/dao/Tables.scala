package queens
package axis.fifth.output
package database.slick
package dao

import _root_.slick.jdbc.JdbcProfile
import _root_.slick.lifted.{ ProvenShape, ForeignKeyQuery }

import database.Mappings.DbBoard

import database.Implicits._


final class Tables[P <: JdbcProfile](val profile: P, val d: dbdimensions.Tables[P]) {

  import profile.api._

  import d.{ Aspects, Algorithms, Models, Interfaces, Uses }


  val boards = TableQuery[Boards]

  val squares = TableQuery[Squares]

  val rounds = TableQuery[Rounds]

  val queens = TableQuery[Queens]

  val solutions = TableQuery[Solutions]

  val solutionPoints = TableQuery[SolutionPoints]


  class Boards(tag: Tag)
    extends Table[DbBoard](tag, "BOARDS") {

    def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
    def size: Rep[Int] = column[Int]("SIZE")

    def * = (size, id.?).mapTo[DbBoard]

  }


  class Squares(tag: Tag)
    extends Table[(Long, Int, Int, Boolean)](tag, "SQUARES") {

    def boardID: Rep[Long] = column[Long]("BOARD_ID")
    def row: Rep[Int] = column[Int]("ROW_NO")
    def col: Rep[Int] = column[Int]("COLUMN")
    def isFree: Rep[Boolean] = column[Boolean]("IS_FREE")

    def * : ProvenShape[(Long, Int, Int, Boolean)] =
      (boardID, row, col, isFree)

    def board: ForeignKeyQuery[Boards, DbBoard] =
      foreignKey("SQUARES_BOARD_FK", boardID, TableQuery[Boards])(_.id)

  }


  class Queens(tag: Tag)
    extends Table[(Long, Long, Long, Long, Long, Long, String)](tag, "QUEENS") {

    def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
    def aspectID: Rep[Long] = column[Long]("ASPECT_ID")
    def algorithmID: Rep[Long] = column[Long]("ALGORITHM_ID")
    def modelID: Rep[Long] = column[Long]("MODEL_ID")
    def interfaceID: Rep[Long] = column[Long]("INTERFACE_ID")
    def useID: Rep[Long] = column[Long]("USE_ID")
    def tags: Rep[String] = column[String]("TAGS")

    def * : ProvenShape[(Long, Long, Long, Long, Long, Long, String)] =
      (id,
      aspectID, algorithmID, modelID, interfaceID, useID,
      tags)

    def aspect: ForeignKeyQuery[Aspects, (Long, String, Long, Long)] =
      foreignKey("ASPECT_FK", aspectID, TableQuery[Aspects])(_.id)

    def algorithm: ForeignKeyQuery[Algorithms, (Long, String, Long)] =
      foreignKey("ALGORITHM_FK", algorithmID, TableQuery[Algorithms])(_.id)

    def model: ForeignKeyQuery[Models, (Long, String, Long)] =
      foreignKey("MODEL_FK", modelID, TableQuery[Models])(_.id)

    def interface: ForeignKeyQuery[Interfaces, (Long, String, Long)] =
      foreignKey("INTERFACE_FK", interfaceID, TableQuery[Interfaces])(_.id)

    def use: ForeignKeyQuery[Uses, (Long, String, Long)] =
      foreignKey("USE_FK", useID, TableQuery[Uses])(_.id)

  }


  class Rounds(_tag: Tag)
    extends Table[(Long, Long, Long, String, Long)](_tag, "ROUNDS") {

    def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
    def boardID: Rep[Long] = column[Long]("BOARD_ID")
    def queenID: Rep[Long] = column[Long]("QUEEN_ID")
    def tag: Rep[String] = column[String]("TAG")
    def number: Rep[Long] = column[Long]("NUMBER")

    def * : ProvenShape[(Long, Long, Long, String, Long)] =
      (id, boardID, queenID, tag, number)

    def board: ForeignKeyQuery[Boards, DbBoard] =
      foreignKey("ROUNDS_BOARD_FK", boardID, TableQuery[Boards])(_.id)

    def queen: ForeignKeyQuery[Queens, (Long, Long, Long, Long, Long, Long, String)] =
      foreignKey("QUEEN_FK", queenID, TableQuery[Queens])(_.id)

  }


  class Solutions(tag: Tag)
    extends Table[(Long, Long, Long)](tag, "SOLUTIONS") {

    def id: Rep[Long] = column[Long]("ID", O.PrimaryKey)
    def roundID: Rep[Long] = column[Long]("ROUND_ID")
    def number: Rep[Long] = column[Long]("NUMBER")

    def * : ProvenShape[(Long, Long, Long)] =
      (id, roundID, number)

    def round: ForeignKeyQuery[Rounds, (Long, Long, Long, String, Long)] =
      foreignKey("ROUND_FK", roundID, TableQuery[Rounds])(_.id)

  }


  class SolutionPoints(tag: Tag)
    extends Table[(Long, Int, Int)](tag, "SOLUTION_POINTS") {

    def solutionID: Rep[Long] = column[Long]("SOLUTION_ID")
    def row: Rep[Int] = column[Int]("ROW_NO")
    def col: Rep[Int] = column[Int]("COLUMN")

    def * : ProvenShape[(Long, Int, Int)] =
      (solutionID, row, col)

    def solution: ForeignKeyQuery[Solutions, (Long, Long, Long)] =
      foreignKey("SOLUTION_FK", solutionID, TableQuery[Solutions])(_.id)

  }

}
