package queens
package axis.fifth.output
package database.slick
package dao
package dbdimensions

import scala.concurrent.{ ExecutionContext, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile
import _root_.slick.jdbc.JdbcBackend.backend.Database

import database.Implicits._


object DAO {

  import dbdimensions.X

  object Aspects {

    import database.dbdimensions.Mappings.{ DbAspect, DbClassicAspect, DbStepperAspect }
    import database.dbdimensions.Mappings.Classic._
    import database.dbdimensions.Mappings.Stepper._

    def mapAspect[P <: JdbcProfile](
      id: Long,
      tables: Tables[P]
    )(implicit
      db: Database,
      ec: ExecutionContext
    ): DbAspect = {

      import tables.profile.api._

      import tables.aspects

      var aspect: String = null
      var classicAspect: Option[DbClassicAspect] = None
      var stepperAspect: Option[DbStepperAspect] = None

      val noop: DBIO[Unit] = DBIO.seq()

      Await.result(
        db.run(noop)
          .flatMap { _ =>
            val classicQuery = for {
              at <- aspects if at.id === id && at.stepperID === X
              cl <- at.classic if at.classicID === cl.id
              lk <- cl.look if cl.lookID === lk.id
            } yield (at.aspect, at.id, cl.id, lk.id, lk.look)

            db.run(classicQuery.result)
          }
          .flatMap { it =>
            assert(it.length <= 1)

            if (it.nonEmpty) {
              val (_aspect, aid, cid, lid, look) = it.head
              classicAspect = Some(
                DbClassicAspect(
                  DbClassic(DbLook(look, lid.?)
                           ,lid
                           ,cid.?)
                  ,aid.?
                )
              )

              aspect = _aspect
            }

            val stepperQuery = for {
              at <- aspects if at.id === id && at.classicID === X
              st <- at.stepper if at.stepperID === st.id
              nx <- st.next if st.nextID === nx.id
            } yield (at.aspect, at.id, st.id, nx.id, nx.next)

            db.run(stepperQuery.result)
          }
          .flatMap { it =>
            assert(it.length <= 1)

            if (it.nonEmpty) {
              val (_aspect, aid, sid, nid, next) = it.head
              stepperAspect = Some(
                DbStepperAspect(
                  DbStepper(DbNext(next, nid.?)
                           ,nid
                           ,sid.?)
                  ,aid.?
                )
              )

              aspect = _aspect
            }

            db.run(noop)
          }
          ,Duration.Inf
      )

      assert(
        classicAspect.isEmpty && stepperAspect.nonEmpty ||
        classicAspect.nonEmpty && stepperAspect.isEmpty
      )

      DbAspect(aspect
              ,classicAspect
              ,classicAspect.map(_.id)
              ,stepperAspect
              ,stepperAspect.map(_.id)
              ,id.?
      )
    }

  }


  object Algorithms {

    import database.dbdimensions.Mappings.{ DbAlgorithm, DbRecursiveAlgorithm }
    import database.dbdimensions.Mappings.Recursive._

    def mapAlgorithm[P <: JdbcProfile](
      id: Long,
      tables: Tables[P]
    )(implicit
      db: Database,
      ec: ExecutionContext
    ): DbAlgorithm = {

      import tables.profile.api._

      import tables.algorithms

      var algorithm: String = null
      var recursiveAlgorithm: Option[DbRecursiveAlgorithm] = None

      val noop: DBIO[Unit] = DBIO.seq()

      Await.result(
        db.run(noop)
          .flatMap { _ =>
            val query = for {
              a <- algorithms if a.id === id && a.recursiveID === X
            } yield (a.algorithm)

            db.run(query.result)
          }
          .flatMap { it =>
            assert(it.length <= 1)

            if (it.nonEmpty) {
              algorithm = it.head
            }

            val recursiveQuery = for {
              a <- algorithms if a.id === id && a.recursiveID =!= X
              r <- a.recursive if a.recursiveID === r.id
              n <- r.nature if r.natureID === n.id
            } yield (a.algorithm, r.id, n.id, n.nature)

            db.run(recursiveQuery.result)
          }
          .flatMap { it =>
            assert(it.length <= 1)

            if (it.nonEmpty) {
              val (_algorithm, rid, nid, nature) = it.head

              assert(algorithm eq null)
              algorithm = _algorithm

              recursiveAlgorithm = Some(
                DbRecursiveAlgorithm(
                  DbRecursive(DbNature(nature, nid.?)
                             ,nid
                             ,rid.?)
                  ,id.?
                )
              )
            }

            db.run(noop)
          }
          ,Duration.Inf
      )

      DbAlgorithm(algorithm
                 ,recursiveAlgorithm
                 ,recursiveAlgorithm.map(_.id)
                 ,id.?
      )
    }

  }


  object Models {

    import database.dbdimensions.Mappings.{ DbModel, DbParallelModel }
    import database.dbdimensions.Mappings.Parallel._

    def mapModel[P <: JdbcProfile](
      id: Long,
      tables: Tables[P]
    )(implicit
      db: Database,
      ec: ExecutionContext
    ): DbModel = {

      import tables.profile.api._

      import tables.models

      var model: String = null
      var parallelModel: Option[DbParallelModel] = None

      val noop: DBIO[Unit] = DBIO.seq()

      Await.result(
        db.run(noop)
          .flatMap { _ =>
            val query = for {
              m <- models if m.id === id && m.parallelID === X
            } yield (m.model)

            db.run(query.result)
          }
          .flatMap { it =>
            assert(it.length <= 1)

            if (it.nonEmpty) {
              model = it.head
            }

            val parallelQuery = for {
              m <- models if m.id === id && m.parallelID =!= X
              p <- m.parallel if m.parallelID === p.id
              pp <- p.paradigm if p.paradigmID === pp.id
            } yield (m.model, p.id, pp.id, pp.paradigm)

            db.run(parallelQuery.result)
          }
          .flatMap { it =>
            assert(it.length <= 1)

            if (it.nonEmpty) {
              val (_model, pid, ppid, paradigm) = it.head

              assert(model eq null)
              model = _model

              parallelModel = Some(
                DbParallelModel(
                  DbParallel(DbParadigm(paradigm, ppid.?)
                            ,ppid
                            ,pid.?
                  )
                  ,id.?
                )
              )
            }

            db.run(noop)
          }
          ,Duration.Inf
      )

      DbModel(model
             ,parallelModel
             ,parallelModel.map(_.id)
             ,id.?
      )
    }

  }


  object Interfaces {

    import database.dbdimensions.Mappings.{ DbInterface, DbProgramInterface }
    import database.dbdimensions.Mappings.Program._

    def mapInterface[P <: JdbcProfile](
      id: Long,
      tables: Tables[P]
    )(implicit
      db: Database,
      ec: ExecutionContext
    ): DbInterface = {

      import tables.profile.api._

      import tables.interfaces

      var interface: String = null
      var programInterface: Option[DbProgramInterface] = None

      val noop: DBIO[Unit] = DBIO.seq()

      Await.result(
        db.run(noop)
          .flatMap { _ =>
            val programQuery = for {
              i <- interfaces if i.id === id
              p <- i.program if i.programID === p.id
              s <- p.style if p.styleID === s.id
            } yield (i.interface, p.id, s.id, s.style)

            db.run(programQuery.result)
          }
          .flatMap { it =>
            assert(it.length <= 1)

            if (it.nonEmpty) {
              val (_interface, pid, sid, style) = it.head

              programInterface = Some(
                DbProgramInterface(
                  DbProgram(DbStyle(style, sid.?)
                           ,sid.?
                           ,pid.?
                  )
                  ,id.?
                )
              )

              interface = _interface
            }

            db.run(noop)
          }
          ,Duration.Inf
      )

      assert(programInterface.nonEmpty)

      DbInterface(interface
                 ,programInterface.get
                 ,programInterface.map(_.id).get
                 ,id.?
      )
    }

  }


  object Uses {

    import database.dbdimensions.Mappings.{ DbUse, DbOutputUse, DbDatabaseOutputUse }
    import database.dbdimensions.Mappings.Output._
    import database.dbdimensions.Mappings.Output.Database._
    import database.dbdimensions.Mappings.Output.Database.Slick._
    import database.dbdimensions.Mappings.Output.Database.Scalike._

    def mapUse[P <: JdbcProfile](
      id: Long,
      tables: Tables[P]
    )(implicit
      db: Database,
      ec: ExecutionContext
    ): DbUse = {

      import tables.profile.api._

      import tables.uses

      var use: String = null
      var output: DbOutput = null
      var databaseOutputUse: Option[DbDatabaseOutputUse] = None

      val noop: DBIO[Unit] = DBIO.seq()

      Await.result(
        db.run(noop)
          .flatMap { _ =>
            val outputQuery = for {
              u <- uses if u.id === id
              o <- u.output if u.outputID === o.id
              k <- o.kind if o.kindID === k.id && k.databaseID === X
            } yield (u.use, o.id, k.id, k.kind)

            db.run(outputQuery.result)
          }
          .flatMap { it =>
            assert(it.length <= 1)

            if (it.nonEmpty) {
              val (_use, oid, kid, kind) = it.head
              output = DbOutput(DbKind(kind, X.?, kid.?)
                               ,kid
                               ,oid.?
                       )
              use = _use
            }

            val databaseOutputQuery = for {
              u <- uses if u.id === id
              o <- u.output if u.outputID === o.id
              k <- o.kind if o.kindID === k.id && k.databaseID =!= X
              db <- k.database if k.databaseID === db.id && db.slickProfileID =!= X
              sp <- db.slickProfile if db.slickProfileID === sp.id
            } yield (u.use, o.id, k.id, k.kind, db.id, db.storage, sp.id, sp.profile, sp.plainSQL)

            db.run(databaseOutputQuery.result)
          }
          .flatMap { it =>
            assert(it.length <= 1)

            if (it.nonEmpty) {
              val (_use, oid, kid, kind, dbid, storage, spid, profile, plainSQL) = it.head

              assert(output eq null)
              output = DbOutput(DbKind(kind, dbid.?, kid.?)
                               ,kid
                               ,oid.?
                       )

              databaseOutputUse = Some(
                DbDatabaseOutputUse(
                  DbDatabaseOutput(
                    DbDatabaseKind(
                      DbDatabase(storage
                                ,Some(DbSlickProfile(profile, plainSQL, spid.?))
                                ,spid.?
                                ,None
                                ,None
                                ,dbid.?
                      )
                      ,dbid
                      ,kid.?
                    )
                    ,oid.?
                  )
                  ,id.?
                )
              )

              use = _use
            }

            val databaseOutputQuery = for {
              u <- uses if u.id === id
              o <- u.output if u.outputID === o.id
              k <- o.kind if o.kindID === k.id && k.databaseID =!= X
              db <- k.database if k.databaseID === db.id && db.scalikeDriverID =!= X
              sd <- db.scalikeDriver if db.scalikeDriverID === sd.id
            } yield (u.use, o.id, k.id, k.kind, db.id, db.storage, sd.id, sd.driver, sd.plainSQL)

            db.run(databaseOutputQuery.result)
          }
          .flatMap { it =>
            assert(it.length <= 1)

            if (it.nonEmpty) {
              val (_use, oid, kid, kind, dbid, storage, sdid, driver, plainSQL) = it.head

              assert(output eq null)
              output = DbOutput(DbKind(kind, dbid.?, kid.?)
                               ,kid
                               ,oid.?
                       )

              databaseOutputUse = Some(
                DbDatabaseOutputUse(
                  DbDatabaseOutput(
                    DbDatabaseKind(
                      DbDatabase(storage
                                ,None
                                ,None
                                ,Some(DbScalikeDriver(driver, plainSQL, sdid.?))
                                ,sdid.?
                                ,dbid.?
                      )
                      ,dbid
                      ,kid.?
                    )
                    ,oid.?
                  )
                  ,id.?
                )
              )

              use = _use
            }

            db.run(noop)
          }
          ,Duration.Inf
      )

      assert(output ne null)

      DbUse(use
           ,DbOutputUse(output
                       ,databaseOutputUse
                       ,id.?
           )
           ,databaseOutputUse.map(_.id).get
           ,id.?
      )
    }

  }

}
