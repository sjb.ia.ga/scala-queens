package queens
package axis.fifth.output
package database.slick

import scala.concurrent.{ ExecutionContext, Await }
import scala.concurrent.duration.Duration

import _root_.slick.jdbc.JdbcProfile
import _root_.slick.jdbc.JdbcBackend.backend.Database


package object dao {

  def init[P <: JdbcProfile](
    db: Database,
    tables: Tables[P]
  )(implicit
    ec: ExecutionContext
  ): Unit = {

    import tables.profile.api._

    import tables.{ boards, squares, rounds, queens, solutions, solutionPoints }

    val setupAction: DBIO[Unit] = DBIO.seq(
      (boards.schema ++ squares.schema ++ queens.schema ++ rounds.schema ++ solutions.schema ++ solutionPoints.schema).create
    )

    Await.result(db.run(setupAction), Duration.Inf)

  }

  def drop[P <: JdbcProfile](
    db: Database,
    tables: Tables[P]
  )(implicit
    ec: ExecutionContext
  ): Unit = {

    import tables.profile.api._

    import tables.{ solutionPoints, solutions, queens, rounds, squares, boards }

    val finalizeAction: DBIO[Unit] = DBIO.seq(
      (solutionPoints.schema ++ solutions.schema ++ rounds.schema ++ queens.schema ++ squares.schema ++ boards.schema).drop
    )

    Await.result(db.run(finalizeAction), Duration.Inf)

  }

}
