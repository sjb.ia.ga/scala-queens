package queens
package base

import common.monad.Item

import conf.Conf


package `0`:

  import common.monad.`0`.`Monad'`

  package x:

    import cats.effect.Sync

    import monix.tail.Iterant

    abstract trait QueensMonad[E[_]: Sync,
      P <: Conf
    ] extends `Monad'`[E, Iterant, Solution, Long]:

      protected val * : P

      def foreach(block: Item[Solution] => Unit)
                 (using Long): Iterant[E, Unit]

      def map[R](block: Item[Solution] => R)
                (using Long): Iterant[E, R]

      def flatMap[R](block: Item[Solution] => Iterant[E, R])
                    (using Long): Iterant[E, R]

  package s:

    import cats.effect.IO

    import fs2.Stream

    abstract trait QueensMonad[
      P <: Conf
    ] extends `Monad'`[IO, Stream, Solution, Long]:

      protected val * : P

      def foreach(block: Item[Solution] => Unit)
                 (using Long): Stream[IO, Unit]

      def map[R](block: Item[Solution] => R)
                (using Long): Stream[IO, R]

      def flatMap[R](block: Item[Solution] => Stream[IO, R])
                    (using Long): Stream[IO, R]
