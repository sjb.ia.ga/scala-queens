package queens
package dimensions.third.first
package zeroth

import java.util.UUID

import scala.util.Try

import base.Board
import common.Flag


package flow:

  import conf.flow.AnyConf

  class Iteration0(override val params: Any*)
        extends AnyVal
        with AnyConf

  object Iteration0:

    inline given Conversion[Iteration1, Iteration0] = { it => Iteration0(it.params*) }

    def unapply(self: Iteration0): Option[(Board, Any, Try[UUID], Flag, () => Unit)] =
      (self.params(0), self.params(1), self.params(2), self.params(3)) match
         case (board: Board, tag: Any, flg: Flag, end: (() => Unit)) =>
           Some((board, tag, Try(tag.asInstanceOf[UUID]), flg, end))
         case _ =>
           None
