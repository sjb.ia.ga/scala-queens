package queens
package dimensions.third.first
package zeroth
package flow
package x

import cats.effect.Sync

import monix.tail.Iterant

import common.geom.Coord

import common.monad.Item


abstract trait QueensClassicCb[E[_]: Sync]
    extends `Queens*`[E, Coord[Int]]:

  import dimensions.second.first.ClassicCb.zero

  override def foreach(block: Item[Solution] => Unit)
                      (using Long, Iteration0): Iterant[E, Unit] =
    super.`foreach*`(block)

  override def map[R](block: Item[Solution] => R)
                     (using Long, Iteration0): Iterant[E, R] =
    super.`map*`(block)

  override def flatMap[R](block: Item[Solution] => Iterant[E, R])
                         (using Long, Iteration0): Iterant[E, R] =
    super.`flatMap*`(block)

