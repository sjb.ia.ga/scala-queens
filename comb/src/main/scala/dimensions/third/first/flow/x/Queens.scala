package queens
package dimensions.third.first
package zeroth
package flow
package x

import java.nio.file.Files

import java.util.UUID
import java.util.Date
import java.io.{ FileOutputStream, ObjectOutputStream }

import cats.effect.Sync

import monix.tail.Iterant
import Iterant.repeatEval

import base.Board

import common.Flag
import common.bis.Cycle
import common.monad.Item

import Macros.zeroth.{ load, save }
import common.Macros.tryc

import dimensions.Dimension.Model.flow

import version.less.nest.Nest


abstract trait Queens[E[_]: Sync, At]
    extends dimensions.third.Queens[At]
    with dimensions.second.`Queens*`[At]:

  final override val model = flow

  protected def `solve*`[R](using M: Long,
                                  start: At,
                                  iteration: Iteration0): Iterant[E, Item[Solution]] =
    val Iteration0(board, tag, _, flg, end) = iteration : @unchecked

    val nest = Nest(aspect, algorithm, model)
    val (_, _, _, uuid) =
      board match
        case Cycle(((board, `nest`, `M`, uuid), _), Some(_)) => (board, nest, M, uuid)
        case _ => ???

    var done: Option[Boolean] = Some(false)

    repeatEval[E, (Item[Solution], Boolean)] {

      var item: (Item[Solution], Boolean) = (null, false)

      var started: Date = null

      done match

        case Some(false) =>

          var pad: Any = null
          var fos: FileOutputStream = null
          var oos: ObjectOutputStream = null

          var solutions = LazyList[Solution]()

          var (number, number2) = 0L -> 0L

          var thaw: Option[Either[Load, At]] = Some(Right(start))

          board match
            case Cycle(_, Some(path)) => load(path) match
              case (x, Some((n, s, l))) => pad = x
                thaw = l.map(Left(_)); solutions = s; number = n; number2 = n
              case (x, _) => pad = x
            case _ =>

          given (Either[Save, Solution] => Boolean) = { s2 =>
            val its = s2.toOption

            its.map { it =>
              solutions = solutions :+ it
              number += 1

              val vi = (board, tag, uuid, number, started, Date())
              item = new Item(it, flg.slot, Some(vi)) -> true
            }

            (board, s2) match
              case (Cycle(_, Some(path)), Left(fun)) if number2 < number =>
                fos = FileOutputStream(path.toString)
                oos = ObjectOutputStream(fos)
                save(oos, solutions, number, Some(fun), pad)
                !flg
              case _ =>

            flg
         }

          given Board = board

          try

            given Either[Load, At] =
              if thaw.isEmpty
              then
                Right(start)
              else
                thaw.get

            started = Date()

            solve match
              case Some(true) =>
                cacheOps.put(cacheKey, solutions)
              case _ =>

          catch
            case ComputationSuspended =>
              tryc(oos.close)
              tryc(fos.close)
            case ResumeNotImplemented | SuspendNotSupported => ???

          finally
            if oos eq null
            then
              done = Some(true)
              item = item._1 -> true

          !flg

        case Some(true) =>

          item = new Item(Nil, flg.slot) -> true
          done = None

        case _ =>

          board match
            case Cycle(_, Some(path)) => tryc(Files.delete(path))
            case _ =>

          tryc(end())

      item
    }
    .takeWhile(_._2)
    .filter(_._1 ne null)
    .map(_._1)


abstract trait `Queens*`[E[_]: Sync, At]
    extends Queens[E, At]
    with dimensions.third.zeroth.Queens[E, Iterant, At, Iteration0]:

  final protected def `foreach*`(block: Item[Solution] => Unit)(using
                                 Long, At, Iteration0): Iterant[E, Unit] =
    val `block*` = `apply*`(block)
    `solve*`.map(`block*`)
      .filter(_ ne None)
      .map(_.get)

  final protected def `map*`[R](block: Item[Solution] => R)(using
                                Long, At, Iteration0): Iterant[E, R] =
    val `block*` = `apply*`(block)
    `solve*`.map(`block*`)
      .filter(_ ne None)
      .map(_.get)

  final protected def `flatMap*`[R](block: Item[Solution] => Iterant[E, R])(using
                                    Long, At, Iteration0): Iterant[E, R] =
    val `block*` = `apply*`(block)
    `solve*`.map(`block*`)
      .filter(_ ne None)
      .map(_.get)
      .flatten
