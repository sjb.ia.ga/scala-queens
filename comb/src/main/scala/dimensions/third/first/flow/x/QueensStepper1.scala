package queens
package dimensions.third.first
package zeroth
package flow
package x

import cats.effect.Sync

import monix.tail.Iterant

import base.Board

import common.geom.Coord

import common.stepper.Stepper1

import common.monad.Item


abstract trait QueensStepper1[E[_]: Sync]
    extends `Queens*`[E, Coord[Stepper1]]:

  import dimensions.second.first.BoardStepper.start
  import common.stepper.factory.given

  override def foreach(block: Item[Solution] => Unit)
                      (using M: Long, iteration: Iteration0): Iterant[E, Unit] =
    implicit val Iteration0(Board(size, _, seed), _, _, _, _) = iteration

    given Coord[Stepper1] = start[Stepper1](size)
    super.`foreach*`(block)

  override def map[R](block: Item[Solution] => R)
                     (using M: Long, iteration: Iteration0): Iterant[E, R] =
    implicit val Iteration0(Board(size, _, seed), _, _, _, _) = iteration

    given Coord[Stepper1] = start[Stepper1](size)
    super.`map*`(block)

  override def flatMap[R](block: Item[Solution] => Iterant[E, R])
                         (using M: Long, iteration: Iteration0): Iterant[E, R] =
    implicit val Iteration0(Board(size, _, seed), _, _, _, _) = iteration

    given Coord[Stepper1] = start[Stepper1](size)
    super.`flatMap*`(block)


package `*`:

  import common.stepper.dup.{ Stepper1Dup => `Stepper1*` }

  abstract trait `QueensStepper1*`[E[_]: Sync]
      extends `Queens*`[E, Coord[`Stepper1*`]]:

    import dimensions.second.first.`*`.`BoardStepper*`.`start*`
    import common.stepper.dup.factory.given

    override def foreach(block: Item[Solution] => Unit)
                        (using M: Long, iteration: Iteration0): Iterant[E, Unit] =
      implicit val Iteration0(Board(size, _, seed), _, _, _, _) = iteration

      given Coord[`Stepper1*`] = `start*`[`Stepper1*`](size)
      super.`foreach*`(block)

    override def map[R](block: Item[Solution] => R)
                       (using M: Long, iteration: Iteration0): Iterant[E, R] =
      implicit val Iteration0(Board(size, _, seed), _, _, _, _) = iteration

      given Coord[`Stepper1*`] = `start*`[`Stepper1*`](size)
      super.`map*`(block)

    override def flatMap[R](block: Item[Solution] => Iterant[E, R])
                           (using M: Long, iteration: Iteration0): Iterant[E, R] =
      implicit val Iteration0(Board(size, _, seed), _, _, _, _) = iteration

      given Coord[`Stepper1*`] = `start*`[`Stepper1*`](size)
      super.`flatMap*`(block)
