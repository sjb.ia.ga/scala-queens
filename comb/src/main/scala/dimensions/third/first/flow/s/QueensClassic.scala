package queens
package dimensions.third.first
package zeroth
package flow
package s

import cats.effect.IO

import fs2.Stream

import common.monad.Item


abstract trait QueensClassic
    extends `Queens*`[Point]:

  import dimensions.second.first.Classic.zero

  override def foreach(block: Item[Solution] => Unit)
                      (using Long, Iteration0): Stream[IO, Unit] =
    super.`foreach*`(block)

  override def map[R](block: Item[Solution] => R)
                     (using Long, Iteration0): Stream[IO, R] =
    super.`map*`(block)

  override def flatMap[R](block: Item[Solution] => Stream[IO, R])
                         (using Long, Iteration0): Stream[IO, R] =
    super.`flatMap*`(block)
