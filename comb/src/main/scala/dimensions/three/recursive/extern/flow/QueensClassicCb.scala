package queens
package dimensions.three
package zeroth
package recursive.extern
package flow

import base.Board

import cache.CacheOpsDelegateTo

import common.monad.Item


package x:

  import monix.eval.{ Coeval, Task }

  package sync:

    import dimensions.second.third.first.zeroth.recursive.extern.flow.x.AbstractQueensClassicCb

    class QueensClassicCb(
      override val validate: Validator,
      override protected val fs: Item[Solution] => Boolean*
    ) extends AbstractQueensClassicCb[Coeval]
        with dimensions.third.first.zeroth.flow.x.QueensClassicCb[Coeval] {

      override protected type * = QueensClassicCb

      override protected def `apply*`(s: Item[Solution] => Boolean*): * =
        new QueensClassicCb(validate, s*) {

          override protected val cacheOps = {
            val delegate = QueensClassicCb.this.cacheOps
            new CacheOpsDelegateTo(delegate.self)
          }

          QueensClassicCb.this.cacheOps.get(QueensClassicCb.this.cacheKey) match {
            case Some(solutions) =>
              cacheOps.put(cacheKey, solutions)
            case _ =>
          }

        }

    }

  package async:

    import dimensions.second.third.first.zeroth.recursive.extern.flow.x.AbstractQueensClassicCb

    class QueensClassicCb(
      override val validate: Validator,
      override protected val fs: Item[Solution] => Boolean*
    ) extends AbstractQueensClassicCb[Task]
        with dimensions.third.first.zeroth.flow.x.QueensClassicCb[Task] {

      override protected type * = QueensClassicCb

      override protected def `apply*`(s: Item[Solution] => Boolean*): * =
        new QueensClassicCb(validate, s*) {

          override protected val cacheOps = {
            val delegate = QueensClassicCb.this.cacheOps
            new CacheOpsDelegateTo(delegate.self)
          }

          QueensClassicCb.this.cacheOps.get(QueensClassicCb.this.cacheKey) match {
            case Some(solutions) =>
              cacheOps.put(cacheKey, solutions)
            case _ =>
          }

        }

    }


package s:

  import dimensions.second.third.first.zeroth.recursive.extern.flow.s.AbstractQueensClassicCb

  class QueensClassicCb(
    override val validate: Validator,
    override protected val fs: Item[Solution] => Boolean*
  ) extends AbstractQueensClassicCb
      with dimensions.third.first.zeroth.flow.s.QueensClassicCb {

    override protected type * = QueensClassicCb

    override protected def `apply*`(s: Item[Solution] => Boolean*): * =
      new QueensClassicCb(validate, s*) {

        override protected val cacheOps = {
          val delegate = QueensClassicCb.this.cacheOps
          new CacheOpsDelegateTo(delegate.self)
        }

        QueensClassicCb.this.cacheOps.get(QueensClassicCb.this.cacheKey) match {
          case Some(solutions) =>
            cacheOps.put(cacheKey, solutions)
          case _ =>
        }

      }

  }
