package queens
package dimensions.three


package breed:

  package object bis:

    given config.builder.apply.Builder = builders.bis.Builder

    given config.bis.Config = configurers.bis.MapperConfig


package object breed:

  given config.builder.apply.Builder = builders.Builder

  given config.Config = configurers.MapperConfig
