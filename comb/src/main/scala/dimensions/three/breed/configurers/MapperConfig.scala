package queens
package dimensions.three
package breed
package configurers

import dimensions.Dimension.{ Monad, Aspect, Algorithm, Model }
import Monad._
import Effect._
import Aspect._
import Algorithm._
import Model._
import Parallel._
import Messaging._


object MapperConfig
    extends config.Config:

  private final class CfgByModel(model: Model)
      extends ByModel:

    private final class CfgOfAlgorithm(algorithm: Algorithm)
        extends OfAlgorithm:

      private final class CfgToAspect(aspect: Aspect)
          extends ToAspect:

        import dimensions.third.Queens

        final class `Cfg Hatch` extends Hatch[Queens[?]]:

          override def apply(): Queens[?] = model match

            case Parallel(Actors) =>
              config.mapper.actors.ConfigByActors.queens(algorithm)(aspect)

            case Parallel(Futures) =>
              config.mapper.futures.ConfigByFutures.queens(algorithm)(aspect)

            case Messaging(Kafka) =>
              config.mapper.kafka.ConfigByKafka.queens(algorithm)(aspect)

            case Messaging(RabbitMQ) =>
              config.mapper.rabbitMQ.ConfigByRabbitMQ.queens(algorithm)(aspect)

            case Messaging(AWSSQS) =>
              config.mapper.awsSQS.ConfigByAWSSQS.queens(algorithm)(aspect)

            case Flow =>
              config.mapper.flow.ConfigByFlow.queens(algorithm)(aspect)

        import Hatch.given

        override def breed[Q <: Queens[?]]: Hatch[Q] = new `Cfg Hatch`

        private final class CfgAtMonad(monad: Monad)
            extends AtMonad:

          import dimensions.third.Queens

          final class `Cfg Hatch` extends Hatch[Queens[?]]:

            override def apply(): Queens[?] = model match

              case Flow => monad match

                case Iterable =>

                  CfgToAspect.this.breed[Queens[?]].apply()

                case Effect(MonixIterantCoeval) =>

                  config.mapper.flow.`0`.x.sync.ConfigByFlow.queens(algorithm)(aspect)

                case Effect(MonixIterantTask) =>

                  config.mapper.flow.`0`.x.async.ConfigByFlow.queens(algorithm)(aspect)

                case Effect(FS2StreamIO) =>

                  config.mapper.flow.`0`.s.ConfigByFlow.queens(algorithm)(aspect)

              case _ => ???

          import Hatch.given

          override def breed[Q <: Queens[?]]: Hatch[Q] = new `Cfg Hatch`

        override def atMonad(monad: Monad): AtMonad = new CfgAtMonad(monad)

      override def toAspect(aspect: Aspect): ToAspect = new CfgToAspect(aspect)

    override def ofAlgorithm(algorithm: Algorithm): OfAlgorithm = new CfgOfAlgorithm(algorithm)

  override def byModel(model: Model): ByModel = new CfgByModel(model)


package bis:

  object MapperConfig
      extends config.bis.Config:

    private final class CfgByModel(model: Model)
        extends ByModel:

      private final class CfgOfAlgorithm(algorithm: Algorithm)
          extends OfAlgorithm:

        private final class CfgToAspect(aspect: Aspect)
            extends ToAspect:

          import dimensions.third.bis.Queens

          final class `Cfg Hatch` extends Hatch[Queens[?]]:

            override def apply(): Queens[?] = model match

              case Parallel(Actors) =>
                config.mapper.actors.bis.ConfigByActors.queens(algorithm)(aspect)

              case Parallel(Futures) =>
                config.mapper.futures.bis.ConfigByFutures.queens(algorithm)(aspect)

              case Messaging(Kafka) =>
                config.mapper.kafka.bis.ConfigByKafka.queens(algorithm)(aspect)

              case Messaging(RabbitMQ) =>
                config.mapper.rabbitMQ.bis.ConfigByRabbitMQ.queens(algorithm)(aspect)

              case Messaging(AWSSQS) =>
                config.mapper.awsSQS.bis.ConfigByAWSSQS.queens(algorithm)(aspect)

              case Flow =>
                config.mapper.flow.bis.ConfigByFlow.queens(algorithm)(aspect)

          import Hatch.given

          override def breed[Q <: Queens[?]]: Hatch[Q] = new `Cfg Hatch`

        override def toAspect(aspect: Aspect): ToAspect = new CfgToAspect(aspect)

      override def ofAlgorithm(algorithm: Algorithm): OfAlgorithm = new CfgOfAlgorithm(algorithm)

    override def byModel(model: Model): ByModel = new CfgByModel(model)
