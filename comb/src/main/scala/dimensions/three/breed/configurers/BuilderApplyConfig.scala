package queens
package dimensions.three
package breed
package configurers

import dimensions.Dimension.{ Monad, Aspect, Algorithm, Model }

import config.Config
import dimensions.third.Queens


object BuilderApplyConfig
    extends Config:

  private given                         (using Nothing => Nothing)
               : Validator = Validator()

  private final class CfgByModel(model: Model)
      extends ByModel:

    private final class CfgOfAlgorithm(algorithm: Algorithm)
        extends OfAlgorithm:

      private final class CfgToAspect(aspect: Aspect)
          extends ToAspect:

        final class `Cfg Hatch`[Q <: Queens[?]]
            extends Hatch[Q]:

          inline override def apply(): Q =
            builders.Builder
              .byModel(model)
              .withAlgorithm(algorithm)
              .andAspect(aspect)
              .breed()

        override def breed[Q <: Queens[?]]: Hatch[Q] = new `Cfg Hatch`[Q]

        private final class CfgAtMonad(monad: Monad)
            extends AtMonad:

          final class `Cfg Hatch`[Q <: Queens[?]]
              extends Hatch[Q]:

            inline override def apply(): Q =
              builders.Builder
                .byModel(model)
                .withAlgorithm(algorithm)
                .andAspect(aspect)
                .butMonad(monad)
                .breed()

          override def breed[Q <: Queens[?]]: Hatch[Q] = new `Cfg Hatch`[Q]

        override def atMonad(monad: Monad): AtMonad = new CfgAtMonad(monad)

      override def toAspect(aspect: Aspect): ToAspect = new CfgToAspect(aspect)

    override def ofAlgorithm(algorithm: Algorithm): OfAlgorithm = new CfgOfAlgorithm(algorithm)

  override def byModel(model: Model): ByModel = new CfgByModel(model)


package bis:

  object BuilderApplyConfig
      extends config.bis.Config:

    private given                         (using Nothing => Nothing)
                 : Validator = Validator()

    private final class CfgByModel(model: Model)
        extends ByModel:

      private final class CfgOfAlgorithm(algorithm: Algorithm)
          extends OfAlgorithm:

        private final class CfgToAspect(aspect: Aspect)
            extends ToAspect:

          final class `Cfg Hatch`[Q <: Queens[?]]
              extends Hatch[Q]:

            inline override def apply(): Q =
              builders.bis.Builder
                .byModel(model)
                .withAlgorithm(algorithm)
                .andAspect(aspect)
                .breed()

          override def breed[Q <: Queens[?]]: Hatch[Q] = new `Cfg Hatch`

        override def toAspect(aspect: Aspect): ToAspect = new CfgToAspect(aspect)

      override def ofAlgorithm(algorithm: Algorithm): OfAlgorithm = new CfgOfAlgorithm(algorithm)

    override def byModel(model: Model): ByModel = new CfgByModel(model)
