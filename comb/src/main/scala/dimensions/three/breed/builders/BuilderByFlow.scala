package queens
package dimensions.three
package breed
package builders

import dimensions.Dimension.{ Monad, Aspect, Algorithm, Model }
import dimensions.Dimension.Monad._
import dimensions.Dimension.Monad.Effect._
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._

import config.builder.apply.{ Builder, BuilderByModel }
import dimensions.third.Queens


private object BuilderByFlow
    extends BuilderByModel:

  private given                          (using Nothing => Nothing)
               : Validator = Validator()

  import Hatch.given

  override lazy val from: Builder => Builder#ByModel = { (builder: Builder) =>

    new builder.ByModel:

      override def withAlgorithm(algorithm: Algorithm): WithAlgorithm = algorithm match

        case Recursive(Native) =>

          import dimensions.three.recursive.native.flow._

          object WithRecursiveNative extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassic.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassicCb.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndClassicCb

              case Stepper(OneIncrement) =>

                object AndStepper1 extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepper1(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndStepper1.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndStepper1

              case Stepper(Permutations) =>

                object AndStepperP extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepperP(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndStepperP.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndStepperP

          WithRecursiveNative

        case Recursive(Closure) =>

          import dimensions.three.recursive.closure.flow._

          object WithRecursiveClosure extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassic.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassicCb.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndClassicCb

              case Stepper(_) => ???

          WithRecursiveClosure

        case Recursive(Trampoline) =>

          import dimensions.three.recursive.trampoline.flow._

          object WithRecursiveTrampoline extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassic.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassicCb.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndClassicCb

              case Stepper(_) => ???

          WithRecursiveTrampoline

        case Recursive(Extern) =>

          import dimensions.three.recursive.extern.flow._
          import dimensions.three.zeroth.recursive.extern.flow.x
          import dimensions.three.zeroth.recursive.extern.flow.s

          object WithRecursiveExtern extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassic.breed[Q]

                      ButIterable

                    case Effect(MonixIterantCoeval) =>

                      object ButMonixIterantCoeval extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.sync.QueensClassic(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantCoeval

                    case Effect(MonixIterantTask) =>

                      object ButMonixIterantTask extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.async.QueensClassic(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantTask

                    case Effect(FS2StreamIO) =>

                      object ButFS2StreamIO extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new s.QueensClassic(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButFS2StreamIO

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassicCb.breed[Q]

                      ButIterable

                    case Effect(MonixIterantCoeval) =>

                      object ButMonixIterantCoeval extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.sync.QueensClassicCb(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantCoeval

                    case Effect(MonixIterantTask) =>

                      object ButMonixIterantTask extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.async.QueensClassicCb(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantTask

                    case Effect(FS2StreamIO) =>

                      object ButFS2StreamIO extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new s.QueensClassicCb(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButFS2StreamIO

                AndClassicCb

              case Stepper(OneIncrement) =>

                object AndStepper1 extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepper1(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndStepper1.breed[Q]

                      ButIterable

                    case Effect(MonixIterantCoeval) =>

                      object ButMonixIterantCoeval extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.sync.QueensStepper1(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantCoeval

                    case Effect(MonixIterantTask) =>

                      object ButMonixIterantTask extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.async.QueensStepper1(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantTask

                    case Effect(FS2StreamIO) =>

                      object ButFS2StreamIO extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new s.QueensStepper1(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButFS2StreamIO

                AndStepper1

              case Stepper(Permutations) =>

                object AndStepperP extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepperP(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndStepperP.breed[Q]

                      ButIterable

                    case Effect(MonixIterantCoeval) =>

                      object ButMonixIterantCoeval extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.sync.QueensStepperP(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantCoeval

                    case Effect(MonixIterantTask) =>

                      object ButMonixIterantTask extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.async.QueensStepperP(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantTask

                    case Effect(FS2StreamIO) =>

                      object ButFS2StreamIO extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new s.QueensStepperP(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButFS2StreamIO

                AndStepperP

          WithRecursiveExtern

        case Recursive(Continuation) =>

          import dimensions.three.recursive.continuation.flow._
          import dimensions.three.zeroth.recursive.continuation.flow.x
          import dimensions.three.zeroth.recursive.continuation.flow.s

          object WithRecursiveContinuation extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassic.breed[Q]

                      ButIterable

                    case Effect(MonixIterantCoeval) =>

                      object ButMonixIterantCoeval extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.sync.QueensClassic(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantCoeval

                    case Effect(MonixIterantTask) =>

                      object ButMonixIterantTask extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.async.QueensClassic(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantTask

                    case Effect(FS2StreamIO) =>

                      object ButFS2StreamIO extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new s.QueensClassic(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButFS2StreamIO

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassicCb.breed[Q]

                      ButIterable

                    case Effect(MonixIterantCoeval) =>

                      object ButMonixIterantCoeval extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.sync.QueensClassicCb(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantCoeval

                    case Effect(MonixIterantTask) =>

                      object ButMonixIterantTask extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.async.QueensClassicCb(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantTask

                    case Effect(FS2StreamIO) =>

                      object ButFS2StreamIO extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new s.QueensClassicCb(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButFS2StreamIO

                AndClassicCb

              case Stepper(OneIncrement) =>

                object AndStepper1 extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepper1(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndStepper1.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndStepper1

              case Stepper(Permutations) =>

                object AndStepperP extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepperP(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndStepperP.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndStepperP

          WithRecursiveContinuation

        case Recursive(TailCall) =>

          import dimensions.three.recursive.tailcall.flow._

          object WithRecursiveTailCall extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassic.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassicCb.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndClassicCb

              case Stepper(_) => ???

          WithRecursiveTailCall

        case Recursive(CatsEval) =>

          import dimensions.three.recursive.catseval.flow._

          object WithRecursiveCatsEval extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassic.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassicCb.breed[Q]

                      ButIterable

                    case Effect(_) => ???

                AndClassicCb

              case Stepper(_) => ???

          WithRecursiveCatsEval

        case Iterative =>

          import dimensions.three.iterative.flow._
          import dimensions.three.zeroth.iterative.flow.x
          import dimensions.three.zeroth.iterative.flow.s

          object WithIterative extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassic.breed[Q]

                      ButIterable

                    case Effect(MonixIterantCoeval) =>

                      object ButMonixIterantCoeval extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.sync.QueensClassic(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantCoeval

                    case Effect(MonixIterantTask) =>

                      object ButMonixIterantTask extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.async.QueensClassic(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantTask

                    case Effect(FS2StreamIO) =>

                      object ButFS2StreamIO extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new s.QueensClassic(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButFS2StreamIO

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndClassicCb.breed[Q]

                      ButIterable

                    case Effect(MonixIterantCoeval) =>

                      object ButMonixIterantCoeval extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.sync.QueensClassicCb(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantCoeval

                    case Effect(MonixIterantTask) =>

                      object ButMonixIterantTask extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.async.QueensClassicCb(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantTask

                    case Effect(FS2StreamIO) =>

                      object ButFS2StreamIO extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new s.QueensClassicCb(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButFS2StreamIO

                AndClassicCb

              case Stepper(OneIncrement) =>

                object AndStepper1 extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepper1(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndStepper1.breed[Q]

                      ButIterable

                    case Effect(MonixIterantCoeval) =>

                      object ButMonixIterantCoeval extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.sync.QueensStepper1(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantCoeval

                    case Effect(MonixIterantTask) =>

                      object ButMonixIterantTask extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.async.QueensStepper1(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantTask

                    case Effect(FS2StreamIO) =>

                      object ButFS2StreamIO extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new s.QueensStepper1(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButFS2StreamIO

                AndStepper1

              case Stepper(Permutations) =>

                object AndStepperP extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepperP(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  override def butMonad(monad: Monad): ButMonad = monad match

                    case Iterable =>

                      object ButIterable extends ButMonad:

                        override def breed[Q <: Queens[?]]: Hatch[Q] = AndStepperP.breed[Q]

                      ButIterable

                    case Effect(MonixIterantCoeval) =>

                      object ButMonixIterantCoeval extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.sync.QueensStepperP(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantCoeval

                    case Effect(MonixIterantTask) =>

                      object ButMonixIterantTask extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new x.async.QueensStepperP(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButMonixIterantTask

                    case Effect(FS2StreamIO) =>

                      object ButFS2StreamIO extends ButMonad:

                        object `Builder Hatch` extends Hatch[Queens[?]]:

                          inline override def apply(): Queens[?] =
                            new s.QueensStepperP(given_Validator)

                        override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                      ButFS2StreamIO

                AndStepperP

          WithIterative

  }


package bis:

  private object BuilderByFlow
      extends BuilderByModel:

    private given                          (using Nothing => Nothing)
                 : Validator = Validator()

    import Hatch.given

    override lazy val from: Builder => Builder#ByModel = { (builder: Builder) =>

      new builder.ByModel:

        override def withAlgorithm(algorithm: Algorithm): WithAlgorithm = algorithm match

          case Recursive(Native) =>

            import dimensions.three.recursive.native.flow.bis._

            object WithRecursiveNative extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(OneIncrement) =>

                  object AndStepper1 extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepper1(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepper1

                case Stepper(Permutations) =>

                  object AndStepperP extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepperP(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepperP

            WithRecursiveNative

          case Recursive(Closure) =>

            import dimensions.three.recursive.closure.flow.bis._

            object WithRecursiveClosure extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(_) => ???

            WithRecursiveClosure

          case Recursive(Trampoline) =>

            import dimensions.three.recursive.trampoline.flow.bis._

            object WithRecursiveTrampoline extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(_) => ???

            WithRecursiveTrampoline

          case Recursive(Extern) =>

            import dimensions.three.recursive.extern.flow.bis._

            object WithRecursiveExtern extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(OneIncrement) =>

                  object AndStepper1 extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepper1(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepper1

                case Stepper(Permutations) =>

                  object AndStepperP extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepperP(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepperP

            WithRecursiveExtern

          case Recursive(Continuation) =>

            import dimensions.three.recursive.continuation.flow.bis._

            object WithRecursiveContinuation extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(OneIncrement) =>

                  object AndStepper1 extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepper1(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepper1

                case Stepper(Permutations) =>

                  object AndStepperP extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepperP(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepperP

            WithRecursiveContinuation

          case Recursive(TailCall) =>

            import dimensions.three.recursive.tailcall.flow.bis._

            object WithRecursiveTailCall extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(_) => ???

            WithRecursiveTailCall

          case Recursive(CatsEval) =>

            import dimensions.three.recursive.catseval.flow.bis._

            object WithRecursiveCatsEval extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(_) => ???

            WithRecursiveCatsEval

          case Iterative =>

            import dimensions.three.iterative.flow.bis._

            object WithIterative extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(OneIncrement) =>

                  object AndStepper1 extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepper1(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepper1

                case Stepper(Permutations) =>

                  object AndStepperP extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepperP(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepperP

            WithIterative

    }
