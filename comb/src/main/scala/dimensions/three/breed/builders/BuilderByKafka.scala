package queens
package dimensions.three
package breed
package builders

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import dimensions.Dimension.Aspect._
import dimensions.Dimension.Aspect.Classic._
import dimensions.Dimension.Aspect.Stepper._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Algorithm.Recursive._

import config.builder.apply.{ Builder, BuilderByModel }
import dimensions.third.Queens


private object BuilderByKafka
    extends BuilderByModel:

  private given                          (using Nothing => Nothing)
               : Validator = Validator()

  import Hatch.given

  override lazy val from: Builder => Builder#ByModel = { (builder: Builder) =>

    new builder.ByModel:

      override def withAlgorithm(algorithm: Algorithm): WithAlgorithm = algorithm match

        case Recursive(Native) =>

          import dimensions.three.recursive.native.kafka._

          object WithRecursiveNative extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassicCb

              case Stepper(OneIncrement) =>

                object AndStepper1 extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepper1(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndStepper1

              case Stepper(Permutations) =>

                object AndStepperP extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepperP(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndStepperP

          WithRecursiveNative

        case Recursive(Closure) =>

          import dimensions.three.recursive.closure.awsSQS._

          object WithRecursiveClosure extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassicCb

              case Stepper(_) => ???

          WithRecursiveClosure

        case Recursive(Trampoline) =>

          import dimensions.three.recursive.trampoline.awsSQS._

          object WithRecursiveTrampoline extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassicCb

              case Stepper(_) => ???

          WithRecursiveTrampoline

        case Recursive(Extern) =>

          import dimensions.three.recursive.extern.kafka._

          object WithRecursiveExtern extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassicCb

              case Stepper(OneIncrement) =>

                object AndStepper1 extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepper1(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndStepper1

              case Stepper(Permutations) =>

                object AndStepperP extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepperP(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndStepperP

          WithRecursiveExtern

        case Recursive(Continuation) =>

          import dimensions.three.recursive.continuation.kafka._

          object WithRecursiveContinuation extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassicCb

              case Stepper(OneIncrement) =>

                object AndStepper1 extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepper1(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndStepper1

              case Stepper(Permutations) =>

                object AndStepperP extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepperP(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndStepperP

          WithRecursiveContinuation

        case Recursive(TailCall) =>

          import dimensions.three.recursive.tailcall.kafka._

          object WithRecursiveTailCall extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassicCb

              case Stepper(_) => ???

          WithRecursiveTailCall

        case Recursive(CatsEval) =>

          import dimensions.three.recursive.catseval.kafka._

          object WithRecursiveCatsEval extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassicCb

              case Stepper(_) => ???

          WithRecursiveCatsEval

        case Iterative =>

          import dimensions.three.iterative.kafka._

          object WithIterative extends WithAlgorithm:

            override def andAspect(aspect: Aspect): AndAspect = aspect match

              case Classic(Straight) =>

                object AndClassic extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassic(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassic

              case Classic(Callback) =>

                object AndClassicCb extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensClassicCb(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndClassicCb

              case Stepper(OneIncrement) =>

                object AndStepper1 extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepper1(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndStepper1

              case Stepper(Permutations) =>

                object AndStepperP extends AndAspect:

                  object `Builder Hatch` extends Hatch[Queens[?]]:

                    inline override def apply(): Queens[?] =
                      new QueensStepperP(given_Validator)

                  override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                AndStepperP

          WithIterative

  }


package bis:

  private object BuilderByKafka
      extends BuilderByModel:

    private given                          (using Nothing => Nothing)
                 : Validator = Validator()

    import Hatch.given

    override lazy val from: Builder => Builder#ByModel = { (builder: Builder) =>

      new builder.ByModel:

        override def withAlgorithm(algorithm: Algorithm): WithAlgorithm = algorithm match

          case Recursive(Native) =>

            import dimensions.three.recursive.native.kafka.bis._

            object WithRecursiveNative extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(OneIncrement) =>

                  object AndStepper1 extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepper1(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepper1

                case Stepper(Permutations) =>

                  object AndStepperP extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepperP(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepperP

            WithRecursiveNative

          case Recursive(Closure) =>

            import dimensions.three.recursive.closure.awsSQS.bis._

            object WithRecursiveClosure extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(_) => ???

            WithRecursiveClosure

          case Recursive(Trampoline) =>

            import dimensions.three.recursive.trampoline.awsSQS.bis._

            object WithRecursiveTrampoline extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(_) => ???

            WithRecursiveTrampoline

          case Recursive(Extern) =>

            import dimensions.three.recursive.extern.kafka.bis._

            object WithRecursiveExtern extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(OneIncrement) =>

                  object AndStepper1 extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepper1(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepper1

                case Stepper(Permutations) =>

                  object AndStepperP extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepperP(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepperP

            WithRecursiveExtern

          case Recursive(Continuation) =>

            import dimensions.three.recursive.continuation.kafka.bis._

            object WithRecursiveContinuation extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(OneIncrement) =>

                  object AndStepper1 extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepper1(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepper1

                case Stepper(Permutations) =>

                  object AndStepperP extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepperP(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepperP

            WithRecursiveContinuation

          case Recursive(TailCall) =>

            import dimensions.three.recursive.tailcall.kafka.bis._

            object WithRecursiveTailCall extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(_) => ???

            WithRecursiveTailCall

          case Recursive(CatsEval) =>

            import dimensions.three.recursive.catseval.kafka.bis._

            object WithRecursiveCatsEval extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(_) => ???

            WithRecursiveCatsEval

          case Iterative =>

            import dimensions.three.iterative.kafka.bis._

            object WithIterative extends WithAlgorithm:

              override def andAspect(aspect: Aspect): AndAspect = aspect match

                case Classic(Straight) =>

                  object AndClassic extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassic(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassic

                case Classic(Callback) =>

                  object AndClassicCb extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensClassicCb(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndClassicCb

                case Stepper(OneIncrement) =>

                  object AndStepper1 extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepper1(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepper1

                case Stepper(Permutations) =>

                  object AndStepperP extends AndAspect:

                    object `Builder Hatch` extends Hatch[Queens[?]]:

                      inline override def apply(): Queens[?] =
                        new QueensStepperP(given_Validator)

                    override def breed[Q <: Queens[?]]: Hatch[Q] = `Builder Hatch`

                  AndStepperP

            WithIterative

    }
