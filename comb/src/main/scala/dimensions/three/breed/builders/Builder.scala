package queens
package dimensions.three
package breed
package builders

import dimensions.Dimension.Model
import Model._
import Parallel._
import Messaging._


object Builder
    extends config.builder.apply.Builder:

  override def byModel(model: Model): config.builder.apply.Builder#ByModel =
    model match

      case Parallel(Actors) => BuilderByActors.from(this)

      case Parallel(Futures) => BuilderByFutures.from(this)

      case Messaging(Kafka) => BuilderByKafka.from(this)

      case Messaging(RabbitMQ) => BuilderByRabbitMQ.from(this)

      case Messaging(AWSSQS) => BuilderByAWSSQS.from(this)

      case Flow => BuilderByFlow.from(this)


package bis:

  object Builder
      extends config.builder.apply.Builder:

    override def byModel(model: Model): config.builder.apply.Builder#ByModel =
      model match

        case Parallel(Actors) => BuilderByActors.from(this)

        case Parallel(Futures) => BuilderByFutures.from(this)

        case Messaging(Kafka) => BuilderByKafka.from(this)

        case Messaging(RabbitMQ) => BuilderByRabbitMQ.from(this)

        case Messaging(AWSSQS) => BuilderByAWSSQS.from(this)

        case Flow => BuilderByFlow.from(this)
