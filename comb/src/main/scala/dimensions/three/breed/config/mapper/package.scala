package queens
package dimensions.three
package breed
package config
package mapper

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Algorithm._


package actors {

  private[breed] object ConfigByActors {

    private given                          (using Nothing => Nothing)
                 : Validator = Validator()

    import dimensions.third.Queens

    def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

      return Map(

        iterative -> Map(

          classic -> new dimensions.three.iterative.actors.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.iterative.actors.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.iterative.actors.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.iterative.actors.QueensStepperP(given_Validator)

        ),

        continuation -> Map(

          classic -> new dimensions.three.recursive.continuation.actors.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.continuation.actors.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.continuation.actors.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.continuation.actors.QueensStepperP(given_Validator)

        ),

        extern -> Map(

          classic -> new dimensions.three.recursive.extern.actors.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.extern.actors.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.extern.actors.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.extern.actors.QueensStepperP(given_Validator)

        ),

        native -> Map(

          classic -> new dimensions.three.recursive.native.actors.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.native.actors.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.native.actors.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.native.actors.QueensStepperP(given_Validator)

        ),

        closure -> Map(

          classic -> new dimensions.three.recursive.closure.actors.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.closure.actors.QueensClassicCb(given_Validator),

        ),

        trampoline -> Map(

          classic -> new dimensions.three.recursive.trampoline.actors.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.trampoline.actors.QueensClassicCb(given_Validator),

        ),

        tailcall -> Map(

          classic -> new dimensions.three.recursive.tailcall.actors.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.tailcall.actors.QueensClassicCb(given_Validator)

        ),

        catseval -> Map(

          classic -> new dimensions.three.recursive.catseval.actors.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.catseval.actors.QueensClassicCb(given_Validator)

        )

      )
  }

  package bis {

    private[breed] object ConfigByActors {

      private given                          (using Nothing => Nothing)
                   : Validator = Validator()

      import dimensions.third.bis.Queens

      def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

        return Map(

          iterative -> Map(

            classic -> new dimensions.three.iterative.actors.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.iterative.actors.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.iterative.actors.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.iterative.actors.bis.QueensStepperP(given_Validator)

          ),

          continuation -> Map(

            classic -> new dimensions.three.recursive.continuation.actors.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.continuation.actors.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.continuation.actors.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.continuation.actors.bis.QueensStepperP(given_Validator)

          ),

          extern -> Map(

            classic -> new dimensions.three.recursive.extern.actors.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.extern.actors.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.extern.actors.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.extern.actors.bis.QueensStepperP(given_Validator)

          ),

          native -> Map(

            classic -> new dimensions.three.recursive.native.actors.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.native.actors.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.native.actors.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.native.actors.bis.QueensStepperP(given_Validator)

          ),

          closure -> Map(

            classic -> new dimensions.three.recursive.closure.actors.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.closure.actors.bis.QueensClassicCb(given_Validator),

          ),

          trampoline -> Map(

            classic -> new dimensions.three.recursive.trampoline.actors.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.trampoline.actors.bis.QueensClassicCb(given_Validator),

          ),

          tailcall -> Map(

            classic -> new dimensions.three.recursive.tailcall.actors.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.tailcall.actors.bis.QueensClassicCb(given_Validator)

          ),

          catseval -> Map(

            classic -> new dimensions.three.recursive.catseval.actors.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.catseval.actors.bis.QueensClassicCb(given_Validator)

          )

        )
    }

  }

}


package futures {

  private[breed] object ConfigByFutures {

    private given                          (using Nothing => Nothing)
                 : Validator = Validator()

    import dimensions.third.Queens

    def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

      return Map(

        iterative -> Map(

          classic -> new dimensions.three.iterative.futures.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.iterative.futures.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.iterative.futures.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.iterative.futures.QueensStepperP(given_Validator)

        ),

        continuation -> Map(

          classic -> new dimensions.three.recursive.continuation.futures.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.continuation.futures.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.continuation.futures.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.continuation.futures.QueensStepperP(given_Validator)

        ),

        extern -> Map(

          classic -> new dimensions.three.recursive.extern.futures.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.extern.futures.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.extern.futures.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.extern.futures.QueensStepperP(given_Validator)

        ),

        native -> Map(

          classic -> new dimensions.three.recursive.native.futures.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.native.futures.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.native.futures.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.native.futures.QueensStepperP(given_Validator)

        ),

        closure -> Map(

          classic -> new dimensions.three.recursive.closure.futures.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.closure.futures.QueensClassicCb(given_Validator),

        ),

        trampoline -> Map(

          classic -> new dimensions.three.recursive.trampoline.futures.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.trampoline.futures.QueensClassicCb(given_Validator),

        ),

        tailcall -> Map(

          classic -> new dimensions.three.recursive.tailcall.futures.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.tailcall.futures.QueensClassicCb(given_Validator)

        ),

        catseval -> Map(

          classic -> new dimensions.three.recursive.catseval.futures.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.catseval.futures.QueensClassicCb(given_Validator)

        )

      )

  }

  package bis {

    private[breed] object ConfigByFutures {

      private given                          (using Nothing => Nothing)
                   : Validator = Validator()

      import dimensions.third.bis.Queens

      def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

        return Map(

          iterative -> Map(

            classic -> new dimensions.three.iterative.futures.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.iterative.futures.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.iterative.futures.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.iterative.futures.bis.QueensStepperP(given_Validator)

          ),

          continuation -> Map(

            classic -> new dimensions.three.recursive.continuation.futures.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.continuation.futures.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.continuation.futures.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.continuation.futures.bis.QueensStepperP(given_Validator)

          ),

          extern -> Map(

            classic -> new dimensions.three.recursive.extern.futures.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.extern.futures.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.extern.futures.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.extern.futures.bis.QueensStepperP(given_Validator)

          ),

          native -> Map(

            classic -> new dimensions.three.recursive.native.futures.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.native.futures.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.native.futures.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.native.futures.bis.QueensStepperP(given_Validator)

          ),

          closure -> Map(

            classic -> new dimensions.three.recursive.closure.futures.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.closure.futures.bis.QueensClassicCb(given_Validator),

          ),

          trampoline -> Map(

            classic -> new dimensions.three.recursive.trampoline.futures.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.trampoline.futures.bis.QueensClassicCb(given_Validator),

          ),

          tailcall -> Map(

            classic -> new dimensions.three.recursive.tailcall.futures.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.tailcall.futures.bis.QueensClassicCb(given_Validator)

          ),

          catseval -> Map(

            classic -> new dimensions.three.recursive.catseval.futures.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.catseval.futures.bis.QueensClassicCb(given_Validator)

          )

        )

    }

  }

}


package kafka {

  private[breed] object ConfigByKafka {

    private given                          (using Nothing => Nothing)
                 : Validator = Validator()

    import dimensions.third.Queens

    def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

      return Map(

        iterative -> Map(

          classic -> new dimensions.three.iterative.kafka.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.iterative.kafka.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.iterative.kafka.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.iterative.kafka.QueensStepperP(given_Validator)

        ),

        continuation -> Map(

          classic -> new dimensions.three.recursive.continuation.kafka.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.continuation.kafka.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.continuation.kafka.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.continuation.kafka.QueensStepperP(given_Validator)

        ),

        extern -> Map(

          classic -> new dimensions.three.recursive.extern.kafka.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.extern.kafka.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.extern.kafka.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.extern.kafka.QueensStepperP(given_Validator)

        ),

        native -> Map(

          classic -> new dimensions.three.recursive.native.kafka.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.native.kafka.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.native.kafka.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.native.kafka.QueensStepperP(given_Validator)

        ),

        closure -> Map(

          classic -> new dimensions.three.recursive.closure.kafka.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.closure.kafka.QueensClassicCb(given_Validator),

        ),

        trampoline -> Map(

          classic -> new dimensions.three.recursive.trampoline.kafka.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.trampoline.kafka.QueensClassicCb(given_Validator),

        ),

        tailcall -> Map(

          classic -> new dimensions.three.recursive.tailcall.kafka.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.tailcall.kafka.QueensClassicCb(given_Validator)

        ),

        catseval -> Map(

          classic -> new dimensions.three.recursive.catseval.kafka.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.catseval.kafka.QueensClassicCb(given_Validator)

        )

      )

  }

  package bis {

    private[breed] object ConfigByKafka {

      private given                          (using Nothing => Nothing)
                   : Validator = Validator()

      import dimensions.third.bis.Queens

      def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

        return Map(

          iterative -> Map(

            classic -> new dimensions.three.iterative.kafka.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.iterative.kafka.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.iterative.kafka.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.iterative.kafka.bis.QueensStepperP(given_Validator)

          ),

          continuation -> Map(

            classic -> new dimensions.three.recursive.continuation.kafka.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.continuation.kafka.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.continuation.kafka.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.continuation.kafka.bis.QueensStepperP(given_Validator)

          ),

          extern -> Map(

            classic -> new dimensions.three.recursive.extern.kafka.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.extern.kafka.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.extern.kafka.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.extern.kafka.bis.QueensStepperP(given_Validator)

          ),

          native -> Map(

            classic -> new dimensions.three.recursive.native.kafka.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.native.kafka.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.native.kafka.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.native.kafka.bis.QueensStepperP(given_Validator)

          ),

          closure -> Map(

            classic -> new dimensions.three.recursive.closure.kafka.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.closure.kafka.bis.QueensClassicCb(given_Validator),

          ),

          trampoline -> Map(

            classic -> new dimensions.three.recursive.trampoline.kafka.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.trampoline.kafka.bis.QueensClassicCb(given_Validator),

          ),

          tailcall -> Map(

            classic -> new dimensions.three.recursive.tailcall.kafka.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.tailcall.kafka.bis.QueensClassicCb(given_Validator)

          ),

          catseval -> Map(

            classic -> new dimensions.three.recursive.catseval.kafka.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.catseval.kafka.bis.QueensClassicCb(given_Validator)

          )

        )

    }

  }

}

package rabbitMQ {

  private[breed] object ConfigByRabbitMQ {

    private given                          (using Nothing => Nothing)
                 : Validator = Validator()

    import dimensions.third.Queens

    def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

      return Map(

        iterative -> Map(

          classic -> new dimensions.three.iterative.rabbitMQ.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.iterative.rabbitMQ.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.iterative.rabbitMQ.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.iterative.rabbitMQ.QueensStepperP(given_Validator)

        ),

        continuation -> Map(

          classic -> new dimensions.three.recursive.continuation.rabbitMQ.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.continuation.rabbitMQ.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.continuation.rabbitMQ.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.continuation.rabbitMQ.QueensStepperP(given_Validator)

        ),

        extern -> Map(

          classic -> new dimensions.three.recursive.extern.rabbitMQ.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.extern.rabbitMQ.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.extern.rabbitMQ.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.extern.rabbitMQ.QueensStepperP(given_Validator)

        ),

        native -> Map(

          classic -> new dimensions.three.recursive.native.rabbitMQ.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.native.rabbitMQ.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.native.rabbitMQ.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.native.rabbitMQ.QueensStepperP(given_Validator)

        ),

        closure -> Map(

          classic -> new dimensions.three.recursive.closure.rabbitMQ.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.closure.rabbitMQ.QueensClassicCb(given_Validator),

        ),

        trampoline -> Map(

          classic -> new dimensions.three.recursive.trampoline.rabbitMQ.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.trampoline.rabbitMQ.QueensClassicCb(given_Validator),

        ),

        tailcall -> Map(

          classic -> new dimensions.three.recursive.tailcall.rabbitMQ.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.tailcall.rabbitMQ.QueensClassicCb(given_Validator)

        ),

        catseval -> Map(

          classic -> new dimensions.three.recursive.catseval.rabbitMQ.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.catseval.rabbitMQ.QueensClassicCb(given_Validator)

        )

      )

  }

  package bis {

    private[breed] object ConfigByRabbitMQ {

      private given                          (using Nothing => Nothing)
                   : Validator = Validator()

      import dimensions.third.bis.Queens

      def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

        return Map(

          iterative -> Map(

            classic -> new dimensions.three.iterative.rabbitMQ.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.iterative.rabbitMQ.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.iterative.rabbitMQ.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.iterative.rabbitMQ.bis.QueensStepperP(given_Validator)

          ),

          continuation -> Map(

            classic -> new dimensions.three.recursive.continuation.rabbitMQ.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.continuation.rabbitMQ.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.continuation.rabbitMQ.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.continuation.rabbitMQ.bis.QueensStepperP(given_Validator)

          ),

          extern -> Map(

            classic -> new dimensions.three.recursive.extern.rabbitMQ.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.extern.rabbitMQ.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.extern.rabbitMQ.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.extern.rabbitMQ.bis.QueensStepperP(given_Validator)

          ),

          native -> Map(

            classic -> new dimensions.three.recursive.native.rabbitMQ.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.native.rabbitMQ.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.native.rabbitMQ.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.native.rabbitMQ.bis.QueensStepperP(given_Validator)

          ),

          closure -> Map(

            classic -> new dimensions.three.recursive.closure.rabbitMQ.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.closure.rabbitMQ.bis.QueensClassicCb(given_Validator),

          ),

          trampoline -> Map(

            classic -> new dimensions.three.recursive.trampoline.rabbitMQ.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.trampoline.rabbitMQ.bis.QueensClassicCb(given_Validator),

          ),

          tailcall -> Map(

            classic -> new dimensions.three.recursive.tailcall.rabbitMQ.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.tailcall.rabbitMQ.bis.QueensClassicCb(given_Validator)

          ),

          catseval -> Map(

            classic -> new dimensions.three.recursive.catseval.rabbitMQ.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.catseval.rabbitMQ.bis.QueensClassicCb(given_Validator)

          )

        )

    }

  }

}


package awsSQS {

  private[breed] object ConfigByAWSSQS {

    private given                          (using Nothing => Nothing)
                 : Validator = Validator()

    import dimensions.third.Queens

    def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

      return Map(

        iterative -> Map(

          classic -> new dimensions.three.iterative.awsSQS.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.iterative.awsSQS.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.iterative.awsSQS.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.iterative.awsSQS.QueensStepperP(given_Validator)

        ),

        continuation -> Map(

          classic -> new dimensions.three.recursive.continuation.awsSQS.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.continuation.awsSQS.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.continuation.awsSQS.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.continuation.awsSQS.QueensStepperP(given_Validator)

        ),

        extern -> Map(

          classic -> new dimensions.three.recursive.extern.awsSQS.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.extern.awsSQS.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.extern.awsSQS.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.extern.awsSQS.QueensStepperP(given_Validator)

        ),

        native -> Map(

          classic -> new dimensions.three.recursive.native.awsSQS.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.native.awsSQS.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.native.awsSQS.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.native.awsSQS.QueensStepperP(given_Validator)

        ),

        closure -> Map(

          classic -> new dimensions.three.recursive.closure.awsSQS.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.closure.awsSQS.QueensClassicCb(given_Validator),

        ),

        trampoline -> Map(

          classic -> new dimensions.three.recursive.trampoline.awsSQS.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.trampoline.awsSQS.QueensClassicCb(given_Validator),

        ),

        tailcall -> Map(

          classic -> new dimensions.three.recursive.tailcall.awsSQS.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.tailcall.awsSQS.QueensClassicCb(given_Validator)

        ),

        catseval -> Map(

          classic -> new dimensions.three.recursive.catseval.awsSQS.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.catseval.awsSQS.QueensClassicCb(given_Validator)

        )

      )

  }

  package bis {

    private[breed] object ConfigByAWSSQS {

      private given                          (using Nothing => Nothing)
                   : Validator = Validator()

      import dimensions.third.bis.Queens

      def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

        return Map(

          iterative -> Map(

            classic -> new dimensions.three.iterative.awsSQS.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.iterative.awsSQS.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.iterative.awsSQS.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.iterative.awsSQS.bis.QueensStepperP(given_Validator)

          ),

          continuation -> Map(

            classic -> new dimensions.three.recursive.continuation.awsSQS.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.continuation.awsSQS.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.continuation.awsSQS.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.continuation.awsSQS.bis.QueensStepperP(given_Validator)

          ),

          extern -> Map(

            classic -> new dimensions.three.recursive.extern.awsSQS.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.extern.awsSQS.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.extern.awsSQS.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.extern.awsSQS.bis.QueensStepperP(given_Validator)

          ),

          native -> Map(

            classic -> new dimensions.three.recursive.native.awsSQS.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.native.awsSQS.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.native.awsSQS.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.native.awsSQS.bis.QueensStepperP(given_Validator)

          ),

          closure -> Map(

            classic -> new dimensions.three.recursive.closure.awsSQS.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.closure.awsSQS.bis.QueensClassicCb(given_Validator),

          ),

          trampoline -> Map(

            classic -> new dimensions.three.recursive.trampoline.awsSQS.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.trampoline.awsSQS.bis.QueensClassicCb(given_Validator),

          ),

          tailcall -> Map(

            classic -> new dimensions.three.recursive.tailcall.awsSQS.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.tailcall.awsSQS.bis.QueensClassicCb(given_Validator)

          ),

          catseval -> Map(

            classic -> new dimensions.three.recursive.catseval.awsSQS.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.catseval.awsSQS.bis.QueensClassicCb(given_Validator)

          )

        )

    }

  }

}


package flow {

  private[breed] object ConfigByFlow {

    private given                          (using Nothing => Nothing)
                 : Validator = Validator()

    import dimensions.third.Queens

    def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

      return Map(

        iterative -> Map(

          classic -> new dimensions.three.iterative.flow.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.iterative.flow.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.iterative.flow.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.iterative.flow.QueensStepperP(given_Validator)

        ),

        continuation -> Map(

          classic -> new dimensions.three.recursive.continuation.flow.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.continuation.flow.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.continuation.flow.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.continuation.flow.QueensStepperP(given_Validator)

        ),

        extern -> Map(

          classic -> new dimensions.three.recursive.extern.flow.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.extern.flow.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.extern.flow.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.extern.flow.QueensStepperP(given_Validator)

        ),

        native -> Map(

          classic -> new dimensions.three.recursive.native.flow.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.native.flow.QueensClassicCb(given_Validator),

          stepper1 -> new dimensions.three.recursive.native.flow.QueensStepper1(given_Validator),

          stepperP -> new dimensions.three.recursive.native.flow.QueensStepperP(given_Validator)

        ),

        closure -> Map(

          classic -> new dimensions.three.recursive.closure.flow.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.closure.flow.QueensClassicCb(given_Validator),

        ),

        trampoline -> Map(

          classic -> new dimensions.three.recursive.trampoline.flow.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.trampoline.flow.QueensClassicCb(given_Validator),

        ),

        tailcall -> Map(

          classic -> new dimensions.three.recursive.tailcall.flow.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.tailcall.flow.QueensClassicCb(given_Validator)

        ),

        catseval -> Map(

          classic -> new dimensions.three.recursive.catseval.flow.QueensClassic(given_Validator),

          classicCb -> new dimensions.three.recursive.catseval.flow.QueensClassicCb(given_Validator)

        )

      )

  }

  package bis {

    private[breed] object ConfigByFlow {

      private given                          (using Nothing => Nothing)
                   : Validator = Validator()

      import dimensions.third.bis.Queens

      def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

        return Map(

          iterative -> Map(

            classic -> new dimensions.three.iterative.flow.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.iterative.flow.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.iterative.flow.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.iterative.flow.bis.QueensStepperP(given_Validator)

          ),

          continuation -> Map(

            classic -> new dimensions.three.recursive.continuation.flow.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.continuation.flow.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.continuation.flow.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.continuation.flow.bis.QueensStepperP(given_Validator)

          ),

          extern -> Map(

            classic -> new dimensions.three.recursive.extern.flow.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.extern.flow.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.extern.flow.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.extern.flow.bis.QueensStepperP(given_Validator)

          ),

          native -> Map(

            classic -> new dimensions.three.recursive.native.flow.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.native.flow.bis.QueensClassicCb(given_Validator),

            stepper1 -> new dimensions.three.recursive.native.flow.bis.QueensStepper1(given_Validator),

            stepperP -> new dimensions.three.recursive.native.flow.bis.QueensStepperP(given_Validator)

          ),

          closure -> Map(

            classic -> new dimensions.three.recursive.closure.flow.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.closure.flow.bis.QueensClassicCb(given_Validator),

          ),

          trampoline -> Map(

            classic -> new dimensions.three.recursive.trampoline.flow.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.trampoline.flow.bis.QueensClassicCb(given_Validator),

          ),

          tailcall -> Map(

            classic -> new dimensions.three.recursive.tailcall.flow.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.tailcall.flow.bis.QueensClassicCb(given_Validator)

          ),

          catseval -> Map(

            classic -> new dimensions.three.recursive.catseval.flow.bis.QueensClassic(given_Validator),

            classicCb -> new dimensions.three.recursive.catseval.flow.bis.QueensClassicCb(given_Validator)

          )

        )

    }

  }

  package `0` {

    package x {

      package sync {

        private[breed] object ConfigByFlow {

          private given                          (using Nothing => Nothing)
                       : Validator = Validator()

          import dimensions.third.Queens

          def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

            return Map(

              iterative -> Map(

                classic -> new dimensions.three.zeroth.iterative.flow.x.sync.QueensClassic(given_Validator),

                classicCb -> new dimensions.three.zeroth.iterative.flow.x.sync.QueensClassicCb(given_Validator),

                stepper1 -> new dimensions.three.zeroth.iterative.flow.x.sync.QueensStepper1(given_Validator),

                stepperP -> new dimensions.three.zeroth.iterative.flow.x.sync.QueensStepperP(given_Validator)

              ),

              continuation -> Map(

                classic -> new dimensions.three.zeroth.recursive.continuation.flow.x.sync.QueensClassic(given_Validator),

                classicCb -> new dimensions.three.zeroth.recursive.continuation.flow.x.sync.QueensClassicCb(given_Validator)

              ),

              extern -> Map(

                classic -> new dimensions.three.zeroth.recursive.extern.flow.x.sync.QueensClassic(given_Validator),

                classicCb -> new dimensions.three.zeroth.recursive.extern.flow.x.sync.QueensClassicCb(given_Validator),

                stepper1 -> new dimensions.three.zeroth.recursive.extern.flow.x.sync.QueensStepper1(given_Validator),

                stepperP -> new dimensions.three.zeroth.recursive.extern.flow.x.sync.QueensStepperP(given_Validator)

              )

            )

        }

      }

      package async {

        private[breed] object ConfigByFlow {

          private given                          (using Nothing => Nothing)
                       : Validator = Validator()

          import dimensions.third.Queens

          def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

            return Map(

              iterative -> Map(

                classic -> new dimensions.three.zeroth.iterative.flow.x.async.QueensClassic(given_Validator),

                classicCb -> new dimensions.three.zeroth.iterative.flow.x.async.QueensClassicCb(given_Validator),

                stepper1 -> new dimensions.three.zeroth.iterative.flow.x.async.QueensStepper1(given_Validator),

                stepperP -> new dimensions.three.zeroth.iterative.flow.x.async.QueensStepperP(given_Validator)

              ),

              continuation -> Map(

                classic -> new dimensions.three.zeroth.recursive.continuation.flow.x.async.QueensClassic(given_Validator),

                classicCb -> new dimensions.three.zeroth.recursive.continuation.flow.x.async.QueensClassicCb(given_Validator)

              ),

              extern -> Map(

                classic -> new dimensions.three.zeroth.recursive.extern.flow.x.async.QueensClassic(given_Validator),

                classicCb -> new dimensions.three.zeroth.recursive.extern.flow.x.async.QueensClassicCb(given_Validator),

                stepper1 -> new dimensions.three.zeroth.recursive.extern.flow.x.async.QueensStepper1(given_Validator),

                stepperP -> new dimensions.three.zeroth.recursive.extern.flow.x.async.QueensStepperP(given_Validator)

              )

            )

        }

      }

    }

    package s {

      private[breed] object ConfigByFlow {

        private given                          (using Nothing => Nothing)
                     : Validator = Validator()

        import dimensions.third.Queens

        def queens: Map[Algorithm, Map[Aspect, Queens[?]]] =

          return Map(

            iterative -> Map(

              classic -> new dimensions.three.zeroth.iterative.flow.s.QueensClassic(given_Validator),

              classicCb -> new dimensions.three.zeroth.iterative.flow.s.QueensClassicCb(given_Validator),

              stepper1 -> new dimensions.three.zeroth.iterative.flow.s.QueensStepper1(given_Validator),

              stepperP -> new dimensions.three.zeroth.iterative.flow.s.QueensStepperP(given_Validator)

            ),

            continuation -> Map(

              classic -> new dimensions.three.zeroth.recursive.continuation.flow.s.QueensClassic(given_Validator),

              classicCb -> new dimensions.three.zeroth.recursive.continuation.flow.s.QueensClassicCb(given_Validator)

            ),

            extern -> Map(

              classic -> new dimensions.three.zeroth.recursive.extern.flow.s.QueensClassic(given_Validator),

              classicCb -> new dimensions.three.zeroth.recursive.extern.flow.s.QueensClassicCb(given_Validator),

              stepper1 -> new dimensions.three.zeroth.recursive.extern.flow.s.QueensStepper1(given_Validator),

              stepperP -> new dimensions.three.zeroth.recursive.extern.flow.s.QueensStepperP(given_Validator)

            )

          )

      }

    }

  }

}
