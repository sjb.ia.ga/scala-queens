package queens
package dimensions.three
package zeroth
package iterative
package flow

import base.Board

import cache.CacheOpsDelegateTo

import common.monad.Item

import common.stepper.dup.factory.given


package x:

  import monix.eval.{ Coeval, Task }

  package sync:

    import dimensions.second.third.first.zeroth.iterative.flow.x.AbstractQueensStepper1

    class QueensStepper1(
      override val validate: Validator,
      override protected val fs: Item[Solution] => Boolean*
    ) extends AbstractQueensStepper1[Coeval]
        with dimensions.third.first.zeroth.flow.x.`*`.`QueensStepper1*`[Coeval] {

      override protected type * = QueensStepper1

      override protected def `apply*`(s: Item[Solution] => Boolean*): * =
        new QueensStepper1(validate, s*) {

          override protected val cacheOps = {
            val delegate = QueensStepper1.this.cacheOps
            new CacheOpsDelegateTo(delegate.self)
          }

          QueensStepper1.this.cacheOps.get(QueensStepper1.this.cacheKey) match {
            case Some(solutions) =>
              cacheOps.put(cacheKey, solutions)
            case _ =>
          }

        }

    }

  package async:

    import dimensions.second.third.first.zeroth.iterative.flow.x.AbstractQueensStepper1

    class QueensStepper1(
      override val validate: Validator,
      override protected val fs: Item[Solution] => Boolean*
    ) extends AbstractQueensStepper1[Task]
        with dimensions.third.first.zeroth.flow.x.`*`.`QueensStepper1*`[Task] {

      override protected type * = QueensStepper1

      override protected def `apply*`(s: Item[Solution] => Boolean*): * =
        new QueensStepper1(validate, s*) {

          override protected val cacheOps = {
            val delegate = QueensStepper1.this.cacheOps
            new CacheOpsDelegateTo(delegate.self)
          }

          QueensStepper1.this.cacheOps.get(QueensStepper1.this.cacheKey) match {
            case Some(solutions) =>
              cacheOps.put(cacheKey, solutions)
            case _ =>
          }

        }

    }


package s:

  import dimensions.second.third.first.zeroth.iterative.flow.s.AbstractQueensStepper1

  class QueensStepper1(
    override val validate: Validator,
    override protected val fs: Item[Solution] => Boolean*
  ) extends AbstractQueensStepper1
      with dimensions.third.first.zeroth.flow.s.`*`.`QueensStepper1*` {

    override protected type * = QueensStepper1

    override protected def `apply*`(s: Item[Solution] => Boolean*): * =
      new QueensStepper1(validate, s*) {

        override protected val cacheOps = {
          val delegate = QueensStepper1.this.cacheOps
          new CacheOpsDelegateTo(delegate.self)
        }

        QueensStepper1.this.cacheOps.get(QueensStepper1.this.cacheKey) match {
          case Some(solutions) =>
            cacheOps.put(cacheKey, solutions)
          case _ =>
        }

      }

  }
