package queens
package dimensions.three
package zeroth
package iterative
package flow

import base.Board

import cache.CacheOpsDelegateTo

import common.monad.Item

import common.stepper.dup.factory.given


package x:

  import monix.eval.{ Coeval, Task }

  package sync:

    import dimensions.second.third.first.zeroth.iterative.flow.x.AbstractQueensStepperP

    class QueensStepperP(
      override val validate: Validator,
      override protected val fs: Item[Solution] => Boolean*
    ) extends AbstractQueensStepperP[Coeval]
        with dimensions.third.first.zeroth.flow.x.`*`.`QueensStepperP*`[Coeval] {

      override protected type * = QueensStepperP

      override protected def `apply*`(s: Item[Solution] => Boolean*): * =
        new QueensStepperP(validate, s*) {

          override protected val cacheOps = {
            val delegate = QueensStepperP.this.cacheOps
            new CacheOpsDelegateTo(delegate.self)
          }

          QueensStepperP.this.cacheOps.get(QueensStepperP.this.cacheKey) match {
            case Some(solutions) =>
              cacheOps.put(cacheKey, solutions)
            case _ =>
          }

        }

    }

  package async:

    import dimensions.second.third.first.zeroth.iterative.flow.x.AbstractQueensStepperP

    class QueensStepperP(
      override val validate: Validator,
      override protected val fs: Item[Solution] => Boolean*
    ) extends AbstractQueensStepperP[Task]
        with dimensions.third.first.zeroth.flow.x.`*`.`QueensStepperP*`[Task] {

      override protected type * = QueensStepperP

      override protected def `apply*`(s: Item[Solution] => Boolean*): * =
        new QueensStepperP(validate, s*) {

          override protected val cacheOps = {
            val delegate = QueensStepperP.this.cacheOps
            new CacheOpsDelegateTo(delegate.self)
          }

          QueensStepperP.this.cacheOps.get(QueensStepperP.this.cacheKey) match {
            case Some(solutions) =>
              cacheOps.put(cacheKey, solutions)
            case _ =>
          }

        }

    }


package s:

  import dimensions.second.third.first.zeroth.iterative.flow.s.AbstractQueensStepperP

  class QueensStepperP(
    override val validate: Validator,
    override protected val fs: Item[Solution] => Boolean*
  ) extends AbstractQueensStepperP
      with dimensions.third.first.zeroth.flow.s.`*`.`QueensStepperP*` {

    override protected type * = QueensStepperP

    override protected def `apply*`(s: Item[Solution] => Boolean*): * =
      new QueensStepperP(validate, s*) {

        override protected val cacheOps = {
          val delegate = QueensStepperP.this.cacheOps
          new CacheOpsDelegateTo(delegate.self)
        }

        QueensStepperP.this.cacheOps.get(QueensStepperP.this.cacheKey) match {
          case Some(solutions) =>
            cacheOps.put(cacheKey, solutions)
          case _ =>
        }

      }

  }
