package queens
package dimensions.three
package zeroth
package iterative
package flow

import base.Board

import cache.CacheOpsDelegateTo

import common.monad.Item


package x:

  import monix.eval.{ Coeval, Task }

  package sync:

    import dimensions.second.third.first.zeroth.iterative.flow.x.AbstractQueensClassic

    class QueensClassic(
      override val validate: Validator,
      override protected val fs: Item[Solution] => Boolean*
    ) extends AbstractQueensClassic[Coeval]
        with dimensions.third.first.zeroth.flow.x.QueensClassic[Coeval] {

      override protected type * = QueensClassic

      override protected def `apply*`(s: Item[Solution] => Boolean*): * =
        new QueensClassic(validate, s*) {

          override protected val cacheOps = {
            val delegate = QueensClassic.this.cacheOps
            new CacheOpsDelegateTo(delegate.self)
          }

          QueensClassic.this.cacheOps.get(QueensClassic.this.cacheKey) match {
            case Some(solutions) =>
              cacheOps.put(cacheKey, solutions)
            case _ =>
          }

        }

    }

  package async:

    import dimensions.second.third.first.zeroth.iterative.flow.x.AbstractQueensClassic

    class QueensClassic(
      override val validate: Validator,
      override protected val fs: Item[Solution] => Boolean*
    ) extends AbstractQueensClassic[Task]
        with dimensions.third.first.zeroth.flow.x.QueensClassic[Task] {

      override protected type * = QueensClassic

      override protected def `apply*`(s: Item[Solution] => Boolean*): * =
        new QueensClassic(validate, s*) {

          override protected val cacheOps = {
            val delegate = QueensClassic.this.cacheOps
            new CacheOpsDelegateTo(delegate.self)
          }

          QueensClassic.this.cacheOps.get(QueensClassic.this.cacheKey) match {
            case Some(solutions) =>
              cacheOps.put(cacheKey, solutions)
            case _ =>
          }

        }

    }


package s:

  import dimensions.second.third.first.zeroth.iterative.flow.s.AbstractQueensClassic

  class QueensClassic(
    override val validate: Validator,
    override protected val fs: Item[Solution] => Boolean*
  ) extends AbstractQueensClassic
      with dimensions.third.first.zeroth.flow.s.QueensClassic {

    override protected type * = QueensClassic

    override protected def `apply*`(s: Item[Solution] => Boolean*): * =
      new QueensClassic(validate, s*) {

        override protected val cacheOps = {
          val delegate = QueensClassic.this.cacheOps
          new CacheOpsDelegateTo(delegate.self)
        }

        QueensClassic.this.cacheOps.get(QueensClassic.this.cacheKey) match {
          case Some(solutions) =>
            cacheOps.put(cacheKey, solutions)
          case _ =>
        }

      }

  }
