package queens
package dimensions.fourth
package zeroth
package program
package monad

import conf.Conf


package x:

  import cats.effect.Sync

  import monix.tail.Iterant

  import base.`0`.x.QueensMonad
  import dimensions.third.zeroth.Queens

  abstract trait QueensMonadProgramInterface[E[_]: Sync,
    P <: Conf
  ] extends QueensMonad[E, P]
      with QueensInterface:

    protected type It <: AnyVal

    protected type Q <: Queens[E, Iterant, ?, It]

    protected def queens: Q


package s:

  import cats.effect.IO

  import fs2.Stream

  import base.`0`.s.QueensMonad
  import dimensions.third.zeroth.Queens

  abstract trait QueensMonadProgramInterface[
    P <: Conf
  ] extends QueensMonad[P]
      with QueensInterface:

    protected type It <: AnyVal

    protected type Q <: Queens[IO, Stream, ?, It]

    protected def queens: Q
