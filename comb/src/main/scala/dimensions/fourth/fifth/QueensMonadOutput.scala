package queens
package dimensions.fourth.fifth
package zeroth

import dimensions.fifth.output.QueensOutputUseSolution

import conf.Conf


package x:

  import cats.effect.Sync

  import dimensions.fourth.zeroth.program.monad.x.QueensMonadProgramInterface

  abstract trait QueensMonadOutput[E[_]: Sync,
    P <: Conf
  ] extends QueensOutputUseSolution
      with QueensMonadProgramInterface[E, P]:

    /*
     * @param predicate: exposes the otherwise protected member
     *                   @code [[queens]], which can thus be captured
     */
    final def apply(predicate: Q => Boolean): Boolean = predicate(queens)

    override def toString(): String = "Monix Monad Program " + super.toString


package s:

  import dimensions.fourth.zeroth.program.monad.s.QueensMonadProgramInterface

  abstract trait QueensMonadOutput[
    P <: Conf
  ] extends QueensOutputUseSolution
      with QueensMonadProgramInterface[P]:

    /*
     * @param predicate: exposes the otherwise protected member
     *                   @code [[queens]], which can thus be captured
     */
    final def apply(predicate: Q => Boolean): Boolean = predicate(queens)

    override def toString(): String = "FS2 Monad Program " + super.toString
