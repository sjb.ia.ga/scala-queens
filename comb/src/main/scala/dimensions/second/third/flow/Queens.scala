package queens
package dimensions.second.third
package zeroth
package flow


package x:

  import cats.effect.Sync

  abstract trait Queens[E[_]: Sync, At]
      extends dimensions.second.Queens[At]
      with dimensions.third.first.zeroth.flow.x.Queens[E, At]


package s:

  abstract trait Queens[At]
      extends dimensions.second.Queens[At]
      with dimensions.third.first.zeroth.flow.s.Queens[At]
