package queens
package dimensions.second.third
package zeroth
package flow

import common.geom.Coord
import common.stepper.{ BoardStepper => BoardStepperType }
import common.stepper.BoardStepperFactory


package x:

  import cats.effect.Sync

  abstract class AbstractQueensBoardStepper[E[_]: Sync,
    S <: BoardStepperType,
    F <: BoardStepperFactory[S]
  ](using override val f: F)
      extends AbstractQueens[E, Coord[S]]
      with dimensions.second.BoardStepper[S, F]


package s:

  abstract class AbstractQueensBoardStepper[
    S <: BoardStepperType,
    F <: BoardStepperFactory[S]
  ](using override val f: F)
      extends AbstractQueens[Coord[S]]
      with dimensions.second.BoardStepper[S, F]
