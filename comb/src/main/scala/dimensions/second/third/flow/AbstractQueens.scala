package queens
package dimensions.second.third
package zeroth
package flow


package x:

  import cats.effect.Sync

  abstract class AbstractQueens[E[_]: Sync, At]
      extends Queens[E, At]:

    override def toString(): String = tags mkString " "


package s:

  abstract class AbstractQueens[At]
      extends Queens[At]:

    override def toString(): String = tags mkString " "
