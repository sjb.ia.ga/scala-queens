package queens
package dimensions.second.third.first
package zeroth
package iterative
package flow

import common.stepper.dup.{ BoardStepperDup => BoardStepperType }
import common.stepper.dup.{ BoardStepperDupFactory => BoardStepperFactory }
import common.geom.Coord


package x:

  import cats.effect.Sync

  abstract class AbstractQueensBoardStepper[E[_]: Sync,
    S <: BoardStepperType,
    F <: BoardStepperFactory[S],
  ](using F)
      extends dimensions.second.third.zeroth.flow.x.AbstractQueensBoardStepper[E, S, F]
      with Queens[E, Coord[S]]
      with dimensions.second.iterative.`BoardStepper*`[S, F]


package s:

  abstract class AbstractQueensBoardStepper[
    S <: BoardStepperType,
    F <: BoardStepperFactory[S],
  ](using F)
      extends dimensions.second.third.zeroth.flow.s.AbstractQueensBoardStepper[S, F]
      with Queens[Coord[S]]
      with dimensions.second.iterative.`BoardStepper*`[S, F]
