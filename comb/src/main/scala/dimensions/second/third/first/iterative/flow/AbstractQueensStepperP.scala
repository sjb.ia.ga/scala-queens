package queens
package dimensions.second.third.first
package zeroth
package iterative
package flow

import common.stepper.dup.{ StepperPDup => StepperPType }
import common.stepper.dup.{ BoardStepperDupFactory => BoardStepperFactory }


package x:

  import cats.effect.Sync

  abstract class AbstractQueensStepperP[E[_]: Sync](using BoardStepperFactory[StepperPType])
      extends AbstractQueensBoardStepper[E, StepperPType, BoardStepperFactory[StepperPType]]
      with dimensions.second.iterative.`StepperP*`


package s:

  abstract class AbstractQueensStepperP(using BoardStepperFactory[StepperPType])
      extends AbstractQueensBoardStepper[StepperPType, BoardStepperFactory[StepperPType]]
      with dimensions.second.iterative.`StepperP*`
