package queens
package dimensions.second.third.first
package zeroth
package recursive
package continuation
package flow


package x:

  import cats.effect.Sync

  abstract class AbstractQueensClassicCb[E[_]: Sync]
      extends AbstractQueens[E, Point]
      with dimensions.second.recursive.continuation.ClassicCb


package s:

  abstract class AbstractQueensClassicCb
      extends AbstractQueens[Point]
      with dimensions.second.recursive.continuation.ClassicCb
