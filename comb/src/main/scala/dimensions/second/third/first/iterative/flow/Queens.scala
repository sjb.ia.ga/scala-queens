package queens
package dimensions.second.third.first
package zeroth
package iterative
package flow


package x:

  import cats.effect.Sync

  abstract trait Queens[E[_]: Sync, At]
      extends dimensions.second.iterative.Queens[At]
      with dimensions.second.third.zeroth.flow.x.Queens[E, At]


package s:

  abstract trait Queens[At]
      extends dimensions.second.iterative.Queens[At]
      with dimensions.second.third.zeroth.flow.s.Queens[At]
