package queens
package dimensions.second.third.first
package zeroth
package recursive
package extern
package flow


package x:

  import cats.effect.Sync

  abstract class AbstractQueens[E[_]: Sync, At]
      extends dimensions.second.third.zeroth.flow.x.AbstractQueens[E, At]
      with Queens[E, At]


package s:

  abstract class AbstractQueens[At]
      extends dimensions.second.third.zeroth.flow.s.AbstractQueens[At]
      with Queens[At]
