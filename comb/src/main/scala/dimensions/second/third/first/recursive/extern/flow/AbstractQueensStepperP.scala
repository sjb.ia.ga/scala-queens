package queens
package dimensions.second.third.first
package zeroth
package recursive
package extern
package flow

import common.stepper.{ StepperP => StepperPType }
import common.stepper.BoardStepperFactory


package x:

  import cats.effect.Sync

  abstract class AbstractQueensStepperP[E[_]: Sync](using BoardStepperFactory[StepperPType])
      extends AbstractQueensBoardStepper[E, StepperPType, BoardStepperFactory[StepperPType]]
      with dimensions.second.recursive.extern.StepperP


package s:

  abstract class AbstractQueensStepperP(using BoardStepperFactory[StepperPType])
      extends AbstractQueensBoardStepper[StepperPType, BoardStepperFactory[StepperPType]]
      with dimensions.second.recursive.extern.StepperP
