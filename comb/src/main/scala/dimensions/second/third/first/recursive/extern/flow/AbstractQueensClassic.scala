package queens
package dimensions.second.third.first
package zeroth
package recursive
package extern
package flow


package x:

  import cats.effect.Sync

  abstract class AbstractQueensClassic[E[_]: Sync]
      extends AbstractQueens[E, Point]
      with dimensions.second.recursive.extern.Classic


package s:

  abstract class AbstractQueensClassic
      extends AbstractQueens[Point]
      with dimensions.second.recursive.extern.Classic
