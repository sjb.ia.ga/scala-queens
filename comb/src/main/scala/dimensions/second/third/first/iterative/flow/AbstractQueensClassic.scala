package queens
package dimensions.second.third.first
package zeroth
package iterative
package flow


package x:

  import cats.effect.Sync

  abstract class AbstractQueensClassic[E[_]: Sync]
      extends AbstractQueens[E, Point]
      with dimensions.second.iterative.Classic


package s:

  abstract class AbstractQueensClassic
      extends AbstractQueens[Point]
      with dimensions.second.iterative.Classic
