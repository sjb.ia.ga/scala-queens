package queens
package dimensions.second.third.first
package zeroth
package iterative
package flow

import common.stepper.dup.{ Stepper1Dup => Stepper1Type }
import common.stepper.dup.{ BoardStepperDupFactory => BoardStepperFactory }


package x:

  import cats.effect.Sync

  abstract class AbstractQueensStepper1[E[_]: Sync](using BoardStepperFactory[Stepper1Type])
      extends AbstractQueensBoardStepper[E, Stepper1Type, BoardStepperFactory[Stepper1Type]]
      with dimensions.second.iterative.`Stepper1*`


package s:

  abstract class AbstractQueensStepper1(using BoardStepperFactory[Stepper1Type])
      extends AbstractQueensBoardStepper[Stepper1Type, BoardStepperFactory[Stepper1Type]]
      with dimensions.second.iterative.`Stepper1*`
