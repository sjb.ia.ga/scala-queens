package queens
package plane.fourth.fifth.third

import scala.util.control.NonLocalReturns.{ returning, throwReturn => thr }

import base.{ Board, EmptyBoard }

import common.NoTag

import dimensions.third.first.flow.{ Iteration, `Queens*` }

import dimensions.three.breed.Hatch
import Hatch.given

import dimensions.Dimension.Aspect.classic
import dimensions.Dimension.Algorithm.native
import dimensions.Dimension.Model.flow

import version.less.nest.Nest


object TryBoard:

  def apply(): Unit =
    System.clearProperty(_root_.queens.main.Properties.Validator.optimized)
    System.clearProperty(_root_.queens.main.Properties.DependencyInjection.config)
    System.clearProperty(_root_.queens.main.Properties.DependencyInjection.build)
    System.clearProperty(_root_.queens.main.Properties.DependencyInjection.guice)
    System.clearProperty(_root_.queens.main.Properties.DependencyInjection.macwire)
    System.clearProperty(_root_.queens.main.Properties.CipherServer.port)
    System.clearProperty("scala.concurrent.context.maxThreads")
    System.clearProperty("scala.concurrent.context.maxExtraThreads")
    System.clearProperty("scala.concurrent.context.numThreads")
    System.clearProperty("scala.concurrent.context.minThreads")
    System.clearProperty("java.util.concurrent.ForkJoinPool.common.parallelism")

  def apply(N: Int, empty: Boolean = false, max: Long = 100)(using seed: Option[Long] = None): Board =

    var n = 0

    while n < max do

      n += 1

      given Board = if empty then EmptyBoard(N) else Board(N)

      if given_Board.isEmpty then
        return given_Board

      if given_Board.N < 3 then
        return given_Board

      given Validator = Validator()

      import dimensions.three.breed.builders.Builder

      val hatch =
        Builder
          .byModel(flow)
          .withAlgorithm(native)
          .andAspect(classic)
          .breed[`Queens*`[?]]

      val queens = hatch()

      given Iteration = Iteration(given_Board, NoTag, false, Right(false), common.Flag(), () => ())

      given Long = 1 // max solutions

      returning {
        queens.foreach { _ =>
          thr(given_Board)
        }
      }

    if empty then EmptyBoard(N) else Board(N)
