package queens
package common
package monad

import java.util.UUID
import java.util.Date

import base.Board

import common.monad.Grow


package `0`:

  package flow:

    import version.less.nest.Nest

    final case class Just[T](override val board: Board,
                             override val tag: T,
                             override val uuid: UUID,
                             override val number: Long,
                             started: Date,
                             ended: Date)
        extends Grow.Just[T]

    implicit class ToJust[T](using tag: Any => T)
        extends Grow.ToJust[T, Date]:
      override type J = Just[T]
      inline def apply(r: (Board, T, UUID, Long, Date, Date)): J =
        Just(r._1, r._2, r._3, r._4, r._5, r._6)
