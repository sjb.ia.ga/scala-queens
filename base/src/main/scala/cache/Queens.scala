package queens
package cache

import base.CacheOps


abstract trait Queens:

  final protected val cacheKey: CacheOps#Key =
    CacheOps()

  protected val cacheOps: CacheOps =
    new CacheOps.BufferCache
//    CacheOps.WithoutCache
