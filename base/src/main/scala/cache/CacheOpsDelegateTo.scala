package queens
package cache

import base.CacheOps


class CacheOpsDelegateTo(delegate: CacheOps)
    extends CacheOps {

  override def put(key: Key, it: LazyList[Solution]): Unit =
    delegate.put(key, it)

  override def get(key: Key): Option[LazyList[Solution]] =
    delegate.get(key)

  override def invalidate(key: Key): Unit =
    delegate.invalidate(key)

  override val self = delegate

}
