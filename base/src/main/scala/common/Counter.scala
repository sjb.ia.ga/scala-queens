package queens
package common


case class Counter private (
  step: Long
)(
  private var count: Long = 1 - step,
  private var counter: Long = 0,
  private var number: Long = 0
):
  def this(preempt: Boolean) = this(if preempt then 1 else 0)()

  inline def unary_+ : Unit = counter += step

  inline def unary_~ : Long = number

  inline def apply(count: Long): Unit =
//    assert(counter == this.count)
    this.count = count.abs
    counter = 0
    number += 1

  inline def ::(number: Long) : Unit =
    count = counter
    this.number = number


object Counter:

  inline given Conversion[Counter, Long] = _.count
  inline given Conversion[Counter, Boolean] = { it => it.counter < it.count }
