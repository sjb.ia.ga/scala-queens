package queens
package common

import scala.quoted.{ Expr, Type, Quotes }


object Macros:

  def trycCode[R](expr: Expr[R])(using Type[R], Quotes): Expr[Option[R]] = '{ try Some($expr) catch _ => None }

  inline def tryc[R](inline expr: => R): Option[R] = ${ trycCode('expr) }

  def trysCode(expr: Expr[Long])(using Quotes): Expr[Unit] =
   '{
      var now = System.currentTimeMillis
      var sleep = $expr
      while sleep > 0L
      do
        var since = now
        try
          Thread.sleep(sleep)
          sleep = 0L
        catch InterruptedException =>
          now = System.currentTimeMillis
          sleep -= now - since
    }

  inline def trys(inline expr: => Long): Unit = ${ trysCode('expr) }

////////////////////////////////////////////////////////////////////////////////

  def busywaitWhileCode(cond: Expr[Boolean],
                        timeout: Expr[Long], poll: Expr[Long],
                        expr: Expr[Unit])
                 (using Quotes): Expr[Boolean] =
   '{
      if $timeout == 0
      then
        !$cond
      else
        var elapsed = 0L
        var since = System.currentTimeMillis
        while elapsed < $timeout && $cond
        do
          trys($poll)
          val now = System.currentTimeMillis
          elapsed += now - since
          since = now
          $expr
        elapsed < $timeout
    }

  inline def busywaitWhile(inline cond: => Boolean)
                          (inline timeout: Long, inline poll: Long)
                          (inline expr: => Unit = {}): Boolean =
   ${ busywaitWhileCode('cond, 'timeout, 'poll, 'expr) }

////////////////////////////////////////////////////////////////////////////////

  def busywaitWhileCode(cond: Expr[Boolean],
                        poll: Expr[Long])
                 (using Quotes): Expr[Unit] =
   '{
      while $cond
      do
        trys($poll)
    }

  inline def busywaitWhile(inline cond: => Boolean)
                          (inline poll: Long): Unit =
   ${ busywaitWhileCode('cond, 'poll) }

////////////////////////////////////////////////////////////////////////////////

  import java.util.concurrent.atomic.AtomicLong

  extension(self: AtomicLong)
    inline def busywaitUntil(value: Long)(poll: Long) =
      busywaitWhile(self.get != value)(poll)
