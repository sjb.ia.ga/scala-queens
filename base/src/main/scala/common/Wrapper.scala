package queens
package common


abstract trait Wrapper[T <: AnyRef]:
  var value: T = _
