package queens
package common

import java.util.concurrent.{ ConcurrentHashMap => Map }

import scala.collection.JavaConverters.mapAsScalaConcurrentMapConverter

import java.util.concurrent.atomic.AtomicBoolean

import scala.collection.mutable.{ ListBuffer => MutableList }

import scala.concurrent.duration.{ Duration, FiniteDuration }

import scala.collection.{ AbstractIterator, IterableOps, IterableFactory, IterableOnce }
import scala.collection.Iterable
import scala.collection.mutable.{ Builder, ImmutableBuilder, Growable }


/**
  * Adapted from source "Capped.scala" in chapter 25 of the book:
  * "Programming in Scala" - fourth edition, by Martin Odersky, Lex Spoon, Bill Venners
  *
  * @param elems - map with keys the sequence number of a queens' Solution
  *                    and values what results from map's or flatMap's block
  * @param poll - polling interval
  * @param alt - alternate source
  */
final class ParallelIterable[A] private (
  elems: Map[Long, Iterable[A]],
  poll: FiniteDuration,
  private val alt: Option[List[A]]
) extends Iterable[A]
    with IterableOps[A, ParallelIterable, ParallelIterable[A]]
    with Growable[(Long, Iterable[A])]:

  def this(poll: FiniteDuration = Duration.fromNanos(10000)) =
    this(new Map(), poll, None)

  /* alternate */
  private[common] def this(elems: ParallelIterable[A], elem: A) =
    this(null, Duration.fromNanos(0), Some(List.from[A](elems.alt.getOrElse(elems.*)) :+ elem))

  private val isDone: AtomicBoolean = new AtomicBoolean(alt.nonEmpty)

  def apply(): Unit = isDone.compareAndSet(false, true)

  private def * : List[A] =
    while !isDone.get() do
      try
        Thread.sleep(poll.toMillis)
      catch
        case _: InterruptedException =>
    val r = MutableList[A]()
    List.from(elems.asScala.keySet)
        .sorted
        .foreach { it => r.addAll(elems.asScala(it)) }
    r.toList

  override def knownSize: Int = alt.fold(elems.size)(_.length)

  override def iterator: Iterator[A] =
    new AbstractIterator[A]:
      private lazy val delegate: Iterator[A] =
        synchronized { alt.getOrElse(*).iterator }

      override def hasNext: Boolean =
        delegate.hasNext

      override def next(): A =
        delegate.next

  override def addOne(it: (Long, Iterable[A])): this.type =
    elems.put(it._1, it._2)
    this

  override def clear(): Unit =
    elems.clear()

  override val iterableFactory: IterableFactory[ParallelIterable] =
    new ParallelIterableFactory()

  override protected def fromSpecific(
    coll: IterableOnce[A]
  ): ParallelIterable[A] =
    iterableFactory.from(coll)

  override protected def newSpecificBuilder: Builder[A, ParallelIterable[A]] =
    iterableFactory.newBuilder

  override def empty: ParallelIterable[A] =
    iterableFactory.empty

  override def className = "ParallelIterable"


sealed class ParallelIterableFactory()
    extends IterableFactory[ParallelIterable]:

  override def from[A](source: IterableOnce[A]): ParallelIterable[A] =
    (newBuilder[A] ++= source).result()

  override def empty[A]: ParallelIterable[A] =
     new ParallelIterable[A]()

  override def newBuilder[A]: Builder[A, ParallelIterable[A]] =
    new ImmutableBuilder[A, ParallelIterable[A]](empty):
      override def addOne(elem: A): this.type =
        elems = new ParallelIterable[A](elems, elem)
        this
