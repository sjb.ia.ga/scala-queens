package queens
package common


case class Flag(slot: String = null):
  private var flag: Boolean = true
  def unary_! = flag = !flag


object Flag:

  given Conversion[Flag, Boolean] = _.flag
