package queens


package common:

  object Implicits {

    @inline implicit def it2it[T](self: Iterable[Option[T]]): Iterable[T] = self.filter(_.nonEmpty).map(_.get)

  }

  object NoTag:
    override def toString(): String = ""

  package object futures:

    import scala.concurrent.{ ExecutionContext, Future, SyncChannel }

    def `Future*`[R](block: Future[R] => R)(using ExecutionContext): Future[R] =
      val fch = new SyncChannel[Future[R]]()
      val f = Future { block(fch.read) }
      fch.write(f)
      f


package object common:

  inline given [T]: Conversion[Iterable[Option[T]], Iterable[T]] = _.filter(_.nonEmpty).map(_.get)

  import scala.reflect.ClassTag

  inline given [T]: Conversion[ClassTag[?], Class[T]] = _.runtimeClass.asInstanceOf[Class[T]]

  given [T]: Conversion[ClassTag[?], T] = { self => val clasz: Class[T] = self; clasz.newInstance }

  import scala.Option.when

  extension[T](its: Iterable[T])
    inline def :-[R](e: => R): Option[R] = when(its ne Nil)(e)
    inline def :*[R](e: => R): Option[R] = when(its eq Nil)(e)

  import java.util.UUID
