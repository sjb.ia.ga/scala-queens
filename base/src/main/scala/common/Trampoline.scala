package queens
package common

import scala.annotation.tailrec


/**
  * {{{
  * def sum(m: Int, n: Int): Int = m + n
  *
  * def prod(m: Int, n: Int): Int = m * n
  *
  * def fibonacci(k: Int): Trampoline[Int] =
  *   if k < 2
  *   then
  *     (math.random * 10).toInt % 3 match
  *       case 0 =>
  *         Done(k)
  *       case 1 =>
  *         for
  *           m <- Done(k)
  *         yield
  *           m
  *       case _ =>
  *         for
  *           m <- Done(k)
  *           n <- Done(k)
  *         yield
  *           prod(m, n)
  *   else
  *     for
  *       m <- Call { fibonacci(k - 2) }
  *       n <- Call { fibonacci(k - 1) }
  *     yield
  *       sum(n, m)
  * }}}
  */
sealed abstract trait Trampoline[+A]:
  import Trampoline._

  final def map[B](fun: A => B): Trampoline[B] =
    this.flatMap(fun andThen pure)

  final def flatMap[B](sequel: A => Trampoline[B]): Trampoline[B] =
    FlatMap(this, sequel)

  @tailrec
  final def apply(): A = this match
    case Done(value) => value
    case Call(closure) => closure(())()
    case FlatMap(Done(value), sequel) => sequel(value)()
    case FlatMap(Call(closure), sequel) => (closure :: sequel)(())()
    case FlatMap(FlatMap(self, prequel), sequel) => FlatMap(self, prequel :: sequel)()

object Trampoline:

  extension[A, B](prequel: A => Trampoline[B])
    inline def ::[C](sequel: B => Trampoline[C]): A => Trampoline[C] =
      prequel(_).flatMap(sequel)

  final case class Done[+A](value: A) extends Trampoline[A]

  final case class Call[A](closure: Unit => Trampoline[A]) extends Trampoline[A]

  final case class FlatMap[A, B](self: Trampoline[A], sequel: A => Trampoline[B]) extends Trampoline[B]

  def pure[A](a: A): Trampoline[A] = new Done(a)

  object Call:
    inline def apply[A](closure: => Trampoline[A]): Trampoline[A] = new Call(_ => closure)
