package queens


object Synchronized:

  @inline def apply[R](obj: Any = this)(expr: => R): R = obj.synced(expr)

  extension(obj: Any)
    private[Synchronized] def synced[R](expr: => R): R = synchronized(expr)
