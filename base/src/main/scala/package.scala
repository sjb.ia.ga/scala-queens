package object queens:

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]


package queens:

  object jQueens:

    // [size] [scheme://host:port] [media] ...
    def parse(s: String)(ms: String*)(to: Int = 58): Array[String] =

        var size_d: Int = 6
        var scheme_d: String = "http"
        var host_d: String = "localhost"
        var port_d: Int =
          if sys.BooleanProp
            .keyExists(_root_.queens.main.Properties.WebSimple.plainjava)
            .value
          then 8700 + to
          else 8000 + to
        var media_d: String = ms(0)

        def usage =
          println("Usage: [size] [[scheme://]host[:port]] [media] ...")
          println(s"Defaults: $size_d $scheme_d://$host_d:$port_d $media_d")

        var args = s.split(" ").take(3).mkString(" ")

        var size: Int = size_d
        var scheme: String = scheme_d
        var host: String = host_d
        var port: Int = port_d
        var media: String = media_d

        var withScheme = false
        var withPort = false
        var withSize = false
        var withMedia = false

        try // size, media
          var rest = args.split(" ")

          var id: Array[String] => Array[String] = identity
          var tail: Array[String] => Array[String] = _.tail
          var init: Array[String] => Array[String] = _.init
          var f: Array[String] => Array[String] = id

          try
            size = rest.headOption.map { it => f = tail; withSize = true; it.trim }.getOrElse(s"$size_d").toInt
          catch
            case _ => f = id; withSize = false; size = size_d
          rest = f(rest)

          f = id
          media = rest.lastOption.map { it => f = init; withMedia = true; it.trim }.getOrElse(media_d)
          if !ms.contains(media) then
            f = id; withMedia = false; media = media_d
          rest = f(rest)

          if rest.size > 1 then
            println(s"either '${rest(0)}' is not a number and")
            println(s"'${rest(1)}' is not a supported media type: ${ms.mkString(" | ")};")
            println(s"or, not one but two base URLs ('${rest(0)}' and '${rest(1)}') are specified")
            usage
            ???

          args = rest.headOption.getOrElse("")
        catch
          case _: NotImplementedError => ???
          case _ =>

        // [scheme://]host[:port]
        try
          val Array[String](sch, rest) = args.split("://")
          scheme = sch.trim
          withScheme = true
          args = rest
        catch
          case _ =>
        try // host[:port]
          val Array[String](h, p) = args.split(":")
          try
            host = h.trim
            port = p.trim.toInt
          catch
            case _ =>
              println(s"port ($p) is not a number")
              usage
              ???
          withPort = true
        catch
          case _: NotImplementedError => ???
          case _ => host = args.trim

        if host.isEmpty then host = host_d

        if !withSize then
          try
            host.toInt
            println(s"host ($host) cannot be the size")
            usage
            ???
          catch
            case _: NotImplementedError => ???
            case _ =>

        if host.contains("/") then
          if withMedia then
            println(s"host ($host) cannot contain '/'")
            usage
            ???
          else if ms.contains(host) then
            if !withScheme && !withPort then
              println(s"host ($host) cannot be a media type")
              usage
              ???
            else
              println(s"host ($host) cannot contain '/'")
              usage
              ???
          else
            val s = if ms.size == 1 then "" else "s"
            val is = if ms.size == 1 then "is" else "are"
            println(s"supported media type$s ($host) $is: ${ms.mkString(" | ")}")
            usage
            ???

        Array(s"$size", scheme, host, s"$port", media) ++ s.split(" ").drop(3)
