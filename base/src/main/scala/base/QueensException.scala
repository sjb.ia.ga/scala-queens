package queens
package base


abstract class QueensException(_msg: String, _throw: Throwable = null)
    extends RuntimeException(_msg, _throw)
