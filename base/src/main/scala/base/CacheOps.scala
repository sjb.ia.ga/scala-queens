package queens
package base

import java.util.UUID

import scala.collection.mutable.{ LongMap => Lengthy }


abstract trait CacheOps {

  type Key = UUID

  def put(key: Key, it: LazyList[Solution]): Unit

  def get(key: Key): Option[LazyList[Solution]]

  def invalidate(key: Key): Unit

  val self: CacheOps

}

object CacheOps {

  object WithoutCache
      extends CacheOps {

    override def put(key: Key, it: LazyList[Solution]): Unit =
      {}

    override def get(key: Key): Option[LazyList[Solution]] =
      None

    override def invalidate(key: Key): Unit =
      {}

    override val self: CacheOps =
      this

  }

  class BufferCache
      extends CacheOps {

    import java.util.concurrent.{ ConcurrentHashMap => Map }

    private val buffer: Map[Key, LazyList[Solution]] = new Map()

    override def put(key: Key, it: LazyList[Solution]): Unit =
      buffer.put(key, it)

    override def get(key: Key): Option[LazyList[Solution]] =
      if buffer.containsKey(key) then
        Some(buffer.get(key))
      else
        None

    override def invalidate(key: Key): Unit =
      buffer.clear()

    override val self: CacheOps =
      this

  }

  def apply() = UUID.randomUUID()

}
