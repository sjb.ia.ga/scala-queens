libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",

  "com.typesafe.akka" %% "akka-actor-typed" % "2.8.5",

  "co.fs2" %% "fs2-core" % "2.5.12",

  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
