package queens
package version.less
package nest

import org.scalatest.flatspec.AnyFlatSpec

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Algorithm._
import Model._

import version.base.Multiplier


class WildcardSpec
    extends AnyFlatSpec:

  "empty nests" should "have no expansion" in {

    val nests: List[Nest] = Nil

    val multiplier = Multiplier.Once[Nest]()

    val wildcard = Wildcard(Wildcard(nests*))

    assert(wildcard(multiplier).isEmpty)

  }

  "one nest" should "have one expansion" in {

    val nests = Set(Nest(classic, native, flow))

    val multiplier = Multiplier.Once[Nest]()

    val wildcard = Wildcard(Wildcard(nests.toSeq*))

    assert(wildcard(multiplier).size == 1)

  }

  "zero multiplier" should "have zero expansions" in {

    val nests = List(Nest(classic, native, flow))

    val multiplier = Multiplier[Nest] { case _ => 0 }

    val wildcard = Wildcard(Wildcard(nests*))

    assert(wildcard(multiplier).size == 0)

  }
