package queens
package common
package cipher
package client

import java.util.UUID

import scala.util.{ Failure, Success }

import org.scalatest.flatspec.AnyFlatSpec


class CipherClientSpec extends AnyFlatSpec {

  private def register: CryptoKey =
    CipherClient() match
      case Some(key) => key
      case _ => throw Exception

  private def encrypt(key: CryptoKey, uuid: UUID): Seq[Byte] =
    try
      CipherClient(key, uuid) match
        case Some(bytes) => bytes
        case _ => throw Exception
    catch
      case _ => throw Exception


  private def decrypt(key: CryptoKey, bytes: Seq[Byte]): UUID =
    try
      CipherClient(key, bytes) match
        case Some(uuid) => uuid
        case _ => throw Exception
    catch
      case _ => throw Exception


  "CipherClient" should "connect and register" in {

    try
      register
      assert(true)
    catch
      case _ =>
        assert(false, s"connection/register")

  }


  "CipherClient" should "connect and encrypt" in {

    try
      val key = register
      encrypt(key, UUID.randomUUID)
      assert(true)
    catch
      case _ =>
        assert(false, s"connection/encryption")

  }


  "CipherClient" should "connect, encrypt and decrypt" in {

    try
      val key = register
      val uuid = UUID.randomUUID
      assert(decrypt(key, encrypt(key, uuid)) == uuid)
    catch
      case _ =>
        assert(false, s"connection/encryption/decryption")

  }

}
