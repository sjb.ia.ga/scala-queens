package queens
package common
package cipher

import java.util.Base64.{ getEncoder, getDecoder }


object Crypto:

  private[cipher] def encode(bytes: Seq[Byte]): String =
    getEncoder.encodeToString(bytes.toArray)

  private[cipher] def decode(base64: String): Seq[Byte] =
    getDecoder.decode(base64).toSeq

  private[cipher] def apply(key: CryptoKey, uuid: java.util.UUID): String =
    ">" + key + uuid

  private[cipher] def apply(key: CryptoKey, uuid: Seq[Byte]): String =
    "<" + key + encode(uuid)

  final class CipherException(msg: String)
      extends base.QueensException(msg)
