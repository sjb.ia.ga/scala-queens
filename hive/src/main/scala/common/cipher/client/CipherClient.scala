package queens
package common
package cipher
package client

import java.util.UUID

import java.io.{ ObjectInputStream, ObjectOutputStream }
import java.net.{ InetAddress, Socket }

import scala.util.{ Failure, Success, Try }


object CipherClient:

  private def apply[R](block: ((ObjectOutputStream, ObjectInputStream)) => R): R =

    import sys.Prop.{ IntProp, StringProp }

    val host =
      if StringProp(_root_.queens.main.Properties.CipherServer.host).isSet
      then StringProp(_root_.queens.main.Properties.CipherServer.host).value
      else "localhost"

    val port =
      if IntProp(_root_.queens.main.Properties.CipherServer.port).isSet
      then IntProp(_root_.queens.main.Properties.CipherServer.port).value
      else 9999

    wrap(new Socket(InetAddress.getByName(host), port))(block)

  extension(out: ObjectOutputStream)
    private def send[T](message: T) =
      out.writeObject(message)
      out.flush()

  extension(in: ObjectInputStream)
    private def receive(): String =
      in.readObject().asInstanceOf[String]

  extension(uuid: String)
    private def toUUID: UUID =
      UUID.fromString(uuid)


  /**
    * connect to and register with the server
    */
  def apply(): Option[CryptoKey] =
    try
      this { case (out, in) =>
        out.send(true)
        val uuid = in.receive()
        if uuid.isEmpty
        then
          None
        else
          Some(uuid.toUUID)
      }
    catch
      case _ =>
        None


  /**
    * connect to the server and encrypt
    */
  def apply(key: CryptoKey, uuid: UUID): Encrypto =
    try
      this { case (out, in) =>
        out.send(Crypto(key, uuid))
        Some(Crypto.decode(in.receive()))
      }
    catch
      case _ =>
        None


  /**
    * connect to the server and decrypt
    */
  def apply(key: CryptoKey, uuid: Seq[Byte]): Decrypto =
    try
      this { case (out, in) =>
        out.send(Crypto(key, uuid))
        Some(in.receive().toUUID)
      }
    catch
      case _ =>
        None
