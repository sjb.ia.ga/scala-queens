package queens
package common

import java.io.{ DataInputStream, DataOutputStream }
import java.io.{ ObjectInputStream, ObjectOutputStream }

import java.net.{ ConnectException, Socket }


package object cipher:

  def wrap[R](expr: => Socket)
             (block: ((ObjectOutputStream, ObjectInputStream)) => R): R =

    import sys.Prop.IntProp

    val timeout =
      if IntProp(_root_.queens.main.Properties.CipherServer.timeout).isSet
      then IntProp(_root_.queens.main.Properties.CipherServer.timeout).value
      else 1000

    var socket: Socket = null
    var out: ObjectOutputStream = null
    var in: ObjectInputStream = null
    try
      var expired = 0
      while expired >= 0 do
        try
          socket = expr
          expired = -1
        catch
          case ex: ConnectException =>
            try { Thread.sleep(100) } catch { case _ => }
            if (expired > timeout) then throw ex
            expired += 100
      out = new ObjectOutputStream(new DataOutputStream(socket.getOutputStream()))
      in = new ObjectInputStream(new DataInputStream(socket.getInputStream()))
      block((out, in))
    finally
      if (in eq null) || (out eq null) || (socket eq null) then
        try
          if in ne null then
            in.close()
          if out ne null then
            out.close()
          if socket ne null then
            socket.close()
        catch
          case _ =>
