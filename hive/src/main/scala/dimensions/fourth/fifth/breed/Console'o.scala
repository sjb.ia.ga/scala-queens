package queens
package dimensions.fourth.fifth
package breed

import java.util.UUID

import conf.mco.Conf

import mco.QueensMonadConsoleOutput


abstract trait `Console'o`[
  P <: Conf
](using
  override protected val * : P
) extends QueensMonadConsoleOutput[P]
    with `Output *'o`[P]:

  override protected type * <: `Console'o`[P]

  override def toString(): String = "Console'o " + super.toString


/**
  * @see [[queens.version.v1.mco.MonadCO]] @{code override def apply(): Spawn[?] = `Console'o`(model)}
  */
object `Console'o`
    extends Spawner:

  import java.util.concurrent.{ ConcurrentHashMap => Map }

  import dimensions.Dimension.Model
  import Model.{ Flow, Parallel, Messaging }
  import Parallel.{ Actors, Futures }
  import Messaging.{ Kafka, RabbitMQ, AWSSQS }

  import common.Flag

  import common.pipeline.Context
  import dimensions.fifth.QueensUseSolution

  override def apply[
    P <: conf.emito.Conf
  ](model: Model
  )(using
    params: P
  ): Spawn[P] = new Spawn[P] {

    override def apply(next: Option[Context => QueensUseSolution]): * =

      new * :

        override protected def `apply*`(queens: *.Q,
                                        tag: Any,
                                        its: Seq[UUID],
                                        dryrun: Either[Simulate, Boolean]
        ): *.T =
          assert(model eq queens.model)

          given Map[UUID, (Boolean, Flag)] = new Map()

          ((model, params) match

            case (Parallel(Actors), p: conf.mco.actors.Parameters) =>

              new actors.`Impl.Console'o by Actors`(using p)(queens, tag, its, dryrun)(next)

            case (Parallel(Futures), p: conf.mco.futures.Parameters) =>

              new futures.`Impl.Console'o by Futures`(using p)(queens, tag, its, dryrun)(next)

            case (Messaging(Kafka), p: conf.mco.kafka.Parameters) =>

              new kafka.`Impl.Console'o by Kafka`(using p)(queens, tag, its, dryrun)(next)

            case (Messaging(RabbitMQ), p: conf.mco.rabbitMQ.Parameters) =>

              new rabbitMQ.`Impl.Console'o by RabbitMQ`(using p)(queens, tag, its, dryrun)(next)

            case (Messaging(AWSSQS), p: conf.mco.awsSQS.Parameters) =>

              new awsSQS.`Impl.Console'o by AWSSQS`(using p)(queens, tag, its, dryrun)(next)

            case (Flow, p: conf.mco.flow.Parameters) =>

              new flow.`Impl.Console'o by Flow`(using p)(queens, tag, its, dryrun)(next)

          ).asInstanceOf[*.T]

    }
