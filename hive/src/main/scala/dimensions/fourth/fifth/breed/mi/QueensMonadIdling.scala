package queens
package dimensions.fourth.fifth
package breed
package mi

import conf.mi.Conf

import dimensions.fifth.QueensIdling


abstract trait QueensMonadIdling[
  P <: Conf
] extends QueensMonadOutput[P]
    with QueensIdling
