package queens
package dimensions.fourth.fifth
package breed
package rabbitMQ

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import dimensions.third.Queens

import conf.mco.rabbitMQ.{ Parameters => P }

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution

import common.Flag
import common.monad.Item


class `Impl.Console'o by RabbitMQ`(using
  override protected val * : P
)(
  _qs: Queens[?],
  _tag: Any,
  _its: Seq[UUID],
  _run: Either[Simulate, Boolean],
  _fs: Item[Solution] => Boolean*
)(
  _next: Option[Context => QueensUseSolution]
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.Output *'o by RabbitMQ`[P](_qs, _tag, _its, _run, _fs*)(_next)
    with `Console'o`[P]:

  override protected type * = `Impl.Console'o by RabbitMQ`

  override def `#`(_t: Any): * = this

  override protected def `apply*`(s: Item[Solution] => Boolean*): * =
    new `Impl.Console'o by RabbitMQ`(_qs, _tag, _its, _run, s*)(_next)
