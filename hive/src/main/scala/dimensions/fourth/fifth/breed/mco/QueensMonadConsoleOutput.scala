package queens
package dimensions.fourth.fifth
package breed
package mco

import conf.mco.Conf

import dimensions.fifth.output.console.QueensConsoleOutput


abstract trait QueensMonadConsoleOutput[
  P <: Conf
] extends QueensMonadOutput[P]
    with QueensConsoleOutput
