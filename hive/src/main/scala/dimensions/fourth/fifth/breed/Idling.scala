package queens
package dimensions.fourth.fifth
package breed

import java.util.UUID

import conf.mi.Conf

import mi.QueensMonadIdling


abstract trait Idling[
  P <: Conf
](using
  override protected val * : P
) extends QueensMonadIdling[P]
    with `Output *'o`[P]:

  override protected type * <: Idling[P]

  override def toString(): String = "Idling " + super.toString


object Idling
    extends Spawner:

  import java.util.concurrent.{ ConcurrentHashMap => Map }

  import dimensions.Dimension.Model
  import Model.Flow

  import common.Flag

  import common.pipeline.Context
  import dimensions.fifth.QueensUseSolution

  override def apply[
    P <: conf.emito.Conf
  ](model: Model
  )(using
    params: P
  ): Spawn[P] = new Spawn[P] {

    override def apply(next: Option[Context => QueensUseSolution]): * =

      new * :

        override protected def `apply*`(queens: *.Q,
                                        tag: Any,
                                        its: Seq[UUID],
                                        dryrun: Either[Simulate, Boolean]
        ): *.T =
          assert(model eq queens.model)

          given Map[UUID, (Boolean, Flag)] = new Map()

          ((model, params) match

            case (Flow, p: conf.mi.flow.Parameters) =>

              new flow.`Impl.Idling by Flow`(using p)(queens, tag, its, dryrun)(next)

            case _ => ???

          ).asInstanceOf[*.T]

    }
