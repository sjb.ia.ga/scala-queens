package queens
package dimensions.fourth.fifth
package zeroth
package breed
package flow
package x

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import monix.eval.{ Coeval, Task }

import dimensions.third.Queens

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution

import common.Flag
import common.monad.Item


package sync:

  import conf.io.x.flow.sync.{ Parameters => P }

  class `Impl.I'o by Flow`(using
    override protected val * : P
  )(
    _qs: Queens[?],
    _tag: Any,
    _its: Seq[UUID],
    _fs: Item[Solution] => Boolean*
  )(
    _next: Option[Context => QueensUseSolution]
  )(using
    `_its*`: Map[UUID, (Boolean, Flag)]
  ) extends `Base.Output *'o by Flow`[Coeval, P](_qs, _tag, _its, _fs*)(_next)
      with breed.x.`I'o`[Coeval, P]:

    override protected type * = `Impl.I'o by Flow`

    override def `#`(_t: Any): * = this

    override protected def `apply*`(s: Item[Solution] => Boolean*): * =
      new `Impl.I'o by Flow`(_qs, _tag, _its, s*)(_next)

    override def toString(): String = "Coeval " + super.toString

package async:

  import conf.io.x.flow.async.{ Parameters => P }

  class `Impl.I'o by Flow`(using
    override protected val * : P
  )(
    _qs: Queens[?],
    _tag: Any,
    _its: Seq[UUID],
    _fs: Item[Solution] => Boolean*
  )(
    _next: Option[Context => QueensUseSolution]
  )(using
    `_its*`: Map[UUID, (Boolean, Flag)]
  ) extends `Base.Output *'o by Flow`[Task, P](_qs, _tag, _its, _fs*)(_next)
      with breed.x.`I'o`[Task, P]:

    override protected type * = `Impl.I'o by Flow`

    override def `#`(_t: Any): * = this

    override protected def `apply*`(s: Item[Solution] => Boolean*): * =
      new `Impl.I'o by Flow`(_qs, _tag, _its, s*)(_next)

    override def toString(): String = "Task " + super.toString
