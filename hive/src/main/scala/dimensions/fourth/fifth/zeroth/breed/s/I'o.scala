package queens
package dimensions.fourth.fifth
package zeroth
package breed
package s

import java.nio.file.Files

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import conf.io.s.Conf

import io.`0`.s.QueensMonadIOutput

import common.bis.Cycle
import Cycle.apply

import common.Flag

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution

import dimensions.Dimension.Model
import Model.Flow

import version.less.nest.Nest


abstract trait `I'o`[
  P <: Conf
](using
  override protected val * : P
) extends QueensMonadIOutput[P]
    with `Output *'o`[P]:

  override protected type * <: `I'o`[P]

  override def toString(): String = "I" + super.toString


object `I'o`
    extends Spawner:

  override def apply[
    P <: conf.emito.`0`.s.Conf
  ](model: Model
  )(using
    params: P
  ): Spawn[P] = new Spawn[P] {

    override def apply(next: Option[Context => QueensUseSolution]): * =

      new * :

        override protected def `apply*`(queens: *.Q,
                                        tag: Any,
                                        its: Seq[UUID]
        )(using M: Long): *.T =
          assert(model eq queens.model)

          given Map[UUID, (Boolean, Flag)] = new Map()

          import conf.io.s.flow.Parameters

          ((model, params) match

            case (Flow, p: Parameters) =>

              val nest = Nest(queens.aspect, queens.algorithm, queens.model)
              val board = Cycle(nest)(using p.board, M)

              val filename = s"by-flow-0-s-${UUID.randomUUID}"
              board(p.tmpdir, filename)

              new flow.s.`Impl.I'o by Flow`(using Parameters(board, null))(queens, tag, its)(next)

            case _ => ???

          ).asInstanceOf[*.T]

    }
