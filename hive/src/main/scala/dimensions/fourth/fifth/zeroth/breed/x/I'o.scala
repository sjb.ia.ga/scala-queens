package queens
package dimensions.fourth.fifth
package zeroth
package breed
package x

import java.nio.file.Files

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import cats.effect.Sync

import monix.eval.{ Coeval, Task }

import conf.io.x.Conf

import io.`0`.x.QueensMonadIOutput

import common.bis.Cycle
import Cycle.apply

import common.Flag

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution

import dimensions.Dimension.Model
import Model.Flow

import version.less.nest.Nest


abstract trait `I'o`[E[_]: Sync,
  P <: Conf[E]
](using
  override protected val * : P
) extends QueensMonadIOutput[E, P]
    with `Output *'o`[E, P]:

  override protected type * <: `I'o`[E, P]

  override def toString(): String = "I" + super.toString


package sync:

  object `I'o`
      extends Spawner[Coeval]:

    override def apply[
      P <: conf.emito.`0`.x.Conf[Coeval]
    ](model: Model
    )(using
      params: P
    ): Spawn[Coeval, P] = new Spawn[Coeval, P] {

      override def apply(next: Option[Context => QueensUseSolution]): * =

        new * :

          override protected def `apply*`(queens: *.Q,
                                          tag: Any,
                                          its: Seq[UUID]
          )(using M: Long): *.T =
            assert(model eq queens.model)

            given Map[UUID, (Boolean, Flag)] = new Map()

            import conf.io.x.flow.sync.Parameters

            ((model, params) match

              case (Flow, p: Parameters) =>

                val nest = Nest(queens.aspect, queens.algorithm, queens.model)
                val board = Cycle(nest)(using p.board, M)

                val filename = s"by-flow-0-x-sync-${UUID.randomUUID}"
                board(p.tmpdir, filename)

                new flow.x.sync.`Impl.I'o by Flow`(using Parameters(board, null))(queens, tag, its)(next)

              case _ => ???

            ).asInstanceOf[*.T]

      }

package async:

  object `I'o`
      extends Spawner[Task]:

    override def apply[
      P <: conf.emito.`0`.x.Conf[Task]
    ](model: Model
    )(using
      params: P
    ): Spawn[Task, P] = new Spawn[Task, P] {

      override def apply(next: Option[Context => QueensUseSolution]): * =

        new * :

          override protected def `apply*`(queens: *.Q,
                                          tag: Any,
                                          its: Seq[UUID]
          )(using M: Long): *.T =
            assert(model eq queens.model)

            given Map[UUID, (Boolean, Flag)] = new Map()

            import conf.io.x.flow.async.Parameters

            ((model, params) match

              case (Flow, p: Parameters) =>

                val nest = Nest(queens.aspect, queens.algorithm, queens.model)
                val board = Cycle(nest)(using p.board, M)

                val filename = s"by-flow-0-x-async-${UUID.randomUUID}"
                board(p.tmpdir, filename)

                new flow.x.async.`Impl.I'o by Flow`(using Parameters(board, null))(queens, tag, its)(next)

              case _ => ???

            ).asInstanceOf[*.T]

      }
