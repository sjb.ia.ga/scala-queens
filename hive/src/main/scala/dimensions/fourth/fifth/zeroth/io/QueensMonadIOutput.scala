package queens
package dimensions.fourth.fifth
package zeroth
package breed
package io


package `0`:

  package x:

    import cats.effect.Sync

    import conf.io.x.Conf

    abstract trait QueensMonadIOutput[E[_]: Sync,
      P <: Conf[E]
    ] extends zeroth.x.QueensMonadOutput[E, P]

  package s:

    import conf.io.s.Conf

    abstract trait QueensMonadIOutput[
      P <: Conf
    ] extends zeroth.s.QueensMonadOutput[P]
