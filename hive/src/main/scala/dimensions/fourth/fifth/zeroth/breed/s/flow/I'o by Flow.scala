package queens
package dimensions.fourth.fifth
package zeroth
package breed
package flow
package s

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import conf.io.s.flow.{ Parameters => P }

import dimensions.third.Queens

import common.pipeline.Context
import dimensions.fifth.QueensUseSolution

import common.Flag
import common.monad.Item


class `Impl.I'o by Flow`(using
  override protected val * : P
)(
  _qs: Queens[?],
  _tag: Any,
  _its: Seq[UUID],
  _fs: Item[Solution] => Boolean*
)(
  _next: Option[Context => QueensUseSolution]
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.Output *'o by Flow`[P](_qs, _tag, _its, _fs*)(_next)
    with breed.s.`I'o`[P]:

  override protected type * = `Impl.I'o by Flow`

  override def `#`(_t: Any): * = this

  override protected def `apply*`(s: Item[Solution] => Boolean*): * =
    new `Impl.I'o by Flow`(_qs, _tag, _its, s*)(_next)

  override def toString(): String = "Coeval " + super.toString
