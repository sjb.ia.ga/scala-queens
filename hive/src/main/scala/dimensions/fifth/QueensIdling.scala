package queens
package dimensions.fifth

import java.util.Date

import scala.Function.const

import common.pipeline.Context.Empty
import common.pipeline.Metadata
import Metadata.{ Boot, Wrap }

import dimensions.fifth.output.QueensOutputUseSolution

import pojo.parser.date.span


abstract trait QueensIdling
    extends QueensOutputUseSolution:

  private var idle = 0L
  private var started: Date = _
  private var ended: Date = Date()

  def apply(): Date =
    started = Date()
    idle = idle.span(from = ended, to = started)
    started

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(_, _, _) => Some(Wrap(Empty, None))

  override protected val >> = {
    case _ =>
      ended = Date()
      const(true)
  }

  override def toString(): String = "Idling :: " + super.toString


object QueensIdling:

  inline given Conversion[QueensIdling, Long] = _.idle
  inline given Conversion[QueensIdling, Date] = _.ended
