package queens
package dimensions.three
package breed

import Hatch.Flavor

import version.less.nest.Nest


abstract trait Hatcher:

  def apply(nest: Nest, flavor: Flavor.Value): Option[Hatch[?]] =
    given Flavor.Value = flavor

    flavor match

      case Flavor.`di-*` =>
        val guice = sys.BooleanProp
          .keyExists(_root_.queens.main.Properties.DependencyInjection.guice)
          .value

        val macwire = sys.BooleanProp
          .keyExists(_root_.queens.main.Properties.DependencyInjection.macwire)
          .value

        if guice && !macwire then apply(nest, Flavor.`di-guice`)
        else apply(nest, Flavor.`di-macwire`)

      case _ =>
        Some(Hatcher(nest))

  import version.base.ErrorCode

  /**
    * @see [[queens.version.base.ErrorCode#ProtocolIsntVal]]
    */
  def isntFlavor(v: Enumeration#Value)
                (implicit e: ErrorCode): e.ProtocolIsntVal = e.ProtocolIsntVal(v)


object Hatcher:

  import configurers.GuiceConfig.{ init => `guice init` }
  import configurers.bis.GuiceConfig.{ init => `guice init 2` }
  import configurers.MacWireConfig.{ init => `macwire init` }
  import configurers.bis.MacWireConfig.{ init => `macwire init 2` }

  def apply()(using flavor: Flavor.Value): Unit = flavor match

    case Flavor.config =>

      sys.BooleanProp
        .valueIsTrue(_root_.queens.main.Properties.DependencyInjection.config)
        .enable()

    case Flavor.build =>

      sys.BooleanProp
        .valueIsTrue(_root_.queens.main.Properties.DependencyInjection.build)
        .enable()

    case Flavor.`di-guice` =>
      `guice init`()
      `guice init 2`()

      sys.BooleanProp
        .valueIsTrue(_root_.queens.main.Properties.DependencyInjection.guice)
        .enable()

    case _ =>
      `macwire init`()
      `macwire init 2`()

      sys.BooleanProp
        .valueIsTrue(_root_.queens.main.Properties.DependencyInjection.macwire)
        .enable()


  import dimensions.third.`Queens*`

  def apply[Q <: `Queens*`[?, ?]](nest: Nest)(using
                                  flavor: Flavor.Value = Flavor.build): Hatch[Q] = nest match

    case Nest(aspect, algorithm, model) => flavor match

      case Flavor.build =>
        import dimensions.three.breed.given

        given_Builder
          .byModel(nest.model)
          .withAlgorithm(nest.algorithm)
          .andAspect(nest.aspect)
          .breed[Q]

      case _ =>
        val (guice, macwire) = flavor match
          case Flavor.`di-guice` => (true, false)
          case Flavor.`di-macwire` | Flavor.`di-*` => (false, true)
          case _ => (false, false)

        (
          if guice
          then
            import dimensions.three.breed.guice.given
            given_Config
          else if macwire
          then
            import dimensions.three.breed.macwire.given
            given_Config
          else
            import dimensions.three.breed.given
            given_Config
        )
          .byModel(model)
          .ofAlgorithm(algorithm)
          .toAspect(aspect)
          .breed[Q]


  object `0`:

    import dimensions.Dimension.Monad

    import dimensions.third.zeroth.Queens

    def apply[Q <: Queens[?, ?, ?, ?]](monad: Monad)
                                      (nest: Nest)(using
                                       flavor: Flavor.Value = Flavor.build): Hatch[Q] = nest match

      case Nest(aspect, algorithm, model) => flavor match

        case Flavor.build =>
          import dimensions.three.breed.given

          given_Builder
            .byModel(nest.model)
            .withAlgorithm(nest.algorithm)
            .andAspect(nest.aspect)
            .butMonad(monad)
            .breed[Q]

        case _ =>
          val (guice, macwire) = flavor match
            case Flavor.`di-guice` => (true, false)
            case Flavor.`di-macwire` | Flavor.`di-*` => (false, true)
            case _ => (false, false)

          (
            if guice
            then
              import dimensions.three.breed.guice.given
              given_Config
            else if macwire
            then
              import dimensions.three.breed.macwire.given
              given_Config
            else
              import dimensions.three.breed.given
              given_Config
          )
            .byModel(model)
            .ofAlgorithm(algorithm)
            .toAspect(aspect)
            .atMonad(monad)
            .breed[Q]


  object bis:

    import dimensions.third.bis.Queens

    def apply[Q <: Queens[?]](nest: Nest)(using
                              flavor: Flavor.Value = Flavor.build): Hatch[Q] = nest match

      case Nest(aspect, algorithm, model) => flavor match

        case Flavor.build =>
          import dimensions.three.breed.bis.given

          given_Builder
            .byModel(nest.model)
            .withAlgorithm(nest.algorithm)
            .andAspect(nest.aspect)
            .breed[Q]

        case _ =>
          val (guice, macwire) = flavor match
            case Flavor.`di-guice` => (true, false)
            case Flavor.`di-macwire` | Flavor.`di-*` => (false, true)
            case _ => (false, false)

          (
            if guice
            then
              import dimensions.three.breed.guice.bis.given
              given_Config
            else if macwire
            then
              import dimensions.three.breed.macwire.bis.given
              given_Config
            else
              import dimensions.three.breed.bis.given
              given_Config
          )
            .byModel(model)
            .ofAlgorithm(algorithm)
            .toAspect(aspect)
            .breed[Q]
