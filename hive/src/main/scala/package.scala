package object queens {

  import java.util.UUID

  import base.Board

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]


  type Simulate = dimensions.fifth.cycle.Simulate

  type IO = dimensions.fifth.cycle.IO


  type Identity_[A] = A

  type Iterable_[X[_], A] = Iterable[A]


  given Conversion[String, List[String]] = _
    .split("[\\[{(<.: ;,>)}\\]]")
    .filterNot(_.isEmpty)
    .map(_.replace('_', ' '))
    .toList


  // PlanProtocol

  type Twin[T] = Tuple2[T, T]


  // ScalaCrypt

  import java.util.UUID

  type CryptoKey = UUID

  type Encrypto = Option[Seq[Byte]]

  type Decrypto = Option[UUID]


  extension(p: Boolean)
    inline def ->(q: => Boolean): Boolean = !p || q

}
