package queens
package version
package base


abstract trait DeseqAxes
    extends Deseq {

  import dimensions.Dimension.Axes

  override type Arguments = Tuple1[
    Axes
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val axes = args(0).asInstanceOf[Axes]
    Tuple1(axes)
  }

}

abstract trait `DeseqAxes*`
    extends Deseq {

  private object Deseq01
      extends `Deseq*`

  private object Deseq2
      extends DeseqAxes

  import dimensions.Dimension.Axes

  override type Arguments = Tuple3[
    CryptoKey,
    version.base.Role => Boolean,
    Axes
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val (key, end) = Deseq01(args.take(2))
    val Tuple1(axes) = Deseq2(args.drop(2).take(1))
    Tuple3[CryptoKey, version.base.Role => Boolean, Axes](key, end, axes)
  }

}
