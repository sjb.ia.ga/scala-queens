package queens
package version
package base
package protocol

import base.Role

import base.ErrorCode


abstract trait BaseProtocol:

  import BaseProtocol._

  final def Error(errorVal: ErrorCode#ProtocolErrorVal, origin: Role) = new Error(errorVal, origin)

  final def Complaint[
    M >: Protocol.Group
  ](toMessage: M, trace: Seq[Role] = Nil) = new Complaint[M](toMessage, trace)


object BaseProtocol:

  final case class Error(errorVal: ErrorCode#ProtocolErrorVal, origin: Role)
      extends message.Protocol.Message

  final case class Complaint[
    M >: Protocol.Group
  ](toMessage: M, trace: Seq[Role] = Nil)
      extends message.Protocol.Message


abstract trait BaseErrorCode
    extends ErrorCode
