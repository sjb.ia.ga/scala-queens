package queens
package version
package base


package protocol {

  import version.less.plane.protocol.base.Protocol.{ Group => PlaneGroup }

  abstract trait BaseGroup
      extends PlaneGroup
      with message.Protocol.Match

  object Protocol {

    type Group = protocol.BaseGroup

  }

  package message {

    import version.less.plane.protocol.session.Protocol.{ Role => SessionRole }
    import version.less.nest.protocol.session.Protocol.{ Role => NestSessionRole }
    import version.v1.vector.protocol.session.Protocol.{ Role => VectorV1SessionRole }
    import version.v2.plane.protocol.session.Protocol.{ Role => PlaneV2SessionRole }
    import version.v3.mco.protocol.session.Protocol.{ Role => MCOV3SessionRole }

    import version.less.plane.protocol.server.Protocol.{ Role => ServerRole }
    import version.less.nest.protocol.server.Protocol.{ Role => NestServerRole }
    import version.v1.mco.protocol.server.Protocol.{ Role => MCOServerRole }
    import version.v1.vector.protocol.server.Protocol.{ Role => VectorV1ServerRole }
    import version.v2.plane.protocol.server.Protocol.{ Role => PlaneV2ServerRole }

    import version.less.plane.protocol.client.Protocol.{ Role => ClientRole }
    import version.less.nest.protocol.client.Protocol.{ Role => NestClientRole }
    import version.v1.mco.protocol.client.Protocol.{ Role => MCOClientRole }
    import version.v1.vector.protocol.client.Protocol.{ Role => VectorV1ClientRole }
    import version.v2.plane.protocol.client.Protocol.{ Role => PlaneV2ClientRole }

    import version.less.plane.protocol.client.defer.Protocol.{ Role => ClientDeferRole }
    import version.less.nest.protocol.client.defer.Protocol.{ Role => NestClientDeferRole }
    import version.v1.mco.protocol.client.defer.Protocol.{ Role => MCOClientDeferRole }
    import version.v1.vector.protocol.client.defer.Protocol.{ Role => VectorV1ClientDeferRole }
    import version.v2.plane.protocol.client.defer.Protocol.{ Role => PlaneV2ClientDeferRole }

    import version.less.plane.protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
    import version.less.nest.protocol.client.frontend.Protocol.{ Role => NestClientFrontendRole }
    import version.v1.mco.protocol.client.frontend.Protocol.{ Role => MCOClientFrontendRole }
    import version.v1.vector.protocol.client.frontend.Protocol.{ Role => VectorV1ClientFrontendRole }
    import version.v2.plane.protocol.client.frontend.Protocol.{ Role => PlaneV2ClientFrontendRole }

    abstract trait BaseMatch
        extends SessionRole // Error, Complaint,
        with NestSessionRole
        with VectorV1SessionRole
        with PlaneV2SessionRole
        with MCOV3SessionRole

        with ServerRole // Error, Complaint,
        with NestServerRole
        with MCOServerRole
        with VectorV1ServerRole

        with ClientRole // Error, Complaint,
        with NestClientRole
        with MCOClientRole
        with VectorV1ClientRole
        with PlaneV2ClientRole

        with ClientDeferRole // Error,
        with NestClientDeferRole
        with MCOClientDeferRole
        with VectorV1ClientDeferRole
        with PlaneV2ClientDeferRole

        with ClientFrontendRole // Error, Complaint
        with NestClientFrontendRole
        with MCOClientFrontendRole
        with VectorV1ClientFrontendRole
        with PlaneV2ClientFrontendRole

    import protocol.Protocol.{ Group => BaseDotGroup }

    abstract trait BaseMessage
        extends BaseDotGroup

    object Protocol {

      type Match = BaseMatch
      type Message = BaseMessage

    }

  }

}
