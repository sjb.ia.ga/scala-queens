package queens
package version
package base


abstract trait ErrorCode extends Enumeration {

  abstract trait ProtocolError

  abstract class ProtocolErrorVal
      extends super.Val
      with ProtocolError


  abstract trait DuplicatedError
      extends ProtocolError

  final case class ProtocolDupVal()
      extends ProtocolErrorVal
      with DuplicatedError


  abstract trait MissingError
      extends ProtocolError

  final case class ProtocolNotVal()
      extends ProtocolErrorVal
      with MissingError


  abstract trait EnumeratingError
      extends ProtocolError

  final case class ProtocolIsntVal(v: Enumeration#Value)
      extends ProtocolErrorVal
      with EnumeratingError


  abstract trait CastingError
      extends ProtocolError

  final case class ProtocolAintVal()
      extends ProtocolErrorVal
      with CastingError


  abstract trait CountingError
      extends ProtocolError

  final case class ProtocolCountVal()
      extends ProtocolErrorVal
      with CountingError


  abstract trait ComparingError
      extends ProtocolError

  final case class ProtocolBadVal()
      extends ProtocolErrorVal
      with ComparingError


  abstract trait ThrowingError
      extends ProtocolError

  final case class ProtocolCatchVal[T](t: Throwable, p: T)
      extends ProtocolErrorVal
      with ThrowingError

}
