package queens
package version
package base

import java.util.UUID


abstract trait DeseqUUID
    extends Deseq {

  private object Deseq0
      extends DeseqCipher

  override type Arguments = Tuple2[
    CryptoKey,
    UUID
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val Tuple1(key) = Deseq0(args.take(1))
    val secret = args(1).asInstanceOf[UUID]
    Tuple2[CryptoKey, UUID](key, secret)
  }

}
