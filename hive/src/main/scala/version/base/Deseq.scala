package queens
package version
package base


abstract trait Deseq {

  type Arguments

  def apply(args: Seq[Any]): Arguments

}

abstract trait DeseqX
    extends Deseq {

  override type Arguments = Unit

  override def apply(args: Seq[Any]): Arguments = ()

}


abstract trait `Deseq*`
    extends Deseq {

  private object Deseq0
      extends DeseqCipher

  override type Arguments = Tuple2[
    CryptoKey,
    Role => Boolean
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val Tuple1(key) = Deseq0(args.take(1))
    val end = args(1).asInstanceOf[Role => Boolean]
    Tuple2[CryptoKey, Role => Boolean](key, end)
  }

}
