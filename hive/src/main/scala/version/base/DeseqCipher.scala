package queens
package version
package base


abstract trait DeseqCipher
    extends Deseq {

  override type Arguments = Tuple1[
    CryptoKey
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val key = args(0).asInstanceOf[CryptoKey]
    Tuple1[CryptoKey](key)
  }

}


