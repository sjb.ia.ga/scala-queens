package queens
package version
package base
package interface

import java.util.concurrent.{ ConcurrentHashMap => MutableMap }


abstract trait NodeIfce:

  private val registry = new MutableMap[Class[?], AnyRef]

  protected def apply[T <: AnyRef](key: Class[T], value: T): Unit = registry.put(key, value)

  protected def apply[T <: AnyRef](key: Class[T]): T = registry.get(key).asInstanceOf[T]
