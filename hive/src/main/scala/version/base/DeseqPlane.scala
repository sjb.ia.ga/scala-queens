package queens
package version
package base


abstract trait DeseqPlane[P <: Plane]
    extends Deseq {

  override type Arguments = Tuple1[P]

  override def apply(args: Seq[Any]): Arguments = {
    val plane = args(0).asInstanceOf[P]
    Tuple1(plane)
  }

}


abstract trait `DeseqPlane*`[P <: Plane]
    extends Deseq {

  private object Deseq0
      extends DeseqCipher

  override type Arguments = Tuple2[
    CryptoKey,
    P
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val Tuple1(key) = Deseq0(args.take(1))
    val plane = args(1).asInstanceOf[P]
    Tuple2[CryptoKey, P](key, plane)
  }

}
