package queens
package version
package base


abstract trait Role

package role {

  package node {

    case object Session
        extends Role

  }

  case object Adapter
      extends Role

  private case object Nobody
      extends Role

  sealed abstract trait EndPointRole
      extends Role

  final case class Interface(from: Role, to: Role) extends Role

  sealed abstract trait DeferRole
      extends Role

  final case class Defer(to: Role) extends DeferRole

  object EndPoint {

    case object Client extends EndPointRole

    case object Server extends EndPointRole

  }

  package object defer {

    val Client = Defer(Nobody)

    val Server = Defer(Backend)

  }

}

package object role {

  def roles = List[Role](Adapter, node.Session,
                         Frontend, Client, role.defer.Client,
                         Server, role.defer.Server, Backend)

  def isInitial(role: Role): Boolean = role match
    case node.Session | Adapter | Client | Frontend | Server => true
    case _ => false

  val Frontend = Interface(Adapter, EndPoint.Client)

  val Client = Interface(Frontend, defer.Client)

  val Server = Interface(Adapter, defer.Server)

  case object Backend extends Role

}
