package queens
package version
package base

import akka.actor.typed.{ ExtensibleBehavior, TypedActorContext, Behavior, Signal }
import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import protocol._


abstract trait Node[EC <: BaseErrorCode, M]
    extends BaseProtocol {

  val role: version.base.Role

  implicit protected val errorCode: EC

  protected abstract trait NodeBehavior[M, T <: NodeBehavior[M, T]]
      extends ExtensibleBehavior[M] {

    final protected type B = Behavior[M]

    protected case object NoneBehavior extends Throwable

    protected case class SomeBehavior(it: B) extends Throwable

    final protected def none: Nothing =
      throw NoneBehavior

    final protected def ignore: Nothing =
      throw SomeBehavior(Behaviors.ignore)

    final protected def same: Nothing =
      throw SomeBehavior(Behaviors.same)

    final protected def stopped: Nothing =
      throw SomeBehavior(Behaviors.stopped)

    final protected def unhandled: Nothing =
      throw SomeBehavior(Behaviors.unhandled)

    override def receiveSignal(ctx: TypedActorContext[M], msg: Signal): B = {
      Behaviors.unhandled
    }

  }

  protected abstract trait NodeInflater[M, T <: NodeBehavior[M, T]]
      extends Deseq {

    def apply(em: Arguments): T

  }

  protected abstract trait NodeInflaterX[M, T <: NodeBehavior[M, T]]
      extends NodeInflater[M, T]
      with DeseqX

  protected abstract trait ObjectNodeBehavior[M, T <: NodeBehavior[M, T], U <: NodeInflater[M, T]] {

    protected val inflater: U

    def apply(args: Any*): T = inflater(inflater(args))

  }

}

