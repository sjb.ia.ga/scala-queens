package queens
package version.v3
package vector
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import conf.emito.Conf

import version.v1.vector.{ Apply, VectorV1, Wildcard }
import version.base.Multiplier

import version.less.plane.base.interface.PlaneProtocolIfce


abstract trait VectorV3ProtocolIfce
    extends PlaneProtocolIfce:

  protected abstract trait ObjectVectorV3SessionExpand
      extends ObjectSessionExpand { self =>

    def apply[
      P <: Conf
    ](using
      params: P
    )(context: ActorContext[?],
      end: version.base.Role => Boolean,
      stop: () => Unit,
      block: V3Message => Unit,
      apply: Apply[P],
      wildcard: Wildcard[P],
      multiplier: Multiplier[VectorV1[P]] = Multiplier.Once()
    ): Int

  }

  override def SessionExpand: ObjectVectorV3SessionExpand
