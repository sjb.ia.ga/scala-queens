package queens
package version.v3
package vector
package interface

import java.util.concurrent.atomic.AtomicLong

import scala.collection.mutable.{ ArrayBuffer => Accumulator }

import akka.actor.typed.scaladsl.ActorContext

import akka.util.ReentrantGuard

import queens.base.breed.Breed

import dimensions.three.breed.Hatch
import version.less.nest.{ Nest, Wildcard => NestWildcard }

import dimensions.fourth.fifth.breed.Spawn
import version.v1.mco.{ MonadCO, Wildcard => MCOWildcard }
import version.patch.v1.mdo.MonadDO
import version.patch.v1.mbo.MonadBO

import conf.emito.Conf

import version.v1.vector.breed.Hive
import version.v1.vector.{ Apply, VectorV1, Wildcard }
import version.base.Multiplier

import base.interface.VectorV3ProtocolIfce


abstract trait ObjectVectorV3ProtocolIfce
    extends VectorV3ProtocolIfce { interface =>

  override object SessionExpand
      extends ObjectVectorV3SessionExpand:

    override def apply[
      P <: Conf
    ](using
      params: P
    )(context: ActorContext[?],
      end: version.base.Role => Boolean,
      stop: () => Unit,
      block: V3Message => Unit,
      vector_v3: Apply[P],
      wildcard: Wildcard[P],
      multiplier: Multiplier[VectorV1[P]]
    ): Int =
      import version.v1.vector.interface.ObjectVectorV1ProtocolIfce._

      val nest_multiplier = multiplier { (nest: Nest) =>
        new VectorV1(nest.toVector)
      }

      val vs: Seq[(Nest, MonadCO[P])] = for
        n <- wildcard._1(nest_multiplier)
        m <- wildcard._2()
        if n.model == m.model
        acc = Accumulator[(PlaneV1, Breed)]()
        lock = new ReentrantGuard()
      yield (
        new NestV1(acc, lock, n),
        m match
          case mb: MonadBO[P] => new MonadBOV1(acc, lock, mb)
          case md: MonadDO[P] => new MonadDOV1(acc, lock, md)
          case mc => new MonadCOV1(acc, lock, mc)
      )

      if vs.size == 0 then
        return 0

      val end_v3 = endV1(end, stop)

      import version.less.nest.protocol.NestMessage
      import version.less.nest.interface.ObjectNestProtocolIfce

      val nest_wildcard = new NestWildcardV1(vs.map(_._1))

      import common.pipeline.Context
      import dimensions.fifth.QueensUseSolution

      val apply_v1 =  { (nest: Nest, hatch: Hatch[params.Q], monadCO: MonadCO[P], spawn: Spawn[P]) =>
                        vector_v3(VectorV1(nest, monadCO),
                                  { case next: Option[Context => QueensUseSolution] =>
                                      Hive(hatch, spawn)(next) })
                      }

      interface(classOf[ObjectNestProtocolIfce[NestMessage]])
        .SessionExpand(context,
          end_v3,
          { () => },
          { (msg: NestMessage) => block(V3NestMessage(msg)) },
          { case (nest: Nest, hatch: Hatch[params.Q]) =>
            import dimensions.three.breed.given
            nestApply(apply_v1)(nest, hatch)
          },
          nest_wildcard,
          Multiplier.Once()
        )

      import version.v1.mco.protocol.MCOMessage
      import version.v3.mco.interface.ObjectMCOV3ProtocolIfce

      val mco_wildcard = new MCOWildcardV1(vs.map(_._2))

      interface(classOf[ObjectMCOV3ProtocolIfce[MCOMessage]])
        .SessionExpand(context,
          end_v3,
          { () => },
          { (msg: MCOMessage) => block(V3MonadCOMessage(msg)) },
          mcoApply(apply_v1).asInstanceOf[mco.Apply[P]],
          mco_wildcard,
          Multiplier.Once()
        )

      vs.size

}
