package queens
package version.v3
package vector

import version.less.nest.protocol.NestMessage
import version.v1.mco.protocol.MCOMessage


abstract trait V3Message

final case class V3NestMessage(msg: NestMessage)
    extends V3Message

final case class V3MonadCOMessage(msg: MCOMessage)
    extends V3Message


object Implicits {

  import interface.ObjectVectorV3ProtocolIfce

  implicit object VectorV3ProtocolIfceImplicit
      extends ObjectVectorV3ProtocolIfce { interface =>

    import version.less.nest.interface.ObjectNestProtocolIfce
    import version.less.nest.Implicits._

    interface(
      classOf[ObjectNestProtocolIfce[NestMessage]],
      implicitly[ObjectNestProtocolIfce[NestMessage]]
    )

    import version.v3.mco.interface.ObjectMCOV3ProtocolIfce
    import version.v3.mco.Implicits._

    interface(
      classOf[ObjectMCOV3ProtocolIfce[MCOMessage]],
      implicitly[ObjectMCOV3ProtocolIfce[MCOMessage]]
    )

  }

}
