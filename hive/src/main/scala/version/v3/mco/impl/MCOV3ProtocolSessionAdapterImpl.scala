package queens
package version.v3
package mco
package impl

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import base.MCOV3ProtocolSessionAdapter

import protocol.base.Protocol.Group
import protocol.session.adapter.Protocol.Role
import protocol.session.adapter._


class MCOV3ProtocolSessionAdapterImpl[
  EC <: MCOV3SessionAdapterErrorCode,
  M >: Group <: Role
](
  override protected val session: ActorRef[Either[?, M]]
)(implicit
  override protected val errorCode: EC
) extends MCOV3ProtocolSessionAdapter[EC, M] {

  class SessionAdapterMCOV3BehaviorImpl(
    override protected val end: version.base.Role => Boolean
  ) extends SessionAdapterMCOV3Behavior[SessionAdapterMCOV3BehaviorImpl]

  private object SessionAdapterMCOV3Inflater
      extends SessionAdapterMCOV3Inflater[SessionAdapterMCOV3BehaviorImpl] {

    override def apply(em: Arguments): SessionAdapterMCOV3BehaviorImpl =
      new SessionAdapterMCOV3BehaviorImpl(em._1)

  }

  private object SessionAdapterMCOV3BehaviorImpl
      extends ObjectRolePlaneBehavior[SessionAdapterMCOV3BehaviorImpl, SessionAdapterMCOV3Inflater.type] {

    override val inflater = SessionAdapterMCOV3Inflater

  }

}


import factory.session.adapter._

import protocol.session.adapter.{ Protocol => Interface }

object MCOV3ProtocolSessionAdapterImpl
    extends MCOV3ProtocolSessionAdapterFactory[Role] {

  override def apply(
    context: ActorContext[?],
    session: ActorRef[Either[?, Role]],
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import mco.session.adapter.Implicits._

      new MCOV3ProtocolSessionAdapterImpl[
        MCOV3SessionAdapterErrorCode,
        Role,
      ](
        session
      ).SessionAdapterMCOV3BehaviorImpl(args*)

    }

}
