package queens
package version.v3
package mco
package impl

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import base.MCOV3ProtocolSession
import base.interface.MCOV3ProtocolSessionIfce

import session.{ Start, Block }
import version.v1.mco.client.frontend.Apply

import protocol.base.Protocol.Group
import version.v1.mco.protocol.server.Protocol.{ Role => ServerRole }
import version.v1.mco.protocol.client.Protocol.{ Role => ClientRole }
import version.v1.mco.protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role
import protocol.session._


class MCOV3ProtocolSessionImpl[L,
  EC <: MCOV3SessionErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole,
  I <: MCOV3ProtocolSessionIfce[M, SM, CM, CFM]
](using
  override protected val errorCode: EC,
  override protected val interface: I
) extends MCOV3ProtocolSession[L, EC, M, SM, CM, CFM, I] {

  class SessionMCOV3BehaviorImpl(
    override protected val start: session.Start[CFM],
    override protected val end: version.base.Role => Boolean,
    override protected val block: session.Block[L, M],
    override protected val apply: version.v1.mco.client.frontend.Apply
  ) extends SessionMCOV3Behavior[SessionMCOV3BehaviorImpl]

  private object SessionMCOV3Inflater
      extends SessionMCOV3Inflater[SessionMCOV3BehaviorImpl] {

    override def apply(em: Arguments): SessionMCOV3BehaviorImpl =
      new SessionMCOV3BehaviorImpl(em._1, em._2, em._3, em._4)

  }

  private object SessionMCOV3BehaviorImpl
      extends ObjectSessionPlaneBehavior[SessionMCOV3BehaviorImpl, SessionMCOV3Inflater.type] {

    override val inflater = SessionMCOV3Inflater

  }

}


import factory.session._

import protocol.session.{ Protocol => Interface }

object MCOV3ProtocolSessionImpl
    extends MCOV3ProtocolSessionFactory[Role] {

  override def apply[L](
    context: ActorContext[?],
    args: Any*
  ): Interface.Actor[L] = context
    .spawnAnonymous {
      import interface.ObjectMCOV3ProtocolSessionIfce
      import session.Implicits._

      new MCOV3ProtocolSessionImpl[L,
        MCOV3SessionErrorCode,
        Role,
        version.v1.mco.protocol.server.Protocol.Role,
        version.v1.mco.protocol.client.Protocol.Role,
        version.v1.mco.protocol.client.frontend.Protocol.Role,
        ObjectMCOV3ProtocolSessionIfce
      ].SessionMCOV3BehaviorImpl(args*)

    }

}
