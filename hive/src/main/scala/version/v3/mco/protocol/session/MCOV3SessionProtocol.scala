package queens
package version.v3
package mco.protocol
package session


import version.less.plane.protocol.session.PlaneSessionProtocol
import version.v1.mco.protocol.MCOProtocol

abstract trait MCOV3SessionProtocol
    extends MCOProtocol
    with PlaneSessionProtocol


import version.less.plane.protocol.session.PlaneSessionErrorCode
import version.v1.mco.protocol.MCOErrorCode

abstract trait MCOV3SessionErrorCode
    extends MCOErrorCode
    with PlaneSessionErrorCode
