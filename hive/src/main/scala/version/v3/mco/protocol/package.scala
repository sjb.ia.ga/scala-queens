package queens
package version.v3
package mco.protocol

import mco.{ Actor => MCOV3Actor }


package base {

  import version.v1.mco.protocol.base.Protocol.{ Group => MCOGroup }

  object Protocol {

    type Group = MCOGroup

  }

}


package session {

  import version.less.plane.protocol.session.Protocol.{ Role => PlaneSessionRole }

  abstract trait MCOV3SessionRole
      extends PlaneSessionRole

  object Protocol {

    import akka.actor.typed.ActorRef

    type Role = MCOV3SessionRole
    private[mco] type Actor[L] = ActorRef[Either[L, Role]]

  }

  package adapter {

    object Protocol {

      type Role = session.Protocol.Role
      private[mco] type Actor = MCOV3Actor[Role]

    }

  }

  package object adapter {

    type MCOV3SessionAdapterProtocol = MCOV3SessionProtocol
    type MCOV3SessionAdapterErrorCode = MCOV3SessionErrorCode

  }

}
