package queens
package version.v3
package mco
package factory.session.adapter

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.session.adapter.Protocol.Role


abstract trait MCOV3ProtocolSessionAdapterFactory[
  M >: Group <: Role
] {

  def apply(
    context: ActorContext[?],
    session: ActorRef[Either[?, M]],
    args: Any*
  ): Actor[M]

}
