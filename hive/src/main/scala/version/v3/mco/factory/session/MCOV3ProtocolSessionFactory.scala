package queens
package version.v3
package mco
package factory.session

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.session.Protocol.Role


abstract trait MCOV3ProtocolSessionFactory[
  M >: Group <: Role
] {

  def apply[L](
    context: ActorContext[?],
    args: Any*
  ): ActorRef[Either[L, M]]

}
