package queens
package version.v3
package mco
package interface

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import conf.emito.Conf

import dimensions.fourth.fifth.breed.Spawn
import version.v1.mco.{ MonadCO, Wildcard }
import version.base.Multiplier

import mco.Apply

import base.interface.MCOV3ProtocolIfce

import protocol.base.Protocol.Group
import protocol.session.Protocol.Role


abstract trait ObjectMCOV3ProtocolIfce[
  M >: Group <: Role
] extends MCOV3ProtocolIfce[M] { interface =>

  override object SessionExpand
      extends ObjectMCOV3SessionExpand:

    import version.base.Role
    import version.base.role.Adapter

    override def apply[
      P <: Conf
    ](using
      params: P
    )(context: ActorContext[?],
      end: Role => Boolean,
      stop: () => Unit,
      block: M => Unit,
      mco_v3: Apply[P],
      wildcard: Wildcard[P],
      multiplier: Multiplier[MonadCO[P]]
    ): Int =
      if wildcard(multiplier).size == 0 then
        return 0

      import factory.session.adapter._
      import protocol.session.adapter.{ Protocol => SessionAdapter }
      type SessionAdapterFactory = MCOV3ProtocolSessionAdapterFactory[SessionAdapter.Role]

      var sessionAdapter: SessionAdapter.Actor = null

      def msg_v3(message: Either[Unit, M]): Unit =
        message match
          case Left(_) =>
            sessionAdapter ! Start(sessionAdapter)
          case Right(msg) =>
            block(msg)

      val end_v3 = { (role: Role) =>
                      val r = end(role)
                      if r && role == Adapter then stop()
                      r
                   }

      import version.v1.mco.protocol.client.frontend.{ Protocol => ClientFrontend }

      import factory.session._
      import protocol.session.{ Protocol => Session }
      type SessionFactory = MCOV3ProtocolSessionFactory[Session.Role]

      val session =
        interface(classOf[SessionFactory])[Unit](context,
          { (frontend: ClientFrontend.Actor) =>
            frontend ! Expand(wildcard, multiplier)
          },
          end_v3,
          msg_v3 _,
          { (monadCO: MonadCO[P], spawn: Spawn[P]) =>
            mco_v3(monadCO, spawn)
          }
        ).asInstanceOf[ActorRef[Either[?, SessionAdapter.Role]]]

      sessionAdapter = interface(classOf[SessionAdapterFactory])(context, session, end)

      session ! Left(())

      wildcard(multiplier).size

}
