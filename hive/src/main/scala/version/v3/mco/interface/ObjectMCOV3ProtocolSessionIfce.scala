package queens
package version.v3
package mco
package interface

import scala.util.{ Failure, Success }

import akka.actor.typed.scaladsl.ActorContext

import common.cipher.Crypto
import common.cipher.client.CipherClient

import base.interface.MCOV3ProtocolSessionIfce
import base.interface.MonadCOV3SessionStart

import version.v1.mco.protocol.server.{ Protocol => Server }
import version.v1.mco.protocol.client.{ Protocol => Client }
import version.v1.mco.protocol.client.frontend.{ Protocol => ClientFrontend }
import protocol.session.Protocol.Role


abstract trait ObjectMCOV3ProtocolSessionIfce extends MCOV3ProtocolSessionIfce[
  Role,
  Server.Role,
  Client.Role,
  ClientFrontend.Role
] { interface =>

  override object StartMonadCOV3
      extends ObjectStartMonadCOV3 {

    override def apply[L](
      ctx: ActorContext[?],
      self: Actor[Role],
      args: Any*
    ): MonadCOV3SessionStart[
      Server.Role,
      Client.Role,
      ClientFrontend.Role
    ] = {
      object V3Deseq extends base.MCOV3DeseqSession[L,
        Role,
        ClientFrontend.Role
      ]

      val (_, end, _, apply) = V3Deseq(args)

      val cryptoKey = CipherClient().getOrElse(throw Crypto.CipherException(""))

      val server = {
        import version.v1.mco.factory.server._

        import protocol.session.{ Protocol => Parameter }
        import version.v1.mco.protocol.server.{ Protocol => Interface }

        type ServerFactory = MCOProtocolServerFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(classOf[ServerFactory])(ctx, self, cryptoKey, end)
      }

      val client = {
        import version.v1.mco.factory.client._

        import version.v1.mco.protocol.server.{ Protocol => Parameter }
        import version.v1.mco.protocol.client.{ Protocol => Interface }

        type ClientFactory = MCOProtocolClientFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(classOf[ClientFactory])(ctx, server, cryptoKey, end)
      }

      val frontend = {
        import version.v1.mco.factory.client.frontend._

        import version.v1.mco.protocol.client.{ Protocol => Parameter }
        import version.v1.mco.protocol.client.frontend.{ Protocol => Interface }

        type ClientFrontendFactory = MCOProtocolClientFrontendFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(classOf[ClientFrontendFactory])(ctx, client, end, apply)
      }

      MonadCOV3SessionStart(server, client, frontend)
    }

  }

}
