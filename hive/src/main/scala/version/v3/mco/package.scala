package queens
package version.v3
package mco


package object session {

  import protocol.base.Protocol.Group
  import version.v1.mco.protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
  import protocol.session.Protocol.Role

  type Start[CFM >: Group <: ClientFrontendRole] = Actor[CFM] => Unit
  type Block[L, M >: Group <: Role] = Either[L, M] => Unit

}


object Implicits {

  import interface.ObjectMCOV3ProtocolIfce
  import version.v1.mco.protocol.MCOMessage

  implicit object MCOV3ProtocolIfceImplicit
      extends ObjectMCOV3ProtocolIfce[MCOMessage] { interface =>

    import factory.session._
    import protocol.session.{ Protocol => Session }

    type SessionFactory = MCOV3ProtocolSessionFactory[
      Session.Role
    ]

    interface(
      classOf[SessionFactory],
      impl.MCOV3ProtocolSessionImpl
    )

    import factory.session.adapter._
    import protocol.session.adapter.{ Protocol => SessionAdapter }

    type SessionAdapterFactory = MCOV3ProtocolSessionAdapterFactory[
      SessionAdapter.Role
    ]

    interface(
      classOf[SessionAdapterFactory],
      impl.MCOV3ProtocolSessionAdapterImpl
    )

  }

}


package session {

  object Implicits {

    implicit object MCOV3SessionErrorCodeImplicit
        extends protocol.session.MCOV3SessionErrorCode

    import interface.ObjectMCOV3ProtocolSessionIfce

    implicit object MCOV3ProtocolSessionIfceImplicit
        extends ObjectMCOV3ProtocolSessionIfce { interface =>

      {
        import version.v1.mco.factory.server._

        import protocol.session.{ Protocol => Parameter }
        import version.v1.mco.protocol.server.{ Protocol => Interface }

        type ServerFactory = MCOProtocolServerFactory[
          Interface.Role,
          Parameter.Role
        ]

        import version.v1.mco.interface.ObjectMCOProtocolServerIfce
        import version.v1.mco.server.Implicits._

        interface(
          classOf[ServerFactory],
          new version.v1.mco.impl.MCOProtocolServerFactoryImpl[
            Interface.Role,
            version.v1.mco.protocol.client.defer.Protocol.Role,
            version.v1.mco.protocol.server.defer.Protocol.Role,
            Parameter.Role,
            ObjectMCOProtocolServerIfce
          ]
        )

      }

      {
        import version.v1.mco.factory.client._

        import version.v1.mco.protocol.server.{ Protocol => Parameter }
        import version.v1.mco.protocol.client.{ Protocol => Interface }

        type ClientFactory = MCOProtocolClientFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(
          classOf[ClientFactory],
          version.v1.mco.impl.MCOProtocolClientImpl
        )
      }

      {
        import version.v1.mco.factory.client.frontend._

        import version.v1.mco.protocol.client.{ Protocol => Parameter }
        import version.v1.mco.protocol.client.frontend.{ Protocol => Interface }

        type ClientFrontendFactory = MCOProtocolClientFrontendFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(
          classOf[ClientFrontendFactory],
          version.v1.mco.impl.ClientMCOProtocolFrontendImpl
        )
      }

    }

  }

  package adapter {

    object Implicits {

      implicit object MCOV3SessionAdapterErrorCodeImplicit
          extends protocol.session.adapter.MCOV3SessionAdapterErrorCode

    }

  }

}
