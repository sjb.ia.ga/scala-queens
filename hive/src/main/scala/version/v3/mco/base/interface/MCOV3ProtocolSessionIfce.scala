package queens
package version.v3
package mco
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import version.v1.mco.protocol.server.Protocol.{ Role => ServerRole }
import version.v1.mco.protocol.client.Protocol.{ Role => ClientRole }
import version.v1.mco.protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role


final case class MonadCOV3SessionStart[
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole
](
  server: Actor[SM],
  client: Actor[CM],
  frontend: Actor[CFM]
)


abstract trait MCOV3ProtocolSessionIfce[
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole
] extends NodeIfce {

  protected abstract trait ObjectStartMonadCOV3 {

    def apply[L](
      ctx: ActorContext[?],
      self: Actor[M],
      args: Any*
    ): MonadCOV3SessionStart[SM, CM, CFM]

  }

  def StartMonadCOV3: ObjectStartMonadCOV3

}
