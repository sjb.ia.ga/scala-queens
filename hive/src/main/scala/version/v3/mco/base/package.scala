package queens
package version.v3
package mco


package object base {

  import version.less.plane.base.PlaneDeseqSessionAdapter

  type MCOV3DeseqSessionAdapter = PlaneDeseqSessionAdapter

}
