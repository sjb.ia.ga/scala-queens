package queens
package version.v3
package mco
package base

import akka.actor.typed.TypedActorContext
import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import version.less.plane.base.PlaneProtocolSession
import interface.MCOV3ProtocolSessionIfce
import interface.MonadCOV3SessionStart

import protocol.base.Protocol.Group
import version.v1.mco.protocol.server.Protocol.{ Role => ServerRole }
import version.v1.mco.protocol.client.Protocol.{ Role => ClientRole }
import version.v1.mco.protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role
import protocol.session._


abstract trait MCOV3ProtocolSession[L,
  EC <: MCOV3SessionErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole,
  I <: MCOV3ProtocolSessionIfce[M, SM, CM, CFM]
] extends MCOV3SessionProtocol
    with PlaneProtocolSession[L, EC, M, I]:

  protected abstract trait SessionMCOV3Behavior[T <: SessionMCOV3Behavior[T]]
      extends SessionPlaneBehavior[T]:

    protected val start: session.Start[CFM]
    protected val block: session.Block[L, M]
    protected val apply: version.v1.mco.client.frontend.Apply

    import version.less.plane.protocol.message.session.{ SessionProtocol => sm }

    import errorCode._

    protected def nest_perceive(ctx: ActorContext[?], msg: M): Nothing = msg match

      case sm.Start(self: Actor[M]) =>

        val MonadCOV3SessionStart(server: Actor[SM], client: Actor[CM], frontend: Actor[CFM]) =
          interface
            .StartMonadCOV3[L](ctx, self, start, end, block, apply)

        start(frontend)
        same


      case sm.Stop() =>
        if end(role) then stopped
        same


      case _ =>
        none


    override protected def perceive(ctx: ActorContext[?], msg: M): Nothing =
      try
        super.errorBehavior({ it => block(Right(it)) }, msg)
      catch
        case NoneBehavior =>
          try
            nest_perceive(ctx, msg)
          catch
            case NoneBehavior =>
              block(Right(msg))
              same

    override def receive(ctx: TypedActorContext[Either[L, M]], msg: Either[L, M]): B = msg match

      case Right(_) =>
        super.receive(ctx.asScala, msg)

      case Left(_) =>
        block(msg)
        Behaviors.same.asInstanceOf[B]



  protected abstract trait SessionMCOV3Inflater[T <: SessionMCOV3Behavior[T]]
      extends SessionPlaneInflater[T]
      with MCOV3DeseqSession[L, M, CFM]
