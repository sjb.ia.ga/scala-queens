package queens
package version.v3
package mco
package base

import version.less.plane.base.PlaneProtocolSessionAdapter

import protocol.base.Protocol.Group
import protocol.session.adapter.Protocol.Role
import protocol.session.adapter._


abstract trait MCOV3ProtocolSessionAdapter[
  EC <: MCOV3SessionAdapterErrorCode,
  M >: Group <: Role
] extends MCOV3SessionAdapterProtocol
    with PlaneProtocolSessionAdapter[EC, M]:

  protected abstract trait SessionAdapterMCOV3Behavior[T <: SessionAdapterMCOV3Behavior[T]]
      extends SessionAdapterPlaneBehavior[T]

  protected abstract trait SessionAdapterMCOV3Inflater[T <: SessionAdapterMCOV3Behavior[T]]
      extends SessionAdapterPlaneInflater[T]
      with MCOV3DeseqSessionAdapter
