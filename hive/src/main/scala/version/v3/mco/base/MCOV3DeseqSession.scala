package queens
package version.v3
package mco
package base

import version.base.Deseq
import version.less.plane.base.PlaneDeseqSession3

import session.{ Start, Block }
import version.v1.mco.client.frontend.Apply

import protocol.base.Protocol.Group
import version.v1.mco.protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role


abstract trait MCOV3DeseqSession[L,
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole
] extends Deseq {

  private object Deseq012
      extends PlaneDeseqSession3[L, M, CFM]

  override type Arguments = Tuple4[
    Start[CFM],
    version.base.Role => Boolean,
    Block[L, M],
    Apply
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val (start, end, block) = Deseq012(args.take(3))
    val apply = args(3).asInstanceOf[Apply]
    (start, end, block, apply)
  }

}
