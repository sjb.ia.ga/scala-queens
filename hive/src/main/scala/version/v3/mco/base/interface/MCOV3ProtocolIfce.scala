package queens
package version.v3
package mco
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import conf.emito.Conf

import version.v1.mco.{ MonadCO, Wildcard }
import version.base.Multiplier

import mco.Apply

import version.less.plane.base.interface.PlaneProtocolIfce

import protocol.base.Protocol.Group
import protocol.session.Protocol.Role


abstract trait MCOV3ProtocolIfce[
  M >: Group <: Role
] extends PlaneProtocolIfce:

  protected abstract trait ObjectMCOV3SessionExpand
      extends ObjectSessionExpand { self =>

    def apply[
      P <: Conf
    ](using
      params: P
    )(context: ActorContext[?],
      end: version.base.Role => Boolean,
      stop: () => Unit,
      block: M => Unit,
      apply: Apply[P],
      wildcard: Wildcard[P],
      multiplier: Multiplier[MonadCO[P]] = Multiplier.Once()
    ): Int

  }

  override def SessionExpand: ObjectMCOV3SessionExpand
