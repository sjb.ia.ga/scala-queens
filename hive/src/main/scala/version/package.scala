package queens
package version

import akka.actor.typed.ActorRef


package less {

  package object plane {

    import protocol.base.Protocol.Group

    type Actor[M >: Group] = ActorRef[M]

  }

  package object nest {

    import protocol.base.Protocol.Group

    type Actor[M >: Group] = ActorRef[M]

    import nest.Nest
    import dimensions.three.breed.Hatch

    type Apply = (Nest, Hatch[?]) => Unit

  }

}


package v1 {

  package object mco {

    import protocol.base.Protocol.Group

    type Actor[M >: Group] = ActorRef[M]

  }

  package object vector {

    import protocol.base.Protocol.Group

    type Actor[M >: Group] = ActorRef[M]

    import breed.Hive

    import conf.emito.Conf

    type Apply[P <: Conf] = (VectorV1[P], Option[?] => Hive[P]) => Unit

  }

}


package v2 {

  package object plane {

    import protocol.base.Protocol.Group

    type Actor[M >: Group] = ActorRef[M]

  }

  package object vector {

    import plane.protocol.base.Protocol.Group

    type Actor[M >: Group] = plane.Actor[M]

    import version.v1.vector.breed.Hive

    import conf.emito.Conf

    type Apply[P <: Conf] = (VectorV2, Option[?] => Hive[P]) => Unit

  }

}


package v3 {

  package object mco {

    import protocol.base.Protocol.Group

    type Actor[M >: Group] = ActorRef[M]

    import dimensions.fourth.fifth.breed.Spawn
    import version.v1.mco.MonadCO

    import conf.emito.Conf

    type Apply[P <: Conf] = (MonadCO[P], Spawn[P]) => Unit

  }

}
