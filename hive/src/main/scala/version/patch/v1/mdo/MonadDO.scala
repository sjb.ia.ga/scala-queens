package queens
package version.patch.v1
package mdo

import dimensions.Dimension
import dimensions.Dimension.Projection
import dimensions.Dimension.{ Model, Interface, Use }
import dimensions.Dimension.Use.Output
import dimensions.Dimension.Use.Output.Database
import dimensions.Dimension.Use.Output.Database.{ Slick, Scalike }

import conf.emito.Conf

import dimensions.fourth.fifth.breed.Spawn
import dimensions.fourth.fifth.breed.store.`Slick Store'o`
import dimensions.fourth.fifth.breed.store.`Scalikejdbc Store'o`

import version.base.Vector

import version.v1.mco.MonadCO


class MonadDO[
  P <: Conf
](
  _m: Model,
  final override val use: Use
)(using
  override protected val * : P
) extends MonadCO[P](_m):

  require(use match { case Output(Database(_)) => true case _ => false })

  override protected object patch
      extends Patch:
    override def apply(): Spawn[P] = use match
      case Output(Database(Slick(_, _))) => `Slick Store'o`[P](model)
      case Output(Database(Scalike(_, _))) => `Scalikejdbc Store'o`[P](model)

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[MonadDO[?]]


object MonadDO:

  def apply[
    P <: Conf
  ](
    it: Projection
  )(using
    params: P
  ): MonadCO[P] =
    assert(it.size == 3)
    require(it(1) == Interface.monadProgram)
    new MonadDO[P](it(0), it(2))
