package queens
package version.patch.v1
package mdo

import dimensions.Dimension.{ Model, Interface, Use }
import dimensions.Dimension.Interface.Program
import dimensions.Dimension.Interface.Program.Monad
import dimensions.Dimension.Use.Output
import dimensions.Dimension.Use.Output.Database

import conf.emito.Conf

import version.base.Plane

import version.v1.mco.MonadCO


class Wildcard[
  P <: Conf
] protected (
  override protected[version] val filter: Wildcard.Filter
)(using override protected val * : P)
    extends version.v1.mco.Wildcard[P]:

  def this()(using params: P) = this(Wildcard.star)

  override protected def apply(factor: MonadCO[P] => Int): List[MonadCO[P]] =
    super.apply(factor, MonadDO.apply)


object Wildcard:

  type Filter = version.v1.mco.Wildcard.Filter

  private val star: Filter = (_, _) match
    case (Model, _) | (Interface, Program(Monad)) | (Use, Output(Database(_))) => true
    case (_, _) => false

  def *[P <: Conf](using params: P) = new Wildcard[P]()

  def apply[
    P <: Conf
  ](using params: P)(filter: Filter, wildcard: Wildcard[P] = *[P]) =
    new Wildcard[P]({ (di, it) =>
      filter(di, it) && wildcard.filter(di, it)
    })

  def apply(monadDO: MonadDO[?]*): Filter = (_, _) match
    case (Model, it: Model) => monadDO.map(_.model).exists(it.==)
    case (Use, Output(Database(_))) => true
    case (Use, _) => false
    case it => star(it._1, it._2)

  def apply(model: Model, use: Use): Filter = (_, _) match
    case (Model, `model`) => true
    case (Use, `use`) if use match { case Output(Database(_)) => true case _ => false } => true
    case (Use, _) => false
    case it => star(it._1, it._2)
