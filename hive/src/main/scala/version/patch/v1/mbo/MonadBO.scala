package queens
package version.patch.v1
package mbo

import dimensions.Dimension
import dimensions.Dimension.Projection
import dimensions.Dimension.{ Model, Interface, Use }
import dimensions.Dimension.Use.Output
import dimensions.Dimension.Use.Output.Buffered
import dimensions.Dimension.Interface.monadProgram

import conf.emito.Conf

import dimensions.fourth.fifth.breed.Spawn
import dimensions.fourth.fifth.breed.`Buffered'o`

import version.base.Vector

import version.v1.mco.MonadCO


class MonadBO[
  P <: Conf
](
  _m: Model,
  final override val use: Use
)(using
  override protected val * : P
) extends MonadCO[P](_m):

  require(use match { case Output(Buffered) => true case _ => false })

  override protected object patch
      extends Patch:
    override def apply(): Spawn[P] = `Buffered'o`[P](model)

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[MonadBO[?]]


object MonadBO:

  def apply[
    P <: Conf
  ](
    it: Projection
  )(using
    params: P
  ): MonadBO[P] =
    assert(it.size == 3)
    require(it(1) == monadProgram)
    new MonadBO[P](it(0), it(2))
