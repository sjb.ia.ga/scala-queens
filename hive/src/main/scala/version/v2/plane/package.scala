package queens
package version.v2
package plane


package object session {

  import protocol.base.Protocol.Group
  import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
  import protocol.session.Protocol.Role

  type Start[CFM >: Group <: ClientFrontendRole] = Actor[CFM] => Unit
  type Block[L, M >: Group <: Role] = Either[L, M] => Unit

}


package client {

  package object frontend {

    import version.base.Plane
    import queens.base.breed.Breed

    type Apply[P <: Plane, R <: Breed] = (P, R) => Unit

  }

}


package session {

  object Implicits {

    implicit object PlaneV2SessionErrorCodeImplicit
        extends protocol.session.PlaneV2SessionErrorCode

    import interface.ObjectPlaneV2ProtocolSessionIfce

    implicit object PlaneV2ProtocolSessionIfceImplicit
        extends ObjectPlaneV2ProtocolSessionIfce { interface =>

      {
        import factory.server._

        import protocol.session.{ Protocol => Parameter }
        import protocol.server.{ Protocol => Interface }

        type ServerFactory = PlaneV2ProtocolServerFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(
          classOf[ServerFactory],
          impl.PlaneV2ProtocolServerImpl
        )
      }

      {
        import factory.client._

        import protocol.server.{ Protocol => Parameter }
        import protocol.client.{ Protocol => Interface }

        type ClientFactory = PlaneV2ProtocolClientFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(
          classOf[ClientFactory],
          impl.PlaneV2ProtocolClientImpl
        )
      }

      {
        import factory.client.frontend._

        import protocol.client.{ Protocol => Parameter }
        import protocol.client.frontend.{ Protocol => Interface }

        type ClientFrontendFactory = PlaneV2ProtocolClientFrontendFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(
          classOf[ClientFrontendFactory],
          impl.ClientPlaneV2ProtocolFrontendImpl
        )
      }

    }

  }

  package adapter {

    object Implicits {

      implicit object PlaneV2SessionAdapterErrorCodeImplicit
          extends protocol.session.adapter.PlaneV2SessionAdapterErrorCode

    }

  }

}


package server {

  object Implicits {

    implicit object PlaneV2ServerErrorCodeImplicit
        extends protocol.server.PlaneV2ServerErrorCode

    import interface.ObjectPlaneV2ProtocolServerIfce

    implicit object PlaneV2ProtocolServerIfceImplicit
        extends ObjectPlaneV2ProtocolServerIfce { interface =>

      import factory.server.defer._

      import protocol.client.defer.{ Protocol => Parameter }
      import protocol.server.defer.{ Protocol => Interface }

      type ServerDeferFactory = PlaneV2ProtocolServerDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(
        classOf[ServerDeferFactory],
        impl.ServerPlaneV2ProtocolDeferImpl
      )

    }

  }

  package defer {

    object Implicits {

      implicit object PlaneV2ServerDeferErrorCodeImplicit
          extends protocol.server.defer.PlaneV2ServerDeferErrorCode

      import interface.ObjectServerPlaneV2ProtocolDeferIfce

      implicit object ServerPlaneV2ProtocolDeferIfceImplicit
          extends ObjectServerPlaneV2ProtocolDeferIfce { interface =>

        import factory.server.backend._

        import protocol.server.defer.{ Protocol => Parameter1 }
        import protocol.client.defer.{ Protocol => Parameter2 }
        import protocol.server.backend.{ Protocol => Interface }

        type ServerBackendFactory = PlaneV2ProtocolServerBackendFactory[
          Interface.Role,
          Parameter1.Role,
          Parameter2.Role
        ]

        interface(
          classOf[ServerBackendFactory],
          impl.ServerPlaneV2ProtocolBackendImpl
        )

      }

    }

  }

  package backend {

    object Implicits {

      implicit object PlaneV2ServerBackendErrorCodeImplicit
          extends protocol.server.backend.PlaneV2ServerBackendErrorCode

    }

  }

}


package client {

  object Implicits {

    implicit object PlaneV2ClientErrorCodeImplicit
        extends protocol.client.PlaneV2ClientErrorCode

    import interface.ObjectPlaneV2ProtocolClientIfce

    implicit object PlaneV2ProtocolClientIfceImplicit
        extends ObjectPlaneV2ProtocolClientIfce { interface =>

      import factory.client.defer._

      import protocol.client.frontend.{ Protocol => Parameter }
      import protocol.client.defer.{ Protocol => Interface }

      type ClientDeferFactory = PlaneV2ProtocolClientDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(
        classOf[ClientDeferFactory],
        impl.ClientPlaneV2ProtocolDeferImpl
      )

    }

  }

  package defer {

    object Implicits {

      implicit object PlaneV2ClientDeferErrorCodeImplicit
          extends protocol.client.defer.PlaneV2ClientDeferErrorCode

    }

  }

  package frontend {

    object Implicits {

      implicit object PlaneV2ClientFrontendErrorCodeImplicit
          extends protocol.client.frontend.PlaneV2ClientFrontendErrorCode

    }

  }

}
