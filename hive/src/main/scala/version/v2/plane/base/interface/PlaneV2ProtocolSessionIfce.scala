package queens
package version.v2
package plane
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed
import version.base.Plane

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role


final case class PlaneV2SessionStart[
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole
](
  server: Actor[SM],
  client: Actor[CM],
  frontend: Actor[CFM]
)


abstract trait PlaneV2ProtocolSessionIfce[
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole
] extends NodeIfce {

  protected abstract trait ObjectStartV2 {

    def apply[L,
      P <: Plane,
      R <: Breed
    ](
      ctx: ActorContext[?],
      self: Actor[M],
      args: Any*
    ): PlaneV2SessionStart[SM, CM, CFM]

  }

  def StartV2: ObjectStartV2

}
