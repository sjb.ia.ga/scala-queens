package queens
package version.v2
package plane


package object base {

  import version.base.{ `DeseqAxes*`, DeseqUUID, `DeseqPlane*`, Plane }

  type PlaneV2DeseqClient = `DeseqAxes*`

  type PlaneV2DeseqServer = `DeseqAxes*`

  type PlaneV2DeseqServerBackend = DeseqUUID

  type PlaneV2DeseqClientDefer[P <: Plane] = `DeseqPlane*`[P]

  import version.less.plane.base.PlaneDeseqSessionAdapter

  type PlaneV2DeseqSessionAdapter = PlaneDeseqSessionAdapter

}
