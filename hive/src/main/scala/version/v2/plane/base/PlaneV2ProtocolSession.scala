package queens
package version.v2
package plane
package base

import akka.actor.typed.TypedActorContext
import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import queens.base.breed.Breed

import version.base.Plane

import version.less.plane.base.PlaneProtocolSession
import interface.PlaneV2ProtocolSessionIfce
import interface.PlaneV2SessionStart

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role
import protocol.session._


abstract trait PlaneV2ProtocolSession[L,
  P <: Plane,
  R <: Breed,
  EC <: PlaneV2SessionErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole,
  I <: PlaneV2ProtocolSessionIfce[M, SM, CM, CFM]
] extends PlaneV2SessionProtocol
    with PlaneProtocolSession[L, EC, M, I]:

  protected abstract trait SessionPlaneV2Behavior[T <: SessionPlaneV2Behavior[T]]
      extends SessionPlaneBehavior[T]:

    import dimensions.Dimension.Axes

    protected val start: session.Start[CFM]
    protected val block: session.Block[L, M]
    protected val apply: client.frontend.Apply[P, R]

    protected val axes: Axes

    import version.less.plane.protocol.message.session.{ SessionProtocol => sm }

    import errorCode._

    override protected def perceive(ctx: ActorContext[?], msg: M): Nothing =

      try
        super.errorBehavior({ it => block(Right(it)) }, msg)
      catch
        case NoneBehavior =>

      msg match

        case sm.Start(self: Actor[M]) =>

          val PlaneV2SessionStart(server: Actor[SM], client: Actor[CM], frontend: Actor[CFM]) =
            interface.StartV2[L, P, R](ctx, self, start, end, block, apply, axes)

          start(frontend)
          same


        case sm.Stop() =>
          if end(role) then stopped
          same


        case _ =>
          block(Right(msg))
          same



    override def receive(ctx: TypedActorContext[Either[L, M]], msg: Either[L, M]): B = msg match

      case Right(_) =>
        super.receive(ctx.asScala, msg)

      case Left(_) =>
        block(msg)
        Behaviors.same.asInstanceOf[B]



  protected abstract trait SessionPlaneV2Inflater[T <: SessionPlaneV2Behavior[T]]
      extends SessionPlaneInflater[T]
      with PlaneV2DeseqSession[L, P, R, M, CFM]
