package queens
package version.v2
package plane
package base

import akka.actor.typed.scaladsl.ActorContext

import dimensions.Dimension.Axes

import version.base.Plane

import version.less.plane.base.ServerPlaneProtocolDefer
import interface.ServerPlaneV2ProtocolDeferIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.server.defer.Protocol.Role
import protocol.server.defer._


abstract trait ServerPlaneV2ProtocolDefer[
  P <: Plane,
  EC <: PlaneV2ServerDeferErrorCode,
  M >: Group <: Role,
  SBM >: Group <: ServerBackendRole,
  CDM >: Group <: ClientDeferRole,
  I <: ServerPlaneV2ProtocolDeferIfce[SBM, M, CDM]
] extends PlaneV2ServerDeferProtocol
    with ServerPlaneProtocolDefer[EC, M, CDM, I]:

  protected abstract trait ServerPlaneV2DeferBehavior[T <: ServerPlaneV2DeferBehavior[T]]
      extends ServerPlaneDeferBehavior[T]:

    protected val axes: Axes

    import version.less.plane.protocol.message.resume.{ ResumeProtocol => rm }
    import version.less.plane.protocol.message.grace.{ GraceProtocol => gm }

    import errorCode._

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =

      msg match

        case rm.Resume(axes) if axes != this.axes =>
          readyTo ! invalidPlane(axes)
          stopped

        case gm.Grace(axes) if axes != this.axes =>
          readyTo ! invalidPlane(axes)
          stopped

        case _ =>


      try
        super.errorBehavior(readyTo, msg)
      catch
        case NoneBehavior =>

      msg match

        case rm.Resume(axes) if axes == this.axes =>
          val self: Actor[M] = ctx.self

          val planeAt: Actor[SBM] = interface
            .ResumeV2[P](ctx, graceTo = self, resultTo = readyTo, cryptoKey, secret)

          readyTo ! Ready(planeAt, PlaneV2(_))
          same

        case gm.Grace(axes) if axes == this.axes =>
          stopped

        case _ =>
          readyTo ! Complaint(msg)
          stopped




  protected abstract trait ServerPlaneV2DeferInflater[T <: ServerPlaneV2DeferBehavior[T]]
      extends RolePlaneInflater[T]
      with PlaneV2DeseqServerDefer
