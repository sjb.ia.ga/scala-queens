package queens
package version.v2
package plane
package base

import queens.base.breed.Breed

import version.base.{ Deseq, Plane }

import client.frontend.Apply


abstract trait PlaneV2DeseqClientFrontend[
  P <: Plane,
  R <: Breed
] extends Deseq {

  override type Arguments = Tuple2[
    version.base.Role => Boolean,
    Apply[P, R]
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val end = args(0).asInstanceOf[version.base.Role => Boolean]
    val apply = args(1).asInstanceOf[Apply[P, R]]
    (end, apply)
  }

}
