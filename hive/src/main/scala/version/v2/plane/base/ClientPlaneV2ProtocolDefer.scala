package queens
package version.v2
package plane
package base

import java.util.UUID

import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed

import common.cipher.client.CipherClient

import version.base.Plane

import version.less.plane.base.ClientPlaneProtocolDefer

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.client.defer.Protocol.Role
import protocol.client.defer._


abstract trait ClientPlaneV2ProtocolDefer[
  P <: Plane,
  R <: Breed,
  EC <: PlaneV2ClientDeferErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  SDM >: Group <: ServerDeferRole,
  SBM >: Group <: ServerBackendRole,
  CFM >: Group <: ClientFrontendRole
] extends PlaneV2ClientDeferProtocol
    with ClientPlaneProtocolDefer[EC, M, CFM]:

  protected abstract trait ClientPlaneV2DeferBehavior[T <: ClientPlaneV2DeferBehavior[T]]
      extends ClientPlaneDeferBehavior[T]:

    protected val plane: P

    import version.less.plane.protocol.message.defer.{ DeferProtocol => dm }
    import version.less.plane.protocol.message.plane.{ PlaneProtocol => pm }
    import version.less.plane.protocol.message.ready.{ ReadyProtocol => ym }
    import version.less.plane.protocol.message.result.{ ResultProtocol => rm }

    import errorCode._

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =

      msg match

        case dm.Defer(_, pm.Plane(_, axes), _) if axes != plane.axes =>
          onBehalf ! invalidPlane(axes)
          stopped


        case _ =>


      try
        super.errorBehavior(onBehalf, msg)
      catch
        case NoneBehavior =>


      msg match

        case dm.Defer(resumeAt: Actor[SDM], pm.Plane(_, axes), it) if axes == plane.axes =>
          CipherClient(cryptoKey, it) match
            case Some(uuid) =>
              secret = uuid
              resumeAt ! Resume(axes)
              same
            case _ =>
              resumeAt ! errorSecret(role)
              stopped


        case ym.Ready(planeAt: Actor[SBM], hiMessage: Function[UUID, SBM] @unchecked) =>
          val breedV2Msg: SBM = BreedV2(plane)
          planeAt ! hiMessage(secret)
          planeAt ! breedV2Msg
          same


        case rm.Result(breed: R) =>
          onBehalf ! Append(plane, breed)
          stopped


        case _ =>
          onBehalf ! Complaint(msg)
          same

  protected abstract trait ClientPlaneV2DeferInflater[T <: ClientPlaneV2DeferBehavior[T]]
      extends RolePlaneInflater[T]
      with PlaneV2DeseqClientDefer[P]
