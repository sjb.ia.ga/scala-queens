package queens
package version.v2
package plane
package base

import version.less.plane.base.PlaneProtocolSessionAdapter

import protocol.base.Protocol.Group
import protocol.session.adapter.Protocol.Role
import protocol.session.adapter._


abstract trait PlaneV2ProtocolSessionAdapter[
  EC <: PlaneV2SessionAdapterErrorCode,
  M >: Group <: Role
] extends PlaneV2SessionAdapterProtocol
    with PlaneProtocolSessionAdapter[EC, M] {

  protected abstract trait SessionAdapterPlaneV2Behavior[T <: SessionAdapterPlaneV2Behavior[T]]
      extends SessionAdapterPlaneBehavior[T]

  protected abstract trait SessionAdapterPlaneV2Inflater[T <: SessionAdapterPlaneV2Behavior[T]]
      extends SessionAdapterPlaneInflater[T]
      with PlaneV2DeseqSessionAdapter

}
