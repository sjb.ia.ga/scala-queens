package queens
package version.v2
package plane
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import version.base.Plane

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.Role


abstract trait ServerPlaneV2ProtocolDeferIfce[
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
] extends NodeIfce {

  protected abstract trait ObjectResumeV2 {

    def apply[
      P <: Plane
    ](
      ctx: ActorContext[?],
      graceTo: Actor[SDM],
      resultTo: Actor[CDM],
      args: Any*
    ): Actor[M]

  }

  def ResumeV2: ObjectResumeV2

}
