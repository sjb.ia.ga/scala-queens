package queens
package version.v2
package plane
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import version.base.Plane
import queens.base.breed.Breed

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.Role


abstract trait PlaneV2ProtocolClientIfce[
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole
] extends NodeIfce {

  abstract trait ObjectBreedV2 {

    def apply[
      P <: Plane,
      R <: Breed
    ](
      ctx: ActorContext[?],
      onBehalf: Actor[CFM],
      args: Any*
    ): Actor[M]

  }

  def BreedV2: ObjectBreedV2

}
