package queens
package version.v2
package plane
package base

import akka.actor.typed.scaladsl.ActorContext

import dimensions.Dimension.Axes

import common.cipher.client.CipherClient

import version.base.Plane

import version.less.plane.base.PlaneProtocolServer
import interface.PlaneV2ProtocolServerIfce

import protocol.base.Protocol.Group
import protocol.session.Protocol.{ Role => SessionRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.Protocol.Role
import protocol.server._


abstract trait PlaneV2ProtocolServer[
  P <: Plane,
  EC <: PlaneV2ServerErrorCode,
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole,
  SDM >: Group <: ServerDeferRole,
  SM >: Group <: SessionRole,
  I <: PlaneV2ProtocolServerIfce[SDM, CDM],
] extends PlaneV2ServerProtocol
    with PlaneProtocolServer[EC, M, SM, I] {

  protected abstract trait ServerPlaneV2Behavior[T <: ServerPlaneV2Behavior[T]]
      extends ServerPlaneBehavior[T] {

    protected val axes: Axes

    import version.less.plane.protocol.message.plane.{ PlaneProtocol => pm }
    import version.less.plane.protocol.message.grace.{ GraceProtocol => gm }

    import errorCode._

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing = {

      try {
        super.errorBehavior(session, msg)
      } catch {
        case NoneBehavior =>
      }

      msg match {

        case pm.Plane(deferTo: Actor[CDM], axes) if axes == this.axes =>
          val secret = java.util.UUID.randomUUID

          val resumeAt: Actor[SDM] = interface
            .PlaneV2[P](ctx, readyTo = deferTo, axes, cryptoKey, secret)

          CipherClient(cryptoKey, secret) match
            case Some(it) =>
              deferTo ! Defer(resumeAt, msg, it)
              same
            case _ =>
              deferTo ! errorSecret(role)
              stopped


        case gm.Grace(axes) if axes == this.axes =>
          session ! Stop()
          if end(role) then stopped
          same


        case _ =>
      }

      try {
        super.errorBehavior(session, msg, after = true)
      } catch {
        case NoneBehavior =>
          session ! Complaint(msg)
          same
      }

    }

  }

  protected abstract trait ServerPlaneV2Inflater[T <: ServerPlaneV2Behavior[T]]
      extends RolePlaneInflater[T]
      with PlaneV2DeseqServer

}
