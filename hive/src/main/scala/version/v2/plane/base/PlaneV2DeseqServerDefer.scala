package queens
package version.v2
package plane
package base

import java.util.UUID

import dimensions.Dimension.Axes

import version.base.{ Deseq, DeseqAxes, DeseqUUID }


abstract trait PlaneV2DeseqServerDefer
    extends Deseq {

  private object Deseq0
      extends DeseqAxes

  private object Deseq12
      extends DeseqUUID

  override type Arguments = Tuple3[
    Axes,
    CryptoKey,
    UUID
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val Tuple1(axes) = Deseq0(args.take(1))
    val (key, secret) = Deseq12(args.drop(1).take(2))
    Tuple3[Axes, CryptoKey, UUID](axes, key, secret)
  }

}
