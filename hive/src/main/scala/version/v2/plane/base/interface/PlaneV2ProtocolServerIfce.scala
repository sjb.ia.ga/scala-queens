package queens
package version.v2
package plane
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import version.base.Plane

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.Role


abstract trait PlaneV2ProtocolServerIfce[
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole
] extends NodeIfce {

  protected abstract trait ObjectPlaneV2 {

    def apply[
      P <: Plane
    ](
      ctx: ActorContext[?],
      readyTo: Actor[CDM],
      args: Any*
    ): Actor[M]

  }

  def PlaneV2: ObjectPlaneV2

}
