package queens
package version.v2
package plane
package base

import version.base.Plane
import queens.base.breed.Breed

import dimensions.Dimension.Axes

import version.base.{ Deseq, DeseqAxes }
import version.less.plane.base.PlaneDeseqSession3

import session.{ Start, Block }
import client.frontend.Apply

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role


abstract trait PlaneV2DeseqSession[L,
  P <: Plane,
  R <: Breed,
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole
] extends Deseq {

  private object Deseq012
      extends PlaneDeseqSession3[L, M, CFM]

  private object Deseq4
      extends DeseqAxes

  override type Arguments = Tuple5[
    Start[CFM],
    version.base.Role => Boolean,
    Block[L, M],
    Apply[P, R],
    Axes
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val (start, end, block) = Deseq012(args.take(3))
    val apply = args(3).asInstanceOf[Apply[P, R]]
    val Tuple1(axes) = Deseq4(args.drop(4).take(1))
    (start, end, block, apply, axes)
  }

}
