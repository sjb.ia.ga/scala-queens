package queens
package version.v2
package plane
package base

import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed

import dimensions.Dimension.Axes

import version.base.Plane

import version.less.plane.base.PlaneProtocolClient
import interface.PlaneV2ProtocolClientIfce

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.client.Protocol.Role
import protocol.client._


abstract trait PlaneV2ProtocolClient[
  P <: Plane,
  R <: Breed,
  EC <: PlaneV2ClientErrorCode,
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole,
  CDM >: Group <: ClientDeferRole,
  SM >: Group <: ServerRole,
  I <: PlaneV2ProtocolClientIfce[CDM, CFM]
] extends PlaneV2ClientProtocol
    with PlaneProtocolClient[EC, M, SM, I] {

  protected abstract trait ClientPlaneV2Behavior[T <: ClientPlaneV2Behavior[T]]
      extends ClientPlaneBehavior[T] {

    protected val axes: Axes

    import version.less.plane.protocol.message.breed.{ BreedProtocol => bm }
    import version.less.plane.protocol.message.grace.{ GraceProtocol => gm }

    import errorCode._

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing = {

      try {
        super.errorBehavior(server, msg)
      } catch {
        case NoneBehavior =>
      }

      msg match {

        case bm.Breed(plane: P @unchecked, onBehalf: Actor[CFM]) if plane.axes == this.axes =>
          val deferTo: Actor[CDM] = interface
            .BreedV2[P, R](ctx, onBehalf, cryptoKey, plane)

          server ! Plane(deferTo, plane.axes)
          same


        case bm.Breed(plane: P @unchecked, _) =>
          server ! invalidPlane(plane.axes)
          stopped


        case gm.Grace(axes) if axes == this.axes =>
          server ! Grace(axes)

          if end(role) then stopped

          same

        case _ =>
          server ! Complaint(msg)
          same

      }

      super.errorBehavior(server, msg, after = true)

    }

  }

  protected abstract trait ClientPlaneV2Inflater[T <: ClientPlaneV2Behavior[T]]
      extends RolePlaneInflater[T]
      with PlaneV2DeseqClient

}
