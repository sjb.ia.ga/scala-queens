package queens
package version.v2
package plane
package base

import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed
import version.base.{ Wildcard, Multiplier }

import version.base.Plane

import version.less.plane.base.ClientPlaneProtocolFrontend

import protocol.base.Protocol.Group
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.Role
import protocol.client.frontend._


abstract trait ClientPlaneV2ProtocolFrontend[
  P <: Plane,
  R <: Breed,
  EC <: PlaneV2ClientFrontendErrorCode,
  M >: Group <: Role,
  CM >: Group <: ClientRole
] extends PlaneV2ClientFrontendProtocol
    with ClientPlaneProtocolFrontend[EC, M, CM]:

  protected abstract trait ClientPlaneV2FrontendBehavior[T <: ClientPlaneV2FrontendBehavior[T]]
      extends ClientPlaneFrontendBehavior[T]:

    private var count = 0

    import version.less.plane.protocol.message.expand.{ ExpandProtocol => em }
    import version.less.plane.protocol.message.append.{ AppendProtocol => am }

    import errorCode._

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try
        super.errorBehavior(client, msg)
      catch
        case NoneBehavior =>

      msg match

        case em.Expand(w, m) if w.isInstanceOf[Wildcard[P]] && m.isInstanceOf[Multiplier[P]] =>
          val self: Actor[M] = ctx.self

          val wildcard: Wildcard[P] = w.asInstanceOf[Wildcard[P]]
          val multiplier: Multiplier[P] = m.asInstanceOf[Multiplier[P]]

          val ns =
            for
              it <- wildcard(multiplier)
            yield it.asInstanceOf[P]

          assert(count == 0)

          for (it <- ns)
            client ! Breed(it, onBehalf = self)
            count += 1

          if count == 0 then
            client ! Grace(wildcard.axes)

            if end(role) then stopped

          same


        case am.Append(plane: P @unchecked, breed: R) if count > 0 =>
          count -= 1

          apply(plane, breed)

          if count == 0 then
            client ! Grace(plane.axes)

            if end(role) then stopped

          same


        case am.Append(plane: P @unchecked, breed: R) =>
          client ! overflow
          stopped


        case _ =>


      try
        super.errorBehavior(client, msg, after = true)
      catch
        case NoneBehavior =>
          client ! Complaint(msg)
          same



  protected abstract trait ClientPlaneV2FrontendInflater[T <: ClientPlaneV2FrontendBehavior[T]]
      extends RolePlaneInflater[T]
      with PlaneV2DeseqClientFrontend[P, R]
