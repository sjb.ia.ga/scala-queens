package queens
package version.v2
package plane
package base

import akka.actor.typed.scaladsl.ActorContext

import dimensions.three.breed.{ Hatch, Hatcher }
import Hatch.Flavor

import version.less.nest.Nest
import version.less.nest.protocol.server.backend.HatcherErrorCode

import dimensions.fourth.fifth.breed.Spawn
import version.v1.mco.MonadCO

import version.base.Plane

import version.less.plane.base.ServerPlaneProtocolBackend

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.Role
import protocol.server.backend._


abstract trait ServerPlaneV2ProtocolBackend[
  P <: Plane,
  EC <: PlaneV2ServerBackendErrorCode,
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
] extends PlaneV2ServerBackendProtocol
    with ServerPlaneProtocolBackend[EC, M, SDM, CDM]:

  protected abstract trait ServerPlaneV2BackendBehavior[T <: ServerPlaneV2BackendBehavior[T]]
      extends ServerPlaneBackendBehavior[T]
      with Hatcher { self: Hatcher =>

    import version.v2.plane.protocol.message.salute.{ PlaneV2Protocol => pm }
    import version.v2.plane.protocol.message.breed.{ BreedV2Protocol => bm }

    import errorCode._

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =

      msg match

        case pm.PlaneV2(it) =>
          if saluted
          then
            resultTo ! dupPlaneV2
          else
            saluted = true

            if it == secret
            then
              same
            else
              resultTo ! badSecret

          stopped


        case _ =>


      try
        super.errorBehavior(resultTo, msg)
      catch
        case NoneBehavior =>


      msg match

        case bm.BreedV2Nest(nest, flavor) =>
          self(nest, flavor) match
            case Some(hatch) =>
              resultTo ! Result(hatch)
            case _ =>
              resultTo ! isntFlavor(flavor)

          graceTo ! Grace(nest.axes)
          stopped


        case bm.BreedV2MonadCO(monadCO) =>
          import version.v1.mco.MonadCO.given
          val spawn: Spawn[?] = monadCO

          resultTo ! Result(spawn)
          graceTo ! Grace(MonadCO.axes)
          stopped


        case _ =>
          resultTo ! Complaint(msg)
          same

  }

  protected abstract trait ServerPlaneV2BackendInflater[T <: ServerPlaneV2BackendBehavior[T]]
      extends RolePlaneInflater[T]
      with PlaneV2DeseqServerBackend
