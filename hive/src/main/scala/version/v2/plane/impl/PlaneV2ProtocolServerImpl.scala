package queens
package version.v2
package plane
package impl

import akka.actor.typed.scaladsl.ActorContext

import dimensions.Dimension.Axes

import version.base.Plane

import base.PlaneV2ProtocolServer
import base.interface.PlaneV2ProtocolServerIfce

import protocol.base.Protocol.Group
import protocol.session.Protocol.{ Role => SessionRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.Protocol.Role
import protocol.server._


class PlaneV2ProtocolServerImpl[
  P <: Plane,
  EC <: PlaneV2ServerErrorCode,
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole,
  SDM >: Group <: ServerDeferRole,
  SM >: Group <: SessionRole,
  I <: PlaneV2ProtocolServerIfce[SDM, CDM]
](
  override protected val session: Actor[SM]
)(implicit
  override protected val errorCode: EC,
  override protected val interface: I
) extends PlaneV2ProtocolServer[P, EC, M, CDM, SDM, SM, I] {

  class ServerPlaneV2BehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val end: version.base.Role => Boolean,
    override protected val axes: Axes
  ) extends ServerPlaneV2Behavior[ServerPlaneV2BehaviorImpl]

  private object ServerPlaneV2Inflater
      extends ServerPlaneV2Inflater[ServerPlaneV2BehaviorImpl] {

    override def apply(em: Arguments): ServerPlaneV2BehaviorImpl =
      new ServerPlaneV2BehaviorImpl(em._1, em._2, em._3)

  }

  private object ServerPlaneV2BehaviorImpl
      extends ObjectRolePlaneBehavior[ServerPlaneV2BehaviorImpl, ServerPlaneV2Inflater.type] {

    override protected val inflater = ServerPlaneV2Inflater

  }

}


import factory.server._

import protocol.session.adapter.{ Protocol => Parameter }
import protocol.server.{ Protocol => Interface }

object PlaneV2ProtocolServerImpl extends PlaneV2ProtocolServerFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply[
    P <: Plane,
  ](
    context: ActorContext[?],
    session: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import interface.ObjectPlaneV2ProtocolServerIfce
      import server.Implicits._

      new PlaneV2ProtocolServerImpl[
        P,
        PlaneV2ServerErrorCode,
        Role,
        protocol.client.defer.Protocol.Role,
        protocol.server.defer.Protocol.Role,
        Parameter.Role,
        ObjectPlaneV2ProtocolServerIfce
      ](
        session
      ).ServerPlaneV2BehaviorImpl(args*)
    }

}
