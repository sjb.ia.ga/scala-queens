package queens
package version.v2
package plane
package impl

import akka.actor.typed.{ ActorRef, Behavior }
import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed

import version.base.Plane

import base.PlaneV2ProtocolSession
import base.interface.PlaneV2ProtocolSessionIfce

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role
import protocol.session._


class PlaneV2ProtocolSessionImpl[L,
  P <: Plane,
  R <: Breed,
  EC <: PlaneV2SessionErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole,
  I <: PlaneV2ProtocolSessionIfce[M, SM, CM, CFM]
](using
  override protected val errorCode: EC,
  override protected val interface: I
) extends PlaneV2ProtocolSession[L, P, R, EC, M, SM, CM, CFM, I] {

  import dimensions.Dimension.Axes

  class SessionPlaneV2BehaviorImpl(
    override protected val start: session.Start[CFM],
    override protected val end: version.base.Role => Boolean,
    override protected val block: session.Block[L, M],
    override protected val apply: client.frontend.Apply[P, R],
    override protected val axes: Axes
  ) extends SessionPlaneV2Behavior[SessionPlaneV2BehaviorImpl]

  private object SessionPlaneV2Inflater
      extends SessionPlaneV2Inflater[SessionPlaneV2BehaviorImpl] {

    override def apply(em: Arguments): SessionPlaneV2BehaviorImpl =
      new SessionPlaneV2BehaviorImpl(em._1, em._2, em._3, em._4, em._5)

  }

  private object SessionPlaneV2BehaviorImpl
      extends ObjectSessionPlaneBehavior[SessionPlaneV2BehaviorImpl, SessionPlaneV2Inflater.type] {

    override protected val inflater = SessionPlaneV2Inflater

  }

}


import factory.session._

object PlaneV2ProtocolSessionImpl
    extends PlaneV2ProtocolSessionFactory[Role] {

  override def apply[L,
    P <: Plane,
    R <: Breed
  ](
    context: ActorContext[?],
    args: Any*
  ): ActorRef[Either[L, Role]] = context
    .spawnAnonymous {
      import interface.ObjectPlaneV2ProtocolSessionIfce
      import session.Implicits._

      new PlaneV2ProtocolSessionImpl[L,
        P,
        R,
        PlaneV2SessionErrorCode,
        Role,
        protocol.server.Protocol.Role,
        protocol.client.Protocol.Role,
        protocol.client.frontend.Protocol.Role,
        ObjectPlaneV2ProtocolSessionIfce
      ].SessionPlaneV2BehaviorImpl(args*)

    }

}
