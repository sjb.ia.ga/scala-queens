package queens
package version.v2
package plane
package impl

import akka.actor.typed.scaladsl.ActorContext

import dimensions.Dimension.Axes

import queens.base.breed.Breed

import version.base.Plane

import base.PlaneV2ProtocolClient
import base.interface.PlaneV2ProtocolClientIfce

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.client.Protocol.Role
import protocol.client._


class PlaneV2ProtocolClientImpl[
  P <: Plane,
  R <: Breed,
  EC <: PlaneV2ClientErrorCode,
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole,
  CDM >: Group <: ClientDeferRole,
  SM >: Group <: ServerRole,
  I <: PlaneV2ProtocolClientIfce[CDM, CFM]
](
  override protected val server: Actor[SM]
)(implicit
  override protected val errorCode: EC,
  override protected val interface: I
) extends PlaneV2ProtocolClient[P, R, EC, M, CFM, CDM, SM, I] {

  class ClientPlaneV2BehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val end: version.base.Role => Boolean,
    override protected val axes: Axes
  ) extends ClientPlaneV2Behavior[ClientPlaneV2BehaviorImpl]

  private object ClientPlaneV2Inflater
      extends ClientPlaneV2Inflater[ClientPlaneV2BehaviorImpl] {

    override def apply(em: Arguments): ClientPlaneV2BehaviorImpl =
      new ClientPlaneV2BehaviorImpl(em._1, em._2, em._3)

  }

  private object ClientPlaneV2BehaviorImpl
      extends ObjectRolePlaneBehavior[ClientPlaneV2BehaviorImpl, ClientPlaneV2Inflater.type] {

    override protected val inflater = ClientPlaneV2Inflater

  }

}


import factory.client._

import protocol.server.{ Protocol => Parameter }
import protocol.client.{ Protocol => Interface }

object PlaneV2ProtocolClientImpl extends PlaneV2ProtocolClientFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply[
    P <: Plane,
    R <: Breed
  ](
    context: ActorContext[?],
    server: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import interface.ObjectPlaneV2ProtocolClientIfce
      import client.Implicits._

      new PlaneV2ProtocolClientImpl[
        P,
        R,
        PlaneV2ClientErrorCode,
        Role,
        protocol.client.frontend.Protocol.Role,
        protocol.client.defer.Protocol.Role,
        Parameter.Role,
        ObjectPlaneV2ProtocolClientIfce
      ](
        server
      ).ClientPlaneV2BehaviorImpl(args*)
    }

}
