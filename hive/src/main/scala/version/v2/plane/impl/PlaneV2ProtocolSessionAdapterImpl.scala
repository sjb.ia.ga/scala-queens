package queens
package version.v2
package plane
package impl

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import base.PlaneV2ProtocolSessionAdapter

import protocol.base.Protocol.Group
import protocol.session.adapter.Protocol.Role
import protocol.session.adapter._


class PlaneV2ProtocolSessionAdapterImpl[
  EC <: PlaneV2SessionAdapterErrorCode,
  M >: Group <: Role
](
  override protected val session: ActorRef[Either[?, M]]
)(implicit
  override protected val errorCode: EC
) extends PlaneV2ProtocolSessionAdapter[EC, M] {

  class SessionAdapterPlaneV2BehaviorImpl(
    override protected val end: version.base.Role => Boolean
  ) extends SessionAdapterPlaneV2Behavior[SessionAdapterPlaneV2BehaviorImpl]

  private object SessionAdapterPlaneV2Inflater
      extends SessionAdapterPlaneV2Inflater[SessionAdapterPlaneV2BehaviorImpl] {

    override def apply(em: Arguments): SessionAdapterPlaneV2BehaviorImpl =
      new SessionAdapterPlaneV2BehaviorImpl(em._1)

  }

  private object SessionAdapterPlaneV2BehaviorImpl
      extends ObjectRolePlaneBehavior[SessionAdapterPlaneV2BehaviorImpl, SessionAdapterPlaneV2Inflater.type] {

    override protected val inflater = SessionAdapterPlaneV2Inflater

  }

}


import factory.session.adapter._

object PlaneV2ProtocolSessionAdapterImpl
    extends PlaneV2ProtocolSessionAdapterFactory[Role] {

  override def apply(
    context: ActorContext[?],
    session: ActorRef[Either[?, Role]],
    args: Any*
  ): Actor[Role] = context
    .spawnAnonymous {
      import plane.session.adapter.Implicits._

      new PlaneV2ProtocolSessionAdapterImpl[
        PlaneV2SessionAdapterErrorCode,
        Role,
      ](
        session
      ).SessionAdapterPlaneV2BehaviorImpl(args*)

    }

}
