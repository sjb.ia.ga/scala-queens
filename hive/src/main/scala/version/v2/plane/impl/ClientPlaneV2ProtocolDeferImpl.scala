package queens
package version.v2
package plane
package impl

import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed

import version.base.Plane

import base.ClientPlaneV2ProtocolDefer

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.client.defer.Protocol.Role
import protocol.client.defer._


class ClientPlaneV2ProtocolDeferImpl[
  P <: Plane,
  R <: Breed,
  EC <: PlaneV2ClientDeferErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  SDM >: Group <: ServerDeferRole,
  SBM >: Group <: ServerBackendRole,
  CFM >: Group <: ClientFrontendRole
](
  override protected val onBehalf: Actor[CFM]
)(implicit
  override protected val errorCode: EC
) extends ClientPlaneV2ProtocolDefer[P, R, EC, M, SM, SDM, SBM, CFM] {

  class ClientPlaneV2DeferBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val plane: P
  ) extends ClientPlaneV2DeferBehavior[ClientPlaneV2DeferBehaviorImpl]

  private object ClientPlaneV2DeferInflater
      extends ClientPlaneV2DeferInflater[ClientPlaneV2DeferBehaviorImpl] {

    override def apply(em: Arguments): ClientPlaneV2DeferBehaviorImpl =
      new ClientPlaneV2DeferBehaviorImpl(em._1, em._2)

  }

  private object ClientPlaneV2DeferBehaviorImpl
      extends ObjectRolePlaneBehavior[ClientPlaneV2DeferBehaviorImpl, ClientPlaneV2DeferInflater.type] {

    override protected val inflater = ClientPlaneV2DeferInflater

  }

}


import factory.client.defer._

import protocol.client.frontend.{ Protocol => Parameter }
import protocol.client.defer.{ Protocol => Interface }

object ClientPlaneV2ProtocolDeferImpl extends PlaneV2ProtocolClientDeferFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply[
    P <: Plane,
    R <: Breed
  ](
    context: ActorContext[?],
    onBehalf: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import client.defer.Implicits._

      new ClientPlaneV2ProtocolDeferImpl[
        P,
        R,
        PlaneV2ClientDeferErrorCode,
        Interface.Role,
        protocol.server.Protocol.Role,
        protocol.server.defer.Protocol.Role,
        protocol.server.backend.Protocol.Role,
        Parameter.Role
      ](
        onBehalf
      ).ClientPlaneV2DeferBehaviorImpl(args*)

    }

}
