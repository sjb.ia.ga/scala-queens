package queens
package version.v2
package plane
package impl

import akka.actor.typed.scaladsl.ActorContext

import version.base.Plane

import base.ServerPlaneV2ProtocolBackend

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.Role
import protocol.server.backend._


class ServerPlaneV2ProtocolBackendImpl[
  P <: Plane,
  EC <: PlaneV2ServerBackendErrorCode,
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
](
  override protected val graceTo: Actor[SDM],
  override protected val resultTo: Actor[CDM]
)(implicit
  override protected val errorCode: EC
) extends ServerPlaneV2ProtocolBackend[P, EC, M, SDM, CDM] {

  class ServerPlaneV2BackendBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val secret: java.util.UUID
  ) extends ServerPlaneV2BackendBehavior[ServerPlaneV2BackendBehaviorImpl]

  private object ServerPlaneV2BackendInflater
      extends ServerPlaneV2BackendInflater[ServerPlaneV2BackendBehaviorImpl] {

    override def apply(em: Arguments): ServerPlaneV2BackendBehaviorImpl =
      new ServerPlaneV2BackendBehaviorImpl(em._1, em._2)

  }

  private object ServerPlaneV2BackendBehaviorImpl
      extends ObjectRolePlaneBehavior[ServerPlaneV2BackendBehaviorImpl, ServerPlaneV2BackendInflater.type] {

    override protected val inflater = ServerPlaneV2BackendInflater

  }

}


import factory.server.backend._

import protocol.server.defer.{ Protocol => Parameter1 }
import protocol.client.defer.{ Protocol => Parameter2 }
import protocol.server.backend.{ Protocol => Interface }

object ServerPlaneV2ProtocolBackendImpl extends PlaneV2ProtocolServerBackendFactory[
  Role,
  Parameter1.Role,
  Parameter2.Role
] {

  override def apply[
    P <: Plane,
  ](
    context: ActorContext[?],
    graceTo: Parameter1.Actor,
    resultTo: Parameter2.Actor,
    args: Any*
  ): Protocol.Actor = context
    .spawnAnonymous {
      import server.backend.Implicits._

      new ServerPlaneV2ProtocolBackendImpl[
        P,
        PlaneV2ServerBackendErrorCode,
        Role,
        Parameter1.Role,
        Parameter2.Role
      ](
        graceTo,
        resultTo
      ).ServerPlaneV2BackendBehaviorImpl(args*)

    }

}
