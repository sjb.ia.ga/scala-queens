package queens
package version.v2
package plane
package impl

import akka.actor.typed.scaladsl.ActorContext

import dimensions.Dimension.Axes

import version.base.Plane

import base.ServerPlaneV2ProtocolDefer
import base.interface.ServerPlaneV2ProtocolDeferIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.server.defer.Protocol.Role
import protocol.server.defer._


class ServerPlaneV2ProtocolDeferImpl[
  P <: Plane,
  EC <: PlaneV2ServerDeferErrorCode,
  M >: Group <: Role,
  SBM >: Group <: ServerBackendRole,
  CDM >: Group <: ClientDeferRole,
  I <: ServerPlaneV2ProtocolDeferIfce[SBM, M, CDM]
](
  override protected val readyTo: Actor[CDM]
)(implicit
  override protected val errorCode: EC,
  override protected val interface: I
) extends ServerPlaneV2ProtocolDefer[P, EC, M, SBM, CDM, I] {

  class ServerPlaneV2DeferBehaviorImpl(
    override protected val axes: Axes,
    override protected val cryptoKey: CryptoKey,
    override protected val secret: java.util.UUID
  ) extends ServerPlaneV2DeferBehavior[ServerPlaneV2DeferBehaviorImpl]

  private object ServerPlaneV2DeferInflater
      extends ServerPlaneV2DeferInflater[ServerPlaneV2DeferBehaviorImpl] {

    override def apply(em: Arguments): ServerPlaneV2DeferBehaviorImpl =
      new ServerPlaneV2DeferBehaviorImpl(em._1, em._2, em._3)

  }

  private object ServerPlaneV2DeferBehaviorImpl
      extends ObjectRolePlaneBehavior[ServerPlaneV2DeferBehaviorImpl, ServerPlaneV2DeferInflater.type] {

    override protected val inflater = ServerPlaneV2DeferInflater

  }

}

import factory.server.defer._

import protocol.client.defer.{ Protocol => Parameter }
import protocol.server.defer.{ Protocol => Interface }

object ServerPlaneV2ProtocolDeferImpl extends PlaneV2ProtocolServerDeferFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply[
    P <: Plane,
  ](
    context: ActorContext[?],
    readyTo: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import interface.ObjectServerPlaneV2ProtocolDeferIfce
      import server.defer.Implicits._

      new ServerPlaneV2ProtocolDeferImpl[
        P,
        PlaneV2ServerDeferErrorCode,
        Interface.Role,
        protocol.server.backend.Protocol.Role,
        Parameter.Role,
        ObjectServerPlaneV2ProtocolDeferIfce
      ](
        readyTo
      ).ServerPlaneV2DeferBehaviorImpl(args*)

    }

}
