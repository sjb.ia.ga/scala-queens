package queens
package version.v2
package plane
package impl

import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed

import version.base.Plane

import client.frontend.Apply

import base.ClientPlaneV2ProtocolFrontend

import protocol.base.Protocol.Group
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.Role
import protocol.client.frontend._


class ClientPlaneV2ProtocolFrontendImpl[
  P <: Plane,
  R <: Breed,
  EC <: PlaneV2ClientFrontendErrorCode,
  M >: Group <: Role,
  CM >: Group <: ClientRole
](
  override protected val client: Actor[CM]
)(implicit
  override protected val errorCode: EC
) extends ClientPlaneV2ProtocolFrontend[P, R, EC, M, CM] {

  class ClientPlaneV2FrontendBehaviorImpl(
    override protected val end: version.base.Role => Boolean,
    _apply: Apply[P, R]
  ) extends ClientPlaneV2FrontendBehavior[ClientPlaneV2FrontendBehaviorImpl] {

    override protected val apply = _apply

  }

  private object ClientPlaneV2FrontendInflater
      extends ClientPlaneV2FrontendInflater[ClientPlaneV2FrontendBehaviorImpl] {

    override def apply(em: Arguments): ClientPlaneV2FrontendBehaviorImpl =
      new ClientPlaneV2FrontendBehaviorImpl(em._1, em._2)

  }

  private object ClientPlaneV2FrontendBehaviorImpl
      extends ObjectRolePlaneBehavior[ClientPlaneV2FrontendBehaviorImpl, ClientPlaneV2FrontendInflater.type] {

    override protected val inflater = ClientPlaneV2FrontendInflater

  }

}


import factory.client.frontend._

import protocol.client.frontend.{ Protocol => Interface }
import protocol.client.{ Protocol => Parameter }

object ClientPlaneV2ProtocolFrontendImpl extends PlaneV2ProtocolClientFrontendFactory[
  Role,
  Parameter.Role
] {

  override def apply[
    P <: Plane,
    R <: Breed
  ](
    context: ActorContext[?],
    client: Parameter.Actor,
    args: Any*
  ): Protocol.Actor = context
    .spawnAnonymous {
      import plane.client.frontend.Implicits._

      new ClientPlaneV2ProtocolFrontendImpl[
        P,
        R,
        PlaneV2ClientFrontendErrorCode,
        Role,
        Parameter.Role
      ](
        client
      ).ClientPlaneV2FrontendBehaviorImpl(args*)

    }

}
