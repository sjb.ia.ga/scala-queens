package queens
package version.v2
package plane
package factory.session

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed
import version.base.Plane

import protocol.base.Protocol.Group
import protocol.session.Protocol.Role


abstract class PlaneV2ProtocolSessionFactory[
  M >: Group <: Role
] {

  def apply[L,
    P <: Plane,
    R <: Breed,
  ](
    context: ActorContext[?],
    args: Any*
  ): ActorRef[Either[L, M]]

}
