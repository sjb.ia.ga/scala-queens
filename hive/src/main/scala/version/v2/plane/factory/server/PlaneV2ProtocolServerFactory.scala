package queens
package version.v2
package plane
package factory.server

import akka.actor.typed.scaladsl.ActorContext

import version.base.Plane

import protocol.base.Protocol.Group
import protocol.session.Protocol.{ Role => SessionRole }
import protocol.server.Protocol.Role


abstract trait PlaneV2ProtocolServerFactory[
  M >: Group <: Role,
  SM >: Group <: SessionRole
] {

  def apply[
    P <: Plane
  ](
    context: ActorContext[?],
    session: Actor[SM],
    args: Any*
  ): Actor[M]

}
