package queens
package version.v2
package plane
package factory.client.frontend

import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed
import version.base.Plane

import protocol.base.Protocol.Group
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.Role


abstract trait PlaneV2ProtocolClientFrontendFactory[
  M >: Group <: Role,
  CM >: Group <: ClientRole
] {

  def apply[
    P <: Plane,
    R <: Breed
  ](
    context: ActorContext[?],
    client: Actor[CM],
    args: Any*
  ): Actor[M]

}
