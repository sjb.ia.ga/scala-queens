package queens
package version.v2
package plane
package factory.server.backend

import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed
import version.base.Plane

import protocol.base.Protocol.Group
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.Role


abstract trait PlaneV2ProtocolServerBackendFactory[
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
] {

  def apply[
    P <: Plane
  ](
    context: ActorContext[?],
    graceTo: Actor[SDM],
    resultTo: Actor[CDM],
    args: Any*
  ): Actor[M]

}
