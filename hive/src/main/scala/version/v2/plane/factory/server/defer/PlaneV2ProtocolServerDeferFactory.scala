package queens
package version.v2
package plane
package factory.server.defer

import akka.actor.typed.scaladsl.ActorContext

import version.base.Plane

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.Role


abstract trait PlaneV2ProtocolServerDeferFactory[
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole
] {

  def apply[
    P <: Plane
  ](
    context: ActorContext[?],
    readyTo: Actor[CDM],
    args: Any*
  ): Actor[M]

}
