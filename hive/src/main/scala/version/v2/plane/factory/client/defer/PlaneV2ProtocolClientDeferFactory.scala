package queens
package version.v2
package plane
package factory.client.defer

import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed
import version.base.Plane

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.Role


abstract trait PlaneV2ProtocolClientDeferFactory[
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole
] {

  def apply[
    P <: Plane,
    R <: Breed
  ](
    context: ActorContext[?],
    onBehalf: Actor[CFM],
    args: Any*
  ): Actor[M]

}
