package queens
package version.v2
package plane
package factory.client

import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed
import version.base.Plane

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.Protocol.Role


abstract trait PlaneV2ProtocolClientFactory[
  M >: Group <: Role,
  SM >: Group <: ServerRole
] {

  def apply[
    P <: Plane,
    R <: Breed
  ](
    context: ActorContext[?],
    server: Actor[SM],
    args: Any*
  ): Actor[M]

}
