package queens
package version.v2
package plane
package interface

import akka.actor.typed.scaladsl.ActorContext

import version.base.Plane

import base.interface.PlaneV2ProtocolServerIfce

import protocol.client.defer.{ Protocol => Parameter }
import protocol.server.defer.{ Protocol => Interface }


/**
  * @see [[queens.version.v2.plane.server.Implicits.PlaneV2ProtocolServerIfceImplicit]]
  */
abstract trait ObjectPlaneV2ProtocolServerIfce extends PlaneV2ProtocolServerIfce[
  Interface.Role,
  Parameter.Role
] { interface =>

  override object PlaneV2
      extends ObjectPlaneV2 {

    override def apply[
      P <: Plane,
    ](
      ctx: ActorContext[?],
      readyTo: Parameter.Actor,
      args: Any*
    ): Interface.Actor = {
      import factory.server.defer._

      type ServerDeferFactory = PlaneV2ProtocolServerDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(classOf[ServerDeferFactory])
        [P](ctx, readyTo, args*)
    }

  }

}
