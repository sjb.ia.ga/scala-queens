package queens
package version.v2
package plane
package interface

import scala.util.{ Failure, Success }

import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed

import common.cipher.Crypto
import common.cipher.client.CipherClient

import version.base.Plane

import base.interface.PlaneV2ProtocolSessionIfce
import base.interface.PlaneV2SessionStart

import protocol.base.Protocol.Group
import protocol.server.{ Protocol => Server }
import protocol.client.{ Protocol => Client }
import protocol.client.frontend.{ Protocol => ClientFrontend }
import protocol.session.Protocol.Role


/**
  * @see [[queens.version.v2.plane.session.Implicits.PlaneV2ProtocolSessionIfceImplicit]]
  */
abstract trait ObjectPlaneV2ProtocolSessionIfce extends PlaneV2ProtocolSessionIfce[
  Role,
  Server.Role,
  Client.Role,
  ClientFrontend.Role
] { interface =>

  override object StartV2
      extends ObjectStartV2 {

    override def apply[L,
      P <: Plane,
      R <: Breed
    ](
      ctx: ActorContext[?],
      self: Actor[Role],
      args: Any*
    ): PlaneV2SessionStart[
      Server.Role,
      Client.Role,
      ClientFrontend.Role
    ] = {
      object Deseq extends base.PlaneV2DeseqSession[L,
        P,
        R,
        Role,
        ClientFrontend.Role
      ]

      val (_, end, _, apply, axes) = Deseq(args)

      val cryptoKey = CipherClient().getOrElse(throw Crypto.CipherException(""))

      val server = {
        import factory.server._

        import protocol.session.{ Protocol => Parameter }
        import protocol.server.{ Protocol => Interface }

        type ServerFactory = PlaneV2ProtocolServerFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(classOf[ServerFactory])
          [P](ctx, self, cryptoKey, end, axes)
      }

      val client = {
        import factory.client._

        import protocol.server.{ Protocol => Parameter }
        import protocol.client.{ Protocol => Interface }

        type ClientFactory = PlaneV2ProtocolClientFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(classOf[ClientFactory])
          [P, R](ctx, server, cryptoKey, end, axes)
      }

      val frontend = {
        import factory.client.frontend._

        import protocol.client.{ Protocol => Parameter }
        import protocol.client.frontend.{ Protocol => Interface }

        type ClientFrontendFactory = PlaneV2ProtocolClientFrontendFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(classOf[ClientFrontendFactory])
          [P, R](ctx, client, end, apply)
      }

      PlaneV2SessionStart(server, client, frontend)
    }

  }

}
