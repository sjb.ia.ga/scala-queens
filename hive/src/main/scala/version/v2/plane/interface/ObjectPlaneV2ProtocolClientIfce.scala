package queens
package version.v2
package plane
package interface

import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed
import version.base.Plane

import base.interface.PlaneV2ProtocolClientIfce

import protocol.client.frontend.{ Protocol => Parameter }
import protocol.client.defer.{ Protocol => Interface }


/**
  * @see [[queens.version.v2.plane.session.Implicits.PlaneV2ProtocolSessionIfceImplicit]]
  */
abstract trait ObjectPlaneV2ProtocolClientIfce extends PlaneV2ProtocolClientIfce[
  Interface.Role,
  Parameter.Role
] { interface =>

  override object BreedV2
      extends ObjectBreedV2 {

    override def apply[
      P <: Plane,
      R <: Breed
    ](
      ctx: ActorContext[?],
      onBehalf: Parameter.Actor,
      args: Any*
    ): Interface.Actor = {
      import factory.client.defer._

      type ClientDeferFactory = PlaneV2ProtocolClientDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(classOf[ClientDeferFactory])
        [P, R](ctx, onBehalf, args*)
    }

  }

}
