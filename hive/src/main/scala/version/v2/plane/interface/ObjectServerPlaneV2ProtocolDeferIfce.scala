package queens
package version.v2
package plane
package interface

import akka.actor.typed.scaladsl.ActorContext

import queens.base.breed.Breed

import version.base.Plane

import base.interface.ServerPlaneV2ProtocolDeferIfce

import protocol.server.defer.{ Protocol => Parameter1 }
import protocol.client.defer.{ Protocol => Parameter2 }
import protocol.server.backend.{ Protocol => Interface }


/**
  * @see [[queens.version.v2.plane.server.defer.Implicits.ServerPlaneV2ProtocolDeferIfceImplicit]]
  */
abstract trait ObjectServerPlaneV2ProtocolDeferIfce extends ServerPlaneV2ProtocolDeferIfce[
  Interface.Role,
  Parameter1.Role,
  Parameter2.Role
] { interface =>

  override object ResumeV2
      extends ObjectResumeV2 {

    override def apply[
      P <: Plane
    ](
      ctx: ActorContext[?],
      graceTo: Parameter1.Actor,
      resultTo: Parameter2.Actor,
      args: Any*
    ): Interface.Actor = {
      import factory.server.backend._

      type ServerBackendFactory = PlaneV2ProtocolServerBackendFactory[
        Interface.Role,
        Parameter1.Role,
        Parameter2.Role
      ]

      interface(classOf[ServerBackendFactory])
        [P](ctx, graceTo, resultTo, args*)
    }

  }

}
