package queens
package version.v2
package plane.protocol
package message.breed

import dimensions.three.breed.Hatch.Flavor

import version.base.Plane

import version.less.nest.Nest

import version.v1.mco.MonadCO


abstract trait BreedV2Protocol
    extends PlaneV2Protocol:

  import BreedV2Protocol._

  inline final def BreedV2[P <: Plane](plane: P, args: Any*): BreedV2 = plane match
    case _: Nest =>
      require(args.size <= 1)
      if args.nonEmpty
      then
        val flavor: Flavor.Value = args(0).asInstanceOf[Flavor.Value]
        BreedV2Nest(plane.asInstanceOf[Nest], flavor)
      else
        BreedV2Nest(plane.asInstanceOf[Nest])

    case _: MonadCO[?] =>
      require(args.isEmpty)
      BreedV2MonadCO(plane.asInstanceOf[MonadCO[?]])

    case _ =>
      ???


object BreedV2Protocol:

  sealed abstract trait BreedV2
      extends Protocol.Message

  final case class BreedV2Nest(
    nest: Nest,
    flavor: Flavor.Value = Flavor()
  ) extends BreedV2

  final case class BreedV2MonadCO(
    monadCO: MonadCO[?]
  ) extends BreedV2


abstract trait BreedV2ErrorCode
    extends PlaneV2ErrorCode
