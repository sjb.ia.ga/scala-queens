package queens
package version.v2
package plane.protocol


import base.BasePlaneV2Protocol

abstract trait PlaneV2Protocol
    extends BasePlaneV2Protocol


import base.BasePlaneV2ErrorCode

abstract trait PlaneV2ErrorCode
    extends BasePlaneV2ErrorCode
