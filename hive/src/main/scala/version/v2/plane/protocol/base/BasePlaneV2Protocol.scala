package queens
package version.v2
package plane.protocol
package base


import version.base.protocol.BaseProtocol

abstract trait BasePlaneV2Protocol
    extends BaseProtocol


import version.base.protocol.BaseErrorCode

abstract trait BasePlaneV2ErrorCode
    extends BaseErrorCode
