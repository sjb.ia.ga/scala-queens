package queens
package version.v2
package plane.protocol
package client


import version.less.plane.protocol.client.PlaneClientProtocol

abstract trait PlaneV2ClientProtocol
    extends PlaneV2Protocol
    with PlaneClientProtocol


import version.less.plane.protocol.client.PlaneClientErrorCode

abstract trait PlaneV2ClientErrorCode
    extends PlaneV2ErrorCode
    with PlaneClientErrorCode
