package queens
package version.v2
package plane.protocol
package server


import version.less.plane.protocol.server.PlaneServerProtocol

abstract trait PlaneV2ServerProtocol
    extends PlaneV2Protocol
    with PlaneServerProtocol


import version.less.plane.protocol.server.PlaneServerErrorCode

abstract trait PlaneV2ServerErrorCode
    extends PlaneV2ErrorCode
    with PlaneServerErrorCode
