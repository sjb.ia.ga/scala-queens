package queens
package version.v2
package plane.protocol
package communication


import message.breed.{ BreedV2Protocol => MsgBreedV2Protocol }

abstract trait PlaneV2Protocol_ClientDefer_ServerBackend
    extends MsgBreedV2Protocol


import message.breed.{ BreedV2ErrorCode => MsgBreedV2ErrorCode }

abstract trait PlaneV2ErrorCode_ClientDefer_ServerBackend
    extends MsgBreedV2ErrorCode


