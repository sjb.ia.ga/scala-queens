package queens
package version.v2
package plane.protocol
package communication.parameters


import message.salute.{ PlaneV2Protocol => MsgPlaneV2Protocol }

abstract trait PlaneV2Protocol_ServerDefer_ClientDefer
    extends MsgPlaneV2Protocol
