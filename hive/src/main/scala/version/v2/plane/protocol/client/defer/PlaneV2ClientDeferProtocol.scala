package queens
package version.v2
package plane.protocol
package client.defer


import defer.PlaneV2DeferProtocol
import version.less.plane.protocol.client.defer.PlaneClientDeferProtocol

abstract trait PlaneV2ClientDeferProtocol
    extends PlaneV2DeferProtocol
    with PlaneClientDeferProtocol
    with communication.PlaneV2Protocol_ClientDefer_ServerBackend


import defer.PlaneV2DeferErrorCode
import version.less.plane.protocol.client.defer.PlaneClientDeferErrorCode

abstract trait PlaneV2ClientDeferErrorCode
    extends PlaneV2DeferErrorCode
    with PlaneClientDeferErrorCode
    with communication.PlaneV2ErrorCode_ClientDefer_ServerBackend
