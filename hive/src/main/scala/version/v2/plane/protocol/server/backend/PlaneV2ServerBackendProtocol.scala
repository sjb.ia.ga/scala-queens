package queens
package version.v2
package plane.protocol
package server.backend


import version.less.plane.protocol.server.backend.PlaneServerBackendProtocol

abstract trait PlaneV2ServerBackendProtocol
    extends PlaneV2Protocol
    with PlaneServerBackendProtocol
    with communication.PlaneV2Protocol_ServerBackend_ClientDefer


import version.less.plane.protocol.server.backend.PlaneServerBackendErrorCode

abstract trait PlaneV2ServerBackendErrorCode
    extends PlaneV2ErrorCode
    with PlaneServerBackendErrorCode
    with version.less.nest.protocol.server.backend.HatcherErrorCode {

  val dupPlaneV2 = ProtocolDupVal()

}
