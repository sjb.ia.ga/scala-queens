package queens
package version.v2
package plane.protocol
package session


import version.less.plane.protocol.session.PlaneSessionProtocol

abstract trait PlaneV2SessionProtocol
    extends PlaneV2Protocol
    with PlaneSessionProtocol


import version.less.plane.protocol.session.PlaneSessionErrorCode

abstract trait PlaneV2SessionErrorCode
    extends PlaneV2ErrorCode
    with PlaneSessionErrorCode
