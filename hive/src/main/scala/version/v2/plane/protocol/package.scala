package queens
package version.v2
package plane.protocol

import version.v2.plane.{ Actor => PlaneV2Actor }


abstract trait PlaneV2Message
    extends session.Protocol.Role
    with client.Protocol.Role
    with client.defer.Protocol.Role
    with client.frontend.Protocol.Role
    with server.Protocol.Role
    with server.defer.Protocol.Role
    with server.backend.Protocol.Role


package base {

  abstract trait PlaneV2Group
      extends PlaneV2Message
      with message.salute.Protocol.Group
      with message.breed.Protocol.Group

  object Protocol {

    type Group = PlaneV2Group

  }

}


package client {

  import version.less.plane.protocol.client.Protocol.{ Role => ClientRole }

  abstract trait PlaneV2ClientRole
      extends ClientRole

  object Protocol {

    type Role = PlaneV2ClientRole
    private[plane] type Actor = PlaneV2Actor[Role]

  }

  package defer {

    import version.less.plane.protocol.client.defer.Protocol.{ Role => ClientDeferRole }

    abstract trait PlaneV2ClientDeferRole
        extends ClientDeferRole

    object Protocol {

      type Role = PlaneV2ClientDeferRole
      private[plane] type Actor = PlaneV2Actor[Role]

    }

  }

  package frontend {

    import version.less.plane.protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }

    abstract trait PlaneV2ClientFrontendRole
        extends ClientFrontendRole

    object Protocol {

      type Role = PlaneV2ClientFrontendRole
      private[v2] type Actor = PlaneV2Actor[Role]

    }

  }

}


package server {

  import version.less.plane.protocol.server.Protocol.{ Role => ServerRole }

  abstract trait PlaneV2ServerRole
      extends ServerRole

  object Protocol {

    type Role = PlaneV2ServerRole
    private[plane] type Actor = PlaneV2Actor[Role]

  }

  package defer {

    import version.less.plane.protocol.server.defer.Protocol.{ Role => ServerDeferRole }

    abstract trait PlaneV2ServerDeferRole
        extends ServerDeferRole

    object Protocol {

      type Role = PlaneV2ServerDeferRole
      private[plane] type Actor = PlaneV2Actor[Role]

    }

  }

  package backend {

    import version.less.plane.protocol.server.backend.Protocol.{ Role => ServerBackendRole }

    abstract trait PlaneV2ServerBackendRole
        extends ServerBackendRole

    object Protocol {

      type Role = PlaneV2ServerBackendRole
      private[plane] type Actor = PlaneV2Actor[Role]

    }

  }

}


package session {

  import version.less.plane.protocol.session.Protocol.{ Role => PlaneSessionRole }

  abstract trait PlaneV2SessionRole
      extends PlaneSessionRole

  object Protocol {

    import akka.actor.typed.ActorRef

    type Role = PlaneV2SessionRole
    private[plane] type Actor[L] = ActorRef[Either[L, Role]]

  }

  package adapter {

    object Protocol {

      type Role = session.Protocol.Role
      private[v2] type Actor = PlaneV2Actor[Role]

    }

  }

  package object adapter {

    type PlaneV2SessionAdapterProtocol = PlaneV2SessionProtocol
    type PlaneV2SessionAdapterErrorCode = PlaneV2SessionErrorCode

  }

}
