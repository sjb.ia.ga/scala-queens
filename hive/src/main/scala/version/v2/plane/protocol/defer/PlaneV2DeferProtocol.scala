package queens
package version.v2
package plane.protocol
package defer


import version.less.plane.protocol.defer.PlaneDeferProtocol

abstract trait PlaneV2DeferProtocol
    extends PlaneV2Protocol
    with PlaneDeferProtocol


import version.less.plane.protocol.defer.PlaneDeferErrorCode

abstract trait PlaneV2DeferErrorCode
    extends PlaneV2ErrorCode
    with PlaneDeferErrorCode

