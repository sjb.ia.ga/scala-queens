package queens
package version.v2
package plane.protocol
package communication


abstract trait PlaneV2Protocol_ServerBackend_ClientDefer
    extends parameters.PlaneV2Protocol_ServerDefer_ClientDefer
    with PlaneV2Protocol_ClientDefer_ServerBackend
