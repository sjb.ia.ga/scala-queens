package queens
package version.v2
package plane.protocol
package message.salute

import java.util.UUID


abstract trait PlaneV2Protocol
    extends plane.protocol.PlaneV2Protocol:

  import PlaneV2Protocol._

  inline final def PlaneV2(secret: UUID) = new PlaneV2(secret)


object PlaneV2Protocol:

  final case class PlaneV2(secret: UUID)
      extends Protocol.Message
