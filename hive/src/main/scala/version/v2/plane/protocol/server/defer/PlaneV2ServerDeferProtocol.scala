package queens
package version.v2
package plane.protocol
package server.defer


import defer.PlaneV2DeferProtocol
import version.less.plane.protocol.server.defer.PlaneServerDeferProtocol

abstract trait PlaneV2ServerDeferProtocol
    extends PlaneV2DeferProtocol
    with PlaneServerDeferProtocol
    with communication.parameters.PlaneV2Protocol_ServerDefer_ClientDefer


import defer.PlaneV2DeferErrorCode
import version.less.plane.protocol.server.defer.PlaneServerDeferErrorCode

abstract trait PlaneV2ServerDeferErrorCode
    extends PlaneV2DeferErrorCode
    with PlaneServerDeferErrorCode
