package queens
package version.v2
package plane.protocol
package client.frontend


import version.less.plane.protocol.client.frontend.PlaneClientFrontendProtocol

abstract trait PlaneV2ClientFrontendProtocol
    extends PlaneV2Protocol
    with PlaneClientFrontendProtocol


import version.less.plane.protocol.client.frontend.PlaneClientFrontendErrorCode

abstract trait PlaneV2ClientFrontendErrorCode
    extends PlaneV2ErrorCode
    with PlaneClientFrontendErrorCode
