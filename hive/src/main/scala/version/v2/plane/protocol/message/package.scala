package queens
package version.v2
package plane.protocol
package message


package salute {

  import server.defer.Protocol.{ Role => PlaneV2ServerDeferRole }

  import server.backend.Protocol.{ Role => PlaneV2ServerBackendRole }

  abstract trait PlaneV2Group
      extends PlaneV2ServerDeferRole
      with PlaneV2ServerBackendRole

  import plane.protocol.base.Protocol.{ Group => PlaneV2DotGroup }

  abstract trait PlaneV2Message
      extends PlaneV2DotGroup

  object Protocol {

    type Group = PlaneV2Group
    type Message = PlaneV2Message

  }

}

package breed {

  import client.defer.Protocol.{ Role => PlaneV2ClientDeferRole }

  import server.backend.Protocol.{ Role => PlaneV2ServerBackendRole }

  abstract trait BreedV2Group
      extends PlaneV2ClientDeferRole
      with PlaneV2ServerBackendRole

  import plane.protocol.base.Protocol.{ Group => PlaneV2Group }

  abstract trait BreedV2Message
      extends PlaneV2Group

  object Protocol {

    type Group = BreedV2Group
    type Message = BreedV2Message

  }

}
