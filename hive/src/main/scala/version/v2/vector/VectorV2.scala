package queens
package version.v2
package vector

import dimensions.Dimension.Projection
import dimensions.Dimension.{ Algorithm, Aspect, Model, Interface, Use }

import version.base.Vector
import version.v1.vector.VectorV1


case class VectorV2(
  _aspect: Aspect,
  _algorithm: Algorithm,
  _model: Model,
  _interface: Interface,
  _use: Use
) extends Vector(_aspect, _algorithm, _model, _interface, _use):

  def this(that: Vector) = this(
    that.aspect,
    that.algorithm,
    that.model,
    that.interface,
    that.use
  )

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[VectorV2]


object VectorV2:

  val axes = Vector.axes

  def apply(v1: VectorV1[?]): VectorV2 =
    new VectorV2(v1)

  def apply(it: Projection): VectorV2 =
    assert(it.size == 5)
    VectorV2(it(0), it(1), it(2), it(3), it(4))
