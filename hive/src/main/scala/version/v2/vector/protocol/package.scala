package queens
package version.v2
package vector.protocol


package session {

  package adapter {

    object Protocol {

      type Role = plane.protocol.session.Protocol.Role

    }

  }

  import plane.protocol.session.{ PlaneV2SessionProtocol, PlaneV2SessionErrorCode }

  package object adapter {

    type VectorV2SessionAdapterProtocol = PlaneV2SessionProtocol
    type VectorV2SessionAdapterErrorCode = PlaneV2SessionErrorCode

  }

}
