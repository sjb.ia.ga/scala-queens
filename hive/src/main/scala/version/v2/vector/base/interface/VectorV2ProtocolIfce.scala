package queens
package version.v2
package vector
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import conf.emito.Conf

import version.v1.mco.MonadCO

import vector.{ Apply, VectorV2, Wildcard }
import version.base.Multiplier

import version.less.plane.base.interface.PlaneProtocolIfce

import plane.protocol.base.Protocol.Group
import plane.protocol.session.Protocol.Role


abstract trait VectorV2ProtocolIfce[
  M >: Group <: Role
] extends PlaneProtocolIfce:

  protected abstract trait ObjectVectorV2SessionExpand
      extends ObjectSessionExpand { self =>

    def apply[
      P <: Conf
    ](using
      params: P
    )(context: ActorContext[?],
      end: version.base.Role => Boolean,
      stop: () => Unit,
      block: M => Unit,
      apply: Apply[P],
      wildcard: Wildcard,
      multiplier: Multiplier[VectorV2] = Multiplier.Once()
    )(using
      mcoPatch: VectorV2 => MonadCO[P]
    ): Int

  }

  override def SessionExpand: ObjectVectorV2SessionExpand
