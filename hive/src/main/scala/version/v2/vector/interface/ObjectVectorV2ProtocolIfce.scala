package queens
package version.v2
package vector
package interface

import scala.collection.mutable.{
  ArrayBuffer => Accumulator,
  HashMap => MutableMap
}

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import akka.util.ReentrantGuard

import queens.base.breed.Breed

import dimensions.three.breed.Hatch
import version.less.nest.{ Nest, Wildcard => NestWildcard }

import dimensions.fourth.fifth.breed.Spawn
import version.v1.mco.{ MonadCO, Wildcard => MCOWildcard }
import version.patch.v1.mdo.MonadDO
import version.patch.v1.mbo.MonadBO

import conf.emito.Conf

import version.v1.vector.VectorV1
import version.v1.vector.breed.Hive

import vector.{ Apply, VectorV2, Wildcard }
import version.base.Multiplier

import base.interface.VectorV2ProtocolIfce

import plane.protocol.base.Protocol.Group
import plane.protocol.session.Protocol.Role


/**
  * @see [[queens.version.v2.vector.Implicits.VectorV2ProtocolIfceImplicit]]
  */
abstract trait ObjectVectorV2ProtocolIfce[
  M >: Group <: Role
] extends VectorV2ProtocolIfce[M] { interface =>

  override object SessionExpand
      extends ObjectVectorV2SessionExpand:

    override def apply[
      P <: Conf
    ](using
      params: P
    )(context: ActorContext[?],
      end: version.base.Role => Boolean,
      stop: () => Unit,
      block: M => Unit,
      vector_v2: Apply[P],
      wildcard: Wildcard,
      multiplier: Multiplier[VectorV2]
    )(using
      mcoPatch: VectorV2 => MonadCO[P] // MonadCO or MonadBO or MonadDO
    ): Int =
      import version.v1.vector.interface.ObjectVectorV1ProtocolIfce._

      val nest_multiplier = multiplier { (nest: Nest) => new VectorV2(nest.toVector) }

      val vs: Seq[(Nest, MonadCO[P])] = for
        v2 <- wildcard(multiplier)
        n = Nest(v2.aspect, v2.algorithm, v2.model)
        m = mcoPatch(v2)
        acc = Accumulator[(PlaneV1, Breed)]()
        lock = new ReentrantGuard()
      yield (
        new NestV1(acc, lock, n),
        m match
          case mb: MonadBO[P] => new MonadBOV1(acc, lock, mb)
          case md: MonadDO[P] => new MonadDOV1(acc, lock, md)
          case mc => new MonadCOV1(acc, lock, mc)
      )

      val nest_wildcard = new NestWildcardV1(vs.map(_._1))
      val mco_wildcard = new MCOWildcardV1(vs.map(_._2))

      if vs.size == 0 then
        return 0

      import plane.factory.session.adapter._
      import plane.protocol.session.adapter.{ Protocol => SessionAdapter }
      type SessionAdapterFactory = PlaneV2ProtocolSessionAdapterFactory[SessionAdapter.Role]

      var sessionAdapter: Twin[SessionAdapter.Actor] = (null, null)

      def msg_v2(message: Either[Unit, M], sessionAdapter: SessionAdapter.Actor): Unit =
        message match
          case Left(_) =>
            sessionAdapter ! Start(sessionAdapter)
          case Right(msg) =>
            block(msg)

      val end_v2 = endV1(end, stop)

      import plane.protocol.client.frontend.{ Protocol => ClientFrontend }

      import plane.factory.session._
      import plane.protocol.session.{ Protocol => Session }
      type SessionFactory = PlaneV2ProtocolSessionFactory[Session.Role]

      import common.pipeline.Context
      import dimensions.fifth.QueensUseSolution

      val apply_v1 =  { (nest: Nest, hatch: Hatch[params.Q], monadCO: MonadCO[P], spawn: Spawn[P]) =>
                        vector_v2(VectorV2(VectorV1(nest, monadCO)),
                                  { case next: Option[Context => QueensUseSolution] =>
                                      Hive(hatch, spawn)(next) })
                      }

      val session: Twin[ActorRef[Either[?, SessionAdapter.Role]]] = (

        interface(classOf[SessionFactory])[Unit, Nest, Hatch[params.Q]](context,
          { (frontend: ClientFrontend.Actor) =>
            frontend ! Expand(nest_wildcard)
          },
          end_v2,
          msg_v2(_, sessionAdapter._1),
          { (nest: Nest, hatch: Hatch[params.Q]) =>
            import dimensions.three.breed.given
            nestApply(apply_v1)(nest, hatch)
          },
          Nest.axes
        ).asInstanceOf[ActorRef[Either[?, SessionAdapter.Role]]],

        interface(classOf[SessionFactory])[Unit, MonadCO[P], Spawn[P]](context,
          { (frontend: ClientFrontend.Actor) =>
            frontend ! Expand(mco_wildcard)
          },
          end_v2,
          msg_v2(_, sessionAdapter._2),
          { (mco: MonadCO[P], spawn: Spawn[P]) =>
            import dimensions.fourth.fifth.breed.given
            mcoApply(apply_v1)(mco, spawn)
          },
          MonadCO.axes
        ).asInstanceOf[ActorRef[Either[?, SessionAdapter.Role]]]

      )

      sessionAdapter = (
        interface(classOf[SessionAdapterFactory])(context, session._1, end_v2),
        interface(classOf[SessionAdapterFactory])(context, session._2, end_v2)
      )

      session._1 ! Left(())
      session._2 ! Left(())

      vs.size

}
