package queens
package version.v2
package vector

import dimensions.Dimension.{ Aspect, Algorithm, Model, Interface, Use }


class Wildcard protected (
  override protected[version] val filter: Wildcard.Filter
) extends version.base.Wildcard[VectorV2]:

  override val axes = VectorV2.axes

  def this() = this(Wildcard.star)

  override protected def apply(factor: VectorV2 => Int): List[VectorV2] =
    super.apply(factor, VectorV2.apply)

  override def equals(any: Any): Boolean = any match
    case that: Wildcard => super.equals(any)
    case _ => false

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[Wildcard]


object Wildcard:

  type Filter = version.base.Wildcard.Filter

  private val star: Filter = { (_, _) => true }

  def * = new Wildcard()

  def apply(wildcard: version.v1.vector.Wildcard[?]): Wildcard =
    apply(wildcard.filter)

  def apply(filter: Filter, wildcard: Wildcard = *): Wildcard =
    new Wildcard({ (di, it) =>
      filter(di, it) && wildcard.filter(di, it)
    })

  def apply(
    aspect: Aspect,
    algorithm: Algorithm,
    model: Model,
    interface: Interface,
    use: Use
  ): Filter = (_, _) match
    case (Aspect, `aspect`) | (Algorithm, `algorithm`) | (Model, `model`) | (Interface, `interface`) | (Use, `use`) => true
    case (_, _) => false
