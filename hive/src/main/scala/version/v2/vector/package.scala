package queens
package version.v2
package vector


package console {

  import version.v1.mco.MonadCO

  object Implicits {

    import conf.mco.Conf

    given [P <: Conf]: (P ?=> VectorV2 => MonadCO[P]) = { v2 => MonadCO(v2.model) }

  }

}


package buffered {

  import version.v1.mco.MonadCO

  object Implicits {

    import dimensions.Dimension.Use.bufferedOutput

    import version.patch.v1.mbo.MonadBO

    import conf.mbo.Conf

    given [P <: Conf]: (P ?=> VectorV2 => MonadCO[P]) = { v2 => new MonadBO(v2.model, bufferedOutput) }

  }

}


package database {

  import version.v1.mco.MonadCO

  package slick {

    package h2 {

      object Implicits {

        import dimensions.Dimension.Use.H2SlickDatabaseOutput
        import version.patch.v1.mdo.MonadDO

        import conf.mdo.slick.Conf

        given [P <: Conf]: (P ?=> VectorV2 => MonadCO[P]) = { v2 => new MonadDO(v2.model, H2SlickDatabaseOutput) }
    }

      package plainsql {

        object Implicits {

          import dimensions.Dimension.Use.H2PlainSQLSlickDatabaseOutput
          import version.patch.v1.mdo.MonadDO

          import conf.mdo.slick.Conf

          given [P <: Conf]: (P ?=> VectorV2 => MonadCO[P]) = { v2 => new MonadDO(v2.model, H2PlainSQLSlickDatabaseOutput) }
        }

      }

    }

    package sqlite {

      object Implicits {

        import dimensions.Dimension.Use.SQLiteSlickDatabaseOutput
        import version.patch.v1.mdo.MonadDO

        import conf.mdo.slick.Conf

        given [P <: Conf]: (P ?=> VectorV2 => MonadCO[P]) = { v2 => new MonadDO(v2.model, SQLiteSlickDatabaseOutput) }

      }

      package plainsql {

        object Implicits {

          import dimensions.Dimension.Use.SQLitePlainSQLSlickDatabaseOutput
          import version.patch.v1.mdo.MonadDO

          import conf.mdo.slick.Conf

          given [P <: Conf]: (P ?=> VectorV2 => MonadCO[P]) = { v2 => new MonadDO(v2.model, SQLitePlainSQLSlickDatabaseOutput) }

        }

      }

    }

  }

  package scalikejdbc {

    package h2 {

      object Implicits {

        import dimensions.Dimension.Use.H2ScalikeDatabaseOutput
        import version.patch.v1.mdo.MonadDO

        import conf.mdo.scalikejdbc.Conf

        given [P <: Conf]: (P ?=> VectorV2 => MonadCO[P]) = { v2 => new MonadDO(v2.model, H2ScalikeDatabaseOutput) }
    }

      package plainsql {

        object Implicits {

          import dimensions.Dimension.Use.H2PlainSQLScalikeDatabaseOutput
          import version.patch.v1.mdo.MonadDO

          import conf.mdo.scalikejdbc.Conf

          given [P <: Conf]: (P ?=> VectorV2 => MonadCO[P]) = { v2 => new MonadDO(v2.model, H2PlainSQLScalikeDatabaseOutput) }
        }

      }

    }

    package sqlite {

      object Implicits {

        import dimensions.Dimension.Use.SQLiteScalikeDatabaseOutput
        import version.patch.v1.mdo.MonadDO

        import conf.mdo.scalikejdbc.Conf

        given [P <: Conf]: (P ?=> VectorV2 => MonadCO[P]) = { v2 => new MonadDO(v2.model, SQLiteScalikeDatabaseOutput) }

      }

      package plainsql {

        object Implicits {

          import dimensions.Dimension.Use.SQLitePlainSQLScalikeDatabaseOutput
          import version.patch.v1.mdo.MonadDO

          import conf.mdo.scalikejdbc.Conf

          given [P <: Conf]: (P ?=> VectorV2 => MonadCO[P]) = { v2 => new MonadDO(v2.model, SQLitePlainSQLScalikeDatabaseOutput) }

        }

      }

    }

  }

}


object Implicits {

  import interface.ObjectVectorV2ProtocolIfce
  import plane.protocol.PlaneV2Message

  implicit object VectorV2ProtocolIfceImplicit
      extends ObjectVectorV2ProtocolIfce[PlaneV2Message] { interface =>

    import plane.factory.session._
    import plane.protocol.session.{ Protocol => Session }

    type SessionFactory = PlaneV2ProtocolSessionFactory[
      Session.Role
    ]

    interface(
      classOf[SessionFactory],
      plane.impl.PlaneV2ProtocolSessionImpl
    )

    import plane.factory.session.adapter._
    import plane.protocol.session.adapter.{ Protocol => SessionAdapter }

    type SessionAdapterFactory = PlaneV2ProtocolSessionAdapterFactory[
      SessionAdapter.Role
    ]

    interface(
      classOf[SessionAdapterFactory],
      plane.impl.PlaneV2ProtocolSessionAdapterImpl
    )

  }

}
