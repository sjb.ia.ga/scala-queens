package queens
package version.v1
package vector


package object session {

  import protocol.base.Protocol.Group
  import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
  import protocol.session.Protocol.Role

  type Start[CFM >: Group <: ClientFrontendRole] = Actor[CFM] => Unit
  type Block[L, M >: Group <: Role] = Either[L, M] => Unit

}


package client {

  package object frontend {

    import version.base.Plane
    import queens.base.breed.Breed

    type Apply = (Plane, Breed) => Unit

  }

}


object Implicits {

  import interface.ObjectVectorV1ProtocolIfce

  implicit object VectorV1ProtocolIfceImplicit
      extends ObjectVectorV1ProtocolIfce[protocol.VectorV1Message] { interface =>

    import factory.session._
    import protocol.session.{ Protocol => Session }

    type SessionFactory = VectorV1ProtocolSessionFactory[
      Session.Role
    ]

    interface(
      classOf[SessionFactory],
      impl.VectorV1ProtocolSessionImpl
    )

    import factory.session.adapter._
    import protocol.session.adapter.{ Protocol => SessionAdapter }

    type SessionAdapterFactory = VectorV1ProtocolSessionAdapterFactory[
      SessionAdapter.Role
    ]

    interface(
      classOf[SessionAdapterFactory],
      impl.VectorV1ProtocolSessionAdapterImpl
    )

  }

}


package session {

  object Implicits {

    implicit object VectorV1SessionErrorCodeImplicit
        extends protocol.session.VectorV1SessionErrorCode

    import interface.ObjectVectorV1ProtocolSessionIfce

    implicit object VectorV1ProtocolSessionIfceImplicit
        extends ObjectVectorV1ProtocolSessionIfce { interface =>

      {
        import factory.server._

        import protocol.session.{ Protocol => Parameter }
        import protocol.server.{ Protocol => Interface }

        type ServerFactory = VectorV1ProtocolServerFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(
          classOf[ServerFactory],
          impl.VectorV1ProtocolServerImpl
        )
      }

      {
        import factory.client._

        import protocol.server.{ Protocol => Parameter }
        import protocol.client.{ Protocol => Interface }

        type ClientFactory = VectorV1ProtocolClientFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(
          classOf[ClientFactory],
          impl.VectorV1ProtocolClientImpl
        )
      }

      {
        import factory.client.frontend._

        import protocol.client.{ Protocol => Parameter }
        import protocol.client.frontend.{ Protocol => Interface }

        type ClientFrontendFactory = VectorV1ProtocolClientFrontendFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(
          classOf[ClientFrontendFactory],
          impl.ClientVectorV1ProtocolFrontendImpl
        )
      }

    }

  }

  package adapter {

    object Implicits {

      implicit object VectorV1SessionAdapterErrorCodeImplicit
          extends protocol.session.adapter.VectorV1SessionAdapterErrorCode

    }

  }

}


package server {

  object Implicits {

    implicit object VectorV1ServerErrorCodeImplicit
        extends protocol.server.VectorV1ServerErrorCode

    import interface.ObjectVectorV1ProtocolServerIfce

    implicit object VectorV1ProtocolServerIfceImplicit
        extends ObjectVectorV1ProtocolServerIfce { interface =>

      import factory.server.defer._

      import protocol.client.defer.{ Protocol => Parameter }
      import protocol.server.defer.{ Protocol => Interface }

      type ServerDeferFactory = VectorV1ProtocolServerDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(
        classOf[ServerDeferFactory],
        impl.ServerVectorV1ProtocolDeferImpl
      )

    }

  }

  package defer {

    object Implicits {

      implicit object VectorV1ServerDeferErrorCodeImplicit
          extends protocol.server.defer.VectorV1ServerDeferErrorCode

      import interface.ObjectServerVectorV1ProtocolDeferIfce

      implicit object ServerVectorV1ProtocolDeferIfceImplicit
          extends ObjectServerVectorV1ProtocolDeferIfce { interface =>

        import factory.server.backend._

        import protocol.server.defer.{ Protocol => Parameter1 }
        import protocol.client.defer.{ Protocol => Parameter2 }
        import protocol.server.backend.{ Protocol => Interface }

        type ServerBackendFactory = VectorV1ProtocolServerBackendFactory[
          Interface.Role,
          Parameter1.Role,
          Parameter2.Role
        ]

        interface(
          classOf[ServerBackendFactory],
          impl.ServerVectorV1ProtocolBackendImpl
        )

      }

    }

  }

  package backend {

    object Implicits {

      implicit object VectorV1ServerBackendErrorCodeImplicit
          extends protocol.server.backend.VectorV1ServerBackendErrorCode

    }

  }

}


package client {

  object Implicits {

    implicit object VectorV1ClientErrorCodeImplicit
        extends protocol.client.VectorV1ClientErrorCode

    import interface.ObjectVectorV1ProtocolClientIfce

    implicit object VectorV1ProtocolClientIfceImplicit
        extends ObjectVectorV1ProtocolClientIfce { interface =>

      import factory.client.defer._

      import protocol.client.frontend.{ Protocol => Parameter }
      import protocol.client.defer.{ Protocol => Interface }

      type ClientDeferFactory = VectorV1ProtocolClientDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(
        classOf[ClientDeferFactory],
        impl.ClientVectorV1ProtocolDeferImpl
      )

    }

  }

  package defer {

    object Implicits {

      implicit object VectorV1ClientDeferErrorCodeImplicit
          extends protocol.client.defer.VectorV1ClientDeferErrorCode

    }

  }

  package frontend {

    object Implicits {

      implicit object VectorV1ClientFrontendErrorCodeImplicit
          extends protocol.client.frontend.VectorV1ClientFrontendErrorCode

    }

  }

}
