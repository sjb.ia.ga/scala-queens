package queens
package version.v1
package vector.protocol
package client.defer


import defer.VectorV1DeferProtocol
import version.less.nest.protocol.client.defer.NestClientDeferProtocol
import mco.protocol.client.defer.MCOClientDeferProtocol

abstract trait VectorV1ClientDeferProtocol
    extends VectorV1DeferProtocol
    with NestClientDeferProtocol
    with MCOClientDeferProtocol


import defer.VectorV1DeferErrorCode
import version.less.nest.protocol.client.defer.NestClientDeferErrorCode
import mco.protocol.client.defer.MCOClientDeferErrorCode

abstract trait VectorV1ClientDeferErrorCode
    extends VectorV1DeferErrorCode
    with NestClientDeferErrorCode
    with MCOClientDeferErrorCode
