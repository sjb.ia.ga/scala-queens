package queens
package version.v1
package vector.protocol

import vector.{ Actor => VectorV1Actor }


abstract trait VectorV1Message
    extends session.Protocol.Role
    with client.Protocol.Role
    with client.defer.Protocol.Role
    with client.frontend.Protocol.Role
    with server.Protocol.Role
    with server.defer.Protocol.Role
    with server.backend.Protocol.Role

package base {

  abstract trait VectorV1Group
      extends VectorV1Message

  object Protocol {

    type Group = VectorV1Group

  }

}


package client {

  import version.less.nest.protocol.client.Protocol.{ Role => NestClientRole }
  import mco.protocol.client.Protocol.{ Role => MCOClientRole }

  abstract trait VectorV1ClientRole
      extends NestClientRole
      with MCOClientRole

  object Protocol {

    type Role = VectorV1ClientRole
    private[vector] type Actor = VectorV1Actor[Role]

  }

  package defer {

    import version.less.nest.protocol.client.defer.Protocol.{ Role => NestClientDeferRole }
    import mco.protocol.client.defer.Protocol.{ Role => MCOClientDeferRole }

    abstract trait VectorV1ClientDeferRole
        extends NestClientDeferRole
        with MCOClientDeferRole

    object Protocol {

      type Role = VectorV1ClientDeferRole
      private[vector] type Actor = VectorV1Actor[Role]

    }

  }

  package frontend {

    import version.less.nest.protocol.client.frontend.Protocol.{ Role => NestClientFrontendRole }
    import mco.protocol.client.frontend.Protocol.{ Role => MCOClientFrontendRole }

    abstract trait VectorV1ClientFrontendRole
        extends NestClientFrontendRole
        with MCOClientFrontendRole

    object Protocol {

      type Role = VectorV1ClientFrontendRole
      private[vector] type Actor = VectorV1Actor[Role]

    }

  }

}


package server {

  import version.less.nest.protocol.server.Protocol.{ Role => NestServerRole }
  import mco.protocol.server.Protocol.{ Role => MCOServerRole }

  abstract trait VectorV1ServerRole
      extends NestServerRole
      with MCOServerRole

  object Protocol {

    type Role = VectorV1ServerRole
    private[vector] type Actor = VectorV1Actor[Role]

  }

  package defer {

    import version.less.nest.protocol.server.defer.Protocol.{ Role => NestServerDeferRole }
    import mco.protocol.server.defer.Protocol.{ Role => MCOServerDeferRole }

    abstract trait VectorV1ServerDeferRole
        extends NestServerDeferRole
        with MCOServerDeferRole

    object Protocol {

      type Role = VectorV1ServerDeferRole
      private[vector] type Actor = VectorV1Actor[Role]

    }

  }

  package backend {

    import version.less.nest.protocol.server.backend.Protocol.{ Role => NestServerBackendRole }
    import mco.protocol.server.backend.Protocol.{ Role => MCOServerBackendRole }

    abstract trait VectorV1ServerBackendRole
        extends NestServerBackendRole
        with MCOServerBackendRole

    object Protocol {

      type Role = VectorV1ServerBackendRole
      private[vector] type Actor = VectorV1Actor[Role]

    }

  }

}


package session {

  import version.less.nest.protocol.session.Protocol.{ Role => NestSessionRole }

  abstract trait VectorV1SessionRole
      extends NestSessionRole

  object Protocol {

    import akka.actor.typed.ActorRef

    type Role = VectorV1SessionRole
    private[vector] type Actor[L] = ActorRef[Either[L, Role]]

  }

  package adapter {

    object Protocol {

      type Role = session.Protocol.Role
      private[vector] type Actor = VectorV1Actor[Role]

    }

  }

  package object adapter {

    type VectorV1SessionAdapterProtocol= VectorV1SessionProtocol
    type VectorV1SessionAdapterErrorCode = VectorV1SessionErrorCode

  }

}
