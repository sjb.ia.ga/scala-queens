package queens
package version.v1
package vector.protocol
package server


import version.less.nest.protocol.server.NestServerProtocol
import mco.protocol.server.MCOServerProtocol

abstract trait VectorV1ServerProtocol
    extends VectorV1Protocol
    with NestServerProtocol
    with MCOServerProtocol


import version.less.nest.protocol.server.NestServerErrorCode
import mco.protocol.server.MCOServerErrorCode

abstract trait VectorV1ServerErrorCode
    extends VectorV1ErrorCode
    with NestServerErrorCode
    with MCOServerErrorCode
