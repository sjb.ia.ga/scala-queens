package queens
package version.v1
package vector.protocol
package client.frontend


import version.less.nest.protocol.client.frontend.NestClientFrontendProtocol
import mco.protocol.client.frontend.MCOClientFrontendProtocol

abstract trait VectorV1ClientFrontendProtocol
    extends VectorV1Protocol
    with NestClientFrontendProtocol
    with MCOClientFrontendProtocol


import version.less.nest.protocol.client.frontend.NestClientFrontendErrorCode
import mco.protocol.client.frontend.MCOClientFrontendErrorCode

abstract trait VectorV1ClientFrontendErrorCode
    extends VectorV1ErrorCode
    with NestClientFrontendErrorCode
    with MCOClientFrontendErrorCode
