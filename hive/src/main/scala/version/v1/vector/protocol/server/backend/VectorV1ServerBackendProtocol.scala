package queens
package version.v1
package vector.protocol
package server.backend


import version.less.nest.protocol.server.backend.NestServerBackendProtocol
import mco.protocol.server.backend.MCOServerBackendProtocol

abstract trait VectorV1ServerBackendProtocol
    extends VectorV1Protocol
    with NestServerBackendProtocol
    with MCOServerBackendProtocol


import version.less.nest.protocol.server.backend.NestServerBackendErrorCode
import mco.protocol.server.backend.MCOServerBackendErrorCode

abstract trait VectorV1ServerBackendErrorCode
    extends VectorV1ErrorCode
    with NestServerBackendErrorCode
    with MCOServerBackendErrorCode
