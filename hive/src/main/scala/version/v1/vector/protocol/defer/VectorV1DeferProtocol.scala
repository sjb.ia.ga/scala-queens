package queens
package version.v1
package vector.protocol
package defer


import version.less.nest.protocol.defer.NestDeferProtocol
import mco.protocol.defer.MCODeferProtocol

abstract trait VectorV1DeferProtocol
    extends VectorV1Protocol
    with NestDeferProtocol
    with MCODeferProtocol


import version.less.nest.protocol.defer.NestDeferErrorCode
import mco.protocol.defer.MCODeferErrorCode

abstract trait VectorV1DeferErrorCode
    extends VectorV1ErrorCode
    with NestDeferErrorCode
    with MCODeferErrorCode
