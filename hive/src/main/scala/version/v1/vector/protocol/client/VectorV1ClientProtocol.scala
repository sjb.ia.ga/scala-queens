package queens
package version.v1
package vector.protocol
package client


import version.less.nest.protocol.client.NestClientProtocol
import mco.protocol.client.MCOClientProtocol

abstract trait VectorV1ClientProtocol
    extends VectorV1Protocol
    with NestClientProtocol
    with MCOClientProtocol


import version.less.nest.protocol.client.NestClientErrorCode
import mco.protocol.client.MCOClientErrorCode

abstract trait VectorV1ClientErrorCode
    extends VectorV1ErrorCode
    with NestClientErrorCode
    with MCOClientErrorCode
