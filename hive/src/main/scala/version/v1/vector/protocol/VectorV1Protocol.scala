package queens
package version.v1
package vector.protocol


import base.BaseVectorV1Protocol

abstract trait VectorV1Protocol
    extends BaseVectorV1Protocol


import base.BaseVectorV1ErrorCode

abstract trait VectorV1ErrorCode
    extends BaseVectorV1ErrorCode
