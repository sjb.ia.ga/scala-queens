package queens
package version.v1
package vector.protocol
package server.defer


import defer.VectorV1DeferProtocol
import version.less.nest.protocol.server.defer.NestServerDeferProtocol
import mco.protocol.server.defer.MCOServerDeferProtocol

abstract trait VectorV1ServerDeferProtocol
    extends VectorV1DeferProtocol
    with NestServerDeferProtocol
    with MCOServerDeferProtocol


import defer.VectorV1DeferErrorCode
import version.less.nest.protocol.server.defer.NestServerDeferErrorCode
import mco.protocol.server.defer.MCOServerDeferErrorCode

abstract trait VectorV1ServerDeferErrorCode
    extends VectorV1DeferErrorCode
    with NestServerDeferErrorCode
    with MCOServerDeferErrorCode
