package queens
package version.v1
package vector.protocol
package session


import version.less.plane.protocol.session.PlaneSessionProtocol

abstract trait VectorV1SessionProtocol
    extends VectorV1Protocol
    with PlaneSessionProtocol


import version.less.plane.protocol.session.PlaneSessionErrorCode

abstract trait VectorV1SessionErrorCode
    extends VectorV1ErrorCode
    with PlaneSessionErrorCode
