package queens
package version.v1
package vector.protocol
package base


import version.base.protocol.BaseProtocol
import version.less.nest.protocol.server.NestServerProtocol
import mco.protocol.server.MCOServerProtocol

abstract trait BaseVectorV1Protocol
    extends BaseProtocol
    with NestServerProtocol
    with MCOServerProtocol


import version.base.protocol.BaseErrorCode
import version.less.nest.protocol.server.NestServerErrorCode
import mco.protocol.server.MCOServerErrorCode

abstract trait BaseVectorV1ErrorCode
    extends BaseErrorCode
    with NestServerErrorCode
    with MCOServerErrorCode
