package queens
package version.v1
package vector

import dimensions.Dimension.Projection
import dimensions.Dimension.Use.Output
import dimensions.Dimension.Use.Output.{ Buffered, Console, Database }
import dimensions.Dimension.Interface.monadProgram

import conf.emito.Conf

import version.base.Vector

import version.less.nest.Nest
import version.v1.mco.MonadCO
import version.patch.v1.mbo.MonadBO
import version.patch.v1.mdo.MonadDO


case class VectorV1[
  P <: Conf
](
  nest: Nest,
  monadCO: MonadCO[P]
)(using
  protected val * : P
) extends Vector(nest.aspect, nest.algorithm, nest.model, monadCO.interface, monadCO.use):

  require(nest.model eq monadCO.model)

  def this(nest: Nest, that: Vector)(using params: P) =
    this(nest, MonadCO.apply[P](that.model))

  def this(that: Vector)(using params: P) =
    this(Nest(that.aspect, that.algorithm, that.model), that)

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[VectorV1[P]]


object VectorV1:

  val axes = Vector.axes

  def apply[
    P <: Conf
  ](
    it: Projection
  )(using
    params: P
  ): VectorV1[P] =
    assert(it.size == 5)
    require(it(3) == monadProgram)
    it(4) match
      case Output(Buffered) =>
        VectorV1(Nest(it.take(3)), MonadBO[P](it.drop(2)))

      case Output(Console) =>
        VectorV1(Nest(it.take(3)), MonadCO[P](it.drop(2)))

      case Output(Database(_)) =>
        VectorV1(Nest(it.take(3)), MonadDO[P](it.drop(2)))

      case _ => ???
