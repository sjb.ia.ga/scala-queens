package queens
package version.v1
package vector
package interface

import scala.util.{ Failure, Success }

import akka.actor.typed.scaladsl.ActorContext

import common.cipher.Crypto
import common.cipher.client.CipherClient

import base.interface.VectorV1ProtocolSessionIfce
import base.interface.VectorV1SessionStart

import protocol.server.{ Protocol => Server }
import protocol.client.{ Protocol => Client }
import protocol.client.frontend.{ Protocol => ClientFrontend }
import protocol.session.Protocol.Role


/**
  * @see [[queens.version.v1.vector.session.Implicits.VectorV1ProtocolSessionIfceImplicit]]
  */
abstract trait ObjectVectorV1ProtocolSessionIfce extends VectorV1ProtocolSessionIfce[
  Role,
  Server.Role,
  Client.Role,
  ClientFrontend.Role
] { interface =>

  override object StartVectorV1
      extends ObjectStartVectorV1 {

    override def apply[L](
      ctx: ActorContext[?],
      self: Actor[Role],
      args: Any*
    ): VectorV1SessionStart[
      Server.Role,
      Client.Role,
      ClientFrontend.Role
    ] = {
      object Deseq extends base.VectorV1DeseqSession[L,
        Role,
        ClientFrontend.Role
      ]

      val (_, end, _, apply) = Deseq(args)

      val cryptoKey = CipherClient().getOrElse(throw Crypto.CipherException(""))

      import factory.client.frontend._

      val server = {
        import factory.server._

        import protocol.session.{ Protocol => Parameter }
        import protocol.server.{ Protocol => Interface }

        type ServerFactory = VectorV1ProtocolServerFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(classOf[ServerFactory])(ctx, self, cryptoKey, end)
      }

      val client = {
        import factory.client._

        import protocol.server.{ Protocol => Parameter }
        import protocol.client.{ Protocol => Interface }

        type ClientFactory = VectorV1ProtocolClientFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(classOf[ClientFactory])(ctx, server, cryptoKey, end)
      }

      val frontend = {
        import factory.client.frontend._

        import protocol.client.{ Protocol => Parameter }
        import protocol.client.frontend.{ Protocol => Interface }

        type ClientFrontendFactory = VectorV1ProtocolClientFrontendFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(classOf[ClientFrontendFactory])(ctx, client, end, apply)
      }

      VectorV1SessionStart(server, client, frontend)
    }

  }

}
