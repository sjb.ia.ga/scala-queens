package queens
package version.v1
package vector
package interface

import akka.actor.typed.scaladsl.ActorContext

import base.interface.ServerVectorV1ProtocolDeferIfce

import protocol.server.defer.{ Protocol => Parameter1 }
import protocol.client.defer.{ Protocol => Parameter2 }
import protocol.server.backend.{ Protocol => Interface }


/**
  * @see [[queens.version.v1.vector.server.defer.Implicits.ServerVectorV1ProtocolDeferIfceImplicit]]
  */
abstract trait ObjectServerVectorV1ProtocolDeferIfce extends ServerVectorV1ProtocolDeferIfce[
  Interface.Role,
  Parameter1.Role,
  Parameter2.Role
] { interface =>

  protected object ResumeVectorV1
        extends ObjectResumeV1 {

    override def apply(
      ctx: ActorContext[?],
      graceTo: Parameter1.Actor,
      resultTo: Parameter2.Actor,
      args: Any*
    ): Interface.Actor = {
      import factory.server.backend._

      type ServerBackendFactory = VectorV1ProtocolServerBackendFactory[
        Interface.Role,
        Parameter1.Role,
        Parameter2.Role
      ]

      interface(classOf[ServerBackendFactory])(ctx, graceTo, resultTo, args*)
    }

  }

  override val ResumeNest = ResumeVectorV1

  override val ResumeMonadCO = ResumeVectorV1

}
