package queens
package version.v1
package vector
package interface

import akka.actor.typed.scaladsl.ActorContext

import base.interface.VectorV1ProtocolServerIfce

import protocol.client.defer.{ Protocol => Parameter }
import protocol.server.defer.{ Protocol => Interface }


/**
  * @see [[queens.version.v1.vector.server.Implicits.VectorV1ProtocolServerIfceImplicit]]
  */
abstract trait ObjectVectorV1ProtocolServerIfce extends VectorV1ProtocolServerIfce[
  Interface.Role,
  Parameter.Role
] { interface =>

  protected object PlaneVectorV1
      extends ObjectPlaneV1 {

    override def apply(
      ctx: ActorContext[?],
      readyTo: Parameter.Actor,
      args: Any*
    ): Interface.Actor = {
      import factory.server.defer._

      type ServerDeferFactory = VectorV1ProtocolServerDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(classOf[ServerDeferFactory])(ctx, readyTo, args*)
    }

  }

  override val PlaneNest = PlaneVectorV1

  override val PlaneMonadCO = PlaneVectorV1

}
