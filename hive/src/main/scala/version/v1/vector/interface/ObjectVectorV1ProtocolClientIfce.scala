package queens
package version.v1
package vector
package interface

import akka.actor.typed.scaladsl.ActorContext

import base.interface.VectorV1ProtocolClientIfce

import protocol.client.frontend.{ Protocol => Parameter }
import protocol.client.defer.{ Protocol => Interface }


/**
  * @see [[queens.version.v1.vector.client.Implicits.VectorV1ProtocolClientIfceImplicit]]
  */
abstract trait ObjectVectorV1ProtocolClientIfce extends VectorV1ProtocolClientIfce[
  Interface.Role,
  Parameter.Role
] { interface =>

  protected class BreedVectorV1(
    planes: Any => Seq[Any]
  ) extends ObjectBreedV1 {

    def apply(
      ctx: ActorContext[?],
      onBehalf: Parameter.Actor,
      args: Any*
    ): Interface.Actor = {
      import factory.client.defer._

      type ClientDeferFactory = VectorV1ProtocolClientDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(classOf[ClientDeferFactory])(ctx, onBehalf, (args(0) +: planes(args(1)))*)
    }

  }

  override object BreedNest
      extends BreedVectorV1(it => it :: null :: Nil)

  override object BreedMonadCO
      extends BreedVectorV1(it => null :: it :: Nil)

}
