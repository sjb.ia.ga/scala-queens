package queens
package version.v1
package vector
package interface

import scala.collection.mutable.{
  ArrayBuffer => Accumulator,
  HashMap => MutableMap
}

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import akka.util.ReentrantGuard

import queens.base.breed.Breed

import dimensions.three.breed.Hatch
import version.less.nest.{ Nest, Wildcard => NestWildcard }

import dimensions.fourth.fifth.breed.Spawn
import version.v1.mco.{ MonadCO, Wildcard => MCOWildcard }
import version.patch.v1.mbo.MonadBO
import version.patch.v1.mdo.MonadDO

import conf.emito.Conf

import vector.breed.Hive
import vector.{ Apply, VectorV1, Wildcard }
import version.base.Multiplier

import version.base.role.node.Session
import version.base.role.{ Adapter, Client, Frontend, Server }

import base.interface.VectorV1ProtocolIfce

import protocol.base.Protocol.Group
import protocol.session.Protocol.Role


/**
  * @see [[queens.version.v1.vector.Implicits.VectorV1ProtocolIfceImplicit]]
  */
abstract trait ObjectVectorV1ProtocolIfce[
  M >: Group <: Role
] extends VectorV1ProtocolIfce[M] { interface =>

  override object SessionExpand
      extends ObjectVectorV1SessionExpand:

    import version.base.Role
    import version.base.role.node.Session

    override def apply[
      P <: Conf
    ](using
      params: P
    )(context: ActorContext[?],
      end: Role => Boolean,
      stop: () => Unit,
      block: M => Unit,
      vector_v1: Apply[P],
      wildcard: Wildcard[P],
      multiplier: Multiplier[VectorV1[P]]
    ): Int =
      import ObjectVectorV1ProtocolIfce._

      val nest_multiplier = multiplier { (nest: Nest) =>
        new VectorV1(nest.toVector)
      }

      val vs: Seq[(Nest, MonadCO[P])] = for
        n <- wildcard._1(nest_multiplier)
        m <- wildcard._2()
        if n.model == m.model
        acc = Accumulator[(PlaneV1, Breed)]()
        lock = new ReentrantGuard()
      yield (
        new NestV1(acc, lock, n),
        m match
          case mb: MonadBO[P] => new MonadBOV1(acc, lock, mb)
          case md: MonadDO[P] => new MonadDOV1(acc, lock, md)
          case mc => new MonadCOV1(acc, lock, mc)
      )

      if vs.size == 0 then
        return 0

      val nest_wildcard = new NestWildcardV1(vs.map(_._1))
      val mco_wildcard = new MCOWildcardV1(vs.map(_._2))

      import factory.session.adapter._
      import protocol.session.adapter.{ Protocol => SessionAdapter }
      type SessionAdapterFactory = VectorV1ProtocolSessionAdapterFactory[SessionAdapter.Role]

      def msg_v1(message: Either[Unit, M], sessionAdapter: SessionAdapter.Actor): Unit =
        message match
          case Left(_) =>
            sessionAdapter ! Start(sessionAdapter)
          case Right(msg) =>
            block(msg)

      val end_v1 = endV1(end, stop)

      var sessionAdapter: Twin[SessionAdapter.Actor] = null

      import protocol.client.frontend.{ Protocol => ClientFrontend }

      import factory.session._
      import protocol.session.{ Protocol => Session }
      type SessionFactory = VectorV1ProtocolSessionFactory[Session.Role]

      import common.pipeline.Context
      import dimensions.fifth.QueensUseSolution

      val apply_v1 =  { (nest: Nest, hatch: Hatch[params.Q], monadCO: MonadCO[P], spawn: Spawn[P]) =>
                        vector_v1(VectorV1(nest, monadCO),
                                  { case next: Option[Context => QueensUseSolution] =>
                                      Hive(hatch, spawn)(next) })
                      }

      val session: Twin[ActorRef[Either[?, SessionAdapter.Role]]] = (

        interface(classOf[SessionFactory])[Unit](context,
          { (frontend: ClientFrontend.Actor) =>
            frontend ! Expand(nest_wildcard)
          },
          end_v1,
          msg_v1(_, sessionAdapter._1),
          nestApply(apply_v1)
        ).asInstanceOf[ActorRef[Either[?, SessionAdapter.Role]]],

        interface(classOf[SessionFactory])[Unit](context,
          { (frontend: ClientFrontend.Actor) =>
            frontend ! Expand(mco_wildcard)
          },
          end_v1,
          msg_v1(_, sessionAdapter._2),
          mcoApply(apply_v1)
        ).asInstanceOf[ActorRef[Either[?, SessionAdapter.Role]]]

      )

      sessionAdapter = (
        interface(classOf[SessionAdapterFactory])(context, session._1, end_v1),
        interface(classOf[SessionAdapterFactory])(context, session._2, end_v1)
      )

      session._1 ! Left(())
      session._2 ! Left(())

      vs.size

}


object ObjectVectorV1ProtocolIfce:

  import dimensions.Dimension.{ Aspect, Algorithm, Model }

  import version.base.Plane

  private[version] abstract trait PlaneV1:
    val acc: Accumulator[(PlaneV1, Breed)]
    val lock: ReentrantGuard

  private[version] class NestV1(
    override val acc: Accumulator[(PlaneV1, Breed)],
    override val lock: ReentrantGuard,
    _nest: Nest
  ) extends Nest(_nest.aspect: Aspect, _nest.algorithm: Algorithm, _nest.model: Model)
      with PlaneV1

  private[version] class MonadCOV1[
    P <: Conf
  ](
    override val acc: Accumulator[(PlaneV1, Breed)],
    override val lock: ReentrantGuard,
    _monadCO: MonadCO[P]
  )(using
    override protected val * : P
  ) extends MonadCO[P](_monadCO.model)
      with PlaneV1

  private[version] class MonadBOV1[
    P <: Conf
  ](
    override val acc: Accumulator[(PlaneV1, Breed)],
    override val lock: ReentrantGuard,
    _monadBO: MonadBO[P]
  )(using
    override protected val * : P
  ) extends MonadBO[P](_monadBO.model, _monadBO.use)
      with PlaneV1

  private[version] class MonadDOV1[
    P <: Conf
  ](
    override val acc: Accumulator[(PlaneV1, Breed)],
    override val lock: ReentrantGuard,
    _monadDO: MonadDO[P]
  )(using
    override protected val * : P
  ) extends MonadDO[P](_monadDO.model, _monadDO.use)
      with PlaneV1

  private[version] abstract trait WildcardV1[P <: Plane]
      extends version.base.Wildcard[P]:

    protected val _ps: Seq[P]

    override protected def apply(factor: P => Int): List[P] =
      _ps.toList

  private[version] class NestWildcardV1(
    override protected val _ps: Seq[Nest]
  ) extends NestWildcard
      with WildcardV1[Nest]

  private[version] class MCOWildcardV1[
    P <: Conf
  ](
    override protected val _ps: Seq[MonadCO[P]]
  )(using
    override protected val * : P
  ) extends MCOWildcard[P]
    with WildcardV1[MonadCO[P]]

  private[version] def nestApply[
    P <: Conf
  ](using
    params: P
  )(
    block: (Nest, Hatch[params.Q], MonadCO[P], Spawn[P]) => Unit
  ): client.frontend.Apply =
    case (nest: Nest, hatch: Hatch[params.Q]) =>
      val it = nest.asInstanceOf[PlaneV1]

      it.lock.withGuard {

        it.acc.size match
          case 0 =>
            it.acc += ((it, hatch))
          case 1 =>
            it.acc.head match
              case (monadCO: MonadCO[P], spawn: Spawn[P]) =>
                block(nest, hatch, monadCO, spawn)
              case _ => ???
          case _ => ???

      }

    case _ => ???

  private[version] def mcoApply[
    P <: Conf
  ](using
    params: P
  )(
    block: (Nest, Hatch[params.Q], MonadCO[P], Spawn[P]) => Unit
  ): client.frontend.Apply =
    case (monadCO: MonadCO[P], spawn: Spawn[P]) =>
      val it = monadCO.asInstanceOf[PlaneV1]

      it.lock.withGuard {

        it.acc.size match
          case 0 =>
            it.acc += ((it, spawn))
          case 1 =>
            it.acc.head match
              case (nest: Nest, hatch: Hatch[params.Q]) =>
                block(nest, hatch, monadCO, spawn)
              case _ => ???
          case _ => ???

      }

    case _ => ???

  def endV1(end: version.base.Role => Boolean,
            stop: () => Unit): version.base.Role => Boolean =
    val `end*` = (new ReentrantGuard(), new MutableMap[version.base.Role, (Boolean, Boolean)]())

    for
      role <- version.base.role.roles
      if version.base.role.isInitial(role)
    do
      `end*`._2(role) = (false, false)

    { (role: version.base.Role) =>

      `end*`._1.withGuard {

        if `end*`._2(role)._1
        then
          val r = `end*`._2(role)._2
          `end*`._2(role) = (false, false)
          if r && role == Adapter then stop()
          r
        else
          val r = end(role)
          `end*`._2(role) = (true, r)
          r

      }

    }
