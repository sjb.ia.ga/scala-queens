package queens
package version.v1
package vector
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.ServerVectorV1ProtocolDefer
import base.interface.ServerVectorV1ProtocolDeferIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.server.defer.Protocol.Role
import protocol.server.defer._


class ServerVectorV1ProtocolDeferImpl[
  EC <: VectorV1ServerDeferErrorCode,
  M >: Group <: Role,
  SBM >: Group <: ServerBackendRole,
  CDM >: Group <: ClientDeferRole,
  I <: ServerVectorV1ProtocolDeferIfce[SBM, M, CDM]
](
  override protected val readyTo: Actor[CDM]
)(implicit
  override protected val errorCode: EC,
  override protected val interface: I
) extends ServerVectorV1ProtocolDefer[EC, M, SBM, CDM, I] {

  class ServerVectorV1DeferBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val secret: java.util.UUID
  ) extends ServerVectorV1DeferBehavior[ServerVectorV1DeferBehaviorImpl]

  private object ServerVectorV1DeferInflater
      extends ServerVectorV1DeferInflater[ServerVectorV1DeferBehaviorImpl] {

    override def apply(em: Arguments): ServerVectorV1DeferBehaviorImpl =
      new ServerVectorV1DeferBehaviorImpl(em._1, em._2)

  }

  private object ServerVectorV1DeferBehaviorImpl
      extends ObjectRolePlaneBehavior[ServerVectorV1DeferBehaviorImpl, ServerVectorV1DeferInflater.type] {

    override protected val inflater = ServerVectorV1DeferInflater

  }

}


import factory.server.defer._

import protocol.client.defer.{ Protocol => Parameter }
import protocol.server.defer.{ Protocol => Interface }

object ServerVectorV1ProtocolDeferImpl extends VectorV1ProtocolServerDeferFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    readyTo: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import interface.ObjectServerVectorV1ProtocolDeferIfce
      import server.defer.Implicits._

      new ServerVectorV1ProtocolDeferImpl[
        VectorV1ServerDeferErrorCode,
        Interface.Role,
        protocol.server.backend.Protocol.Role,
        Parameter.Role,
        ObjectServerVectorV1ProtocolDeferIfce,
      ](
        readyTo
      ).ServerVectorV1DeferBehaviorImpl(args*)

    }

}
