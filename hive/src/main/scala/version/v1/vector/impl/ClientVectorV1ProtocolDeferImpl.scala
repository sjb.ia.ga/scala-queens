package queens
package version.v1
package vector
package impl

import akka.actor.typed.scaladsl.ActorContext

import version.less.nest.Nest
import mco.MonadCO

import base.ClientVectorV1ProtocolDefer

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.client.defer.Protocol.Role
import protocol.client.defer._


class ClientVectorV1ProtocolDeferImpl[
  EC <: VectorV1ClientDeferErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  SDM >: Group <: ServerDeferRole,
  SBM >: Group <: ServerBackendRole,
  CFM >: Group <: ClientFrontendRole
](
  override protected val onBehalf: Actor[CFM]
)(implicit
  override protected val errorCode: EC
) extends ClientVectorV1ProtocolDefer[EC, M, SM, SDM, SBM, CFM] {

  class ClientVectorV1DeferBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val nest: Nest,
    override protected val monadCO: MonadCO[?]
  ) extends ClientVectorV1DeferBehavior[ClientVectorV1DeferBehaviorImpl]

  private object ClientVectorV1DeferInflater
      extends ClientVectorV1DeferInflater[ClientVectorV1DeferBehaviorImpl] {

    override def apply(em: Arguments): ClientVectorV1DeferBehaviorImpl =
      new ClientVectorV1DeferBehaviorImpl(em._1, em._2, em._3)

  }

  private object ClientVectorV1DeferBehaviorImpl
      extends ObjectRolePlaneBehavior[ClientVectorV1DeferBehaviorImpl, ClientVectorV1DeferInflater.type] {

    override protected val inflater = ClientVectorV1DeferInflater

  }

}


import factory.client.defer._

import protocol.client.frontend.{ Protocol => Parameter }
import protocol.client.defer.{ Protocol => Interface }

object ClientVectorV1ProtocolDeferImpl extends VectorV1ProtocolClientDeferFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    onBehalf: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import client.defer.Implicits._

      new ClientVectorV1ProtocolDeferImpl[
        VectorV1ClientDeferErrorCode,
        Interface.Role,
        protocol.server.Protocol.Role,
        protocol.server.defer.Protocol.Role,
        protocol.server.backend.Protocol.Role,
        Parameter.Role
      ](
        onBehalf
      ).ClientVectorV1DeferBehaviorImpl(args*)

    }

}
