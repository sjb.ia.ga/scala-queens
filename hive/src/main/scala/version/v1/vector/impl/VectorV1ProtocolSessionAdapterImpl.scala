package queens
package version.v1
package vector
package impl

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import base.VectorV1ProtocolSessionAdapter

import protocol.base.Protocol.Group
import protocol.session.adapter.Protocol.Role
import protocol.session.adapter._


class VectorV1ProtocolSessionAdapterImpl[
  EC <: VectorV1SessionAdapterErrorCode,
  M >: Group <: Role
](
  override protected val session: ActorRef[Either[?, M]]
)(implicit
  override protected val errorCode: EC
) extends VectorV1ProtocolSessionAdapter[EC, M] {

  class SessionAdapterVectorV1BehaviorImpl(
    override protected val end: version.base.Role => Boolean
  ) extends SessionAdapterVectorV1Behavior[SessionAdapterVectorV1BehaviorImpl]

  private object SessionAdapterVectorV1Inflater
      extends SessionAdapterVectorV1Inflater[SessionAdapterVectorV1BehaviorImpl] {

    override def apply(em: Arguments): SessionAdapterVectorV1BehaviorImpl =
      new SessionAdapterVectorV1BehaviorImpl(em._1)

  }

  private object SessionAdapterVectorV1BehaviorImpl
      extends ObjectRolePlaneBehavior[SessionAdapterVectorV1BehaviorImpl, SessionAdapterVectorV1Inflater.type] {

    override protected val inflater = SessionAdapterVectorV1Inflater

  }

}


import factory.session.adapter._

import protocol.session.adapter.{ Protocol => Interface }

object VectorV1ProtocolSessionAdapterImpl
    extends VectorV1ProtocolSessionAdapterFactory[Role] {

  override def apply(
    context: ActorContext[?],
    session: ActorRef[Either[?, Role]],
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import vector.session.adapter.Implicits._

      new VectorV1ProtocolSessionAdapterImpl[
        VectorV1SessionAdapterErrorCode,
        Role,
      ](
        session
      ).SessionAdapterVectorV1BehaviorImpl(args*)

    }

}
