package queens
package version.v1
package vector
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.VectorV1ProtocolClient
import base.interface.VectorV1ProtocolClientIfce

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.client.Protocol.Role
import protocol.client._


class VectorV1ProtocolClientImpl[
  EC <: VectorV1ClientErrorCode,
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole,
  CDM >: Group <: ClientDeferRole,
  SM >: Group <: ServerRole,
  I <: VectorV1ProtocolClientIfce[CDM, CFM]
](
  override protected val server: Actor[SM]
)(implicit
  override protected val errorCode: EC,
  override protected val interface: I
) extends VectorV1ProtocolClient[EC, M, CFM, CDM, SM, I] {

  class ClientVectorV1BehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val end: version.base.Role => Boolean
  ) extends ClientVectorV1Behavior[ClientVectorV1BehaviorImpl]

  private object ClientVectorV1Inflater
      extends ClientVectorV1Inflater[ClientVectorV1BehaviorImpl] {

    override def apply(em: Arguments): ClientVectorV1BehaviorImpl =
      new ClientVectorV1BehaviorImpl(em._1, em._2)

  }

  private object ClientVectorV1BehaviorImpl
      extends ObjectRolePlaneBehavior[ClientVectorV1BehaviorImpl, ClientVectorV1Inflater.type] {

    override val inflater = ClientVectorV1Inflater

  }

}


import factory.client._

import protocol.server.{ Protocol => Parameter }
import protocol.client.{ Protocol => Interface }

object VectorV1ProtocolClientImpl extends VectorV1ProtocolClientFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    server: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import interface.ObjectVectorV1ProtocolClientIfce
      import client.Implicits._

      new VectorV1ProtocolClientImpl[
        VectorV1ClientErrorCode,
        Role,
        protocol.client.frontend.Protocol.Role,
        protocol.client.defer.Protocol.Role,
        Parameter.Role,
        ObjectVectorV1ProtocolClientIfce
      ](
        server
      ).ClientVectorV1BehaviorImpl(args*)
    }

}
