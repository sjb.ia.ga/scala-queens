package queens
package version.v1
package vector
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.ServerVectorV1ProtocolBackend

import protocol.base.Protocol.Group
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.Role
import protocol.server.backend._


class ServerVectorV1ProtocolBackendImpl[
  EC <: VectorV1ServerBackendErrorCode,
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
](
  override protected val graceTo: Actor[SDM],
  override protected val resultTo: Actor[CDM]
)(implicit
  override protected val errorCode: EC
) extends ServerVectorV1ProtocolBackend[EC, M, SDM, CDM] {

  class ServerVectorV1BackendBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val secret: java.util.UUID
  ) extends ServerVectorV1BackendBehavior[ServerVectorV1BackendBehaviorImpl]

  private object ServerVectorV1BackendInflater
      extends ServerVectorV1BackendInflater[ServerVectorV1BackendBehaviorImpl] {

    override def apply(em: Arguments): ServerVectorV1BackendBehaviorImpl =
      new ServerVectorV1BackendBehaviorImpl(em._1, em._2)

  }

  private object ServerVectorV1BackendBehaviorImpl
      extends ObjectRolePlaneBehavior[ServerVectorV1BackendBehaviorImpl, ServerVectorV1BackendInflater.type] {

    override val inflater = ServerVectorV1BackendInflater

  }

}


import factory.server.backend._

import protocol.server.defer.{ Protocol => Parameter1 }
import protocol.client.defer.{ Protocol => Parameter2 }
import protocol.server.backend.{ Protocol => Interface }

object ServerVectorV1ProtocolBackendImpl extends VectorV1ProtocolServerBackendFactory[
  Role,
  Parameter1.Role,
  Parameter2.Role
] {

  override def apply(
    context: ActorContext[?],
    graceTo: Parameter1.Actor,
    resultTo: Parameter2.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import server.backend.Implicits._

      new ServerVectorV1ProtocolBackendImpl[
        VectorV1ServerBackendErrorCode,
        Role,
        Parameter1.Role,
        Parameter2.Role
      ](
        graceTo,
        resultTo
      ).ServerVectorV1BackendBehaviorImpl(args*)

    }

}
