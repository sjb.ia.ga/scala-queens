package queens
package version.v1
package vector
package impl

import akka.actor.typed.scaladsl.ActorContext

import client.frontend.Apply

import base.ClientVectorV1ProtocolFrontend

import protocol.base.Protocol.Group
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.Role
import protocol.client.frontend._


class ClientVectorV1ProtocolFrontendImpl[
  EC <: VectorV1ClientFrontendErrorCode,
  M >: Group <: Role,
  CM >: Group <: ClientRole
](
  override protected val client: Actor[CM],
)(implicit
  override protected val errorCode: EC
) extends ClientVectorV1ProtocolFrontend[EC, M, CM] {

  class ClientVectorV1FrontendBehaviorImpl(
    override protected val end: version.base.Role => Boolean,
    override protected val apply: Apply
  ) extends ClientVectorV1FrontendBehavior[ClientVectorV1FrontendBehaviorImpl]

  private object ClientVectorV1FrontendInflater
      extends ClientVectorV1FrontendInflater[ClientVectorV1FrontendBehaviorImpl] {

    override def apply(em: Arguments): ClientVectorV1FrontendBehaviorImpl =
      new ClientVectorV1FrontendBehaviorImpl(em._1, em._2)

  }

  private object ClientVectorV1FrontendBehaviorImpl
      extends ObjectRolePlaneBehavior[ClientVectorV1FrontendBehaviorImpl, ClientVectorV1FrontendInflater.type] {

    override protected val inflater = ClientVectorV1FrontendInflater

  }

}


import factory.client.frontend._

import protocol.client.{ Protocol => Parameter }
import protocol.client.frontend.{ Protocol => Interface }

object ClientVectorV1ProtocolFrontendImpl extends VectorV1ProtocolClientFrontendFactory[
  Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    client: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import vector.client.frontend.Implicits._

      new ClientVectorV1ProtocolFrontendImpl[
        VectorV1ClientFrontendErrorCode,
        Role,
        Parameter.Role
      ](
        client
      ).ClientVectorV1FrontendBehaviorImpl(args*)

    }

}
