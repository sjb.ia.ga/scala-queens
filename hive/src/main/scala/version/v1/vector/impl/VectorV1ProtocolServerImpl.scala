package queens
package version.v1
package vector
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.VectorV1ProtocolServer
import base.interface.VectorV1ProtocolServerIfce

import protocol.base.Protocol.Group
import protocol.session.Protocol.{ Role => SessionRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.Protocol.Role
import protocol.server._


class VectorV1ProtocolServerImpl[
  EC <: VectorV1ServerErrorCode,
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole,
  SDM >: Group <: ServerDeferRole,
  SM >: Group <: SessionRole,
  I <: VectorV1ProtocolServerIfce[SDM, CDM]
](
  override protected val session: Actor[SM]
)(implicit
  override protected val errorCode: EC,
  override protected val interface: I
) extends VectorV1ProtocolServer[EC, M, CDM, SDM, SM, I] {

  class ServerVectorV1BehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val end: version.base.Role => Boolean
  ) extends ServerVectorV1Behavior[ServerVectorV1BehaviorImpl]

  private object ServerVectorV1Inflater
      extends ServerVectorV1Inflater[ServerVectorV1BehaviorImpl] {

    override def apply(em: Arguments): ServerVectorV1BehaviorImpl =
      new ServerVectorV1BehaviorImpl(em._1, em._2)

  }

  private object ServerVectorV1BehaviorImpl
      extends ObjectRolePlaneBehavior[ServerVectorV1BehaviorImpl, ServerVectorV1Inflater.type] {

    override protected val inflater = ServerVectorV1Inflater

  }

}


import factory.server._

import protocol.session.adapter.{ Protocol => Parameter }
import protocol.server.{ Protocol => Interface }

object VectorV1ProtocolServerImpl extends VectorV1ProtocolServerFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    session: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import interface.ObjectVectorV1ProtocolServerIfce
      import server.Implicits._

      new VectorV1ProtocolServerImpl[
        VectorV1ServerErrorCode,
        Role,
        protocol.client.defer.Protocol.Role,
        protocol.server.defer.Protocol.Role,
        Parameter.Role,
        ObjectVectorV1ProtocolServerIfce
      ](
        session
      ).ServerVectorV1BehaviorImpl(args*)
    }

}
