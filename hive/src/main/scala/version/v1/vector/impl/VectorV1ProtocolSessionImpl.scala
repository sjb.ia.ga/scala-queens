package queens
package version.v1
package vector
package impl

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import base.VectorV1ProtocolSession
import base.interface.VectorV1ProtocolSessionIfce

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role
import protocol.session._


class VectorV1ProtocolSessionImpl[L,
  EC <: VectorV1SessionErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole,
  I <: VectorV1ProtocolSessionIfce[M, SM, CM, CFM]
](using
  override protected val errorCode: EC,
  override protected val interface: I
) extends VectorV1ProtocolSession[L, EC, M, SM, CM, CFM, I] {

  class SessionVectorV1BehaviorImpl(
    override protected val start: session.Start[CFM],
    override protected val end: version.base.Role => Boolean,
    override protected val block: session.Block[L, M],
    override protected val apply: client.frontend.Apply
  ) extends SessionVectorV1Behavior[SessionVectorV1BehaviorImpl]

  private object SessionVectorV1Inflater
      extends SessionVectorV1Inflater[SessionVectorV1BehaviorImpl] {

    override def apply(em: Arguments): SessionVectorV1BehaviorImpl =
      new SessionVectorV1BehaviorImpl(em._1, em._2, em._3, em._4)

  }

  private object SessionVectorV1BehaviorImpl
      extends ObjectSessionPlaneBehavior[SessionVectorV1BehaviorImpl, SessionVectorV1Inflater.type] {

    override protected val inflater = SessionVectorV1Inflater

  }

}


import factory.session._

object VectorV1ProtocolSessionImpl
    extends VectorV1ProtocolSessionFactory[Role] {

  override def apply[L](
    context: ActorContext[?],
    args: Any*
  ): ActorRef[Either[L, Role]] = context
    .spawnAnonymous {
      import interface.ObjectVectorV1ProtocolSessionIfce
      import session.Implicits._

      new VectorV1ProtocolSessionImpl[L,
        VectorV1SessionErrorCode,
        Role,
        protocol.server.Protocol.Role,
        protocol.client.Protocol.Role,
        protocol.client.frontend.Protocol.Role,
        ObjectVectorV1ProtocolSessionIfce
      ].SessionVectorV1BehaviorImpl(args*)

    }

}
