package queens
package version.v1
package vector
package base
package interface

import version.base.interface.NodeIfce
import version.less.nest.base.interface.NestProtocolClientIfce
import mco.base.interface.MCOProtocolClientIfce

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.Role


abstract trait VectorV1ProtocolClientIfce[
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole
] extends NodeIfce
    with NestProtocolClientIfce[M, CFM]
    with MCOProtocolClientIfce[M, CFM] {

  protected abstract trait ObjectBreedV1
    extends ObjectBreedNest
    with ObjectBreedMonadCO

}
