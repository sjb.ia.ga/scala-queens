package queens
package version.v1
package vector


package object base {

  import version.base.{ `Deseq*`, DeseqPlane, DeseqUUID }

  type VectorV1DeseqClient = `Deseq*`

  type VectorV1DeseqServer = `Deseq*`

  type VectorV1DeseqServerDefer = DeseqUUID

  type VectorV1DeseqServerBackend = DeseqUUID

  import version.less.plane.base.PlaneDeseqSessionAdapter

  type VectorV1DeseqSessionAdapter = PlaneDeseqSessionAdapter

}
