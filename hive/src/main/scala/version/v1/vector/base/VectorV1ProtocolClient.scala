package queens
package version.v1
package vector
package base

import akka.actor.typed.scaladsl.ActorContext

import version.less.nest.Nest
import mco.MonadCO

import interface.VectorV1ProtocolClientIfce
import version.less.nest.base.NestProtocolClient
import mco.base.MCOProtocolClient

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.client.Protocol.Role
import protocol.client._


abstract trait VectorV1ProtocolClient[
  EC <: VectorV1ClientErrorCode,
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole,
  CDM >: Group <: ClientDeferRole,
  SM >: Group <: ServerRole,
  I <: VectorV1ProtocolClientIfce[CDM, CFM]
] extends VectorV1ClientProtocol
    with NestProtocolClient[EC, M, CFM, CDM, SM, I]
    with MCOProtocolClient[EC, M, CFM, CDM, SM, I] {

  protected abstract trait ClientVectorV1Behavior[T <: ClientVectorV1Behavior[T]]
      extends ClientNestBehavior[T]
      with ClientMCOBehavior[T] {

    import errorCode._

    protected def vectorV1_perceive(ctx: ActorContext[M], msg: M): Nothing =
      exclusive(ctx, msg, nest_perceive(_, _), mco_perceive(_, _))

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        super.errorBehavior(server, msg)
      } catch {
        case NoneBehavior =>
          try {
            vectorV1_perceive(ctx, msg)
          } catch {
            case NoneBehavior =>
              try {
                super.errorBehavior(server, msg, after = true)
              } catch {
                case NoneBehavior =>
                  server ! Complaint(msg)
                  same
              }
          }
      }

  }

  protected abstract trait ClientVectorV1Inflater[T <: ClientVectorV1Behavior[T]]
      extends RolePlaneInflater[T]
      with VectorV1DeseqClient

}
