package queens
package version.v1
package vector
package base

import akka.actor.typed.scaladsl.ActorContext

import version.less.nest.Nest
import mco.MonadCO

import version.less.nest.base.ClientNestProtocolFrontend
import mco.base.ClientMCOProtocolFrontend

import protocol.base.Protocol.Group
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.Role
import protocol.client.frontend._


abstract trait ClientVectorV1ProtocolFrontend[
  EC <: VectorV1ClientFrontendErrorCode,
  M >: Group <: Role,
  CM >: Group <: ClientRole
] extends VectorV1ClientFrontendProtocol
    with ClientNestProtocolFrontend[EC, M, CM]
    with ClientMCOProtocolFrontend[EC, M, CM] {

  protected abstract trait ClientVectorV1FrontendBehavior[T <: ClientVectorV1FrontendBehavior[T]]
      extends ClientNestFrontendBehavior[T]
      with ClientMCOFrontendBehavior[T] {

    import errorCode._

    protected def vectorV1_perceive(ctx: ActorContext[M], msg: M): Nothing =
      exclusive(ctx, msg, nest_perceive(_, _), mco_perceive(_, _))

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        super.errorBehavior(client, msg)
      } catch {
        case NoneBehavior =>
          try {
            vectorV1_perceive(ctx, msg)
          } catch {
            case NoneBehavior =>
              try {
                super.errorBehavior(client, msg, after = true)
              } catch {
                case NoneBehavior =>
                  client ! Complaint(msg)
                  same
              }
          }
      }

  }

  protected abstract trait ClientVectorV1FrontendInflater[T <: ClientVectorV1FrontendBehavior[T]]
      extends RolePlaneInflater[T]
      with VectorV1DeseqClientFrontend

}
