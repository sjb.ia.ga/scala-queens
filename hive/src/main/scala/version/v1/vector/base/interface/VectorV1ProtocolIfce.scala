package queens
package version.v1
package vector
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import conf.emito.Conf

import vector.{ Apply, VectorV1, Wildcard }
import version.base.Multiplier

import version.less.plane.base.interface.PlaneProtocolIfce

import protocol.base.Protocol.Group
import protocol.session.Protocol.Role


abstract trait VectorV1ProtocolIfce[
  M >: Group <: Role
] extends PlaneProtocolIfce {

  protected abstract trait ObjectVectorV1SessionExpand
      extends ObjectSessionExpand { self =>

    def apply[
      P <: Conf
    ](using
      params: P
    )(context: ActorContext[?],
      end: version.base.Role => Boolean,
      stop: () => Unit,
      block: M => Unit,
      apply: Apply[P],
      wildcard: Wildcard[P],
      multiplier: Multiplier[VectorV1[P]] = Multiplier.Once()
    ): Int

  }

  override def SessionExpand: ObjectVectorV1SessionExpand

}
