package queens
package version.v1
package vector
package base

import akka.actor.typed.scaladsl.ActorContext

import interface.ServerVectorV1ProtocolDeferIfce
import version.less.nest.base.ServerNestProtocolDefer
import mco.base.ServerMCOProtocolDefer

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.server.defer.Protocol.Role
import protocol.server.defer._


abstract trait ServerVectorV1ProtocolDefer[
  EC <: VectorV1ServerDeferErrorCode,
  M >: Group <: Role,
  SBM >: Group <: ServerBackendRole,
  CDM >: Group <: ClientDeferRole,
  I <: ServerVectorV1ProtocolDeferIfce[SBM, M, CDM]
] extends VectorV1ServerDeferProtocol
    with ServerNestProtocolDefer[EC, M, SBM, CDM, I]
    with ServerMCOProtocolDefer[EC, M, SBM, CDM, I] {

  protected abstract trait ServerVectorV1DeferBehavior[T <: ServerVectorV1DeferBehavior[T]]
      extends ServerNestDeferBehavior[T]
      with ServerMCODeferBehavior[T] {

    import version.less.plane.protocol.message.resume.{ ResumeProtocol => rm }
    import version.less.plane.protocol.message.grace.{ GraceProtocol => gm }

    import errorCode._

    protected def vectorV1_errorBehavior(replyTo: Actor[CDM], msg: M): Nothing = msg match {
      case rm.Resume(_) => none
      case gm.Grace(_) => none
      case _ =>
        excl_errorBehavior(replyTo, msg, nest_errorBehavior(_, _), mco_errorBehavior(_, _))
    }

    protected def vectorV1_perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        exclusive(ctx, msg, nest_perceive(_, _), mco_perceive(_, _))
      } catch {
        case NoneBehavior =>
          msg match {
            case rm.Resume(axes) =>
              readyTo ! invalidPlane(axes)
              stopped
            case gm.Grace(axes) =>
              readyTo ! invalidPlane(axes)
              stopped
            case _ =>
              none
          }
      }

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        vectorV1_errorBehavior(readyTo, msg)
      } catch {
        case NoneBehavior =>
          try {
            super.errorBehavior(readyTo, msg)
          } catch {
            case NoneBehavior =>
              try {
                vectorV1_perceive(ctx, msg)
              } catch {
                case NoneBehavior =>
                  readyTo ! Complaint(msg)
                  stopped
              }
          }
      }

  }

  protected abstract trait ServerVectorV1DeferInflater[T <: ServerVectorV1DeferBehavior[T]]
      extends RolePlaneInflater[T]
      with VectorV1DeseqServerDefer

}
