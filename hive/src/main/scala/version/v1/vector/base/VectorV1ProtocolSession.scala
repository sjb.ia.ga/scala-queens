package queens
package version.v1
package vector
package base

import akka.actor.typed.TypedActorContext
import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import version.less.plane.base.PlaneProtocolSession
import interface.VectorV1ProtocolSessionIfce
import interface.VectorV1SessionStart

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role
import protocol.session._


abstract trait VectorV1ProtocolSession[L,
  EC <: VectorV1SessionErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole,
  I <: VectorV1ProtocolSessionIfce[M, SM, CM, CFM]
] extends VectorV1SessionProtocol
    with PlaneProtocolSession[L, EC, M, I] {

  protected abstract trait SessionVectorV1Behavior[T <: SessionVectorV1Behavior[T]]
      extends SessionPlaneBehavior[T] {

    import version.less.plane.protocol.message.session.{ SessionProtocol => sm }

    import errorCode._

    protected val start: session.Start[CFM]
    protected val block: session.Block[L, M]
    protected val apply: client.frontend.Apply

    protected def vectorV1_perceive(ctx: ActorContext[?], msg: M): Nothing = msg match {

      case sm.Start(self: Actor[M]) =>

        val VectorV1SessionStart(server: Actor[SM], client: Actor[CM], frontend: Actor[CFM]) =
          interface.StartVectorV1[L](ctx, self, start, end, block, apply)

        start(frontend)

        same

      case _ =>
        none

    }

    override protected def perceive(ctx: ActorContext[?], msg: M): Nothing =
      try {
        super.errorBehavior({ it => block(Right(it)) }, msg)
      } catch {
        case NoneBehavior =>
          try {
            vectorV1_perceive(ctx, msg)
          } catch {
            case NoneBehavior =>
              block(Right(msg))
              same
          }
      }

    override def receive(ctx: TypedActorContext[Either[L, M]], msg: Either[L, M]): B = msg match {

      case Right(_) =>
        super.receive(ctx.asScala, msg)

      case Left(_) =>
        block(msg)
        Behaviors.same.asInstanceOf[B]

    }

  }

  protected abstract trait SessionVectorV1Inflater[T <: SessionVectorV1Behavior[T]]
      extends SessionPlaneInflater[T]
      with VectorV1DeseqSession[L, M, CFM]

}
