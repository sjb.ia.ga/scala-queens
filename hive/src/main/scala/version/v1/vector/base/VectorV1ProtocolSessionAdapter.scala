package queens
package version.v1
package vector
package base

import version.less.plane.base.PlaneProtocolSessionAdapter

import protocol.base.Protocol.Group
import protocol.session.adapter.Protocol.Role
import protocol.session.adapter._


abstract trait VectorV1ProtocolSessionAdapter[
  EC <: VectorV1SessionAdapterErrorCode,
  M >: Group <: Role
] extends VectorV1SessionAdapterProtocol
    with PlaneProtocolSessionAdapter[EC, M] {

  protected abstract trait SessionAdapterVectorV1Behavior[T <: SessionAdapterVectorV1Behavior[T]]
      extends SessionAdapterPlaneBehavior[T]

  protected abstract trait SessionAdapterVectorV1Inflater[T <: SessionAdapterVectorV1Behavior[T]]
      extends SessionAdapterPlaneInflater[T]
      with VectorV1DeseqSessionAdapter

}
