package queens
package version.v1
package vector
package base

import akka.actor.typed.scaladsl.ActorContext

import version.less.nest.base.ServerNestProtocolBackend
import mco.base.ServerMCOProtocolBackend

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.Role
import protocol.server.backend._


abstract trait ServerVectorV1ProtocolBackend[
  EC <: VectorV1ServerBackendErrorCode,
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
] extends VectorV1ServerBackendProtocol
    with ServerNestProtocolBackend[EC, M, SDM, CDM]
    with ServerMCOProtocolBackend[EC, M, SDM, CDM] {

  protected abstract trait ServerVectorV1BackendBehavior[T <: ServerVectorV1BackendBehavior[T]]
      extends ServerNestBackendBehavior[T]
      with ServerMCOBackendBehavior[T] {

    import errorCode._

    protected def vectorV1_perceive_salute(ctx: ActorContext[M], msg: M): Nothing =
      exclusive(ctx, msg, nest_perceive_salute(_, _), mco_perceive_salute(_, _))

    protected def vectorV1_perceive(ctx: ActorContext[M], msg: M): Nothing =
      exclusive(ctx, msg, nest_perceive(_, _), mco_perceive(_, _))

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        vectorV1_perceive_salute(ctx, msg)
      } catch {
        case NoneBehavior =>
          try {
            super.errorBehavior(resultTo, msg)
          } catch {
            case NoneBehavior =>
              try {
                vectorV1_perceive(ctx, msg)
              } catch {
                case NoneBehavior =>
                  resultTo ! Complaint(msg)
                  same
              }
          }
      }

  }

  protected abstract trait ServerVectorV1BackendInflater[T <: ServerVectorV1BackendBehavior[T]]
      extends RolePlaneInflater[T]
      with VectorV1DeseqServerBackend

}
