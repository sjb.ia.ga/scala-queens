package queens
package version.v1
package vector
package base
package interface

import version.base.interface.NodeIfce
import version.less.nest.base.interface.ServerNestProtocolDeferIfce
import mco.base.interface.ServerMCOProtocolDeferIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.Role


abstract trait ServerVectorV1ProtocolDeferIfce[
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
] extends NodeIfce
    with ServerNestProtocolDeferIfce[M, SDM, CDM]
    with ServerMCOProtocolDeferIfce[M, SDM, CDM] {

  protected abstract trait ObjectResumeV1
    extends ObjectResumeNest
    with ObjectResumeMonadCO

}
