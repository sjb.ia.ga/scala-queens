package queens
package version.v1
package vector
package base

import version.base.Deseq

import client.frontend.Apply


abstract trait VectorV1DeseqClientFrontend
    extends Deseq {

  override type Arguments = Tuple2[
    version.base.Role => Boolean,
    Apply
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val end = args(0).asInstanceOf[version.base.Role => Boolean]
    val apply = args(1).asInstanceOf[Apply]
    (end, apply)
  }

}
