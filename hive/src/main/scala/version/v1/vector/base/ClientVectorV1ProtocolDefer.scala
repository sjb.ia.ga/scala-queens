package queens
package version.v1
package vector
package base

import akka.actor.typed.scaladsl.ActorContext

import version.less.nest.Nest
import mco.MonadCO

import version.less.nest.base.ClientNestProtocolDefer
import mco.base.ClientMCOProtocolDefer

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.client.defer.Protocol.Role
import protocol.client.defer._


abstract trait ClientVectorV1ProtocolDefer[
  EC <: VectorV1ClientDeferErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  SDM >: Group <: ServerDeferRole,
  SBM >: Group <: ServerBackendRole,
  CFM >: Group <: ClientFrontendRole
] extends VectorV1ClientDeferProtocol
    with ClientNestProtocolDefer[EC, M, SM, SDM, SBM, CFM]
    with ClientMCOProtocolDefer[EC, M, SM, SDM, SBM, CFM] {

  protected abstract trait ClientVectorV1DeferBehavior[T <: ClientVectorV1DeferBehavior[T]]
      extends ClientNestDeferBehavior[T]
      with ClientMCODeferBehavior[T] {

    import version.less.plane.protocol.message.defer.{ DeferProtocol => dm }
    import version.less.plane.protocol.message.plane.{ PlaneProtocol => pm }
    import version.less.plane.protocol.message.ready.{ ReadyProtocol => ym }

    import errorCode._

    protected def vectorV1_errorBehavior(replyTo: Actor[CFM], msg: M): Nothing =
      msg match {
        case dm.Defer(_, pm.Plane(_, _), _) => none
        case _ =>
          excl_errorBehavior(replyTo, msg, nest_errorBehavior(_, _), mco_errorBehavior(_, _))
      }

    protected def vectorV1_perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        exclusive(ctx, msg, { (_, _) =>
          msg match {
            case ym.Ready(_, _) if nest eq null => none
            case _ =>
              nest_perceive(ctx, msg)
          }
        }, { (_, _) =>
          msg match {
            case ym.Ready(_, _) if monadCO eq null => none
            case _ =>
              mco_perceive(ctx, msg)
          }
        })
      } catch {
        case NoneBehavior =>
          msg match {
            case dm.Defer(_, pm.Plane(_, axes), _) =>
              onBehalf ! invalidPlane(axes)
              stopped
            case _ =>
              none
          }
      }

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        vectorV1_errorBehavior(onBehalf, msg)
      } catch {
        case NoneBehavior =>
          try {
            super.errorBehavior(onBehalf, msg)
          } catch {
            case NoneBehavior =>
              try {
                vectorV1_perceive(ctx, msg)
              } catch {
                case NoneBehavior =>
                  onBehalf ! Complaint(msg)
                  stopped
              }
          }
      }

  }

  protected abstract trait ClientVectorV1DeferInflater[T <: ClientVectorV1DeferBehavior[T]]
      extends RolePlaneInflater[T]
      with VectorV1DeseqClientDefer

}
