package queens
package version.v1
package vector
package base
package interface

import version.less.nest.base.interface.NestProtocolServerIfce
import mco.base.interface.MCOProtocolServerIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.Role


abstract trait VectorV1ProtocolServerIfce[
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole
] extends NestProtocolServerIfce[M, CDM]
    with MCOProtocolServerIfce[M, CDM] {

  protected abstract trait ObjectPlaneV1
    extends ObjectPlaneNest
    with ObjectPlaneMonadCO

}
