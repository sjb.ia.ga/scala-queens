package queens
package version.v1
package vector
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role


final case class VectorV1SessionStart[
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole
](server: Actor[SM], client: Actor[CM], frontend: Actor[CFM])


abstract trait VectorV1ProtocolSessionIfce[
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole
] extends NodeIfce {

  protected abstract trait ObjectStartVectorV1 {

    def apply[L](
      ctx: ActorContext[?],
      self: Actor[M],
      args: Any*
    ): VectorV1SessionStart[SM, CM, CFM]

  }

  def StartVectorV1: ObjectStartVectorV1

}
