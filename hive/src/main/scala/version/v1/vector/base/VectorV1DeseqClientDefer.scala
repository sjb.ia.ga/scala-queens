package queens
package version.v1
package vector
package base

import version.base.{ Deseq, DeseqCipher, DeseqPlane }

import version.less.nest.Nest
import mco.MonadCO


abstract trait VectorV1DeseqClientDefer
    extends Deseq {

  private object Deseq0
      extends DeseqCipher

  private object Deseq1
      extends DeseqPlane[Nest]

  private object Deseq2
      extends DeseqPlane[MonadCO[?]]

  override type Arguments = Tuple3[
    CryptoKey,
    Nest,
    MonadCO[?]
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val Tuple1(key) = Deseq0(args.take(1))
    val Tuple1(nest) = Deseq1(args.drop(1).take(1))
    val Tuple1(monadCO) = Deseq2(args.drop(2).take(1))
    Tuple3[CryptoKey, Nest, MonadCO[?]](key, nest, monadCO)
  }

}
