package queens
package version.v1
package vector
package base

import akka.actor.typed.scaladsl.ActorContext

import version.less.nest.Nest
import mco.MonadCO

import interface.VectorV1ProtocolServerIfce
import version.less.nest.base.NestProtocolServer
import mco.base.MCOProtocolServer

import protocol.base.Protocol.Group
import protocol.session.Protocol.{ Role => SessionRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.Protocol.Role
import protocol.server._


abstract trait VectorV1ProtocolServer[
  EC <: VectorV1ServerErrorCode,
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole,
  SDM >: Group <: ServerDeferRole,
  SM >: Group <: SessionRole,
  I <: VectorV1ProtocolServerIfce[SDM, CDM]
] extends VectorV1ServerProtocol
    with NestProtocolServer[EC, M, CDM, SDM, SM, I]
    with MCOProtocolServer[EC, M, CDM, SDM, SM, I] {

  protected abstract trait ServerVectorV1Behavior[T <: ServerVectorV1Behavior[T]]
      extends ServerNestBehavior[T]
      with ServerMCOBehavior[T] {

    import errorCode._

    protected def vectorV1_perceive(ctx: ActorContext[M], msg: M): Nothing =
      exclusive(ctx, msg, nest_perceive(_, _), mco_perceive(_, _))

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        super.errorBehavior(session, msg)
      } catch {
        case NoneBehavior =>
          try {
            vectorV1_perceive(ctx, msg)
          } catch {
            case NoneBehavior =>
              try {
                super.errorBehavior(session, msg, after = true)
              } catch {
                case NoneBehavior =>
                  session ! Complaint(msg)
                  same
              }
          }
      }

  }

  protected abstract trait ServerVectorV1Inflater[T <: ServerVectorV1Behavior[T]]
      extends RolePlaneInflater[T]
      with VectorV1DeseqServer

}
