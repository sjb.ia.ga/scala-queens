package queens
package version.v1
package vector
package breed

import java.util.UUID
import java.util.concurrent.{ ConcurrentHashMap => Map }

import common.Flag

import conf.mco.Conf


final class `Hatch'em, Spawn'it, Console'o`[
  P <: Conf
](
  _em: Iterable[Hive[P]],
  _tag: Any,
  _its: Seq[UUID] = UUID.randomUUID :: Nil
)(using
  override protected val * : P
)(
  override protected val fs: *.Item => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)] = new Map()
) extends `Base.Hatch'em, Spawn'it, Output'o`[P](_em, _tag, _its):

  override protected type * = `Hatch'em, Spawn'it, Console'o`[P]

  override protected def apply[R](
    it: *.Item,
    block: *.Item => R
  ): R =
    if *.`Console'o Header` then
      Console.print(s"[$this(${it.the})] ")
    block(it)

  override def `#`(t: Any): * =
    new `Hatch'em, Spawn'it, Console'o`[P](_em, t, _its)(fs*)

  override protected def `apply°`(s: *.Item => Boolean*): * =
    new `Hatch'em, Spawn'it, Console'o`[P](_em, _tag, _its)(s*)

  override def toString(): String = "Console " + super.toString


object `Hatch'em, Spawn'it, Console'o`:

  import common.NoTag

  def apply[
    P <: Conf
  ](
    em: Iterable[Hive[P]]
  )(using
    params: P
  ): `Hatch'em, Spawn'it, Output'o`[P] =
    new `Hatch'em, Spawn'it, Console'o`[P](em, NoTag)()
