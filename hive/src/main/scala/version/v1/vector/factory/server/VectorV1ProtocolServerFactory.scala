package queens
package version.v1
package vector
package factory.server

import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.session.Protocol.{ Role => SessionRole }
import protocol.server.Protocol.Role


abstract trait VectorV1ProtocolServerFactory[
  M >: Group <: Role,
  SM >: Group <: SessionRole
] {

  def apply(
    context: ActorContext[?],
    session: Actor[SM],
    args: Any*
  ): Actor[M]

}
