package queens
package version.v1
package vector

import dimensions.Dimension
import dimensions.Dimension.{ Axes, Scalar }

import conf.emito.Conf

import version.base.Multiplier

import version.less.nest.Nest
import mco.MonadCO


class Wildcard[
  P <: Conf
] protected (
  override val _1: version.less.nest.Wildcard,
  override val _2: mco.Wildcard[P]
)(using
  protected val * : P
) extends Product2[version.less.nest.Wildcard, mco.Wildcard[P]]
    with version.base.Wildcard[VectorV1[P]]:

  override protected[version] val filter =
    Wildcard.mkFilter(_1.filter, _2.filter)

  override val axes = VectorV1.axes

  def this()(using `mcoWildcard*`: mco.Wildcard[P])(using params: P) =
    this(version.less.nest.Wildcard.*, `mcoWildcard*`)

  override protected def apply(factor: VectorV1[P] => Int): List[VectorV1[P]] =
    super.apply(factor, VectorV1.apply)

  override def equals(any: Any): Boolean = any match
    case that: Wildcard[P] =>
      (that canEqual this) &&
      this._1 == that._1 && this._2 == that._2
    case _ => false

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[Wildcard[P]]


object Wildcard:

  type Filter = version.base.Wildcard.Filter

  private[version] def mkFilter(nestFilter: Filter, mcoFilter: Filter): Filter = { (di, it) =>
    val n = Nest.axes.contains(di)
    val m = MonadCO.axes.contains(di)

    val nf = n && nestFilter(di, it)
    val mf = m && mcoFilter(di, it)

    (nf || mf) && ((n && m) -> (nf && mf))
  }

  def *[P <: Conf](using `mcoWildcard*`: mco.Wildcard[P])(using params: P) =
    new Wildcard[P]()

  def apply[
    P <: Conf
  ](
    nestWildcard: version.less.nest.Wildcard,
    mcoWildcard: mco.Wildcard[P]
  )(using
    params: P
  ): Wildcard[P] =
    new Wildcard[P](nestWildcard, mcoWildcard)

  def apply[
    P <: Conf
  ](using params: P)(filter: Filter, wildcard: Wildcard[P] = *[P](using mco.Wildcard.*[P])): Wildcard[P] =
    Wildcard[P](
      version.less.nest.Wildcard(filter, wildcard._1),
      mco.Wildcard[P](filter, wildcard._2)
    )
