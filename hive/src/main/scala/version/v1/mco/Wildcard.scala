package queens
package version.v1
package mco

import dimensions.Dimension.{ Model, Interface, Use }
import dimensions.Dimension.Interface.Program
import dimensions.Dimension.Interface.Program.Monad
import dimensions.Dimension.Use.Output
import dimensions.Dimension.Use.Output.Console

import conf.emito.Conf


class Wildcard[
  P <: Conf
] protected (
  override protected[version] val filter: Wildcard.Filter
)(using
  protected val * : P
) extends version.base.Wildcard[MonadCO[P]]:

  override val axes = MonadCO.axes

  def this()(using params: P) = this(Wildcard.star)

  override protected def apply(factor: MonadCO[P] => Int): List[MonadCO[P]] =
    super.apply(factor, MonadCO.apply)

  override def equals(any: Any): Boolean = any match
    case that: Wildcard[P] => super.equals(any)
    case _ => false

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[Wildcard[P]]


object Wildcard:

  type Filter = version.base.Wildcard.Filter

  private val star: Filter = (_, _) match
    case (Model, _) | (Interface, Program(Monad)) | (Use, Output(Console)) => true
    case (_, _) => false

  def *[P <: Conf](using params: P) = new Wildcard[P]()

  def apply[
    P <: Conf
  ](using params: P)(filter: Filter, wildcard: Wildcard[P] = *[P]) =
    new Wildcard[P]({ (di, it) =>
      filter(di, it) && wildcard.filter(di, it)
    })

  def apply(monadCO: MonadCO[?]*): Filter = (_, _) match
    case (Model, it: Model) => monadCO.map(_.model).exists(it.==)
    case it => star(it._1, it._2)

  def apply(model: Model): Filter = (_, _) match
    case (Model, `model`) => true
    case it => star(it._1, it._2)
