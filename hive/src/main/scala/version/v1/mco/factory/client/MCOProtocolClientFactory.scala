package queens
package version.v1
package mco
package factory.client

import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.Protocol.Role


abstract trait MCOProtocolClientFactory[
  M >: Group <: Role,
  SM >: Group <: ServerRole
] {

  def apply(
    context: ActorContext[?],
    server: Actor[SM],
    args: Any*
  ): Actor[M]

}
