package queens
package version.v1
package mco
package factory.client.defer

import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.Role


abstract trait MCOProtocolClientDeferFactory[
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole
] {

  def apply(
    context: ActorContext[?],
    onBehalf: Actor[CFM],
    args: Any*
  ): Actor[M]

}
