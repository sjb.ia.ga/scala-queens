package queens
package version.v1
package mco
package factory.server.backend

import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.Role


abstract trait MCOProtocolServerBackendFactory[
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
] {

  def apply(
    context: ActorContext[?],
    graceTo: Actor[SDM],
    resultTo: Actor[CDM],
    args: Any*
  ): Actor[M]

}
