package queens
package version.v1
package mco
package factory.server

import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import version.less.plane.protocol.session.Protocol.{ Role => PlaneSessionRole }
import protocol.server.Protocol.Role


abstract trait MCOProtocolServerFactory[
  M >: Group <: Role,
  SM >: Group <: PlaneSessionRole
] {

  def apply(
    context: ActorContext[?],
    session: Actor[SM],
    args: Any*
  ): Actor[M]

}
