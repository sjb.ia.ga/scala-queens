package queens
package version.v1
package mco
package factory.server.defer

import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.Role


abstract trait MCOProtocolServerDeferFactory[
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole
] {

  def apply(
    context: ActorContext[?],
    readyTo: Actor[CDM],
    args: Any*
  ): Actor[M]

}
