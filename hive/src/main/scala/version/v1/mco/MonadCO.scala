package queens
package version.v1
package mco

import dimensions.Dimension
import dimensions.Dimension.{ Axes, Projection }
import dimensions.Dimension.{ Model, Interface, Use }

import conf.emito.Conf

import dimensions.third.Queens
import dimensions.fourth.fifth.breed.Spawn
import dimensions.fourth.fifth.breed.`Console'o`

import dimensions.fourth.fifth.QueensMonadOutput
import dimensions.fourth.fifth.breed.mco.QueensMonadConsoleOutput

import version.base.{ Plane, Vector }


object MonadCO:

  given Conversion[MonadCO[?], Spawn[?]] = _.patch()

  def apply[
    P <: Conf
  ](
    it: Projection
  )(using
    params: P
  ): MonadCO[P] =
    assert(it.size == 3)
    require(it(1) == Interface.monadProgram)
    require(it(2) == Use.consoleOutput)
    new MonadCO[P](it(0))

  val axes = Dimension(Model, Interface, Use)


case class MonadCO[
  P <: Conf
](
  model: Model
)(using
  protected val * : P
) extends Plane:

  override def axes: Axes = MonadCO.axes

  override def toVector: Vector =
    new Vector(null, null, model, interface, use)

  final val interface: Interface = Interface.monadProgram
  val use: Use = Use.consoleOutput

  protected abstract trait Patch:
    def apply(): Spawn[P]

  private object _patch
      extends Patch:
    override def apply(): Spawn[P] = `Console'o`[P](model)

  protected def patch: Patch = _patch

  override protected val str = List(
    s"model=$model",
    s"interface=$interface",
    s"use=$use"
  )

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[MonadCO[?]]
