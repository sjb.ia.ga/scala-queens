package queens
package version.v1
package mco
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.ServerMCOProtocolDefer
import base.interface.ServerMCOProtocolDeferIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.server.defer.Protocol.Role
import protocol.server.defer._


class ServerMCOProtocolDeferImpl[
  EC <: MCOServerDeferErrorCode,
  M >: Group <: Role,
  SBM >: Group <: ServerBackendRole,
  CDM >: Group <: ClientDeferRole,
  I <: ServerMCOProtocolDeferIfce[SBM, M, CDM]
](
  override protected val readyTo: Actor[CDM]
)(implicit
  override protected val errorCode: EC,
  override protected val interface: I
) extends ServerMCOProtocolDefer[EC, M, SBM, CDM, I] {

  class ServerMCODeferBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val secret: java.util.UUID
  ) extends ServerMCODeferBehavior[ServerMCODeferBehaviorImpl]

  private object ServerMCODeferInflater
      extends ServerMCODeferInflater[ServerMCODeferBehaviorImpl] {

    override def apply(em: Arguments): ServerMCODeferBehaviorImpl =
      new ServerMCODeferBehaviorImpl(em._1, em._2)

  }

  private object ServerMCODeferBehaviorImpl
      extends ObjectRolePlaneBehavior[ServerMCODeferBehaviorImpl, ServerMCODeferInflater.type] {

    override val inflater = ServerMCODeferInflater

  }

}

import factory.server.defer._

import protocol.client.defer.{ Protocol => Parameter }
import protocol.server.defer.{ Protocol => Interface }

object ServerMCOProtocolDeferImpl extends MCOProtocolServerDeferFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    readyTo: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import interface.ObjectServerMCOProtocolDeferIfce
      import server.defer.Implicits._

      new ServerMCOProtocolDeferImpl[
        MCOServerDeferErrorCode,
        Interface.Role,
        protocol.server.backend.Protocol.Role,
        Parameter.Role,
        ObjectServerMCOProtocolDeferIfce,
      ](
        readyTo
      ).ServerMCODeferBehaviorImpl(args*)

    }

}
