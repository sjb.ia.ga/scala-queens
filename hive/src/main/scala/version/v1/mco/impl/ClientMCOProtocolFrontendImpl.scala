package queens
package version.v1
package mco
package impl

import akka.actor.typed.scaladsl.ActorContext

import dimensions.fourth.fifth.breed.Spawn
import mco.MonadCO

import client.frontend.Apply

import base.ClientMCOProtocolFrontend

import protocol.base.Protocol.Group
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.Role
import protocol.client.frontend._


class ClientMCOProtocolFrontendImpl[
  EC <: MCOClientFrontendErrorCode,
  M >: Group <: Role,
  CM >: Group <: ClientRole
](
  override protected val client: Actor[CM],
)(implicit
  override protected val errorCode: EC
) extends ClientMCOProtocolFrontend[EC, M, CM] {

  class ClientMCOFrontendBehaviorImpl(
    override protected val end: version.base.Role => Boolean,
    _apply: Apply
  ) extends ClientMCOFrontendBehavior[ClientMCOFrontendBehaviorImpl] {

    override protected val apply = _apply

  }

  private object ClientMCOFrontendInflater
      extends ClientMCOFrontendInflater[ClientMCOFrontendBehaviorImpl] {

    override def apply(em: Arguments): ClientMCOFrontendBehaviorImpl =
      new ClientMCOFrontendBehaviorImpl(em._1, em._2)

  }

  private object ClientMCOFrontendBehaviorImpl
      extends ObjectRolePlaneBehavior[ClientMCOFrontendBehaviorImpl, ClientMCOFrontendInflater.type] {

    override val inflater = ClientMCOFrontendInflater

  }

}


import factory.client.frontend._

import protocol.client.{ Protocol => Parameter }
import protocol.client.frontend.{ Protocol => Interface }

object ClientMCOProtocolFrontendImpl extends MCOProtocolClientFrontendFactory[
  Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    client: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import mco.client.frontend.Implicits._

      new ClientMCOProtocolFrontendImpl[
        MCOClientFrontendErrorCode,
        Interface.Role,
        Parameter.Role
      ](
        client
      ).ClientMCOFrontendBehaviorImpl(args*)

    }

}
