package queens
package version.v1
package mco
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.MCOProtocolClient
import base.interface.MCOProtocolClientIfce

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.client.Protocol.Role
import protocol.client._


class MCOProtocolClientImpl[
  EC <: MCOClientErrorCode,
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole,
  CDM >: Group <: ClientDeferRole,
  SM >: Group <: ServerRole,
  I <: MCOProtocolClientIfce[CDM, CFM]
](
  override protected val server: Actor[SM]
)(implicit
  override protected val errorCode: EC,
  override protected val interface: I
) extends MCOProtocolClient[EC, M, CFM, CDM, SM, I] {

  class ClientMCOBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val end: version.base.Role => Boolean
  ) extends ClientMCOBehavior[ClientMCOBehaviorImpl]

  private object ClientMCOInflater
      extends ClientMCOInflater[ClientMCOBehaviorImpl] {

    override def apply(em: Arguments): ClientMCOBehaviorImpl =
      new ClientMCOBehaviorImpl(em._1, em._2)

  }

  private object ClientMCOBehaviorImpl
      extends ObjectRolePlaneBehavior[ClientMCOBehaviorImpl, ClientMCOInflater.type] {

    override val inflater = ClientMCOInflater

  }

}


import factory.client._

import protocol.server.{ Protocol => Parameter }
import protocol.client.{ Protocol => Interface }

object MCOProtocolClientImpl extends MCOProtocolClientFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    server: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import interface.ObjectMCOProtocolClientIfce
      import client.Implicits._

      new MCOProtocolClientImpl[
        MCOClientErrorCode,
        Interface.Role,
        protocol.client.frontend.Protocol.Role,
        protocol.client.defer.Protocol.Role,
        Parameter.Role,
        ObjectMCOProtocolClientIfce
      ](
        server
      ).ClientMCOBehaviorImpl(args*)
    }

}
