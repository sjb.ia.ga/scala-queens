package queens
package version.v1
package mco
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.ClientMCOProtocolDefer

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.client.defer.Protocol.Role
import protocol.client.defer._


class ClientMCOProtocolDeferImpl[
  EC <: MCOClientDeferErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  SDM >: Group <: ServerDeferRole,
  SBM >: Group <: ServerBackendRole,
  CFM >: Group <: ClientFrontendRole
](
  override protected val onBehalf: Actor[CFM]
)(implicit
  override protected val errorCode: EC
) extends ClientMCOProtocolDefer[EC, M, SM, SDM, SBM, CFM] {

  class ClientMCODeferBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val monadCO: MonadCO[?]
  ) extends ClientMCODeferBehavior[ClientMCODeferBehaviorImpl]

  private object ClientMCODeferInflater
      extends ClientMCODeferInflater[ClientMCODeferBehaviorImpl] {

    override def apply(em: Arguments): ClientMCODeferBehaviorImpl =
      new ClientMCODeferBehaviorImpl(em._1, em._2)

  }

  private object ClientMCODeferBehaviorImpl
      extends ObjectRolePlaneBehavior[ClientMCODeferBehaviorImpl, ClientMCODeferInflater.type] {

    override val inflater = ClientMCODeferInflater

  }

}


import factory.client.defer._

import protocol.client.frontend.{ Protocol => Parameter }
import protocol.client.defer.{ Protocol => Interface }

object ClientMCOProtocolDeferImpl extends MCOProtocolClientDeferFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    onBehalf: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import client.defer.Implicits._

      new ClientMCOProtocolDeferImpl[
        MCOClientDeferErrorCode,
        Interface.Role,
        protocol.server.Protocol.Role,
        protocol.server.defer.Protocol.Role,
        protocol.server.backend.Protocol.Role,
        Parameter.Role
      ](
        onBehalf
      ).ClientMCODeferBehaviorImpl(args*)

    }

}
