package queens
package version.v1
package mco
package impl

import base.MCOProtocolServer
import base.interface.MCOProtocolServerIfce

import protocol.base.Protocol.Group
import version.less.plane.protocol.session.Protocol.{ Role => PlaneSessionRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.Protocol.Role
import protocol.server._


class MCOProtocolServerImpl[
  EC <: MCOServerErrorCode,
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole,
  SDM >: Group <: ServerDeferRole,
  SM >: Group <: PlaneSessionRole,
  I <: MCOProtocolServerIfce[SDM, CDM]
](
  override protected val session: Actor[SM]
)(implicit
  override protected val errorCode: EC,
  override protected val interface: I
) extends MCOProtocolServer[EC, M, CDM, SDM, SM, I] {

  class ServerMCOBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val end: version.base.Role => Boolean
  ) extends ServerMCOBehavior[ServerMCOBehaviorImpl]

  private[impl] object ServerMCOInflater
      extends ServerMCOInflater[ServerMCOBehaviorImpl] {

    override def apply(em: Arguments): ServerMCOBehaviorImpl =
      new ServerMCOBehaviorImpl(em._1, em._2)

  }

  private[impl] object ServerMCOBehaviorImpl
      extends ObjectRolePlaneBehavior[ServerMCOBehaviorImpl, ServerMCOInflater.type] {

    override val inflater = ServerMCOInflater

  }

}


import interface.ObjectMCOProtocolServerIfce
import server.Implicits._

object MCOProtocolServerImpl extends MCOProtocolServerFactoryImpl[
  Role,
  ClientDeferRole,
  ServerDeferRole,
  PlaneSessionRole,
  ObjectMCOProtocolServerIfce
]
