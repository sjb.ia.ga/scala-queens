package queens
package version.v1
package mco
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.interface.MCOProtocolServerIfce

import protocol.base.Protocol.Group
import version.less.plane.protocol.session.Protocol.{ Role => PlaneSessionRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.Protocol.Role
import protocol.server._

import factory.server._

class MCOProtocolServerFactoryImpl[
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole,
  SDM >: Group <: ServerDeferRole,
  SM >: Group <: PlaneSessionRole,
  I <: MCOProtocolServerIfce[SDM, CDM]
](
  implicit val interface: I
) extends MCOProtocolServerFactory[M, SM] {

  override def apply(
    context: ActorContext[?],
    session: Actor[SM],
    args: Any*
  ): Actor[M] = context
    .spawnAnonymous {
      import server.Implicits._

      new MCOProtocolServerImpl[
        MCOServerErrorCode,
        M,
        CDM,
        SDM,
        SM,
        I
      ](
        session
      ).ServerMCOBehaviorImpl(args*)
    }

}
