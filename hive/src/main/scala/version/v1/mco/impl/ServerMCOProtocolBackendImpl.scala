package queens
package version.v1
package mco
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.ServerMCOProtocolBackend

import protocol.base.Protocol.Group
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.Role
import protocol.server.backend._


class ServerMCOProtocolBackendImpl[
  EC <: MCOServerBackendErrorCode,
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
](
  override protected val graceTo: Actor[SDM],
  override protected val resultTo: Actor[CDM]
)(implicit
  override protected val errorCode: EC
) extends ServerMCOProtocolBackend[EC, M, SDM, CDM] {

  class ServerMCOBackendBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val secret: java.util.UUID
  ) extends ServerMCOBackendBehavior[ServerMCOBackendBehaviorImpl]

  private object ServerMCOBackendInflater
      extends ServerMCOBackendInflater[ServerMCOBackendBehaviorImpl]:
    override def apply(em: Arguments): ServerMCOBackendBehaviorImpl =
      new ServerMCOBackendBehaviorImpl(em._1, em._2)

  private object ServerMCOBackendBehaviorImpl
      extends ObjectRolePlaneBehavior[ServerMCOBackendBehaviorImpl, ServerMCOBackendInflater.type]:
    override val inflater = ServerMCOBackendInflater

}


import factory.server.backend._

import protocol.server.defer.{ Protocol => Parameter1 }
import protocol.client.defer.{ Protocol => Parameter2 }
import protocol.server.backend.{ Protocol => Interface }

object ServerMCOProtocolBackendImpl extends MCOProtocolServerBackendFactory[
  Role,
  Parameter1.Role,
  Parameter2.Role
] {

  override def apply(
    context: ActorContext[?],
    graceTo: Parameter1.Actor,
    resultTo: Parameter2.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import server.backend.Implicits._

      new ServerMCOProtocolBackendImpl[
        MCOServerBackendErrorCode,
        Role,
        Parameter1.Role,
        Parameter2.Role
      ](
        graceTo,
        resultTo
      ).ServerMCOBackendBehaviorImpl(args*)

    }

}
