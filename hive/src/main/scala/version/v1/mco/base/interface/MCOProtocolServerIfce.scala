package queens
package version.v1
package mco
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.Role


abstract trait MCOProtocolServerIfce[
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole
] extends NodeIfce {

  protected abstract trait ObjectPlaneMonadCO {

    def apply(
      ctx: ActorContext[?],
      readyTo: Actor[CDM],
      args: Any*
    ): Actor[M]

  }

  def PlaneMonadCO: ObjectPlaneMonadCO

}
