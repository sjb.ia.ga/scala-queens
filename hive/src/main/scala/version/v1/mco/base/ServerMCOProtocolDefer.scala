package queens
package version.v1
package mco
package base

import akka.actor.typed.scaladsl.ActorContext

import version.less.plane.base.ServerPlaneProtocolDefer
import interface.ServerMCOProtocolDeferIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.server.defer.Protocol.Role
import protocol.server.defer._


abstract trait ServerMCOProtocolDefer[
  EC <: MCOServerDeferErrorCode,
  M >: Group <: Role,
  SBM >: Group <: ServerBackendRole,
  CDM >: Group <: ClientDeferRole,
  I <: ServerMCOProtocolDeferIfce[SBM, M, CDM]
] extends MCOServerDeferProtocol
    with ServerPlaneProtocolDefer[EC, M, CDM, I] {

  protected abstract trait ServerMCODeferBehavior[T <: ServerMCODeferBehavior[T]]
      extends ServerPlaneDeferBehavior[T] {

    import version.less.plane.protocol.message.resume.{ ResumeProtocol => rm }
    import version.less.plane.protocol.message.grace.{ GraceProtocol => gm }

    import errorCode._

    protected def mco_errorBehavior(replyTo: Actor[CDM], msg: M): Nothing = msg match {

      case rm.Resume(axes) if axes != mco.MonadCO.axes =>
        replyTo ! invalidPlane(axes)
        stopped


      case gm.Grace(axes) if axes != mco.MonadCO.axes =>
        replyTo ! invalidPlane(axes)
        stopped


      case _ =>
        none

    }

    protected def mco_perceive(ctx: ActorContext[M], msg: M): Nothing = msg match {

      case rm.Resume(axes) if axes == mco.MonadCO.axes =>
        val self: Actor[M] = ctx.self

        val mcoAt: Actor[SBM] = interface
          .ResumeMonadCO(ctx, graceTo = self, resultTo = readyTo, cryptoKey, secret)

        readyTo ! Ready(mcoAt, MonadCO(_))
        same


      case gm.Grace(axes) if axes == mco.MonadCO.axes =>
        stopped


      case _ =>
        none

    }

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        mco_errorBehavior(readyTo, msg)
      } catch {
        case NoneBehavior =>
          try {
            super.errorBehavior(readyTo, msg)
          } catch {
            case NoneBehavior =>
              try {
                mco_perceive(ctx, msg)
              } catch {
                case NoneBehavior =>
                  readyTo ! Complaint(msg)
                  stopped
              }
          }
      }

  }

  protected abstract trait ServerMCODeferInflater[T <: ServerMCODeferBehavior[T]]
      extends RolePlaneInflater[T]
      with MCODeseqServerDefer

}
