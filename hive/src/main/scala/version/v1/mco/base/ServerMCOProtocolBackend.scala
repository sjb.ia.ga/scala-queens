package queens
package version.v1
package mco
package base

import akka.actor.typed.scaladsl.ActorContext

import version.less.plane.base.ServerPlaneProtocolBackend

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.Role
import protocol.server.backend._


abstract trait ServerMCOProtocolBackend[
  EC <: MCOServerBackendErrorCode,
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
] extends MCOServerBackendProtocol
    with ServerPlaneProtocolBackend[EC, M, SDM, CDM]:

  protected abstract trait ServerMCOBackendBehavior[T <: ServerMCOBackendBehavior[T]]
      extends ServerPlaneBackendBehavior[T]:

    import version.v1.mco.protocol.message.salute.{ MonadCOProtocol => mm }
    import version.v1.mco.protocol.message.breed.{ SpawnProtocol => sm }

    import errorCode._

    protected def mco_perceive_salute(ctx: ActorContext[M], msg: M): Nothing = msg match

      case mm.MonadCO(it) =>
        if saluted
        then
          resultTo ! dupMonadCO
        else
          saluted = true

          if it == secret
          then
            same
          else
            resultTo ! badSecret

        stopped

      case _ =>
        none


    protected def mco_perceive(ctx: ActorContext[M], msg: M): Nothing = msg match

      case sm.Spawn(monadCO) =>
        import mco.MonadCO.given
        val spawn: dimensions.fourth.fifth.breed.Spawn[?] = monadCO

        resultTo ! Result(spawn)
        graceTo ! Grace(mco.MonadCO.axes)
        stopped


      case _ =>
        none


    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try
        mco_perceive_salute(ctx, msg)
      catch
        case NoneBehavior =>
          try
            super.errorBehavior(resultTo, msg)
          catch
            case NoneBehavior =>
              try
                mco_perceive(ctx, msg)
              catch
                case NoneBehavior =>
                  resultTo ! Complaint(msg)
                  same


  protected abstract trait ServerMCOBackendInflater[T <: ServerMCOBackendBehavior[T]]
      extends RolePlaneInflater[T]
      with MCODeseqServerBackend
