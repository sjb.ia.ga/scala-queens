package queens
package version.v1
package mco
package base

import java.util.UUID

import akka.actor.typed.scaladsl.ActorContext

import mco.MonadCO

import common.cipher.client.CipherClient

import version.less.plane.base.ClientPlaneProtocolDefer

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.client.defer.Protocol.Role
import protocol.client.defer._


abstract trait ClientMCOProtocolDefer[
  EC <: MCOClientDeferErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  SDM >: Group <: ServerDeferRole,
  SBM >: Group <: ServerBackendRole,
  CFM >: Group <: ClientFrontendRole
] extends MCOClientDeferProtocol
    with ClientPlaneProtocolDefer[EC, M, CFM] {

  protected abstract trait ClientMCODeferBehavior[T <: ClientMCODeferBehavior[T]]
      extends ClientPlaneDeferBehavior[T] {

    protected val monadCO: MonadCO[?]

    import version.less.plane.protocol.message.defer.{ DeferProtocol => dm }
    import version.less.plane.protocol.message.plane.{ PlaneProtocol => pm }
    import version.less.plane.protocol.message.ready.{ ReadyProtocol => ym }
    import version.less.plane.protocol.message.result.{ ResultProtocol => rm }

    import errorCode._

    protected def mco_errorBehavior(replyTo: Actor[CFM], msg: M): Nothing = msg match {

      case dm.Defer(_, pm.Plane(_, axes), _) if axes != MonadCO.axes =>
        replyTo ! invalidPlane(axes)
        stopped


      case _ =>
        none

    }

    protected def mco_perceive(ctx: ActorContext[M], msg: M): Nothing = msg match {

      case dm.Defer(resumeAt: Actor[SDM], pm.Plane(_, axes), it) if axes == MonadCO.axes =>
        CipherClient(cryptoKey, it) match
          case Some(uuid) =>
            secret = uuid
            resumeAt ! Resume(axes)
            same
          case _ =>
            resumeAt ! errorSecret(role)
            stopped


      case ym.Ready(mcoAt: Actor[SBM], hiMessage: Function[UUID, SBM] @unchecked) =>
        mcoAt ! hiMessage(secret)
        mcoAt ! Spawn(monadCO)
        same


      case rm.Result(spawn: dimensions.fourth.fifth.breed.Spawn[?]) =>
        import dimensions.fourth.fifth.breed.given
        onBehalf ! Append(monadCO, spawn)
        stopped


      case _ =>
        none

    }

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        mco_errorBehavior(onBehalf, msg)
      } catch {
        case NoneBehavior =>
          try {
            super.errorBehavior(onBehalf, msg)
          } catch {
            case NoneBehavior =>
              try {
                mco_perceive(ctx, msg)
              } catch {
                case NoneBehavior =>
                  onBehalf ! Complaint(msg)
                  stopped
              }
          }
      }

  }

  protected abstract trait ClientMCODeferInflater[T <: ClientMCODeferBehavior[T]]
      extends RolePlaneInflater[T]
      with MCODeseqClientDefer

}
