package queens
package version.v1
package mco
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.Role


abstract trait MCOProtocolClientIfce[
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole
] extends NodeIfce {

  protected abstract trait ObjectBreedMonadCO {

    def apply(
      ctx: ActorContext[?],
      onBehalf: Actor[CFM],
      args: Any*
    ): Actor[M]

  }

  def BreedMonadCO: ObjectBreedMonadCO

}
