package queens
package version.v1
package mco
package base

import akka.actor.typed.scaladsl.ActorContext

import version.less.plane.base.ClientPlaneProtocolFrontend

import protocol.base.Protocol.Group
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.Role
import protocol.client.frontend._


abstract trait ClientMCOProtocolFrontend[
  EC <: MCOClientFrontendErrorCode,
  M >: Group <: Role,
  CM >: Group <: ClientRole
] extends MCOClientFrontendProtocol
    with ClientPlaneProtocolFrontend[EC, M, CM] {

  protected abstract trait ClientMCOFrontendBehavior[T <: ClientMCOFrontendBehavior[T]]
      extends ClientPlaneFrontendBehavior[T] {

    import dimensions.fourth.fifth.breed.Spawn
    import mco.{ MonadCO, Wildcard }
    import version.base.Multiplier

    private var count = 0

    import version.less.plane.protocol.message.expand.{ ExpandProtocol => em }
    import version.less.plane.protocol.message.append.{ AppendProtocol => am }

    import errorCode._

    protected def mco_perceive(ctx: ActorContext[M], msg: M): Nothing = msg match {

      case em.Expand(wildcard: Wildcard[?], Multiplier(factor)) =>
        val self: Actor[M] = ctx.self

        val ns = for {
          it <- wildcard(factor)
        } yield it

        assert(count == 0)

        for (it <- ns) {
          client ! Breed(it, onBehalf = self)
          count += 1
        }

        if count == 0 then
          client ! Grace(wildcard.axes)

          if end(role) then stopped

        same


      case am.Append(monadCO: MonadCO[?], spawn: Spawn[?]) if count > 0 =>
        count -= 1

        apply(monadCO, spawn)

        if count == 0 then
          client ! Grace(monadCO.axes)

          if end(role) then stopped

        same


      case am.Append(_: MonadCO[?], _: Spawn[?]) =>
        client ! overflow
        stopped


      case _ =>
        none

    }

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        super.errorBehavior(client, msg)
      } catch {
        case NoneBehavior =>
          try {
            mco_perceive(ctx, msg)
          } catch {
            case NoneBehavior =>
              try {
                super.errorBehavior(client, msg, after = true)
              } catch {
                case NoneBehavior =>
                  client ! Complaint(msg)
                  same
              }
          }
      }

  }

  protected abstract trait ClientMCOFrontendInflater[T <: ClientMCOFrontendBehavior[T]]
      extends RolePlaneInflater[T]
      with MCODeseqClientFrontend

}
