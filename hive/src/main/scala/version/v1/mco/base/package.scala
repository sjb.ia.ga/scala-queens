package queens
package version.v1
package mco


package object base {

  import version.base.{ `Deseq*`, `DeseqPlane*`, DeseqUUID }

  type MCODeseqClient = `Deseq*`

  type MCODeseqClientDefer = `DeseqPlane*`[MonadCO[?]]

  type MCODeseqServer = `Deseq*`

  type MCODeseqServerDefer = DeseqUUID

  type MCODeseqServerBackend = DeseqUUID

}
