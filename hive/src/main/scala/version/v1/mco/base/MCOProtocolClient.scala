package queens
package version.v1
package mco
package base

import akka.actor.typed.scaladsl.ActorContext

import mco.MonadCO

import version.less.plane.base.PlaneProtocolClient
import interface.MCOProtocolClientIfce

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.client.Protocol.Role
import protocol.client._


abstract trait MCOProtocolClient[
  EC <: MCOClientErrorCode,
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole,
  CDM >: Group <: ClientDeferRole,
  SM >: Group <: ServerRole,
  I <: MCOProtocolClientIfce[CDM, CFM]
] extends MCOClientProtocol
    with PlaneProtocolClient[EC, M, SM, I] {

  protected abstract trait ClientMCOBehavior[T <: ClientMCOBehavior[T]]
      extends ClientPlaneBehavior[T] {

    import version.less.plane.protocol.message.breed.{ BreedProtocol => bm }
    import version.less.plane.protocol.message.grace.{ GraceProtocol => gm }

    import errorCode._

    protected def mco_perceive(ctx: ActorContext[M], msg: M): Nothing = msg match {

      case bm.Breed(monadCO: MonadCO[?], onBehalf: Actor[CFM]) =>
        val deferTo: Actor[CDM] = interface
          .BreedMonadCO(ctx, onBehalf, cryptoKey, monadCO)

        server ! Plane(deferTo, monadCO.axes)
        same


      case gm.Grace(axes) if axes == MonadCO.axes =>
        server ! Grace(axes)

        if end(role) then stopped

        same


      case _ =>
        none

    }

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        super.errorBehavior(server, msg)
      } catch {
        case NoneBehavior =>
          try {
            mco_perceive(ctx, msg)
          } catch {
            case NoneBehavior =>
              try {
                super.errorBehavior(server, msg, after = true)
              } catch {
                case NoneBehavior =>
                  server ! Complaint(msg)
                  same
              }
          }
      }

  }

  protected abstract trait ClientMCOInflater[T <: ClientMCOBehavior[T]]
      extends RolePlaneInflater[T]
      with MCODeseqClient

}
