package queens
package version.v1
package mco
package base

import akka.actor.typed.scaladsl.ActorContext

import mco.MonadCO

import common.cipher.client.CipherClient

import version.less.plane.base.PlaneProtocolServer
import interface.MCOProtocolServerIfce

import protocol.base.Protocol.Group
import version.less.plane.protocol.session.Protocol.{ Role => SessionRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.Protocol.Role
import protocol.server._


abstract trait MCOProtocolServer[
  EC <: MCOServerErrorCode,
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole,
  SDM >: Group <: ServerDeferRole,
  SM >: Group <: SessionRole,
  I <: MCOProtocolServerIfce[SDM, CDM]
] extends MCOServerProtocol
    with PlaneProtocolServer[EC, M, SM, I] {

  protected abstract trait ServerMCOBehavior[T <: ServerMCOBehavior[T]]
      extends ServerPlaneBehavior[T] {

    import version.less.plane.protocol.message.plane.{ PlaneProtocol => pm }
    import version.less.plane.protocol.message.grace.{ GraceProtocol => gm }

    import errorCode._

    protected def mco_perceive(ctx: ActorContext[M], msg: M): Nothing = msg match {

      case pm.Plane(deferTo: Actor[CDM], axes) if axes == MonadCO.axes =>
        val secret = java.util.UUID.randomUUID

        val resumeAt: Actor[SDM] = interface
          .PlaneMonadCO(ctx, readyTo = deferTo, cryptoKey, secret)

        CipherClient(cryptoKey, secret) match
          case Some(it) =>
            deferTo ! Defer(resumeAt, msg, it)
            same
          case _ =>
            deferTo ! errorSecret(role)
            stopped


      case gm.Grace(axes) if axes == MonadCO.axes =>
        session ! Stop()
        if end(role) then stopped
        same


      case _ =>
        none

    }

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        super.errorBehavior(session, msg)
      } catch {
        case NoneBehavior =>
          try {
            mco_perceive(ctx, msg)
          } catch {
            case NoneBehavior =>
              try {
                super.errorBehavior(session, msg, after = true)
              } catch {
                case NoneBehavior =>
                  session ! Complaint(msg)
                  same
              }
          }
      }

  }

  protected abstract trait ServerMCOInflater[T <: ServerMCOBehavior[T]]
      extends RolePlaneInflater[T]
      with MCODeseqServer

}
