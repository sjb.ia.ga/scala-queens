package queens
package version.v1
package mco
package interface

import akka.actor.typed.scaladsl.ActorContext

import base.interface.MCOProtocolClientIfce

import protocol.client.frontend.{ Protocol => Parameter }
import protocol.client.defer.{ Protocol => Interface }


/**
  * @see [[queens.version.v1.mco.client.Implicits.MCOProtocolClientIfceImplicit]]
  */
abstract trait ObjectMCOProtocolClientIfce extends MCOProtocolClientIfce[
  Interface.Role,
  Parameter.Role
] { interface =>

  override object BreedMonadCO
      extends ObjectBreedMonadCO {

    override def apply(
      ctx: ActorContext[?],
      onBehalf: Parameter.Actor,
      args: Any*
    ): Interface.Actor = {
      import factory.client.defer._

      type ClientDeferFactory = MCOProtocolClientDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(classOf[ClientDeferFactory])(ctx, onBehalf, args*)
    }

  }

}
