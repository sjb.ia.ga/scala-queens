package queens
package version.v1
package mco
package interface

import akka.actor.typed.scaladsl.ActorContext

import base.interface.MCOProtocolServerIfce

import protocol.client.defer.{ Protocol => Parameter }
import protocol.server.defer.{ Protocol => Interface }

/**
  * @see [[queens.version.v1.mco.server.Implicits.MCOProtocolServerIfceImplicit]]
  */
abstract trait ObjectMCOProtocolServerIfce extends MCOProtocolServerIfce[
  Interface.Role,
  Parameter.Role
] { interface =>

  override object PlaneMonadCO
      extends ObjectPlaneMonadCO {

    override def apply(
      ctx: ActorContext[?],
      readyTo: Parameter.Actor,
      args: Any*
    ): Interface.Actor = {
      import factory.server.defer._

      type ServerDeferFactory = MCOProtocolServerDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(classOf[ServerDeferFactory])(ctx, readyTo, args*)
    }

  }

}
