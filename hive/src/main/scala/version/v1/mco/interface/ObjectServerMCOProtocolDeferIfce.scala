package queens
package version.v1
package mco
package interface

import akka.actor.typed.scaladsl.ActorContext

import base.interface.ServerMCOProtocolDeferIfce

import protocol.server.defer.{ Protocol => Parameter1 }
import protocol.client.defer.{ Protocol => Parameter2 }
import protocol.server.backend.{ Protocol => Interface }


/**
  * @see [[queens.version.v1.mco.server.defer.Implicits.ServerMCOProtocolDeferIfceImplicit]]
  */
abstract trait ObjectServerMCOProtocolDeferIfce extends ServerMCOProtocolDeferIfce[
  Interface.Role,
  Parameter1.Role,
  Parameter2.Role
] { interface =>

  override object ResumeMonadCO
      extends ObjectResumeMonadCO {

    override def apply(
      ctx: ActorContext[?],
      graceTo: Parameter1.Actor,
      resultTo: Parameter2.Actor,
      args: Any*
    ): Interface.Actor = {
      import factory.server.backend._

      type ServerBackendFactory = MCOProtocolServerBackendFactory[
        Interface.Role,
        Parameter1.Role,
        Parameter2.Role
      ]

      interface(classOf[ServerBackendFactory])(ctx, graceTo, resultTo, args*)
    }

  }

}
