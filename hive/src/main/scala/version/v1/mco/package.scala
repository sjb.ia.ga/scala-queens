package queens
package version.v1
package mco


package client {

  package object frontend {

    import mco.MonadCO
    import dimensions.fourth.fifth.breed.Spawn

    import conf.emito.Conf

    type Apply = (MonadCO[?], Spawn[?]) => Unit

  }

}


package server {

  object Implicits {

    implicit object MCOServerErrorCodeImplicit
        extends protocol.server.MCOServerErrorCode

    import interface.ObjectMCOProtocolServerIfce

    implicit object MCOProtocolServerIfceImplicit
        extends ObjectMCOProtocolServerIfce { interface =>

      import factory.server.defer._

      import protocol.client.defer.{ Protocol => Parameter }
      import protocol.server.defer.{ Protocol => Interface }

      type ServerDeferFactory = MCOProtocolServerDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(
        classOf[ServerDeferFactory],
        impl.ServerMCOProtocolDeferImpl
      )

    }

  }

  package defer {

    object Implicits {

      implicit object MCOServerDeferErrorCodeImplicit
          extends protocol.server.defer.MCOServerDeferErrorCode

      import interface.ObjectServerMCOProtocolDeferIfce

      implicit object ServerMCOProtocolDeferIfceImplicit
          extends ObjectServerMCOProtocolDeferIfce { interface =>

        import factory.server.backend._

        import protocol.server.defer.{ Protocol => Parameter1 }
        import protocol.client.defer.{ Protocol => Parameter2 }
        import protocol.server.backend.{ Protocol => Interface }

        type ServerBackendFactory = MCOProtocolServerBackendFactory[
          Interface.Role,
          Parameter1.Role,
          Parameter2.Role
        ]

        interface(
          classOf[ServerBackendFactory],
          impl.ServerMCOProtocolBackendImpl
        )

      }

    }

  }

  package backend {

    object Implicits {

      implicit object MCOServerBackendErrorCodeImplicit
          extends protocol.server.backend.MCOServerBackendErrorCode

    }

  }

}


package client {

  object Implicits {

    implicit object MCOClientErrorCodeImplicit
        extends protocol.client.MCOClientErrorCode

    import interface.ObjectMCOProtocolClientIfce

    implicit object MCOProtocolClientIfceImplicit
        extends ObjectMCOProtocolClientIfce { interface =>

      import factory.client.defer._

      import protocol.client.frontend.{ Protocol => Parameter }
      import protocol.client.defer.{ Protocol => Interface }

      type ClientDeferFactory = MCOProtocolClientDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(
        classOf[ClientDeferFactory],
        impl.ClientMCOProtocolDeferImpl
      )

    }

  }

  package defer {

    object Implicits {

      implicit object MCOClientDeferErrorCodeImplicit
          extends protocol.client.defer.MCOClientDeferErrorCode

    }

  }

  package frontend {

    object Implicits {

      implicit object MCOClientFrontendErrorCodeImplicit
          extends protocol.client.frontend.MCOClientFrontendErrorCode

    }

  }

}
