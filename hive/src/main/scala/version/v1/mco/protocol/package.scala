package queens
package version.v1
package mco.protocol

import version.v1.mco.{ Actor => MCOActor }


abstract trait MCOMessage
    extends version.less.plane.protocol.session.Protocol.Role
    with version.v3.mco.protocol.session.Protocol.Role
    with client.Protocol.Role
    with client.defer.Protocol.Role
    with client.frontend.Protocol.Role
    with server.Protocol.Role
    with server.defer.Protocol.Role
    with server.backend.Protocol.Role


package base {

  abstract trait MCOGroup
      extends MCOMessage
      with vector.protocol.base.Protocol.Group
      with message.salute.Protocol.Match
      with message.salute.Protocol.Ready.Salute.Match
      with message.breed.Protocol.Match

  object Protocol {

    type Group = MCOGroup

  }

}


package client {

  import version.less.plane.protocol.client.Protocol.{ Role => ClientRole }

  abstract trait MCOClientRole
      extends ClientRole

  object Protocol {

    type Role = MCOClientRole
    private[mco] type Actor = MCOActor[Role]

  }

  package defer {

    import version.less.plane.protocol.client.defer.Protocol.{ Role => ClientDeferRole }

    abstract trait MCOClientDeferRole
        extends ClientDeferRole

    object Protocol {

      type Role = MCOClientDeferRole
      private[mco] type Actor = MCOActor[Role]

    }

  }

  package frontend {

    import version.less.plane.protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }

    abstract trait MCOClientFrontendRole
        extends ClientFrontendRole

    object Protocol {

      type Role = MCOClientFrontendRole
      private[version] type Actor = MCOActor[Role]

    }

  }

}

package server {

  import version.less.plane.protocol.server.Protocol.{ Role => ServerRole }

  abstract trait MCOServerRole
      extends ServerRole

  object Protocol {

    type Role = MCOServerRole
    private[mco] type Actor = MCOActor[Role]

  }

  package defer {

    import version.less.plane.protocol.server.defer.Protocol.{ Role => ServerDeferRole }

    abstract trait MCOServerDeferRole
        extends ServerDeferRole

    object Protocol {

      type Role = MCOServerDeferRole
      private[mco] type Actor = MCOActor[Role]

    }

  }

  package backend {

    import version.less.plane.protocol.server.backend.Protocol.{ Role => ServerBackendRole }

    abstract trait MCOServerBackendRole
        extends ServerBackendRole

    object Protocol {

      type Role = MCOServerBackendRole
      private[mco] type Actor = MCOActor[Role]

    }

  }

}
