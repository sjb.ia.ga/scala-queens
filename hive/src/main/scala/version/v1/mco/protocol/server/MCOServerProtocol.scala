package queens
package version.v1
package mco.protocol
package server


import version.less.plane.protocol.server.PlaneServerProtocol

abstract trait MCOServerProtocol
    extends MCOProtocol
    with PlaneServerProtocol


import version.less.plane.protocol.server.PlaneServerErrorCode

abstract trait MCOServerErrorCode
    extends MCOErrorCode
    with PlaneServerErrorCode
