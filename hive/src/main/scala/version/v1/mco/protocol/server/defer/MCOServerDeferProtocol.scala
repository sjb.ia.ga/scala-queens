package queens
package version.v1
package mco.protocol
package server.defer


import defer.MCODeferProtocol
import version.less.plane.protocol.server.defer.PlaneServerDeferProtocol
import message.salute.{ MonadCOProtocol => MsgMonadCOProtocol }

abstract trait MCOServerDeferProtocol
    extends MCODeferProtocol
    with PlaneServerDeferProtocol
    with MsgMonadCOProtocol


import defer.MCODeferErrorCode
import version.less.plane.protocol.server.defer.PlaneServerDeferErrorCode
import message.salute.{ MonadCOErrorCode => MsgMonadCOErrorCode }

abstract trait MCOServerDeferErrorCode
    extends MCODeferErrorCode
    with PlaneServerDeferErrorCode
    with MsgMonadCOErrorCode
