package queens
package version.v1
package mco.protocol
package message


package salute {

  import server.defer.Protocol.{ Role => MCOServerDeferRole }

  import server.backend.Protocol.{ Role => MCOServerBackendRole }

  abstract trait MonadCOMatch
      extends MCOServerDeferRole
      with MCOServerBackendRole

  import vector.protocol.server.defer.Protocol.{ Role => VectorV1ServerDeferRole }

  import vector.protocol.server.backend.Protocol.{ Role => VectorV1ServerBackendRole }

  abstract trait MonadCOReadySaluteMatch
      extends MonadCOMatch
      with VectorV1ServerDeferRole
      with VectorV1ServerBackendRole

  import base.Protocol.{ Group => MCOGroup }

  abstract trait MonadCOMessage
      extends MCOGroup

  object Protocol {

    type Match = MonadCOMatch
    type Message = MonadCOMessage

    object Ready {

      object Salute {

        type Match = MonadCOReadySaluteMatch

      }

    }

  }

}

package breed {

  import client.defer.Protocol.{ Role => MCOClientDeferRole }

  import server.backend.Protocol.{ Role => MCOServerBackendRole }

  abstract trait SpawnMatch
      extends MCOClientDeferRole
      with MCOServerBackendRole

  import base.Protocol.{ Group => MCOGroup }

  abstract trait SpawnMessage
      extends MCOGroup

  object Protocol {

    type Match = SpawnMatch
    type Message = SpawnMessage

  }

}
