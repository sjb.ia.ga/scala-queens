package queens
package version.v1
package mco.protocol
package message.salute

import java.util.UUID


abstract trait MonadCOProtocol:

  import MonadCOProtocol._

  inline final def MonadCO(secret: UUID) = new MonadCO(secret)


object MonadCOProtocol:

  final case class MonadCO(secret: UUID)
      extends Protocol.Message


abstract trait MonadCOErrorCode
    extends MCOErrorCode
