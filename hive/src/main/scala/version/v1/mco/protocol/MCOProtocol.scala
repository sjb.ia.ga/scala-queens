package queens
package version.v1
package mco.protocol


import base.BaseMCOProtocol

abstract trait MCOProtocol
    extends BaseMCOProtocol


import base.BaseMCOErrorCode

abstract trait MCOErrorCode
    extends BaseMCOErrorCode
