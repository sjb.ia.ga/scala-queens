package queens
package version.v1
package mco.protocol
package defer


import version.less.plane.protocol.defer.PlaneDeferProtocol

abstract trait MCODeferProtocol
    extends MCOProtocol
    with PlaneDeferProtocol


import version.less.plane.protocol.defer.PlaneDeferErrorCode

abstract trait MCODeferErrorCode
    extends MCOErrorCode
    with PlaneDeferErrorCode

