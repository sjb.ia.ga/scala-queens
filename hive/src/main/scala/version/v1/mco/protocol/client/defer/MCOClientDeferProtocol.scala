package queens
package version.v1
package mco.protocol
package client.defer


import defer.MCODeferProtocol
import version.less.plane.protocol.client.defer.PlaneClientDeferProtocol
import message.breed.{ SpawnProtocol => MsgSpawnProtocol }

abstract trait MCOClientDeferProtocol
    extends MCODeferProtocol
    with PlaneClientDeferProtocol
    with MsgSpawnProtocol


import defer.MCODeferErrorCode
import version.less.plane.protocol.client.defer.PlaneClientDeferErrorCode
import message.breed.{ SpawnErrorCode => MsgSpawnErrorCode }

abstract trait MCOClientDeferErrorCode
    extends MCODeferErrorCode
    with PlaneClientDeferErrorCode
    with MsgSpawnErrorCode
