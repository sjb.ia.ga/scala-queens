package queens
package version.v1
package mco.protocol
package message.breed

import mco.MonadCO


abstract trait SpawnProtocol
    extends MCOProtocol:

  import SpawnProtocol._

  inline final def Spawn(mco: MonadCO[?]) = new Spawn(mco)


object SpawnProtocol:

  final case class Spawn(
    mco: MonadCO[?]
  ) extends Protocol.Message


abstract trait SpawnErrorCode
    extends MCOErrorCode
