package queens
package version.v1
package mco.protocol
package client.frontend


import version.less.plane.protocol.client.frontend.PlaneClientFrontendProtocol

abstract trait MCOClientFrontendProtocol
    extends MCOProtocol
    with PlaneClientFrontendProtocol


import version.less.plane.protocol.client.frontend.PlaneClientFrontendErrorCode

abstract trait MCOClientFrontendErrorCode
    extends MCOErrorCode
    with PlaneClientFrontendErrorCode
