package queens
package version.v1
package mco.protocol
package base


import version.base.protocol.BaseProtocol

abstract trait BaseMCOProtocol
    extends BaseProtocol


import version.base.protocol.BaseErrorCode

abstract trait BaseMCOErrorCode
    extends BaseErrorCode
