package queens
package version.v1
package mco.protocol
package server.backend


import version.less.plane.protocol.server.backend.PlaneServerBackendProtocol
import message.salute.{ MonadCOProtocol => MsgMonadCOProtocol }
import message.breed.{ SpawnProtocol => MsgSpawnProtocol }

abstract trait MCOServerBackendProtocol
    extends MCOProtocol
    with PlaneServerBackendProtocol
    with MsgMonadCOProtocol
    with MsgSpawnProtocol


import version.less.plane.protocol.server.backend.PlaneServerBackendErrorCode
import message.salute.{ MonadCOErrorCode => MsgMonadCOErrorCode }
import message.breed.{ SpawnErrorCode => MsgSpawnErrorCode }

abstract trait MCOServerBackendErrorCode
    extends MCOErrorCode
    with PlaneServerBackendErrorCode
    with MsgMonadCOErrorCode
    with MsgSpawnErrorCode {

  val dupMonadCO = ProtocolDupVal()

}
