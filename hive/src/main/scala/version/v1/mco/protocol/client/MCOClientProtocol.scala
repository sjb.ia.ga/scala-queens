package queens
package version.v1
package mco.protocol
package client


import version.less.plane.protocol.client.PlaneClientProtocol

abstract trait MCOClientProtocol
    extends MCOProtocol
    with PlaneClientProtocol


import version.less.plane.protocol.client.PlaneClientErrorCode

abstract trait MCOClientErrorCode
    extends MCOErrorCode
    with PlaneClientErrorCode
