package queens
package version.v0
package vector
package breed
package flow
package s

import java.util.UUID
import java.util.concurrent.{ ConcurrentHashMap => Map }

import breed.s.Hive

import common.Flag
import common.monad.Item

import conf.io.s.flow.Parameters


final class `Hatch'em, Spawn'it, I'o`(
  _em: Iterable[Hive[Parameters]],
  _tag: Any,
  _its: Seq[UUID] = UUID.randomUUID :: Nil
)(using
  override protected val * : Parameters
)(
  override protected val fs: *.Item => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)] = new Map()
) extends `Base.Hatch'em, Spawn'it, Output'o`[Parameters](_em, _tag, _its):

  override protected type * = `Hatch'em, Spawn'it, I'o`

  override def `#`(t: Any): * =
    new `Hatch'em, Spawn'it, I'o`(_em, t, _its)(fs*)

  override protected def `apply°`(s: *.Item => Boolean*): * =
    new `Hatch'em, Spawn'it, I'o`(_em, _tag, _its)(s*)

  override def toString(): String = "I" + super.toString


object `Hatch'em, Spawn'it, I'o`:

  import common.NoTag

  def apply(
    em: Iterable[Hive[Parameters]]
  )(using
    params: Parameters
  ): `Hatch'em, Spawn'it, Output'o`[Parameters] =
    new `Hatch'em, Spawn'it, I'o`(em, NoTag)()
