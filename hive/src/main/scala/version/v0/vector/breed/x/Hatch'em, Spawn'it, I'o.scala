package queens
package version.v0
package vector
package breed
package flow
package x

import java.util.UUID
import java.util.concurrent.{ ConcurrentHashMap => Map }

import monix.eval.{ Coeval, Task }

import breed.x.Hive

import common.Flag
import common.monad.Item


package sync:

  import conf.io.x.flow.sync.Parameters

  final class `Hatch'em, Spawn'it, I'o`(
    _em: Iterable[Hive[Coeval, Parameters]],
    _tag: Any,
    _its: Seq[UUID] = UUID.randomUUID :: Nil
  )(using
    override protected val * : Parameters
  )(
    override protected val fs: *.Item => Boolean*
  )(using
    `_its*`: Map[UUID, (Boolean, Flag)] = new Map()
  ) extends `Base.Hatch'em, Spawn'it, Output'o`[Coeval, Parameters](_em, _tag, _its):

    override protected type * = `Hatch'em, Spawn'it, I'o`

    override def `#`(t: Any): * =
      new `Hatch'em, Spawn'it, I'o`(_em, t, _its)(fs*)

    override protected def `apply°`(s: *.Item => Boolean*): * =
      new `Hatch'em, Spawn'it, I'o`(_em, _tag, _its)(s*)

    override def toString(): String = "I" + super.toString


  object `Hatch'em, Spawn'it, I'o`:

    import common.NoTag

    def apply(
      em: Iterable[Hive[Coeval, Parameters]]
    )(using
      params: Parameters
    ): `Hatch'em, Spawn'it, Output'o`[Coeval, Parameters] =
      new `Hatch'em, Spawn'it, I'o`(em, NoTag)()

package async:

  import conf.io.x.flow.async.Parameters

  final class `Hatch'em, Spawn'it, I'o`(
    _em: Iterable[Hive[Task, Parameters]],
    _tag: Any,
    _its: Seq[UUID] = UUID.randomUUID :: Nil
  )(using
    override protected val * : Parameters
  )(
    override protected val fs: *.Item => Boolean*
  )(using
    `_its*`: Map[UUID, (Boolean, Flag)] = new Map()
  ) extends `Base.Hatch'em, Spawn'it, Output'o`[Task, Parameters](_em, _tag, _its):

    override protected type * = `Hatch'em, Spawn'it, I'o`

    override def `#`(t: Any): * =
      new `Hatch'em, Spawn'it, I'o`(_em, t, _its)(fs*)

    override protected def `apply°`(s: *.Item => Boolean*): * =
      new `Hatch'em, Spawn'it, I'o`(_em, _tag, _its)(s*)

    override def toString(): String = "I" + super.toString


  object `Hatch'em, Spawn'it, I'o`:

    import common.NoTag

    def apply(
      em: Iterable[Hive[Task, Parameters]]
    )(using
      params: Parameters
    ): `Hatch'em, Spawn'it, Output'o`[Task, Parameters] =
      new `Hatch'em, Spawn'it, I'o`(em, NoTag)()
