package queens
package version.less
package plane
package base

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.Role
import protocol.server.defer._


abstract trait ServerPlaneProtocolDefer[
  EC <: PlaneServerDeferErrorCode,
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole,
  I <: NodeIfce
] extends PlaneServerDeferProtocol
    with PlaneProtocolDefer[EC, M, CDM] {

  import version.base.role.defer

  final override val role = defer.Server

  protected val readyTo: Actor[CDM]

  protected val interface: I

  protected abstract trait ServerPlaneDeferBehavior[T <: ServerPlaneDeferBehavior[T]]
      extends `RolePlaneBehavior*`[T] {

    private var resumed = false

    import version.less.plane.protocol.message.resume.{ ResumeProtocol => rm }

    import errorCode._

    override protected def errorBehavior(replyTo: Actor[CDM], msg: M, after: Boolean): Nothing = msg match {

      case rm.Resume(_) =>
        if resumed
        then
          replyTo ! dupResume
          stopped
        else
          resumed = true
          none


      case _ if !after && !resumed =>
        replyTo ! notResume
        stopped


      case _ =>
        super.errorBehavior(replyTo, msg, after)

    }

  }


}
