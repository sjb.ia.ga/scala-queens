package queens
package version.less
package plane
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import version.base.{ Plane, Multiplier, Wildcard }

import version.base.interface.NodeIfce


abstract trait PlaneProtocolIfce
    extends NodeIfce:

  import protocol.message.session.SessionProtocol
  import protocol.message.expand.ExpandProtocol

  protected abstract trait ObjectSessionExpand
      extends SessionProtocol
      with ExpandProtocol

  def SessionExpand: ObjectSessionExpand
