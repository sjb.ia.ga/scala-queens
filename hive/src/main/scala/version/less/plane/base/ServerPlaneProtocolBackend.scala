package queens
package version.less
package plane
package base

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.Role
import protocol.server.backend._


abstract trait ServerPlaneProtocolBackend[
  EC <: PlaneServerBackendErrorCode,
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
] extends PlaneServerBackendProtocol
    with PlaneProtocolRole[EC, Group, M, CDM] {

  import version.base.role.Backend

  final override val role = Backend

  protected val resultTo: Actor[CDM]

  protected val graceTo: Actor[SDM]

  protected abstract trait ServerPlaneBackendBehavior[T <: ServerPlaneBackendBehavior[T]]
      extends `RolePlaneBehavior*`[T] {

    protected var saluted = false

    import errorCode._

    override protected def errorBehavior(replyTo: Actor[CDM], msg: M, after: Boolean): Nothing = msg match {

      case _ if !after && !saluted =>
        replyTo ! notSalute
        stopped

      case _ =>
        super.errorBehavior(replyTo, msg, after)

    }

  }

}
