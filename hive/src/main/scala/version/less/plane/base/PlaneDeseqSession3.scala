package queens
package version.less
package plane
package base

import version.base.Deseq

import session.{ Start, Block }

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role


abstract trait PlaneDeseqSession3[L,
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole
] extends Deseq {

  override type Arguments = Tuple3[
    Start[CFM],
    version.base.Role => Boolean,
    Block[L, M]
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val start = args(0).asInstanceOf[Start[CFM]]
    val end = args(1).asInstanceOf[version.base.Role => Boolean]
    val block = args(2).asInstanceOf[Block[L, M]]
    (start, end, block)
  }

}


