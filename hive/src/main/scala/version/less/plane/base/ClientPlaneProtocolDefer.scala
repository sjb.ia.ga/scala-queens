package queens
package version.less
package plane
package base

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.Role
import protocol.client.defer._


abstract trait ClientPlaneProtocolDefer[
  EC <: PlaneClientDeferErrorCode,
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole
] extends PlaneClientDeferProtocol
    with PlaneProtocolDefer[EC, M, CFM] {

  import version.base.role.defer

  final override val role = defer.Client

  protected val onBehalf: Actor[CFM]

  protected abstract trait ClientPlaneDeferBehavior[T <: ClientPlaneDeferBehavior[T]]
      extends PlaneDeferBehavior[T] {

    protected val cryptoKey: CryptoKey

    private var deferred = false

    private var ready = false

    protected var secret: java.util.UUID = null

    import version.less.plane.protocol.message.defer.{ DeferProtocol => dm }
    import version.less.plane.protocol.message.ready.{ ReadyProtocol => ym }

    import errorCode._

    override protected def errorBehavior(replyTo: Actor[CFM], msg: M, after: Boolean): Nothing = msg match {

      case dm.Defer(_, _, _) =>
        if deferred
        then
          replyTo ! dupDefer
          stopped
        else
          deferred = true
          none

      case _ if !after && !deferred =>
        replyTo ! notDefer
        stopped

      case ym.Ready(_, _) =>
        if ready
        then
          replyTo ! dupReady
          stopped
        else
          ready = true
          none

      case _ if !after && !ready =>
        replyTo ! notReady
        stopped

      case _ =>
        super.errorBehavior(replyTo, msg, after)

    }

  }

}
