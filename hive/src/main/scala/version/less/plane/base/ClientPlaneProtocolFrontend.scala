package queens
package version.less
package plane
package base

import protocol.base.Protocol.Group
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.Role
import protocol.client.frontend._


abstract trait ClientPlaneProtocolFrontend[
  EC <: PlaneClientFrontendErrorCode,
  M >: Group <: Role,
  CM >: Group <: ClientRole
] extends PlaneClientFrontendProtocol
    with PlaneProtocolRole[EC, Group, M, CM] {

  import version.base.role.Frontend

  final override val role = Frontend

  protected val client: Actor[CM]

  protected abstract trait ClientPlaneFrontendBehavior[T <: ClientPlaneFrontendBehavior[T]]
      extends RolePlaneBehavior[T] {

    protected val end: version.base.Role => Boolean

    protected val apply: (version.base.Plane, queens.base.breed.Breed) => Unit

    implicit final protected def app2app[
      P <: version.base.Plane,
      R <: queens.base.breed.Breed
    ](app: (P, R) => Unit): (version.base.Plane, queens.base.breed.Breed) => Unit =
      app.asInstanceOf[(version.base.Plane, queens.base.breed.Breed) => Unit]

    private var expanded = false

    import version.less.plane.protocol.message.expand.{ ExpandProtocol => em }
    import version.less.plane.protocol.message.append.{ AppendProtocol => am }

    import errorCode._

    override protected def errorBehavior(replyTo: Actor[CM], msg: M, after: Boolean): Nothing = msg match {

      case em.Expand(_, _) if !after =>
        if expanded
        then
          replyTo ! dupExpand
          stopped
        else
          expanded = true
          none

      case _ if !after && !expanded =>
        client ! notExpand
        stopped

      case em.Expand(_, _) if after =>
        client ! mismatch
        stopped

      case am.Append(_, _) if after =>
        client ! mismatch
        stopped

      case _ =>
        none

    }

  }

}
