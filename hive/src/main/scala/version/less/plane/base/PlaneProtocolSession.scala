package queens
package version.less
package plane
package base

import akka.actor.typed.{ TypedActorContext, Signal, PostStop }
import akka.actor.typed.scaladsl.ActorContext

import version.base.Node
import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.session.Protocol.Role
import protocol.session._


abstract trait PlaneProtocolSession[L,
  EC <: PlaneSessionErrorCode,
  M >: Group <: Role,
  I <: NodeIfce
] extends PlaneSessionProtocol
    with Node[EC, Either[L, M]] {

  final override val role = version.base.role.node.Session

  protected val interface: I

  protected abstract trait SessionPlaneBehavior[T <: SessionPlaneBehavior[T]]
      extends NodeBehavior[Either[L, M], T] {

    protected val end: version.base.Role => Boolean

    override def receiveSignal(ctx: TypedActorContext[Either[L, M]], msg: Signal): B = {
      msg match {
        case PostStop => end(role)
        case _ =>
      }
      super.receiveSignal(ctx, msg)
    }

    protected var started: Boolean = false

    import version.base.protocol.{ BaseProtocol => bm }
    import version.less.plane.protocol.message.session.{ SessionProtocol => sm }

    import errorCode._

    protected def errorBehavior(block: M => Unit, msg: M): Nothing = {

      msg match {

        case sm.Start(_) if started =>
          block(Error(dupStart, role))
          stopped

        case sm.Start(_) =>
          started = true
          none

        case sm.Stop() if !started =>
          stopped

        case _ =>

      }

      msg match {

        case _ if !started =>
          block(Error(notStart, role))
          stopped

        case bm.Error(_, _) =>
          block(msg)
          same

        case bm.Complaint(bm.Complaint(msc, inner), outer) =>
          errorBehavior(block, Complaint(msc, inner ++ outer))

        case bm.Complaint(_, _) =>
          block(msg)
          same

        case sm.Stop() =>
          if end(role) then stopped
          same

        case _ =>
          none

      }

    }

    protected def perceive(ctx: ActorContext[?], msg: M): Nothing

    override def receive(ctx: TypedActorContext[Either[L, M]], msg: Either[L, M]): B =
      try {

        msg match {

          case Right(msg) =>
            perceive(ctx.asScala, msg)

          case _ =>
            same
        }

      } catch {

        case SomeBehavior(it) => it

        case NoneBehavior =>
          throw new queens.base.QueensException("error") { }

        case t: Throwable =>
          throw new queens.base.QueensException("error", t) { }

      }

  }

  protected abstract trait SessionPlaneInflater[T <: SessionPlaneBehavior[T]]
      extends NodeInflater[Either[L, M], T]

  protected abstract trait SessionPlaneInflaterX[T <: SessionPlaneBehavior[T]]
      extends SessionPlaneInflater[T]
      with NodeInflaterX[Either[L, M], T]

  protected abstract trait ObjectSessionPlaneBehavior[T <: SessionPlaneBehavior[T], U <: SessionPlaneInflater[T]]
      extends ObjectNodeBehavior[Either[L, M], T, U]

}
