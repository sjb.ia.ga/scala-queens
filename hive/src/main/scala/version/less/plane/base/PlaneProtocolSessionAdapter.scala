package queens
package version.less
package plane
package base

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.session.adapter.Protocol.Role
import protocol.session.adapter._


abstract trait PlaneProtocolSessionAdapter[
  EC <: PlaneSessionAdapterErrorCode,
  M >: Group <: Role
] extends PlaneSessionAdapterProtocol
    with PlaneProtocolRole[EC, Group, M, M] {

  import version.base.role.Adapter

  final override val role = Adapter

  protected val session: ActorRef[Either[?, M]]

  protected abstract trait SessionAdapterPlaneBehavior[T <: SessionAdapterPlaneBehavior[T]]
      extends RolePlaneBehavior[T] {

    protected val end: version.base.Role => Boolean

    import version.less.plane.protocol.message.session.{ SessionProtocol => sm }

    override protected def errorBehavior(replyTo: Actor[M], msg: M, after: Boolean): Nothing = ???

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing = {

      session ! Right(msg)

      msg match {

        case sm.Stop() =>
          if end(role) then stopped
          same

        case _ =>
          same
      }

    }

  }

  protected abstract trait SessionAdapterPlaneInflater[T <: SessionAdapterPlaneBehavior[T]]
      extends RolePlaneInflater[T]

}
