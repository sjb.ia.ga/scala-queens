package queens
package version.less
package plane
package base

import akka.actor.typed.TypedActorContext
import akka.actor.typed.scaladsl.ActorContext

import version.base.Node

import protocol.base.Protocol.{ Group => PlaneGroup }
import protocol._


abstract trait PlaneProtocolRole[
  EC <: PlaneErrorCode,
  Group >: PlaneGroup,
  M >: Group,
  RM >: Group
] extends PlaneProtocol
    with Node[EC, M]:

  protected abstract trait `RolePlaneBehavior*`[T <: `RolePlaneBehavior*`[T]]
      extends RolePlaneBehavior[T]:
    protected val cryptoKey: CryptoKey
    protected val secret: java.util.UUID


  protected abstract trait RolePlaneBehavior[T <: RolePlaneBehavior[T]]
      extends NodeBehavior[M, T]:

    implicit final protected def msg2msg[
      Role,
      MI <: Group,
      MO >: MI <: Role
    ](
      msg: MI
    ): MO =
      msg.asInstanceOf[MO]

    implicit final protected def err2msg[
      Role,
      MO >: Group <: Role
    ](errorVal: version.base.ErrorCode#ProtocolErrorVal): MO =
        import version.base.protocol.message.Protocol.{ Message => MI }
        val msg: MI = Error(errorVal, role)
        throw Exception(msg.toString)
        msg

    protected def excl_errorBehavior(
      replyTo: Actor[RM],
      msg: M,
      bl1: (Actor[RM], M) => Nothing,
      bl2: (Actor[RM], M) => Nothing
    ): Nothing =
      try
        bl1(replyTo, msg)
      finally
        try
          bl2(replyTo, msg)
        catch
          case NoneBehavior =>

    import version.base.protocol.{ BaseProtocol => bm }

    protected def errorBehavior(replyTo: Actor[RM], msg: M, after: Boolean = false): Nothing = msg match

      case bm.Error(_, _) =>
        replyTo ! Complaint(msg, Seq(role))
        stopped

      case bm.Complaint(msg, trace) =>
        replyTo ! Complaint(msg, trace)
        same

      case _ =>
        none


    protected def exclusive(
      ctx: ActorContext[M],
      msg: M,
      bl1: (ActorContext[M], M) => Nothing,
      bl2: (ActorContext[M], M) => Nothing
    ): Nothing =
      try
        bl1(ctx, msg)
      finally
        try
          bl2(ctx, msg)
        catch
          case NoneBehavior =>

    protected def perceive(ctx: ActorContext[M], msg: M): Nothing

    override def receive(ctx: TypedActorContext[M], msg: M): B =
      try
        perceive(ctx.asScala, msg)
      catch
        case SomeBehavior(it) => it

        case NoneBehavior =>
          throw new queens.base.QueensException("error") {}

        case t: Throwable =>
          throw PlaneProtocolRole.PlaneProtocolRoleReceiveException(t)


  protected abstract trait RolePlaneInflater[T <: RolePlaneBehavior[T]]
      extends NodeInflater[M, T]

  protected abstract trait RolePlaneInflaterX[T <: RolePlaneBehavior[T]]
      extends RolePlaneInflater[T]
      with NodeInflaterX[M, T]

  protected abstract trait ObjectRolePlaneBehavior[T <: RolePlaneBehavior[T], U <: RolePlaneInflater[T]]
      extends ObjectNodeBehavior[M, T, U]


object PlaneProtocolRole:

  final case class PlaneProtocolRoleReceiveException(t: Throwable)
      extends queens.base.QueensException("", t)
