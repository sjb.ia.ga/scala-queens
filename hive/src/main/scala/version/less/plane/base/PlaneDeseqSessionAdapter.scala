package queens
package version.less
package plane
package base

import version.base.Deseq


abstract trait PlaneDeseqSessionAdapter
    extends Deseq {

  override type Arguments = Tuple1[
    version.base.Role => Boolean
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val end = args(0).asInstanceOf[version.base.Role => Boolean]
    Tuple1(end)
  }

}


