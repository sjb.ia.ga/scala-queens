package queens
package version.less
package plane
package base

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.Protocol.Role
import protocol.client._


abstract trait PlaneProtocolClient[
  EC <: PlaneClientErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  I <: NodeIfce
] extends PlaneClientProtocol
    with PlaneProtocolRole[EC, Group, M, SM] {

  import version.base.role.Client

  final override val role = Client

  implicit protected val server: Actor[SM]

  protected val interface: I

  protected abstract trait ClientPlaneBehavior[T <: ClientPlaneBehavior[T]]
      extends RolePlaneBehavior[T] {

    protected val cryptoKey: CryptoKey

    protected val end: version.base.Role => Boolean

    import errorCode._

    import version.less.plane.protocol.message.grace.{ GraceProtocol => gm }

    override protected def errorBehavior(replyTo: Actor[SM], msg: M, after: Boolean): Nothing = msg match {

      case gm.Grace(axes) if after =>
        replyTo ! invalidPlane(axes)
        stopped

      case _ =>
        super.errorBehavior(replyTo, msg, after)

    }

  }

}
