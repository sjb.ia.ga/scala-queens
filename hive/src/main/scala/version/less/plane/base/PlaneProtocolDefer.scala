package queens
package version.less
package plane
package base

import protocol.base.Protocol.Group
import protocol.defer._


abstract trait PlaneProtocolDefer[
  EC <: PlaneDeferErrorCode,
  M >: Group,
  RM >: Group
] extends PlaneDeferProtocol
    with PlaneProtocolRole[EC, Group, M, RM] {

  protected abstract trait PlaneDeferBehavior[T <: PlaneDeferBehavior[T]]
      extends RolePlaneBehavior[T]

}
