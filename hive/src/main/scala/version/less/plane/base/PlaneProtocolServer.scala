package queens
package version.less
package plane
package base

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.session.Protocol.{ Role => SessionRole }
import protocol.server.Protocol.Role
import protocol.server._


abstract trait PlaneProtocolServer[
  EC <: PlaneServerErrorCode,
  M >: Group <: Role,
  SM >: Group <: SessionRole,
  I <: NodeIfce
] extends PlaneServerProtocol
    with PlaneProtocolRole[EC, Group, M, SM]:

  import version.base.role.Server

  final override val role = Server

  protected val session: Actor[SM]

  protected val interface: I

  protected abstract trait ServerPlaneBehavior[T <: ServerPlaneBehavior[T]]
      extends RolePlaneBehavior[T]:

    protected val cryptoKey: CryptoKey

    protected val end: version.base.Role => Boolean

    import version.less.plane.protocol.message.plane.{ PlaneProtocol => pm }
    import version.less.plane.protocol.message.grace.{ GraceProtocol => gm }

    import errorCode._

    override protected def errorBehavior(replyTo: Actor[SM], msg: M, after: Boolean): Nothing = msg match

      case pm.Plane(_, axes) if after =>
        session ! invalidPlane(axes)
        stopped

      case gm.Grace(axes) if after =>
        session ! invalidPlane(axes)
        stopped

      case _ =>
        super.errorBehavior(replyTo, msg, after)
