package queens
package version.less
package plane


package object session {

  import protocol.base.Protocol.Group
  import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
  import protocol.session.Protocol.Role

  type Start[CFM >: Group <: ClientFrontendRole] = Actor[CFM] => Unit
  type Block[L, M >: Group <: Role] = Either[L, M] => Unit

}
