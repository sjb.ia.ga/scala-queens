package queens
package version.less
package plane.protocol
package message


package session {

  import version.less.plane.protocol.session.Protocol.{ Role => SessionRole }
  import nest.protocol.session.Protocol.{ Role => NestSessionRole }
  import version.v1.vector.protocol.session.Protocol.{ Role => VectorV1SessionRole }
  import version.v2.plane.protocol.session.Protocol.{ Role => PlaneV2SessionRole }
  import version.v3.mco.protocol.session.Protocol.{ Role => MCOV3SessionRole }

  abstract trait SessionMatch
      extends SessionRole // Start, Stop,
      with NestSessionRole
      with VectorV1SessionRole
      with PlaneV2SessionRole
      with MCOV3SessionRole

  import base.Protocol.{ Group => PlaneGroup }
  import nest.protocol.base.Protocol.{ Group => NestGroup }
  import version.v1.vector.protocol.base.Protocol.{ Group => VectorV1Group }
  import version.v2.plane.protocol.base.Protocol.{ Group => PlaneV2Group }
  import version.v3.mco.protocol.base.Protocol.{ Group => MCOV3Group }

  abstract trait SessionMessage
      extends PlaneGroup
      with NestGroup
      with VectorV1Group
      with PlaneV2Group
      with MCOV3Group

  abstract trait SessionStartSelfGroup
      extends NestGroup
      with VectorV1Group
      with PlaneV2Group

  object Protocol {

    type Match = SessionMatch
    type Message = SessionMessage

    object Start {

      object Self {

        type Group = SessionStartSelfGroup
        type Role = version.less.plane.protocol.session.adapter.Protocol.Role

      }

    }

  }

}


package append {

  import nest.protocol.client.defer.Protocol.{ Role => NestClientDeferRole }
  import version.v1.mco.protocol.client.defer.Protocol.{ Role => MCOClientDeferRole }
  import version.v2.plane.protocol.client.defer.Protocol.{ Role => PlaneV2ClientDeferRole }

  import client.frontend.Protocol.{ Role => ClientFrontendRole }
  import nest.protocol.client.frontend.Protocol.{ Role => NestClientFrontendRole }
  import version.v1.mco.protocol.client.frontend.Protocol.{ Role => MCOClientFrontendRole }
  import version.v2.plane.protocol.client.frontend.Protocol.{ Role => PlaneV2ClientFrontendRole }

  abstract trait AppendMatch
      extends NestClientDeferRole
      with MCOClientDeferRole
      with PlaneV2ClientDeferRole

      with ClientFrontendRole
      with NestClientFrontendRole
      with MCOClientFrontendRole
      with PlaneV2ClientFrontendRole

  import base.Protocol.{ Group => PlaneGroup }
  import nest.protocol.base.Protocol.{ Group => NestGroup }
  import version.v1.mco.protocol.base.Protocol.{ Group => MCOGroup }
  import version.v2.plane.protocol.base.Protocol.{ Group => PlaneV2Group }

  abstract trait AppendMessage
      extends PlaneGroup
      with NestGroup
      with MCOGroup
      with PlaneV2Group

  object Protocol {

    type Match = AppendMatch
    type Message = AppendMessage

  }

}


package expand {

  import client.frontend.Protocol.{ Role => ClientFrontendRole }
  import nest.protocol.client.frontend.Protocol.{ Role => NestClientFrontendRole }
  import version.v1.mco.protocol.client.frontend.Protocol.{ Role => MCOClientFrontendRole }
  import version.v2.plane.protocol.client.frontend.Protocol.{ Role => PlaneV2ClientFrontendRole }

  abstract trait ExpandMatch
      extends ClientFrontendRole
      with NestClientFrontendRole
      with MCOClientFrontendRole
      with PlaneV2ClientFrontendRole

  import base.Protocol.{ Group => PlaneGroup }
  import nest.protocol.base.Protocol.{ Group => NestGroup }
  import version.v1.mco.protocol.base.Protocol.{ Group => MCOGroup }
  import version.v2.plane.protocol.base.Protocol.{ Group => PlaneV2Group }

  abstract trait ExpandMessage
      extends PlaneGroup
      with NestGroup
      with MCOGroup
      with PlaneV2Group

  abstract trait ExpandFromGroup
      extends NestGroup
      with MCOGroup
      with PlaneV2Group

  object Protocol {

    type Match = ExpandMatch
    type Message = ExpandMessage

  }

}


package breed {

  import nest.protocol.client.Protocol.{ Role => NestClientRole }
  import version.v1.mco.protocol.client.Protocol.{ Role => MCOClientRole }
  import version.v2.plane.protocol.client.Protocol.{ Role => PlaneV2ClientRole }

  import nest.protocol.client.frontend.Protocol.{ Role => NestClientFrontendRole }
  import version.v1.mco.protocol.client.frontend.Protocol.{ Role => MCOClientFrontendRole }
  import version.v2.plane.protocol.client.frontend.Protocol.{ Role => PlaneV2ClientFrontendRole }

  abstract trait BreedMatch
      extends NestClientRole
      with MCOClientRole
      with PlaneV2ClientRole

      with NestClientFrontendRole
      with MCOClientFrontendRole
      with PlaneV2ClientFrontendRole

  import nest.protocol.base.Protocol.{ Group => NestGroup }
  import version.v1.mco.protocol.base.Protocol.{ Group => MCOGroup }
  import version.v2.plane.protocol.base.Protocol.{ Group => PlaneV2Group }

  abstract trait BreedMessage
      extends NestGroup
      with MCOGroup
      with PlaneV2Group

  abstract trait BreedOnBehalfGroup
      extends NestGroup
      with MCOGroup
      with PlaneV2Group

  object Protocol {

    type Match = BreedMatch
    type Message = BreedMessage

    object Breed {

      object OnBehalf {

        type Group = BreedOnBehalfGroup
        type Role = client.frontend.Protocol.Role

      }

    }

  }

}


package plane {

  import server.Protocol.{ Role => ServerRole }
  import nest.protocol.server.Protocol.{ Role => NestServerRole }
  import version.v1.mco.protocol.server.Protocol.{ Role => MCOServerRole }
  import version.v2.plane.protocol.server.Protocol.{ Role => PlaneV2ServerRole }

  import client.Protocol.{ Role => ClientRole }
  import nest.protocol.client.Protocol.{ Role => NestClientRole }
  import version.v1.mco.protocol.client.Protocol.{ Role => MCOClientRole }
  import version.v2.plane.protocol.client.Protocol.{ Role => PlaneV2ClientRole }

  abstract trait PlaneMsgMatch
      extends ServerRole
      with NestServerRole
      with MCOServerRole
      with PlaneV2ServerRole

      with ClientRole
      with NestClientRole
      with MCOClientRole
      with PlaneV2ClientRole

  import base.Protocol.{ Group => PlaneDotGroup }
  import nest.protocol.base.Protocol.{ Group => NestGroup }
  import version.v1.mco.protocol.base.Protocol.{ Group => MCOGroup }
  import version.v2.plane.protocol.base.Protocol.{ Group => PlaneV2Group }

  abstract trait PlaneMessage
      extends PlaneDotGroup
      with NestGroup
      with MCOGroup
      with PlaneV2Group

  abstract trait PlaneDeferToGroup
      extends NestGroup
      with MCOGroup
      with PlaneV2Group

  object Protocol {

    type Match = PlaneMsgMatch
    type Message = PlaneMessage

    object Plane {

      object DeferTo {

        type Group = PlaneDeferToGroup
        type Role = client.defer.Protocol.Role

      }

    }

  }

}


package defer {

  import server.Protocol.{ Role => ServerRole }
  import nest.protocol.server.Protocol.{ Role => NestServerRole }
  import version.v1.mco.protocol.server.Protocol.{ Role => MCOServerRole }
  import version.v2.plane.protocol.server.Protocol.{ Role => PlaneV2ServerRole }

  import client.defer.Protocol.{ Role => ClientDeferRole }
  import nest.protocol.client.defer.Protocol.{ Role => NestClientDeferRole }
  import version.v1.mco.protocol.client.defer.Protocol.{ Role => MCOClientDeferRole }
  import version.v1.vector.protocol.client.defer.Protocol.{ Role => VectorV1ClientDeferRole }
  import version.v2.plane.protocol.client.defer.Protocol.{ Role => PlaneV2ClientDeferRole }

  abstract trait DeferMatch
      extends ServerRole
      with NestServerRole
      with MCOServerRole
      with PlaneV2ServerRole

      with ClientDeferRole
      with NestClientDeferRole
      with MCOClientDeferRole
      with VectorV1ClientDeferRole
      with PlaneV2ClientDeferRole

  import base.Protocol.{ Group => PlaneGroup }
  import nest.protocol.base.Protocol.{ Group => NestGroup }
  import version.v1.mco.protocol.base.Protocol.{ Group => MCOGroup }
  import version.v1.vector.protocol.base.Protocol.{ Group => VectorV1Group }
  import version.v2.plane.protocol.base.Protocol.{ Group => PlaneV2Group }

  abstract trait DeferMessage
      extends PlaneGroup
      with NestGroup
      with MCOGroup
      with VectorV1Group
      with PlaneV2Group

  abstract trait DeferResumeAtGroup
      extends NestGroup
      with MCOGroup
      with PlaneV2Group

  object Protocol {

    type Match = DeferMatch
    type Message = DeferMessage

    object Defer {

      object ResumeAt {

        type Group = DeferResumeAtGroup
        type Role = server.defer.Protocol.Role

      }

      object AtRequest {

        type Role = server.Protocol.Role

      }

    }

  }

}


package ready {

  import client.defer.Protocol.{ Role => ClientDeferRole }
  import nest.protocol.client.defer.Protocol.{ Role => NestClientDeferRole }
  import version.v1.mco.protocol.client.defer.Protocol.{ Role => MCOClientDeferRole }
  import version.v1.vector.protocol.client.defer.Protocol.{ Role => VectorV1ClientDeferRole }
  import version.v2.plane.protocol.client.defer.Protocol.{ Role => PlaneV2ClientDeferRole }

  import server.defer.Protocol.{ Role => ServerDeferRole }
  import nest.protocol.server.defer.Protocol.{ Role => NestServerDeferRole }
  import version.v1.mco.protocol.server.defer.Protocol.{ Role => MCOServerDeferRole }
  import version.v2.plane.protocol.server.defer.Protocol.{ Role => PlaneV2ServerDeferRole }

  abstract trait ReadyMatch
      extends ClientDeferRole
      with NestClientDeferRole
      with MCOClientDeferRole
      with VectorV1ClientDeferRole
      with PlaneV2ClientDeferRole

      with ServerDeferRole
      with NestServerDeferRole
      with MCOServerDeferRole
      with PlaneV2ServerDeferRole

  import base.Protocol.{ Group => PlaneGroup }
  import nest.protocol.base.Protocol.{ Group => NestGroup }
  import version.v1.mco.protocol.base.Protocol.{ Group => MCOGroup }
  import version.v1.vector.protocol.base.Protocol.{ Group => VectorV1Group }
  import version.v2.plane.protocol.base.Protocol.{ Group => PlaneV2Group }

  abstract trait ReadyMessage
      extends PlaneGroup
      with NestGroup
      with MCOGroup
      with VectorV1Group
      with PlaneV2Group

  abstract trait ReadyPlaneAtGroup
      extends NestGroup
      with MCOGroup
      with PlaneV2Group

  object Protocol {

    type Match = ReadyMatch
    type Message = ReadyMessage

    object Ready {

      object PlaneAt {

        type Group = ReadyPlaneAtGroup
        type Role = server.backend.Protocol.Role

      }

    }

  }

}


package resume {

  import client.defer.Protocol.{ Role => ClientDeferRole }
  import nest.protocol.client.defer.Protocol.{ Role => NestClientDeferRole }
  import version.v1.mco.protocol.client.defer.Protocol.{ Role => MCOClientDeferRole }
  import version.v2.plane.protocol.client.defer.Protocol.{ Role => PlaneV2ClientDeferRole }

  import nest.protocol.server.defer.Protocol.{ Role => NestServerDeferRole }
  import version.v1.mco.protocol.server.defer.Protocol.{ Role => MCOServerDeferRole }
  import version.v1.vector.protocol.server.defer.Protocol.{ Role => VectorV1ServerDeferRole }
  import version.v2.plane.protocol.server.defer.Protocol.{ Role => PlaneV2ServerDeferRole }

  abstract trait ResumeMatch
      extends ClientDeferRole
      with NestClientDeferRole
      with MCOClientDeferRole
      with PlaneV2ClientDeferRole

      with NestServerDeferRole
      with MCOServerDeferRole
      with VectorV1ServerDeferRole
      with PlaneV2ServerDeferRole

  import base.Protocol.{ Group => PlaneGroup }
  import nest.protocol.base.Protocol.{ Group => NestGroup }
  import version.v1.mco.protocol.base.Protocol.{ Group => MCOGroup }
  import version.v1.vector.protocol.base.Protocol.{ Group => VectorV1Group }
  import version.v2.plane.protocol.base.Protocol.{ Group => PlaneV2Group }

  abstract trait ResumeMessage
      extends PlaneGroup
      with NestGroup
      with MCOGroup
      with VectorV1Group
      with PlaneV2Group

  object Protocol {

    type Match = ResumeMatch
    type Message = ResumeMessage

  }

}


package grace {

  import nest.protocol.server.defer.Protocol.{ Role => NestServerDeferRole }
  import version.v1.mco.protocol.server.defer.Protocol.{ Role => MCOServerDeferRole }
  import version.v1.vector.protocol.server.defer.Protocol.{ Role => VectorV1ServerDeferRole }
  import version.v2.plane.protocol.server.defer.Protocol.{ Role => PlaneV2ServerDeferRole }

  import server.backend.Protocol.{ Role => ServerBackendRole }
  import nest.protocol.server.backend.Protocol.{ Role => NestServerBackendRole }
  import version.v1.mco.protocol.server.backend.Protocol.{ Role => MCOServerBackendRole }
  import version.v2.plane.protocol.server.backend.Protocol.{ Role => PlaneV2ServerBackendRole }

  abstract trait GraceMatch
      extends NestServerDeferRole
      with MCOServerDeferRole
      with VectorV1ServerDeferRole
      with PlaneV2ServerDeferRole

      with ServerBackendRole
      with NestServerBackendRole
      with MCOServerBackendRole
      with PlaneV2ServerBackendRole

  import base.Protocol.{ Group => PlaneGroup }
  import nest.protocol.base.Protocol.{ Group => NestGroup }
  import version.v1.mco.protocol.base.Protocol.{ Group => MCOGroup }
  import version.v1.vector.protocol.base.Protocol.{ Group => VectorV1Group }
  import version.v2.plane.protocol.base.Protocol.{ Group => PlaneV2Group }

  abstract trait GraceMessage
      extends PlaneGroup
      with NestGroup
      with MCOGroup
      with VectorV1Group
      with PlaneV2Group

  object Protocol {

    type Match = GraceMatch
    type Message = GraceMessage

  }

}


package result {

  import client.defer.Protocol.{ Role => ClientDeferRole }
  import nest.protocol.client.defer.Protocol.{ Role => NestClientDeferRole }
  import version.v1.mco.protocol.client.defer.Protocol.{ Role => MCOClientDeferRole }
  import version.v2.plane.protocol.client.defer.Protocol.{ Role => PlaneV2ClientDeferRole }

  import server.backend.Protocol.{ Role => ServerBackendRole }
  import nest.protocol.server.backend.Protocol.{ Role => NestServerBackendRole }
  import version.v1.mco.protocol.server.backend.Protocol.{ Role => MCOServerBackendRole }
  import version.v2.plane.protocol.server.backend.Protocol.{ Role => PlaneV2ServerBackendRole }

  abstract trait ResultMatch
      extends ClientDeferRole
      with NestClientDeferRole
      with MCOClientDeferRole
      with PlaneV2ClientDeferRole

      with ServerBackendRole
      with NestServerBackendRole
      with MCOServerBackendRole
      with PlaneV2ServerBackendRole

  import base.Protocol.{ Group => PlaneGroup }
  import nest.protocol.base.Protocol.{ Group => NestGroup }
  import version.v1.mco.protocol.base.Protocol.{ Group => MCOGroup }
  import version.v2.plane.protocol.base.Protocol.{ Group => PlaneV2Group }

  abstract trait ResultMessage
      extends PlaneGroup
      with NestGroup
      with MCOGroup
      with PlaneV2Group

  object Protocol {

    type Match = ResultMatch
    type Message = ResultMessage

  }

}
