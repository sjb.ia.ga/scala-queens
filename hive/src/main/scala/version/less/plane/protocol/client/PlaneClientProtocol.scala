package queens
package version.less
package plane.protocol
package client


abstract trait PlaneClientProtocol
    extends PlaneProtocol
    with communication.PlaneProtocol_Client_Server
    with communication.PlaneProtocol_Client_ClientFrontend


abstract trait PlaneClientErrorCode
    extends PlaneErrorCode
    with communication.PlaneErrorCode_Client_Server
    with communication.PlaneErrorCode_Client_ClientFrontend
