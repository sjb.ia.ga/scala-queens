package queens
package version.less
package plane.protocol
package session


abstract trait PlaneSessionProtocol
    extends PlaneProtocol
    with communication.PlaneProtocol_Session_Server


abstract trait PlaneSessionErrorCode
    extends PlaneErrorCode
    with communication.PlaneErrorCode_Session_Server {

  val dupStart = ProtocolDupVal()

  val notStart = ProtocolNotVal()

}
