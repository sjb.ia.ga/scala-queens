package queens
package version.less
package plane.protocol
package server.backend


abstract trait PlaneServerBackendProtocol
    extends PlaneProtocol
    with communication.PlaneProtocol_ServerBackend_ServerDefer
    with communication.PlaneProtocol_ServerBackend_ClientDefer


abstract trait PlaneServerBackendErrorCode
    extends PlaneErrorCode
    with communication.PlaneErrorCode_ServerBackend_ServerDefer
    with communication.PlaneErrorCode_ServerBackend_ClientDefer {

  val notSalute = ProtocolNotVal()

  val badSecret = ProtocolBadVal()

}
