package queens
package version.less
package plane.protocol
package message.breed

import version.base.Plane

import version.less.plane.Actor


abstract trait BreedProtocol
    extends PlaneProtocol:

  import BreedProtocol._

  inline final def Breed[
    P <: Plane,
    BM >: base.Protocol.Group <: Protocol.Breed.OnBehalf.Role
  ](
    plane: P,
    onBehalf: Actor[BM]
  ) = new Breed[P, BM](plane, onBehalf)


object BreedProtocol:

  final case class Breed[
    P <: Plane,
    BM >: base.Protocol.Group <: Protocol.Breed.OnBehalf.Role
  ](
    plane: P,
    onBehalf: Actor[BM]
  ) extends Protocol.Message


abstract trait BreedErrorCode
    extends PlaneErrorCode
