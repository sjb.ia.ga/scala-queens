package queens
package version.less
package plane.protocol
package communication


import message.ready.{ ReadyProtocol => MsgReadyProtocol }
import message.resume.{ ResumeProtocol => MsgResumeProtocol }

abstract trait PlaneProtocol_ClientDefer_ServerDefer
    extends MsgReadyProtocol
    with MsgResumeProtocol


import message.ready.{ ReadyErrorCode => MsgReadyErrorCode }
import message.resume.{ ResumeErrorCode => MsgResumeErrorCode }

abstract trait PlaneErrorCode_ClientDefer_ServerDefer
    extends MsgReadyErrorCode
    with MsgResumeErrorCode
