package queens
package version.less
package plane.protocol
package communication


import message.session.{ SessionProtocol => MsgSessionProtocol }

abstract trait PlaneProtocol_Server_Session
    extends MsgSessionProtocol


abstract trait PlaneErrorCode_Server_Session
    extends PlaneErrorCode
