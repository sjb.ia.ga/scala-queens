package queens
package version.less
package plane.protocol
package message.defer

import version.less.plane.Actor


abstract trait DeferProtocol
    extends PlaneProtocol:

  import DeferProtocol._

  inline final def Defer[
    RM >: base.Protocol.Group <: Protocol.Defer.ResumeAt.Role,
    MR >: base.Protocol.Group <: Protocol.Defer.AtRequest.Role
  ](
    resumeAt: Actor[RM],
    atRequest: MR,
    encryptedBytes: Seq[Byte]
  ) = new Defer[RM, MR](resumeAt, atRequest, encryptedBytes)


object DeferProtocol:

  final case class Defer[
    RM >: base.Protocol.Group <: Protocol.Defer.ResumeAt.Role,
    MR >: base.Protocol.Group <: Protocol.Defer.AtRequest.Role
  ](
    resumeAt: Actor[RM],
    atRequest: MR,
    encryptedBytes: Seq[Byte]
  ) extends Protocol.Message


abstract trait DeferErrorCode
    extends PlaneErrorCode
