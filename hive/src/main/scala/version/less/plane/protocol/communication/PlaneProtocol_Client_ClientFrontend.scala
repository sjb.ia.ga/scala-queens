package queens
package version.less
package plane.protocol
package communication


import message.breed.{ BreedProtocol => MsgBreedProtocol }
import message.grace.{ GraceProtocol => MsgGraceProtocol }

abstract trait PlaneProtocol_Client_ClientFrontend
    extends MsgBreedProtocol
    with MsgGraceProtocol


import message.breed.{ BreedErrorCode => MsgBreedErrorCode }

abstract trait PlaneErrorCode_Client_ClientFrontend
    extends MsgBreedErrorCode
