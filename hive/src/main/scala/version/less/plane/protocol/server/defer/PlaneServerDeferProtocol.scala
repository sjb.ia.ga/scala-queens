package queens
package version.less
package plane.protocol
package server.defer


import defer.PlaneDeferProtocol

abstract trait PlaneServerDeferProtocol
    extends PlaneDeferProtocol
    with communication.PlaneProtocol_ServerDefer_ServerBackend


import defer.PlaneDeferErrorCode

abstract trait PlaneServerDeferErrorCode
    extends PlaneDeferErrorCode
    with communication.PlaneErrorCode_ServerDefer_ServerBackend {

  val dupResume = ProtocolDupVal()

  val notResume = ProtocolNotVal()

}
