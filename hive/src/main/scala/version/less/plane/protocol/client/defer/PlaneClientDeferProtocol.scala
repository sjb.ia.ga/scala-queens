package queens
package version.less
package plane.protocol
package client.defer


import defer.PlaneDeferProtocol

abstract trait PlaneClientDeferProtocol
    extends PlaneDeferProtocol
    with communication.parameters.PlaneProtocol_ClientDefer_Server
    with communication.PlaneProtocol_ClientDefer_Server
    with communication.PlaneProtocol_ClientDefer_ServerBackend
    with communication.PlaneProtocol_ClientDefer_ClientFrontend


import defer.PlaneDeferErrorCode

abstract trait PlaneClientDeferErrorCode
    extends PlaneDeferErrorCode
    with communication.PlaneErrorCode_ClientDefer_Server
    with communication.PlaneErrorCode_ClientDefer_ServerBackend
    with communication.PlaneErrorCode_ClientDefer_ClientFrontend {

  val dupDefer = ProtocolDupVal()

  val notDefer = ProtocolNotVal()

  val dupReady = ProtocolDupVal()

  val notReady = ProtocolNotVal()

}
