package queens
package version.less
package plane.protocol
package message.append

import version.base.Plane
import queens.base.breed.Breed


abstract trait AppendProtocol
    extends PlaneProtocol:

  import AppendProtocol._

  inline final def Append[
    P <: Plane,
    R <: Breed
  ](plane: P, breed: R) = new Append[P, R](plane, breed)


object AppendProtocol:

  final case class Append[
    P <: Plane,
    R <: Breed
  ](plane: P, breed: R)
      extends Protocol.Message


abstract trait AppendErrorCode
    extends PlaneErrorCode
