package queens
package version.less
package plane.protocol
package defer


abstract trait PlaneDeferProtocol
    extends PlaneProtocol
    with communication.PlaneProtocol_ClientDefer_ServerDefer

abstract trait PlaneDeferErrorCode
    extends PlaneErrorCode
    with communication.PlaneErrorCode_ClientDefer_ServerDefer
