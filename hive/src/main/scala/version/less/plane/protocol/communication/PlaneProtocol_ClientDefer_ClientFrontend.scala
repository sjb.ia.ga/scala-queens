package queens
package version.less
package plane.protocol
package communication


import message.append.{ AppendProtocol => MsgAppendProtocol }

abstract trait PlaneProtocol_ClientDefer_ClientFrontend
    extends MsgAppendProtocol


import message.append.{ AppendErrorCode => MsgAppendErrorCode }

abstract trait PlaneErrorCode_ClientDefer_ClientFrontend
    extends MsgAppendErrorCode
