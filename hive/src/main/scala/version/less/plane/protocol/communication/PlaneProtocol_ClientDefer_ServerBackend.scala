package queens
package version.less
package plane.protocol
package communication


import message.result.{ ResultProtocol => MsgResultProtocol }

abstract trait PlaneProtocol_ClientDefer_ServerBackend
    extends MsgResultProtocol


import message.result.{ ResultErrorCode => MsgResultErrorCode }

abstract trait PlaneErrorCode_ClientDefer_ServerBackend
    extends MsgResultErrorCode
