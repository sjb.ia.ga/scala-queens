package queens
package version.less
package plane.protocol
package communication


import message.plane.{ PlaneProtocol => MsgPlaneProtocol }
import message.grace.{ GraceProtocol => MsgGraceProtocol }

abstract trait PlaneProtocol_Client_Server
    extends MsgPlaneProtocol
    with MsgGraceProtocol


import message.plane.{ PlaneErrorCode => MsgPlaneErrorCode }

abstract trait PlaneErrorCode_Client_Server
    extends MsgPlaneErrorCode
