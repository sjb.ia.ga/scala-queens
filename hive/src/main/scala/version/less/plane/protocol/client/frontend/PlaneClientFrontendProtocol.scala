package queens
package version.less
package plane.protocol
package client.frontend


abstract trait PlaneClientFrontendProtocol
    extends PlaneProtocol
    with communication.PlaneProtocol_ClientFrontend_Client
    with communication.PlaneProtocol_ClientFrontend_ClientDefer
    with message.expand.ExpandProtocol


abstract trait PlaneClientFrontendErrorCode
    extends PlaneErrorCode
    with communication.PlaneErrorCode_ClientFrontend_Client
    with communication.PlaneErrorCode_ClientFrontend_ClientDefer {

  val dupExpand = ProtocolDupVal()

  val notExpand = ProtocolNotVal()

}
