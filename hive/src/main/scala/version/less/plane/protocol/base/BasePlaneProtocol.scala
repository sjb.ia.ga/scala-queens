package queens
package version.less
package plane.protocol
package base


import version.base.protocol.BaseProtocol

abstract trait BasePlaneProtocol
    extends BaseProtocol


import version.base.protocol.BaseErrorCode

abstract trait BasePlaneErrorCode
    extends BaseErrorCode
