package queens
package version.less
package plane.protocol
package message.plane

import dimensions.Dimension.Axes

import version.less.plane.Actor


abstract trait PlaneProtocol
    extends plane.protocol.PlaneProtocol:

  import PlaneProtocol._

  inline final def Plane[
    DM >: base.Protocol.Group <: Protocol.Plane.DeferTo.Role
  ](
    deferTo: Actor[DM],
    axes: Axes
  ) = new Plane[DM](deferTo, axes)


object PlaneProtocol:

  final case class Plane[
    DM >: base.Protocol.Group <: Protocol.Plane.DeferTo.Role
  ](
    deferTo: Actor[DM],
    axes: Axes
  ) extends Protocol.Message


abstract trait PlaneErrorCode
    extends plane.protocol.PlaneErrorCode
