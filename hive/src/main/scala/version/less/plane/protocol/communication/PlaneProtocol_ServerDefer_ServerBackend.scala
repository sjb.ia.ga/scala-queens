package queens
package version.less
package plane.protocol
package communication


import message.grace.{ GraceProtocol => MsgGraceProtocol }

abstract trait PlaneProtocol_ServerDefer_ServerBackend
    extends MsgGraceProtocol


abstract trait PlaneErrorCode_ServerDefer_ServerBackend
    extends PlaneErrorCode
