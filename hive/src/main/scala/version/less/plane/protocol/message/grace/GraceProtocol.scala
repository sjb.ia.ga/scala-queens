package queens
package version.less
package plane.protocol
package message.grace

import dimensions.Dimension.Axes


abstract trait GraceProtocol
    extends PlaneProtocol:

  import GraceProtocol._

  inline final def Grace(axes: Axes) = new Grace(axes)


object GraceProtocol:

  final case class Grace(axes: Axes)
      extends Protocol.Message
