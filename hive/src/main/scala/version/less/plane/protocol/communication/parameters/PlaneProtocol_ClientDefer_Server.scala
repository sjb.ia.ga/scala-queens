package queens
package version.less
package plane.protocol
package communication.parameters


import message.plane.{ PlaneProtocol => MsgPlaneProtocol }

abstract trait PlaneProtocol_ClientDefer_Server
    extends MsgPlaneProtocol
