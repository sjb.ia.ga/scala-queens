package queens
package version.less
package plane.protocol
package message.resume

import dimensions.Dimension.Axes

import version.less.plane.Actor


abstract trait ResumeProtocol
    extends PlaneProtocol:

  import ResumeProtocol._

  inline final def Resume(axes: Axes) = new Resume(axes)


object ResumeProtocol:

  final case class Resume(axes: Axes)
      extends Protocol.Message


abstract trait ResumeErrorCode
    extends PlaneErrorCode
