package queens
package version.less
package plane.protocol
package message.expand

import version.less.plane.Actor

import version.base.{ Plane, Wildcard, Multiplier }


abstract trait ExpandProtocol
    extends PlaneProtocol:

  import ExpandProtocol._

  inline final def Expand[
    P <: Plane,
    W <: Wildcard[P]
  ](
    wildcard: W,
    multiplier: Multiplier[P] = Multiplier.Once[P]()
  ) = new Expand[P, W](wildcard, multiplier)


object ExpandProtocol:

  final case class Expand[
    P <: Plane,
    W <: Wildcard[P]
  ](
    wildcard: W,
    multiplier: Multiplier[P] = Multiplier.Once[P]()
  ) extends ExpandMessage
