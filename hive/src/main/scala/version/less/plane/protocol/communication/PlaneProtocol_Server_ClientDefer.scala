package queens
package version.less
package plane.protocol
package communication


import message.defer.{ DeferProtocol => MsgDeferProtocol }

abstract trait PlaneProtocol_Server_ClientDefer
    extends MsgDeferProtocol


import message.defer.{ DeferErrorCode => MsgDeferErrorCode }

abstract trait PlaneErrorCode_Server_ClientDefer
    extends MsgDeferErrorCode
