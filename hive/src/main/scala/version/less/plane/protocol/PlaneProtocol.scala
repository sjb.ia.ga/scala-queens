package queens
package version.less
package plane.protocol


import base.BasePlaneProtocol

abstract trait PlaneProtocol
    extends BasePlaneProtocol


import base.BasePlaneErrorCode

abstract trait PlaneErrorCode
    extends BasePlaneErrorCode {

  val mismatch = ProtocolAintVal()

  val overflow = ProtocolCountVal()


  import dimensions.Dimension.Axes

  final case class ProtocolInvalidVal(axes: Axes)
      extends ProtocolErrorVal()

  def invalidPlane(axes: Axes) = ProtocolInvalidVal(axes)


  def errorSecret(role: version.base.Role) =
    ProtocolCatchVal(null, role)

}
