package queens
package version.less
package plane.protocol


package object communication {

  type PlaneProtocol_Session_Server = PlaneProtocol_Server_Session

  type PlaneErrorCode_Session_Server =  PlaneErrorCode_Server_Session


  type PlaneProtocol_Server_Client = PlaneProtocol_Client_Server

  type PlaneErrorCode_Server_Client =  PlaneErrorCode_Client_Server


  type PlaneProtocol_ClientDefer_Server = PlaneProtocol_Server_ClientDefer

  type PlaneErrorCode_ClientDefer_Server = PlaneErrorCode_Server_ClientDefer


  type PlaneProtocol_ServerBackend_ServerDefer = PlaneProtocol_ServerDefer_ServerBackend

  type PlaneErrorCode_ServerBackend_ServerDefer = PlaneErrorCode_ServerDefer_ServerBackend


  type PlaneProtocol_ServerBackend_ClientDefer = PlaneProtocol_ClientDefer_ServerBackend

  type PlaneErrorCode_ServerBackend_ClientDefer = PlaneErrorCode_ClientDefer_ServerBackend


  type PlaneProtocol_ServerDefer_ClientDefer = PlaneProtocol_ClientDefer_ServerDefer

  type PlaneErrorCode_ServerDefer_ClientDefer = PlaneErrorCode_ClientDefer_ServerDefer


  type PlaneProtocol_ClientFrontend_Client = PlaneProtocol_Client_ClientFrontend

  type PlaneErrorCode_ClientFrontend_Client = PlaneErrorCode_Client_ClientFrontend


  type PlaneProtocol_ClientFrontend_ClientDefer = PlaneProtocol_ClientDefer_ClientFrontend

  type PlaneErrorCode_ClientFrontend_ClientDefer = PlaneErrorCode_ClientDefer_ClientFrontend

}
