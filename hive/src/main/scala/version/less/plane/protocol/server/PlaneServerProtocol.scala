package queens
package version.less
package plane.protocol
package server


abstract trait PlaneServerProtocol
    extends PlaneProtocol
    with communication.PlaneProtocol_Server_Session
    with communication.PlaneProtocol_Server_Client
    with communication.PlaneProtocol_Server_ClientDefer


abstract trait PlaneServerErrorCode
    extends PlaneErrorCode
    with communication.PlaneErrorCode_Server_Session
    with communication.PlaneErrorCode_Server_Client
    with communication.PlaneErrorCode_Server_ClientDefer {

  val dupPlane = ProtocolDupVal()

  val notPlane = ProtocolNotVal()

}
