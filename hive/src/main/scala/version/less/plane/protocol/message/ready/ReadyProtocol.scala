package queens
package version.less
package plane.protocol
package message.ready

import java.util.UUID

import version.less.plane.Actor


abstract trait ReadyProtocol
    extends PlaneProtocol:

  import ReadyProtocol._

  inline final def Ready[
    PM >: base.Protocol.Group <: Protocol.Ready.PlaneAt.Role
  ](
    planeAt: Actor[PM],
    hiMessage: Function[UUID, PM]
  ) = new Ready[PM](planeAt, hiMessage)


object ReadyProtocol:

  final case class Ready[
    PM >: base.Protocol.Group <: Protocol.Ready.PlaneAt.Role
  ](
    planeAt: Actor[PM],
    hiMessage: Function[UUID, PM]
  ) extends Protocol.Message


abstract trait ReadyErrorCode
    extends PlaneErrorCode
