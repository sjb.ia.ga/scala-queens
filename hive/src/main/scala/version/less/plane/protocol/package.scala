package queens
package version.less
package plane.protocol


package base {

  abstract trait PlaneGroup
      extends nest.protocol.base.Protocol.Group
      with version.v1.mco.protocol.base.Protocol.Group
      with version.v2.plane.protocol.base.Protocol.Group
      with message.session.Protocol.Match
      with message.session.Protocol.Start.Self.Group
      with message.expand.Protocol.Match
      with message.breed.Protocol.Match
      with message.breed.Protocol.Breed.OnBehalf.Group
      with message.plane.Protocol.Match
      with message.plane.Protocol.Plane.DeferTo.Group
      with message.defer.Protocol.Match
      with message.ready.Protocol.Match
      with message.ready.Protocol.Ready.PlaneAt.Group
      with message.resume.Protocol.Match
      with message.grace.Protocol.Match
      with message.result.Protocol.Match
      with message.append.Protocol.Match

  object Protocol {

    type Group = PlaneGroup

  }

}


package client {

  abstract trait ClientRole

  object Protocol {

    type Role = ClientRole

  }

  package defer {

    abstract trait ClientDeferRole

    object Protocol {

      type Role = ClientDeferRole

    }

  }

  package frontend {

    abstract trait ClientFrontendRole

    object Protocol {

      type Role = ClientFrontendRole

    }

  }

}

package server {

  abstract trait ServerRole

  object Protocol {

    type Role = ServerRole

  }

  package defer {

    abstract trait ServerDeferRole

    object Protocol {

      type Role = ServerDeferRole

    }

  }

  package backend {

    abstract trait ServerBackendRole

    object Protocol {

      type Role = ServerBackendRole

    }

  }

}

package session {

  abstract trait SessionRole

  object Protocol {

    type Role = SessionRole

  }

  package adapter {

    object Protocol {

      type Role = session.Protocol.Role

    }

  }

  package object adapter {

    type PlaneSessionAdapterProtocol= PlaneSessionProtocol
    type PlaneSessionAdapterErrorCode = PlaneSessionErrorCode

  }

}
