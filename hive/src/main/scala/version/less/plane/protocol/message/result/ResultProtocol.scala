package queens
package version.less
package plane.protocol
package message.result

import queens.base.breed.Breed


abstract trait ResultProtocol
    extends PlaneProtocol:

  import ResultProtocol._

  inline final def Result(breed: Breed) = new Result(breed)


object ResultProtocol:

  final case class Result(breed: Breed)
      extends Protocol.Message


abstract trait ResultErrorCode
    extends PlaneErrorCode
