package queens
package version.less
package plane.protocol
package message.session

import version.less.plane.Actor


abstract trait SessionProtocol
    extends PlaneProtocol:

  import SessionProtocol._

  inline final def Start[
    SM >: base.Protocol.Group <: Protocol.Start.Self.Role
  ](
    self: Actor[SM]
  ) = new Start(self)

  inline final def Stop() = new Stop()


object SessionProtocol:

  final case class Start[
    SM >: base.Protocol.Group <: Protocol.Start.Self.Role
  ](
    self: Actor[SM]
  ) extends Protocol.Message

  final case class Stop()
      extends Protocol.Message
