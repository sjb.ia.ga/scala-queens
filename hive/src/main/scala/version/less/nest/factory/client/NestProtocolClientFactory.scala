package queens
package version.less
package nest
package factory.client

import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.Protocol.Role


abstract trait NestProtocolClientFactory[
  M >: Group <: Role,
  SM >: Group <: ServerRole
] {

  def apply(
    context: ActorContext[?],
    server: Actor[SM],
    args: Any*
  ): Actor[M]

}
