package queens
package version.less
package nest
package factory.server

import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.session.Protocol.{ Role => SessionRole }
import protocol.server.Protocol.Role


abstract trait NestProtocolServerFactory[
  M >: Group <: Role,
  SM >: Group <: SessionRole
] {

  def apply(
    context: ActorContext[?],
    session: Actor[SM],
    args: Any*
  ): Actor[M]

}
