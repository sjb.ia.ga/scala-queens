package queens
package version.less
package nest
package factory.server.backend

import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.Role


abstract trait NestProtocolServerBackendFactory[
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
] {

  def apply(
    context: ActorContext[?],
    graceTo: Actor[SDM],
    resultTo: Actor[CDM],
    args: Any*
  ): Actor[M]

}
