package queens
package version.less
package nest
package factory.session

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.session.Protocol.Role


abstract trait NestProtocolSessionFactory[
  M >: Group <: Role
] {

  def apply[L](
    context: ActorContext[?],
    args: Any*
  ): ActorRef[Either[L, M]]

}
