package queens
package version.less
package nest
package factory.session.adapter

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.session.adapter.Protocol.Role


abstract trait NestProtocolSessionAdapterFactory[
  M >: Group <: Role
] {

  def apply(
    context: ActorContext[?],
    session: ActorRef[Either[?, M]],
    args: Any*
  ): Actor[M]

}
