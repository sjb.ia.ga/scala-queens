package queens
package version.less
package nest
package factory.client.frontend

import akka.actor.typed.scaladsl.ActorContext

import protocol.base.Protocol.Group
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.Role


abstract trait NestProtocolClientFrontendFactory[
  M >: Group <: Role,
  CM >: Group <: ClientRole
] {

  def apply(
    context: ActorContext[?],
    client: Actor[CM],
    args: Any*
  ): Actor[M]

}
