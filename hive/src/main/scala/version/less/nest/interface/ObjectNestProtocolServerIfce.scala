package queens
package version.less
package nest
package interface

import akka.actor.typed.scaladsl.ActorContext

import base.interface.NestProtocolServerIfce

import protocol.client.defer.{ Protocol => Parameter }
import protocol.server.defer.{ Protocol => Interface }


/**
  * @see [[queens.version.less.nest.server.Implicits.NestProtocolServerIfceImplicit]]
  */
abstract trait ObjectNestProtocolServerIfce extends NestProtocolServerIfce[
  Interface.Role,
  Parameter.Role
] { interface =>

  override object PlaneNest
      extends ObjectPlaneNest {

    override def apply(
      ctx: ActorContext[?],
      readyTo: Parameter.Actor,
      args: Any*
    ): Interface.Actor = {
      import factory.server.defer._

      type ServerDeferFactory = NestProtocolServerDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(classOf[ServerDeferFactory])(ctx, readyTo, args*)
    }

  }

}
