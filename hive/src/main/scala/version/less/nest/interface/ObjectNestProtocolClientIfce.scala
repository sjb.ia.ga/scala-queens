package queens
package version.less
package nest
package interface

import akka.actor.typed.scaladsl.ActorContext

import base.interface.NestProtocolClientIfce

import protocol.client.frontend.{ Protocol => Parameter }
import protocol.client.defer.{ Protocol => Interface }


/**
  * @see [[queens.version.less.nest.client.Implicits.NestProtocolClientIfceImplicit]]
  */
abstract trait ObjectNestProtocolClientIfce extends NestProtocolClientIfce[
  Interface.Role,
  Parameter.Role
] { interface =>

  override object BreedNest
      extends ObjectBreedNest {

    override def apply(
      ctx: ActorContext[?],
      onBehalf: Parameter.Actor,
      args: Any*
    ): Interface.Actor = {
      import factory.client.defer._

      type ClientDeferFactory = NestProtocolClientDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(classOf[ClientDeferFactory])(ctx, onBehalf, args*)
    }

  }

}
