package queens
package version.less
package nest
package interface

import scala.util.{ Failure, Success }

import akka.actor.typed.scaladsl.ActorContext

import common.cipher.Crypto
import common.cipher.client.CipherClient

import base.interface.NestProtocolSessionIfce
import base.interface.NestSessionStart

import protocol.server.{ Protocol => Server }
import protocol.client.{ Protocol => Client }
import protocol.client.frontend.{ Protocol => ClientFrontend }
import protocol.session.Protocol.Role


/**
  * @see [[queens.version.less.nest.session.Implicits.NestProtocolSessionIfceImplicit]]
  */
abstract trait ObjectNestProtocolSessionIfce extends NestProtocolSessionIfce[
  Role,
  Server.Role,
  Client.Role,
  ClientFrontend.Role
] { interface =>

  override object StartNest
      extends ObjectStartNest {

    override def apply[L](
      ctx: ActorContext[?],
      self: Actor[Role],
      args: Any*
    ): NestSessionStart[
      Server.Role,
      Client.Role,
      ClientFrontend.Role
    ] = {
      object Deseq extends base.NestDeseqSession[L,
        Role,
        ClientFrontend.Role
      ]

      val (_, end, _, apply) = Deseq(args)

      val cryptoKey = CipherClient().getOrElse(throw Crypto.CipherException(""))

      val server = {
        import factory.server._

        import protocol.session.{ Protocol => Parameter }
        import protocol.server.{ Protocol => Interface }

        type ServerFactory = NestProtocolServerFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(classOf[ServerFactory])(ctx, self, cryptoKey, end)
      }

      val client = {
        import factory.client._

        import protocol.server.{ Protocol => Parameter }
        import protocol.client.{ Protocol => Interface }

        type ClientFactory = NestProtocolClientFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(classOf[ClientFactory])(ctx, server, cryptoKey, end)
      }

      val frontend = {
        import factory.client.frontend._

        import protocol.client.{ Protocol => Parameter }
        import protocol.client.frontend.{ Protocol => Interface }

        type ClientFrontendFactory = NestProtocolClientFrontendFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(classOf[ClientFrontendFactory])(ctx, client, end, apply)
      }

      NestSessionStart(server, client, frontend)
    }

  }

}
