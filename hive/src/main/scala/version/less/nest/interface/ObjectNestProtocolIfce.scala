package queens
package version.less
package nest
package interface

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import dimensions.three.breed.Hatch
import nest.{ Apply, Nest, Wildcard }
import version.base.Multiplier

import base.interface.NestProtocolIfce

import protocol.base.Protocol.Group
import protocol.session.Protocol.Role


/**
  * @see [[queens.version.less.nest.Implicits.NestProtocolIfceImplicit]]
  */
abstract trait ObjectNestProtocolIfce[
  M >: Group <: Role
] extends NestProtocolIfce[M] { interface =>

  override object SessionExpand
      extends ObjectNestSessionExpand:

    import version.base.Role
    import version.base.role.Adapter

    override def apply(
      context: ActorContext[?],
      end: Role => Boolean,
      stop: () => Unit,
      block: M => Unit,
      less_nest: Apply,
      wildcard: Wildcard,
      multiplier: Multiplier[Nest]
    ): Int =
      if wildcard(multiplier).size == 0 then
        return 0

      import factory.session.adapter._
      import protocol.session.adapter.{ Protocol => SessionAdapter }
      type SessionAdapterFactory = NestProtocolSessionAdapterFactory[SessionAdapter.Role]

      var sessionAdapter: SessionAdapter.Actor = null

      def msg_nest(message: Either[Unit, M]): Unit =
        message match
          case Left(_) =>
            sessionAdapter ! Start(sessionAdapter)
          case Right(msg) =>
            block(msg)

      val end_nest = { (role: Role) =>
                        val r = end(role)
                        if r && role == Adapter then stop()
                        r
                     }

      import protocol.client.frontend.{ Protocol => ClientFrontend }

      import factory.session._
      import protocol.session.{ Protocol => Session }
      type SessionFactory = NestProtocolSessionFactory[Session.Role]

      val session =
        interface(classOf[SessionFactory])[Unit](context,
          { (frontend: ClientFrontend.Actor) =>
            frontend ! Expand(wildcard, multiplier)
          },
          end_nest,
          msg_nest _,
          { (nest: Nest, hatch: Hatch[?]) =>
            less_nest(nest, hatch)
          }
        ).asInstanceOf[ActorRef[Either[?, SessionAdapter.Role]]]

      sessionAdapter = interface(classOf[SessionAdapterFactory])(context, session, end_nest)

      session ! Left(())

      wildcard(multiplier).size

}
