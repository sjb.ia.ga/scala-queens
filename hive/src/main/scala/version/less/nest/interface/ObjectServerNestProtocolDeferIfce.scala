package queens
package version.less
package nest
package interface

import akka.actor.typed.scaladsl.ActorContext

import base.interface.ServerNestProtocolDeferIfce

import protocol.server.defer.{ Protocol => Parameter1 }
import protocol.client.defer.{ Protocol => Parameter2 }
import protocol.server.backend.{ Protocol => Interface }


/**
  * @see [[queens.version.less.nest.server.defer.Implicits.ServerNestProtocolDeferIfceImplicit]]
  */
abstract trait ObjectServerNestProtocolDeferIfce extends ServerNestProtocolDeferIfce[
  Interface.Role,
  Parameter1.Role,
  Parameter2.Role
] { interface =>

  override object ResumeNest
      extends ObjectResumeNest {

    override def apply(
      ctx: ActorContext[?],
      graceTo: Parameter1.Actor,
      resultTo: Parameter2.Actor,
      args: Any*
    ): Interface.Actor = {
      import factory.server.backend._

      type ServerBackendFactory = NestProtocolServerBackendFactory[
        Interface.Role,
        Parameter1.Role,
        Parameter2.Role
      ]

      interface(classOf[ServerBackendFactory])(ctx, graceTo, resultTo, args*)     }

  }

}
