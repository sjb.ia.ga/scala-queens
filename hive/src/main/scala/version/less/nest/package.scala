package queens
package version.less
package nest


package object session {

  import protocol.base.Protocol.Group
  import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
  import protocol.session.Protocol.Role

  type Start[CFM >: Group <: ClientFrontendRole] = Actor[CFM] => Unit
  type Block[L, M >: Group <: Role] = Either[L, M] => Unit

}


package client {

  package object frontend {

    import dimensions.three.breed.Hatch

    type Apply = (Nest, Hatch[?]) => Unit

  }

}


object Implicits {

  import interface.ObjectNestProtocolIfce

  implicit object NestProtocolIfceImplicit
      extends ObjectNestProtocolIfce[protocol.NestMessage] { interface =>

    import factory.session._
    import protocol.session.{ Protocol => Session }

    type SessionFactory = NestProtocolSessionFactory[
      Session.Role
    ]

    interface(
      classOf[SessionFactory],
      impl.NestProtocolSessionImpl
    )

    import factory.session.adapter._
    import protocol.session.adapter.{ Protocol => SessionAdapter }

    type SessionAdapterFactory = NestProtocolSessionAdapterFactory[
      SessionAdapter.Role
    ]

    interface(
      classOf[SessionAdapterFactory],
      impl.NestProtocolSessionAdapterImpl
    )

  }

}


package session {

  object Implicits {

    implicit object NestSessionErrorCodeImplicit
        extends protocol.session.NestSessionErrorCode

    import interface.ObjectNestProtocolSessionIfce

    implicit object NestProtocolSessionIfceImplicit
        extends ObjectNestProtocolSessionIfce { interface =>

      {
        import factory.server._

        import protocol.session.{ Protocol => Parameter }
        import protocol.server.{ Protocol => Interface }

        type ServerFactory = NestProtocolServerFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(
          classOf[ServerFactory],
          impl.NestProtocolServerImpl
        )
      }

      {
        import factory.client._

        import protocol.server.{ Protocol => Parameter }
        import protocol.client.{ Protocol => Interface }

        type ClientFactory = NestProtocolClientFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(
          classOf[ClientFactory],
          impl.NestProtocolClientImpl
        )
      }

      {
        import factory.client.frontend._

        import protocol.client.{ Protocol => Parameter }
        import protocol.client.frontend.{ Protocol => Interface }

        type ClientFrontendFactory = NestProtocolClientFrontendFactory[
          Interface.Role,
          Parameter.Role
        ]

        interface(
          classOf[ClientFrontendFactory],
          impl.ClientNestProtocolFrontendImpl
        )
      }

    }

  }

  package adapter {

    object Implicits {

      implicit object NestSessionAdapterErrorCodeImplicit
          extends protocol.session.adapter.NestSessionAdapterErrorCode

    }

  }

}


package server {

  object Implicits {

    implicit object NestServerErrorCodeImplicit
        extends protocol.server.NestServerErrorCode

    import interface.ObjectNestProtocolServerIfce

    implicit object NestProtocolServerIfceImplicit
        extends ObjectNestProtocolServerIfce { interface =>

      import factory.server.defer._

      import protocol.client.defer.{ Protocol => Parameter }
      import protocol.server.defer.{ Protocol => Interface }

      type ServerDeferFactory = NestProtocolServerDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(
        classOf[ServerDeferFactory],
        impl.ServerNestProtocolDeferImpl
      )

    }

  }

  package defer {

    object Implicits {

      implicit object NestServerDeferErrorCodeImplicit
          extends protocol.server.defer.NestServerDeferErrorCode

      import interface.ObjectServerNestProtocolDeferIfce

      implicit object ServerNestProtocolDeferIfceImplicit
          extends ObjectServerNestProtocolDeferIfce { interface =>

        import factory.server.backend._

        import protocol.server.defer.{ Protocol => Parameter1 }
        import protocol.client.defer.{ Protocol => Parameter2 }
        import protocol.server.backend.{ Protocol => Interface }

        type ServerBackendFactory = NestProtocolServerBackendFactory[
          Interface.Role,
          Parameter1.Role,
          Parameter2.Role
        ]

        interface(
          classOf[ServerBackendFactory],
          impl.ServerNestProtocolBackendImpl
        )

      }

    }

  }

  package backend {

    object Implicits {

      implicit object NestServerBackendErrorCodeImplicit
          extends protocol.server.backend.NestServerBackendErrorCode

    }

  }

}


package client {

  object Implicits {

    implicit object NestClientErrorCodeImplicit
        extends protocol.client.NestClientErrorCode

    import interface.ObjectNestProtocolClientIfce

    implicit object NestProtocolClientIfceImplicit
        extends ObjectNestProtocolClientIfce { interface =>

      import factory.client.defer._

      import protocol.client.frontend.{ Protocol => Parameter }
      import protocol.client.defer.{ Protocol => Interface }

      type ClientDeferFactory = NestProtocolClientDeferFactory[
        Interface.Role,
        Parameter.Role
      ]

      interface(
        classOf[ClientDeferFactory],
        impl.ClientNestProtocolDeferImpl
      )

    }

  }

  package defer {

    object Implicits {

      implicit object NestClientDeferErrorCodeImplicit
          extends protocol.client.defer.NestClientDeferErrorCode

    }

  }

  package frontend {

    object Implicits {

      implicit object NestClientFrontendErrorCodeImplicit
          extends protocol.client.frontend.NestClientFrontendErrorCode

    }

  }

}
