package queens
package version.less
package nest
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.ServerNestProtocolBackend

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.Role
import protocol.server.backend._


class ServerNestProtocolBackendImpl[
  EC <: NestServerBackendErrorCode,
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
](
  override protected val graceTo: Actor[SDM],
  override protected val resultTo: Actor[CDM]
)(implicit
  override protected val errorCode: EC
) extends ServerNestProtocolBackend[EC, M, SDM, CDM] {

  class ServerNestBackendBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val secret: java.util.UUID
  ) extends ServerNestBackendBehavior[ServerNestBackendBehaviorImpl]

  private object ServerNestBackendInflater
      extends ServerNestBackendInflater[ServerNestBackendBehaviorImpl] {

    override def apply(em: Arguments): ServerNestBackendBehaviorImpl =
      new ServerNestBackendBehaviorImpl(em._1, em._2)

  }

  private object ServerNestBackendBehaviorImpl
      extends ObjectRolePlaneBehavior[ServerNestBackendBehaviorImpl, ServerNestBackendInflater.type] {

    override val inflater = ServerNestBackendInflater

  }

}


import factory.server.backend._

import protocol.server.defer.{ Protocol => Parameter1 }
import protocol.client.defer.{ Protocol => Parameter2 }
import protocol.server.backend.{ Protocol => Interface }

object ServerNestProtocolBackendImpl extends NestProtocolServerBackendFactory[
  Role,
  Parameter1.Role,
  Parameter2.Role
] {

  override def apply(
    context: ActorContext[?],
    graceTo: Parameter1.Actor,
    resultTo: Parameter2.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import server.backend.Implicits._

      new ServerNestProtocolBackendImpl[
        NestServerBackendErrorCode,
        Role,
        Parameter1.Role,
        Parameter2.Role
      ](
        graceTo,
        resultTo
      ).ServerNestBackendBehaviorImpl(args*)

    }

}
