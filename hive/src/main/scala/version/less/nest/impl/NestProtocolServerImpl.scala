package queens
package version.less
package nest
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.NestProtocolServer
import base.interface.NestProtocolServerIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.session.Protocol.{ Role => SessionRole }
import protocol.server.Protocol.Role
import protocol.server._


class NestProtocolServerImpl[
  EC <: NestServerErrorCode,
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole,
  SDM >: Group <: ServerDeferRole,
  SM >: Group <: SessionRole,
  I <: NestProtocolServerIfce[SDM, CDM]
](
  override protected val session: Actor[SM]
)(implicit
  override protected val errorCode: EC,
  override protected val interface: I
) extends NestProtocolServer[EC, M, CDM, SDM, SM, I] {

  class ServerNestBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val end: version.base.Role => Boolean
  ) extends ServerNestBehavior[ServerNestBehaviorImpl]

  private object ServerNestInflater
      extends ServerNestInflater[ServerNestBehaviorImpl] {

    override def apply(em: Arguments): ServerNestBehaviorImpl =
      new ServerNestBehaviorImpl(em._1, em._2)

  }

  private object ServerNestBehaviorImpl
      extends ObjectRolePlaneBehavior[ServerNestBehaviorImpl, ServerNestInflater.type] {

    override val inflater = ServerNestInflater

  }

}


import factory.server._

import protocol.session.adapter.{ Protocol => Parameter }
import protocol.server.{ Protocol => Interface }

object NestProtocolServerImpl extends NestProtocolServerFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    session: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import interface.ObjectNestProtocolServerIfce
      import server.Implicits._

      new NestProtocolServerImpl[
        NestServerErrorCode,
        Role,
        protocol.client.defer.Protocol.Role,
        protocol.server.defer.Protocol.Role,
        Parameter.Role,
        ObjectNestProtocolServerIfce
      ](
        session
      ).ServerNestBehaviorImpl(args*)
    }

}
