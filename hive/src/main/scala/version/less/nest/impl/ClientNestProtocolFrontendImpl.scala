package queens
package version.less
package nest
package impl

import akka.actor.typed.scaladsl.ActorContext

import client.frontend.Apply

import base.ClientNestProtocolFrontend

import protocol.base.Protocol.Group
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.Role
import protocol.client.frontend._


class ClientNestProtocolFrontendImpl[
  EC <: NestClientFrontendErrorCode,
  M >: Group <: Role,
  CM >: Group <: ClientRole
](
  override protected val client: Actor[CM],
)(implicit
  override protected val errorCode: EC
) extends ClientNestProtocolFrontend[EC, M, CM] {

  class ClientNestFrontendBehaviorImpl(
    override protected val end: version.base.Role => Boolean,
    _apply: Apply
  ) extends ClientNestFrontendBehavior[ClientNestFrontendBehaviorImpl] {

    override protected val apply = _apply

  }

  private object ClientNestFrontendInflater
      extends ClientNestFrontendInflater[ClientNestFrontendBehaviorImpl] {

    override def apply(em: Arguments): ClientNestFrontendBehaviorImpl =
      new ClientNestFrontendBehaviorImpl(em._1, em._2)

  }

  private object ClientNestFrontendBehaviorImpl
      extends ObjectRolePlaneBehavior[ClientNestFrontendBehaviorImpl, ClientNestFrontendInflater.type] {

    override val inflater = ClientNestFrontendInflater

  }

}


import factory.client.frontend._

import protocol.client.frontend.{ Protocol => Interface }
import protocol.client.{ Protocol => Parameter }

object ClientNestProtocolFrontendImpl extends NestProtocolClientFrontendFactory[
  Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    client: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import nest.client.frontend.Implicits._

      new ClientNestProtocolFrontendImpl[
        NestClientFrontendErrorCode,
        Role,
        Parameter.Role
      ](
        client
      ).ClientNestFrontendBehaviorImpl(args*)

    }

}
