package queens
package version.less
package nest
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.NestProtocolClient
import base.interface.NestProtocolClientIfce

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.client.Protocol.Role
import protocol.client._


class NestProtocolClientImpl[
  EC <: NestClientErrorCode,
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole,
  CDM >: Group <: ClientDeferRole,
  SM >: Group <: ServerRole,
  I <: NestProtocolClientIfce[CDM, CFM]
](
  override protected val server: Actor[SM]
)(implicit
  override protected val errorCode: EC,
  override protected val interface: I
) extends NestProtocolClient[EC, M, CFM, CDM, SM, I] {

  class ClientNestBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val end: version.base.Role => Boolean
  ) extends ClientNestBehavior[ClientNestBehaviorImpl]

  private object ClientNestInflater
      extends ClientNestInflater[ClientNestBehaviorImpl] {

    override def apply(em: Arguments): ClientNestBehaviorImpl =
      new ClientNestBehaviorImpl(em._1, em._2)

  }

  private object ClientNestBehaviorImpl
      extends ObjectRolePlaneBehavior[ClientNestBehaviorImpl, ClientNestInflater.type] {

    override val inflater = ClientNestInflater

  }

}


import factory.client._

import protocol.server.{ Protocol => Parameter }
import protocol.client.{ Protocol => Interface }

object NestProtocolClientImpl extends NestProtocolClientFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    server: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import interface.ObjectNestProtocolClientIfce
      import client.Implicits._

      new NestProtocolClientImpl[
        NestClientErrorCode,
        Role,
        protocol.client.frontend.Protocol.Role,
        protocol.client.defer.Protocol.Role,
        Parameter.Role,
        ObjectNestProtocolClientIfce
      ](
        server
      ).ClientNestBehaviorImpl(args*)
    }

}
