package queens
package version.less
package nest
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.ServerNestProtocolDefer
import base.interface.ServerNestProtocolDeferIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.server.defer.Protocol.Role
import protocol.server.defer._


class ServerNestProtocolDeferImpl[
  EC <: NestServerDeferErrorCode,
  M >: Group <: Role,
  SBM >: Group <: ServerBackendRole,
  CDM >: Group <: ClientDeferRole,
  I <: ServerNestProtocolDeferIfce[SBM, M, CDM]
](
  override protected val readyTo: Actor[CDM]
)(implicit
  override protected val errorCode: EC,
  override protected val interface: I
) extends ServerNestProtocolDefer[EC, M, SBM, CDM, I] {

  class ServerNestDeferBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val secret: java.util.UUID
  ) extends ServerNestDeferBehavior[ServerNestDeferBehaviorImpl]

  private object ServerNestDeferInflater
      extends ServerNestDeferInflater[ServerNestDeferBehaviorImpl] {

    override def apply(em: Arguments): ServerNestDeferBehaviorImpl =
      new ServerNestDeferBehaviorImpl(em._1, em._2)

  }

  private object ServerNestDeferBehaviorImpl
      extends ObjectRolePlaneBehavior[ServerNestDeferBehaviorImpl, ServerNestDeferInflater.type] {

    override val inflater = ServerNestDeferInflater

  }

}


import factory.server.defer._

import protocol.client.defer.{ Protocol => Parameter }
import protocol.server.defer.{ Protocol => Interface }

object ServerNestProtocolDeferImpl extends NestProtocolServerDeferFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    readyTo: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import interface.ObjectServerNestProtocolDeferIfce
      import server.defer.Implicits._

      new ServerNestProtocolDeferImpl[
        NestServerDeferErrorCode,
        Interface.Role,
        protocol.server.backend.Protocol.Role,
        Parameter.Role,
        ObjectServerNestProtocolDeferIfce,
      ](
        readyTo
      ).ServerNestDeferBehaviorImpl(args*)

    }

}
