package queens
package version.less
package nest
package impl

import akka.actor.typed.scaladsl.ActorContext

import base.ClientNestProtocolDefer

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.client.defer.Protocol.Role
import protocol.client.defer._


class ClientNestProtocolDeferImpl[
  EC <: NestClientDeferErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  SDM >: Group <: ServerDeferRole,
  SBM >: Group <: ServerBackendRole,
  CFM >: Group <: ClientFrontendRole
](
  override protected val onBehalf: Actor[CFM]
)(implicit
  override protected val errorCode: EC
) extends ClientNestProtocolDefer[EC, M, SM, SDM, SBM, CFM] {

  class ClientNestDeferBehaviorImpl(
    override protected val cryptoKey: CryptoKey,
    override protected val nest: Nest
  ) extends ClientNestDeferBehavior[ClientNestDeferBehaviorImpl]

  private object ClientNestDeferInflater
      extends ClientNestDeferInflater[ClientNestDeferBehaviorImpl] {

    override def apply(em: Arguments): ClientNestDeferBehaviorImpl =
      new ClientNestDeferBehaviorImpl(em._1, em._2)

  }

  private object ClientNestDeferBehaviorImpl
      extends ObjectRolePlaneBehavior[ClientNestDeferBehaviorImpl, ClientNestDeferInflater.type] {

    override val inflater = ClientNestDeferInflater

  }

}


import factory.client.defer._

import protocol.client.frontend.{ Protocol => Parameter }
import protocol.client.defer.{ Protocol => Interface }

object ClientNestProtocolDeferImpl extends NestProtocolClientDeferFactory[
  Interface.Role,
  Parameter.Role
] {

  override def apply(
    context: ActorContext[?],
    onBehalf: Parameter.Actor,
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import client.defer.Implicits._

      new ClientNestProtocolDeferImpl[
        NestClientDeferErrorCode,
        Interface.Role,
        protocol.server.Protocol.Role,
        protocol.server.defer.Protocol.Role,
        protocol.server.backend.Protocol.Role,
        Parameter.Role
      ](
        onBehalf
      ).ClientNestDeferBehaviorImpl(args*)

    }

}
