package queens
package version.less
package nest
package impl

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import base.NestProtocolSessionAdapter

import protocol.base.Protocol.Group
import protocol.session.adapter.Protocol.Role
import protocol.session.adapter._


class NestProtocolSessionAdapterImpl[
  EC <: NestSessionAdapterErrorCode,
  M >: Group <: Role
](
  override protected val session: ActorRef[Either[?, M]]
)(implicit
  override protected val errorCode: EC
) extends NestProtocolSessionAdapter[EC, M] {

  class SessionAdapterNestBehaviorImpl(
    override protected val end: version.base.Role => Boolean
  ) extends SessionAdapterNestBehavior[SessionAdapterNestBehaviorImpl]

  private object SessionAdapterNestInflater
      extends SessionAdapterNestInflater[SessionAdapterNestBehaviorImpl] {

    override def apply(em: Arguments): SessionAdapterNestBehaviorImpl =
      new SessionAdapterNestBehaviorImpl(em._1)

  }

  private object SessionAdapterNestBehaviorImpl
      extends ObjectRolePlaneBehavior[SessionAdapterNestBehaviorImpl, SessionAdapterNestInflater.type] {

    override val inflater = SessionAdapterNestInflater

  }

}


import factory.session.adapter._

import protocol.session.adapter.{ Protocol => Interface }

object NestProtocolSessionAdapterImpl
    extends NestProtocolSessionAdapterFactory[Role] {

  override def apply(
    context: ActorContext[?],
    session: ActorRef[Either[?, Role]],
    args: Any*
  ): Interface.Actor = context
    .spawnAnonymous {
      import nest.session.adapter.Implicits._

      new NestProtocolSessionAdapterImpl[
        NestSessionAdapterErrorCode,
        Role,
      ](
        session
      ).SessionAdapterNestBehaviorImpl(args*)

    }

}
