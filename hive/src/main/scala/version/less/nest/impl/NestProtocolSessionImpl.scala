package queens
package version.less
package nest
package impl

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import base.NestProtocolSession
import base.interface.NestProtocolSessionIfce

import session.{ Start, Block }
import client.frontend.Apply

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role
import protocol.session._


class NestProtocolSessionImpl[L,
  EC <: NestSessionErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole,
  I <: NestProtocolSessionIfce[M, SM, CM, CFM]
](using
  override protected val errorCode: EC,
  override protected val interface: I
) extends NestProtocolSession[L, EC, M, SM, CM, CFM, I] {

  class SessionNestBehaviorImpl(
    override protected val start: session.Start[CFM],
    override protected val end: version.base.Role => Boolean,
    override protected val block: session.Block[L, M],
    override protected val apply: client.frontend.Apply
  ) extends SessionNestBehavior[SessionNestBehaviorImpl]

  private object SessionNestInflater
      extends SessionNestInflater[SessionNestBehaviorImpl] {

    override def apply(em: Arguments): SessionNestBehaviorImpl =
      new SessionNestBehaviorImpl(em._1, em._2, em._3, em._4)

  }

  private object SessionNestBehaviorImpl
      extends ObjectSessionPlaneBehavior[SessionNestBehaviorImpl, SessionNestInflater.type] {

    override val inflater = SessionNestInflater

  }

}


import factory.session._

import protocol.session.{ Protocol => Interface }

object NestProtocolSessionImpl
    extends NestProtocolSessionFactory[Role] {

  override def apply[L](
    context: ActorContext[?],
    args: Any*
  ): Interface.Actor[L] = context
    .spawnAnonymous {
      import interface.ObjectNestProtocolSessionIfce
      import session.Implicits._

      new NestProtocolSessionImpl[L,
        NestSessionErrorCode,
        Role,
        protocol.server.Protocol.Role,
        protocol.client.Protocol.Role,
        protocol.client.frontend.Protocol.Role,
        ObjectNestProtocolSessionIfce
      ].SessionNestBehaviorImpl(args*)

    }

}
