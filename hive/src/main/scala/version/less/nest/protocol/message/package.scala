package queens
package version.less
package nest.protocol
package message


package salute {

  import server.defer.Protocol.{ Role => NestServerDeferRole }

  import server.backend.Protocol.{ Role => NestServerBackendRole }

  abstract trait NestMatch
      extends NestServerDeferRole
      with NestServerBackendRole

  import version.v1.vector.protocol.server.defer.Protocol.{ Role => VectorV1ServerDeferRole }

  import version.v1.vector.protocol.server.backend.Protocol.{ Role => VectorV1ServerBackendRole }

  abstract trait NestReadySaluteMatch
      extends NestMatch
      with VectorV1ServerDeferRole
      with VectorV1ServerBackendRole

  import base.Protocol.{ Group => NestGroup }

  abstract trait NestMessage
      extends NestGroup

  object Protocol {

    type Match = NestMatch
    type Message = NestMessage

    object Ready {

      object Salute {

        type Match = NestReadySaluteMatch

      }

    }

  }

}


package breed {

  import client.defer.Protocol.{ Role => NestClientDeferRole }

  import server.backend.Protocol.{ Role => NestServerBackendRole }

  abstract trait HatchMatch
      extends NestClientDeferRole
      with NestServerBackendRole

  import base.Protocol.{ Group => NestGroup }

  abstract trait HatchMessage
      extends NestGroup

  object Protocol {

    type Match = HatchMatch
    type Message = HatchMessage

  }

}
