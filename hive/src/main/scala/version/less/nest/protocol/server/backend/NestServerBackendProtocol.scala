package queens
package version.less
package nest.protocol
package server.backend


import plane.protocol.server.backend.PlaneServerBackendProtocol

abstract trait NestServerBackendProtocol
    extends NestProtocol
    with PlaneServerBackendProtocol
    with communication.NestProtocol_ServerBackend_ClientDefer


import plane.protocol.server.backend.PlaneServerBackendErrorCode

abstract trait NestServerBackendErrorCode
    extends NestErrorCode
    with PlaneServerBackendErrorCode {

  val dupNest = ProtocolDupVal()

}
