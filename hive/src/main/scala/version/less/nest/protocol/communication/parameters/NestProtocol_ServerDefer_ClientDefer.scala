package queens
package version.less
package nest.protocol
package communication.parameters


import message.salute.{ NestProtocol => MsgNestProtocol }

abstract trait NestProtocol_ServerDefer_ClientDefer
    extends MsgNestProtocol
