package queens
package version.less
package nest.protocol
package communication


abstract trait NestProtocol_ServerBackend_ClientDefer
    extends parameters.NestProtocol_ServerDefer_ClientDefer
    with NestProtocol_ClientDefer_ServerBackend
