package queens
package version.less
package nest.protocol
package client


import plane.protocol.client.PlaneClientProtocol

abstract trait NestClientProtocol
    extends NestProtocol
    with PlaneClientProtocol


import plane.protocol.client.PlaneClientErrorCode

abstract trait NestClientErrorCode
    extends NestErrorCode
    with PlaneClientErrorCode
