package queens
package version.less
package nest.protocol
package defer


import plane.protocol.defer.PlaneDeferProtocol

abstract trait NestDeferProtocol
    extends NestProtocol
    with PlaneDeferProtocol


import plane.protocol.defer.PlaneDeferErrorCode

abstract trait NestDeferErrorCode
    extends NestErrorCode
    with PlaneDeferErrorCode
