package queens
package version.less
package nest.protocol
package server


import plane.protocol.server.PlaneServerProtocol

abstract trait NestServerProtocol
    extends NestProtocol
    with PlaneServerProtocol


import plane.protocol.server.PlaneServerErrorCode

abstract trait NestServerErrorCode
    extends NestErrorCode
    with PlaneServerErrorCode
