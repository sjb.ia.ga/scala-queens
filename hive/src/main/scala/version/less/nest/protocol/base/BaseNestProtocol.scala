package queens
package version.less
package nest.protocol
package base


import version.base.protocol.BaseProtocol

abstract trait BaseNestProtocol
    extends BaseProtocol


import version.base.protocol.BaseErrorCode

abstract trait BaseNestErrorCode
    extends BaseErrorCode
