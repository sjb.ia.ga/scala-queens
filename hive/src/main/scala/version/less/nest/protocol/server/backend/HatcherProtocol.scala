package queens
package version.less
package nest.protocol
package server.backend

import version.base.protocol.BaseErrorCode


abstract trait HatcherErrorCode
    extends BaseErrorCode {

  def isntFlavor(v: Enumeration#Value) = ProtocolIsntVal(v)

}
