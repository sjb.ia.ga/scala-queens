package queens
package version.less
package nest.protocol

import nest.{ Actor => NestActor }


abstract trait NestMessage
    extends session.Protocol.Role
    with client.Protocol.Role
    with client.defer.Protocol.Role
    with client.frontend.Protocol.Role
    with server.Protocol.Role
    with server.defer.Protocol.Role
    with server.backend.Protocol.Role


package base {

  abstract trait NestGroup
      extends NestMessage
      with version.v1.vector.protocol.base.Protocol.Group
      with message.salute.Protocol.Match
      with message.salute.Protocol.Ready.Salute.Match
      with message.breed.Protocol.Match

  object Protocol {

    type Group = NestGroup

  }

}


package client {

  import plane.protocol.client.Protocol.{ Role => ClientRole }

  abstract trait NestClientRole
      extends ClientRole

  object Protocol {

    type Role = NestClientRole
    private[nest] type Actor = NestActor[Role]

  }

  package defer {

    import plane.protocol.client.defer.Protocol.{ Role => ClientDeferRole }

    abstract trait NestClientDeferRole
        extends ClientDeferRole

    object Protocol {

      type Role = NestClientDeferRole
      private[nest] type Actor = NestActor[Role]

    }

  }

  package frontend {

    import plane.protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }

    abstract trait NestClientFrontendRole
        extends ClientFrontendRole

    object Protocol {

      type Role = NestClientFrontendRole
      private[nest] type Actor = NestActor[Role]

    }

  }

}


package server {

  import plane.protocol.server.Protocol.{ Role => ServerRole }

  abstract trait NestServerRole
      extends ServerRole

  object Protocol {

    type Role = NestServerRole
    private[nest] type Actor = NestActor[Role]

  }

  package defer {

    import plane.protocol.server.defer.Protocol.{ Role => ServerDeferRole }

    abstract trait NestServerDeferRole
        extends ServerDeferRole

    object Protocol {

      type Role = NestServerDeferRole
      private[nest] type Actor = NestActor[Role]

    }

  }

  package backend {

    import plane.protocol.server.backend.Protocol.{ Role => ServerBackendRole }

    abstract trait NestServerBackendRole
        extends ServerBackendRole

    object Protocol {

      type Role = NestServerBackendRole
      private[nest] type Actor = NestActor[Role]

    }

  }

}


package session {

  import plane.protocol.session.Protocol.{ Role => SessionRole }

  abstract trait NestSessionRole
      extends SessionRole

  object Protocol {

    import akka.actor.typed.ActorRef

    type Role = NestSessionRole
    private[nest] type Actor[L] = ActorRef[Either[L, Role]]

  }

  package adapter {

    object Protocol {

      type Role = session.Protocol.Role
      private[nest] type Actor = NestActor[Role]

    }

  }

  package object adapter {

    type NestSessionAdapterProtocol = NestSessionProtocol
    type NestSessionAdapterErrorCode = NestSessionErrorCode

  }

}
