package queens
package version.less
package nest.protocol
package client.defer


import defer.NestDeferProtocol
import plane.protocol.client.defer.PlaneClientDeferProtocol

abstract trait NestClientDeferProtocol
    extends NestDeferProtocol
    with PlaneClientDeferProtocol
    with communication.NestProtocol_ClientDefer_ServerBackend


import defer.NestDeferErrorCode
import plane.protocol.client.defer.PlaneClientDeferErrorCode

abstract trait NestClientDeferErrorCode
    extends NestDeferErrorCode
    with PlaneClientDeferErrorCode
    with communication.NestErrorCode_ClientDefer_ServerBackend
