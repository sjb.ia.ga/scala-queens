package queens
package version.less
package nest.protocol
package communication


import message.breed.{ HatchProtocol => MsgHatchProtocol }

abstract trait NestProtocol_ClientDefer_ServerBackend
    extends MsgHatchProtocol


import message.breed.{ HatchErrorCode => MsgHatchErrorCode }

abstract trait NestErrorCode_ClientDefer_ServerBackend
    extends MsgHatchErrorCode


