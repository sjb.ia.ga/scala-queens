package queens
package version.less
package nest.protocol
package client.frontend


import plane.protocol.client.frontend.PlaneClientFrontendProtocol

abstract trait NestClientFrontendProtocol
    extends NestProtocol
    with PlaneClientFrontendProtocol


import plane.protocol.client.frontend.PlaneClientFrontendErrorCode

abstract trait NestClientFrontendErrorCode
    extends NestErrorCode
    with PlaneClientFrontendErrorCode
