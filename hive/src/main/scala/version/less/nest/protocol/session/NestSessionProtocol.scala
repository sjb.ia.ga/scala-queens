package queens
package version.less
package nest.protocol
package session


import plane.protocol.session.PlaneSessionProtocol

abstract trait NestSessionProtocol
    extends NestProtocol
    with PlaneSessionProtocol


import plane.protocol.session.PlaneSessionErrorCode

abstract trait NestSessionErrorCode
    extends NestErrorCode
    with PlaneSessionErrorCode
