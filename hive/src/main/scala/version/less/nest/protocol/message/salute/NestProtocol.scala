package queens
package version.less
package nest.protocol
package message.salute

import java.util.UUID


abstract trait NestProtocol
    extends nest.protocol.NestProtocol:

  import NestProtocol._

  inline final def Nest(secret: UUID) = new Nest(secret)


object NestProtocol:

  final case class Nest(secret: UUID)
      extends Protocol.Message


abstract trait NestErrorCode
    extends nest.protocol.NestErrorCode
