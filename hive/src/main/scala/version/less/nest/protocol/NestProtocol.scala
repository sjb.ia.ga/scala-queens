package queens
package version.less
package nest.protocol


import base.BaseNestProtocol

abstract trait NestProtocol
    extends BaseNestProtocol


import base.BaseNestErrorCode

abstract trait NestErrorCode
    extends BaseNestErrorCode
