package queens
package version.less
package nest.protocol
package message.breed

import dimensions.three.breed.Hatch.Flavor
import nest.Nest


abstract trait HatchProtocol
    extends NestProtocol:

  import HatchProtocol._

  inline final def Hatch(
    nest: Nest,
    flavor: Flavor.Value = Flavor()
  ) = new Hatch(nest, flavor)


object HatchProtocol:

  final case class Hatch(
    nest: Nest,
    flavor: Flavor.Value = Flavor()
  ) extends Protocol.Message


abstract trait HatchErrorCode
    extends NestErrorCode
