package queens
package version.less
package nest.protocol
package server.defer


import defer.NestDeferProtocol
import plane.protocol.server.defer.PlaneServerDeferProtocol

abstract trait NestServerDeferProtocol
    extends NestDeferProtocol
    with PlaneServerDeferProtocol
    with communication.parameters.NestProtocol_ServerDefer_ClientDefer


import defer.NestDeferErrorCode
import plane.protocol.server.defer.PlaneServerDeferErrorCode

abstract trait NestServerDeferErrorCode
    extends NestDeferErrorCode
    with PlaneServerDeferErrorCode
