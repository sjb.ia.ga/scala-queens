package queens
package version.less
package nest
package base

import akka.actor.typed.scaladsl.ActorContext

import dimensions.three.breed.Hatcher

import plane.base.ServerPlaneProtocolBackend

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.Role
import protocol.server.backend._


abstract trait ServerNestProtocolBackend[
  EC <: NestServerBackendErrorCode,
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
] extends NestServerBackendProtocol
    with ServerPlaneProtocolBackend[EC, M, SDM, CDM] {

  protected abstract trait ServerNestBackendBehavior[T <: ServerNestBackendBehavior[T]]
      extends ServerPlaneBackendBehavior[T]
      with Hatcher { self: Hatcher =>

    import version.less.nest.protocol.message.salute.{ NestProtocol => nm }
    import version.less.nest.protocol.message.breed.{ HatchProtocol => hm }

    import errorCode._

    protected def nest_perceive_salute(ctx: ActorContext[M], msg: M): Nothing = msg match {

      case nm.Nest(it) =>
        if saluted
        then
          resultTo ! dupNest
        else
          saluted = true

          if it == secret
          then
            same
          else
            resultTo ! badSecret

        stopped


      case _ =>
        none

    }

    protected def nest_perceive(ctx: ActorContext[M], msg: M): Nothing = msg match {

      case hm.Hatch(nest, flavor: Enumeration#Value) =>
        self(nest, flavor) match {
          case Some(hatch) =>
            resultTo ! Result(hatch)
          case _ =>
            resultTo ! isntFlavor(flavor)
        }
        graceTo ! Grace(nest.axes)
        stopped


      case _ =>
        none

    }

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        nest_perceive_salute(ctx, msg)
      } catch {
        case NoneBehavior =>
          try {
            super.errorBehavior(resultTo, msg)
          } catch {
            case NoneBehavior =>
              try {
                nest_perceive(ctx, msg)
              } catch {
                case NoneBehavior =>
                  resultTo ! Complaint(msg)
                  same
              }
          }
      }

  }

  protected abstract trait ServerNestBackendInflater[T <: ServerNestBackendBehavior[T]]
      extends RolePlaneInflater[T]
      with NestDeseqServerBackend

}
