package queens
package version.less
package nest
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import nest.{ Nest, Wildcard }
import version.base.Multiplier

import nest.Apply

import plane.base.interface.PlaneProtocolIfce

import protocol.base.Protocol.Group
import protocol.session.Protocol.Role


abstract trait NestProtocolIfce[
  M >: Group <: Role
] extends PlaneProtocolIfce:

  protected abstract trait ObjectNestSessionExpand
      extends ObjectSessionExpand { self =>

    def apply(
      context: ActorContext[?],
      end: version.base.Role => Boolean,
      stop: () => Unit,
      block: M => Unit,
      apply: Apply,
      wildcard: Wildcard = Wildcard.*,
      multiplier: Multiplier[Nest] = Multiplier.Once()
    ): Int

  }

  override def SessionExpand: ObjectNestSessionExpand
