package queens
package version.less
package nest
package base

import akka.actor.typed.TypedActorContext
import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import plane.base.PlaneProtocolSession
import interface.NestProtocolSessionIfce
import interface.NestSessionStart

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role
import protocol.session._


abstract trait NestProtocolSession[L,
  EC <: NestSessionErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole,
  I <: NestProtocolSessionIfce[M, SM, CM, CFM]
] extends NestSessionProtocol
    with PlaneProtocolSession[L, EC, M, I] {

  protected abstract trait SessionNestBehavior[T <: SessionNestBehavior[T]]
      extends SessionPlaneBehavior[T] {

    protected val start: session.Start[CFM]
    protected val block: session.Block[L, M]
    protected val apply: client.frontend.Apply

    import version.less.plane.protocol.message.session.{ SessionProtocol => sm }

    import errorCode._

    protected def nest_perceive(ctx: ActorContext[?], msg: M): Nothing = msg match {

      case sm.Start(self: Actor[M]) =>

        val NestSessionStart(server: Actor[SM], client: Actor[CM], frontend: Actor[CFM]) =
          interface.StartNest[L](ctx, self, start, end, block, apply)

        start(frontend)
        same


      case _ =>
        none

    }

    override protected def perceive(ctx: ActorContext[?], msg: M): Nothing =
      try {
        super.errorBehavior({ it => block(Right(it)) }, msg)
      } catch {
        case NoneBehavior =>
          try {
            nest_perceive(ctx, msg)
          } catch {
            case NoneBehavior =>
              block(Right(msg))
              same
          }
      }

    override def receive(ctx: TypedActorContext[Either[L, M]], msg: Either[L, M]): B = msg match {

      case Right(_) =>
        super.receive(ctx.asScala, msg)

      case Left(_) =>
        block(msg)
        Behaviors.same.asInstanceOf[B]

    }

  }

  protected abstract trait SessionNestInflater[T <: SessionNestBehavior[T]]
      extends SessionPlaneInflater[T]
      with NestDeseqSession[L, M, CFM]

}
