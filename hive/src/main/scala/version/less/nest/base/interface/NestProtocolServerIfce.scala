package queens
package version.less
package nest
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.Role


abstract trait NestProtocolServerIfce[
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole
] extends NodeIfce {

  protected abstract trait ObjectPlaneNest {

    def apply(
      ctx: ActorContext[?],
      readyTo: Actor[CDM],
      args: Any*
    ): Actor[M]

  }

  def PlaneNest: ObjectPlaneNest

}
