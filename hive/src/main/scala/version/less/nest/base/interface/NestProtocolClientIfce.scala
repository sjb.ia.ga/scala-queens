package queens
package version.less
package nest
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.Role


abstract trait NestProtocolClientIfce[
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole
] extends NodeIfce {

  protected abstract trait ObjectBreedNest {

    def apply(
      ctx: ActorContext[?],
      onBehalf: Actor[CFM],
      args: Any*
    ): Actor[M]

  }

  def BreedNest: ObjectBreedNest

}
