package queens
package version.less
package nest
package base

import akka.actor.typed.scaladsl.ActorContext

import plane.base.ClientPlaneProtocolFrontend

import protocol.base.Protocol.Group
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.Role
import protocol.client.frontend._


abstract trait ClientNestProtocolFrontend[
  EC <: NestClientFrontendErrorCode,
  M >: Group <: Role,
  CM >: Group <: ClientRole
] extends NestClientFrontendProtocol
    with ClientPlaneProtocolFrontend[EC, M, CM]:

  protected abstract trait ClientNestFrontendBehavior[T <: ClientNestFrontendBehavior[T]]
      extends ClientPlaneFrontendBehavior[T]:

    import dimensions.three.breed.Hatch
    import nest.{ Nest, Wildcard }
    import version.base.Multiplier

    private var count = 0

    import version.less.plane.protocol.message.expand.{ ExpandProtocol => em }
    import version.less.plane.protocol.message.append.{ AppendProtocol => am }

    import errorCode._

    protected def nest_perceive(ctx: ActorContext[M], msg: M): Nothing = msg match

      case em.Expand(wildcard: Wildcard, Multiplier(factor)) =>
        val self: Actor[M] = ctx.self

        val ns =
          for
            it <- wildcard(factor)
          yield it

        assert(count == 0)

        for (it <- ns) {
          client ! Breed(it, onBehalf = self)
          count += 1
        }

        if count == 0 then
          client ! Grace(wildcard.axes)

          if end(role) then stopped

        same


      case am.Append(nest: Nest, hatch: Hatch[?]) if count > 0 =>
        count -= 1

        apply(nest, hatch)

        if count == 0 then
          client ! Grace(nest.axes)

          if end(role) then stopped

        same


      case am.Append(_: Nest, _: Hatch[?]) =>
        client ! overflow
        stopped


      case _ =>
        none


    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try
        super.errorBehavior(client, msg)
      catch
        case NoneBehavior =>
          try
            nest_perceive(ctx, msg)
          catch
            case NoneBehavior =>
              try
                super.errorBehavior(client, msg, after = true)
              catch
                case NoneBehavior =>
                  client ! Complaint(msg)
                  same


  protected abstract trait ClientNestFrontendInflater[T <: ClientNestFrontendBehavior[T]]
      extends RolePlaneInflater[T]
      with NestDeseqClientFrontend
