package queens
package version.less
package nest
package base

import akka.actor.typed.scaladsl.ActorContext

import nest.Nest

import plane.base.PlaneProtocolClient
import interface.NestProtocolClientIfce

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.client.Protocol.Role
import protocol.client._


abstract trait NestProtocolClient[
  EC <: NestClientErrorCode,
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole,
  CDM >: Group <: ClientDeferRole,
  SM >: Group <: ServerRole,
  I <: NestProtocolClientIfce[CDM, CFM]
] extends NestClientProtocol
    with PlaneProtocolClient[EC, M, SM, I] {

  protected abstract trait ClientNestBehavior[T <: ClientNestBehavior[T]]
      extends ClientPlaneBehavior[T] {

    import version.less.plane.protocol.message.breed.{ BreedProtocol => bm }
    import version.less.plane.protocol.message.grace.{ GraceProtocol => gm }

    import errorCode._

    protected def nest_perceive(ctx: ActorContext[M], msg: M): Nothing = msg match {

      case bm.Breed(nest: Nest, onBehalf: Actor[CFM]) =>
        val deferTo: Actor[CDM] = interface
          .BreedNest(ctx, onBehalf, cryptoKey, nest)

        server ! Plane(deferTo, nest.axes)
        same


      case gm.Grace(axes) if axes == Nest.axes =>
        server ! Grace(axes)

        if end(role) then stopped
        same


      case _ =>
        none

    }

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        super.errorBehavior(server, msg)
      } catch {
        case NoneBehavior =>
          try {
            nest_perceive(ctx, msg)
          } catch {
            case NoneBehavior =>
              try {
                super.errorBehavior(server, msg, after = true)
              } catch {
                case NoneBehavior =>
                  server ! Complaint(msg)
                  same
              }
          }
      }

  }

  protected abstract trait ClientNestInflater[T <: ClientNestBehavior[T]]
      extends RolePlaneInflater[T]
      with NestDeseqClient

}
