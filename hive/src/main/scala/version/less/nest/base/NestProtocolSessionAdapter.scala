package queens
package version.less
package nest
package base

import plane.base.PlaneProtocolSessionAdapter

import protocol.base.Protocol.Group
import protocol.session.adapter.Protocol.Role
import protocol.session.adapter._


abstract trait NestProtocolSessionAdapter[
  EC <: NestSessionAdapterErrorCode,
  M >: Group <: Role
] extends NestSessionAdapterProtocol
    with PlaneProtocolSessionAdapter[EC, M] {

  protected abstract trait SessionAdapterNestBehavior[T <: SessionAdapterNestBehavior[T]]
      extends SessionAdapterPlaneBehavior[T]

  protected abstract trait SessionAdapterNestInflater[T <: SessionAdapterNestBehavior[T]]
      extends SessionAdapterPlaneInflater[T]
      with NestDeseqSessionAdapter

}
