package queens
package version.less
package nest
package base

import akka.actor.typed.scaladsl.ActorContext

import plane.base.ServerPlaneProtocolDefer
import interface.ServerNestProtocolDeferIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.server.defer.Protocol.Role
import protocol.server.defer._


abstract trait ServerNestProtocolDefer[
  EC <: NestServerDeferErrorCode,
  M >: Group <: Role,
  SBM >: Group <: ServerBackendRole,
  CDM >: Group <: ClientDeferRole,
  I <: ServerNestProtocolDeferIfce[SBM, M, CDM]
] extends NestServerDeferProtocol
    with ServerPlaneProtocolDefer[EC, M, CDM, I] {

  protected abstract trait ServerNestDeferBehavior[T <: ServerNestDeferBehavior[T]]
      extends ServerPlaneDeferBehavior[T] {

    import version.less.plane.protocol.message.resume.{ ResumeProtocol => rm }
    import version.less.plane.protocol.message.grace.{ GraceProtocol => gm }

    import errorCode._

    protected def nest_errorBehavior(replyTo: Actor[CDM], msg: M): Nothing = msg match {

      case rm.Resume(axes) if axes != nest.Nest.axes =>
        replyTo ! invalidPlane(axes)
        stopped


      case gm.Grace(axes) if axes != nest.Nest.axes =>
        replyTo ! invalidPlane(axes)
        stopped


      case _ =>
        none

    }

    protected def nest_perceive(ctx: ActorContext[M], msg: M): Nothing = msg match {

      case rm.Resume(axes) if axes == nest.Nest.axes =>
        val self: Actor[M] = ctx.self

        val nestAt: Actor[SBM] = interface
          .ResumeNest(ctx, graceTo = self, resultTo = readyTo, cryptoKey, secret)

        readyTo ! Ready(nestAt, Nest(_))
        same


      case gm.Grace(axes) if axes == nest.Nest.axes =>
        stopped


      case _ =>
        none

    }

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        nest_errorBehavior(readyTo, msg)
      } catch {
        case NoneBehavior =>
          try {
            super.errorBehavior(readyTo, msg)
          } catch {
            case NoneBehavior =>
              try {
                nest_perceive(ctx, msg)
              } catch {
                case NoneBehavior =>
                  readyTo ! Complaint(msg)
                  same
              }
          }
      }

  }

  protected abstract trait ServerNestDeferInflater[T <: ServerNestDeferBehavior[T]]
      extends RolePlaneInflater[T]
      with NestDeseqServerDefer

}
