package queens
package version.less
package nest
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.Role


abstract trait ServerNestProtocolDeferIfce[
  M >: Group <: Role,
  SDM >: Group <: ServerDeferRole,
  CDM >: Group <: ClientDeferRole
] extends NodeIfce {

  protected abstract trait ObjectResumeNest {

    def apply(
      ctx: ActorContext[?],
      graceTo: Actor[SDM],
      resultTo: Actor[CDM],
      args: Any*
    ): Actor[M]

  }

  def ResumeNest: ObjectResumeNest

}
