package queens
package version.less
package nest


package object base {

  import version.base.{ `Deseq*`, `DeseqPlane*`, DeseqUUID }

  type NestDeseqClient = `Deseq*`

  type NestDeseqClientDefer = `DeseqPlane*`[Nest]

  type NestDeseqServer = `Deseq*`

  type NestDeseqServerDefer = DeseqUUID

  type NestDeseqServerBackend = DeseqUUID

  import plane.base.PlaneDeseqSessionAdapter

  type NestDeseqSessionAdapter = PlaneDeseqSessionAdapter

}
