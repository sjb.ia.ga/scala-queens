package queens
package version.less
package nest
package base
package interface

import akka.actor.typed.scaladsl.ActorContext

import version.base.interface.NodeIfce

import protocol.base.Protocol.Group
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.client.Protocol.{ Role => ClientRole }
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role


final case class NestSessionStart[
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole
](
  server: Actor[SM],
  client: Actor[CM],
  frontend: Actor[CFM]
)


abstract trait NestProtocolSessionIfce[
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  CM >: Group <: ClientRole,
  CFM >: Group <: ClientFrontendRole
] extends NodeIfce {

  protected abstract trait ObjectStartNest {

    def apply[L](
      ctx: ActorContext[?],
      self: Actor[M],
      args: Any*
    ): NestSessionStart[SM, CM, CFM]

  }

  def StartNest: ObjectStartNest

}
