package queens
package version.less
package nest
package base

import version.base.Deseq
import version.less.plane.base.PlaneDeseqSession3

import session.{ Start, Block }
import client.frontend.Apply

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.session.Protocol.Role


abstract trait NestDeseqSession[L,
  M >: Group <: Role,
  CFM >: Group <: ClientFrontendRole
] extends Deseq {

  private object Deseq012
      extends PlaneDeseqSession3[L, M, CFM]

  override type Arguments = Tuple4[
    Start[CFM],
    version.base.Role => Boolean,
    Block[L, M],
    Apply
  ]

  override def apply(args: Seq[Any]): Arguments = {
    val (start, end, block) = Deseq012(args.take(3))
    val apply = args(3).asInstanceOf[Apply]
    (start, end, block, apply)
  }

}
