package queens
package version.less
package nest
package base

import java.util.UUID

import akka.actor.typed.scaladsl.ActorContext

import nest.Nest

import common.cipher.client.CipherClient

import plane.base.ClientPlaneProtocolDefer

import protocol.base.Protocol.Group
import protocol.client.frontend.Protocol.{ Role => ClientFrontendRole }
import protocol.server.Protocol.{ Role => ServerRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.backend.Protocol.{ Role => ServerBackendRole }
import protocol.client.defer.Protocol.Role
import protocol.client.defer._


abstract trait ClientNestProtocolDefer[
  EC <: NestClientDeferErrorCode,
  M >: Group <: Role,
  SM >: Group <: ServerRole,
  SDM >: Group <: ServerDeferRole,
  SBM >: Group <: ServerBackendRole,
  CFM >: Group <: ClientFrontendRole
] extends NestClientDeferProtocol
    with ClientPlaneProtocolDefer[EC, M, CFM] {

  protected abstract trait ClientNestDeferBehavior[T <: ClientNestDeferBehavior[T]]
      extends ClientPlaneDeferBehavior[T] {

    protected val nest: Nest

    import version.less.plane.protocol.message.defer.{ DeferProtocol => dm }
    import version.less.plane.protocol.message.plane.{ PlaneProtocol => pm }
    import version.less.plane.protocol.message.ready.{ ReadyProtocol => ym }
    import version.less.plane.protocol.message.result.{ ResultProtocol => rm }

    import errorCode._

    protected def nest_errorBehavior(replyTo: Actor[CFM], msg: M): Nothing = msg match {

      case dm.Defer(_, pm.Plane(_, axes), _) if axes != Nest.axes =>
        replyTo ! invalidPlane(axes)
        stopped

      case _ =>
        none

    }

    protected def nest_perceive(ctx: ActorContext[M], msg: M): Nothing = msg match {

      case dm.Defer(resumeAt: Actor[SDM], pm.Plane(_, axes), it) if axes == Nest.axes =>
        CipherClient(cryptoKey, it) match
          case Some(uuid) =>
            secret = uuid
            resumeAt ! Resume(axes)
            same
          case _ =>
            resumeAt ! errorSecret(role)
            stopped


      case ym.Ready(nestAt: Actor[SBM], hiMessage: Function[UUID, SBM] @unchecked) =>
        nestAt ! hiMessage(secret)
        nestAt ! Hatch(nest)
        same


      case rm.Result(hatch: dimensions.three.breed.Hatch[?]) =>
        onBehalf ! Append(nest, hatch)
        stopped


      case _ =>
        none

    }

    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try {
        nest_errorBehavior(onBehalf, msg)
      } catch {
        case NoneBehavior =>
          try {
            super.errorBehavior(onBehalf, msg)
          } catch {
            case NoneBehavior =>
              try {
                nest_perceive(ctx, msg)
              } catch {
                case NoneBehavior =>
                  onBehalf ! Complaint(msg)
                  stopped
              }
          }
      }

  }

  protected abstract trait ClientNestDeferInflater[T <: ClientNestDeferBehavior[T]]
      extends RolePlaneInflater[T]
      with NestDeseqClientDefer

}
