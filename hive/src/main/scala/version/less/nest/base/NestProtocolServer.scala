package queens
package version.less
package nest
package base

import akka.actor.typed.scaladsl.ActorContext

import nest.Nest

import common.cipher.client.CipherClient

import plane.base.PlaneProtocolServer
import interface.NestProtocolServerIfce

import protocol.base.Protocol.Group
import protocol.session.Protocol.{ Role => SessionRole }
import protocol.client.defer.Protocol.{ Role => ClientDeferRole }
import protocol.server.defer.Protocol.{ Role => ServerDeferRole }
import protocol.server.Protocol.Role
import protocol.server._


abstract trait NestProtocolServer[
  EC <: NestServerErrorCode,
  M >: Group <: Role,
  CDM >: Group <: ClientDeferRole,
  SDM >: Group <: ServerDeferRole,
  SM >: Group <: SessionRole,
  I <: NestProtocolServerIfce[SDM, CDM]
] extends NestServerProtocol
    with PlaneProtocolServer[EC, M, SM, I]:

  protected abstract trait ServerNestBehavior[T <: ServerNestBehavior[T]]
      extends ServerPlaneBehavior[T]:

    import version.less.plane.protocol.message.plane.{ PlaneProtocol => pm }
    import version.less.plane.protocol.message.grace.{ GraceProtocol => gm }

    import errorCode._

    protected def nest_perceive(ctx: ActorContext[M], msg: M): Nothing = msg match

      case pm.Plane(deferTo: Actor[CDM], axes) if axes == Nest.axes =>
        val secret = java.util.UUID.randomUUID

        val resumeAt: Actor[SDM] = interface
          .PlaneNest(ctx, readyTo = deferTo, cryptoKey, secret)

        CipherClient(cryptoKey, secret) match
          case Some(it) =>
            deferTo ! Defer(resumeAt, msg, it)
            same
          case _ =>
            deferTo ! errorSecret(role)
            stopped



      case gm.Grace(axes) if axes == Nest.axes =>
        session ! Stop()
        if end(role) then stopped
        same


      case _ =>
        none


    override protected def perceive(ctx: ActorContext[M], msg: M): Nothing =
      try
        super.errorBehavior(session, msg)
      catch
        case NoneBehavior =>
          try
            nest_perceive(ctx, msg)
          catch
            case NoneBehavior =>
              try
                super.errorBehavior(session, msg, after = true)
              catch
                case NoneBehavior =>
                  session ! Complaint(msg)
                  same


  protected abstract trait ServerNestInflater[T <: ServerNestBehavior[T]]
      extends RolePlaneInflater[T]
      with NestDeseqServer
