package queens
package conf.mi

import base.Board


abstract trait Conf
    extends conf.emito.Conf:

  import dimensions.fourth.fifth.breed.Idling

  override type P <: Conf

  override type T <: Idling[P]


package flow:

  case class Parameters(override val board: Board)
      extends Conf
      with conf.emito.flow.Conf:

    import dimensions.fourth.fifth.breed.Idling

    override type P = this.type

    override type T = Idling[P]
