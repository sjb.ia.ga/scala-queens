package queens
package conf.mco

import base.Board


abstract trait Conf
    extends conf.emito.Conf
    with dimensions.fifth.output.console.conf.Conf:

  import dimensions.fourth.fifth.breed.`Console'o`

  override type P <: Conf

  override type T <: `Console'o`[P]


package actors:

  import akka.actor.typed.scaladsl.ActorContext

  import conf.actors.Callbacks

  case class Parameters(override val board: Board, override val it: (ActorContext[?], Callbacks))
      extends Conf
      with conf.emito.actors.Conf:

    import dimensions.fourth.fifth.breed.`Console'o`

    override type P = this.type

    override type T = `Console'o`[P]


  object Parameters:

    def apply(ctx: ActorContext[?], cbs: Callbacks)
             (block: Parameters => Unit = { _ => })
             (using board: Board): Parameters =
      val params = new Parameters(board, (ctx, cbs))
      block(params)
      params


package futures:

  import scala.concurrent.ExecutionContext

  import conf.futures.Callbacks

  case class Parameters(override val board: Board, override val it: (ExecutionContext, Callbacks))
      extends Conf
      with conf.emito.futures.Conf:

    import dimensions.fourth.fifth.breed.`Console'o`

    override type P = this.type

    override type T = `Console'o`[P]


  object Parameters:

    def apply(cbs: Callbacks)
             (block: Parameters => Unit = { _ => })
             (using board: Board)
             (using ec: ExecutionContext): Parameters =
      val params = new Parameters(board, (ec, cbs))
      block(params)
      params


package flow:

  case class Parameters(override val board: Board)
      extends Conf
      with conf.emito.flow.Conf:

    import dimensions.fourth.fifth.breed.`Console'o`

    override type P = this.type

    override type T = `Console'o`[P]


  object Parameters:

    def apply(block: Parameters => Unit = { _ => })
             (using board: Board): Parameters =
      val params = new Parameters(board)
      block(params)
      params


package kafka:

  import org.apache.avro.generic.GenericRecord
  import org.apache.kafka.clients.producer.KafkaProducer

  case class Parameters(override val board: Board, override val it: KafkaProducer[String, GenericRecord])
      extends Conf
      with conf.emito.kafka.Conf:

    import dimensions.fourth.fifth.breed.`Console'o`

    override type P = this.type

    override type T = `Console'o`[P]


  object Parameters:

    def apply(producer: KafkaProducer[String, GenericRecord])
             (block: Parameters => Unit = { _ => })
             (using board: Board): Parameters =
      val params = new Parameters(board, producer)
      block(params)
      params


package rabbitMQ:

  import com.rabbitmq.client.Channel

  case class Parameters(override val board: Board, override val it: Channel)
      extends Conf
      with conf.emito.rabbitMQ.Conf:

    import dimensions.fourth.fifth.breed.`Console'o`

    override type P = this.type

    override type T = `Console'o`[P]


  object Parameters:

    def apply(channel: Channel)
             (block: Parameters => Unit = { _ => })
             (using board: Board): Parameters =
      val params = new Parameters(board, channel)
      block(params)
      params


package awsSQS:

  import com.amazonaws.services.sqs.AmazonSQS

  case class Parameters(override val board: Board, override val it: AmazonSQS)
      extends Conf
      with conf.emito.awsSQS.Conf:

    import dimensions.fourth.fifth.breed.`Console'o`

    override type P = this.type

    override type T = `Console'o`[P]


  object Parameters:

    def apply(sqs: AmazonSQS)
             (block: Parameters => Unit = { _ => })
             (using board: Board): Parameters =
      val params = new Parameters(board, sqs)
      block(params)
      params
