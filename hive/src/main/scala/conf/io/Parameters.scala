package queens
package conf.io

import base.Board

import common.monad.`0`.`Monad'`


abstract trait Conf
    extends conf.emito.`0`.Conf:

  override type P <: Conf


package x:

  import cats.effect.Sync

  import monix.eval.{ Coeval, Task }

  abstract trait Conf[E[_]: Sync]
      extends conf.emito.`0`.x.Conf[E]:

    import dimensions.fourth.fifth.zeroth.breed.x.`I'o`

    override type P <: Conf[E]

    override type T = `I'o`[E, P]


  package flow:

    abstract trait Conf[E[_]: Sync]
        extends x.Conf[E]
        with conf.emito.`0`.x.flow.Conf[E]:

      override type P <: Conf[E]

    package sync:

      import java.nio.file.Path

      case class Parameters(override val board: Board,
                            override val tmpdir: Path)
          extends flow.Conf[Coeval]:

        override type P = this.type


      object Parameters:

        def apply(tmpdir: Path)(using board: Board): Parameters =
          new Parameters(board, tmpdir)

    package async:

      import java.nio.file.Path

      case class Parameters(override val board: Board,
                            override val tmpdir: Path)
          extends flow.Conf[Task]:

        override type P = this.type


      object Parameters:

        def apply(tmpdir: Path)(using board: Board): Parameters =
          new Parameters(board, tmpdir)


package s:

  abstract trait Conf
      extends conf.emito.`0`.s.Conf:

    import dimensions.fourth.fifth.zeroth.breed.s.`I'o`

    override type P <: Conf

    override type T = `I'o`[P]


  package flow:

    abstract trait Conf
        extends s.Conf
        with conf.emito.`0`.s.flow.Conf:

      override type P <: Conf

    import java.nio.file.Path

    case class Parameters(override val board: Board,
                          override val tmpdir: Path)
        extends flow.Conf:

      override type P = this.type


    object Parameters:

      def apply(tmpdir: Path)(using board: Board): Parameters =
        new Parameters(board, tmpdir)
