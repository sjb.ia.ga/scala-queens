package queens.dimensions.fourth.webapp.plainjava;

import scala.Option;
import scala.Function1;
import scala.util.Try;
import scala.collection.Map;
import scala.collection.immutable.Seq;
import scala.runtime.BoxedUnit;

import queens.common.pipeline.Metadata;

import queens.dimensions.fourth.app.Parameter;

import queens.dimensions.fourth.webapp.WebMethod;

import queens.dimensions.fourth.webapp.Web$u0020Client;


@SuppressWarnings("unchecked")
public abstract class Base$PlainJAVA$Web$Client<T, Q, WM extends WebMethod<T, Q, WM>>
    implements PlainJAVA$Web$Client<T, Q, WM> {

    protected abstract WM set(Option<Map<String, String>> query,
                              String media,
                              Seq<Parameter<?>> params,
                              String... url);

    @Override
    public Option<Metadata> $less$less(Metadata metadata) {
        return $less$less$(metadata, it -> set(it._1(), it._2(), it._3(), it._4()));
    }

    protected abstract Try<Q> get(WM it);

    @Override
    public Function1<Metadata, Function1<BoxedUnit, Object>> $greater$greater() {
        return $greater$greater$(it -> get(it));
    }
}
