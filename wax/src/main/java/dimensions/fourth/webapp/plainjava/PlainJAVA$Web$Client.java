package queens.dimensions.fourth.webapp.plainjava;

import java.util.function.Function;

import scala.Option;
import scala.Function1;
import scala.Tuple2;
import scala.Tuple3;
import scala.Tuple4;
import scala.util.Try;
import scala.collection.Map;
import scala.collection.immutable.Seq;
import scala.runtime.BoxedUnit;

import queens.common.pipeline.Metadata;
import queens.common.pipeline.Metadata.Boot;
import queens.common.pipeline.Metadata.Media;

import static queens.Util.*;

import queens.dimensions.fourth.app.Parameter;

import queens.dimensions.fourth.webapp.QueensWebAppInterface.Web;
import queens.dimensions.fourth.webapp.QueensWebAppInterface.Web$;
import queens.dimensions.fourth.webapp.WebMethod;

import queens.dimensions.fourth.webapp.Web$u0020Client;


@SuppressWarnings("unchecked")
public interface PlainJAVA$Web$Client<T, Q, WM extends WebMethod<T, Q, WM>>
    extends Web$u0020Client {

    default Option<Metadata> $less$less$(Metadata metadata, Function<Tuple4<Option<Map<String, String>>, String, Seq<Parameter<?>>, String[]>, WM> set) {
        if (metadata instanceof Boot boot)
            if (boot.context() instanceof Web<?, ?>) {
                Web<Q, WM> web = (Web<Q, WM>) boot.context();
                String media = web.media();
                Seq<Parameter<?>> params = web.params();

                if (boot.data().nonEmpty()) {
                    String key = (String) boot.data().get();
                    WM it;
                    if (web.url().isLeft()) {
                        Tuple3<String, Object, Map<String, String>> url = web.url().left().get();
                        String[] urls = { url._1(), (String) url._2() };
                        it = set.apply(Tuple4.apply(Option.apply(url._3()), media, params, urls));
                    } else {
                        String[] url = { web.url().right().get() };
                        it = set.apply(Tuple4.apply(Option.empty(), media, params, url));
                    }
                    return web.apply(Tuple2.apply(key, Option.apply(it)));
                } else
                    return Option.empty();
            } else
                return Option.empty();
        else
            return Option.empty();
    }

    default Function1<Metadata, Function1<BoxedUnit, Object>> $greater$greater$(Function<WM, Try<Q>> get) {
        return
        new Function1<Metadata, Function1<BoxedUnit, Object>>() {
            @Override
            public Function1<BoxedUnit, Object> apply(Metadata metadata) {
                if (metadata instanceof Media Media) {
                    if (Media.data().nonEmpty()) {
                        if (Media.data().get() instanceof WebMethod<?, ?, ?> wm) {
                            WM it = (WM) wm;
                            it.result_$eq(get.apply(it));
                            return const1(it.result().isSuccess());
                        } else
                            return const1(false);
                    } else
                        return const1(false);
                } else
                    return const1(false);
            }
        };
    }
}
