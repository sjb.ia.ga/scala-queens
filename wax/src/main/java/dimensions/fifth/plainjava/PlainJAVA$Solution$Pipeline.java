package queens.dimensions.fifth.plainjava;

import java.util.UUID;

import scala.Option;
import scala.Function1;
import scala.Tuple2;
import scala.collection.immutable.List;
import static scala.collection.JavaConverters.asJava;

import queens.base.Board;

import queens.common.Pipeline;
import queens.common.pipeline.Context;
import queens.common.pipeline.Metadata;
import queens.common.pipeline.Context.Round;

import static queens.Util.const1;


@SuppressWarnings("unchecked")
public abstract class PlainJAVA$Solution$Pipeline
    implements Pipeline<List<Tuple2<Object, Object>>> {

    private static queens.dimensions.Dimension.Model$ Model$ = queens.dimensions.Dimension.Model$.MODULE$;
    private static queens.dimensions.fifth.output.console.QueensConsoleOutput$ QCO$ = queens.dimensions.fifth.output.console.QueensConsoleOutput$.MODULE$;

    protected abstract boolean apply(java.util.List<Tuple2<Object, Object>> it, Round round, UUID uuid);

    @Override
    public Function1<Metadata, Function1<List<Tuple2<Object, Object>>, Object>> $greater$greater() {
        return
        new Function1<Metadata, Function1<List<Tuple2<Object, Object>>, Object>>() {
            @Override
            public Function1<List<Tuple2<Object, Object>>, Object> apply(Metadata metadata) {
                if (metadata.context() instanceof Round round) {
                    if (metadata.data().nonEmpty()) {
                        if (metadata.data().get() instanceof UUID uuid) {
                            return new Function1<List<Tuple2<Object, Object>>, Object>() {
                                @Override
                                public Object apply(List<Tuple2<Object, Object>> it) {
                                    return PlainJAVA$Solution$Pipeline.this.apply(asJava(it), round, uuid);
                                }
                            };
                        } else
                            return const1(false);
                    } else
                        return const1(false);
                } else
                    return const1(false);
            }
        };
    }

    @Override
    public Option<Function1<Context, Pipeline>> next() {
        return Option
            .apply(
                  new Function1<Context, Pipeline>() {
                      @Override
                      public Pipeline apply(Context ctxt) {
                          return QCO$.apply(ctxt);
                      }
                  }
                  );
    }
}
