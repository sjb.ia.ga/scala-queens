package queens;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Map.Entry;

import scala.Function0;
import scala.Function1;
import scala.Tuple2;
import scala.collection.immutable.List;
import scala.collection.Map;
import scala.collection.immutable.Nil$;
import scala.collection.immutable.Seq$;
import scala.collection.immutable.Seq;
import scala.runtime.BoxedUnit;
import static scala.runtime.BoxedUnit.UNIT;
import static scala.collection.JavaConverters.asScala;
import static scala.collection.JavaConverters.asJava;

import queens.dimensions.Dimension.Aspect;
import queens.dimensions.Dimension.Algorithm;


public final class Util {

    private static queens.dimensions.Dimension.Aspect$ Aspect$ = queens.dimensions.Dimension.Aspect$.MODULE$;
    private static queens.dimensions.Dimension.Algorithm$ Algorithm$ = queens.dimensions.Dimension.Algorithm$.MODULE$;

    public static <T> Seq<T> empty() {
        return (Seq<T>) Nil$.MODULE$.toSeq();
    }

    public static <T> Seq<T> singleton(T value) {
        return (Seq<T>) Seq$.MODULE$.fill(1, fun0(value));
    }

    public static <T> Function0<T> fun0(T value) {
        Function0<T> f =
            new Function0<T>() {
                @Override
                public T apply() {
                    return value;
                }
            };
        return f;
    }

    public static <S, T> Function1<S, T> const1(T result) {
        return new Function1<S, T>() {
            @Override
            public T apply(S __) {
                return result;
            }
        };
    };

    public static java.util.Map<String, java.util.List<String>> matrix(List<String> vars) {
        final LinkedList<Tuple2<String, String>> pairs = new LinkedList<>();
        vars.foreach(
            new Function1<String, BoxedUnit>() {
                @Override
                public BoxedUnit apply(String variable) {
                    Seq<String> pair = asScala(Arrays.asList(variable.split("="))).take(2).toSeq();
                    pairs.add(Tuple2.apply(pair.apply(0), pair.apply(1)));
                    return UNIT;
                }
            }
        );

        HashMap<String, Seq<String>> matrix = new HashMap<>();

        for (Tuple2<String, String> pair : pairs) {
            String key = pair._1();
            Seq<String> value;
            if (matrix.containsKey(key))
                value = matrix.get(key);
            else
                value = empty();
            matrix.put(key, value.appended(pair._2()).toSeq());
        }

        HashMap<String, java.util.List<String>> planes = new HashMap<>();

        for (java.util.Map.Entry<String, Seq<String>> entry : matrix.entrySet())
            planes.put(entry.getKey(), asJava(entry.getValue().toList()));

        return planes;
    }

    public static java.util.Map<String, java.util.Map<String, String>> uri2(Map<Aspect, Map<Algorithm, Object>> mat) {
        HashMap<String, java.util.Map<String, String>> result = new HashMap<>();
        for (Entry<Aspect, Map<Algorithm, Object>> at_mam : asJava(mat).entrySet()) {
            HashMap<String, String> expand = new HashMap<>();
            for (Entry<Algorithm, Object> am_n : asJava(at_mam.getValue()).entrySet()) {
                Integer n = (Integer) am_n.getValue();
                expand.put(am_n.getKey().toString(), n.toString());
            }
            result.put(at_mam.getKey().toString(), expand);
        }
        return result;
    }

}
