package queens
package dimensions.fifth.fourth.third
package cue.cycle
package actors

import java.util.UUID
import java.util.NoSuchElementException

import scala.collection.mutable.{ HashMap, ListBuffer, Queue }

import scala.concurrent.duration._
import scala.util.Success

import akka.actor.typed.{ ActorRef, Behavior }
import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }
import akka.util.Timeout

import org.reactivestreams.{ Publisher, Subscriber, Subscription }

import common.Macros.busywaitWhile


private[actors] class CueTag(
  stopper: Publisher[Unit],
  val ctx: ActorContext[Option[Either[ActorRef[Unit], (Long, Long)]]],
  val end: ActorRef[Unit],
  val timeout: Long,
  val poll: Long = 50L
)(
  implicit private val responseTimeout: Timeout = 1.second
):

  private object Stopper extends Subscriber[Unit]:
    override def onSubscribe(s: Subscription): Unit = { s.request(1) }
    override def onNext(t: Unit): Unit = {}
    override def onComplete(): Unit = {}
    override def onError(t: Throwable): Unit = {}

  def outOfSync(): Unit = stopper.subscribe(Stopper)

  import CueTag._

  val buf = HashMap[UUID, Queue[(Long, Long)]]()
  private val bin = ListBuffer[UUID]()

  val ref = ctx
    .spawnAnonymous[Either[(UUID, (Long, Long)), Either[CueReq, UUID]]](Behaviors
      .receiveMessage {
        case Left((uuid, it)) =>
          if !bin.contains(uuid) then
            if !buf.contains(uuid) then buf(uuid) = Queue[(Long, Long)]()
            buf(uuid).enqueue(it)
          Behaviors.same

        case Right(Left(CueReq(uuid, replyTo))) =>
          replyTo ! Some(Right {
            try
              buf(uuid).dequeue
            catch
              case _: NoSuchElementException =>
                -1L -> -1L
          })

          Behaviors.same

        case Right(Right(uuid)) =>
          buf -= uuid
          bin += uuid
          ack = true
          Behaviors.same
      }
    )

  var ack: Boolean = _

  def apply(uuid: UUID, prefetch: Boolean = false): Unit =
    ack = false
    if prefetch
    then
      ctx.ask[Either[(UUID, (Long, Long)), Either[CueReq, UUID]], Option[Either[ActorRef[Unit], (Long, Long)]]](ref, { replyTo => Right(Left(CueReq(uuid, replyTo))) }) {
        case Success(it) => it
        case _ => None
      }
    else
      ref ! Right(Right(uuid))
    busywaitWhile(!ack)(1L)


object CueTag:

  case class CueReq(uuid: UUID, replyTo: ActorRef[Option[Either[ActorRef[Unit], (Long, Long)]]])
