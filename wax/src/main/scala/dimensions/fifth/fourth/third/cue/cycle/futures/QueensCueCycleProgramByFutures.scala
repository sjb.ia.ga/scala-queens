package queens
package dimensions.fifth.fourth.third
package cue.cycle
package futures

import java.util.{ Date, UUID }

import scala.concurrent.ExecutionContext

import common.pipeline.Context.Round

import dimensions.third.first.Iteration1
import dimensions.third.first.futures.bis.{ Iteration2, Queens }

import dimensions.three.breed.{ Hatch, Hatcher }
import Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import futures.{ CueTag => Tag }

import pojo.parser.date.span

import dimensions.fifth.fourth.cue.cycle.QueensCueCycleProgram
import dimensions.fifth.fourth.third.cue.futures.QueensCueUseSolutionByFutures
import dimensions.fourth.third.program.cycle.futures.QueensCycleProgramInterfaceByFutures


abstract trait QueensCueCycleProgramByFutures
    extends QueensCueCycleProgram[Iteration1]
    with QueensCueUseSolutionByFutures
    with QueensCycleProgramInterfaceByFutures:

  override protected def apply(wildcard: Wildcard,
                               multiplier: Multiplier[Nest]
                             )(using
                               iteration: Iteration1
                             )(using
                               Flavor.Value): Long =
    var idle = 0L
    var started = Date()

    val Iteration2(_, tag: Tag, _, flg, _, (_, cbs)) = iteration
    var Iteration2(cycle, _, _, _, _, _) = iteration

    val tag2 = UUID.fromString(flg.slot)

    given ExecutionContext = cbs.executionContext

    for
      nest <- wildcard(multiplier)
      hatch = Hatcher.bis[Queens[?]](nest)
      queens = hatch()
    do
      given Simulate = QueensCueCycleProgramByFutures(tag, cycle.io._1._4)

      given Iteration1 = Iteration1(cycle +: iteration.params.tail*)

      var ended = Date()
      idle = idle.span(from = started, to = ended)

      cycle.idle = 0L

      started = Date()

      cycle.since = started

      queens { it ?=>
        ended = Date()

        val Some(Some((_, _, uuid, n, _, _))) = it[(?, ?, UUID, Long, ?, ?)](flg.slot)

        ||(it, Round(cycle, tag2, queens, n), Some(uuid))

        tag.idling(started, Left(ended -> n))

        cycle.idle = cycle.idle.span(from = ended)

        started = Date()
      }

      cbs(None)
      tag(cycle.io._1._4)

      cycle.idle = cycle.idle.span(from = started)

      ended = Date()

      idle += cycle.idle

      tag.idling(cycle.since, Right(cycle.idle))

      cycle = cycle.turn

      idle = idle.span(from = ended)

      started = Date()

    idle = idle.span(from = started)

    idle


  override def toString(): String = super.toString // plainjava


object QueensCueCycleProgramByFutures:

  import scala.Option.unless

  import common.bis.Cycle

  import common.Macros.{ tryc, busywaitWhile }

  import dimensions.fifth.cycle.{ Preemption, Suspension, Simulate, Simulation }

  def apply(tag: Tag, uuid: UUID): Simulate =

    val pre = Preemption {

      case ((_, (_, None)), ((number, _), Right(_))) =>
        Right(-1)

      case (_, (_, Right(_))) =>
        tag(uuid, prefetch = true)
        Left(1)

      case (_, ((number, _), Left(prev))) =>
        if prev == 0 then tag(uuid, prefetch = true)
        val (num, cue) = tag.cue
        if cue < 0
        then
          Left(0)
        else
          assert(num == number)
          Left(cue)
    }

    Simulate (
      Suspension(pre) {
        case ((Some(it: Cycle), io), _) if it.io != io =>
          tag.outOfSync()
          Some(Simulation.stop)
        case ((Some(it), _), _) if !it.isInstanceOf[Cycle] =>
          tag.outOfSync()
          Some(Simulation.stop)
        case ((Some(_), _), _) | ((_, (_, Some(_))), (_, Right(_))) =>
          unless {
            busywaitWhile(tag.buf.contains(uuid) -> tag.buf(uuid).isEmpty)(tag.timeout, tag.poll)()
          }(Simulation.stop)
        case _ =>
          None
      }
    )
