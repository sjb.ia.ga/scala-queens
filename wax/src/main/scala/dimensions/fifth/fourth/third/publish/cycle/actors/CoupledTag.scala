package queens
package dimensions.fifth.fourth.third
package publish.cycle
package actors

import java.util.UUID

import java.util.concurrent.atomic.AtomicLong

import java.util.concurrent.{ PriorityBlockingQueue => Queue }

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import common.Macros.tryc

import dimensions.fifth.fourth.third.publish.actors.Macros.{ Event, enqueue }


private[actors] class CoupledTag(
  val legacy: Boolean,
  val timeout: Long,
  val poll: Long = 50L
):

  var total = 0L

  def apply(pub: QueensCoupledPublisherCycleProgramByActors[?, ?],
            number: Long,
            ref: Event,
            io: IO)
     (using context: Ctx): Unit =
    val (_, _, spindle) = context
    if number < 0
    then
      tryc {
        pub(number, null, null).foreach { batch =>
          batch.dequeue(Some(()))
          //assert(batch.isEmpty)
        }
        total += -number-1
        spindle.decrementAndGet() // solver
        //assert(spindle.decrementAndGet() == 0)
      }
    else
      enqueue(pub, number, ref, io)
