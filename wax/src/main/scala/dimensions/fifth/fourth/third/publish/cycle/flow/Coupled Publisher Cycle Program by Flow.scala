package queens
package dimensions.fifth.fourth.third
package publish.cycle
package flow

import java.util.UUID

import base.Board

import common.bis.Cycle
import common.Flag
import common.Macros.trys

import dimensions.third.first.Iteration1

import dimensions.three.breed.Hatch.Flavor

import dimensions.fifth.publish.QueensPublisher

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import dimensions.fifth.third.publish.flow.given

import flow.{ CoupledTag => Tag }

import dimensions.fifth.fourth.publish.cycle.`Base.Publisher Cycle Program`


abstract trait `Base.Coupled Publisher Cycle Program by Flow`[
  T,
  CP <: QueensCoupledPublisher[T]
](
  name: String,
  legacy: Boolean
) extends `Base.Publisher Cycle Program`[Iteration1, Nest, Unit, T]
    with QueensCoupledPublisherCycleProgramByFlow[T, CP]:

  protected def preemption: CP

  def apply[R](wildcard: Wildcard,
               multiplier: Multiplier[Nest])
              (cycle: Cycle, timeout: Long)
              (block: QueensPublisher[T] => R)
        (using Flavor.Value): (R, Long) =

    val r = block(preemption)

    val key = UUID.randomUUID.toString

    val tag = Tag(legacy, timeout)

    given Iteration1 = new Iteration1(Cycle(cycle), tag, Flag(key), { () => })

    super.apply(wildcard, multiplier)

    r -> tag.total


  override def toString(): String = name + " " + super.toString
