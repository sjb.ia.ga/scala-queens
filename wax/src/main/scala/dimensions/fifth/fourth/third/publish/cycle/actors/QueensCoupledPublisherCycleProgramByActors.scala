package queens
package dimensions.fifth.fourth.third
package publish.cycle
package actors

import java.util.UUID

import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.{ PriorityBlockingQueue => Queue }

import akka.actor.typed.scaladsl.ActorContext

import dimensions.fifth.fourth.third.publish.actors.Macros.Event

import common.bis.Cycle

import common.pipeline.Context.Round

import common.Macros.tryc

import dimensions.third.first.Iteration1
import dimensions.third.first.actors.bis.{ Iteration2, Queens }

import dimensions.three.breed.{ Hatch, Hatcher }
import Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import actors.{ CoupledTag => Tag }

import dimensions.fifth.fourth.publish.cycle.{ QueensSolutionProcessor, QueensPublisherCycleProgram }
import dimensions.fifth.fourth.third.publish.actors.QueensPublisherUseSolutionByActors
import dimensions.fourth.third.program.cycle.actors.QueensCycleProgramInterfaceByActors


abstract trait QueensCoupledPublisherCycleProgramByActors[
  T,
  CP <: QueensCoupledPublisher[T]
] extends QueensPublisherCycleProgram[Iteration1, Queue[(Long, Event)], Ctx, T]
    with QueensPublisherUseSolutionByActors
    with QueensCycleProgramInterfaceByActors
    with QueensSolutionProcessor[Queue[(Long, Event)], Ctx]:

  protected def preemption: CP

  /**
    * Coupled with preemption.onMark.
    * @see [[queens.dimensions.fifth.fourth.third.publish.cycle.QueensCoupledPublisher#onMark]]
    */
  final override def onNext(it: IO): Unit =
    tryc(preemption.onMark(it))
    super.onNext(it)

  private var io: IO = null

  final override def onComplete(): Unit =
    tryc {
      if !preemption.noMark then
        preemption.onMark(io)
    }
    io = null
    super.onComplete()

  override protected def apply(wildcard: Wildcard,
                               multiplier: Multiplier[Nest])
                              (using
                               iteration: Iteration1)
                              (using
                               Flavor.Value): Long =
    val Iteration2(Cycle(io0, _), tag: Tag, _, flg, _, (ctx, cbs)) = iteration
    var Iteration2(cycle, _, _, _, _, _) = iteration

    try

      for
        nest <- wildcard(multiplier)
        hatch = Hatcher.bis[Queens[?]](nest)
        queens = hatch()
      do
        subscribe(this.asInstanceOf[org.reactivestreams.Subscriber[IO]])

        try

          given Simulate = QueensCoupledPublisherCycleProgramByActors(this, cbs, tag)

          given Iteration1 = Iteration1(cycle +: iteration.params.tail*)

          queens { it ?=>
            val Some((_, _, uuid, n, _, _)) = it[(?, ?, UUID, Long, ?, ?)](flg.slot).get

            ||(it, Round(cycle, common.NoTag, queens, n), Some(uuid))
          }

          cbs()

        finally
          if io ne null
          then
            io = null
            super.onComplete()

          reset
          preemption.reset
          cycle = cycle.turn

    finally
      cbs.ctx.value = ctx
      preemption.emit0(io0)
      preemption.emit()

    -1L


  override def toString(): String = "Coupled " + super.toString // plainjava


object QueensCoupledPublisherCycleProgramByActors:

  import common.bis.Cycle

  import conf.actors.bis.Callbacks

  import dimensions.fifth.cycle.{ Preemption, Suspension, Simulation, Simulate }

  import dimensions.fifth.publish.QueensPublisher.idle

  def apply[
    T,
    CP <: QueensCoupledPublisher[T]
  ](pub: QueensCoupledPublisherCycleProgramByActors[T, CP],
    cbs: Callbacks,
    tag: Tag
  ): Simulate =

    def pre = Preemption {

      case ((_, io @ (_, None)), ((number, Some(((ctx: ActorContext[?], spindle: AtomicLong), _))), Right(_))) =>
        tryc(pub.preemption.mark(io))
        pub.io = io

        given Ctx = (cbs.ctx, ctx, spindle)
        tag(pub, -(number+1), null, null)

        Right(-1)

      case ((_, io), ((number, Some(((ctx: ActorContext[?], spindle: AtomicLong), ref: Event))), Right(_))) =>
        tryc(pub.preemption.mark(io))

        given Ctx = (cbs.ctx, ctx, spindle)
        tag(pub, number, ref, io)

        Right(1)

      case _ =>
        tryc(pub.preemption.onNext(()))
        Left(1)

    }

    Simulate {
      Suspension(pre) {
        case ((Some(it: Cycle), io), _) if it.io != io =>
          Some(Simulation.stop)
        case ((Some(it), _), _) if !it.isInstanceOf[Cycle] =>
          Some(Simulation.stop)
        case ((Some(_), _), _) =>
          pub.preemption.idle(tag.timeout, tag.poll, legacy = tag.legacy)
        case _ =>
          None
      }
    }
