package queens
package dimensions.fifth.fourth.third
package cue.cycle

import java.util.Date

import java.util.concurrent.Flow.{ Publisher => FlowPublisher }

import org.reactivestreams.{ FlowAdapters, Publisher }

import scala.Function.const

import base.Board

import common.pipeline.Context

import dimensions.fifth.QueensUseSolution


class `Cue Cycle Processor` private (
  override protected val cues: Publisher[Cue],
  idling: ((Date, Either[(Date, Long), Long])) => Unit,
  override protected val next: Option[Context => QueensUseSolution]
)(using
  Boolean
) extends QueensCueCycleProcessor(idling):

  def this(
    cues: Publisher[Cue] | FlowPublisher[Cue]
  )(
    idling: ((Date, Either[(Date, Long), Long])) => Unit = const(())
  )(
    next: Option[Context => QueensUseSolution] = None
  )(using
    Boolean
  ) = this(
    if cues.isInstanceOf[Publisher[Cue]]
    then cues.asInstanceOf[Publisher[Cue]]
    else FlowAdapters.toPublisher(cues.asInstanceOf[FlowPublisher[Cue]]),
    idling,
    next)

  override def toString(): String = "Processor " + super.toString


object `Cue Cycle Processor`:

  def unapply(it: `Cue Cycle Processor`): Option[(Date, Date, Long)] = QueensCueCycleProcessor.unapply(it)

