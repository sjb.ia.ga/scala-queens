package queens
package dimensions.fifth.fourth.third
package publish.cycle
package flow

import common.Macros.tryc

import version.less.nest.Nest


private[flow] class CoupledTag(
  val legacy: Boolean,
  val timeout: Long,
  val poll: Long = 50L
):

  var total = 0L

  def apply(pub: QueensCoupledPublisherCycleProgramByFlow[?, ?],
            number: Long,
            nest: Nest,
            io: IO): Unit =
    if number < 0
    then
      tryc {
        pub(number, null, null)(using ())
        total += -number-1
      }
    else
      tryc(pub(number, nest, io)(using ()))
