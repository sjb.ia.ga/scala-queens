package queens
package dimensions.fifth.fourth.third
package web.simple
package flow

import java.util.Date
import java.util.UUID

import scala.Function.const
import scala.util.Success
import scala.reflect.ClassTag

import base.Board

import common.monad.flow._
import common.monad.tag.no.given

import dimensions.third.first.Iteration1
import dimensions.third.first.flow.{ Iteration, `Queens*` }
import Iteration.given

import dimensions.three.breed.{ Hatch, Hatcher }
import Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import conf.mi.flow.{ Parameters => Ps }
import dimensions.fourth.fifth.breed.Idling
import dimensions.fifth.QueensIdling.given

import dimensions.Dimension.Model.flow

import pojo.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }
import pojo.parser.date.span

import dimensions.fifth.fourth.web.simple.QueensWebSimpleProgram
import dimensions.fourth.third.program.simple.flow.QueensSimpleProgramInterfaceByFlow


abstract trait QueensWebSimpleProgramByFlow[
  O <: Queens[R, B, P, S, N, Q],
  B <: Board2[R, B],
  R <: Square2,
  S <: Solution2[R, B, P, S],
  P <: Point2,
  Q <: Queens2[R, B, P, S, N, Q],
  N <: Nest2
](using
  ct_o: ClassTag[O],
  ct_b: ClassTag[B],
  ct_r: ClassTag[R],
  ct_s: ClassTag[S],
  ct_p: ClassTag[P],
  ct_q: ClassTag[Q],
  ct_n: ClassTag[N]
) extends QueensWebSimpleProgram[Iteration1, O, B, R, S, P, Q, N]
    with QueensSimpleProgramInterfaceByFlow:

  override protected type T = java.lang.Long

  override protected def apply(wildcard: Wildcard,
                               multiplier: Multiplier[Nest]
                             )(using
                               iteration1: Iteration1
                             )(using
                               Long, Flavor.Value): T =
    var idle = 0L
    var started = Date()

    val iteration: Iteration = iteration1

    val Iteration(board, _, Success(tag: UUID), pre, run, _, _) = iteration

    for
      nest <- wildcard(multiplier)
      hatch = Hatcher[`Queens*`[?]](nest)
      queens = hatch()
    do
      var ended = Date()
      idle = idle.span(from = started, to = ended)

      def program: Long =
        given Boolean = pre
        given Ps = Ps(board)
        val idling: Idling[Ps] = Idling(flow)(Some(const(this)))(queens, tag, Nil, run)

        ended = idling()

        for
          it <- idling
        do
          idling(it) {
            this(tag, ended, Left(idling))

            ended = idling()
          }

        idling

      idle += this(tag, ended, Right(program))

      started = Date()

    idle = idle.span(from = started)

    idle

  override def toString(): String = super.toString // plainjava
