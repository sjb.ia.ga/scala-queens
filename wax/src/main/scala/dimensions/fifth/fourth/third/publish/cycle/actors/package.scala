package queens
package dimensions.fifth.fourth.third
package publish.cycle

import java.util.UUID

import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.{ PriorityBlockingQueue => Queue }

import common.Macros.trys

import dimensions.fifth.fourth.third.publish.actors.Macros.Event


package object actors:

  import dimensions.fifth.third.publish.actors.QueensBatchPublisherByActors

  type QueensSolutionPublisherByActors = QueensBatchPublisherByActors[IO]

  extension(self: Queue[(Long, Event)])
    def dequeue(msg: Option[Unit] = None) =
      val (_, it) = self.peek
      it ! msg.map(Left(_)).getOrElse(Right(None))
      while !self.isEmpty && (self.peek._2 eq it)
      do
        trys(1L)

  inline def fifo(): Queue[(Long, Event)] =
    new Queue[(Long, Event)](1, (i, j) => if i._1 < j._1 then -1 else if i._1 > j._1 then 1 else 0)

  inline def event(event: Event): Queue[(Long, Event)] =
    val r = fifo()
    r.add(-1L -> event)
    r
