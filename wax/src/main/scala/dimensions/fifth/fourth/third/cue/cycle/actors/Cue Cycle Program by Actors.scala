package queens
package dimensions.fifth.fourth.third
package cue.cycle
package actors

import java.util.UUID

import akka.actor.typed.{ ActorRef, Behavior }
import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import org.reactivestreams.Publisher

import common.bis.Cycle
import common.Flag

import dimensions.third.first.Iteration1
import conf.actors.bis.Callbacks

import dimensions.three.breed.Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import actors.{ CueTag => Tag }

import dimensions.fifth.fourth.cue.cycle.`Base.Cue Cycle Program`


abstract trait `Base.Cue Cycle Program by Actors`
    extends `Base.Cue Cycle Program`[Iteration1]
    with QueensCueCycleProgramByActors:

  protected def byActors(wildcard: Wildcard,
                         multiplier: Multiplier[Nest])
                        (stopper: Publisher[Unit])
                        (cycle: Cycle, timeout: Long)
                        (block: (((UUID, (Long, Long))) => Unit) => Unit)
                  (using Flavor.Value): Behavior[Option[Either[ActorRef[Unit], (Long, Long)]]] =
    var tag: Tag = null
    var cue: ActorRef[Unit] = null

    Behaviors
      .receive[Option[Either[ActorRef[Unit], (Long, Long)]]] {
        case (ctx, Some(Left(ref))) =>

          tag = Tag(stopper, ctx, end = ref, timeout)

          cue = ctx
            .spawnAnonymous[Unit](Behaviors
              .receive { (ctx, _) =>
                val cbs = Callbacks()

                val key = UUID.randomUUID.toString

                block(tag.ref ! Left(_))

                given Iteration1 = new Iteration1(Cycle(cycle), tag, Flag(key), { () => }, ctx -> cbs)

                try
                  super.apply(wildcard, multiplier)
                finally
                  tag.ctx.self ! None

                Behaviors.same
              }
            )

          cue ! ()

          Behaviors.same

        case (_, Some(Right(cue))) =>
          this.cue = Some(cue)
          tag.ack = true
          Behaviors.same

        case (ctx, _) =>
          ctx.stop(tag.ref)
          ctx.stop(cue)

          tag.end ! ()

          Behaviors.stopped
      }
