package queens
package dimensions.fifth.fourth.third
package publish.cycle
package futures

import java.util.UUID

import scala.concurrent.{ ExecutionContext, Future, SyncChannel }

import common.Macros.tryc

import conf.futures.bis.Callbacks


private[futures] class Tag[T](
  val t: T,
  val poll: Long = 50L,
  val timeout: Long = 5000L
):

  val nch = new SyncChannel[Long]()

  def apply(pub: QueensPublisherCycleProgramByFutures[T],
            cbs: Callbacks,
            numberOpt: Option[Long],
            future: Future[Unit],
            io: IO)
           (end: => Unit = {})
     (using ExecutionContext): Unit =
    val number = numberOpt.getOrElse(nch.read)
    if io eq null
    then
      tryc {
        val batch = pub(number, null, null)
        batch.foreach(_._1.write(()))
        cbs(batch.map(_._2))
        end
      }
    else
      Future { tryc(pub(number, ch0 -> future, io)) }

  private val ch0 = new SyncChannel[Unit]()
