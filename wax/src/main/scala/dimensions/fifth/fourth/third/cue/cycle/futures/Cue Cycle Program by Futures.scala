package queens
package dimensions.fifth.fourth.third
package cue.cycle
package futures

import java.util.Date
import java.util.UUID
import java.util.concurrent.ConcurrentHashMap

import scala.Function.{ const, tupled }
import scala.concurrent.ExecutionContext

import org.reactivestreams.Publisher

import common.bis.Cycle
import common.Flag

import conf.futures.bis.Callbacks

import dimensions.third.first.Iteration1

import dimensions.three.breed.Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import futures.{ CueTag => Tag }

import dimensions.fifth.fourth.cue.cycle.`Base.Cue Cycle Program`


abstract trait `Base.Cue Cycle Program by Futures`
    extends `Base.Cue Cycle Program`[Iteration1]
    with QueensCueCycleProgramByFutures:

  protected def byFutures(key: String = UUID.randomUUID.toString,
                          idling: ((Date, Either[(Date, Long), Long])) => Unit = const(()))
                         (wildcard: Wildcard,
                          multiplier: Multiplier[Nest])
                         (stopper: Publisher[Unit])
                         (cycle: Cycle, timeout: Long)
                         (block: (((UUID, (Long, Long))) => Unit) => Unit)
                   (using Flavor.Value)
                (implicit ec: ExecutionContext): Long =
    val cbs = Callbacks()

    val tag = Tag(idling, stopper, timeout)

    block(tupled(tag.apply))

    given Iteration1 = new Iteration1(Cycle(cycle), tag, Flag(key), { () => }, ec -> cbs)

    this(wildcard, multiplier)
