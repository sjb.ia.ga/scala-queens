package queens
package dimensions.fifth.fourth.third
package publish.cycle
package futures

import scala.concurrent.{ ExecutionContext, Future, SyncChannel }

import common.Macros.tryc

import conf.futures.bis.Callbacks


private[futures] class CoupledTag(
  val legacy: Boolean,
  val timeout: Long,
  val poll: Long = 50L
):

  var total = 0L

  val nch = SyncChannel[Long]()

  def apply(pub: QueensCoupledPublisherCycleProgramByFutures[?, ?],
            cbs: Callbacks,
            numberOpt: Option[Long],
            future: Future[Unit],
            io: IO)
     (using ExecutionContext): Unit =
    val number = numberOpt.getOrElse(nch.read)
    if number < 0
    then
      tryc {
        val batch = pub(number, null, null)
        batch.foreach(_._1.write(()))
        cbs(batch.map(_._2))
        total += -number-1
      }
    else
      Future { tryc(pub(number, ch0 -> future, io)) }

  private val ch0 = new SyncChannel[Unit]()
