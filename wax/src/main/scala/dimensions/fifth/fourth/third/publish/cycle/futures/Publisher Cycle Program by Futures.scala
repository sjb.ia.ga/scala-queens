package queens
package dimensions.fifth.fourth.third
package publish.cycle
package futures

import java.util.UUID

import scala.concurrent.{ ExecutionContext, Future, SyncChannel }

import base.Board

import common.bis.Cycle
import common.Flag

import conf.futures.bis.Callbacks

import dimensions.third.first.Iteration1

import dimensions.three.breed.Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import dimensions.fifth.fourth.publish.cycle.`Base.Publisher Cycle Program`


abstract trait `Base.Publisher Cycle Program by Futures`[T](t: T)
    extends `Base.Publisher Cycle Program`[Iteration1, (SyncChannel[Unit], Future[Unit]), ExecutionContext, T]
    with QueensPublisherCycleProgramByFutures[T]:

  protected def byFutures(wildcard: Wildcard,
                          multiplier: Multiplier[Nest])
                   (using board: Board)
                   (using Long, Flavor.Value)
                   (implicit ec: ExecutionContext): Unit =
    val cbs = Callbacks()

    val key = UUID.randomUUID.toString

    given Iteration1 = new Iteration1(Cycle(wildcard, multiplier), { () => Tag(t) }, Flag(key), { () => }, ec -> cbs)

    this(wildcard, multiplier)
