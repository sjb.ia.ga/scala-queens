package queens
package dimensions.fifth.fourth.third
package cue.cycle

import java.util.Date
import java.util.UUID
import java.util.Collections.sort
import java.util.LinkedList

import scala.reflect.ClassTag

import base.Board

import queens.given
import common.given

import dimensions.fourth.subscribe.QueensSubscriber.Subscriber
import dimensions.fourth.subscribe.QueensSubscription._
import dimensions.fifth.publish.QueensPublisher

import common.pipeline.Context.Round

import version.less.nest.Nest

import pojo.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }
import pojo.parser.date.jDate.{ spanFrom, apply }
import pojo.jPojo.given
import pojo.last

import dimensions.fifth.fourth.publish.cycle.QueensPojoProcessor


abstract trait QueensPojoSolutionProcessor[
  O <: Queens[R, B, P, S, N, Q],
  B <: Board2[R, B],
  R <: Square2,
  S <: Solution2[R, B, P, S],
  P <: Point2,
  Q <: Queens2[R, B, P, S, N, Q],
  N <: Nest2
](using
  ct_o: ClassTag[O],
  ct_b: ClassTag[B],
  ct_r: ClassTag[R],
  ct_s: ClassTag[S],
  ct_p: ClassTag[P],
  ct_q: ClassTag[Q],
  ct_n: ClassTag[N],
  b2b2: Conversion[Board, B]
) extends QueensPojoProcessor[O, B, R, S, P, Q, N]:

  private var cues: QueensPublisher[(IO, (Solution, Round))] = _

  private var result: LinkedList[Q] = _

  private var count: Long = _

  def apply(cues: (((Date, Either[(Date, Long), Long])) => Unit) => `Cue Cycle Processor`): this.type =

    result = LinkedList[Q]()

    this.cues = cues(
      {
        case (started, Left((ended, _))) =>
          val queens2 = result.last
          val solution2 = queens2.getSolutions.last
          solution2.getBoard()(started, ended)
          solution2(started, ended)

        case (started, Right(idle)) if !result.isEmpty =>
          val queens2 = result.last
          queens2(started, Date(), Some(idle))
      }
    )

    this


  override protected def onSubscriber(subscriber: Subscriber[O]): Unit = synchronized {
    if cues eq null
    then
      subscriber.onComplete()
  }


  override protected def onUpdate(): Long = synchronized {
    if cues ne null
    then
      val m = super.onUpdate()

      if subscription eq null
      then
        cues.subscribe(this.asInstanceOf[org.reactivestreams.Subscriber[(IO, (Solution, Round))]])

      count = m

      if m > 0
      then
        count = m max 3
        subscription.request(count)

    0L
  }


  override def onNext(t: (IO, (Solution, Round))): Unit =
    t match
      case (((board, _, _, _), _), (Nil, _)) =>
        count -= 1

        if count >= 0
        then

          val `Cue Cycle Processor`(started, ended, idle) = cues

          val response: O = ct_o

          response.setBoard(board)

          response.setQueens(result)
          response.setCount(result.size)

          sort(response.getBoard.getSquares)
          sort(response.getQueens)

          response.getBoard()(started, started)

          response(started, ended, Some(idle.spanFrom(ended)))

          result = LinkedList[Q]()

          emit(Some(response))

      case (((_, _, _, uuid), _), (it, Round(board, tag: UUID, queens, n))) =>
        import Board.apply

        val tags: List[String] = queens.toString

        val board2: B = board(it)
        sort(board2.getSquares)

        val solution2: S = it
        solution2.setNumber(n)
        solution2.setBoard(board2)
        sort(solution2.getQueens)

        if n == 1 then
          result.add {
            val nest2: N = Nest(queens.aspect, queens.algorithm, queens.model)
            val queens2: Q = ct_q
            queens2.setTag(tag.toString)
            queens2.setUUID(uuid)
            queens2.setNest(nest2)
            queens2.setTags(tags.sorted)
            queens2.setValidator(queens.validate.name)
            queens2.setSolutions(LinkedList[S]())
            queens2
        }

        result.last.getSolutions.add(solution2)

        subscription.request(count)

    super.onNext(t)


  override def onError(t: Throwable): Unit =
    synchronized {
      cues = null
      emit(t)
    }

    super.onError(t)


  override def onComplete(): Unit =
    synchronized {
      cues = null
      emit()
    }

    super.onComplete()
