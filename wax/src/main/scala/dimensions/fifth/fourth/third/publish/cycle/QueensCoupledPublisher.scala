package queens
package dimensions.fifth.fourth.third
package publish.cycle

import scala.collection.mutable.Queue

import dimensions.fifth.fourth.publish.cycle.{ QueensPreemptionPublisher, QueensPreemptionSubscriber }


abstract trait QueensCoupledPublisher[T]
    extends QueensPreemptionPublisher[T]
    with QueensPreemptionSubscriber[Unit]: // an interface, no subscription

  private var _offset = 0L
  protected def offset = _offset
  private var _number = 0L
  protected def number = _number
  private var marker = 0L
  private var passes = Queue[Long]()

  protected def at(it: IO, marker: Long): T
  protected def on(marker: Long): Unit
  protected def no(marker: Long): Boolean

  def reset: Unit =
    _offset = counter
    _number = 0L
    marker = 0L
    passes = Queue[Long]()

  final override def onError(t: Throwable): Unit = ???

  final override def onComplete(): Unit = ???

  /**
    * @see [[queens.dimensions.fifth.fourth.third.publish.cycle.actors.QueensCoupledPublisherCycleProgramByActors#onNext]]
    * @see [[queens.dimensions.fifth.fourth.third.publish.cycle.futures.QueensCoupledPublisherCycleProgramByFutures#onNext]]
    * @see [[queens.dimensions.fifth.fourth.third.publish.cycle.flow.QueensCoupledPublisherCycleProgramByFlow#onNext]]
    */
  final override def onMark(it: IO): Unit =
    emit(Some(at(it, passes.dequeue)))
    _number += 1

  final override def mark(_it: IO) =
    marker += 1
    on(marker)
    passes.enqueue(marker)

  final def noMark: Boolean = no(passes.front)

  def emit0(io: IO): Unit


object QueensCoupledPublisher:

  import dimensions.fifth.fourth.publish.cycle.QueensPreemptionSubscriber

  abstract trait QueensCoupledSubscriber[T]
      extends QueensPreemptionSubscriber[T]:

    final override protected def span(number: Long, prev: Long): Option[Long] = ???

    final override def onMark(_it: IO): Unit = ???

    override def onComplete(): Unit = {}

