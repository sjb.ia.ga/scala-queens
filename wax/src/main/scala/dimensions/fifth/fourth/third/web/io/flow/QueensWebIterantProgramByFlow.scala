package queens
package dimensions.fifth.fourth.third
package zeroth
package web.io
package flow

import java.nio.file.Files

import java.util.Date
import java.util.UUID

import scala.Function.const
import scala.util.Success
import scala.reflect.ClassTag

import base.Board

import common.monad.`0`.flow._
import common.monad.tag.uuid.given

import dimensions.third.first.Iteration1
import dimensions.third.first.zeroth.flow.Iteration0
import Iteration0.given

import dimensions.three.breed.{ Hatch, Hatcher }
import Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import version.v0.vector.breed.x.Hive

import pojo.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }
import pojo.parser.date.span

import dimensions.fifth.fourth.web.simple.QueensWebSimpleProgram
import dimensions.fourth.third.program.simple.flow.QueensSimpleProgramInterfaceByFlow


package x:

  import monix.eval.{ Coeval, Task }
  import monix.tail.Iterant.eval

  package sync:

    import conf.io.x.flow.sync.{ Parameters => Ps }

    import dimensions.fourth.fifth.zeroth.breed.x.sync.`I'o`

    import dimensions.Dimension.Monad.monixIterantCoeval

    import version.v0.vector.breed.flow.x.sync.`Hatch'em, Spawn'it, I'o`

    import Coeval.catsSync

    abstract trait QueensWebIterantProgramByFlow[
      O <: Queens[R, B, P, S, N, Q],
      B <: Board2[R, B],
      R <: Square2,
      S <: Solution2[R, B, P, S],
      P <: Point2,
      Q <: Queens2[R, B, P, S, N, Q],
      N <: Nest2
    ](using
      ct_o: ClassTag[O],
      ct_b: ClassTag[B],
      ct_r: ClassTag[R],
      ct_s: ClassTag[S],
      ct_p: ClassTag[P],
      ct_q: ClassTag[Q],
      ct_n: ClassTag[N]
    ) extends QueensWebSimpleProgram[Iteration1, O, B, R, S, P, Q, N]
        with QueensSimpleProgramInterfaceByFlow:

      override protected type T = Coeval[Long]

      override protected def apply(wildcard: Wildcard,
                                   multiplier: Multiplier[Nest]
                                 )(using
                                   iteration1: Iteration1
                                 )(using
                                   Long, Flavor.Value): T =
        val since = Date()

        val iteration: Iteration0 = iteration1

        val Iteration0(board, _, Success(tag: UUID), _, _) = iteration

        val tmpdir = Files.createTempDirectory("queens-plane-543-web-io-flow-0-x-sync-")

        given Ps = Ps(tmpdir)(using board)

        val hive: Iterable[Hive[Coeval, Ps]] = for {
          it <- wildcard(multiplier)
        } yield {
          val hatch = Hatcher.`0`(monixIterantCoeval)(it)
          val spawn = `I'o`(it.model)
          Hive.sync(hatch, spawn)(Some(const(this)))
        }

        val emito = `Hatch'em, Spawn'it, I'o`(hive)

        var idle = -1L

        val busy =
          for
            o <- emito `#` tag
            _ <- eval { idle = 0L }
            started <- eval(Date())
            it <- o
            ended <- eval(Date())
            time <- eval {
              o(it) {
                val Just(_, _, _, _, started, ended) = o `just get` it
                this(tag, started, Left(ended))
                ended.getTime - started.getTime
              } match
                case None => this(tag, started, Right(idle.span(from = ended))); 0L
                case Some(time) => idle = idle.span(from = ended); time
            }
          yield
            time

        busy.sumL.map(Date().getTime - since.getTime - _) <* Coeval(Files.delete(tmpdir))

  package async:

    import conf.io.x.flow.async.{ Parameters => Ps }

    import dimensions.fourth.fifth.zeroth.breed.x.async.`I'o`

    import dimensions.Dimension.Monad.monixIterantTask

    import version.v0.vector.breed.flow.x.async.`Hatch'em, Spawn'it, I'o`

    import Task.catsAsync

    abstract trait QueensWebIterantProgramByFlow[
      O <: Queens[R, B, P, S, N, Q],
      B <: Board2[R, B],
      R <: Square2,
      S <: Solution2[R, B, P, S],
      P <: Point2,
      Q <: Queens2[R, B, P, S, N, Q],
      N <: Nest2
    ](using
      ct_o: ClassTag[O],
      ct_b: ClassTag[B],
      ct_r: ClassTag[R],
      ct_s: ClassTag[S],
      ct_p: ClassTag[P],
      ct_q: ClassTag[Q],
      ct_n: ClassTag[N]
    ) extends QueensWebSimpleProgram[Iteration1, O, B, R, S, P, Q, N]
        with QueensSimpleProgramInterfaceByFlow:

      override protected type T = Task[Long]

      override protected def apply(wildcard: Wildcard,
                                   multiplier: Multiplier[Nest]
                                 )(using
                                   iteration1: Iteration1
                                 )(using
                                   Long, Flavor.Value): T =
        val since = Date()

        val iteration: Iteration0 = iteration1

        val Iteration0(board, _, Success(tag: UUID), _, _) = iteration

        val tmpdir = Files.createTempDirectory("queens-plane-543-web-io-flow-0-x-async-")

        given Ps = Ps(tmpdir)(using board)

        val hive: Iterable[Hive[Task, Ps]] = for {
          it <- wildcard(multiplier)
        } yield {
          val hatch = Hatcher.`0`(monixIterantTask)(it)
          val spawn = `I'o`(it.model)
          Hive.async(hatch, spawn)(Some(const(this)))
        }

        val emito = `Hatch'em, Spawn'it, I'o`(hive)

        var idle = -1L

        val busy =
          for
            o <- emito `#` tag
            _ <- eval { idle = 0L }
            started <- eval(Date())
            it <- o
            ended <- eval(Date())
            time <- eval {
              o(it) {
                val Just(_, _, _, _, started, ended) = o `just get` it
                this(tag, started, Left(ended))
                ended.getTime - started.getTime
              } match
                case None => this(tag, started, Right(idle.span(from = ended))); 0L
                case Some(time) => idle = idle.span(from = ended); time
            }
          yield
            time

        busy.sumL.map(Date().getTime - since.getTime - _) <* Task(Files.delete(tmpdir))
