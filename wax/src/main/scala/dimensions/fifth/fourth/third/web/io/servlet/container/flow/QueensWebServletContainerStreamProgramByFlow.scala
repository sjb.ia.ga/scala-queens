package queens
package dimensions.fifth.fourth.third
package zeroth
package web.io
package servlet.container
package flow

import scala.reflect.ClassTag

import dimensions.third.first.Iteration1

import pojo.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }


package s:

  import dimensions.fifth.fourth.web.simple.servlet.container.QueensWebServletContainerSimpleProgram
  import dimensions.fifth.fourth.third.zeroth.web.io.flow.s.QueensWebStreamProgramByFlow

  abstract trait QueensWebServletContainerStreamProgramByFlow[
    O <: Queens[R, B, P, S, N, Q],
    B <: Board2[R, B],
    R <: Square2,
    S <: Solution2[R, B, P, S],
    P <: Point2,
    Q <: Queens2[R, B, P, S, N, Q],
    N <: Nest2
  ](using
    ct_o: ClassTag[O],
    ct_b: ClassTag[B],
    ct_r: ClassTag[R],
    ct_s: ClassTag[S],
    ct_p: ClassTag[P],
    ct_q: ClassTag[Q],
    ct_n: ClassTag[N]
  ) extends QueensWebServletContainerSimpleProgram[Iteration1, O, B, R, S, P, Q, N]
      with QueensWebStreamProgramByFlow[O, B, R, S, P, Q, N]
