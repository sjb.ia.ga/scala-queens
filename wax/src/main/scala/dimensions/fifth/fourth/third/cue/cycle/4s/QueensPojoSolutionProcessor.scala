package queens
package dimensions.fifth.fourth.third
package cue.cycle
package `4s`

import java.util.Date
import java.util.UUID

import scala.collection.mutable.{ ListBuffer => MutableList }

import base.Board

import queens.given
import common.given

import dimensions.fourth.subscribe.QueensSubscriber.Subscriber
import dimensions.fourth.subscribe.QueensSubscription._
import dimensions.fifth.publish.QueensPublisher

import common.pipeline.Context.Round

import version.less.nest.Nest

import pojo.parser.date.jDate.spanFrom

import pojo.`4s`.{ Queens, Board2, Square2, Queens2, Solution2, Point2, Nest2 }
import Board2.given

import dimensions.fifth.fourth.publish.cycle.`4s`.QueensPojoProcessor


abstract trait QueensPojoSolutionProcessor
    extends QueensPojoProcessor:

  private var cues: QueensPublisher[(IO, (Solution, Round))] = _

  private var result: MutableList[Queens2] = _

  private var count: Long = _

  def apply(cues: (((Date, Either[(Date, Long), Long])) => Unit) => `Cue Cycle Processor`): this.type =

    result = MutableList[Queens2]()

    this.cues = cues(
      {
        case (started, Left((ended, _))) =>
          val queens2 = result.last
          val solution2 = queens2.solutions.last
          result(result.size-1) = queens2
            .copy(solutions = queens2.solutions.init :+
                              solution2.copy(board = solution2.board.copy(started = started
                                                                         ,ended = ended
                                                                         ,elapsed = ended.getTime - started.getTime
                                                                         ,idle = 0L)
                                            ,started = started
                                            ,ended = ended
                                            ,elapsed = ended.getTime - started.getTime
                                            ,idle = 0L)
            )

        case (started, Right(idle)) if !result.isEmpty =>
          val queens2 = result.last
          val ended = Date()
          result(result.size-1) = queens2
            .copy(started = started
                 ,ended = ended
                 ,elapsed = ended.getTime - started.getTime
                 ,idle = idle)
      }
    )

    this


  override protected def onSubscriber(subscriber: Subscriber[Queens]): Unit = synchronized {
    if cues eq null
    then
      subscriber.onComplete()
  }


  override protected def onUpdate(): Long = synchronized {
    if cues ne null
    then
      val m = super.onUpdate()

      if subscription eq null
      then
        cues.subscribe(this.asInstanceOf[org.reactivestreams.Subscriber[(IO, (Solution, Round))]])

      count = m

      if m > 0
      then
        count = m max 3
        subscription.request(count)

    0L
  }


  override def onNext(t: (IO, (Solution, Round))): Unit =
    t match
      case (((board @ Board(_, _, seed), _, _, _), _), (Nil, _)) =>
        count -= 1

        if count >= 0
        then

          val `Cue Cycle Processor`(started, ended, idle) = cues

          val response = Queens(Board2(board, seed.getOrElse(Long.MinValue), started, started, 0L, 0L)
                               ,result.size.toLong, result.toList.sorted
                               ,started, ended, ended.getTime - started.getTime, idle.spanFrom(ended))

          result = MutableList[Queens2]()

          emit(Some(response))

      case (((_, _, _, uuid), _), (it, Round(board @ Board(_, _, seed), tag: UUID, queens, n))) =>
        import Board.apply

        val tags: List[String] = queens.toString

        val board2 = Board2(board(it), seed.getOrElse(Long.MinValue))

        val solution2 = Solution2(n, it.map(Point2(_, _)).sorted, board2)

        if n == 1 then
          result += Queens2(tags.sorted
                           ,queens.validate.name
                           ,tag.toString
                           ,uuid
                           ,Nest2(queens.aspect.toString, queens.algorithm.toString, queens.model.toString)
                           ,Nil)

        result(result.size-1) = result.last.copy(solutions = result.last.solutions :+ solution2)

        subscription.request(count)

    super.onNext(t)


  override def onError(t: Throwable): Unit =
    synchronized {
      cues = null
      emit(t)
    }

    super.onError(t)


  override def onComplete(): Unit =
    synchronized {
      cues = null
      emit()
    }

    super.onComplete()
