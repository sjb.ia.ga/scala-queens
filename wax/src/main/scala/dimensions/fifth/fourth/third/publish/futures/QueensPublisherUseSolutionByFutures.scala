package queens
package dimensions.fifth.fourth.third
package publish
package futures

import java.util.UUID

import scala.concurrent.{ ExecutionContext, Future, SyncChannel }

import dimensions.fifth.publish.QueensPublisherUseSolution
import dimensions.fifth.fourth.third.publish.cycle.futures.QueensSolutionPublisherByFutures


abstract trait QueensPublisherUseSolutionByFutures
    extends QueensPublisherUseSolution[(SyncChannel[Unit], Future[Unit]), ExecutionContext]
    with QueensSolutionPublisherByFutures
