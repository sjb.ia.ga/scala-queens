package queens
package dimensions.fifth.fourth.third
package publish
package flow

import dimensions.fifth.publish.QueensPublisherUseSolution
import dimensions.fifth.fourth.third.publish.cycle.flow.QueensSolutionPublisherByFlow

import version.less.nest.Nest


abstract trait QueensPublisherUseSolutionByFlow
    extends QueensPublisherUseSolution[Nest, Unit]
    with QueensSolutionPublisherByFlow
