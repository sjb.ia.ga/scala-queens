package queens
package dimensions.fifth.fourth.third
package zeroth
package web.io
package flow

import java.nio.file.Files

import java.util.Date
import java.util.UUID

import scala.Function.const
import scala.util.Success

import base.Board

import common.monad.`0`.flow._
import common.monad.tag.uuid.given

import dimensions.third.first.Iteration1
import dimensions.third.first.zeroth.flow.Iteration0
import Iteration0.given

import dimensions.three.breed.{ Hatch, Hatcher }
import Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import version.v0.vector.breed.s.Hive

import pojo.parser.date.span

import dimensions.fifth.fourth.web.simple.`4s`.QueensWebSimpleProgram
import dimensions.fourth.third.program.simple.flow.QueensSimpleProgramInterfaceByFlow


package `4s`:

  import cats.effect.IO
  import fs2.Stream.eval

  import conf.io.s.flow.{ Parameters => Ps }

  import dimensions.fourth.fifth.zeroth.breed.s.`I'o`

  import dimensions.Dimension.Monad.fs2StreamIO

  import version.v0.vector.breed.flow.s.`Hatch'em, Spawn'it, I'o`

  abstract trait QueensWebStreamProgramByFlow
      extends QueensWebSimpleProgram[Iteration1]
      with QueensSimpleProgramInterfaceByFlow:

    override protected type T = IO[Long]

    override protected def apply(wildcard: Wildcard,
                                 multiplier: Multiplier[Nest]
                               )(using
                                 iteration1: Iteration1
                               )(using
                                 Long, Flavor.Value): T =
      val since = Date()

      val iteration: Iteration0 = iteration1

      val Iteration0(board, _, Success(tag: UUID), _, _) = iteration

      val tmpdir = Files.createTempDirectory("queens-plane-543-web-io-flow-0-4s-")

      given Ps = Ps(tmpdir)(using board)

      val hive: Iterable[Hive[Ps]] = for {
        it <- wildcard(multiplier)
      } yield {
        val hatch = Hatcher.`0`(fs2StreamIO)(it)
        val spawn = `I'o`(it.model)
        Hive(hatch, spawn)(Some(const(this)))
      }

      val emito = `Hatch'em, Spawn'it, I'o`(hive)

      var idle = -1L

      val busy =
        for
          o <- emito `#` tag
          _ <- eval(IO { idle = 0L })
          started <- eval(IO(Date()))
          it <- o
          ended <- eval(IO(Date()))
          time <- eval {
            IO {
              o(it) {
                val Just(_, _, _, _, started, ended) = o `just get` it
                this(tag, started, Left(ended))
              } match
                case None => this(tag, started, Right(idle.span(from = ended))); 0L
                case Some(time) => idle = idle.span(from = ended); time
            }
          }
        yield
          time

      busy.compile.fold(0L)(_ + _).map(Date().getTime - since.getTime - _) <* IO(Files.delete(tmpdir))
