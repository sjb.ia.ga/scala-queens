package queens
package dimensions.fifth.fourth.third
package publish
package actors

import java.util.UUID

import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.{ PriorityBlockingQueue => Queue }

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import Macros.Event

import dimensions.fifth.publish.QueensPublisherUseSolution
import dimensions.fifth.fourth.third.publish.cycle.actors.QueensSolutionPublisherByActors


abstract trait QueensPublisherUseSolutionByActors
    extends QueensPublisherUseSolution[Queue[(Long, Event)], Ctx]
    with QueensSolutionPublisherByActors
