package queens
package dimensions.fifth.fourth.third
package cue.cycle
package actors

import java.util.UUID

import common.pipeline.Context.Round

import dimensions.third.first.Iteration1
import dimensions.third.first.actors.bis.{ Iteration2, Queens }

import dimensions.three.breed.{ Hatch, Hatcher }
import Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import actors.{ CueTag => Tag }

import dimensions.fifth.fourth.cue.cycle.QueensCueCycleProgram
import dimensions.fifth.fourth.third.cue.actors.QueensCueUseSolutionByActors
import dimensions.fourth.third.program.cycle.actors.QueensCycleProgramInterfaceByActors


abstract trait QueensCueCycleProgramByActors
    extends QueensCueCycleProgram[Iteration1]
    with QueensCueUseSolutionByActors
    with QueensCycleProgramInterfaceByActors:

  protected var cue: Option[(Long, Long)] = _

  override protected def apply(wildcard: Wildcard,
                               multiplier: Multiplier[Nest]
                             )(using
                               iteration: Iteration1
                             )(using
                               Flavor.Value): Long =
    val Iteration2(_, tag: Tag, _, flg, _, (_, cbs)) = iteration
    var Iteration2(cycle, _, _, _, _, _) = iteration

    for
      nest <- wildcard(multiplier)
      hatch = Hatcher.bis[Queens[?]](nest)
      queens = hatch()
    do
      cue = None

      given Simulate = QueensCueCycleProgramByActors(this, tag, cycle.io._1._4)

      given Iteration1 = Iteration1(cycle +: iteration.params.tail*)

      queens { it ?=>
        val Some(Some((_, _, uuid, n, _, _))) = it[(?, ?, UUID, Long, ?, ?)](flg.slot)

        ||(it, Round(cycle, common.NoTag, queens, n), Some(uuid))
      }

      cbs()
      tag(cycle.io._1._4)
      cycle = cycle.turn

    -1L


  override def toString(): String = super.toString // plainjava


object QueensCueCycleProgramByActors:

  import java.util.concurrent.atomic.AtomicLong

  import scala.Option.unless

  import common.bis.Cycle

  import common.Macros.{ busywaitWhile, busywaitUntil }

  import dimensions.fifth.fourth.third.publish.actors.Macros.Event

  import dimensions.fifth.cycle.{ Preemption, Suspension, Simulate, Simulation }

  def apply(
    pub: QueensCueCycleProgramByActors,
    tag: Tag,
    uuid: UUID
  ): Simulate =

    def pre = Preemption {

      case ((_, (_, None)), ((number, Some(((_, spindle: AtomicLong), _))), Right(_))) =>
        spindle.busywaitUntil(1)(tag.poll)
        spindle.decrementAndGet
        Right(-1)

      case (_, (_, Right(_))) =>
        tag(uuid, prefetch = true)
        Left(1)

      case (_, ((number, _), Left(prev))) =>
        if prev == 0 then tag(uuid, prefetch = true)
        pub.cue match
          case Some((num, cue)) =>
            if cue < 0
            then
              Left(0)
            else
              assert(num == number)
              Left(cue)
          case _ =>
            Left(0)
    }

    Simulate (
      Suspension(pre) {
        case ((Some(Cycle(it, _)), io), _) if it != io =>
          tag.outOfSync()
          Some(Simulation.stop)
        case ((Some(it), _), _) if !it.isInstanceOf[Cycle] =>
          tag.outOfSync()
          Some(Simulation.stop)
        case ((Some(_), _), _) | ((_, (_, Some(_))), (_, Right(_))) =>
          unless {
            busywaitWhile(tag.buf.contains(uuid) -> tag.buf(uuid).isEmpty)(tag.timeout, tag.poll)()
          }(Simulation.stop)
        case _ =>
          None
      }
    )
