package queens
package dimensions.fifth.fourth.third
package cue.cycle

import java.util.{ Date, UUID }
import java.nio.file.{ Files, Path }

import org.reactivestreams.Publisher

import scala.concurrent.{ ExecutionContext, Future }

import akka.actor.typed.scaladsl.ActorContext

import base.Board

import common.bis.Cycle
import Cycle.apply

import common.Macros.tryc

import common.Flag

import common.pipeline.Context.Round

import dimensions.fourth.subscribe.QueensSubscriber.Subscriber
import dimensions.fourth.subscribe.QueensSubscription._
import dimensions.fifth.publish.QueensPublisher

import dimensions.fifth.fourth.third.publish.actors.Macros.Event

import dimensions.third.bis.Queens
import dimensions.third.first.Iteration1

import dimensions.three.breed.{ Hatch, Hatcher }
import Hatch.Flavor

import dimensions.Dimension.Model
import Model._
import Parallel._
import Messaging._

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import pojo.parser.date.span

import dimensions.fifth.fourth.cue.cycle.QueensCueCycleProgram
import dimensions.fifth.fourth.publish.cycle.QueensCueProcessor


abstract trait QueensCueCycleProcessor(
  idling: ((Date, Either[(Date, Long), Long])) => Unit
) extends QueensCueCycleProgram[Iteration1]
    with QueensCueProcessor:

  import QueensCueCycleProcessor._

  protected val cues: Publisher[Cue]

  private var first: Boolean = _

  private var tmp: Path = _

  private var bin: Path = _

  private var exp: List[Nest] = _

  private var tag: Any = _
  private var key: String = _

  private object upstream:
    var cbs: conf.actors.bis.Callbacks = _

  private object downstream:
    var ec: ExecutionContext = _
    var cbs: Option[Either[conf.actors.bis.Callbacks, conf.futures.bis.Callbacks]] = _
    var batch: Future[?] = _

  private var suspended: Date = _
  private var idle: Long = _
  private var started: Date = _
  private var ended: Date = _

  implicit private var flavor: Flavor.Value = _


  override def apply(wildcard: Wildcard,
                     multiplier: Multiplier[Nest]
                   )(using
                     iteration: Iteration1
                   )(using
                     Flavor.Value): Long =
    ended = Date()

    exp = wildcard(multiplier)

    if exp.nonEmpty
    then

      this.flavor = implicitly[Flavor.Value]

      tmp = Files.createTempDirectory("queens-cue-cycle-processor-")

      first = true

      exp.head match
        case Nest(_, _, model @ Parallel(Actors)) =>
          import dimensions.third.first.actors.bis.Iteration2

          implicit val Iteration2(Cycle(((board: Board, _, max: Long, _), _), _), tag, _, _, _, (_, cbs)) = iteration

          this.tag = tag
          upstream.cbs = cbs
          downstream.cbs = Some(Left(conf.actors.bis.Callbacks()))
          this(Cycle(wildcard, multiplier), model)

        case Nest(_, _, model @ Parallel(Futures)) =>
          import dimensions.third.first.futures.bis.Iteration2

          implicit val Iteration2(Cycle(((board: Board, _, max: Long, _), _), _), tag, _, _, _, (ec, cbs)) = iteration

          this.tag = tag
          downstream.ec = ec
          downstream.cbs = Some(Right(cbs))
          downstream.batch = Future{}
          this(Cycle(wildcard, multiplier), model)

        case Nest(_, _, model @ Flow) =>
          import dimensions.third.first.flow.bis.Iteration2

          implicit val Iteration2(Cycle(((board: Board, _, max: Long, _), _), _), tag, _, _, _) = iteration

          this.tag = tag
          downstream.cbs = None
          this(Cycle(wildcard, multiplier), model)

        case Nest(_, _, Messaging(AWSSQS)) | Nest(_, _, Messaging(Kafka)) | Nest(_, _, Messaging(RabbitMQ)) => ???

    idle = 0L.span(from = ended)
    started = ended
    suspended = Date()

    -exp.size


  override protected def onSubscriber(subscriber: Subscriber[(IO, (Solution, Round))]): Unit = synchronized {
    if bin eq null
    then
      subscriber.onComplete()
  }


  override protected def onUpdate(): Long = synchronized {
    if bin ne null
    then
      val m = super.onUpdate()

      if subscription eq null
      then
        cues.subscribe(this)

      if m > 0
      then
        subscription.request(m)

    0L
  }


  override def onNext(it: Cue): Unit =
    val (_, num, cue, _, _) = it

    if bin eq null then
      throw OutOfSyncException(overflow = true)

    if first
    then
      first = false
    else if num == 0
    then
      thaw(None)

    if cue > 0
    then
      thaw(Some(it))

      super.onNext(it)


  override def onError(t: Throwable): Unit =
    synchronized {
      emit(t)
    }

    super.onError(t)


  override def onComplete(): Unit =
    synchronized {
      if bin ne null
      then
        emit(OutOfSyncException())
      else
        emit()
    }

    super.onComplete()


  override protected def unsubscribe(): Unit =
    super.unsubscribe()

    if bin ne null then
      reset

    tryc(Files.delete(tmp))

    tmp = null


  private def thaw(cue: Option[Cue]): Unit = exp(bin) match
    case (cycle, Some(nest @ Nest(_, _, model))) =>

      val hatch = Hatcher.bis[Queens[?]](nest)

      val queens = hatch()

      val flg = Flag(key)

      given Iteration1 =
        model match
          case Parallel(Actors) =>
            Iteration1(cycle, tag, flg, { () => }, upstream.cbs.ctx.value -> downstream.cbs.get.left.get)

          case Parallel(Futures) =>
            Iteration1(cycle, tag, flg, { () => }, downstream.ec -> downstream.cbs.get.right.get)

          case Flow =>
            Iteration1(cycle, tag, flg, { () => })

          case Messaging(Kafka) => ???

          case Messaging(RabbitMQ) => ???

          case _ => ???

      given Simulate = cue.map(QueensCueCycleProcessor(this, _)).getOrElse(QueensCueCycleProcessor())

      idle = idle.span(from = suspended)

      suspended = Date()

      queens { it ?=>
        val resumed = Date()

        val Some(Some((_, _, uuid, n, _, _))) = it[(?, ?, UUID, Long, ?, ?)](flg.slot)

        val round = Round(cycle.io._1._1, tag, queens, n)

        ||(it, round, Some(uuid))

        emit(Some(cycle.io -> (it.the -> round)))

        idling(suspended, Left(resumed -> n))

        cycle.idle = cycle.idle.span(from = resumed)

        suspended = Date()
      }

      downstream.cbs match
        case Some(Left(cbs)) =>
          cbs.spindle.decrementAndGet()
          cbs()
        case Some(Right(cbs)) =>
          cbs(Some(downstream.batch))
          given ExecutionContext = downstream.ec
          downstream.batch = Future{}
        case _ =>

      assert(cue.isEmpty -> cycle.image.isEmpty)
      assert(cycle.image.isEmpty -> cue.isEmpty)

      if cycle.image.isEmpty
      then
        idle += cycle.idle

        idling(cycle.since, Right(cycle.idle))

        if cycle.turn eq null
        then
          reset
        else
          this(cycle.turn, model)

      if cue.isEmpty
      then
        ended = Date()

        idle = idle.span(from = suspended)

        emit(Some(cycle.io -> (Nil -> null)))

        idle = 0L.span(from = ended)
        started = ended
        suspended = Date()

      idle = idle.span(from = suspended)

      suspended = Date()

    case _ =>
      throw OutOfSyncException(underflow = true)


  private def apply(cycle: Cycle, model: Model): Unit =
    if bin ne null then tryc(Files.delete(bin))

    val prefix = model match
      case Parallel(Actors) => "actors"
      case Parallel(Futures) => "futures"
      case Messaging(AWSSQS) => "awssqs"
      case Messaging(Kafka) => "kafka"
      case Messaging(RabbitMQ) => "rabbitmq"
      case Flow => "flow"

    bin = cycle(tmp, s"by-$prefix-")
    key = UUID.randomUUID.toString

  def reset: Unit = synchronized {
    tryc(Files.delete(bin))
    bin = null
  }


object QueensCueCycleProcessor:

  final case class OutOfSyncException(overflow: Boolean = false, underflow: Boolean = false)
      extends base.QueensException(
    if overflow then "too many cues"
    else if underflow then "too few expansions"
    else "too few cues / too many expansions"
  )


  def unapply(it: QueensCueCycleProcessor): Option[(Date, Date, Long)] =
    if it.tmp eq null then None
    else Some((it.started, it.ended, it.idle))


  import dimensions.fifth.cycle.Preemption


  def apply(): Simulate = Left(Preemption.step(-1))

  def apply(pub: QueensCueCycleProcessor, it: Cue): Simulate =

    Left {
      Preemption {
        case (_, ((number, _), Left(_))) =>
          val (_, num, cue, _, _) = it
          assert(num == number)
          Left(cue)

        case (_, ((_, Some((_, ref: Event))), _)) =>
          ref ! Left(())
          Left(1)

        case (_, ((_, Some(future: Future[Unit])), _)) =>
          pub.downstream.batch = pub.downstream.batch zip future
          Left(1)

        case _ =>
          Left(1)
      }
    }
