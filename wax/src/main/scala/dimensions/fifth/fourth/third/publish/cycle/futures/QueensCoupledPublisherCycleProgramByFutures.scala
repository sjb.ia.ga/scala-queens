package queens
package dimensions.fifth.fourth.third
package publish.cycle
package futures

import java.util.UUID

import scala.concurrent.{ ExecutionContext, Future, SyncChannel }

import common.bis.Cycle

import common.pipeline.Context.Round

import common.Macros.tryc

import dimensions.third.first.Iteration1
import dimensions.third.first.futures.bis.{ Iteration2, Queens }

import dimensions.three.breed.{ Hatch, Hatcher }
import Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import futures.{ CoupledTag => Tag }

import dimensions.fifth.fourth.publish.cycle.{ QueensSolutionProcessor, QueensPublisherCycleProgram }
import dimensions.fifth.fourth.third.publish.futures.QueensPublisherUseSolutionByFutures
import dimensions.fourth.third.program.cycle.futures.QueensCycleProgramInterfaceByFutures


abstract trait QueensCoupledPublisherCycleProgramByFutures[
  T,
  CP <: QueensCoupledPublisher[T]
] extends QueensPublisherCycleProgram[Iteration1, (SyncChannel[Unit], Future[Unit]), ExecutionContext, T]
    with QueensPublisherUseSolutionByFutures
    with QueensCycleProgramInterfaceByFutures
    with QueensSolutionProcessor[(SyncChannel[Unit], Future[Unit]), ExecutionContext]:

  protected def preemption: CP

  /**
    * Coupled with preemption.onMark.
    * @see [[queens.dimensions.fifth.fourth.third.publish.cycle.QueensCoupledPublisher#onMark]]
    */
  final override def onNext(it: IO): Unit =
    tryc(preemption.onMark(it))
    super.onNext(it)

  private var io: IO = null

  final override def onComplete(): Unit =
    tryc {
      if !preemption.noMark then
        preemption.onMark(io)
    }
    io = null
    super.onComplete()

  override protected def apply(wildcard: Wildcard,
                               multiplier: Multiplier[Nest])
                              (using
                               iteration: Iteration1)
                              (using
                               Flavor.Value): Long =
    val Iteration2(Cycle(io0, _), tag: Tag, _, flg, _, (ec, cbs)) = iteration
    var Iteration2(cycle, _, _, _, _, _) = iteration

    given ExecutionContext = ec

    try

      for
        nest <- wildcard(multiplier)
        hatch = Hatcher.bis[Queens[?]](nest)
        queens = hatch()
      do
        subscribe(this.asInstanceOf[org.reactivestreams.Subscriber[IO]])

        try

          given Simulate = QueensCoupledPublisherCycleProgramByFutures(this, cbs, tag)

          given Iteration1 = Iteration1(cycle +: iteration.params.tail*)

          queens { it ?=>
            val Some((_, _, uuid, n, _, _)) = it[(?, ?, UUID, Long, ?, ?)](flg.slot).get

            ||(it, Round(cycle, common.NoTag, queens, n), Some(uuid))
          }

          tag(this, cbs, None, null, null)

        finally
          if io ne null
          then
            super.onComplete()
            io = null

          reset
          preemption.reset
          cycle = cycle.turn

    finally
      preemption.emit0(io0)
      preemption.emit()

    -1L


  override def toString(): String = "Coupled " + super.toString // plainjava


object QueensCoupledPublisherCycleProgramByFutures:

  import common.bis.Cycle

  import conf.futures.bis.Callbacks

  import dimensions.fifth.cycle.{ Preemption, Suspension, Simulation, Simulate }

  import dimensions.fifth.publish.QueensPublisher.idle

  def apply[
    T,
    CP <: QueensCoupledPublisher[T]
  ](pub: QueensCoupledPublisherCycleProgramByFutures[T, CP],
    cbs: Callbacks,
    tag: Tag
  ): Simulate =

    def pre = Preemption {

      case ((_, io @ (_, None)), ((number, _), Right(_))) =>
        tryc(pub.preemption.mark(io))
        pub.io = io
        tag.nch.write(-(number+1))
        Right(-1)

      case ((_, io), ((number, Some(future: Future[Unit])), Right(_))) =>
        tryc(pub.preemption.mark(io))

        given ExecutionContext = cbs.executionContext
        tag(pub, cbs, Some(number), future, io)

        Right(1)

      case _ =>
        tryc(pub.preemption.onNext(()))
        Left(1)

    }

    Simulate {
      Suspension(pre) {
        case ((Some(it: Cycle), io), _) if it.io != io =>
          Some(Simulation.stop)
        case ((Some(it), _), _) if !it.isInstanceOf[Cycle] =>
          Some(Simulation.stop)
        case ((Some(_), _), _) =>
          pub.preemption.idle(tag.timeout, tag.poll, legacy = tag.legacy)
        case _ =>
          None
      }
    }
