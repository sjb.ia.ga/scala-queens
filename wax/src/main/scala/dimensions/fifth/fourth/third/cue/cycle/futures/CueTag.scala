package queens
package dimensions.fifth.fourth.third
package cue.cycle
package futures

import java.util.Date
import java.util.UUID
import java.util.NoSuchElementException

import scala.collection.mutable.{ HashMap, ListBuffer, Queue }

import org.reactivestreams.{ Publisher, Subscriber, Subscription }


private[futures] class CueTag(
  val idling: ((Date, Either[(Date, Long), Long])) => Unit,
  stopper: Publisher[Unit],
  val timeout: Long,
  val poll: Long = 50L
):

  private object Stopper extends Subscriber[Unit]:
    override def onSubscribe(s: Subscription): Unit = { s.request(1) }
    override def onNext(t: Unit): Unit = {}
    override def onComplete(): Unit = {}
    override def onError(t: Throwable): Unit = {}

  def outOfSync(): Unit = stopper.subscribe(Stopper)

  val buf = HashMap[UUID, Queue[(Long, Long)]]()
  private val bin = ListBuffer[UUID]()

  var cue: (Long, Long) = _

  def apply(uuid: UUID, it: (Long, Long)) = synchronized {
    if !bin.contains(uuid) then
      if !buf.contains(uuid) then buf(uuid) = Queue[(Long, Long)]()
      buf(uuid).enqueue(it)
  }

  def apply(uuid: UUID, prefetch: Boolean = false) = synchronized {
    if prefetch
    then
      try
        cue = buf(uuid).dequeue()
      catch
        case _: NoSuchElementException =>
          cue = -1L -> -1L
    else
      buf -= uuid
      bin += uuid
  }
