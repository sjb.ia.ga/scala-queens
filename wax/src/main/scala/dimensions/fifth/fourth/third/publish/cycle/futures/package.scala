package queens
package dimensions.fifth.fourth.third
package publish.cycle

import java.util.UUID


package object futures:

  import dimensions.fifth.third.publish.futures.QueensBatchPublisherByFutures

  type QueensSolutionPublisherByFutures = QueensBatchPublisherByFutures[IO]
