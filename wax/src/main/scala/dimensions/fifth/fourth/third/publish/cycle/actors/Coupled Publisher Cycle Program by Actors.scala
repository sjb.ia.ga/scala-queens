package queens
package dimensions.fifth.fourth.third
package publish.cycle
package actors

import java.util.UUID

import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.{ PriorityBlockingQueue => Queue }

import akka.actor.typed.{ ActorRef, Behavior }
import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import base.Board

import dimensions.fifth.fourth.third.publish.actors.Macros.Event

import common.bis.Cycle
import common.Flag
import common.Macros.trys

import dimensions.third.first.Iteration1
import conf.actors.bis.Callbacks

import dimensions.three.breed.Hatch.Flavor

import dimensions.fifth.publish.QueensPublisher

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import dimensions.fifth.third.publish.actors.given

import actors.{ CoupledTag => Tag }

import dimensions.fifth.fourth.publish.cycle.`Base.Publisher Cycle Program`


abstract trait `Base.Coupled Publisher Cycle Program by Actors`[
  T,
  CP <: QueensCoupledPublisher[T]
](
  name: String,
  legacy: Boolean
) extends `Base.Publisher Cycle Program`[Iteration1, Queue[(Long, Event)], Ctx, T]
    with QueensCoupledPublisherCycleProgramByActors[T, CP]:

  protected def preemption: CP

  def apply[R](wildcard: Wildcard,
               multiplier: Multiplier[Nest])
              (cycle: Cycle, timeout: Long)
              (block: QueensPublisher[T] => R)
        (using Flavor.Value): (R, Behavior[(Option[Callbacks], ActorRef[Long])]) =
    block(preemption) -> Behaviors
      .receive { case (ctx, (opt, ref)) =>
        val cbs = opt.getOrElse(Callbacks())

        val key = UUID.randomUUID.toString

        val tag = Tag(legacy, timeout)

        given Iteration1 = new Iteration1(Cycle(cycle), tag, Flag(key), { () => }, ctx -> cbs)

        try
          super.apply(wildcard, multiplier)
        finally
          ref ! tag.total

        Behaviors.stopped
      }

  override def toString(): String = name + " " + super.toString
