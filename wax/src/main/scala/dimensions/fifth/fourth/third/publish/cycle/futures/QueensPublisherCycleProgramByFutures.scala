package queens
package dimensions.fifth.fourth.third
package publish.cycle
package futures

import java.util.UUID

import scala.concurrent.{ ExecutionContext, Future, SyncChannel }

import common.pipeline.Context.Round

import dimensions.third.first.Iteration1
import dimensions.third.first.futures.Iteration
import dimensions.third.first.futures.bis.{ Iteration2, Queens }
import Iteration.given

import dimensions.three.breed.{ Hatch, Hatcher }
import Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import dimensions.fifth.fourth.publish.cycle.{ QueensPreemptionPublisher, QueensPublisherCycleProgram }
import dimensions.fifth.fourth.third.publish.futures.QueensPublisherUseSolutionByFutures
import dimensions.fourth.third.program.cycle.futures.QueensCycleProgramInterfaceByFutures


abstract trait QueensPublisherCycleProgramByFutures[T]
    extends QueensPublisherCycleProgram[Iteration1, (SyncChannel[Unit], Future[Unit]), ExecutionContext, T]
    with QueensPublisherUseSolutionByFutures
    with QueensCycleProgramInterfaceByFutures:

  protected def preemption: QueensPreemptionPublisher[T]

  override protected def apply(wildcard: Wildcard,
                               multiplier: Multiplier[Nest]
                             )(using
                               iteration: Iteration1
                             )(using
                               Flavor.Value): Long =
    val Iteration2( _, mkTag: (() => Tag[T]), _, flg, _, (_, cbs)) = iteration
    var Iteration2(cycle, _, _, _, _, _) = iteration

    given ExecutionContext = cbs.executionContext

    for
      nest <- wildcard(multiplier)
      hatch = Hatcher.bis[Queens[?]](nest)
      queens = hatch()
    do
      val tag = mkTag()

      given Simulate = QueensPublisherCycleProgramByFutures(this, cbs, tag)

      given Iteration = Iteration1(cycle +: iteration.params.tail*)

      queens { it ?=>
        val Some(Some((_, _, uuid, n, _, _))) = it[(?, ?, UUID, Long, ?, ?)](flg.slot)

        ||(it, Round(cycle, common.NoTag, queens, n), Some(uuid))
      }

      tag(this, cbs, None, null, null) { preemption.emit() }

      reset
      cycle = cycle.turn

    -1L


  override def toString(): String = super.toString // plainjava


object QueensPublisherCycleProgramByFutures:

  import java.util.UUID

  import scala.concurrent.{ Future, ExecutionContext }

  import common.Macros.tryc

  import conf.futures.bis.Callbacks

  import dimensions.fifth.cycle.{ Preemption, Suspension, Simulate }

  import dimensions.fifth.publish.QueensPublisher.idle

  def apply[T](pub: QueensPublisherCycleProgramByFutures[T],
               cbs: Callbacks,
               tag: Tag[T]
  ): Simulate =

    def pre = Preemption {

      case ((_, (_, None)), ((number, _), Right(_))) =>
        tag.nch.write(-(number+1))
        Right(-1)

      case ((_, io), ((number, Some(future: Future[Unit])), Right(prev))) =>
        val next = pub.next(number, prev)

        given ExecutionContext = cbs.executionContext
        tryc(pub.preemption.mark(io))
        tag(pub, cbs, Some(number), future, io)()

        Right(next)

      case (_, ((number, _), Left(prev))) =>
        val next = pub.preemption.next(number, prev)

        tryc(pub.preemption.emit(Some(tag.t), countdown = false))

        Left(next)

    }

    Simulate {
      Suspension(pre) {
        case ((_, (_, Some(_))), (_, Right(_))) =>
          pub.idle(tag.timeout, tag.poll)
        case _ =>
          None
      }
    }
