package queens
package dimensions.fifth.fourth.third
package publish
package actors

import scala.quoted.{ Expr, Type, Quotes }


object Macros:

  import java.util.concurrent.atomic.{ AtomicBoolean, AtomicLong }
  import java.util.concurrent.{ PriorityBlockingQueue => Queue }

  import akka.actor.typed.ActorRef
  import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

  import common.Wrapper


  type Message = Either[Unit, Option[ActorRef[?]]]
  type Event = ActorRef[Message]

  type Ctx = (Wrapper[ActorContext[?]], ActorContext[?], AtomicLong)


  def spawnCode(i: Expr[Long],
                block: Expr[ActorContext[?] ?=> Unit],
                continuation: Expr[Unit],
                context: Expr[Ctx],
                queue: Expr[Queue[(Long, Event)]])
         (using Quotes): Expr[Event] =
    '{
      val (w, ctx, spindle) = $context
      spindle.incrementAndGet
      val guard = AtomicBoolean(false)

      val r = ctx
        .spawnAnonymous[Message](Behaviors
          .receive { (ctx, msg) =>
            if guard.compareAndSet(false, true) then
              $block(using ctx)
            msg match
              case Right(Some(ref: Event)) => // tests
                ref ! Left(())
                spindle.decrementAndGet
                Behaviors.stopped
              case Right(_) =>
                Behaviors.same
              case Left(_) =>
                w.value = ctx
                $continuation
                // val (j, _) = $queue.remove()
                // assert($i == j)
                $queue.remove()
                spindle.decrementAndGet
                Behaviors.stopped
          }
        )
      $queue.add($i -> r)
      r
    }

  inline def spawn(inline i: Long
                 )(inline block: ActorContext[?] ?=> Unit = {}
                 )(inline continuation: => Unit = {}
           )(using inline context: Ctx
           )(using inline queue: Queue[(Long, Event)]
  ): Event =
    ${ spawnCode('i, 'block, 'continuation, 'context, 'queue)  }

  inline def zip(inline i: Long,
                 inline ref: Event)
                (inline continuation: => Unit)
          (using inline context: Ctx
         )(using inline queue: Queue[(Long, Event)]
  ): Unit =
    ${ spawnCode('i, '{ ctx ?=> ref ! Right(Some(ctx.self)) }, 'continuation, 'context, 'queue)  }

////////////////////////////////////////////////////////////////////////////////

  import common.Macros.tryc

  import dimensions.fifth.third.publish.actors.QueensBatchPublisherByActors

  import dimensions.fifth.fourth.third.publish.cycle.actors.{ fifo, event }

  def enqueueCode(pub: Expr[QueensBatchPublisherByActors[IO]],
                  number: Expr[Long],
                  ref: Expr[Event],
                  io: Expr[IO],
                  context: Expr[Ctx])
           (using Quotes): Expr[Unit] =
   '{
      val (w, ctx, spindle) = $context
      spindle.incrementAndGet
      ctx
        .spawnAnonymous[Unit](Behaviors
          .receive { (ctx, _) =>
            tryc($pub($number, event($ref), $io)(using (w, ctx, spindle)))
            spindle.decrementAndGet
            Behaviors.stopped
          }
        ) ! ()
    }

  inline def enqueue(inline pub: QueensBatchPublisherByActors[IO],
                     inline number: Long,
                     inline ref: Event,
                     inline io: IO)
              (using inline context: Ctx): Unit =
   ${ enqueueCode('pub, 'number, 'ref, 'io, 'context) }
