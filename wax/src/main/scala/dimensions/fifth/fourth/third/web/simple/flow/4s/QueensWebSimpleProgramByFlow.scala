package queens
package dimensions.fifth.fourth.third
package web.simple
package flow

import java.util.Date
import java.util.UUID

import scala.Function.const
import scala.util.Success

import base.Board

import common.monad.flow._
import common.monad.tag.uuid.given

import dimensions.third.first.Iteration1
import dimensions.third.first.flow.{ Iteration, `Queens*` }
import Iteration.given

import dimensions.three.breed.{ Hatch, Hatcher }
import Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import version.v1.vector.breed.Hive

import pojo.parser.date.span

import dimensions.fifth.fourth.web.simple.`4s`.QueensWebSimpleProgram
import dimensions.fourth.third.program.simple.flow.QueensSimpleProgramInterfaceByFlow


package `4s`:

  import conf.mbo.flow.{ Parameters => Ps }

  import dimensions.fourth.fifth.breed.`Buffered'o`

  import version.v1.vector.breed.`Hatch'em, Spawn'it, Buffered'o`

  abstract trait QueensWebSimpleProgramByFlow
      extends QueensWebSimpleProgram[Iteration1]
      with QueensSimpleProgramInterfaceByFlow:

    override protected type T = java.lang.Long

    override protected def apply(wildcard: Wildcard,
                                 multiplier: Multiplier[Nest]
                               )(using
                                 iteration1: Iteration1
                               )(using
                                 Long, Flavor.Value): T =
      val since = Date()

      val iteration: Iteration = iteration1

      val Iteration(board, _, Success(tag: UUID), pre, run, _, _) = iteration

      given Boolean = pre
      given Ps = Ps()(using board)

      val hive: Iterable[Hive[Ps]] = for {
        it <- wildcard(multiplier)
      } yield {
        val hatch = Hatcher[`Queens*`[?]](it)
        val spawn = `Buffered'o`(it.model)
        Hive(hatch, spawn)(Some(const(this)))
      }

      val emito = `Hatch'em, Spawn'it, Buffered'o`(hive)

      val busy =
        for
          o <- emito `#` tag
        yield {
          val started = Date()
          var idle = 0L
          val busy =
            for
              it <- o
            yield
              val ended = Date()
              o(it) {
                val Just(_, _, _, _, started, ended) = o `just get` it
                this(tag, started, Left(ended))
              } match
                case Some(time) => idle = idle.span(from = ended); time
                case _ => ???
          this(tag, started, Right(idle))
          busy.sum
        }

      Date().getTime - since.getTime - busy.sum
