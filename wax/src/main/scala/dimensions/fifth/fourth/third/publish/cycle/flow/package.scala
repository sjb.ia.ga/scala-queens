package queens
package dimensions.fifth.fourth.third
package publish.cycle

import java.util.UUID


package object flow:

  import dimensions.fifth.third.publish.flow.QueensBatchPublisherByFlow

  type QueensSolutionPublisherByFlow = QueensBatchPublisherByFlow[IO]
