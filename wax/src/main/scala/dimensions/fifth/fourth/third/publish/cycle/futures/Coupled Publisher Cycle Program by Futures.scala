package queens
package dimensions.fifth.fourth.third
package publish.cycle
package futures

import java.util.UUID

import scala.concurrent.{ ExecutionContext, Future, SyncChannel }

import base.Board

import common.bis.Cycle
import common.Flag
import common.Macros.trys

import dimensions.third.first.Iteration1
import conf.futures.bis.Callbacks

import dimensions.three.breed.Hatch.Flavor

import dimensions.fifth.publish.QueensPublisher

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import dimensions.fifth.third.publish.futures.given

import futures.{ CoupledTag => Tag }

import dimensions.fifth.fourth.publish.cycle.`Base.Publisher Cycle Program`


abstract trait `Base.Coupled Publisher Cycle Program by Futures`[
  T,
  CP <: QueensCoupledPublisher[T]
](
  name: String,
  legacy: Boolean
) extends `Base.Publisher Cycle Program`[Iteration1, (SyncChannel[Unit], Future[Unit]), ExecutionContext, T]
    with QueensCoupledPublisherCycleProgramByFutures[T, CP]:

  protected def preemption: CP

  def apply[R](wildcard: Wildcard,
               multiplier: Multiplier[Nest])
              (cycle: Cycle, timeout: Long)
              (block: QueensPublisher[T] => R)
        (using Flavor.Value)
     (implicit ec: ExecutionContext): (R, Future[Long]) =
    block(preemption) -> Future {
      val cbs = Callbacks()

      val key = UUID.randomUUID.toString

      val tag = Tag(legacy, timeout)

      given Iteration1 = new Iteration1(Cycle(cycle), tag, Flag(key), { () => }, ec -> cbs)

      super.apply(wildcard, multiplier)

      tag.total
    }


  override def toString(): String = name + " " + super.toString
