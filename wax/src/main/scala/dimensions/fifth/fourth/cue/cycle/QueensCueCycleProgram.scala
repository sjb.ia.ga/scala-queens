package queens
package dimensions.fifth.fourth
package cue.cycle

import dimensions.fifth.cue.QueensCueUseSolution
import dimensions.fourth.program.cycle.QueensCycleProgramInterface


abstract trait QueensCueCycleProgram[
  It <: AnyRef
] extends QueensCueUseSolution
    with QueensCycleProgramInterface[It]:

  override def toString(): String = "Cue " + super.toString
