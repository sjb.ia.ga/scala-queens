package queens
package dimensions.fifth.fourth
package cue.cycle

import dimensions.fifth.fourth.cue.cycle.QueensCueCycleProgram


abstract trait `Base.Cue Cycle Program`[It <: AnyRef]
    extends QueensCueCycleProgram[It]
