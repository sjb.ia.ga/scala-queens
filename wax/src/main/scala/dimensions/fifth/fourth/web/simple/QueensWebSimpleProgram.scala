package queens
package dimensions.fifth.fourth
package web.simple

import java.util.Collections.sort
import java.util.Date
import java.util.UUID
import java.util.LinkedList

import scala.Function.const
import scala.reflect.ClassTag

import base.Board
import Board.apply

import common.pipeline.Context.Round
import common.pipeline.Metadata
import Metadata.{ Boot, Wrap }

import conf.AnyConf

import queens.given
import common.given

import version.less.nest.Nest

import pojo.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }
import pojo.parser.date.jDate.spanFrom
import pojo.jPojo.given
import pojo.last

import dimensions.fourth.program.simple.QueensSimpleProgramInterface
import dimensions.fifth.web.QueensWebUseSolution


abstract trait QueensWebSimpleProgram[It <: AnyRef,
  O <: Queens[R, B, P, S, N, Q],
  B <: Board2[R, B],
  R <: Square2,
  S <: Solution2[R, B, P, S],
  P <: Point2,
  Q <: Queens2[R, B, P, S, N, Q],
  N <: Nest2
](using
  ct_o: ClassTag[O],
  ct_b: ClassTag[B],
  ct_r: ClassTag[R],
  ct_s: ClassTag[S],
  ct_p: ClassTag[P],
  ct_q: ClassTag[Q],
  ct_n: ClassTag[N]
) extends QueensWebUseSolution[O, Q]
    with QueensSimpleProgramInterface[It]:

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(_, ctxt: Round, data) => Some(Wrap(ctxt, data))

  override protected def >> = QueensWebSimpleProgram[O, B, R, S, P, Q, N](this(_), this(_))

  protected def apply(tag: UUID,
                      started: Date,
                      in: Either[Date, Long]): Long =
    val ended = Date()

    this(tag) match
      case None => ???

      case Some(response) if !response.getQueens.isEmpty =>
        import pojo.parser.date.jDate.apply

        val queens2 = response.getQueens.last
        in match
          case Left(ended) =>
            val solution2 = queens2.getSolutions.last
            solution2.getBoard()(started, ended)
            solution2(started, ended)
          case Right(idle) =>
            queens2(started, ended, Some(idle))

      case _ =>
        in.map(_.spanFrom(ended)).getOrElse(0L)

  override def toString(): String = super.toString // plainjava


object QueensWebSimpleProgram:

  def apply[
    O <: Queens[R, B, P, S, N, Q],
    B <: Board2[R, B],
    R <: Square2,
    S <: Solution2[R, B, P, S],
    P <: Point2,
    Q <: Queens2[R, B, P, S, N, Q],
    N <: Nest2
  ](using
    ct_o: ClassTag[O],
    ct_b: ClassTag[B],
    ct_r: ClassTag[R],
    ct_s: ClassTag[S],
    ct_p: ClassTag[P],
    ct_q: ClassTag[Q],
    ct_n: ClassTag[N]
  )(get: UUID => Option[O],
    add: (() => Q) => (Option[UUID], UUID) ?=> Q)
   (metadata: Metadata): Solution => Boolean = metadata match
    case Wrap(Round(board, tag: UUID, queens, n), Some(uuid: UUID)) =>

      { solution => get(tag) match

        case Some(response) =>
          val tags: List[String] = queens.toString
            .split("[\\[{(<.: ;,>)}\\]]")
            .filterNot(_.isEmpty)
            //.map(_.replace('_', ' '))
            .toList

          val board2: B = board(solution)
          sort(board2.getSquares)

          val solution2: S = solution
          solution2.setNumber(n)
          solution2.setBoard(board2)
          sort(solution2.getQueens)

          given Option[UUID] = Some(tag)
          given UUID = uuid

          add { () =>
            val nest2: N = Nest(queens.aspect, queens.algorithm, queens.model)
            val queens2: Q = ct_q
            queens2.setTag(tag.toString)
            queens2.setUUID(uuid)
            queens2.setNest(nest2)
            queens2.setTags(tags.sorted)
            queens2.setValidator(queens.validate.name)
            queens2.setSolutions(LinkedList[S]())
            response.getQueens.add(queens2)
            queens2
          }.getSolutions.add(solution2)

          true

        case _ => false

      }

    case _ => const(false)
