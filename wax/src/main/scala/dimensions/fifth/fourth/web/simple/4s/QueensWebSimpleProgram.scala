package queens
package dimensions.fifth.fourth
package web.simple
package `4s`

import java.util.Collections.sort
import java.util.Date
import java.util.UUID
import java.util.LinkedList

import scala.Function.const

import base.Board
import Board.apply

import common.pipeline.Context.Round
import common.pipeline.Metadata
import Metadata.{ Boot, Wrap }

import conf.AnyConf

import queens.given
import common.given

import version.less.nest.Nest

import pojo.`4s`.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }
import pojo.parser.date.jDate.spanFrom

import dimensions.fourth.program.simple.QueensSimpleProgramInterface
import dimensions.fifth.web.QueensWebUseSolution


abstract trait QueensWebSimpleProgram[It <: AnyRef]
    extends QueensWebUseSolution[Queens, Queens2]
    with QueensSimpleProgramInterface[It]:

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(_, ctxt: Round, data) => Some(Wrap(ctxt, data))

  override protected def >> = QueensWebSimpleProgram(this(_), this(_))

  protected def apply(tag: UUID,
                      started: Date,
                      in: Either[Date, Long]): Long =
    val ended = Date()

    this(tag) match
      case None => ???

      case Some(response) if !response.queens.isEmpty =>
        var queens2 = response.queens.last
        in match
          case Left(ended) =>
            val elapsed = ended.getTime - started.getTime
            var solution2 = queens2.solutions.last
            solution2.board = solution2.board.copy(started = started,
                                                   ended = ended,
                                                   elapsed = elapsed,
                                                   idle = 0L)
            solution2 = solution2.copy(started = started,
                                       ended = ended,
                                       elapsed = elapsed,
                                       idle = 0L)
            queens2.solutions = queens2.solutions.init :+ solution2
            elapsed

          case Right(idle) =>
            queens2 = queens2.copy(started = started,
                                   ended = ended,
                                   elapsed = ended.getTime - started.getTime,
                                   idle = idle)
            response.queens = response.queens.init :+ queens2
            idle

      case _ =>
        in.map(_.spanFrom(ended)).getOrElse(0L)

  override def toString(): String = super.toString // plainjava


object QueensWebSimpleProgram:

  import Board2.given
  import Solution2.given
  import Nest2.given

  def apply(get: UUID => Option[Queens],
            add: (() => Queens2) => (Option[UUID], UUID) ?=> Queens2)
           (metadata: Metadata): Solution => Boolean = metadata match
    case Wrap(Round(board, tag: UUID, queens, n), Some(uuid: UUID)) =>

      { solution => get(tag) match

        case Some(response) =>
          val tags: List[String] = queens.toString
            .split("[\\[{(<.: ;,>)}\\]]")
            .filterNot(_.isEmpty)
            //.map(_.replace('_', ' '))
            .toList

          val squares: List[Square2] = board(solution)
          val board2 = Board2(squares.sorted, board.seed.getOrElse(0L))

          val points: List[Point2] = solution
          val solution2 = Solution2(n, points.sorted, board2)

          given Option[UUID] = Some(tag)
          given UUID = uuid

          add { () =>
            val nest2: Nest2 = Nest(queens.aspect, queens.algorithm, queens.model)
            val queens2 = Queens2(tags.sorted, queens.validate.name, tag.toString, uuid, nest2)
            response.queens :+= queens2
            queens2
          }.solutions :+= solution2

          true

        case _ => false

      }

    case _ => const(false)
