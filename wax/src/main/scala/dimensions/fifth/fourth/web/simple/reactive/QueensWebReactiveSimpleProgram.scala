package queens
package dimensions.fifth.fourth
package web.simple
package reactive

import scala.reflect.ClassTag

import pojo.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }

import dimensions.fifth.web.reactive.QueensWebReactiveUseSolution


abstract trait QueensWebReactiveSimpleProgram[It <: AnyRef,
  O <: Queens[R, B, P, S, N, Q],
  B <: Board2[R, B],
  R <: Square2,
  S <: Solution2[R, B, P, S],
  P <: Point2,
  Q <: Queens2[R, B, P, S, N, Q],
  N <: Nest2
](using
  ct_o: ClassTag[O],
  ct_b: ClassTag[B],
  ct_r: ClassTag[R],
  ct_s: ClassTag[S],
  ct_p: ClassTag[P],
  ct_q: ClassTag[Q],
  ct_n: ClassTag[N]
) extends QueensWebReactiveUseSolution[O, Q]
    with QueensWebSimpleProgram[It, O, B, R, S, P, Q, N]:

  override def toString(): String = super.toString // plainjava
