package queens
package dimensions.fifth.fourth
package publish


package object cycle:

  import dimensions.fifth.publish.{ QueensPublisher, QueensBatchPublisher, QueensPublisherWithOps }

  import dimensions.fourth.subscribe.{ QueensSubscriber, QueensSubscription, QueensSubscriberWithOps }

  type QueensSolutionSubscriber = QueensSubscriberWithOps[IO]

  type QueensPreemptionSubscriber[T] = QueensSubscriberWithOps.QueensPreemptionSubscriber[T, IO]

  type QueensPreemptionPublisher[T] = QueensPublisherWithOps.QueensPreemptionPublisher[T, IO]

  type HasPreemptionPublisher[T] = QueensPublisherWithOps.HasPreemptionPublisher[T, IO]

////////////////////////////////////////////////////////////////////////////////

  import java.util.concurrent.Flow.{ Processor => JavaProcessor, Subscription => JavaSubscription }
  import org.reactivestreams.{ Processor => OrgProcessor, Subscription => OrgSubscription }

  import QueensPublisher._
  import QueensSubscription._
  import QueensSubscriber._

  abstract trait Processor[T, R](using Boolean)
      extends JavaProcessor[T, R], OrgProcessor[T, R],
              Subscriber[T], Publisher[R]:

     override val self = if implicitly[Boolean] then Left(this) else Right(this)

////////////////////////////////////////////////////////////////////////////////

  abstract trait QueensSolutionProcessor[R, C](using Boolean)
    extends Processor[IO, IO]
    with QueensBatchPublisher[IO, R, C]
    with QueensSolutionSubscriber:

    final override protected def onSubscription(subscription: Either[JavaSubscription, OrgSubscription]): Unit =
      super.onSubscription(subscription)
      subscription.request(Long.MaxValue)

    final override protected def onSubscriber(subscriber: Subscriber[IO]): Unit = subscriber.self match
      case Left(it) if this ne it => ???
      case Right(it) if this ne it => ???
      case _ =>

    final override def thaw(_number: Long): Long = ???

    final override protected def span(_n: Long, _p: Long): Option[Long] = ???

////////////////////////////////////////////////////////////////////////////////

  import common.pipeline.Context.Round

  abstract trait QueensCueProcessor(using Boolean)
      extends Processor[Cue, (IO, (Solution, Round))]
      with QueensPublisher[(IO, (Solution, Round))]
      with QueensSubscriber[Cue]

////////////////////////////////////////////////////////////////////////////////

  import pojo.{ Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2 }

  abstract trait QueensPojoProcessor[
    O <: Queens[R, B, P, S, N, Q],
    B <: Board2[R, B],
    R <: Square2,
    S <: Solution2[R, B, P, S],
    P <: Point2,
    Q <: Queens2[R, B, P, S, N, Q],
    N <: Nest2
  ](using
    Boolean
  ) extends Processor[(IO, (Solution, Round)), O]
      with QueensPublisher[O]
      with QueensSubscriber[(IO, (Solution, Round))]

////////////////////////////////////////////////////////////////////////////////

package cycle:

  package object `4s`:

    import common.pipeline.Context.Round

    import dimensions.fifth.publish.QueensPublisher

    import dimensions.fourth.subscribe.QueensSubscriber

    import pojo.`4s`.{ Queens => `Queens*` }

    abstract trait QueensPojoProcessor(using Boolean)
        extends Processor[(IO, (Solution, Round)), `Queens*`]
        with QueensPublisher[`Queens*`]
        with QueensSubscriber[(IO, (Solution, Round))]
