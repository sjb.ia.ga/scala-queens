package queens
package dimensions.fifth.fourth
package publish.cycle

import dimensions.fifth.publish.QueensPublisherUseSolution
import dimensions.fourth.program.cycle.QueensCycleProgramInterface
import dimensions.fifth.fourth.publish.cycle.{ HasPreemptionPublisher, QueensPreemptionPublisher }


abstract trait QueensPublisherCycleProgram[
  It <: AnyRef,
  R,
  C,
  T,
](using
  C ?=> ((R, R)) => R
) extends QueensPublisherUseSolution[R, C]
    with QueensCycleProgramInterface[It]
    with HasPreemptionPublisher[T]:

  override def toString(): String = "Publisher " + super.toString
