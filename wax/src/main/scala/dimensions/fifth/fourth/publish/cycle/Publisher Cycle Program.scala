package queens
package dimensions.fifth.fourth
package publish.cycle

import dimensions.fifth.fourth.publish.cycle.QueensPublisherCycleProgram


abstract trait `Base.Publisher Cycle Program`[It <: AnyRef, R, C, T]
    extends QueensPublisherCycleProgram[It, R, C, T]
