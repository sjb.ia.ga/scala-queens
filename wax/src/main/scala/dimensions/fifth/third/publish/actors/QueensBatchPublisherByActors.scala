package queens
package dimensions.fifth.third
package publish

import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.{ PriorityBlockingQueue => Queue }

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import akka.util.ReentrantGuard

import common.Macros.busywaitUntil

import dimensions.fifth.fourth.third.publish.actors.Macros.{ Event, spawn, zip }

import dimensions.fifth.fourth.third.publish.cycle.actors.{ dequeue, fifo }


package actors:

  import scala.collection.mutable.{ LongMap => Lengthy }

  import dimensions.fifth.publish.QueensBatchPublisher


  abstract trait QueensBatchPublisherByActors[T]
      extends QueensBatchPublisher[T, Queue[(Long, Event)], Ctx]:

    private implicit val queue: Queue[(Long, Event)] = fifo()
    private val lock = new ReentrantGuard()

    override protected val constant = Some(queue)

    batch = (constant, constant)

    override protected def emit(i: Long)(using Ctx): Queue[(Long, Event)] =
      val (event, t) = hold(i)
      hold -= i
      val ref = event.remove._2
      zip(i, ref)(emit(Some(t)))
      constant.get

    override protected def flush(from: Long, upto: Long, sync: Boolean)(using Ctx): Unit =
      if sync then return
      lock.withGuard { (from to upto).foreach { _ => queue.dequeue() } }

    override protected def result(using context: Ctx): Option[Queue[(Long, Event)]] =
      val batch =
        if this.batch._1.isEmpty && this.batch._2.isEmpty
        then
          None
        else
          val (_, _, spindle) = context
          spindle.busywaitUntil(1)(1L) // sim
          //assert(queue.isEmpty)
          spawn(-1)()(emit())
          constant
      this.batch = (None, None)
      batch


package object actors:

  given (Ctx ?=> ((Queue[(Long, Event)], Queue[(Long, Event)])) => Queue[(Long, Event)]) = _._1
