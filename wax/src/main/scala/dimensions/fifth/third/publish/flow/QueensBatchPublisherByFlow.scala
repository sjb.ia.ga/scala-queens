package queens
package dimensions.fifth.third
package publish

import dimensions.Dimension.Aspect.classic
import dimensions.Dimension.Algorithm.iterative
import dimensions.Dimension.Model.flow

import version.less.nest.Nest


package flow:

  import scala.collection.mutable.{ LongMap => Lengthy }

  import dimensions.fifth.publish.QueensBatchPublisher


  abstract trait QueensBatchPublisherByFlow[T]
      extends QueensBatchPublisher[T, Nest, Unit]:

    override protected val constant = Some(Nest(classic, iterative, flow))

    batch = (constant, constant)

    override protected def emit(i: Long)(using Unit): Nest =
      val (_, t) = hold(i)
      hold -= i
      emit(Some(t))
      constant.get

    override protected def result(using Unit): Option[Nest] =
      val batch =
        if this.batch._1.isEmpty && this.batch._2.isEmpty
        then
          None
        else
          constant
      this.batch = (None, None)
      batch.foreach { _ => emit() }
      batch


package object flow:

  given (Unit ?=> ((Nest, Nest)) => Nest) = _._1
