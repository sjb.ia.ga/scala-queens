package queens
package dimensions.fifth.third
package publish

import scala.concurrent.{ Await, ExecutionContext, Future, SyncChannel }
import scala.concurrent.duration.Duration


package futures:

  import scala.collection.mutable.{ LongMap => Lengthy }

  import dimensions.fifth.publish.QueensBatchPublisher


  abstract trait QueensBatchPublisherByFutures[
    T
  ](using
    zip: ExecutionContext ?=> (((SyncChannel[Unit], Future[Unit]), (SyncChannel[Unit], Future[Unit]))) => (SyncChannel[Unit], Future[Unit])
  ) extends QueensBatchPublisher[T, (SyncChannel[Unit], Future[Unit]), ExecutionContext]:

    private val ch0 = SyncChannel[Unit]()

    override protected def emit(i: Long)(using ExecutionContext): (SyncChannel[Unit], Future[Unit]) =
      val (event, t) = hold(i)
      hold -= i
      ch0 -> event._2.map { _ => emit(Some(t)) }

    override protected def emit(m: Long, n: Long)(using ExecutionContext): (SyncChannel[Unit], Future[Unit]) =
      val ch = SyncChannel[Unit]()
      ch -> (m to n).foldLeft(Future(ch.read)){ (batch, i) => batch.flatMap { _ => emit(i)._2 } }

    override protected def flush(_from: Long, _upto: Long, sync: Boolean)(using ExecutionContext): Unit =
      if !sync then return
      var (ch, it) = batch._1.get
      ch.write(())
      ch = SyncChannel[Unit]()
      batch = Some(ch -> it.map { _ => ch.read }) -> batch._2

    override protected def result(using ExecutionContext): Option[(SyncChannel[Unit], Future[Unit])] =
      val batch =
        if this.batch._1.nonEmpty && this.batch._2.nonEmpty
        then
          Some(zip(this.batch._1.get -> this.batch._2.get))
        else if this.batch._1.nonEmpty
        then
          this.batch._1
        else if this.batch._2.nonEmpty
        then
          this.batch._2
        else
          None
      this.batch = (None, None)
      batch.map { (ch, it) => ch -> it.map { _ => emit() } }


package object futures:

  given (ExecutionContext ?=> (((SyncChannel[Unit], Future[Unit]), (SyncChannel[Unit], Future[Unit]))) => (SyncChannel[Unit], Future[Unit])) = (_, _) match
   case ((chl, batchl), (chr, batchr)) =>
     val ch = SyncChannel[Unit]()
     ch -> Future(ch.read).flatMap { _ => chl.write(()); batchl.flatMap { _ => chr.write(()); batchr } }
