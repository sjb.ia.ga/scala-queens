package queens
package dimensions.fifth
package publish

import java.util.concurrent.{ ConcurrentHashMap => Map }

import java.util.concurrent.Flow.{ Subscriber => JavaSubscriber, Subscription => JavaSubscription }
import org.reactivestreams.{ Subscriber => OrgSubscriber, Subscription => OrgSubscription }

import common.Macros.tryc

import dimensions.fourth.subscribe.{ QueensSubscriber, QueensSubscription }
import QueensSubscriber._
import QueensSubscription._

import QueensPublisher._


abstract trait QueensPublisher[T]
    extends Publisher[T]:

  protected final val data = Map[QueensSubscription[T], (Subscriber[T], Option[Long])]()
  protected final val quota = Map[Subscriber[T], Long]()

  inline def isEmpty: Boolean = data.isEmpty()

  inline def nonEmpty: Boolean = !isEmpty

  override protected def subscribe(s: Either[JavaSubscriber[? >: T], OrgSubscriber[? >: T]]): Unit =

    val subscriber = new Subscriber[T] { override val self = s }

    val subscription = new QueensSubscription[T](
      { (self, n) =>
        data.put(self, subscriber -> n)
        quota.put(subscriber, n.getOrElse(0))
        onUpdate()
      },
      { self =>
        data.remove(self)
        quota.remove(subscriber)
        onUpdate()
      }
    )

    s match
      case Left(it) =>

        val s: Either[JavaSubscription, OrgSubscription] = Left(subscription.asInstanceOf[JavaSubscription])

        if it.isInstanceOf[QueensSubscriber[T]]
        then
          subscription.suspend()
        else
          s.request(0)

        it.onSubscribe(subscription)

      case Right(it) =>

        val s: Either[JavaSubscription, OrgSubscription] = Right(subscription.asInstanceOf[OrgSubscription])

        if it.isInstanceOf[QueensSubscriber[T]]
        then
          subscription.suspend()
        else
          s.request(0)

        it.onSubscribe(subscription)

    onSubscriber(subscriber)


  protected def onUpdate(): Long =
    var m = 0L
    val iterator = data.elements
    while iterator.hasMoreElements
    do
      iterator.nextElement match
        case (_, Some(n)) => m = n max m
        case _ =>
    m


  def emit(t: Throwable): Unit =
    val iterator = data.elements
    while iterator.hasMoreElements
    do
      val (subscriber, _) = iterator.nextElement
      subscriber.onError(t)


  // sequential
  def emit(t: Option[T] = None, countdown: Boolean = true): Unit =
    val iterator = data.elements
    if t.isEmpty
    then
      while iterator.hasMoreElements
      do
        val (subscriber, _) = iterator.nextElement
        subscriber.onComplete()
    else
      try
        val it = t.get
        if countdown
        then
          val b = iterator.hasMoreElements
          while iterator.hasMoreElements
          do
            iterator.nextElement match
              case (subscriber, Some(_)) if quota.get(subscriber) > 0 =>
                quota.put(subscriber, quota.get(subscriber) - 1)
                subscriber.onNext(it)
              case _ =>
        else
          while iterator.hasMoreElements
          do
            iterator.nextElement match
              case (subscriber, Some(_)) =>
                subscriber.onNext(it)
              case _ =>
      catch
        case t =>
          emit(t)
          throw t


object QueensPublisher:

  import java.util.concurrent.Flow.{ Publisher => JavaPublisher }
  import org.reactivestreams.{ Publisher => OrgPublisher }

  abstract trait Publisher[T]
      extends JavaPublisher[T], OrgPublisher[T]:
    protected def subscribe(flow: Either[JavaSubscriber[? >: T], OrgSubscriber[? >: T]]): Unit

    protected def onSubscriber(subscriber: Subscriber[T]): Unit = {}

    final override def subscribe(subscriber: JavaSubscriber[? >: T]): Unit =
      subscribe(Left(subscriber))

    final override def subscribe(subscriber: OrgSubscriber[? >: T]): Unit =
      subscribe(Right(subscriber))


  import dimensions.fourth.subscribe.QueensSubscriberWithOps.QueensPreemptionSubscriber

  abstract trait QueensPreemptionPublisher[T, M]
    extends QueensPublisherWithOps[T]:

    // synchronous
    def mark(it: M): Unit =
      val iterator = data.elements
      while iterator.hasMoreElements
      do
        iterator.nextElement match
          case (subscriber, Some(_)) =>
            subscriber.self match
              case Left(sub: QueensPreemptionSubscriber[T, M]) =>
                sub.onMark(it)
              case Right(sub: QueensPreemptionSubscriber[T, M]) =>
                sub.onMark(it)
          case _ =>

  abstract trait HasPreemptionPublisher[T, M]:

    protected def preemption: QueensPreemptionPublisher[T, M]


  import scala.Option.unless

  import common.Macros.busywaitWhile

  import dimensions.fifth.cycle.Simulation

  extension[T](self: QueensPublisher[T])
    def idle(timeout: Long, poll: Long, legacy: Boolean = true): Option[Simulation[?]] =
      def isEmpty: Boolean =
        val iterator = self.data.elements
        if legacy
        then
          while iterator.hasMoreElements
          do
            iterator.nextElement match
              case (_, Some(_)) =>
                return false
              case _ =>
          true
        else
          !iterator.hasMoreElements

      unless(busywaitWhile(isEmpty)(timeout, poll) { println(s"!!! NO SUBSCRIBERS ${self.getClass} !!!") })(Simulation.stop)
