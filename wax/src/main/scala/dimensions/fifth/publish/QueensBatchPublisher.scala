package queens
package dimensions.fifth
package publish

import java.util.concurrent.atomic.AtomicLong

import scala.collection.mutable.{ LongMap => Lengthy }

import common.Macros.busywaitUntil


abstract trait QueensBatchPublisher[
  T,
  R,
  C
](using zip: C ?=> ((R, R)) => R
) extends QueensPublisherWithOps[T]:

  protected val hold = Lengthy[(R, T)]()

  protected val span = QueensBatchPublisher.ends(Lengthy[Long](), Long.MaxValue, 0L)
  protected val naps = QueensBatchPublisher.ends(Lengthy[Long](), Long.MinValue, Long.MaxValue)

  private val counter = AtomicLong(0L)
  protected var batch: (Option[R], Option[R]) = (None, None)

  def reset: Unit =
    hold.clear()
    span.clear(); span.bound = Long.MaxValue; span.next = 0L
    naps.clear(); naps.bound = Long.MinValue; naps.next = Long.MaxValue
    counter.set(0L)
    batch = (constant, constant)

  private def join(dir: Lengthy[Long], rev: Lengthy[Long], number: Long, pm1L: Long): Boolean =
    var n = number+pm1L
    if dir.contains(n)
    then
      val m = dir(n)
      dir -= n
      dir(number) = m
      rev(m) = number
      n = number-pm1L
      if rev.contains(n)
      then
        dir(rev(n)) = m
        dir -= number
        rev(m) = rev(n)
        rev -= n
      true
    else
      false


  protected val constant: Option[R] = None

  protected def emit(i: Long)(using C): R

  protected def emit(m: Long, n: Long)(using C): R = ???

  protected def flush(from: Long, upto: Long, sync: Boolean)(using C): Unit = {}

  protected def result(using C): Option[R]


  def apply(number: Long,
            event: R,
            value: T)
     (using C): Option[R] =

    if number < 0
    then
      counter.busywaitUntil(-number-1)(1L)
      result

    else
      assert(event != null)

      def grow(min: Long,
               dir: Lengthy[Long],
               prj: ((Option[R], Option[R])) => Option[R],
               zip: ((R, R)) => R = zip,
               swp: ((Long, Long)) => (Long, Long) = identity
      ): Long =
        val (m, n) = swp(min -> dir(min))

        span -= m
        naps -= n

        if constant.nonEmpty
        then
          (m to n).foreach(emit)
        else
          val batch = emit(m, n)

          if prj(this.batch).isEmpty
          then
            this.batch = Some(batch) -> prj(this.batch.swap)
          else
            this.batch = Some(zip(prj(this.batch).get -> batch)) -> prj(this.batch.swap)

        counter.addAndGet(n - m + 1)

        swp(m -> n)._2

      var from = Long.MaxValue
      var upto = Long.MinValue

      synchronized {
        if join(span, naps, number, 1L)
        then
          hold(number) = event -> value
          span.bound = span.bound min number
        else if join(naps, span, number, -1L)
        then
          hold(number) = event -> value
          naps.bound = naps.bound max number
        else
          if span.isEmpty || !(span.clamp.lower(number) || naps.clamp.upper(number))
          then
            span(number) = number
            naps(number) = number
            hold(number) = event -> value
            span.bound = span.bound min number
            naps.bound = naps.bound max number

        val batch =
          if span.next == span.bound
          then
            span.next = grow(span.bound, span, _._1)
            from = span.bound
            upto = span.next
            span.next += 1
            true
          else if naps.next == naps.bound
          then
            val swp: ((R, R)) => (R, R) = _.swap
            val zip: ((R, R)) => R = this.zip
            naps.next = grow(naps.bound, naps, _._2, zip o swp,  _.swap)
            naps.next -= 1
            this.batch = this.batch.swap
            true
          else
            false

        if batch
        then
          if span.isEmpty
          then
            span.min(Long.MaxValue)
            naps.max(Long.MinValue)
          else
            span.min()
            naps.max()

        if from <= upto
        then
          flush(from, upto, sync = true)
      }

      if from <= upto
      then
        flush(from, upto, sync = false)

      None


object QueensBatchPublisher:

  /**
    * A "span" over multiple intervals, whose ends are only kept: either
    * left (lower bound) or right (upper bound).
    */
  class ends(val values: Lengthy[Long], var bound: Long, var next: Long):
    object clamp:
      def lower(number: Long): Boolean = bound <= number && number <= values(bound)
      def upper(number: Long): Boolean = bound >= number && number >= values(bound)
    def min(m: Long = values.keySet.min) = bound = m
    def max(m: Long = values.keySet.max) = bound = m

  given Conversion[ends, Lengthy[Long]] = _.values
