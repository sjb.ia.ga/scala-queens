package queens
package dimensions.fifth
package publish

import dimensions.fourth.subscribe.QueensSubscriberWithOps


abstract trait QueensPublisherWithOps[T]
    extends QueensPublisher[T]
    with QueensPreemptionOps[T]


object QueensPublisherWithOps:

  import dimensions.fourth.subscribe.QueensSubscriberWithOps.QueensPreemptionSubscriber

  abstract trait QueensPreemptionPublisher[T, M]
    extends QueensPublisherWithOps[T]:

    def mark(it: M) =
      val iterator = data.elements
      while iterator.hasMoreElements
      do
        iterator.nextElement match
          case (subscriber: QueensPreemptionSubscriber[T, M], Some(_)) =>
            subscriber.onMark(it)
          case _ =>

  abstract trait HasPreemptionPublisher[T, M]:

    protected def preemption: QueensPreemptionPublisher[T, M]
