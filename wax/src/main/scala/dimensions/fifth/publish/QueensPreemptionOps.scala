package queens
package dimensions.fifth
package publish

import scala.collection.mutable.{ ListBuffer => MutableList }

import org.reactivestreams.Subscriber

import dimensions.fourth.subscribe.QueensSubscriberWithOps


abstract trait QueensPreemptionOps[T] { this: QueensPublisherWithOps[T] =>

  def next(number: Long, prev: Long): Long =

    val rs = MutableList[QueensSubscriberWithOps[T]]()

    if true then
      val iterator = data.elements
      while iterator.hasMoreElements
      do
        iterator.nextElement match
          case (subscriber: QueensSubscriberWithOps[T], None) =>
            rs += subscriber
          case _ =>

    var m = 0L
    var f = true

    rs.foreach { subscriber =>
      val n = subscriber.thaw(number)
      if n > 0 then
        if f then m = n
        else m = n min m
        f = false
    }

    val ns = MutableList[QueensSubscriberWithOps[T]]()

    if true then
      val iterator = data.elements
      while iterator.hasMoreElements
      do
        iterator.nextElement match
          case (subscriber: QueensSubscriberWithOps[T], Some(_)) =>
            ns += subscriber
          case _ =>

    if ns.nonEmpty then
      ns.foreach(number -> prev :: _)

      var n = Long.MaxValue

      val iterator = data.elements
      while iterator.hasMoreElements
      do
        iterator.nextElement match
          case (subscriber: QueensSubscriberWithOps[T], Some(it)) if ns.contains(subscriber) =>
            n = n min it
          case _ =>

      m = if f then n else n min m

    ns.foreach(_ ++ m)

    m

}
