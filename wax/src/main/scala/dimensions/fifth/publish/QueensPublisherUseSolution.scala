package queens
package dimensions.fifth
package publish


abstract trait QueensPublisherUseSolution[
  R,
  C
](using
  C ?=> ((R, R)) => R
) extends QueensUseSolution
    with QueensBatchPublisher[IO, R, C]
