package queens
package dimensions.fifth
package web

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import scala.collection.JavaConverters.mapAsScalaConcurrentMapConverter


abstract trait QueensWebUseSolution[R <: AnyRef, Q]
    extends QueensUseSolution:

  protected def plainJAVA: Boolean

  private val data = (Map[UUID, R](), Map[(UUID, UUID), Q]())

  @inline final protected def apply(obj: R): UUID =
    val tag = UUID.randomUUID
    data._1.put(tag, obj)
    tag

  @inline final protected def apply(tag: UUID): Option[R] =
    if data._1.containsKey(tag)
    then
      Some(data._1.get(tag))
    else
      None

  protected def apply(add: () => Q)(using uuid: UUID, tag: Option[UUID]): Q = synchronized {
    if !data._2.containsKey(tag.get -> uuid)
    then
      data._2.put(tag.get -> uuid, add())
    data._2.get(tag.get -> uuid)
  }

  final protected def apply(tag: UUID, obj: R): Boolean =
    if data._1.containsKey(tag) && (data._1.get(tag) eq obj)
    then
      data._1.remove(tag)
      data._2.asScala
        .filter(_._1._1 eq tag)
        .keys
        .foreach(data._2.remove(_))
      true
    else
      false

  override def toString(): String = s"${if plainJAVA then "Java" else "Scala"} Web" + super.toString
