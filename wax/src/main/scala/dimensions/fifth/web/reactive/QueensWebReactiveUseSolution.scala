package queens
package dimensions.fifth
package web
package reactive


abstract trait QueensWebReactiveUseSolution[R <: AnyRef, Q]
    extends QueensWebUseSolution[R, Q]:

  override def toString(): String = super.toString + " :: Reactive"
