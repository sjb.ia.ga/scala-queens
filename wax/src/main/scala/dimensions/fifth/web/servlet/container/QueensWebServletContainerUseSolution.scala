package queens
package dimensions.fifth
package web
package servlet.container


abstract trait QueensWebServletContainerUseSolution[R <: AnyRef, Q]
    extends QueensWebUseSolution[R, Q]:

  protected def webXML: Boolean

  override def toString(): String = super.toString + s" :: Servlet Container ${if webXML then "web.xml" else "annotation"} based"
