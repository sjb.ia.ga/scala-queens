package queens
package dimensions.fifth
package web
package microservices


abstract trait QueensWebMicroservicesUseSolution[R <: AnyRef, Q]
    extends QueensWebUseSolution[R, Q]:

  override def toString(): String = super.toString + " :: Microservices"
