package queens
package dimensions.fourth
package app

import java.util.UUID
import java.util.concurrent.{ ConcurrentHashMap => Map }

import base.{ Board, EmptyBoard }

import common.Pipeline
import common.pipeline.Context.App

import common.Flag

import common.monad.{ Item, `MonadWithFilter"` }
import common.monad.Grow.ItemValue

import dimensions.fourth.fifth.breed.{ `Base.Breed *'"`, `Breed *'"` }

abstract class `Base.QueensAppInterface`[
  O, AD <: AppData[O, AD],
  CTX <: App[O, AD, CTX],
  P <: Pipeline[Unit], It <: AnyVal
] protected (
  override protected val ctxs: Iterable[CTX]
)(
  override protected val tag: Any,
  _its: Seq[UUID]
)(
  override protected val fs: Item[P] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.Breed *'"`[P](_its)
    with QueensAppInterface[O, AD, CTX, P, It]


abstract trait QueensAppInterface[
  O, AD <: AppData[O, AD],
  CTX <: App[O, AD, CTX],
  P <: Pipeline[Unit], It <: AnyVal
] extends QueensInterface
    with `MonadWithFilter"`[P, It, Boolean]
    with `Breed *'"`[P]:

  protected val ctxs: Iterable[CTX]

  override protected type T56 = ItemValue[?]

  def foreach(block: Item[P] => Unit)
             (using It, Boolean): Iterable[Unit]

  def map[R](block: Item[P] => R)
            (using It, Boolean): Iterable[R]

  def flatMap[R](block: Item[P] => Iterable[R])
                (using It, Boolean): Iterable[R]

  override def toString(): String = "App"
