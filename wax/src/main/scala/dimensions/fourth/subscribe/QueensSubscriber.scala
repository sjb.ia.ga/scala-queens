package queens
package dimensions.fourth
package subscribe

import java.util.concurrent.Flow.{ Subscription => JavaSubscription }
import org.reactivestreams.{ Subscription => OrgSubscription }

import QueensSubscription._
import QueensSubscriber._


abstract trait QueensSubscriber[T]
    extends Subscriber[T]:

  private var _subscription: Either[JavaSubscription, OrgSubscription] = _
  protected def subscription: Either[JavaSubscription, OrgSubscription] = _subscription

  private var _counter = 0L
  protected final def counter = _counter

  @inline override protected def onSubscription(s: Either[JavaSubscription, OrgSubscription]): Unit =
    super.onSubscription(s)
    _subscription = s

  @inline override def onError(t: Throwable): Unit =
    _counter = 0
    super.onError(t)
    unsubscribe()

  @inline override def onNext(t: T): Unit =
    super.onNext(t)
    _counter += 1

  @inline override def onComplete(): Unit =
    _counter = 0
    super.onComplete()
    unsubscribe()

  @inline protected def unsubscribe(): Unit =
    subscription.cancel()
    _subscription = null


object QueensSubscriber:

  import java.util.concurrent.Flow.{ Subscriber => JavaSubscriber }
  import org.reactivestreams.{ Subscriber => OrgSubscriber }

  abstract trait Subscriber[T]
      extends JavaSubscriber[T], OrgSubscriber[T]:
    val self: Either[JavaSubscriber[? >: T], OrgSubscriber[? >: T]]

    protected def onSubscription(s: Either[JavaSubscription, OrgSubscription]): Unit = self -> s match
      case (Left(it), Left(s)) if this ne it => it.onSubscribe(s)
      case (Right(it), Right(s)) if this ne it => it.onSubscribe(s)
      case _ =>

    final override def onSubscribe(s: JavaSubscription): Unit = onSubscription(Left(s))

    final override def onSubscribe(s: OrgSubscription): Unit = onSubscription(Right(s))

    override def onError(t: Throwable): Unit = self match
      case Left(it) if this ne it => it.onError(t)
      case Right(it) if this ne it => it.onError(t)
      case _ =>

    override def onNext(t: T): Unit = self match
      case Left(it) if this ne it => it.onNext(t)
      case Right(it) if this ne it => it.onNext(t)
      case _ =>

    override def onComplete(): Unit = self match
      case Left(it) if this ne it => it.onComplete()
      case Right(it) if this ne it => it.onComplete()
      case _ =>
