package queens
package dimensions.fourth
package subscribe

import java.util.concurrent.Flow.{ Subscription => JavaSubscription }
import org.reactivestreams.{ Subscription => OrgSubscription }

import QueensSubscription._


class QueensSubscription[T](
  update: (QueensSubscription[T], Option[Long]) => Unit,
  remove: QueensSubscription[T] => Unit
) extends Subscription:

  inline override def request(n: Long): Unit =
    update(this, Some(n))

  inline override def cancel(): Unit =
    remove(this)

  inline def suspend(): Unit =
    update(this, None)


object QueensSubscription:

  abstract trait Subscription
      extends JavaSubscription, OrgSubscription

  extension(self: Either[JavaSubscription, OrgSubscription])
    inline def request(n: Long): Unit = self match
       case Left(it) => it.request(n)
       case Right(it) => it.request(n)
    inline def cancel(): Unit = self match
       case Left(it) => it.cancel()
       case Right(it) => it.cancel()
