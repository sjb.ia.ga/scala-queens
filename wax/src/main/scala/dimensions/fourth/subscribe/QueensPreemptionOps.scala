package queens
package dimensions.fourth
package subscribe

import dimensions.fourth.subscribe.QueensSubscription._


abstract trait QueensPreemptionOps[T] { this: QueensSubscriberWithOps[T] =>

  @inline def thaw(number: Long): Long =
    resume(Long.MaxValue)

  protected def span(number: Long, prev: Long): Option[Long]

  def ::(arg: (Long, Long)): Unit =
    val (number, prev) = arg
    val next = span(number, prev).map(_.abs).getOrElse(0L)
    this ++ next

  def ++(next: Long): Unit =
    if next == 0
    then
      suspend()
    else
      subscription.request(next)

  protected def resume(n: Long): Long =
    if n == 0
    then
      suspend()
      n
    else
      val m = n.abs
      subscription.request(m)
      m

  inline protected def suspend(): Unit =
    subscription match
      case Left(it) => it.suspend()
      case Right(it) => it.suspend()

}

