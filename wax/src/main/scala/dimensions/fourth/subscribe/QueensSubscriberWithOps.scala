package queens
package dimensions.fourth
package subscribe


abstract trait QueensSubscriberWithOps[T]
    extends QueensSubscriber[T]
    with QueensPreemptionOps[T]:

  override def subscription: Either[QueensSubscription[T], QueensSubscription[T]] =
    super.subscription match
      case Left(it) => Left(it.asInstanceOf[QueensSubscription[T]])
      case Right(it) => Right(it.asInstanceOf[QueensSubscription[T]])


object QueensSubscriberWithOps:

  abstract trait QueensPreemptionSubscriber[T, M](using Boolean)
      extends QueensSubscriberWithOps[T]:

    override val self = if implicitly[Boolean] then Left(this) else Right(this)

    def onMark(t: M): Unit = {}
