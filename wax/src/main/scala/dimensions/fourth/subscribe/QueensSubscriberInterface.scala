package queens
package dimensions.fourth
package subscribe

import dimensions.fifth.fourth.publish.cycle.QueensSolutionSubscriber


abstract trait QueensSubscriberInterface
    extends QueensInterface
    with QueensSolutionSubscriber:

  override def toString(): String = "Subscriber"
