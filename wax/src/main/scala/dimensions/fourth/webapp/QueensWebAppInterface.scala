package queens
package dimensions.fourth
package webapp

import java.util.UUID
import java.util.concurrent.{ ConcurrentHashMap => Map }

import scala.collection.mutable.{ ListBuffer => MutableList }
import scala.util.Try

import common.pipeline.Metadata
import Metadata.Media

import base.EmptyBoard

import common.Flag
import common.Flag.given
import common.monad.Item

import common.Macros.tryc

import dimensions.third.`Queens*`

import app.{ `Base.QueensAppInterface`, QueensAppInterface, AppData, Parameter }
import QueensWebAppInterface.{ Iteration, Web, FromResponseToQueens }


abstract class `Base.QueensWebAppInterface`[
  Q, WM <: WebMethod[?, Q, WM]
] protected (
  from: FromResponseToQueens[Q],
  webs: Iterable[Web[Q, WM]]
)(
  _tag: Any,
  _its: Seq[UUID]
)(
  _fs: Item[`Web Client`] => Boolean*
)(using
  `_its*`: Map[UUID, (Boolean, Flag)]
) extends `Base.QueensAppInterface`[Q, WM, Web[Q, WM], `Web Client`, Iteration](webs)(_tag, _its)(_fs*)
    with QueensWebAppInterface[Q, WM](from):

  override protected type * <: `Base.QueensWebAppInterface`[Q, WM]


abstract trait QueensWebAppInterface[
  Q, WM <: WebMethod[?, Q, WM]
](
  from: FromResponseToQueens[Q]
) extends QueensAppInterface[Q, WM, Web[Q, WM], `Web Client`, Iteration]:

  override protected type * <: QueensWebAppInterface[Q, WM]

  import scala.Function.const

  import common.monad.Item.given
  import common.monad.Grow.`it's`

  override def apply[E](it: Item[`Web Client`])(expr: (UUID, java.lang.Long) ?=> E): Option[E] =
    `just get*`(it) {
      case false => const(None)
      case _ => {
        case _ if !it => None
        case (key, (_, _, uuid, n, responses: AppData.ItemValue[Q, Iterable[`Queens3*`], WM], results: WebMethod.ItemValue[Q, WM])) =>
          {
             for
               web <- ctxs
               if web.requests(it, key)
               (n, method, response, to) <- web.responses[Q, WM](from, tag, key)
               if response.isSuccess
               obj = response.get
               queens <- to(obj)
             yield
               (n, method, obj, queens)
           }.toSeq
            .`it's`(results)

          ctxs
            .flatMap(_.responses[Q, WM](from, tag, key))
            .`it's`(responses)

          val r = expr(using uuid, n)
          r match
            case false => this stop it
            case _ =>

          Some(r)
      }
    }


  private def `for*`[R](wrap: R => Unit,
                        block: Item[`Web Client`] => R)(using
                        iteration: Iteration,
                        preemption: Boolean): Unit =
    val id = UUID.randomUUID
    val flg = `flag*`(id)
    var ppl: Option[`Web Client`] = None

    val r =
      try
        `iterations*`.put(id, (true, flg))

        val `block*` = `apply*`(block)

        var loop = 1L
        while flg do
          val pl = iteration.fun(loop, ppl)

          if pl.isEmpty
          then
            !flg

          else
            ctxs.foreach(_.queue.put(flg.slot, MutableList[Media]()))

            val it = pl.get

            if preemption then
              block(new Item(it, flg.slot))

            val vi = (null, tag, UUID.randomUUID, loop, AppData.ItemValue[Q, Iterable[`Queens3*`], WM](), WebMethod.ItemValue[Q, WM]())

            `block*`(new Item(it, flg.slot, Some(vi))).foreach(wrap)

            loop += 1

          ppl = pl
      finally
        tryc(ctxs.foreach(_.queue.remove(flg.slot)))
        tryc(`iterations*`.remove(id, (true, flg)))
    r


  inline override def foreach(block: Item[`Web Client`] => Unit)
                             (using Iteration, Boolean): Iterable[Unit] =
    `for*`[Unit]({ (_: Unit) => }, block)
    Nil

  inline override def map[R](block: Item[`Web Client`] => R)
                            (using Iteration, Boolean): Iterable[R] =
    val r = MutableList[R]()
    `for*`(r.addOne _, block)
    r

  inline override def flatMap[R](block: Item[`Web Client`] => Iterable[R])
                                (using Iteration, Boolean): Iterable[R] =
    val r = MutableList[R]()
    `for*`(r.addAll _, block)
    r

  override def toString(): String = "Web " + super.toString


object QueensWebAppInterface:

  case class Iteration(fun: (Long, Option[`Web Client`]) => Option[`Web Client`])
      extends AnyVal


  import common.pipeline.Context.{ Empty, App }

  case class Web[
    Q,
    WM <: WebMethod[?, Q, WM]
  ] private[webapp] (
    url: Either[(String, Any, scala.collection.Map[String, String]), String],
    media: String,
    override val params: Parameter[?]*
  ) extends App[Q, WM, Web[Q, WM]]
      with (((String, Option[WM])) => Option[Metadata]):

    private[QueensWebAppInterface] val queue: Map[String, MutableList[Media]] = Map()

    override def apply(it: (String, Option[WM])): Option[Metadata] = it match
      case (key, method) =>
        method.map { _ =>
          val metadata = Media(Empty, method)
          queue.get(key) += metadata
          metadata
        }

    inline private[QueensWebAppInterface] def requests(
      cli: `Web Client`,
      key: String
    ): Boolean = cli.||((), this, Some(key))

    private[QueensWebAppInterface] def responses[
      Q,
      WM <: WebMethod[?, Q, WM]
    ](
      from: FromResponseToQueens[Q],
      tag: Any,
      key: String
    ): Iterable[(Int, WM, Try[Q], Q => Iterable[`Queens3*`])] =
      val its = this
        .queue
        .get(key)
        .map(_.data.get)
        .asInstanceOf[Iterable[WM]]
      its.zipWithIndex.map { (it, n) => (n, it, it.result, obj => from(obj, tag, key)) }

  type FromResponseToQueens[Q] = Function3[Q, Any, String, Iterable[`Queens3*`]]
