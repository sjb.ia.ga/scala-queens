package queens
package dimensions.fourth
package webapp

import app.AppData


/**
  * @see [[queens.axis.fourth.webapp.okhttp.`OkHttp Web Client`.Get]]
  * @see [[queens.axis.fourth.webapp.okhttp.plainjava.OkHttp$Web$Client.Get]]
  * @see [[queens.axis.fourth.webapp.retrofit.`Retrofit Web Client`.Get]]
  * @see [[queens.axis.fourth.webapp.retrofit.plainjava.Retrofit$Web$Client.Get]]
  * @see [[queens.axis.fourth.webapp.spring.`Spring Web Client`.Get]]
  * @see [[queens.axis.fourth.webapp.spring.plainjava.Spring$Web$Client.Get]]
  * @see [[queens.axis.fourth.webapp.sttp.`sttp Web Client`.Get]]
  * @see [[queens.axis.fourth.webapp.sttp.plainjava.sttp$Web$Client.Get]]
  * @see [[queens.axis.fourth.webapp.vertx.`Vertx Web Client`.Get]]
  * @see [[queens.axis.fourth.webapp.vertx.plainjava.Vertx$Web$Client.Get]]
  */
abstract trait WebMethod[+T, Q, WM <: WebMethod[T, Q, WM]]
    extends AppData[Q, WM]:
  protected val request: T
  inline def response: Option[Q] = result.toOption


object WebMethod:

  import common.monad.Grow

  import dimensions.third.`Queens*`

  class ItemValue[Q, WM <: WebMethod[?, Q, WM]](_it: (Int, WM, Q, `Queens3*`)*)
      extends Grow.ItemValue[Seq[(Int, WM, Q, `Queens3*`)]](_it)
