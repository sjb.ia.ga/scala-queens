package queens
package dimensions.fourth
package webapp

import scala.collection.{ IterableOps, IterableFactory, IterableOnce }
import scala.collection.Iterable
import scala.collection.mutable.{ Builder, ImmutableBuilder, Growable }


final class TagKeyEmptyIterable[A](
  val tag: Any,
  val key: String
) extends Iterable[A]
    with IterableOps[A, TagKeyEmptyIterable, TagKeyEmptyIterable[A]]
    with Growable[(Int, Iterable[A])]:

  override def knownSize: Int = 0

  override def iterator: Iterator[A] = Nil.iterator

  override def addOne(it: (Int, Iterable[A])): this.type = this

  override def clear(): Unit = {}

  override val iterableFactory: IterableFactory[TagKeyEmptyIterable] =
    new TagKeyEmptyIterableFactory(tag, key)

  override protected def fromSpecific(
    coll: IterableOnce[A]
  ): TagKeyEmptyIterable[A] =
    iterableFactory.from(coll)

  override protected def newSpecificBuilder: Builder[A, TagKeyEmptyIterable[A]] =
    iterableFactory.newBuilder

  override def empty: TagKeyEmptyIterable[A] =
    iterableFactory.empty

  override def className = "TagKeyEmptyIterable"


sealed class TagKeyEmptyIterableFactory(tag: Any, key: String)
    extends IterableFactory[TagKeyEmptyIterable]:

  override def from[A](source: IterableOnce[A]): TagKeyEmptyIterable[A] =
    (newBuilder[A] ++= source).result()

  override def empty[A]: TagKeyEmptyIterable[A] =
     new TagKeyEmptyIterable[A](tag, key)

  override def newBuilder[A]: Builder[A, TagKeyEmptyIterable[A]] =
    new ImmutableBuilder[A, TagKeyEmptyIterable[A]](empty):
      override def addOne(elem: A): this.type = this
