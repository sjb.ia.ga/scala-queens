package queens
package dimensions.fourth
package webapp

import scala.collection.Map

import dimensions.Dimension.{ Aspect, Algorithm, Model }

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import dimensions.fourth.app.Parameter


abstract trait `Web*`[Q, WM <: WebMethod[?, Q, WM]]:

  protected type Web = QueensWebAppInterface.Web[Q, WM]

  def apply(media: String)
           (wildcard: Wildcard,
            multiplier: Multiplier[Nest])
           (params: Parameter[?]*): Iterable[Web] =
    val scheme = sys.Prop
      .StringProp(_root_.queens.main.Properties.WebSimple.scheme)
      .value
    val host = sys.Prop
      .StringProp(_root_.queens.main.Properties.WebSimple.host)
      .value
    val port = sys.Prop
      .IntProp(_root_.queens.main.Properties.WebSimple.port)
      .value
    val infra = sys.Prop
      .StringProp(_root_.queens.main.Properties.WebSimple.infra)
      .value
    this(scheme, host, port, infra, media)(wildcard, multiplier)(params*)

  def apply(baseUrl: String,
            matrix: Map[Model, Map[Aspect, Map[Algorithm, Int]]],
            query: Map[String, String]
  ): Iterable[Either[Any, String]]

  protected def apply(scheme: String,
                      host: String,
                      port: Int,
                      infra: String
  ): String =
    if infra.isEmpty
    then
      s"$scheme://$host:$port/queens/solve/"
    else
      s"$scheme://$host:$port/queens/solve/$infra/"

  import `Web*`.toQuery

  protected def apply(scheme: String,
                      host: String,
                      port: Int,
                      infra: String,
                      media: String)
                     (wildcard: Wildcard,
                      multiplier: Multiplier[Nest])
                     (params: Parameter[?]*): Iterable[Web] =
    val mml = `Web*`(wildcard(multiplier))
    val baseUrl = this(scheme, host, port, infra)
    val query: Map[String, String]  = params.toQuery()
    var fun: String => String = null
    fun = { it =>
      val qs = query.map(_.toString+"="+_).mkString("&")
      fun = _+"?"+qs
      fun(it)
    }
    for
      it <- this(baseUrl, mml, query).toIterable
    yield
      new Web(it.map(fun).swap.map((baseUrl, _, query)).swap, media, params*)


object `Web*`:

  import dimensions.three.breed.Hatch.Flavor

  final case class FlavorException(flavor: String, value: Option[Flavor.Value])
    extends base.QueensException(s"$flavor request parameter ${value match { case None => "not supported" case _ => "unknown" } }")

  import scala.collection.mutable.HashMap

  def apply(nests: List[Nest]): Map[Model, Map[Aspect, Map[Algorithm, Int]]] =
    val mml = HashMap[Model, HashMap[Aspect, HashMap[Algorithm, Int]]]()

    for
      Nest(at, am, ml) <- nests
    do
      if !mml.contains(ml) then mml(ml) = HashMap[Aspect, HashMap[Algorithm, Int]]()
      val mat = mml(ml)

      if !mat.contains(at) then mat(at) = HashMap[Algorithm, Int]()
      val mam = mat(at)

      if !mam.contains(am) then mam(am) = 0
      mam(am) += 1

    mml


  import common.given

  extension(self: Iterable[Parameter[?]])
    inline def toQuery(): Map[String, String] =
      val ps: Iterable[Option[(String, String)]] = self.map(_())
      val params: Iterable[(String, String)] = ps
      params.toMap
