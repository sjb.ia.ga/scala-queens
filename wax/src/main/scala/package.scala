package object queens {

  import java.util.UUID

  import base.Board

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]


  type Simulate = dimensions.fifth.cycle.Simulate

  type IO = dimensions.fifth.cycle.IO


  type Identity_[A] = A

  type Iterable_[X[_], A] = Iterable[A]


  import java.util.UUID

  import base.Board

  import version.less.nest.Nest

  type Cue = ((Board, Nest, Long, UUID), Long, Long, Long, Long)


  type Ctx = dimensions.fifth.fourth.third.publish.actors.Macros.Ctx



  given Conversion[String, List[String]] = _
    .split("[\\[{(<.: ;,>)}\\]]")
    .filterNot(_.isEmpty)
    //.map(_.replace('_', ' '))
    .toList


  extension(p: Boolean)
    inline def ->(q: => Boolean): Boolean = !p || q


  extension[B, C](g: B => C)
    inline def o[A](f: A => B): A => C = f andThen g


  import dimensions.third.`Queens*`

  type `Queens3*` = `Queens*`[Point, Boolean]


  // ScalaTest

}
