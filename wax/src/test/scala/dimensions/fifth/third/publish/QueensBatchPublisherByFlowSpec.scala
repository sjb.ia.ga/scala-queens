package queens
package dimensions.fifth.third
package publish
package flow

import java.util.UUID

import org.reactivestreams.{ Subscriber, Subscription }

import org.scalatest.flatspec.AnyFlatSpec

import dimensions.Dimension.Aspect.classic
import dimensions.Dimension.Algorithm.iterative
import dimensions.Dimension.Model.flow

import version.less.nest.Nest


class QueensBatchPublisherByFlowSpec
    extends AnyFlatSpec
    with QueensBatchPublisherByFlow[IO] {

  private val TEST = Object()


  "Six flow (solutions) in order 5 2 4 3 0 1" should "emit and complete successfully" in {
    Synchronized(TEST) {

      var complete = 0
      var next = 0

      val sub = new Subscriber[IO] {

        override def onSubscribe(subscription: Subscription): Unit =
          subscription.request(Long.MaxValue)

        override def onComplete(): Unit = complete += 1

        override def onNext(t: IO): Unit = next += 1

        override def onError(t: Throwable): Unit = throw t

      }

      subscribe(sub)

      var total = 0

      given Unit = ()

      def serve(i: Long, n: Nest) =
        this(i, n, (null, null, i, UUID.randomUUID) -> None)

      val ns = (0 to 5).map { _ => total += 1; Nest(classic, iterative, flow) }

      serve(5, ns(5))
      serve(2, ns(2))
      serve(4, ns(4))
      serve(3, ns(3))
      serve(0, ns(0))
      serve(1, ns(1))

      this(-7, null, null)

      assert(total == 6)
      assert(next == total && complete == 1)
    }
  }

}
