package queens
package dimensions.fifth.third
package publish
package futures

import java.util.UUID

import scala.concurrent.{ Await, ExecutionContext, Future, SyncChannel }
import scala.concurrent.duration.Duration
import ExecutionContext.Implicits._

import org.reactivestreams.{ Subscriber, Subscription }

import org.scalatest.flatspec.AnyFlatSpec

import dimensions.fifth.fourth.publish.cycle.QueensSolutionSubscriber


class QueensBatchPublisherByFuturesSpec
    extends AnyFlatSpec
    with QueensBatchPublisherByFutures[IO] {

  {
    import sys.Prop.StringProp
    sys.Prop("scala.concurrent.context.numThreads").set("x32")
  }

  import sys.Prop.IntProp
  sys.Prop("scala.concurrent.context.maxThreads").set("8192")
  sys.Prop("scala.concurrent.context.maxExtraThreads").set("8192")
  sys.Prop("scala.concurrent.context.minThreads").set("512")
  sys.Prop("java.util.concurrent.ForkJoinPool.common.parallelism").set("4096")

  private val TEST = Object()

  private val ch0 = new SyncChannel[Unit]()


  "Six futures (solutions) in order 5 2 4 3 0 1" should "emit and complete successfully" in {
    Synchronized(TEST) {

      var complete = 0
      var next = 0

      val sub = new Subscriber[IO] {

        override def onSubscribe(subscription: Subscription): Unit =
          subscription.request(Long.MaxValue)

        override def onComplete(): Unit = complete += 1

        override def onNext(t: IO): Unit = next += 1

        override def onError(t: Throwable): Unit = throw t

      }

      subscribe(sub)

      var total = 0

      def serve(i: Long, f: Future[Unit]) =
        Future { this(i, ch0 -> f, (null, null, i, UUID.randomUUID) -> None) }

      val fs = (0 to 5).map(i => Future { total += 1 })

      serve(5, fs(5))
      serve(2, fs(2))
      serve(4, fs(4))
      serve(3, fs(3))
      serve(0, fs(0))
      serve(1, fs(1))

      this(-7, null, null)
        .foreach { (ch, it) =>
          ch.write(())
          Await.ready(it, Duration.Inf)
        }

      assert(total == 6)
      assert(next == total && complete == 1)
    }
  }

}
