package queens
package dimensions.fifth.third
package publish
package actors

import java.util.UUID

import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.{ PriorityBlockingQueue => Queue }

import scala.concurrent.duration._

import org.reactivestreams.{ Subscriber, Subscription }

import akka.actor.typed.{ ActorRef, Behavior }
import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit

import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.matchers.should.Matchers

import common.Wrapper

import dimensions.fifth.fourth.third.publish.actors.Macros.{ enqueue, spawn => `spawn*`, Event }

import dimensions.fifth.fourth.third.publish.cycle.actors.{ dequeue, fifo }


class QueensBatchPublisherByActorsSpec
    extends ScalaTestWithActorTestKit
    with AnyWordSpecLike
    with QueensBatchPublisherByActors[IO] {


  object CTX extends Wrapper[ActorContext[?]]


  import QueensBatchPublisherByActorsSpec._

  "Six actors/events (solutions) in order 5 2 4 3 0 1" should {
    "emit and complete successfully" in {

      val `13`: Behavior[Test] = Behaviors
        .receive {

          case (ctx, Test(Left(ref))) =>

            val spindle = AtomicLong(0)

            var complete = 0
            var next = 0

            val total = AtomicLong(0)

            var batch: Option[Queue[(Long, Event)]] = None

            given Ctx = (CTX, ctx, spindle)

            def event(i: Int) =
              given Queue[(Long, Event)] = fifo()
              `spawn*`(i) { total.incrementAndGet } ()

            def serve(i: Long, e: Event) =
              enqueue(this, i, e, (null, null, i, UUID.randomUUID) -> None)

            val sub = new Subscriber[IO] {

              override def onSubscribe(subscription: Subscription): Unit =
                subscription.request(Long.MaxValue)

              override def onComplete(): Unit = complete += 1

              override def onNext(t: IO): Unit = next += 1

              override def onError(t: Throwable): Unit = throw t

            }

            subscribe(sub)

            val rs = (0 to 5).map(event)

            serve(5, rs(5))
            serve(2, rs(2))
            serve(4, rs(4))
            serve(3, rs(3))
            serve(0, rs(0))
            serve(1, rs(1))

            batch = this(-7, null, null)

            batch.get.dequeue(Some(()))

            ref ! Done(total.get, next, complete, batch.isEmpty, spindle.get)

            Behaviors.same

          case (ctx, Test(Right(ref))) =>

            ref ! Stop

            Behaviors.stopped
        }

      val actor = spawn(`13`, "Queens-Test")
      val done = createTestProbe[Done]()
      val stop = createTestProbe[Stop.type]()

      actor ! Test(Left(done.ref))

      done.expectMessage(6.seconds, Done(6, 6, 1, false, 0))

      actor ! Test(Right(stop.ref))

      stop.expectMessage(Stop)
    }
  }

}


object QueensBatchPublisherByActorsSpec:

  case class Test(response: Either[ActorRef[Done], ActorRef[Stop.type]])
  case class Done(total: Long, next: Long, complete: Long, empty: Boolean, spindle: Long)
  case object Stop
