libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_2.13" % "24.6.0",

  "net.codingwell" %% "scala-guice" % "7.0.0",

  "co.fs2" %% "fs2-core" % "2.5.12",

  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
