package queens
package dimensions.three
package guice

import com.google.inject.Provider

import dimensions.third.Queens

import version.less.nest.Nest


abstract trait GuiceQueens
    extends Provider[Queens[?]]:

  val nest: Nest


package `0`:

  import dimensions.Dimension.Monad

  abstract trait GuiceQueens
      extends dimensions.three.guice.GuiceQueens:

    val monad: Monad


package bis:

  import dimensions.third.bis.{ Queens => Queens2 }

  abstract trait GuiceQueens
      extends Provider[Queens2[?]]:

    val nest: Nest
