package queens
package dimensions.three


package breed:

  package guice:

    package object bis:

      import config.bis.Config

      given Config = configurers.bis.GuiceConfig

  package object guice:

    import config.Config

    given Config = configurers.GuiceConfig
