package queens
package dimensions.three
package breed
package config.guice

import scala.collection.immutable.Set

import com.google.inject.Inject

import dimensions.three.guice.GuiceQueens


class GuiceQueensWrapper @Inject() (val guiceQueens: Set[GuiceQueens])


package bis:

  import dimensions.three.guice.bis.{ GuiceQueens => GuiceQueens2 }

  class GuiceQueensWrapper @Inject() (val guiceQueens: Set[GuiceQueens2])
