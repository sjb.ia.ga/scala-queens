package queens
package dimensions.three
package breed
package configurers

import java.util.UUID

import com.google.inject.{ AbstractModule, Guice, Injector, Provider }
import net.codingwell.scalaguice.{ ScalaModule, ScalaMultibinder }
import net.codingwell.scalaguice.InjectorExtensions._

import com.github.blemale.scaffeine.Scaffeine
import scala.concurrent.duration._

import dimensions.Dimension.{ Monad, Algorithm, Aspect, Model }

import di.{ ValidatorRaw, ValidatorOpt }

import version.less.nest.Nest


object GuiceConfig
    extends config.Config:

  import config.guice.GuiceQueensWrapper

  import dimensions.third.Queens

  import dimensions.three.guice.GuiceQueens

  private var injector: Injector = _

  def init(): Unit = injector = Guice.createInjector(new ConfigModule)


  class ConfigModule extends AbstractModule with ScalaModule:

    override def configure: Unit =
      bind[Validator].toInstance(Validator())

      bind[CacheProvider].toInstance(new CacheProvider {
        override val get: Cache =
          Scaffeine()
            .recordStats()
            .expireAfterWrite(1.hour)
            .maximumSize(100000)
            .build[UUID, LazyList[Solution]]()
      })

      val guiceQueensMulti = ScalaMultibinder
        .newSetBinder[GuiceQueens](binder)

      guiceQueensMulti
        .addBinding.to[iterative.actors.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[iterative.actors.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[iterative.actors.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[iterative.actors.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[iterative.futures.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[iterative.futures.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[iterative.futures.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[iterative.futures.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[iterative.kafka.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[iterative.kafka.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[iterative.kafka.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[iterative.kafka.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[iterative.rabbitMQ.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[iterative.rabbitMQ.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[iterative.rabbitMQ.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[iterative.rabbitMQ.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[iterative.awsSQS.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[iterative.awsSQS.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[iterative.awsSQS.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[iterative.awsSQS.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.`0`.x.sync.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.`0`.x.sync.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.`0`.x.sync.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.`0`.x.sync.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.`0`.x.async.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.`0`.x.async.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.`0`.x.async.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.`0`.x.async.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.`0`.s.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.`0`.s.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.`0`.s.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[iterative.flow.guice.`0`.s.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.native.actors.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.native.actors.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.native.actors.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.native.actors.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.native.futures.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.native.futures.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.native.futures.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.native.futures.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.native.kafka.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.native.kafka.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.native.kafka.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.native.kafka.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.native.rabbitMQ.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.native.rabbitMQ.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.native.rabbitMQ.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.native.rabbitMQ.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.native.awsSQS.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.native.awsSQS.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.native.awsSQS.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.native.awsSQS.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.native.flow.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.native.flow.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.native.flow.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.native.flow.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.closure.actors.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.closure.actors.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.closure.futures.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.closure.futures.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.closure.kafka.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.closure.kafka.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.closure.rabbitMQ.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.closure.rabbitMQ.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.closure.awsSQS.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.closure.awsSQS.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.closure.flow.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.closure.flow.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.trampoline.actors.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.trampoline.actors.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.trampoline.futures.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.trampoline.futures.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.trampoline.kafka.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.trampoline.kafka.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.trampoline.rabbitMQ.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.trampoline.rabbitMQ.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.trampoline.awsSQS.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.trampoline.awsSQS.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.trampoline.flow.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.trampoline.flow.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.extern.actors.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.extern.actors.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.extern.actors.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.extern.actors.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.extern.futures.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.extern.futures.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.extern.futures.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.extern.futures.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.extern.kafka.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.extern.kafka.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.extern.kafka.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.extern.kafka.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.extern.rabbitMQ.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.extern.rabbitMQ.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.extern.rabbitMQ.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.extern.rabbitMQ.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.extern.awsSQS.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.extern.awsSQS.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.extern.awsSQS.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.extern.awsSQS.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.`0`.x.sync.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.`0`.x.sync.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.`0`.x.sync.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.`0`.x.sync.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.`0`.x.async.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.`0`.x.async.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.`0`.x.async.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.`0`.x.async.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.`0`.s.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.`0`.s.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.`0`.s.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.extern.flow.guice.`0`.s.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.continuation.actors.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.actors.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.actors.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.actors.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.continuation.futures.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.futures.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.futures.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.futures.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.continuation.kafka.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.kafka.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.kafka.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.kafka.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.continuation.rabbitMQ.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.rabbitMQ.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.rabbitMQ.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.rabbitMQ.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.continuation.awsSQS.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.awsSQS.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.awsSQS.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.awsSQS.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.continuation.flow.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.flow.guice.GuiceQueensClassicCb]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.flow.guice.GuiceQueensStepper1]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.flow.guice.GuiceQueensStepperP]

      guiceQueensMulti
        .addBinding.to[recursive.continuation.flow.guice.`0`.x.sync.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.flow.guice.`0`.x.sync.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.continuation.flow.guice.`0`.x.async.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.flow.guice.`0`.x.async.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.continuation.flow.guice.`0`.s.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.continuation.flow.guice.`0`.s.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.tailcall.actors.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.tailcall.actors.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.tailcall.futures.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.tailcall.futures.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.tailcall.kafka.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.tailcall.kafka.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.tailcall.rabbitMQ.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.tailcall.rabbitMQ.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.tailcall.awsSQS.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.tailcall.awsSQS.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.tailcall.flow.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.tailcall.flow.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.catseval.actors.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.catseval.actors.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.catseval.futures.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.catseval.futures.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.catseval.kafka.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.catseval.kafka.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.catseval.rabbitMQ.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.catseval.rabbitMQ.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.catseval.awsSQS.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.catseval.awsSQS.guice.GuiceQueensClassicCb]

      guiceQueensMulti
        .addBinding.to[recursive.catseval.flow.guice.GuiceQueensClassic]
      guiceQueensMulti
        .addBinding.to[recursive.catseval.flow.guice.GuiceQueensClassicCb]


  private final class CfgByModel(using model: Model) extends ByModel:

    private final class CfgOfAlgorithm(using algorithm: Algorithm) extends OfAlgorithm:

      private final class CfgToAspect(using aspect: Aspect) extends ToAspect:

        final class `Cfg Hatch`[Q <: Queens[?]] extends Hatch[Q]:

          override def apply(): Q =
            require(injector ne null)

            val wrapper = injector
              .instance[GuiceQueensWrapper]

            val nest = Nest(aspect, algorithm, model)

            wrapper
              .guiceQueens
              .filterNot(_.isInstanceOf[dimensions.three.guice.`0`.GuiceQueens])
              .find(_.nest == nest)
              .get
              .get
              .asInstanceOf[Q]

        override def breed[Q <: Queens[?]]: Hatch[Q] = new `Cfg Hatch`[Q]

        private final class CfgAtMonad(using monad: Monad) extends AtMonad:

          final class `Cfg Hatch`[Q <: Queens[?]] extends Hatch[Q]:

            override def apply(): Q =
              require(injector ne null)

              val wrapper = injector
                .instance[GuiceQueensWrapper]

              val nest = Nest(aspect, algorithm, model)

              wrapper
                .guiceQueens
                .filter(_.isInstanceOf[dimensions.three.guice.`0`.GuiceQueens])
                .map(_.asInstanceOf[dimensions.three.guice.`0`.GuiceQueens])
                .find { it => it.monad == monad && it.nest == nest }
                .get
                .get
                .asInstanceOf[Q]

          override def breed[Q <: Queens[?]]: Hatch[Q] = new `Cfg Hatch`[Q]

        override def atMonad(using Monad): AtMonad = new CfgAtMonad

      override def toAspect(using Aspect): ToAspect = new CfgToAspect

    override def ofAlgorithm(using Algorithm): OfAlgorithm = new CfgOfAlgorithm

  override def byModel(using Model): ByModel = new CfgByModel


package bis:

  object GuiceConfig
      extends config.bis.Config:

    import config.guice.bis.GuiceQueensWrapper

    import dimensions.third.bis.Queens

    import dimensions.three.guice.bis.GuiceQueens

    private var injector: Injector = _

    def init(): Unit = injector = Guice.createInjector(new ConfigModule)


    class ConfigModule extends AbstractModule with ScalaModule:

      override def configure: Unit =
        bind[Validator].toInstance(Validator())

        bind[CacheProvider].toInstance(new CacheProvider {
          override val get: Cache =
            Scaffeine()
              .recordStats()
              .expireAfterWrite(1.hour)
              .maximumSize(100000)
              .build[UUID, LazyList[Solution]]()
        })

        val guiceQueensMulti = ScalaMultibinder
          .newSetBinder[GuiceQueens](binder)

        guiceQueensMulti
          .addBinding.to[iterative.actors.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[iterative.actors.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[iterative.actors.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[iterative.actors.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[iterative.futures.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[iterative.futures.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[iterative.futures.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[iterative.futures.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[iterative.kafka.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[iterative.kafka.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[iterative.kafka.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[iterative.kafka.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[iterative.rabbitMQ.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[iterative.rabbitMQ.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[iterative.rabbitMQ.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[iterative.rabbitMQ.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[iterative.awsSQS.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[iterative.awsSQS.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[iterative.awsSQS.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[iterative.awsSQS.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[iterative.flow.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[iterative.flow.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[iterative.flow.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[iterative.flow.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.native.actors.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.native.actors.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.native.actors.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.native.actors.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.native.futures.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.native.futures.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.native.futures.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.native.futures.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.native.kafka.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.native.kafka.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.native.kafka.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.native.kafka.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.native.rabbitMQ.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.native.rabbitMQ.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.native.rabbitMQ.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.native.rabbitMQ.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.native.awsSQS.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.native.awsSQS.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.native.awsSQS.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.native.awsSQS.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.native.flow.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.native.flow.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.native.flow.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.native.flow.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.closure.actors.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.closure.actors.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.closure.futures.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.closure.futures.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.closure.kafka.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.closure.kafka.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.closure.rabbitMQ.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.closure.rabbitMQ.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.closure.awsSQS.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.closure.awsSQS.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.closure.flow.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.closure.flow.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.trampoline.actors.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.trampoline.actors.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.trampoline.futures.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.trampoline.futures.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.trampoline.kafka.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.trampoline.kafka.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.trampoline.rabbitMQ.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.trampoline.rabbitMQ.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.trampoline.awsSQS.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.trampoline.awsSQS.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.trampoline.flow.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.trampoline.flow.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.extern.actors.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.extern.actors.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.extern.actors.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.extern.actors.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.extern.futures.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.extern.futures.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.extern.futures.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.extern.futures.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.extern.kafka.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.extern.kafka.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.extern.kafka.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.extern.kafka.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.extern.rabbitMQ.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.extern.rabbitMQ.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.extern.rabbitMQ.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.extern.rabbitMQ.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.extern.awsSQS.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.extern.awsSQS.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.extern.awsSQS.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.extern.awsSQS.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.extern.flow.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.extern.flow.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.extern.flow.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.extern.flow.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.continuation.actors.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.actors.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.actors.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.actors.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.continuation.futures.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.futures.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.futures.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.futures.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.continuation.kafka.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.kafka.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.kafka.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.kafka.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.continuation.rabbitMQ.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.rabbitMQ.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.rabbitMQ.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.rabbitMQ.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.continuation.awsSQS.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.awsSQS.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.awsSQS.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.awsSQS.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.continuation.flow.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.flow.guice.bis.GuiceQueensClassicCb]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.flow.guice.bis.GuiceQueensStepper1]
        guiceQueensMulti
          .addBinding.to[recursive.continuation.flow.guice.bis.GuiceQueensStepperP]

        guiceQueensMulti
          .addBinding.to[recursive.tailcall.actors.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.tailcall.actors.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.tailcall.futures.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.tailcall.futures.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.tailcall.kafka.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.tailcall.kafka.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.tailcall.rabbitMQ.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.tailcall.rabbitMQ.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.tailcall.awsSQS.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.tailcall.awsSQS.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.tailcall.flow.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.tailcall.flow.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.catseval.actors.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.catseval.actors.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.catseval.futures.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.catseval.futures.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.catseval.kafka.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.catseval.kafka.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.catseval.rabbitMQ.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.catseval.rabbitMQ.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.catseval.awsSQS.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.catseval.awsSQS.guice.bis.GuiceQueensClassicCb]

        guiceQueensMulti
          .addBinding.to[recursive.catseval.flow.guice.bis.GuiceQueensClassic]
        guiceQueensMulti
          .addBinding.to[recursive.catseval.flow.guice.bis.GuiceQueensClassicCb]


    private final class CfgByModel(using model: Model) extends ByModel:

      private final class CfgOfAlgorithm(using algorithm: Algorithm) extends OfAlgorithm:

        private final class CfgToAspect(using aspect: Aspect) extends ToAspect:

          final class `Cfg Hatch`[Q <: Queens[?]] extends Hatch[Q]:

            override def apply(): Q =
              require(injector ne null)

              val wrapper = injector
                .instance[GuiceQueensWrapper]

              val nest = Nest(aspect, algorithm, model)

              wrapper
                .guiceQueens
                .find(_.nest == nest)
                .get
                .get
                .asInstanceOf[Q]

          override def breed[Q <: Queens[?]]: Hatch[Q] = new `Cfg Hatch`[Q]

        override def toAspect(using Aspect): ToAspect = new CfgToAspect

      override def ofAlgorithm(using Algorithm): OfAlgorithm = new CfgOfAlgorithm

    override def byModel(using Model): ByModel = new CfgByModel
