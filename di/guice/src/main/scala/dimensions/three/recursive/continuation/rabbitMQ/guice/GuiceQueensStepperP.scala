package queens
package dimensions.three
package recursive.continuation
package rabbitMQ
package guice

import com.google.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.stepperP
import dimensions.Dimension.Algorithm.continuation
import dimensions.Dimension.Model.rabbitMQ

import dimensions.three.guice.GuiceQueens

import version.less.nest.Nest


class GuiceQueensStepperP @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends GuiceQueens {

  final override val nest = Nest(stepperP, continuation, rabbitMQ)

  override def get(): QueensStepperP =
    new QueensStepperP(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)

}


package bis:

  import dimensions.three.recursive.continuation.rabbitMQ.bis.QueensStepperP

  import dimensions.three.guice.bis.{ GuiceQueens => GuiceQueens2 }

  class GuiceQueensStepperP @Inject() (
    _v: Validator
  ) extends GuiceQueens2:

    final override val nest = Nest(stepperP, continuation, rabbitMQ)

    override def get(): QueensStepperP = new QueensStepperP(_v)
