package queens
package dimensions.three
package recursive.extern
package awsSQS
package guice

import com.google.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.stepper1
import dimensions.Dimension.Algorithm.extern
import dimensions.Dimension.Model.awsSQS

import dimensions.three.guice.GuiceQueens

import version.less.nest.Nest


class GuiceQueensStepper1 @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends GuiceQueens {

  final override val nest = Nest(stepper1, extern, awsSQS)

  override def get(): QueensStepper1 =
    new QueensStepper1(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)

}


package bis:

  import dimensions.three.recursive.extern.awsSQS.bis.QueensStepper1

  import dimensions.three.guice.bis.{ GuiceQueens => GuiceQueens2 }

  class GuiceQueensStepper1 @Inject() (
    _v: Validator
  ) extends GuiceQueens2:

    final override val nest = Nest(stepper1, extern, awsSQS)

    override def get(): QueensStepper1 = new QueensStepper1(_v)
