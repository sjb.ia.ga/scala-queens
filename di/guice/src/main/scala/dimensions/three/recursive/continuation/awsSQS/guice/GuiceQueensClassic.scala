package queens
package dimensions.three
package recursive.continuation
package awsSQS
package guice

import com.google.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.classic
import dimensions.Dimension.Algorithm.continuation
import dimensions.Dimension.Model.awsSQS

import dimensions.three.guice.GuiceQueens

import version.less.nest.Nest


class GuiceQueensClassic @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends GuiceQueens {

  final override val nest = Nest(classic, continuation, awsSQS)

  override def get(): QueensClassic =
    new QueensClassic(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)

}


package bis:

  import dimensions.three.recursive.continuation.awsSQS.bis.QueensClassic

  import dimensions.three.guice.bis.{ GuiceQueens => GuiceQueens2 }

  class GuiceQueensClassic @Inject() (
    _v: Validator
  ) extends GuiceQueens2:

    final override val nest = Nest(classic, continuation, awsSQS)

    override def get(): QueensClassic = new QueensClassic(_v)
