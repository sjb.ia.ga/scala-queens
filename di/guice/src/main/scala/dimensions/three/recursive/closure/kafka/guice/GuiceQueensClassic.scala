package queens
package dimensions.three
package recursive.closure
package kafka
package guice

import com.google.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.classic
import dimensions.Dimension.Algorithm.closure
import dimensions.Dimension.Model.kafka

import dimensions.three.guice.GuiceQueens

import version.less.nest.Nest


class GuiceQueensClassic @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends GuiceQueens {

  final override val nest = Nest(classic, closure, kafka)

  override def get(): QueensClassic =
    new QueensClassic(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)

}


package bis:

  import dimensions.three.recursive.closure.kafka.bis.QueensClassic

  import dimensions.three.guice.bis.{ GuiceQueens => GuiceQueens2 }

  class GuiceQueensClassic @Inject() (
    _v: Validator
  ) extends GuiceQueens2:

    final override val nest = Nest(classic, closure, kafka)

    override def get(): QueensClassic = new QueensClassic(_v)
