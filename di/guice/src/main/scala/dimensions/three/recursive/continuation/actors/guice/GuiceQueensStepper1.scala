package queens
package dimensions.three
package recursive.continuation
package actors
package guice

import com.google.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.stepper1
import dimensions.Dimension.Algorithm.continuation
import dimensions.Dimension.Model.actors

import dimensions.three.guice.GuiceQueens

import version.less.nest.Nest


class GuiceQueensStepper1 @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends GuiceQueens {

  final override val nest = Nest(stepper1, continuation, actors)

  override def get(): QueensStepper1 =
    new QueensStepper1(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)

}


package bis:

  import dimensions.three.recursive.continuation.actors.bis.QueensStepper1

  import dimensions.three.guice.bis.{ GuiceQueens => GuiceQueens2 }

  class GuiceQueensStepper1 @Inject() (
    _v: Validator
  ) extends GuiceQueens2:

    final override val nest = Nest(stepper1, continuation, actors)

    override def get(): QueensStepper1 = new QueensStepper1(_v)
