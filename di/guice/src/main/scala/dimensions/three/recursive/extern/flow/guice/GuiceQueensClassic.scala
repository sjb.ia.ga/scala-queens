package queens
package dimensions.three
package recursive.extern
package flow
package guice

import com.google.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.classic
import dimensions.Dimension.Algorithm.extern
import dimensions.Dimension.Model.flow

import dimensions.three.guice.GuiceQueens

import version.less.nest.Nest


class GuiceQueensClassic @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends GuiceQueens:

  final override val nest = Nest(classic, extern, flow)

  override def get(): QueensClassic =
    new QueensClassic(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)


package `0`:

  package x:

    package sync:

      import dimensions.Dimension.Monad.monixIterantCoeval

      import dimensions.three.zeroth.recursive.extern.flow.x.sync.QueensClassic

      import dimensions.three.guice.`0`.{ GuiceQueens => GuiceQueens0 }

      class GuiceQueensClassic @Inject() (
        _v: Validator
      ) extends GuiceQueens0:

        final override val monad = monixIterantCoeval
        final override val nest = Nest(classic, extern, flow)

        override def get(): QueensClassic = new QueensClassic(_v)

    package async:

      import dimensions.Dimension.Monad.monixIterantTask

      import dimensions.three.zeroth.recursive.extern.flow.x.async.QueensClassic

      import dimensions.three.guice.`0`.{ GuiceQueens => GuiceQueens0 }

      class GuiceQueensClassic @Inject() (
        _v: Validator
      ) extends GuiceQueens0:

        final override val monad = monixIterantTask
        final override val nest = Nest(classic, extern, flow)

        override def get(): QueensClassic = new QueensClassic(_v)

  package s:

    import dimensions.Dimension.Monad.fs2StreamIO

    import dimensions.three.zeroth.recursive.extern.flow.s.QueensClassic

    import dimensions.three.guice.`0`.{ GuiceQueens => GuiceQueens0 }

    class GuiceQueensClassic @Inject() (
      _v: Validator
    ) extends GuiceQueens0:

      final override val monad = fs2StreamIO
      final override val nest = Nest(classic, extern, flow)

      override def get(): QueensClassic = new QueensClassic(_v)


package bis:

  import dimensions.three.recursive.extern.flow.bis.QueensClassic

  import dimensions.three.guice.bis.{ GuiceQueens => GuiceQueens2 }

  class GuiceQueensClassic @Inject() (
    _v: Validator
  ) extends GuiceQueens2:

    final override val nest = Nest(classic, extern, flow)

    override def get(): QueensClassic = new QueensClassic(_v)
