package queens
package dimensions.three
package iterative
package flow
package guice

import com.google.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.stepper1
import dimensions.Dimension.Algorithm.iterative
import dimensions.Dimension.Model.flow

import dimensions.three.guice.GuiceQueens

import version.less.nest.Nest


class GuiceQueensStepper1 @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends GuiceQueens:

  final override val nest = Nest(stepper1, iterative, flow)

  override def get(): QueensStepper1 =
    new QueensStepper1(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)


package `0`:

  package x:

    package sync:

      import dimensions.Dimension.Monad.monixIterantCoeval

      import dimensions.three.zeroth.iterative.flow.x.sync.QueensStepper1

      import dimensions.three.guice.`0`.{ GuiceQueens => GuiceQueens0 }

      class GuiceQueensStepper1 @Inject() (
        _v: Validator
      ) extends GuiceQueens0:

        final override val monad = monixIterantCoeval
        final override val nest = Nest(stepper1, iterative, flow)

        override def get(): QueensStepper1 = new QueensStepper1(_v)

    package async:

      import dimensions.Dimension.Monad.monixIterantTask

      import dimensions.three.zeroth.iterative.flow.x.async.QueensStepper1

      import dimensions.three.guice.`0`.{ GuiceQueens => GuiceQueens0 }

      class GuiceQueensStepper1 @Inject() (
        _v: Validator
      ) extends GuiceQueens0:

        final override val monad = monixIterantTask
        final override val nest = Nest(stepper1, iterative, flow)

        override def get(): QueensStepper1 = new QueensStepper1(_v)

  package s:

    import dimensions.Dimension.Monad.fs2StreamIO

    import dimensions.three.zeroth.iterative.flow.s.QueensStepper1

    import dimensions.three.guice.`0`.{ GuiceQueens => GuiceQueens0 }

    class GuiceQueensStepper1 @Inject() (
      _v: Validator
    ) extends GuiceQueens0:

      final override val monad = fs2StreamIO
      final override val nest = Nest(stepper1, iterative, flow)

      override def get(): QueensStepper1 = new QueensStepper1(_v)


package bis:

  import dimensions.three.iterative.flow.bis.QueensStepper1

  import dimensions.three.guice.bis.{ GuiceQueens => GuiceQueens2 }

  class GuiceQueensStepper1 @Inject() (
    _v: Validator
  ) extends GuiceQueens2:

    final override val nest = Nest(stepper1, iterative, flow)

    override def get(): QueensStepper1 = new QueensStepper1(_v)
