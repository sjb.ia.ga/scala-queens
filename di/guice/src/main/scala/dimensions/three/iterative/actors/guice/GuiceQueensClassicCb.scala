package queens
package dimensions.three
package iterative
package actors
package guice

import com.google.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.classicCb
import dimensions.Dimension.Algorithm.iterative
import dimensions.Dimension.Model.actors

import dimensions.three.guice.GuiceQueens

import version.less.nest.Nest


class GuiceQueensClassicCb @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends GuiceQueens {

  final override val nest = Nest(classicCb, iterative, actors)

  override def get(): QueensClassicCb =
    new QueensClassicCb(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)

}


package bis:

  import dimensions.three.iterative.actors.bis.QueensClassicCb

  import dimensions.three.guice.bis.{ GuiceQueens => GuiceQueens2 }

  class GuiceQueensClassicCb @Inject() (
    _v: Validator
  ) extends GuiceQueens2:

    final override val nest = Nest(classicCb, iterative, actors)

    override def get(): QueensClassicCb = new QueensClassicCb(_v)
