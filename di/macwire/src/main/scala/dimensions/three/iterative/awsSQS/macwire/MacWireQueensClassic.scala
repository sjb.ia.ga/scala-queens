package queens
package dimensions.three
package iterative
package awsSQS
package macwire

import javax.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.classic
import dimensions.Dimension.Algorithm.iterative
import dimensions.Dimension.Model.awsSQS

import dimensions.three.macwire.MacWireQueens

import version.less.nest.Nest


class MacWireQueensClassic @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends MacWireQueens:

  final override val nest = Nest(classic, iterative, awsSQS)

  override def get() =
    new QueensClassic(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)


package bis:

  import dimensions.three.iterative.awsSQS.bis.QueensClassic

  import dimensions.three.macwire.bis.{ MacWireQueens => MacWireQueens2 }

  class MacWireQueensClassic @Inject() (
    _v: Validator
  ) extends MacWireQueens2:

    final override val nest = Nest(classic, iterative, awsSQS)

    override def get(): QueensClassic = new QueensClassic(_v)
