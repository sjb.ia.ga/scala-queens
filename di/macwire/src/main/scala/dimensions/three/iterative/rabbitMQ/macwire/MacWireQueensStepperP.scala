package queens
package dimensions.three
package iterative
package rabbitMQ
package macwire

import javax.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.stepperP
import dimensions.Dimension.Algorithm.iterative
import dimensions.Dimension.Model.rabbitMQ

import dimensions.three.macwire.MacWireQueens

import version.less.nest.Nest


class MacWireQueensStepperP @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends MacWireQueens:

  final override val nest = Nest(stepperP, iterative, rabbitMQ)

  override def get() =
    new QueensStepperP(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)


package bis:

  import dimensions.three.iterative.rabbitMQ.bis.QueensStepperP

  import dimensions.three.macwire.bis.{ MacWireQueens => MacWireQueens2 }

  class MacWireQueensStepperP @Inject() (
    _v: Validator
  ) extends MacWireQueens2:

    final override val nest = Nest(stepperP, iterative, rabbitMQ)

    override def get(): QueensStepperP = new QueensStepperP(_v)
