package queens
package dimensions.three
package recursive.extern
package flow
package macwire

import javax.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.stepper1
import dimensions.Dimension.Algorithm.extern
import dimensions.Dimension.Model.flow

import dimensions.three.macwire.MacWireQueens

import version.less.nest.Nest


class MacWireQueensStepper1 @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends MacWireQueens:

  final override val nest = Nest(stepper1, extern, flow)

  override def get() =
    new QueensStepper1(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)


package `0`:

  package x:

    package sync:

      import dimensions.Dimension.Monad.monixIterantCoeval

      import dimensions.three.zeroth.recursive.extern.flow.x.sync.QueensStepper1

      import dimensions.three.macwire.`0`.{ MacWireQueens => MacWireQueens0 }

      class MacWireQueensStepper1 @Inject() (
        _v: Validator
      ) extends MacWireQueens0:

        final override val monad = monixIterantCoeval
        final override val nest = Nest(stepper1, extern, flow)

        override def get(): QueensStepper1 = new QueensStepper1(_v)

    package async:

      import dimensions.Dimension.Monad.monixIterantTask

      import dimensions.three.zeroth.recursive.extern.flow.x.async.QueensStepper1

      import dimensions.three.macwire.`0`.{ MacWireQueens => MacWireQueens0 }

      class MacWireQueensStepper1 @Inject() (
        _v: Validator
      ) extends MacWireQueens0:

        final override val monad = monixIterantTask
        final override val nest = Nest(stepper1, extern, flow)

        override def get(): QueensStepper1 = new QueensStepper1(_v)

  package s:

    import dimensions.Dimension.Monad.fs2StreamIO

    import dimensions.three.zeroth.recursive.extern.flow.s.QueensStepper1

    import dimensions.three.macwire.`0`.{ MacWireQueens => MacWireQueens0 }

    class MacWireQueensStepper1 @Inject() (
      _v: Validator
    ) extends MacWireQueens0:

      final override val monad = fs2StreamIO
      final override val nest = Nest(stepper1, extern, flow)

      override def get(): QueensStepper1 = new QueensStepper1(_v)


package bis:

  import dimensions.three.recursive.extern.flow.bis.QueensStepper1

  import dimensions.three.macwire.bis.{ MacWireQueens => MacWireQueens2 }

  class MacWireQueensStepper1 @Inject() (
    _v: Validator
  ) extends MacWireQueens2:

    final override val nest = Nest(stepper1, extern, flow)

    override def get(): QueensStepper1 = new QueensStepper1(_v)
