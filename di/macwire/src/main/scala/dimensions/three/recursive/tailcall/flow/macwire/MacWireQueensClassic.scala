package queens
package dimensions.three
package recursive.tailcall
package flow
package macwire

import javax.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.classic
import dimensions.Dimension.Algorithm.tailcall
import dimensions.Dimension.Model.flow

import dimensions.three.macwire.MacWireQueens

import version.less.nest.Nest


class MacWireQueensClassic @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends MacWireQueens:

  final override val nest = Nest(classic, tailcall, flow)

  override def get() =
    new QueensClassic(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)


package bis:

  import dimensions.three.recursive.tailcall.flow.bis.QueensClassic

  import dimensions.three.macwire.bis.{ MacWireQueens => MacWireQueens2 }

  class MacWireQueensClassic @Inject() (
    _v: Validator
  ) extends MacWireQueens2:

    final override val nest = Nest(classic, tailcall, flow)

    override def get(): QueensClassic = new QueensClassic(_v)
