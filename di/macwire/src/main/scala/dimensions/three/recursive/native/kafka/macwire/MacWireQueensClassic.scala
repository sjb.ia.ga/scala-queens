package queens
package dimensions.three
package recursive.native
package kafka
package macwire

import javax.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.classic
import dimensions.Dimension.Algorithm.native
import dimensions.Dimension.Model.kafka

import dimensions.three.macwire.MacWireQueens

import version.less.nest.Nest


class MacWireQueensClassic @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends MacWireQueens:

  final override val nest = Nest(classic, native, kafka)

  override def get() =
    new QueensClassic(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)


package bis:

  import dimensions.three.recursive.native.kafka.bis.QueensClassic

  import dimensions.three.macwire.bis.{ MacWireQueens => MacWireQueens2 }

  class MacWireQueensClassic @Inject() (
    _v: Validator
  ) extends MacWireQueens2:

    final override val nest = Nest(classic, native, kafka)

    override def get(): QueensClassic = new QueensClassic(_v)
