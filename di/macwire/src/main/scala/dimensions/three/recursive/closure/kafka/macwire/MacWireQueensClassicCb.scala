package queens
package dimensions.three
package recursive.closure
package kafka
package macwire

import javax.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.classicCb
import dimensions.Dimension.Algorithm.closure
import dimensions.Dimension.Model.kafka

import dimensions.three.macwire.MacWireQueens

import version.less.nest.Nest


class MacWireQueensClassicCb @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends MacWireQueens:

  final override val nest = Nest(classicCb, closure, kafka)

  override def get() =
    new QueensClassicCb(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)


package bis:

  import dimensions.three.recursive.closure.kafka.bis.QueensClassicCb

  import dimensions.three.macwire.bis.{ MacWireQueens => MacWireQueens2 }

  class MacWireQueensClassicCb @Inject() (
    _v: Validator
  ) extends MacWireQueens2:

    final override val nest = Nest(classicCb, closure, kafka)

    override def get(): QueensClassicCb = new QueensClassicCb(_v)
