package queens
package dimensions.three
package recursive.continuation
package flow
package macwire

import javax.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.classicCb
import dimensions.Dimension.Algorithm.continuation
import dimensions.Dimension.Model.flow

import dimensions.three.macwire.MacWireQueens

import version.less.nest.Nest


class MacWireQueensClassicCb @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends MacWireQueens:

  final override val nest = Nest(classicCb, continuation, flow)

  override def get() =
    new QueensClassicCb(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)


package `0`:

  package x:

    package sync:

      import dimensions.Dimension.Monad.monixIterantCoeval

      import dimensions.three.zeroth.recursive.continuation.flow.x.sync.QueensClassicCb

      import dimensions.three.macwire.`0`.{ MacWireQueens => MacWireQueens0 }

      class MacWireQueensClassicCb @Inject() (
        _v: Validator
      ) extends MacWireQueens0:

        final override val monad = monixIterantCoeval
        final override val nest = Nest(classicCb, continuation, flow)

        override def get(): QueensClassicCb = new QueensClassicCb(_v)

    package async:

      import dimensions.Dimension.Monad.monixIterantTask

      import dimensions.three.zeroth.recursive.continuation.flow.x.async.QueensClassicCb

      import dimensions.three.macwire.`0`.{ MacWireQueens => MacWireQueens0 }

      class MacWireQueensClassicCb @Inject() (
        _v: Validator
      ) extends MacWireQueens0:

        final override val monad = monixIterantTask
        final override val nest = Nest(classicCb, continuation, flow)

        override def get(): QueensClassicCb = new QueensClassicCb(_v)

  package s:

    import dimensions.Dimension.Monad.fs2StreamIO

    import dimensions.three.zeroth.recursive.continuation.flow.s.QueensClassicCb

    import dimensions.three.macwire.`0`.{ MacWireQueens => MacWireQueens0 }

    class MacWireQueensClassicCb @Inject() (
      _v: Validator
    ) extends MacWireQueens0:

      final override val monad = fs2StreamIO
      final override val nest = Nest(classicCb, continuation, flow)

      override def get(): QueensClassicCb = new QueensClassicCb(_v)


package bis:

  import dimensions.three.recursive.continuation.flow.bis.QueensClassicCb

  import dimensions.three.macwire.bis.{ MacWireQueens => MacWireQueens2 }

  class MacWireQueensClassicCb @Inject() (
    _v: Validator
  ) extends MacWireQueens2:

    final override val nest = Nest(classicCb, continuation, flow)

    override def get(): QueensClassicCb = new QueensClassicCb(_v)
