package queens
package dimensions.three
package recursive.native
package kafka
package macwire

import javax.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.stepper1
import dimensions.Dimension.Algorithm.native
import dimensions.Dimension.Model.kafka

import dimensions.three.macwire.MacWireQueens

import version.less.nest.Nest


class MacWireQueensStepper1 @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends MacWireQueens:

  final override val nest = Nest(stepper1, native, kafka)

  override def get() =
    new QueensStepper1(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)


package bis:

  import dimensions.three.recursive.native.kafka.bis.QueensStepper1

  import dimensions.three.macwire.bis.{ MacWireQueens => MacWireQueens2 }

  class MacWireQueensStepper1 @Inject() (
    _v: Validator
  ) extends MacWireQueens2:

    final override val nest = Nest(stepper1, native, kafka)

    override def get(): QueensStepper1 = new QueensStepper1(_v)
