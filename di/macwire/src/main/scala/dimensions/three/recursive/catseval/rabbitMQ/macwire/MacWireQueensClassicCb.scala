package queens
package dimensions.three
package recursive.catseval
package rabbitMQ
package macwire

import javax.inject.Inject

import cache.CacheOpsWithImpl

import dimensions.Dimension.Aspect.classicCb
import dimensions.Dimension.Algorithm.catseval
import dimensions.Dimension.Model.rabbitMQ

import dimensions.three.macwire.MacWireQueens

import version.less.nest.Nest


class MacWireQueensClassicCb @Inject() (
  _v: Validator,
  _c: CacheProvider
) extends MacWireQueens:

  final override val nest = Nest(classicCb, catseval, rabbitMQ)

  override def get() =
    new QueensClassicCb(_v):
      override protected val cacheOps =
        new CacheOpsWithImpl(_c.get)


package bis:

  import dimensions.three.recursive.catseval.rabbitMQ.bis.QueensClassicCb

  import dimensions.three.macwire.bis.{ MacWireQueens => MacWireQueens2 }

  class MacWireQueensClassicCb @Inject() (
    _v: Validator
  ) extends MacWireQueens2:

    final override val nest = Nest(classicCb, catseval, rabbitMQ)

    override def get(): QueensClassicCb = new QueensClassicCb(_v)
