package queens
package dimensions.three
package breed
package config.macwire

import javax.inject.Inject

import dimensions.three.macwire.MacWireQueens


class MacWireQueensWrapper @Inject() (val macwireQueens: Set[MacWireQueens])


package bis:

  import dimensions.three.macwire.bis.{ MacWireQueens => MacWireQueens2 }

  class MacWireQueensWrapper @Inject() (val macwireQueens: Set[MacWireQueens2])
