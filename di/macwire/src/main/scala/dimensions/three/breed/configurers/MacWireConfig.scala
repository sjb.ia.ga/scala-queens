package queens
package dimensions.three
package breed
package configurers

import java.util.UUID

import com.softwaremill.macwire._

import com.github.blemale.scaffeine.Scaffeine
import scala.concurrent.duration._

import dimensions.Dimension.{ Monad, Algorithm, Aspect, Model }

import di.{ ValidatorRaw, ValidatorOpt, Validator2 }

import version.less.nest.Nest


object MacWireConfig
    extends config.Config:

  import config.macwire.MacWireQueensWrapper

  import dimensions.third.Queens

  import dimensions.three.macwire.MacWireQueens

  private var wrapper: MacWireQueensWrapper = null

  def init(): Unit = synchronized {

    init1(
      {
        val plainjava = sys.BooleanProp
          .keyExists(_root_.queens.main.Properties.Validator.plainjava)
          .value
        if plainjava
        then
          wire[Validator2]
        else
          val opt = sys.BooleanProp
            .keyExists(_root_.queens.main.Properties.Validator.optimized)
            .value
          if opt
          then
            wire[ValidatorOpt]
          else
            wire[ValidatorRaw]
      }
    ,
      new CacheProvider:
        override val get: Cache =
          Scaffeine()
            .recordStats()
            .expireAfterWrite(1.hour)
            .maximumSize(100000)
            .build[UUID, LazyList[Solution]]()
    )

    def init1(
      validator: Validator,
      cacheProvider: CacheProvider
    ) =
      init2(
        wire[iterative.actors.macwire.MacWireQueensClassic],
        wire[iterative.actors.macwire.MacWireQueensClassicCb],
        wire[iterative.actors.macwire.MacWireQueensStepper1],
        wire[iterative.actors.macwire.MacWireQueensStepperP],

        wire[iterative.futures.macwire.MacWireQueensClassic],
        wire[iterative.futures.macwire.MacWireQueensClassicCb],
        wire[iterative.futures.macwire.MacWireQueensStepper1],
        wire[iterative.futures.macwire.MacWireQueensStepperP],

        wire[iterative.kafka.macwire.MacWireQueensClassic],
        wire[iterative.kafka.macwire.MacWireQueensClassicCb],
        wire[iterative.kafka.macwire.MacWireQueensStepper1],
        wire[iterative.kafka.macwire.MacWireQueensStepperP],

        wire[iterative.rabbitMQ.macwire.MacWireQueensClassic],
        wire[iterative.rabbitMQ.macwire.MacWireQueensClassicCb],
        wire[iterative.rabbitMQ.macwire.MacWireQueensStepper1],
        wire[iterative.rabbitMQ.macwire.MacWireQueensStepperP],

        wire[iterative.awsSQS.macwire.MacWireQueensClassic],
        wire[iterative.awsSQS.macwire.MacWireQueensClassicCb],
        wire[iterative.awsSQS.macwire.MacWireQueensStepper1],
        wire[iterative.awsSQS.macwire.MacWireQueensStepperP],

        wire[iterative.flow.macwire.MacWireQueensClassic],
        wire[iterative.flow.macwire.MacWireQueensClassicCb],
        wire[iterative.flow.macwire.MacWireQueensStepper1],
        wire[iterative.flow.macwire.MacWireQueensStepperP],

        wire[iterative.flow.macwire.`0`.x.sync.MacWireQueensClassic],
        wire[iterative.flow.macwire.`0`.x.sync.MacWireQueensClassicCb],
        wire[iterative.flow.macwire.`0`.x.sync.MacWireQueensStepper1],
        wire[iterative.flow.macwire.`0`.x.sync.MacWireQueensStepperP],

        wire[iterative.flow.macwire.`0`.x.async.MacWireQueensClassic],
        wire[iterative.flow.macwire.`0`.x.async.MacWireQueensClassicCb],
        wire[iterative.flow.macwire.`0`.x.async.MacWireQueensStepper1],
        wire[iterative.flow.macwire.`0`.x.async.MacWireQueensStepperP],

        wire[iterative.flow.macwire.`0`.s.MacWireQueensClassic],
        wire[iterative.flow.macwire.`0`.s.MacWireQueensClassicCb],
        wire[iterative.flow.macwire.`0`.s.MacWireQueensStepper1],
        wire[iterative.flow.macwire.`0`.s.MacWireQueensStepperP],

        wire[recursive.native.actors.macwire.MacWireQueensClassic],
        wire[recursive.native.actors.macwire.MacWireQueensClassicCb],
        wire[recursive.native.actors.macwire.MacWireQueensStepper1],
        wire[recursive.native.actors.macwire.MacWireQueensStepperP],

        wire[recursive.native.futures.macwire.MacWireQueensClassic],
        wire[recursive.native.futures.macwire.MacWireQueensClassicCb],
        wire[recursive.native.futures.macwire.MacWireQueensStepper1],
        wire[recursive.native.futures.macwire.MacWireQueensStepperP],

        wire[recursive.native.kafka.macwire.MacWireQueensClassic],
        wire[recursive.native.kafka.macwire.MacWireQueensClassicCb],
        wire[recursive.native.kafka.macwire.MacWireQueensStepper1],
        wire[recursive.native.kafka.macwire.MacWireQueensStepperP],

        wire[recursive.native.rabbitMQ.macwire.MacWireQueensClassic],
        wire[recursive.native.rabbitMQ.macwire.MacWireQueensClassicCb],
        wire[recursive.native.rabbitMQ.macwire.MacWireQueensStepper1],
        wire[recursive.native.rabbitMQ.macwire.MacWireQueensStepperP],

        wire[recursive.native.awsSQS.macwire.MacWireQueensClassic],
        wire[recursive.native.awsSQS.macwire.MacWireQueensClassicCb],
        wire[recursive.native.awsSQS.macwire.MacWireQueensStepper1],
        wire[recursive.native.awsSQS.macwire.MacWireQueensStepperP],

        wire[recursive.native.flow.macwire.MacWireQueensClassic],
        wire[recursive.native.flow.macwire.MacWireQueensClassicCb],
        wire[recursive.native.flow.macwire.MacWireQueensStepper1],
        wire[recursive.native.flow.macwire.MacWireQueensStepperP],

        wire[recursive.closure.actors.macwire.MacWireQueensClassic],
        wire[recursive.closure.actors.macwire.MacWireQueensClassicCb],

        wire[recursive.closure.futures.macwire.MacWireQueensClassic],
        wire[recursive.closure.futures.macwire.MacWireQueensClassicCb],

        wire[recursive.closure.kafka.macwire.MacWireQueensClassic],
        wire[recursive.closure.kafka.macwire.MacWireQueensClassicCb],

        wire[recursive.closure.rabbitMQ.macwire.MacWireQueensClassic],
        wire[recursive.closure.rabbitMQ.macwire.MacWireQueensClassicCb],

        wire[recursive.closure.awsSQS.macwire.MacWireQueensClassic],
        wire[recursive.closure.awsSQS.macwire.MacWireQueensClassicCb],

        wire[recursive.closure.flow.macwire.MacWireQueensClassic],
        wire[recursive.closure.flow.macwire.MacWireQueensClassicCb],

        wire[recursive.trampoline.actors.macwire.MacWireQueensClassic],
        wire[recursive.trampoline.actors.macwire.MacWireQueensClassicCb],

        wire[recursive.trampoline.futures.macwire.MacWireQueensClassic],
        wire[recursive.trampoline.futures.macwire.MacWireQueensClassicCb],

        wire[recursive.trampoline.kafka.macwire.MacWireQueensClassic],
        wire[recursive.trampoline.kafka.macwire.MacWireQueensClassicCb],

        wire[recursive.trampoline.rabbitMQ.macwire.MacWireQueensClassic],
        wire[recursive.trampoline.rabbitMQ.macwire.MacWireQueensClassicCb],

        wire[recursive.trampoline.awsSQS.macwire.MacWireQueensClassic],
        wire[recursive.trampoline.awsSQS.macwire.MacWireQueensClassicCb],

        wire[recursive.trampoline.flow.macwire.MacWireQueensClassic],
        wire[recursive.trampoline.flow.macwire.MacWireQueensClassicCb],

        wire[recursive.extern.actors.macwire.MacWireQueensClassic],
        wire[recursive.extern.actors.macwire.MacWireQueensClassicCb],
        wire[recursive.extern.actors.macwire.MacWireQueensStepper1],
        wire[recursive.extern.actors.macwire.MacWireQueensStepperP],

        wire[recursive.extern.futures.macwire.MacWireQueensClassic],
        wire[recursive.extern.futures.macwire.MacWireQueensClassicCb],
        wire[recursive.extern.futures.macwire.MacWireQueensStepper1],
        wire[recursive.extern.futures.macwire.MacWireQueensStepperP],

        wire[recursive.extern.kafka.macwire.MacWireQueensClassic],
        wire[recursive.extern.kafka.macwire.MacWireQueensClassicCb],
        wire[recursive.extern.kafka.macwire.MacWireQueensStepper1],
        wire[recursive.extern.kafka.macwire.MacWireQueensStepperP],

        wire[recursive.extern.rabbitMQ.macwire.MacWireQueensClassic],
        wire[recursive.extern.rabbitMQ.macwire.MacWireQueensClassicCb],
        wire[recursive.extern.rabbitMQ.macwire.MacWireQueensStepper1],
        wire[recursive.extern.rabbitMQ.macwire.MacWireQueensStepperP],

        wire[recursive.extern.awsSQS.macwire.MacWireQueensClassic],
        wire[recursive.extern.awsSQS.macwire.MacWireQueensClassicCb],
        wire[recursive.extern.awsSQS.macwire.MacWireQueensStepper1],
        wire[recursive.extern.awsSQS.macwire.MacWireQueensStepperP],

        wire[recursive.extern.flow.macwire.MacWireQueensClassic],
        wire[recursive.extern.flow.macwire.MacWireQueensClassicCb],
        wire[recursive.extern.flow.macwire.MacWireQueensStepper1],
        wire[recursive.extern.flow.macwire.MacWireQueensStepperP],

        wire[recursive.extern.flow.macwire.`0`.x.sync.MacWireQueensClassic],
        wire[recursive.extern.flow.macwire.`0`.x.sync.MacWireQueensClassicCb],
        wire[recursive.extern.flow.macwire.`0`.x.sync.MacWireQueensStepper1],
        wire[recursive.extern.flow.macwire.`0`.x.sync.MacWireQueensStepperP],

        wire[recursive.extern.flow.macwire.`0`.x.async.MacWireQueensClassic],
        wire[recursive.extern.flow.macwire.`0`.x.async.MacWireQueensClassicCb],
        wire[recursive.extern.flow.macwire.`0`.x.async.MacWireQueensStepper1],
        wire[recursive.extern.flow.macwire.`0`.x.async.MacWireQueensStepperP],

        wire[recursive.extern.flow.macwire.`0`.s.MacWireQueensClassic],
        wire[recursive.extern.flow.macwire.`0`.s.MacWireQueensClassicCb],
        wire[recursive.extern.flow.macwire.`0`.s.MacWireQueensStepper1],
        wire[recursive.extern.flow.macwire.`0`.s.MacWireQueensStepperP],

        wire[recursive.continuation.actors.macwire.MacWireQueensClassic],
        wire[recursive.continuation.actors.macwire.MacWireQueensClassicCb],
        wire[recursive.continuation.actors.macwire.MacWireQueensStepper1],
        wire[recursive.continuation.actors.macwire.MacWireQueensStepperP],

        wire[recursive.continuation.futures.macwire.MacWireQueensClassic],
        wire[recursive.continuation.futures.macwire.MacWireQueensClassicCb],
        wire[recursive.continuation.futures.macwire.MacWireQueensStepper1],
        wire[recursive.continuation.futures.macwire.MacWireQueensStepperP],

        wire[recursive.continuation.kafka.macwire.MacWireQueensClassic],
        wire[recursive.continuation.kafka.macwire.MacWireQueensClassicCb],
        wire[recursive.continuation.kafka.macwire.MacWireQueensStepper1],
        wire[recursive.continuation.kafka.macwire.MacWireQueensStepperP],

        wire[recursive.continuation.rabbitMQ.macwire.MacWireQueensClassic],
        wire[recursive.continuation.rabbitMQ.macwire.MacWireQueensClassicCb],
        wire[recursive.continuation.rabbitMQ.macwire.MacWireQueensStepper1],
        wire[recursive.continuation.rabbitMQ.macwire.MacWireQueensStepperP],

        wire[recursive.continuation.awsSQS.macwire.MacWireQueensClassic],
        wire[recursive.continuation.awsSQS.macwire.MacWireQueensClassicCb],
        wire[recursive.continuation.awsSQS.macwire.MacWireQueensStepper1],
        wire[recursive.continuation.awsSQS.macwire.MacWireQueensStepperP],

        wire[recursive.continuation.flow.macwire.MacWireQueensClassic],
        wire[recursive.continuation.flow.macwire.MacWireQueensClassicCb],
        wire[recursive.continuation.flow.macwire.MacWireQueensStepper1],
        wire[recursive.continuation.flow.macwire.MacWireQueensStepperP],

        wire[recursive.continuation.flow.macwire.`0`.x.sync.MacWireQueensClassic],
        wire[recursive.continuation.flow.macwire.`0`.x.sync.MacWireQueensClassicCb],

        wire[recursive.continuation.flow.macwire.`0`.x.async.MacWireQueensClassic],
        wire[recursive.continuation.flow.macwire.`0`.x.async.MacWireQueensClassicCb],

        wire[recursive.continuation.flow.macwire.`0`.s.MacWireQueensClassic],
        wire[recursive.continuation.flow.macwire.`0`.s.MacWireQueensClassicCb],

        wire[recursive.tailcall.actors.macwire.MacWireQueensClassic],
        wire[recursive.tailcall.actors.macwire.MacWireQueensClassicCb],

        wire[recursive.tailcall.futures.macwire.MacWireQueensClassic],
        wire[recursive.tailcall.futures.macwire.MacWireQueensClassicCb],

        wire[recursive.tailcall.kafka.macwire.MacWireQueensClassic],
        wire[recursive.tailcall.kafka.macwire.MacWireQueensClassicCb],

        wire[recursive.tailcall.rabbitMQ.macwire.MacWireQueensClassic],
        wire[recursive.tailcall.rabbitMQ.macwire.MacWireQueensClassicCb],

        wire[recursive.tailcall.awsSQS.macwire.MacWireQueensClassic],
        wire[recursive.tailcall.awsSQS.macwire.MacWireQueensClassicCb],

        wire[recursive.tailcall.flow.macwire.MacWireQueensClassic],
        wire[recursive.tailcall.flow.macwire.MacWireQueensClassicCb],

        wire[recursive.catseval.actors.macwire.MacWireQueensClassic],
        wire[recursive.catseval.actors.macwire.MacWireQueensClassicCb],

        wire[recursive.catseval.futures.macwire.MacWireQueensClassic],
        wire[recursive.catseval.futures.macwire.MacWireQueensClassicCb],

        wire[recursive.catseval.kafka.macwire.MacWireQueensClassic],
        wire[recursive.catseval.kafka.macwire.MacWireQueensClassicCb],

        wire[recursive.catseval.rabbitMQ.macwire.MacWireQueensClassic],
        wire[recursive.catseval.rabbitMQ.macwire.MacWireQueensClassicCb],

        wire[recursive.catseval.awsSQS.macwire.MacWireQueensClassic],
        wire[recursive.catseval.awsSQS.macwire.MacWireQueensClassicCb],

        wire[recursive.catseval.flow.macwire.MacWireQueensClassic],
        wire[recursive.catseval.flow.macwire.MacWireQueensClassicCb],
      )

      def init2(macwireQueens: MacWireQueens*) =
        init3(macwireQueens.toSet)

        def init3(macwireQueens: Set[MacWireQueens]) =
          wrapper = wire[MacWireQueensWrapper]

  }

  private final class CfgByModel(using model: Model) extends ByModel:

    private final class CfgOfAlgorithm(using algorithm: Algorithm) extends OfAlgorithm:

      private final class CfgToAspect(using aspect: Aspect) extends ToAspect:

        final class `Cfg Hatch`[Q <: Queens[?]] extends Hatch[Q]:

          override def apply(): Q =
            require(wrapper ne null)

            val nest = Nest(aspect, algorithm, model)

            wrapper
              .macwireQueens
              .filterNot(_.isInstanceOf[dimensions.three.macwire.`0`.MacWireQueens])
              .find(_.nest == nest)
              .get
              .get
              .asInstanceOf[Q]

        override def breed[Q <: Queens[?]]: Hatch[Q] = new `Cfg Hatch`[Q]

        private final class CfgAtMonad(using monad: Monad) extends AtMonad:

          final class `Cfg Hatch`[Q <: Queens[?]] extends Hatch[Q]:

            override def apply(): Q =
              require(wrapper ne null)

              val nest = Nest(aspect, algorithm, model)

              wrapper
                .macwireQueens
                .filter(_.isInstanceOf[dimensions.three.macwire.`0`.MacWireQueens])
                .map(_.asInstanceOf[dimensions.three.macwire.`0`.MacWireQueens])
                .find { it => it.monad == monad && it.nest == nest }
                .get
                .get
                .asInstanceOf[Q]

          override def breed[Q <: Queens[?]]: Hatch[Q] = new `Cfg Hatch`[Q]

        override def atMonad(using Monad): AtMonad = new CfgAtMonad

      override def toAspect(using Aspect): ToAspect = new CfgToAspect

    override def ofAlgorithm(using Algorithm): OfAlgorithm = new CfgOfAlgorithm

  override def byModel(using Model): ByModel = new CfgByModel


package bis:

  object MacWireConfig
      extends config.bis.Config:

    import config.macwire.bis.MacWireQueensWrapper

    import dimensions.third.bis.Queens

    import dimensions.three.macwire.bis.MacWireQueens

    private var wrapper: MacWireQueensWrapper = null

    def init(): Unit = synchronized {

      init1(
        {
          val plainjava = sys.BooleanProp
            .keyExists(_root_.queens.main.Properties.Validator.plainjava)
            .value
          if plainjava
          then
            wire[Validator2]
          else
            val opt = sys.BooleanProp
              .keyExists(_root_.queens.main.Properties.Validator.optimized)
              .value
            if opt
            then
              wire[ValidatorOpt]
            else
              wire[ValidatorRaw]
        }
      ,
        new CacheProvider:
          override val get: Cache =
            Scaffeine()
              .recordStats()
              .expireAfterWrite(1.hour)
              .maximumSize(100000)
              .build[UUID, LazyList[Solution]]()
      )

      def init1(
        validator: Validator,
        cacheProvider: CacheProvider
      ) =
        init2(
          wire[iterative.actors.macwire.bis.MacWireQueensClassic],
          wire[iterative.actors.macwire.bis.MacWireQueensClassicCb],
          wire[iterative.actors.macwire.bis.MacWireQueensStepper1],
          wire[iterative.actors.macwire.bis.MacWireQueensStepperP],

          wire[iterative.futures.macwire.bis.MacWireQueensClassic],
          wire[iterative.futures.macwire.bis.MacWireQueensClassicCb],
          wire[iterative.futures.macwire.bis.MacWireQueensStepper1],
          wire[iterative.futures.macwire.bis.MacWireQueensStepperP],

          wire[iterative.kafka.macwire.bis.MacWireQueensClassic],
          wire[iterative.kafka.macwire.bis.MacWireQueensClassicCb],
          wire[iterative.kafka.macwire.bis.MacWireQueensStepper1],
          wire[iterative.kafka.macwire.bis.MacWireQueensStepperP],

          wire[iterative.rabbitMQ.macwire.bis.MacWireQueensClassic],
          wire[iterative.rabbitMQ.macwire.bis.MacWireQueensClassicCb],
          wire[iterative.rabbitMQ.macwire.bis.MacWireQueensStepper1],
          wire[iterative.rabbitMQ.macwire.bis.MacWireQueensStepperP],

          wire[iterative.awsSQS.macwire.bis.MacWireQueensClassic],
          wire[iterative.awsSQS.macwire.bis.MacWireQueensClassicCb],
          wire[iterative.awsSQS.macwire.bis.MacWireQueensStepper1],
          wire[iterative.awsSQS.macwire.bis.MacWireQueensStepperP],

          wire[iterative.flow.macwire.bis.MacWireQueensClassic],
          wire[iterative.flow.macwire.bis.MacWireQueensClassicCb],
          wire[iterative.flow.macwire.bis.MacWireQueensStepper1],
          wire[iterative.flow.macwire.bis.MacWireQueensStepperP],

          wire[recursive.native.actors.macwire.bis.MacWireQueensClassic],
          wire[recursive.native.actors.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.native.actors.macwire.bis.MacWireQueensStepper1],
          wire[recursive.native.actors.macwire.bis.MacWireQueensStepperP],

          wire[recursive.native.futures.macwire.bis.MacWireQueensClassic],
          wire[recursive.native.futures.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.native.futures.macwire.bis.MacWireQueensStepper1],
          wire[recursive.native.futures.macwire.bis.MacWireQueensStepperP],

          wire[recursive.native.kafka.macwire.bis.MacWireQueensClassic],
          wire[recursive.native.kafka.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.native.kafka.macwire.bis.MacWireQueensStepper1],
          wire[recursive.native.kafka.macwire.bis.MacWireQueensStepperP],

          wire[recursive.native.rabbitMQ.macwire.bis.MacWireQueensClassic],
          wire[recursive.native.rabbitMQ.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.native.rabbitMQ.macwire.bis.MacWireQueensStepper1],
          wire[recursive.native.rabbitMQ.macwire.bis.MacWireQueensStepperP],

          wire[recursive.native.awsSQS.macwire.bis.MacWireQueensClassic],
          wire[recursive.native.awsSQS.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.native.awsSQS.macwire.bis.MacWireQueensStepper1],
          wire[recursive.native.awsSQS.macwire.bis.MacWireQueensStepperP],

          wire[recursive.native.flow.macwire.bis.MacWireQueensClassic],
          wire[recursive.native.flow.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.native.flow.macwire.bis.MacWireQueensStepper1],
          wire[recursive.native.flow.macwire.bis.MacWireQueensStepperP],

          wire[recursive.closure.actors.macwire.bis.MacWireQueensClassic],
          wire[recursive.closure.actors.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.closure.futures.macwire.bis.MacWireQueensClassic],
          wire[recursive.closure.futures.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.closure.kafka.macwire.bis.MacWireQueensClassic],
          wire[recursive.closure.kafka.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.closure.rabbitMQ.macwire.bis.MacWireQueensClassic],
          wire[recursive.closure.rabbitMQ.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.closure.awsSQS.macwire.bis.MacWireQueensClassic],
          wire[recursive.closure.awsSQS.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.closure.flow.macwire.bis.MacWireQueensClassic],
          wire[recursive.closure.flow.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.trampoline.actors.macwire.bis.MacWireQueensClassic],
          wire[recursive.trampoline.actors.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.trampoline.futures.macwire.bis.MacWireQueensClassic],
          wire[recursive.trampoline.futures.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.trampoline.kafka.macwire.bis.MacWireQueensClassic],
          wire[recursive.trampoline.kafka.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.trampoline.rabbitMQ.macwire.bis.MacWireQueensClassic],
          wire[recursive.trampoline.rabbitMQ.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.trampoline.awsSQS.macwire.bis.MacWireQueensClassic],
          wire[recursive.trampoline.awsSQS.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.trampoline.flow.macwire.bis.MacWireQueensClassic],
          wire[recursive.trampoline.flow.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.extern.actors.macwire.bis.MacWireQueensClassic],
          wire[recursive.extern.actors.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.extern.actors.macwire.bis.MacWireQueensStepper1],
          wire[recursive.extern.actors.macwire.bis.MacWireQueensStepperP],

          wire[recursive.extern.futures.macwire.bis.MacWireQueensClassic],
          wire[recursive.extern.futures.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.extern.futures.macwire.bis.MacWireQueensStepper1],
          wire[recursive.extern.futures.macwire.bis.MacWireQueensStepperP],

          wire[recursive.extern.kafka.macwire.bis.MacWireQueensClassic],
          wire[recursive.extern.kafka.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.extern.kafka.macwire.bis.MacWireQueensStepper1],
          wire[recursive.extern.kafka.macwire.bis.MacWireQueensStepperP],

          wire[recursive.extern.rabbitMQ.macwire.bis.MacWireQueensClassic],
          wire[recursive.extern.rabbitMQ.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.extern.rabbitMQ.macwire.bis.MacWireQueensStepper1],
          wire[recursive.extern.rabbitMQ.macwire.bis.MacWireQueensStepperP],

          wire[recursive.extern.awsSQS.macwire.bis.MacWireQueensClassic],
          wire[recursive.extern.awsSQS.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.extern.awsSQS.macwire.bis.MacWireQueensStepper1],
          wire[recursive.extern.awsSQS.macwire.bis.MacWireQueensStepperP],

          wire[recursive.extern.flow.macwire.bis.MacWireQueensClassic],
          wire[recursive.extern.flow.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.extern.flow.macwire.bis.MacWireQueensStepper1],
          wire[recursive.extern.flow.macwire.bis.MacWireQueensStepperP],

          wire[recursive.continuation.actors.macwire.bis.MacWireQueensClassic],
          wire[recursive.continuation.actors.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.continuation.actors.macwire.bis.MacWireQueensStepper1],
          wire[recursive.continuation.actors.macwire.bis.MacWireQueensStepperP],

          wire[recursive.continuation.futures.macwire.bis.MacWireQueensClassic],
          wire[recursive.continuation.futures.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.continuation.futures.macwire.bis.MacWireQueensStepper1],
          wire[recursive.continuation.futures.macwire.bis.MacWireQueensStepperP],

          wire[recursive.continuation.kafka.macwire.bis.MacWireQueensClassic],
          wire[recursive.continuation.kafka.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.continuation.kafka.macwire.bis.MacWireQueensStepper1],
          wire[recursive.continuation.kafka.macwire.bis.MacWireQueensStepperP],

          wire[recursive.continuation.rabbitMQ.macwire.bis.MacWireQueensClassic],
          wire[recursive.continuation.rabbitMQ.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.continuation.rabbitMQ.macwire.bis.MacWireQueensStepper1],
          wire[recursive.continuation.rabbitMQ.macwire.bis.MacWireQueensStepperP],

          wire[recursive.continuation.awsSQS.macwire.bis.MacWireQueensClassic],
          wire[recursive.continuation.awsSQS.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.continuation.awsSQS.macwire.bis.MacWireQueensStepper1],
          wire[recursive.continuation.awsSQS.macwire.bis.MacWireQueensStepperP],

          wire[recursive.continuation.flow.macwire.bis.MacWireQueensClassic],
          wire[recursive.continuation.flow.macwire.bis.MacWireQueensClassicCb],
          wire[recursive.continuation.flow.macwire.bis.MacWireQueensStepper1],
          wire[recursive.continuation.flow.macwire.bis.MacWireQueensStepperP],

          wire[recursive.tailcall.actors.macwire.bis.MacWireQueensClassic],
          wire[recursive.tailcall.actors.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.tailcall.futures.macwire.bis.MacWireQueensClassic],
          wire[recursive.tailcall.futures.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.tailcall.kafka.macwire.bis.MacWireQueensClassic],
          wire[recursive.tailcall.kafka.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.tailcall.rabbitMQ.macwire.bis.MacWireQueensClassic],
          wire[recursive.tailcall.rabbitMQ.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.tailcall.awsSQS.macwire.bis.MacWireQueensClassic],
          wire[recursive.tailcall.awsSQS.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.tailcall.flow.macwire.bis.MacWireQueensClassic],
          wire[recursive.tailcall.flow.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.catseval.actors.macwire.bis.MacWireQueensClassic],
          wire[recursive.catseval.actors.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.catseval.futures.macwire.bis.MacWireQueensClassic],
          wire[recursive.catseval.futures.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.catseval.kafka.macwire.bis.MacWireQueensClassic],
          wire[recursive.catseval.kafka.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.catseval.rabbitMQ.macwire.bis.MacWireQueensClassic],
          wire[recursive.catseval.rabbitMQ.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.catseval.awsSQS.macwire.bis.MacWireQueensClassic],
          wire[recursive.catseval.awsSQS.macwire.bis.MacWireQueensClassicCb],

          wire[recursive.catseval.flow.macwire.bis.MacWireQueensClassic],
          wire[recursive.catseval.flow.macwire.bis.MacWireQueensClassicCb],
        )

        def init2(macwireQueens: MacWireQueens*) =
          init3(macwireQueens.toSet)

          def init3(macwireQueens: Set[MacWireQueens]) =
            wrapper = wire[MacWireQueensWrapper]

    }

    private final class CfgByModel(using model: Model) extends ByModel:

      private final class CfgOfAlgorithm(using algorithm: Algorithm) extends OfAlgorithm:

        private final class CfgToAspect(using aspect: Aspect) extends ToAspect:

          final class `Cfg Hatch`[Q <: Queens[?]] extends Hatch[Q]:

            override def apply(): Q =
              require(wrapper ne null)

              val nest = Nest(aspect, algorithm, model)

              wrapper
                .macwireQueens
                .find(_.nest == nest)
                .get
                .get
                .asInstanceOf[Q]

          override def breed[Q <: Queens[?]]: Hatch[Q] = new `Cfg Hatch`[Q]

        override def toAspect(using Aspect): ToAspect = new CfgToAspect

      override def ofAlgorithm(using Algorithm): OfAlgorithm = new CfgOfAlgorithm

    override def byModel(using Model): ByModel = new CfgByModel
