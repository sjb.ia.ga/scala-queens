package queens
package dimensions.three


package breed:

  package macwire:

    package object bis:

      import config.bis.Config

      given Config = configurers.bis.MacWireConfig

  package object macwire:

    import config.Config

    given Config = configurers.MacWireConfig
