package queens
package dimensions.three
package macwire

import javax.inject.Provider

import dimensions.third.Queens

import version.less.nest.Nest


abstract trait MacWireQueens
    extends Provider[Queens[?]]:

  val nest: Nest


package `0`:

  import dimensions.Dimension.Monad

  abstract trait MacWireQueens
      extends dimensions.three.macwire.MacWireQueens:

    val monad: Monad


package bis:

  import dimensions.third.bis.{ Queens => Queens2 }

  abstract trait MacWireQueens
      extends Provider[Queens2[?]]:

    val nest: Nest
