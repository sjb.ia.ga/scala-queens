libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_2.13" % "24.6.0",
  "com.softwaremill.macwire" %% "macros" % "2.5.8" % "provided",
  "javax.inject" % "javax.inject" % "1",

  "co.fs2" %% "fs2-core" % "2.5.12",

  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
