package queens
package common
package monad


abstract trait `Monad'`[T, P1 <: AnyVal]
    extends `0`.`Monad'`[Identity_, Iterable_, T, P1]


abstract trait `Monad"`[T, P1 <: AnyVal, P2 <: AnyVal]
    extends `0`.`Monad"`[Identity_, Iterable_, T, P1, P2]


package `0`:

  abstract trait `Monad'`[E[_], F[X[_], _], T, P1 <: AnyVal]:

    protected def `for*`[R](block: Item[T] => R)(using P1): F[E, R] = ???


  abstract trait `Monad"`[E[_], F[X[_], _], T, P1 <: AnyVal, P2 <: AnyVal]:

    protected def `for*`[R](block: Item[T] => R)(using P1, P2): F[E, R] = ???
