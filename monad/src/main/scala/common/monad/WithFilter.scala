package queens
package common
package monad


abstract trait `WithFilter*`[T]:

  protected type * <: `WithFilter*`[T]

  protected val fs: Seq[Item[T] => Boolean]

  import Item.given

  final protected def `apply*`[R](block: Item[T] => R): Item[T] => Option[R] = { it =>
    if fs.foldLeft(true) { (r, f) => r && f(it) }
    then
      Some(block(it))
    else
      None
  }

  final def withFilter(f: Item[T] => Boolean): * =
    this.`apply*`(fs :+ f*)

  protected def `apply*`(s: Item[T] => Boolean*): *
