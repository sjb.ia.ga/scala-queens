package queens
package common
package monad


abstract trait `MonadWithFilter'`[T, P1 <: AnyVal]
    extends `0`.`MonadWithFilter'`[Identity_, Iterable_, T, P1]:

  override protected type * <: `MonadWithFilter'`[T, P1]


abstract trait `MonadWithFilter"`[T, P1 <: AnyVal, P2 <: AnyVal]
    extends `0`.`MonadWithFilter"`[Identity_, Iterable_, T, P1, P2]:

  override protected type * <: `MonadWithFilter"`[T, P1, P2]


package `0`:

  abstract trait `MonadWithFilter'`[E[_], F[X[_], _], T, P1 <: AnyVal]
      extends `Monad'`[E, F, T, P1]
      with `WithFilter*`[T]:

    override protected type * <: `MonadWithFilter'`[E, F, T, P1]


  abstract trait `MonadWithFilter"`[E[_], F[X[_], _], T, P1 <: AnyVal, P2 <: AnyVal]
      extends `Monad"`[E, F, T, P1, P2]
      with `WithFilter*`[T]:

    override protected type * <: `MonadWithFilter"`[E, F, T, P1, P2]
