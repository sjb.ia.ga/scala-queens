package queens
package common
package monad

import java.util.UUID


class Item[T](val the: T, private val key: String, private val value: Option[Any] = None):

  private[monad] val uuid = UUID.randomUUID

  def apply[R](slot: String): Option[Option[R]] =
    if (slot ne null) && (key eq slot)
    then Some(value.asInstanceOf[Option[R]])
    else None


object Item:

  inline given [T]: Conversion[Item[T], T] = _.the
  inline given Conversion[Item[?], Boolean] = _.value.nonEmpty
