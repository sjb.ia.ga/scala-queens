package object queens:

  type Identity_[A] = A

  type Iterable_[X[_], A] = Iterable[A]
