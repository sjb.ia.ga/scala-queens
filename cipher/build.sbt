libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",
  "xyz.wiedenhoeft" % "scalacrypt_2.11" % "0.4.0",

  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
