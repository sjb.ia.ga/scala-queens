package queens
package common
package cipher

import scala.util.{ Failure, Success }

import org.scalatest.flatspec.AnyFlatSpec


class CipherSpec extends AnyFlatSpec {

  "Cipher" should "encrypt/decrypt" in {

    val plainText = java.util.UUID.randomUUID()

    Cipher() match {
      case Failure(t) => assert(false, s"cipher suite: $t")
      case Success(suite) => Cipher(suite, plainText) match {
        case Success(encryptedBytes) => Cipher(suite, encryptedBytes) match {
          case Success(`plainText`) => assert(true)
          case Success(_) => assert(false, s"encryption/decryption")
          case Failure(exception) => assert(false, s"decryption: $exception")
        }
        case Failure(exception) => assert(false, s"encryption: $exception")
      }
    }

  }

  "Cipher 1" should "encrypt/decrypt" in {

    val plainText = java.util.UUID.randomUUID()

    Cipher(0) match {
      case Failure(t) => assert(false, s"cipher suite: $t")
      case Success(suite) => Cipher(suite, plainText) match {
        case Success(encryptedBytes) => Cipher(suite, encryptedBytes) match {
          case Success(`plainText`) => assert(true)
          case Success(_) => assert(false, s"encryption/decryption")
          case Failure(exception) => assert(false, s"decryption: $exception")
        }
        case Failure(exception) => assert(false, s"encryption: $exception")
      }
    }

  }

  "Cipher 2" should "encrypt/decrypt" in {

    val plainText = java.util.UUID.randomUUID()

    Cipher(1) match {
      case Failure(t) => assert(false, s"cipher suite: $t")
      case Success(suite) => Cipher(suite, plainText) match {
        case Success(encryptedBytes) => Cipher(suite, encryptedBytes) match {
          case Success(`plainText`) => assert(true)
          case Success(_) => assert(false, s"encryption/decryption")
          case Failure(exception) => assert(false, s"decryption: $exception")
        }
        case Failure(exception) => assert(false, s"encryption: $exception")
      }
    }

  }

  "Cipher 3" should "encrypt/decrypt" in {

    val plainText = java.util.UUID.randomUUID()

    Cipher(2) match {
      case Failure(t) => assert(false, s"cipher suite: $t")
      case Success(suite) => Cipher(suite, plainText) match {
        case Success(encryptedBytes) => Cipher(suite, encryptedBytes) match {
          case Success(`plainText`) => assert(true)
          case Success(_) => assert(false, s"encryption/decryption")
          case Failure(exception) => assert(false, s"decryption: $exception")
        }
        case Failure(exception) => assert(false, s"encryption: $exception")
      }
    }

  }

  "Cipher 4" should "encrypt/decrypt" in {

    val plainText = java.util.UUID.randomUUID()

    Cipher(3) match {
      case Failure(t) => assert(false, s"cipher suite: $t")
      case Success(suite) => Cipher(suite, plainText) match {
        case Success(encryptedBytes) => Cipher(suite, encryptedBytes) match {
          case Success(`plainText`) => assert(true)
          case Success(_) => assert(false, s"encryption/decryption")
          case Failure(exception) => assert(false, s"decryption: $exception")
        }
        case Failure(exception) => assert(false, s"encryption: $exception")
      }
    }

  }

  "Cipher 5" should "encrypt/decrypt" in {

    val plainText = java.util.UUID.randomUUID()

    Cipher(4) match {
      case Failure(t) => assert(false, s"cipher suite: $t")
      case Success(suite) => Cipher(suite, plainText) match {
        case Success(encryptedBytes) => Cipher(suite, encryptedBytes) match {
          case Success(`plainText`) => assert(true)
          case Success(_) => assert(false, s"encryption/decryption")
          case Failure(exception) => assert(false, s"decryption: $exception")
        }
        case Failure(exception) => assert(false, s"encryption: $exception")
      }
    }

  }
/*
  "Cipher 6" should "encrypt/decrypt" in {

    val plainText = java.util.UUID.randomUUID()

    Cipher(5) match {
      case Failure(t) => assert(false, s"cipher suite: $t")
      case Success(suite) => Cipher(suite, plainText) match {
        case Success(encryptedBytes) => Cipher(suite, encryptedBytes) match {
          case Success(`plainText`) => assert(true)
          case Success(_) => assert(false, s"encryption/decryption")
          case Failure(exception) => assert(false, s"decryption: $exception")
        }
        case Failure(exception) => assert(false, s"encryption: $exception")
      }
    }

  }

  "Cipher 7" should "encrypt/decrypt" in {

    val plainText = java.util.UUID.randomUUID()

    Cipher(6) match {
      case Failure(t) => assert(false, s"cipher suite: $t")
      case Success(suite) => Cipher(suite, plainText) match {
        case Success(encryptedBytes) => Cipher(suite, encryptedBytes) match {
          case Success(`plainText`) => assert(true)
          case Success(_) => assert(false, s"encryption/decryption")
          case Failure(exception) => assert(false, s"decryption: $exception")
        }
        case Failure(exception) => assert(false, s"encryption: $exception")
      }
    }

  }
*/
}
