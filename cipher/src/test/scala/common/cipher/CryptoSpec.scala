package queens
package common
package cipher

import scala.util.Success

import org.scalatest.flatspec.AnyFlatSpec


class CryptoSpec extends AnyFlatSpec {

  "Crypto encrypt" should "apply/unapply" in {

    val key = java.util.UUID.randomUUID()
    val uuid = java.util.UUID.randomUUID()

    Crypto(key, uuid) match {
      case Crypto(`key`, Left(`uuid`)) => assert(true)
      case _ => assert(false)
    }

  }

  "Crypto decrypt" should "apply/unapply" in {

    val key = java.util.UUID.randomUUID()
    val uuid = java.util.UUID.randomUUID()

    Cipher() match {
      case Success(suite) =>
        Cipher(suite, uuid) match {
          case Success(bytes) =>
            Crypto(key, bytes) match {
              case Crypto(`key`, Right(`bytes`)) => assert(true)
              case _ => assert(false)
            }
          case _ => assert(false)
        }
      case _ => assert(false)
    }

  }

}
