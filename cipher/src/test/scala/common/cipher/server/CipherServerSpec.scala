package queens
package common
package cipher
package server

import java.io.{ ObjectInputStream, ObjectOutputStream }
import java.net.{ InetAddress, Socket }

import org.scalatest.flatspec.AnyFlatSpec


class CipherServerSpec extends AnyFlatSpec {

  implicit private final class Sender(out: ObjectOutputStream) {
    def send[T](message: T) = {
      out.writeObject(message)
      out.flush()
    }
  }

  private def apply(block: ((ObjectOutputStream, ObjectInputStream)) => Unit) = {
    val port = 9999

    wrap((new CipherServer(port))())(new Socket(InetAddress.getByName("localhost"), port))(block)
  }


  "CipherServer" should "start/stop" in {
    val port = 9999

    val stop = (new CipherServer(port))()

    stop()
  }


  "CipherServer" should "end" in {

    try {
      this { case (out, in) =>
        out.send(false)
        assert(true)
      }
    } catch {
      case t: Throwable =>
        assert(false, t)
    }

  }

  "CipherServer" should "create session cipher key/suite" in {

    try {
      this { case (out, in) =>
        out.send(true)
        val key = in.readObject().asInstanceOf[String]
        if (key.nonEmpty)
          assert(true)
        else
          assert(false, "cipher suite")
      }
    } catch {
      case t: Throwable =>
        assert(false, t)
    }

  }


  "CipherServer" should "encrypt" in {

    try {
      this { case (out, in) =>
        out.send(true)

        val uuid = in.readObject().asInstanceOf[String]
        if (uuid.isEmpty)
          assert(false, "cipher suite")

        val key = java.util.UUID.fromString(uuid)

        val plainText = java.util.UUID.randomUUID()
        out.send(Crypto(key, plainText))

        val encryptedBytes = in.readObject().asInstanceOf[String]

        if (encryptedBytes.isEmpty)
            assert(false, "encryption")

        assert(true)
      }
    } catch {
      case t: Throwable =>
        assert(false, t)
    }

  }


  "CipherServer" should "encrypt/decrypt" in {

    try {
      this { case (out, in) =>
        out.send(true)

        val uuid = in.readObject().asInstanceOf[String]
        if (uuid.isEmpty)
          assert(false, "cipher suite")

        val key = java.util.UUID.fromString(uuid)

        val plainText = java.util.UUID.randomUUID()
        out.send(Crypto(key, plainText))

        val encryptedBytes = in.readObject().asInstanceOf[String]

        if (encryptedBytes.isEmpty)
            assert(false, "encryption")

        out.send(Crypto(key, Crypto.decode(encryptedBytes)))

        in.readObject().asInstanceOf[String] match {
          case it: String =>
            assert(it == plainText.toString, "decryption")

          case _ =>
            assert(false, s"encryption/decryption")
        }
      }
    } catch {
      case t: Throwable =>
        assert(false, t)
    }

  }

}
