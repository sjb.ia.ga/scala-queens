package queens

import common.cipher.server.CipherServer


object Main extends App {
  val port = 9999

  val server = new CipherServer(port)
  val stop = server()

  try {
    Console.in.read()
  } finally {
    stop()
  }

}
