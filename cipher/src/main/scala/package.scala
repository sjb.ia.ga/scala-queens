package object queens {

  import java.util.UUID

  import scala.util.Try

  type CryptoKey = UUID

  type Encrypto = Try[Seq[Byte]]

  type Decrypto = Try[UUID]

}
