package queens
package common
package cipher

import java.util.UUID

import scala.collection.mutable.ArraySeq

import scala.util.{ Failure, Success, Try }

import xyz.wiedenhoeft.scalacrypt.suites._
import xyz.wiedenhoeft.scalacrypt.{ BlockCipherSuite, Key, Random }


sealed abstract class Cipher[K <: Key] {
  def apply(): Try[BlockCipherSuite[K]]
}

private[cipher] object Cipher {

  import xyz.wiedenhoeft.scalacrypt.{
    SymmetricKey128,
    SymmetricKey256,
    SymmetricKey512,
    SymmetricKey1024
  }

  case object `AES128*`
      extends Cipher[SymmetricKey128] {
    override def apply(): Try[BlockCipherSuite[SymmetricKey128]] =
      AES128_CBC_NoPadding(Key.generate[SymmetricKey128])
  }

  case object `AES256*`
      extends Cipher[SymmetricKey256] {
    override def apply(): Try[BlockCipherSuite[SymmetricKey256]] =
      AES256_CBC_NoPadding(Key.generate[SymmetricKey256])
  }

  case object AES128
      extends Cipher[SymmetricKey128] {
    override def apply(): Try[BlockCipherSuite[SymmetricKey128]] =
      AES128_CBC_PKCS7Padding(Key.generate[SymmetricKey128])
  }

  case object AES256
      extends Cipher[SymmetricKey256] {
    override def apply(): Try[BlockCipherSuite[SymmetricKey256]] =
      AES256_CBC_PKCS7Padding(Key.generate[SymmetricKey256])
  }

  case object Threefish256
      extends Cipher[SymmetricKey256] {
    override def apply(): Try[BlockCipherSuite[SymmetricKey256]] =
      Threefish256_CBC_PKCS7Padding(Key.generate[SymmetricKey256])
  }

  case object Threefish512
      extends Cipher[SymmetricKey512] {
    override def apply(): Try[BlockCipherSuite[SymmetricKey512]] =
      Threefish512_CBC_PKCS7Padding(Key.generate[SymmetricKey512])
  }

  case object Threefish1024
      extends Cipher[SymmetricKey1024] {
    override def apply(): Try[BlockCipherSuite[SymmetricKey1024]] =
      Threefish1024_CBC_PKCS7Padding(Key.generate[SymmetricKey1024])
  }

  private val ciphers: Seq[Cipher[_]] = Seq(
//    `AES128*`,
//    `AES256*`,
    AES128,
    AES256,
    Threefish256,
    Threefish512,
    Threefish1024
  )

  def apply(i: Int): Try[BlockCipherSuite[_]] =
    ciphers(i)()

  def apply(): Try[BlockCipherSuite[_]] = {
    val s: Seq[Byte] = Random.nextBytes(4)

    var r: Int = 0
    for (i <- 0 until 4)
      r += s(i).toInt << i*8

    ciphers(r.abs % ciphers.length)()
  }

  def apply(
    suite: BlockCipherSuite[_],
    uuid: UUID
  ): Encrypto =
    suite.encrypt(uuid.toString.map(_.toByte).toSeq)

  def apply(
    suite: BlockCipherSuite[_],
    uuid: Seq[Byte]
  ): Decrypto =
    suite.decrypt(uuid) match {
      case Success(it) =>
        Success(UUID.fromString(new String(it.toArray)))
      case Failure(t) =>
        Failure(t)
    }

  final class CipherException(msg: String, t: Throwable)
      extends RuntimeException(msg, t)

}
