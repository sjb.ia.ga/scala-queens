package queens
package common

import java.io.{ DataInputStream, DataOutputStream }
import java.io.{ ObjectInputStream, ObjectOutputStream }

import java.net.{ ConnectException, Socket }


package object cipher {

  private val timeout = 1000

  def wrap[R](error : () => Unit = { () => })
             (expr: => Socket)
             (block: ((ObjectOutputStream, ObjectInputStream)) => R): R = {
    var socket: Socket = null
    var out: ObjectOutputStream = null
    var in: ObjectInputStream = null
    try {
      var expired = 0
      while (expired >= 0) {
        try {
          socket = expr
          expired = -1
        } catch {
          case ex: ConnectException =>
            try { Thread.sleep(100) } catch { case _ => }
            if (expired > timeout) { throw ex }
            expired += 100
        }
      }
      out = new ObjectOutputStream(new DataOutputStream(socket.getOutputStream()))
      in = new ObjectInputStream(new DataInputStream(socket.getInputStream()))
      block((out, in))
    } finally {
      if ((in eq null) || (out eq null) || (socket eq null))
        try {
          if (in ne null)
            in.close()
          if (out ne null)
            out.close()
          if (socket ne null)
            socket.close()
        } catch {
          case _ =>
        }
      error()
    }
  }

}
