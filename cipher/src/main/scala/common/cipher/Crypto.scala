package queens
package common
package cipher

import java.util.UUID

import java.util.Base64.{ getEncoder, getDecoder }


private[cipher] object Crypto {

  def encode(bytes: Seq[Byte]): String =
    getEncoder.encodeToString(bytes.toArray)

  def decode(base64: String): Seq[Byte] =
    getDecoder.decode(base64).toSeq

  def apply(key: CryptoKey, uuid: UUID): String =
    ">" + key + uuid

  def apply(key: CryptoKey, uuid: Seq[Byte]): String =
    "<" + key + encode(uuid)

  def unapply(enc: String): Option[(CryptoKey, Either[UUID, Seq[Byte]])] =
    try {
      val key = UUID.fromString(enc.substring(1, 1+36))
      if (enc.charAt(0) == '>') {
        val uuid = UUID.fromString(enc.substring(1+36))
        Some(Tuple2(key, Left(uuid)))
      } else if (enc.charAt(0) == '<') {
        val uuid = decode(enc.substring(1+36))
        Some(Tuple2(key, Right(uuid)))
      } else {
        None
      }
    } catch {
      case _ =>
        None
    }

}
