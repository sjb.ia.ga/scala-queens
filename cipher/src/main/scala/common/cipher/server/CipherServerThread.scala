package queens
package common
package cipher
package server

import java.net.Socket

import scala.util.{ Failure, Success, Try }

import xyz.wiedenhoeft.scalacrypt.BlockCipherSuite


private[server] case class CipherServerThread(socket: Socket)
    extends Thread("CipherServerThread") {

  override def run(): Unit = wrap()(socket) { case (out, in) =>

    while (true) {

      in.readObject().asInstanceOf[Any] match {

        case false =>
          return

        case true =>

          CipherServer() match {
            case Success(uuid) =>
              out.writeObject(uuid.toString)
            case Failure(_) =>
              out.writeObject("")
          }

        case Crypto(key, crypto) =>

          CipherServer(key) match {
            case Success(suite) =>
              crypto match {
                case Left(uuid) =>
                  cipher.Cipher(suite, uuid) match {
                    case Success(bytes) =>
                      out.writeObject(Crypto.encode(bytes))
                    case _ =>
                      out.writeObject("")
                  }
                case Right(bytes) =>
                  cipher.Cipher(suite, bytes) match {
                    case Success(uuid) =>
                      out.writeObject(uuid.toString)
                    case _ =>
                      out.writeObject("")
                  }
              }
            case Failure(t) =>
              out.writeObject("")
          }

        case _ =>
          out.writeObject("")

      }

      out.flush()

    }

  }

}
