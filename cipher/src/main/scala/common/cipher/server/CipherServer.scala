package queens
package common
package cipher
package server

import java.io.{ Console => _, _ }
import java.net.ServerSocket

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }
import java.util.concurrent.locks.{ ReentrantLock => Lock }

import scala.concurrent.{ Await, Future }
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits._

import scala.util.{ Failure, Success, Try }

import xyz.wiedenhoeft.scalacrypt.BlockCipherSuite


/**
 * Simple client/server application using Java sockets.
 */
class CipherServer(port: Int) {

  private var listener: ServerSocket = null
  private val lock = new Lock()

  def apply(): () => Unit = {
    val f: Future[_] = Future {
      var started = false
      try {
        listener = new ServerSocket(port)
        lock.lock()
        started = true
        while (true)
          new CipherServerThread(listener.accept()).start()
      } catch {
        case e: IOException if (!started) =>
          Console.err.println(s"Could not listen on port: $port.")
          throw e
      } finally {
        try {
          listener.close()
        } catch {
          case _ =>
        }
      }
    }
    return { () => this(f) }
  }

  private def apply(f: Future[_]): Unit = {
    while (!lock.isLocked())
      try {
        Thread.sleep(100)
      } catch {
        case _ =>
      }
    try {
      listener.close()
    } catch {
      case _ =>
    } finally {
      try {
        Await.ready(f, Duration.Inf)
      } catch {
        case _ =>
      }
    }
  }

}

private[server] object CipherServer {

  private val session: Map[CryptoKey, Try[BlockCipherSuite[_]]] = new Map()

  def apply(): Try[UUID] =
    try {
      val key = UUID.randomUUID()
      val suite = cipher.Cipher()

      session.put(key, suite)

      suite match {
        case Success(_) => Success(key)
        case Failure(t) => Failure(t)
      }
    } catch {
      case t: Throwable =>
        Failure(t)
    }

  def apply(key: CryptoKey): Try[BlockCipherSuite[_]] =
    try {
      session.get(key) match {
        case null => throw new NoSuchElementException(s"crypto key = $key")
        case it => it
      }
    } catch {
      case t: Throwable =>
        Failure(t)
    }

}
