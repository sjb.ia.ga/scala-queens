# scala-queens

This is a "_toy_" project.


## Wiki

For more details, visit the [wiki](https://gitlab.com/sjb.ia.ga/scala-queens/-/wikis/Scala-Queens) page.

## Name

N-Queens Puzzle in the [Scala](https://www.scala-lang.org) Programming Language

## Description

The board may have "pieces" that occupy cells (at row & column coordinates) and
can make queens *not* threaten each other (horizontally, vertically or diagonally).

## Installation

To build the project, launch [sbt](https://www.scala-sbt.org).

> sbt compile

>sbt:queens> Test / compile

## Usage

For `buffered` output, use either:

>sbt:queens> project plane-fourth-fifth-third-program-monad-output-buffered-actors; run

>sbt:queens> project plane-fourth-fifth-third-program-monad-output-buffered-flow; run

>sbt:queens> project plane-fourth-fifth-third-program-monad-output-buffered-concurrent; run

For `console` output, use either:

>sbt:queens> project plane-fourth-fifth-third-program-monad-output-console-actors; run

>sbt:queens> project plane-fourth-fifth-third-program-monad-output-console-concurrent; run

>sbt:queens> project plane-fourth-fifth-third-program-monad-output-console-flow; run

>sbt:queens> project plane-fourth-fifth-third-program-monad-output-console-kafka; run

>sbt:queens> project plane-fourth-fifth-third-program-monad-output-console-rabbitMQ; run

For "`io`" (console) output (`FS2` `Stream` or `Monix` `Iterant`), use:

>sbt:queens> project plane-fourth-fifth-third-program-monad-output-io-flow; run

The `AWS SQS` client uses [elasticmq](https://github.com/softwaremill/elasticmq#elasticmq-via-docker),
so make sure its `Docker` container is running:

>~/queens $ docker run -p 9324:9324 -p 9325:9325 softwaremill/elasticmq-native

then:

>sbt:queens> project plane-fourth-fifth-third-program-monad-output-console-awsSQS; run

For `database` store & query with `Scalikejdbc`, use:

>sbt:queens> project plane-fourth-fifth-third-program-monad-output-database-scalikejdbc-flow; run

For `database` store & query with `Slick`, use:

>sbt:queens> project plane-fourth-fifth-third-program-monad-output-database-slick-jdbc-flow; run

For protocol "Main" programs, use the `cipher` server in a different terminal:

>sbt:queens> project cipher; run

---

For `web` servers, use:

>sbt:queens> project plane-fifth-fourth-web-simple; run

except for `Dropwizard`, use:

>sbt:queens> project plane-fifth-fourth-web-simple; run server dropwizard.yml

and for `JavaEE`, use:

>sbt:queens> project plane-fifth-fourth-web-simple; Tomcat / start

(NB: For `JavaEE`, replace path separator for semicolon in `URL`s.)

--

For `web` clients, use:

>sbt:queens> project plane-fourth-fifth-webapp-console; run

To use a specific port - `8058` for `SpringBoot` web server running -, use:

>sbt:queens> project plane-fourth-fifth-webapp-console; run localhost:8058

or - to specify `JSON` as media and `6` the board size -, use:

>sbt:queens> project plane-fourth-fifth-webapp-console; run 6 localhost:8058 application/json

Not all clients support all three media types: `application/json`, `application/xml`, `text/plain`.

NB: If the "`solve`" server supports it, add a (must) fourth "`infra`" path segment:

>sbt:queens> project plane-fourth-fifth-webapp-console; run 6 localhost:8358 application/json x-async


---

For [reactive] "`io`" `SpringBoot` web server - port 8358 - using `Monix` `Iterant`, use:

>sbt:queens> project plane-fifth-fourth-web-io; run

and command-line client ("async" / `Task`):

>curl -X GET -L 'http://localhost:8358/queens/solve/x-async/model/flow;classicStraight=1;recursiveContinuation=1;?size=6&max=10' -H 'Accept: application/json'

or ("sync" / `Coeval`):

>curl -X GET -L 'http://localhost:8358/queens/solve/x-sync/model/flow;classicCallback=1;recursiveExtern=1;?size=6&max=10' -H 'Accept: application/xml'

For "`io`" `SpringBoot` web server - port 8358 - using `FS2` `Stream`, use:

>sbt:queens> project plane-fifth-fourth-web-io; run

and command-line client:

>curl -X GET -L 'http://localhost:8358/queens/solve/s/model/flow;classicStraight=1;recursiveContinuation=1;?size=6&max=10' -H 'Accept: application/json'

For "`io`" `http4s` web server - port 8045 - using `FS2` `Stream`, use:

>sbt:queens> project plane-fifth-fourth-web-io; run

and command-line client:

>curl -X GET -L 'http://localhost:8045/queens/solve/model/flow;classicStraight=1;recursiveContinuation=1;?size=6&max=10' -H 'Accept: text/plain'

---

For reactive `web` servers (with `cue`s), use:

>sbt:queens> project plane-fifth-fourth-web-cue; run

For reactive `web` clients (with `cue`s), use:

>sbt:queens> project plane-fourth-fifth-webapp-console-cue; run

The "`cue`" implementation is based on the added resume/suspend support
of iterations (saving/loading state in a file): it "records" the times solving
would preempt itself between successive solutions, and "replays" the cues by
solving (faster) without the already counted preemptions. The same cues may work
with different `aspect`s and/or `algorithm`s (except, of course, the `JVM native`
implementation, and, for instance, the stepper-permutation).

How cues are recorded/replayed is called "cycling". Thus, there are three
ways of solving:

1. `monad` - the first `Main` programs (as a `foreach`/`map`/`flatMap` loop)

2. `simple` - the "second" web servers (from a `HTTP` request/response method)

3. `cycle` - together with the "third wave" of processors, that upon a solution,
             publish it then suspend the state in a file, and, subscribed to a
             `cue` publisher, resume from that file.

The suspend/resume support allows for reactive programming, either the
`org.reactivestreams.*` or `java.util.concurrent.Flow.*` publisher/subscribe
interfaces: though unrelated, they are inherited together (coinciding
at `Subscription`) such that the client code may use either one.

For tests with `Reactor`, `RxJava` or `Akka` streams, use:

>sbt:queens> project plane-fifth-fourth-publish-subscribe; test

>sbt:queens> project plane-fifth-fourth-publish-subscribe; testOnly `*ReactorCueProcessorReplay*Flow*`

### Docker-ization

The `main` programs for `rabbitMQ` & `awsSQS` models can be used without `sbt`, via `docker-compose`
(a classic-native [4x4] empty board):

>~/queens $ pushd plane/fourth/fifth/third/program/monad/output/console/rabbitMQ
>~/queens/plane/fourth/fifth/third/program/monad/output/console/rabbitMQ $ docker-compose up

---

>~/queens $ pushd plane/fourth/fifth/third/program/monad/output/console/awsSQS
>~/queens/plane/fourth/fifth/third/program/monad/output/console/awsSQS $ docker-compose up


## Report

>find ! -ipath '*./.*' -iname '*.scala' | wc -l

Sources counters:

- Scala: #1912

- Java: #140

>find ! -ipath '*./.*' -iname '*.scala' -exec grep 'class\W' '{}' ';' | grep -v '^[ ]*[/*]' | tr ' ' '\n' | grep '^class$' | wc -l

Scala counters:

- `val`s: #7137

- `def`s: #4740

- `class`es: #1972

- `object`s: #1878

- `trait`s: #1201

Java counters:

- `class`es: #103

- `interface`s: #44


## Roadmap

- Start: August, 2021
- Monad & Console + Database Output & Akka Plane Protocol: March, 2022
- Restart: December, 2022
- Redesign monads w/ pipeline: February, 2023
- Web servers & clients - `Spring MVC`, `Micronaut`, `Dropwizard`, `Vert.x` (reactive): March, 2023
- "Simulation" (Preemption / Suspension): February-March, 2023
- Resume/Suspend: April, 2023
- Reactive / `Spring WebFlux`: March-May, 2023
- `JavaEE` Web servlet: May, 2023
- `Guice` for `Scala` 3: May, 2023
- `Kafka` model: May, July, 2023
- `RabbitMQ` model: July, 2023
- `sbt-docker` plugin: July, 2023
- `AWSSQS` model ([`elasticmq`](https://github.com/softwaremill/elasticmq#using-the-amazon-java-sdk-to-access-an-elasticmq-server) endpoint): July, 2023
- `tailrec` & `catseval` & `continuation` algorithms: September, 2023
- `recursive` `continuation` algorithm - resume/suspend (classic & classic w/ callbacks): September, 2023
- 0th dimension & [`monix`](https://monix.io) [`Iterant`](https://monix.io/api/current/monix/tail/Iterant.html) monad: September-October, 2023
- `monix` `Task` & Web Server `Spring MVC WebFlux`: October, 2023
- `monix` `Coeval` 0th monad & Web Server `Spring MVC`: October, 2023
- [`fs2`](https://fs2.io) [`Stream`](https://s01.oss.sonatype.org/service/local/repositories/releases/archive/co/fs2/fs2-core_2.13/2.5.11/fs2-core_2.13-2.5.11-javadoc.jar/!/fs2/Stream.html) 0th monad & Web Server `Spring MVC WebFlux`: October, 2023
- `recursive` `closure` algorithm (classic & classic w/ callbacks): October, 2023
- `recursive` `trampoline` algorithm (classic & classic w/ callbacks): October, 2023
- [`http4s`](https://http4s.org) server & client: November, 2023
- [`Akka Http`](https://akka.io) server (_not_ streaming): November, 2023


## License

MIT

## Project status

Beta
