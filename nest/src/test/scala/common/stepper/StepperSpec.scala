package queens
package common
package stepper

import org.scalatest.flatspec.AnyFlatSpec

import common.stepper.LtdStepperForwardLenient
import common.stepper.LtdStepper
import common.stepper.LtdStepper._


class StepperSpec extends AnyFlatSpec {

  case class LtdStepperTest(N: Int)
      extends LtdStepper[Int]:

    override type S = LtdStepperTest

    override protected val pos = Integer2(0)

    protected val MIN: Int = 0

    protected val MAX: Int = N - 1

    override def current = pos.get

  case class LtdStepperFwdLenTest(N: Int)
      extends LtdStepperForwardLenient[Int]:

    override type S = LtdStepperFwdLenTest

    override protected val pos = Integer2(0)

    override protected val lenient = Boolean2(true)

    protected val MIN: Int = 0

    protected val MAX: Int = N - 1

    override def current = if isLenient then pos.get else N

  "A LtdStepper" should "throw ForwardLimitReached" in {

    val s = LtdStepperTest(1)

    intercept[ForwardLimitReached.type] {
      s.forward
    }

  }

  "A LtdStepper" should "throw BackwardLimitReached" in {

    val s = LtdStepperTest(1)

    intercept[BackwardLimitReached.type] {
      s.backward
    }

  }

  "A forward lenient LtdStepper" should "throw ForwardLimitReached with lenience" in {

    val s = LtdStepperFwdLenTest(1)

    s.forward

    intercept[ForwardLimitReached.type] {
      s.forward
    }

  }

}
