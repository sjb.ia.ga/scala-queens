package queens
package common
package stepper

import scala.collection.mutable.{ ListBuffer => MutableList }

import org.scalatest.flatspec.AnyFlatSpec

import common.stepper.StepperP


class StepperPSpec extends AnyFlatSpec {

  given Option[Long] = None

  "A StepperP" should "on forward position integrally" in {

    val N = 6

    val r = MutableList[Int]()

    val s = StepperP(N)

    for (i <- 0 until N - 1) {
      r += s.current
      s.forward
    }
    r += s.current

    assert(r.sorted == (0 until N))

  }

  "A StepperP" should "on backward position integrally" in {

    val N = 6

    val r = MutableList[Int]()

    val s = StepperP(N)

    for (i <- 0 until N - 1)
      s.forward

    for (i <- -(N - 1) to -1) {
      r += s.current
      s.backward
    }
    r += s.current

    assert(r.sorted == (0 until N))

  }

}
