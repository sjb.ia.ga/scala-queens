package queens
package common
package stepper

import org.scalatest.flatspec.AnyFlatSpec

import common.stepper.Stepper1


class Stepper1Spec extends AnyFlatSpec {

  "A Stepper1" should "on forward position consecutively" in {

    val N = 6

    val s = Stepper1(N)

    for (i <- 0 until N - 1) {
      assert(s.current == i)
      s.forward
    }
    assert(s.current == N - 1)

  }

  "A Stepper1" should "on backward position consecutively" in {

    val N = 6

    val s = Stepper1(N)

    for (i <- 0 until N - 1)
      s.forward

    for (i <- -(N - 1) to -1) {
      s.backward
      assert(s.current == -i-1)
    }

  }

}
