package queens
package common

import java.io.{ FileInputStream, ObjectInputStream, FileOutputStream, ObjectOutputStream }
import java.nio.file.{ Files, Path }

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.BeforeAndAfterAll

import base.{ Board, EmptyBoard }

import common.bis.Cycle
import Cycle.apply

import common.Macros.tryc

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect.Classic
import Classic.Straight
import Algorithm.Recursive
import Recursive.Extern
import Model.Flow

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


class CycleSerializationSpec extends AnyFlatSpec with BeforeAndAfterAll {

  var tmp: Path = null

  override def beforeAll(): Unit =
    tmp = Files.createTempDirectory("cycle-spec-")

  override def afterAll(): Unit =
    tryc(Files.delete(tmp))


  given Board = new EmptyBoard(4)

  given Long = Long.MaxValue


  "Cycle serialize once" should "deserialize once" in {

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        case (Algorithm, Recursive(Extern)) => true
        case (Model, Flow) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case _ => 1
    }

    val exp = wildcard(multiplier)

    val out = Cycle(wildcard, multiplier)

    val bin = out(tmp, "test-1-")

    try
      val (in, Some(_)) = exp(bin)

      assert(out == in && (in.turn eq null))

    finally
      tryc(Files.delete(bin))

  }


  "Cycle serialize twice" should "deserialize twice" in {

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        case (Algorithm, Recursive(Extern)) => true
        case (Model, Flow) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case _ => 2
    }

    val exp = wildcard(multiplier)

    val out = Cycle(wildcard, multiplier)

    val bin = out(tmp, "test-2-")

    try
      val (in, Some(_)) = exp(bin)

      assert(out == in && (in.turn ne null))

      val out2 = in.turn
      val bin2 = out2(tmp, "test-3-")

      try
        val (in2, Some(_)) = exp(bin2)

        assert(out2 == in2 && (in2.turn eq null))

      finally
        tryc(Files.delete(bin2))

    finally
      tryc(Files.delete(bin))

  }


}
