package queens

import org.scalatest.flatspec.AnyFlatSpec

import base.{ Board, EmptyBoard }
import di.ValidatorRaw
import common.geom.x


class ValidatorRawSpec extends AnyFlatSpec {

  "A raw 3x3 board validator" should "NOT verify queens on empty row" in {

    val X = true
    val O, Q = false

    given Board = new Board(
      List(
        List(Q, O, Q),
        List(O, O, O),
        List(O, Q, O),
      )
    )

    val solution: Solution = List(0 x 0, 0 x 2, 2 x 1)

    !ValidatorRaw()(solution)

  }


  "A raw 3x3 board validator" should "NOT verify queens on empty column" in {

    val X = true
    val O, Q = false

    given Board = new Board(
      List(
        List(Q, O, O),
        List(O, O, Q),
        List(Q, O, O),
      )
    )

    val solution: Solution = List(0 x 0, 2 x 0, 1 x 2)

    !ValidatorRaw()(solution)

  }


  "A raw 3x3 board validator" should "NOT verify queens on empty main diagonal" in {

    val X = true
    val O, Q = false

    given Board = new Board(
      List(
        List(Q, O, O),
        List(X, O, O),
        List(Q, X, Q),
      )
    )

    val solution: Solution = List(0 x 0, 2 x 2, 2 x 0)

    !ValidatorRaw()(solution)

  }


  "A raw 4x4 board validator" should "verify queens on non-empty diagonal" in {

    val X = true
    val O, Q = false

    given Board = new Board(
      List(
        List(Q, X, O, Q),
        List(X, X, X, O),
        List(O, X, Q, O),
        List(Q, O, O, O)
      )
    )

    val solution: Solution = List(0 x 0, 0 x 3, 3 x 0, 3 x 2)

    ValidatorRaw()(solution)

  }

}
