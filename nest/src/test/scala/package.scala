package object queens {

  import java.util.UUID

  import base.Board

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]


  type Simulate = dimensions.fifth.cycle.Simulate

  type IO = dimensions.fifth.cycle.IO


  import java.io.{ ObjectInputStream, ObjectOutputStream }

  type Load = ObjectInputStream
  type Save = Option[ObjectOutputStream] => Boolean


  // ScalaTest

}
