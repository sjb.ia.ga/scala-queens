package queens
package dimensions.second
package recursive
package native

import org.scalatest.flatspec.AnyFlatSpec

import base.{ Board, EmptyBoard }

import common.geom.Coord

import common.stepper.{ Stepper1 => Stepper1Type }
import common.stepper.BoardStepperFactory

import dimensions.second.first.BoardStepper.start
import common.stepper.factory.given


class RecursiveNativeStepper1Spec extends AnyFlatSpec {

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  given Option[Long] = None


  sealed abstract class TestThrowable(val t: Throwable)

  final class TestMaxSolutionsReached(t: Throwable)
      extends Throwable(t)

  final class TestAlgorithmInterrupted(t: Throwable)
      extends Throwable(t)

  final class TestAlgorithmError(t: Throwable)
      extends Throwable(t)


  class Stepper1Test(using
    override protected val f: BoardStepperFactory[Stepper1Type]
  ) extends Stepper1:

    override val validate: Validator = Validator()

    override def solve(using Long, Either[Load, At])
                      (using Board)
                      (using Either[Save, Solution] => Boolean): Option[Boolean] =
      try
        queens
        Some(true)
      catch
        case MaxSolutionsReached =>
          throw TestMaxSolutionsReached(MaxSolutionsReached)
        case AlgorithmInterrupted =>
          throw TestAlgorithmInterrupted(AlgorithmInterrupted)
        case AlgorithmError(it) =>
          throw TestAlgorithmError(it)


  "Recursive native algorithm stepper1 interrupted" should "throw AlgorithmInterrupted" in {

    given Board = new EmptyBoard(6)

    val `13` = new Stepper1Test

    given Long = 1

    given Either[Load, Coord[Stepper1Type]] = Right(start[Stepper1Type](given_Board.N))

    given (Either[Save, Solution] => Boolean) = { _ => false }

    try
      `13`.solve
      assert(false)
    catch
      case _: TestAlgorithmInterrupted =>
        assert(true)
      case _ =>
        assert(false)
  }


  "Recursive native algorithm stepper1 2x2" should "solve 0 solutions" in {

    given Board = new EmptyBoard(2)

    val `13` = new Stepper1Test

    given Long = Long.MaxValue

    given Either[Load, Coord[Stepper1Type]] = Right(start[Stepper1Type](given_Board.N))

    var count = 0

    given (Either[Save, Solution] => Boolean) = { it => it.foreach(_=>count+=1); true }

    try
      `13`.solve
      assert(count == 0)
    catch
      case _ =>
        assert(false)
  }


  "Recursive native algorithm stepper1 4x4" should "solve 2 solutions" in {

    given Board = new EmptyBoard(4)

    val `13` = new Stepper1Test

    given Long = Long.MaxValue

    given Either[Load, Coord[Stepper1Type]] = Right(start[Stepper1Type](given_Board.N))

    var count = 0

    given (Either[Save, Solution] => Boolean) = { it => it.foreach(_=>count+=1); true }

    try
      `13`.solve
      assert(count == 2)
    catch
      case _ =>
        assert(false)
  }


  "Recursive native algorithm stepper1 6x6 empty board" should "solve 4 solutions" in {

    given Board = new EmptyBoard(6)

    val `13` = new Stepper1Test

    given Long = Long.MaxValue

    given Either[Load, Coord[Stepper1Type]] = Right(start[Stepper1Type](given_Board.N))

    var count = 0

    given (Either[Save, Solution] => Boolean) = { it => it.foreach(_=>count+=1); true }

    try
      `13`.solve
      assert(count == 4)
    catch
      case _ =>
        assert(false)
  }


  "Recursive native algorithm stepper1 6x6 selected for 1 solution" should "solve 1 solution" in {

    val o = false
    val x = true

    given Board = new Board(List(
      List(x, o, x, x, x, x),
      List(x, x, x, o, x, x),
      List(x, x, x, x, x, o),
      List(o, x, x, x, x, x),
      List(x, x, o, x, x, x),
      List(x, x, x, x, o, x)
    ))

    val `13` = new Stepper1Test

    given Long = Long.MaxValue

    given Either[Load, Coord[Stepper1Type]] = Right(start[Stepper1Type](given_Board.N))

    var count = 0

    given (Either[Save, Solution] => Boolean) = { it => it.foreach(_=>count+=1); true }

    try
      `13`.solve
      assert(count == 1)
    catch
      case _ =>
        assert(false)
  }


/*
  "Recursive native algorithm stepper1 8x8 empty board" should "solve 92 solutions" in {

    given Board = new EmptyBoard(8)

    val `13` = new Stepper1Test

    given Long = Long.MaxValue

    given Either[Load, Coord[Stepper1Type]] = Right(start[Stepper1Type](given_Board.N))

    var count = 0

    given (Either[Save, Solution] => Boolean) = { it => it.foreach(_=>count+=1); true }

    try
      `13`.solve
      assert(count == 92)
    catch
      case _ =>
        assert(false)
  }
*/

}
