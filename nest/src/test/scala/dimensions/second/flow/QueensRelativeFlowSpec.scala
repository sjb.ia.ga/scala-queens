package queens
package dimensions.second
package iterative

import scala.util.control.NonLocalReturns.{ returning, throwReturn => thr }

import org.scalatest.flatspec.AnyFlatSpec

import base.{ Board, EmptyBoard }

import common.NoTag

import dimensions.Dimension.Aspect._
import dimensions.Dimension.Algorithm._
import dimensions.Dimension.Model.flow

import dimensions.third.first.flow.`Queens*`

import dimensions.three.breed.given

import common.Flag
import dimensions.third.first.flow.Iteration


class QueensRelativeFlowSpec extends AnyFlatSpec {

  sys.BooleanProp
    .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
    .enable()

  def compareSolutions(a: Seq[Solution], b: Seq[Solution]): Boolean = returning {
    for (it <- a) {
      if !b.exists(_.sorted == it.sorted) then
        thr(false)
    }
    for (it <- b) {
      if !a.exists(_.sorted == it.sorted) then
        thr(false)
    }
    true
  }


  def getResults(using board: Board): List[Seq[Solution]] =

    given Iteration = Iteration(board, NoTag, false, Right(false), Flag(), { () => })

    given Long = Long.MaxValue

    List(

      {
        val hatch = given_Builder
          .byModel(flow)
          .withAlgorithm(iterative)
          .andAspect(classic)
          .breed[`Queens*`[?]]

        val queens = hatch()

        (
          for
            it <- queens
            solution: Solution = it
          yield
            solution
        ).toSeq
      },

      {
        val hatch = given_Builder
          .byModel(flow)
          .withAlgorithm(iterative)
          .andAspect(classicCb)
          .breed[`Queens*`[?]]

        val queens = hatch()

        (
          for
            it <- queens
            solution: Solution = it
          yield
            solution
        ).toSeq
      },

      {
        val hatch = given_Builder
          .byModel(flow)
          .withAlgorithm(iterative)
          .andAspect(stepper1)
          .breed[`Queens*`[?]]

        val queens = hatch()

        (
          for
            it <- queens
            solution: Solution = it
          yield
            solution
        ).toSeq
      },

      {
        val hatch = given_Builder
          .byModel(flow)
          .withAlgorithm(iterative)
          .andAspect(stepperP)
          .breed[`Queens*`[?]]

        val queens = hatch()

        (
          for
            it <- queens
            solution: Solution = it
          yield
            solution
        ).toSeq
      },

      {
        val hatch = given_Builder
          .byModel(flow)
          .withAlgorithm(extern)
          .andAspect(classic)
          .breed[`Queens*`[?]]

        val queens = hatch()

        (
          for
            it <- queens
            solution: Solution = it
          yield
            solution
        ).toSeq
      },

      {
        val hatch = given_Builder
          .byModel(flow)
          .withAlgorithm(extern)
          .andAspect(classicCb)
          .breed[`Queens*`[?]]

        val queens = hatch()

        (
          for
            it <- queens
            solution: Solution = it
          yield
            solution
        ).toSeq
      },

      {
        val hatch = given_Builder
          .byModel(flow)
          .withAlgorithm(extern)
          .andAspect(stepper1)
          .breed[`Queens*`[?]]

        val queens = hatch()

        (
          for
            it <- queens
            solution: Solution = it
          yield
            solution
        ).toSeq
      },

      {
        val hatch = given_Builder
          .byModel(flow)
          .withAlgorithm(extern)
          .andAspect(stepperP)
          .breed[`Queens*`[?]]

        val queens = hatch()

        (
          for
            it <- queens
            solution: Solution = it
          yield
            solution
        ).toSeq
      },

      {
        val hatch = given_Builder
          .byModel(flow)
          .withAlgorithm(native)
          .andAspect(classic)
          .breed[`Queens*`[?]]

        val queens = hatch()

        (
          for
            it <- queens
            solution: Solution = it
          yield
            solution
        ).toSeq
      },

      {
        val hatch = given_Builder
          .byModel(flow)
          .withAlgorithm(native)
          .andAspect(classicCb)
          .breed[`Queens*`[?]]

        val queens = hatch()

        (
          for
            it <- queens
            solution: Solution = it
          yield
            solution
        ).toSeq
      },

      {
        val hatch = given_Builder
          .byModel(flow)
          .withAlgorithm(native)
          .andAspect(stepper1)
          .breed[`Queens*`[?]]

        val queens = hatch()

        (
          for
            it <- queens
            solution: Solution = it
          yield
            solution
        ).toSeq
      },

      {
        val hatch = given_Builder
          .byModel(flow)
          .withAlgorithm(native)
          .andAspect(stepperP)
          .breed[`Queens*`[?]]

        val queens = hatch()

        (
          for
            it <- queens
            solution: Solution = it
          yield
            solution
        ).toSeq
      },

    )


  "All Queens by Flow w/ empty 4x4 board" should "have identical solutions" in {

    given Board = new EmptyBoard(4)

    val results = getResults

    assert(results.size == 4 * 3)

    for
      i <- 0 until results.size
      j <- i + 1 until results.size
    do
      assert(results(i).size == results(j).size)
      assert(compareSolutions(results(i), results(j)))

  }


/*
  "All Queens by Flow w/ empty 6x6 board" should "have identical solutions" in {

    given Board = new EmptyBoard(6)

    val results = getResults

    assert(results.size == 4 * 3)

    for
      i <- 0 until results.size
      j <- i + 1 until results.size
    do
      assert(results(i).size == results(j).size)
      assert(compareSolutions(results(i), results(j)))

  }
*/

}
