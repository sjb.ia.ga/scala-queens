package queens
package dimensions.three
package recursive
package extern
package actors

import java.nio.file.{ Files, Path }

import akka.actor.typed.{ ActorRef, Behavior }
import akka.actor.typed.scaladsl.Behaviors

import akka.actor.testkit.typed.scaladsl.ScalaTestWithActorTestKit

import org.scalatest.wordspec.AnyWordSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.BeforeAndAfterAll

import base.{ Board, EmptyBoard }

import common.bis.Cycle
import Cycle.apply

import common.Macros.tryc

import common.NoTag

import common.geom.x

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Classic._
import Stepper._
import Algorithm.Recursive
import Recursive.Extern
import Model.Parallel
import Parallel.Actors

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import common.monad.Item.given

import common.Flag


class QueensAllRecursiveExternActorsCycleSpec
    extends ScalaTestWithActorTestKit
    with AnyWordSpecLike
    with BeforeAndAfterAll {

  import QueensAllRecursiveExternActorsCycleSpec._

  var tmp: Path = null

  override def beforeAll(): Unit =
    tmp = Files.createTempDirectory("queens-all-recursive-extern-actors-cycle-spec-")

  override def afterAll(): Unit =
    tryc(Files.delete(tmp))

  extension(a: Seq[Solution])
    def ===(b: Seq[Solution]): Boolean =
      a.map(_.sorted).sorted == b.map(_.sorted).sorted


  "Cycle once all 4*2 expansions actors Queens bis" should {
    "suspend/resume 4*2*2 solutions" in {

      import conf.actors.bis.Callbacks

      import dimensions.third.first.actors.bis.Queens

      import dimensions.three.breed.bis.given
      import dimensions.third.first.Iteration1

      import dimensions.fifth.cycle.Preemption

      val wildcard = Wildcard (
        (_, _) match
          case (Aspect, Classic(Straight)) => true
          case (Aspect, Classic(Callback)) => true
          case (Aspect, Stepper(OneIncrement)) => true
          case (Aspect, Stepper(Permutations)) => true
          case (Algorithm, Recursive(Extern)) => true
          case (Model, Parallel(Actors)) => true
          case _ => false
      )

      val multiplier = Multiplier[Nest] {
        case _ => 1
      }

      val exp = wildcard(multiplier)

      given Board = new EmptyBoard(4)

      given Long = Long.MaxValue

      var result: Seq[Solution] = Nil

      val `13`: Behavior[Test] = Behaviors
        .receive { case (ctx, Test(ref)) =>

          var bin: Path = null

          try

            def reset(cycle: Cycle) =
              if bin ne null then tryc(Files.delete(bin))
              bin = cycle(tmp, "test-2-")

            reset(Cycle(wildcard, multiplier))

            var done = false

            while !done
            do

              val (cycle, Some(nest)) = exp(bin)

              val hatch = given_Builder
                .byModel(nest.model)
                .withAlgorithm(nest.algorithm)
                .andAspect(nest.aspect)
                .breed[Queens[?]]

              val queens = hatch()

              val cbs = Callbacks()

              given Iteration1 = Iteration1(cycle, NoTag, Flag(), { () => }, ctx -> cbs)

              given Simulate = Left(Preemption.step())

              queens { it ?=>
                result = result :+ it
              }

              cbs.spindle.decrementAndGet()
              cbs()

              if cycle.image.isEmpty
              then
                if cycle.turn eq null
                then
                  done = true
                else
                  reset(cycle.turn)

          finally
            if bin ne null then tryc(Files.delete(bin))

          ref ! Stop

          Behaviors.stopped
        }

      val actor = spawn(`13`, "Queens-Test")
      val stop = createTestProbe[Stop.type]()

      actor ! Test(stop.ref)

      stop.expectMessage(Stop)

      val solutions: List[Solution] = List.fill(exp.size)(
        List(
          List(3 x 1, 2 x 3, 1 x 0, 0 x 2),
          List(3 x 2, 2 x 0, 1 x 3, 0 x 1),
        )
      ).reduce(_:::_)

      assert(solutions === result)

    }
  }

}


object QueensAllRecursiveExternActorsCycleSpec:

  case class Test(response: ActorRef[Stop.type])
  case object Stop
