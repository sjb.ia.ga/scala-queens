package queens
package dimensions.three
package recursive
package extern
package flow

import java.nio.file.{ Files, Path }

import scala.Function.const

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.BeforeAndAfterAll

import base.{ Board, EmptyBoard }

import common.bis.Cycle
import Cycle.apply

import common.Macros.tryc

import common.NoTag

import common.geom.x

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Classic._
import Stepper._
import Algorithm._
import Recursive._
import Model.Flow

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import common.monad.Item.given

import common.Flag


class QueensAllRecursiveExternFlowCycleSpec extends AnyFlatSpec with BeforeAndAfterAll {

  var tmp: Path = null

  override def beforeAll(): Unit =
    tmp = Files.createTempDirectory("queens-recursive-extern-flow-cycle-spec-")

  override def afterAll(): Unit =
    tryc(Files.delete(tmp))

  extension(a: Seq[Solution])
    def ===(b: Seq[Solution]): Boolean =
      a.map(_.sorted).sorted == b.map(_.sorted).sorted


  "Cycle once all 4*2 expansions recursive extern flow Queens" should "suspend/resume 4*2*2 solutions" in {

    import dimensions.third.first.flow.`Queens*`

    import dimensions.three.breed.given
    import dimensions.third.first.flow.Iteration

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        case (Aspect, Classic(Callback)) => true
        case (Aspect, Stepper(OneIncrement)) => true
        case (Aspect, Stepper(Permutations)) => true
        case (Algorithm, Recursive(Extern)) => true
        case (Model, Flow) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case _ => 1
    }

    val exp = wildcard(multiplier)

    given Board = new EmptyBoard(4)

    given Long = Long.MaxValue

    var bin: Path = null

    var result: Seq[Solution] = Nil

    try

      def reset(cycle: Cycle) =
        if bin ne null then tryc(Files.delete(bin))
        bin = cycle(tmp, "test-1-")

      reset(Cycle(wildcard, multiplier))

      var done = false

      while !done
      do

        val (cycle, Some(nest)) = exp(bin)

        val hatch = given_Builder
          .byModel(nest.model)
          .withAlgorithm(nest.algorithm)
          .andAspect(nest.aspect)
          .breed[`Queens*`[?]]

        val queens = hatch()

        given Iteration = Iteration(cycle, NoTag, false, Right(false), Flag(), { () => })

        for
          it <- queens
        do
          result = result :+ it

        if cycle.image.isEmpty
        then
          if cycle.turn eq null
          then
            done = true
          else
            reset(cycle.turn)

    finally
      if bin ne null then tryc(Files.delete(bin))

    val solutions: List[Solution] = List.fill(exp.size)(
      List(
        List(3 x 1, 2 x 3, 1 x 0, 0 x 2),
        List(3 x 2, 2 x 0, 1 x 3, 0 x 1),
      )
    ).reduce(_:::_)

    assert(solutions === result)

  }


  "Cycle once all 4*2 expansions recursive extern flow Queens bis" should "suspend/resume 4*2*2 solutions" in {

    import dimensions.third.first.flow.bis.Queens

    import dimensions.three.breed.bis.given
    import dimensions.third.first.Iteration1

    import dimensions.fifth.cycle.Preemption

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        case (Aspect, Classic(Callback)) => true
        case (Aspect, Stepper(OneIncrement)) => true
        case (Aspect, Stepper(Permutations)) => true
        case (Algorithm, Recursive(Extern)) => true
        case (Model, Flow) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case _ => 1
    }

    val exp = wildcard(multiplier)

    given Board = new EmptyBoard(4)

    given Long = Long.MaxValue

    var bin: Path = null

    var result: Seq[Solution] = Nil

    try

      def reset(cycle: Cycle) =
        if bin ne null then tryc(Files.delete(bin))
        bin = cycle(tmp, "test-2-")

      reset(Cycle(wildcard, multiplier))

      var done = false

      while !done
      do

        val (cycle, Some(nest)) = exp(bin)

        val hatch = given_Builder
          .byModel(nest.model)
          .withAlgorithm(nest.algorithm)
          .andAspect(nest.aspect)
          .breed[Queens[?]]

        val queens = hatch()

        given Iteration1 = Iteration1(cycle, NoTag, Flag(), { () => })

        given Simulate = Left(Preemption.step())

        queens { it ?=>
          result = result :+ it
        }

        if cycle.image.isEmpty
        then
          if cycle.turn eq null
          then
            done = true
          else
            reset(cycle.turn)

    finally
      if bin ne null then tryc(Files.delete(bin))

    val solutions: List[Solution] = List.fill(exp.size)(
      List(
        List(3 x 1, 2 x 3, 1 x 0, 0 x 2),
        List(3 x 2, 2 x 0, 1 x 3, 0 x 1),
      )
    ).reduce(_:::_)

    assert(solutions === result)

  }

}
