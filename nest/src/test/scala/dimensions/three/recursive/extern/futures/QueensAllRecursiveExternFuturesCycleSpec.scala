package queens
package dimensions.three
package recursive
package extern
package futures

import java.nio.file.{ Files, Path }
import java.util.concurrent.TimeoutException

import scala.concurrent.{ Await, ExecutionContext, Future }
import scala.concurrent.duration.{ Duration, FiniteDuration }
import ExecutionContext.Implicits._

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.BeforeAndAfterAll

import scala.util.control.NonLocalReturns.{ returning, throwReturn => thr }

import base.{ Board, EmptyBoard }

import common.bis.Cycle
import Cycle.apply

import common.Macros.tryc

import common.NoTag

import common.geom.x

import dimensions.Dimension.{ Aspect, Algorithm, Model }
import Aspect._
import Classic._
import Stepper._
import Algorithm.Recursive
import Recursive.Extern
import Model.Parallel
import Parallel.Futures

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier

import common.monad.Item.given

import common.Flag


class QueensAllRecursiveExternFuturesCycleSpec extends AnyFlatSpec with BeforeAndAfterAll {

  {
    import sys.Prop.StringProp
    sys.Prop("scala.concurrent.context.numThreads").set("x32")
  }

  import sys.Prop.IntProp
  sys.Prop("scala.concurrent.context.maxThreads").set("8192")
  sys.Prop("scala.concurrent.context.maxExtraThreads").set("8192")
  sys.Prop("scala.concurrent.context.minThreads").set("512")
  sys.Prop("java.util.concurrent.ForkJoinPool.common.parallelism").set("4096")


  import QueensAllRecursiveExternFuturesCycleSpec._


  var tmp: Path = null

  override def beforeAll(): Unit =
    tmp = Files.createTempDirectory("queens-all-recursive-extern-futures-cycle-spec-")

  override def afterAll(): Unit =
    tryc(Files.delete(tmp))

  extension(a: Seq[Solution])
    def ===(b: Seq[Solution]): Boolean =
      a.map(_.sorted).sorted == b.map(_.sorted).sorted


  "Cycle once all 4*2 expansions futures Queens" should "suspend/resume 4*2*2 solutions" in {

    import conf.futures.Callbacks

    import dimensions.third.first.futures.`Queens*`
    import dimensions.three.breed.given

    import dimensions.third.first.futures.Iteration

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        case (Aspect, Classic(Callback)) => true
        case (Aspect, Stepper(OneIncrement)) => true
        case (Aspect, Stepper(Permutations)) => true
        case (Algorithm, Recursive(Extern)) => true
        case (Model, Parallel(Futures)) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case _ => 1
    }

    val exp = wildcard(multiplier)

    given Board = new EmptyBoard(4)

    given Long = Long.MaxValue

    var bin: Path = null

    var result: Seq[Solution] = Nil

    try

      def reset(cycle: Cycle) =
        if bin ne null then tryc(Files.delete(bin))
        bin = cycle(tmp, "test-1-")

      reset(Cycle(wildcard, multiplier))

      var done = false

      while !done
      do

        val (cycle, Some(nest)) = exp(bin)

        val hatch = given_Builder
          .byModel(nest.model)
          .withAlgorithm(nest.algorithm)
          .andAspect(nest.aspect)
          .breed[`Queens*`[?]]

        val queens = hatch()

        var s: Future[?] = null
        var fs: Seq[Future[?]] = Nil

        val ec = implicitly[ExecutionContext]
        val cbs = Callbacks[NoTag.type](
          { (_, _, _, f) => s = f },
          { (_, _, _, _, _, it: Option[Future[?]]) => it.foreach { f => fs = fs :+ f } }
        )

        given Iteration = Iteration(cycle, NoTag, false, Right(false), Flag(), { () => }, ec -> cbs)

        for
          it <- queens
        do
          result = result :+ it

        await(Seq(s))()
        await(fs)()

        if cycle.image.isEmpty
        then
          if cycle.turn eq null
          then
            done = true
          else
            reset(cycle.turn)

    finally
      if bin ne null then tryc(Files.delete(bin))

    val solutions: List[Solution] = List.fill(exp.size)(
      List(
        List(3 x 1, 2 x 3, 1 x 0, 0 x 2),
        List(3 x 2, 2 x 0, 1 x 3, 0 x 1),
      )
    ).reduce(_:::_)

    assert(solutions === result)

  }

  "Cycle once all 4*2 expansions futures Queens bis" should "suspend/resume 4*2*2 solutions" in {

    import conf.futures.bis.Callbacks

    import dimensions.third.first.futures.bis.Queens

    import dimensions.three.breed.bis.given
    import dimensions.third.first.Iteration1

    import dimensions.fifth.cycle.Preemption

    val wildcard = Wildcard (
      (_, _) match
        case (Aspect, Classic(Straight)) => true
        case (Aspect, Classic(Callback)) => true
        case (Aspect, Stepper(OneIncrement)) => true
        case (Aspect, Stepper(Permutations)) => true
        case (Algorithm, Recursive(Extern)) => true
        case (Model, Parallel(Futures)) => true
        case _ => false
    )

    val multiplier = Multiplier[Nest] {
      case _ => 1
    }

    val exp = wildcard(multiplier)

    given Board = new EmptyBoard(4)

    given Long = Long.MaxValue

    var bin: Path = null

    var result: Seq[Solution] = Nil

    try

      def reset(cycle: Cycle) =
        if bin ne null then tryc(Files.delete(bin))
        bin = cycle(tmp, "test-2-")

      reset(Cycle(wildcard, multiplier))

      var done = false

      while !done
      do

        val (cycle, Some(nest)) = exp(bin)

        val hatch = given_Builder
          .byModel(nest.model)
          .withAlgorithm(nest.algorithm)
          .andAspect(nest.aspect)
          .breed[Queens[?]]

        val queens = hatch()

        val ec = implicitly[ExecutionContext]
        val cbs = Callbacks()

        given Iteration1 = Iteration1(cycle, NoTag, Flag(), { () => }, ec -> cbs)

        given Simulate = Left(Preemption.step())

        queens { it ?=>
          result = result :+ it
        }

        cbs(None)

        if cycle.image.isEmpty
        then
          if cycle.turn eq null
          then
            done = true
          else
            reset(cycle.turn)

    finally
      if bin ne null then tryc(Files.delete(bin))

    val solutions: List[Solution] = List.fill(exp.size)(
      List(
        List(3 x 1, 2 x 3, 1 x 0, 0 x 2),
        List(3 x 2, 2 x 0, 1 x 3, 0 x 1),
      )
    ).reduce(_:::_)

    assert(solutions === result)

  }


}


object QueensAllRecursiveExternFuturesCycleSpec:

  private def await(fs: Seq[Future[?]])
                   (ns: FiniteDuration = Duration.fromNanos(500000)): Unit =
    var n = fs.size
    while n > 0 do
      n = fs.size
      for it <- fs do
        if it.isCompleted
        then
          n -= 1
        else
          try
            Await.ready(it, ns)
          catch
            case _: InterruptedException | _: TimeoutException =>
