package queens
package dimensions.three
package recursive
package native
package flow

import org.scalatest.flatspec.AnyFlatSpec

import scala.util.control.NonLocalReturns.{ returning, throwReturn => thr }

import base.{ Board, EmptyBoard }

import common.NoTag

import common.geom.x

import dimensions.Dimension.Aspect.classic
import dimensions.Dimension.Algorithm.native
import dimensions.Dimension.Model.flow

import dimensions.third.first.flow.`Queens*`
import common.monad.Item.given

import dimensions.three.breed.given

import common.Flag
import dimensions.third.first.flow.Iteration


class QueensClassicRecursiveNativeFlowSpec extends AnyFlatSpec {

  def compareSolutions(a: Seq[Solution], b: Seq[Solution]): Boolean = returning {
    for (it <- a) {
      if !b.exists(_.sorted == it.sorted) then
        thr(false)
    }
    for (it <- b) {
      if !a.exists(_.sorted == it.sorted) then
        thr(false)
    }
    true
  }


  "A flow recursive native classic Queens" should "have 1 solution" in {

    sys.BooleanProp
      .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
      .enable()

    val x = true
    val o = false

    given Board = new Board(
      List(
        List(o, x, o),
        List(x, x, x),
        List(x, x, o),
      )
    )

    given Iteration = Iteration(given_Board, NoTag, false, Right(false), Flag(), { () => })

    given Long = Long.MaxValue

    val hatch = given_Builder
      .byModel(flow)
      .withAlgorithm(native)
      .andAspect(classic)
      .breed[`Queens*`[?]]

    val queens = hatch()

    val result: Iterable[Solution] = for { it <- queens } yield it

    val solutions: List[Solution] = List(
      List(0 x 0, 0 x 2, 2 x 2)
    )

    assert(compareSolutions(solutions, result.toSeq))

  }


  "A flow recursive native classic Queens" should "have 4 solutions" in {

    sys.BooleanProp
      .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
      .enable()

    val X = true
    val O = false

    given Board = new Board(
      List(
        List(O, X, O),
        List(X, X, X),
        List(O, X, O),
      )
    )

    given Iteration = Iteration(given_Board, NoTag, false, Right(false), Flag(), { () => })

    given Long = Long.MaxValue

    val hatch = given_Builder
      .byModel(flow)
      .withAlgorithm(native)
      .andAspect(classic)
      .breed[`Queens*`[?]]

    val queens = hatch()

    val result: Iterable[Solution] = for (it <- queens) yield it

    val solutions: List[Solution] = List(
      List(0 x 0, 0 x 2, 2 x 2),
      List(0 x 0, 2 x 0, 2 x 2),
      List(0 x 2, 2 x 0, 2 x 2),
      List(0 x 0, 2 x 0, 0 x 2),
    )

    assert(compareSolutions(solutions, result.toSeq))

  }


  "A flow recursive native classic Queens w/ empty main diagonal" should "have 0 solutions" in {

    sys.BooleanProp
      .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
      .enable()

    val X = true
    val O = false

    given Board = new Board(
      List(
        List(X, X, O),
        List(X, O, X),
        List(O, X, X),
      )
    )

    given Iteration = Iteration(given_Board, NoTag, false, Right(false), Flag(), { () => })

    given Long = Long.MaxValue

    val hatch = given_Builder
      .byModel(flow)
      .withAlgorithm(native)
      .andAspect(classic)
      .breed[`Queens*`[?]]

    val queens = hatch()

    var count = 0

    for (it <- queens) count += 1

    assert(count == 0)

  }


  "A flow recursive native classic Queens w/ empty diagonal" should "have 0 solutions" in {

    sys.BooleanProp
      .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
      .enable()

    val X = true
    val O = false

    given Board = new Board(
      List(
        List(X, X, O),
        List(X, O, X),
        List(O, X, X),
      )
    )

    given Iteration = Iteration(given_Board, NoTag, false, Right(false), Flag(), { () => })

    given Long = Long.MaxValue

    val hatch = given_Builder
      .byModel(flow)
      .withAlgorithm(native)
      .andAspect(classic)
      .breed[`Queens*`[?]]

    val queens = hatch()

    var count = 0

    for (it <- queens) count += 1

    assert(count == 0)

  }


  "A flow recursive native classic Queens w/ empty column" should "have 3 solutions" in {

    sys.BooleanProp
      .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
      .enable()

    val X = true
    val O = false

    given Board = new Board(
      List(
        List(O, X, O),
        List(O, X, X),
        List(O, X, O),
      )
    )

    given Iteration = Iteration(given_Board, NoTag, false, Right(false), Flag(), { () => })

    given Long = Long.MaxValue

    val hatch = given_Builder
      .byModel(flow)
      .withAlgorithm(native)
      .andAspect(classic)
      .breed[`Queens*`[?]]

    val queens = hatch()

    val result: Iterable[Solution] = for (it <- queens) yield it

    val solutions: List[Solution] = List(
      List(0 x 0, 0 x 2, 2 x 2),
      List(1 x 0, 0 x 2, 2 x 2),
      List(2 x 0, 0 x 2, 2 x 2),
    )

    assert(compareSolutions(solutions, result.toSeq))

  }


  "A flow recursive native classic Queens w/ empty row" should "have 3 solutions" in {

    sys.BooleanProp
      .valueIsTrue(_root_.queens.main.Properties.Validator.optimized)
      .enable()

    val X = true
    val O = false

    given Board = new Board(
      List(
        List(O, O, O),
        List(X, X, X),
        List(O, X, O),
      )
    )

    given Iteration = Iteration(given_Board, NoTag, false, Right(false), Flag(), { () => })

    given Long = Long.MaxValue

    val hatch = given_Builder
      .byModel(flow)
      .withAlgorithm(native)
      .andAspect(classic)
      .breed[`Queens*`[?]]

    val queens = hatch()

    val result: Iterable[Solution] = for (it <- queens) yield it

    val solutions: List[Solution] = List(
      List(0 x 0, 2 x 0, 2 x 2),
      List(0 x 1, 2 x 0, 2 x 2),
      List(0 x 2, 2 x 0, 2 x 2),
    )

    assert(compareSolutions(solutions, result.toSeq))

  }


  "A flow recursive native classic Queens w/ empty 4x4 board" should "have 2 solutions in cache" in {

    given Board = new EmptyBoard(4)

    given Iteration = Iteration(given_Board, NoTag, false, Right(false), Flag(), { () => })

    given Long = Long.MaxValue

    val hatch = given_Builder
      .byModel(flow)
      .withAlgorithm(native)
      .andAspect(classic)
      .breed[`Queens*`[?]]

    val queens = hatch()

    val result = (for (it <- queens) yield it.the).toSeq

    val result2 = (for (it <- queens) yield it.the).toSeq

    assert(result.size == result.size)

    val compare = for
      i <- 0 until result.size
      j <- 0 until given_Board.N
    yield
      result(i)(j) eq result2(i)(j)

    assert(compare.forall(identity _))

  }

  "Simulate a flow recursive native classic 4x4" should "accumulate 2 solutions on callback" in {
    import java.util.UUID

    import common.bis.Cycle

    import di.ValidatorOpt
    import dimensions.three.recursive.native.flow.bis.QueensClassic

    import dimensions.third.first.Iteration1

    import dimensions.fifth.cycle.{ Preemption, Simulate }

    given Board = new EmptyBoard(4)

    given Long = Long.MaxValue

    val io: IO = (given_Board, null, given_Long, UUID.randomUUID) -> None

    val cycle = new Cycle(io, null)()

    given Iteration1 = Iteration1(Cycle(cycle), NoTag, Flag(), { () => })

    var result: Seq[Solution] = Nil

    val preemption = Preemption {
      case ((None, (_, Some(its))), (_, Right(_))) =>
        result = result :+ its.last.toList
        Left(1)

      case _ =>
        Left(1)
    }

    given Simulate = Left(preemption)

    var f = false

    QueensClassic(ValidatorOpt()) { f = true }

    assert(f)

    val solutions: List[Solution] = List(
      List(3 x 1, 2 x 3, 1 x 0, 0 x 2),
      List(3 x 2, 2 x 0, 1 x 3, 0 x 1),
    )

    assert(compareSolutions(solutions, result))

  }

}
