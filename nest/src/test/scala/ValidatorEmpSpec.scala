package queens

import org.scalatest.flatspec.AnyFlatSpec

import base.{ Board, EmptyBoard }
import di.ValidatorEmp
import common.geom.x


class ValidatorEmpSpec extends AnyFlatSpec {

  "A 3x3 empty board validator" should "NOT verify queens on empty row" in {

    given Board = new EmptyBoard(3)

    val solution: Solution = List(0 x 0, 0 x 2, 2 x 1)

    !ValidatorEmp(solution)

  }


  "A 3x3 empty board validator" should "NOT verify queens on empty column" in {

    given Board = new EmptyBoard(3)

    val solution: Solution = List(0 x 0, 2 x 0, 1 x 2)

    !ValidatorEmp(solution)

  }


  "A 3x3 empty board validator" should "NOT verify queens on empty main diagonal" in {

    given Board = new EmptyBoard(3)

    val solution: Solution = List(0 x 0, 2 x 2, 2 x 0)

    !ValidatorEmp(solution)

  }


  "A 5x5 empty board validator" should "verify queens" in {

    given Board = new EmptyBoard(5)

    val solution: Solution = List(0 x 0, 1 x 2, 2 x 4, 3 x 2, 4 x 3)

    ValidatorEmp(solution)

  }

}
