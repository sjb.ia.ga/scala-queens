package queens.common;


public class Integer2 extends Serializable2<Integer> {

    public Integer2() {
        this(0);
    }

    public Integer2(int value) {
        this.value = value;
    }

    public void inc() {
        value++;
    }

    public void dec() {
        value--;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Integer2(value);
    }

}
