package queens.common;

import java.io.Serializable;
import java.io.IOException;
import java.io.ObjectStreamException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


abstract class Serializable2<T> implements Serializable {

    public static final long serialVersionUID = 2L;

    protected T value;

    public T get() {
        return value;
    }

    public void set(T value) {
        this.value = value;
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.writeObject(value);
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        value = (T) in.readObject();
    }

    private void readObjectNoData() throws ObjectStreamException {}

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Serializable2<T> that = (Serializable2<T>) obj;

        return this.value.equals(that.value);
    }

    @Override
    public String toString() {
        return value.toString();
    }

}
