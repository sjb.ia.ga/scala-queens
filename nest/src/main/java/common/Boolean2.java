package queens.common;


public class Boolean2 extends Serializable2<Boolean> {

    public Boolean2() {
        this(false);
    }

    public Boolean2(boolean value) {
        this.value = value;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new Boolean2(value);
    }

}
