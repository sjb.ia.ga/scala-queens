package queens
package base

import scala.quoted.{ Expr, Type, Quotes }


object Macros:

  import common.pipeline.Context.Round

  import dimensions.third.`Queens*`

  import dimensions.fifth.output.console.QueensConsoleOutput

  def `co*Code`[It <: AnyVal](
    tag: Expr[Any],
    board: Expr[Board],
    queens: Expr[`Queens*`[Point, It]],
    cb: Expr[(`Queens*`[Point, It], Solution, Long) => Boolean],
    fun: Expr[(`Queens*`[Point, It], Solution) => Unit],
    max: Expr[Long], it: Expr[It])
   (using Type[It], Quotes): Expr[Unit] =
   '{
      val pipeline = QueensConsoleOutput($queens)

      given Long = $max
      given It = $it
      var number = 0L
      for
        it <- $queens
        if { number += 1; $cb($queens, it, number) }
      do
        pipeline.||(it, Round($board, $tag, $queens, number))
        $fun($queens, it)
    }

  inline def `co*`[It <: AnyVal](
    inline tag: Any,
    inline board: Board,
    inline queens: `Queens*`[Point, It])
   (inline cb: (`Queens*`[Point, It], Solution, Long) => Boolean = { (_: `Queens*`[Point, It], _: Solution, _: Long) => true })
   (inline fun: (`Queens*`[Point, It], Solution) => Unit = { (_: `Queens*`[Point, It], _: Solution) => })
   (using inline max: Long, inline it: It): Unit =
   ${ `co*Code`('tag, 'board, 'queens, 'cb, 'fun, 'max, 'it) }
