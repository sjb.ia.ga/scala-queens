package queens
package base

import common.monad.{ Item, `Monad"` }

import conf.Conf


abstract trait QueensMonad[
  P <: Conf
] extends `Monad"`[Solution, Long, Boolean]:

  protected val * : P

  def foreach(block: Item[Solution] => Unit)
             (using Long, Boolean): Iterable[Unit]

  def map[R](block: Item[Solution] => R)
            (using Long, Boolean): Iterable[R]

  def flatMap[R](block: Item[Solution] => Iterable[R])
                (using Long, Boolean): Iterable[R]
