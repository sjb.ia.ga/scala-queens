package queens
package base
package breed


/**
  * @see [[queens.dimensions.three.breed.Hatch]]
  * @see [[queens.dimensions.fourth.fifth.breed.Spawn]]
  */
abstract trait Breed
