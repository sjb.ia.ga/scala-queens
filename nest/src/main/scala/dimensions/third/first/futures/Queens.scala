package queens
package dimensions.third.first
package futures

import java.util.UUID
import java.io.{ FileOutputStream, ObjectOutputStream }

import java.util.concurrent.atomic.AtomicLong

import scala.Function.const

import scala.concurrent.{ ExecutionContext, Future }

import base.Board

import common.bis.Cycle
import common.{ Counter, Flag, ParallelIterable, :- }
import common.futures._
import common.monad.Item

import Counter.given
import Macros.{ biSim, load, save, suspending }
import common.Macros.{ tryc, busywaitUntil }

import conf.futures.bis.Callbacks

import dimensions.fifth.cycle.{ Simulate, Progress }

import dimensions.Dimension.Model.futures

import version.less.nest.Nest


abstract trait Queens[At]
    extends dimensions.third.Queens[At]
    with dimensions.second.`Queens*`[At]:

  final override val model = futures

  protected def `solve*`[R](wrap: ((Long, R)) => Unit)(
                            block: Item[Solution] => Option[R])(using
                            M: Long,
                            start: At,
                            iteration: Iteration): Unit =
    val Iteration(board, tag, _, pre, run, flg, end, (ec, cbs)) = iteration

    val dry = run.getOrElse(false)

    assert(pre -> run.isRight)

    if M <= 0 || dry then
      end()
      return

    val nest = Nest(aspect, algorithm, model)
    val i @ (_, _, _, uuid) =
      board match
        case Cycle(((board, `nest`, `M`, uuid), _), _) => (board, nest, M, uuid)
        case _ => (board, nest, M, UUID.randomUUID)

    given ExecutionContext = ec

    cbs.iterationCallback(tag, nest, uuid,
      `Future*` { solver =>
        val spindle =
          cbs match
            case it: Callbacks => it.spindle
            case _ => AtomicLong(0)

        val sim = run.swap.toOption

        var tries = new Counter(sim.map(const(true)).getOrElse(false))
        var finds = new Counter(sim.map(const(true)).getOrElse(false))

        val bis: ((Simulate, Progress)) => (Boolean, Any) => Option[Unit] =
          biSim(flg, tries, finds, _)

        var pad: Any = null
        var fos: FileOutputStream = null
        var oos: ObjectOutputStream = null

        var solutions = LazyList[Solution]()

        var (number, number2) = 0L -> 0L

        var thaw: Option[Either[Load, At]] = Some(Right(start))

        board match
          case Cycle(_, Some(path)) => load(path) match
            case (x, Some((n, t, f, s, l))) => pad = x
              thaw = l.map(Left(_)); tries = t; finds = f; solutions = s; number = n; number2 = n
            case (x, _) => pad = x
          case _ =>

        var event: Future[Unit] = null

        val preemptor: ((Option[Future[?]], (Progress, Boolean))) => Option[Unit] =
          { case (evt, (pro, ctr)) =>
              sim.map(_ -> pro).flatMap(bis(_)(ctr, evt))
          }

        val iterator: Option[(Solution, (Long, (Progress, Boolean)))] => Unit =
          {
            case Some((s, (n, p))) =>

              if (s eq Nil) -> pre then
                spindle.incrementAndGet()
                event = `Future*` { f =>
                  try
                    val vi = (board, tag, uuid, n, solver, f)
                    val it = new Item(s, flg.slot, s :- vi)
                    block(it).foreach(r => wrap(n -> r))
                  finally
                    tryc(cbs.solutionCallback(tag, nest, uuid, s :- n, solver, Some(f)))
                    spindle.decrementAndGet()
                }

              if !pre
              then
                preemptor((s :- event) -> p)

            case _ =>
              if !suspending(oos eq null, board)
              then
                tryc(cbs.solutionCallback(tag, nest, uuid, Some(number), solver, None))
                if !pre
                then
                  number :: finds
                  preemptor(None -> ((None -> (i -> None)) -> false))

          }

        given (Either[Save, Solution] => Boolean) = { s2 =>
          val its = s2.toOption

          if flg then
            if !pre then +(if its.isEmpty then tries else finds)

            its.map { it =>
              solutions = solutions :+ it
              number += 1
            }

            if its.isEmpty -> (tries -> pre)
            then
              val pro = None -> (i -> its.flatMap { _ => sim.map { _ => LazyList.from(solutions) } })
              iterator(its.orElse(Some(Nil)).map(_ -> (number -> (pro -> its.isEmpty))))

            (board, s2) match
              case (cycle @ Cycle(_, Some(path)), Left(fun)) if number2 < number =>
                if flg
                then
                  fos = FileOutputStream(path.toString)
                  oos = ObjectOutputStream(fos)
                  save(oos, tries, finds, solutions, number, Some(fun), pad)
                  !flg
                else
                  cycle.image = None
              case _ =>

          flg
        }

        given Board = board

        try

          given Either[Load, At] =
            if thaw.isEmpty
            then
              Right(start)

            else
              if thaw.get.isRight
              then
                if !pre
                then
                  preemptor(None -> ((Some(board) -> (i -> None)) -> true))

                board match
                  case (cycle @ Cycle(_, Some(path))) =>
                    if flg
                    then
                      fos = FileOutputStream(path.toString)
                      oos = ObjectOutputStream(fos)
                      save(oos, tries, finds, solutions, number, None, pad)
                      !flg
                      throw ComputationSuspended
                    else
                      cycle.image = None
                  case _ =>

              thaw.get

          if flg then
            solve match
              case Some(true) =>
                cacheOps.put(cacheKey, solutions)
              case _ =>

        catch
          case ComputationSuspended =>
            tryc(oos.close)
            tryc(fos.close)
          case ResumeNotImplemented | SuspendNotSupported =>

        finally
          iterator(None)
          tryc(end())

        spindle.busywaitUntil(0L)(1L)

        spindle.decrementAndGet // bis

      }
    )


abstract trait `Queens*`[At]
    extends Queens[At]
    with dimensions.third.`Queens*`[At, Iteration]:

  inline protected def `solve*`[R](pit: ParallelIterable[?],
                                   wrap: ((Long, R)) => Unit = { (_: (Long, R)) => })(
                                   block: Item[Solution] => Option[R])(using
                                   M: Long,
                                   start: At,
                                   iteration: Iteration): Unit =
    val Iteration(board, tag, _, pre, run, flg, end, it) = iteration
    `solve*`[R](wrap)(block)(using M, start, Iteration(board, tag, pre, run, flg, { () => tryc { pit(); end() } }, it))

  final protected def `foreach*`(block: Item[Solution] => Unit)(using
                                 Long, At, Iteration): Iterable[Unit] =
    val `block*` = `apply*`(block)
    val r = new ParallelIterable[Unit]()
    `solve*`[Unit](r)(`block*`)
    Nil

  final protected def `map*`[R](block: Item[Solution] => R)(using
                                Long, At, Iteration): Iterable[R] =
    val `block*` = `apply*`(block)
    val r = new ParallelIterable[R]()
    val wrap: ((Long, R)) => Unit = { case (n, it) => r.addOne(n -> Some(it)) }
    `solve*`(r, wrap)(`block*`)
    r

  final protected def `flatMap*`[R](block: Item[Solution] => Iterable[R])(using
                                    Long, At, Iteration): Iterable[R] =
    val `block*` = `apply*`(block)
    val r = new ParallelIterable[R]()
    `solve*`(r, r.addOne _)(`block*`)
    r


package bis:

  import base.CacheOps

  abstract trait Queens[At]
      extends dimensions.third.bis.Queens[At]
      with dimensions.third.first.futures.Queens[At]:

    override protected val cacheOps: CacheOps =
      CacheOps.WithoutCache
