package queens
package dimensions.third.first

import java.util.UUID

import scala.util.Try

import base.Board
import common.Flag
import common.bis.Cycle


class Iteration1(val params: Any*)


package actors:

  import akka.actor.typed.scaladsl.ActorContext

  import akka.actor.typed.ActorRef

  import conf.actors.Callbacks

  import conf.actors.AnyConf

  class Iteration(override val params: Any*)
      extends AnyVal
      with AnyConf:

    override def it: (ActorContext[?], Callbacks) = params.last.asInstanceOf[(ActorContext[?], Callbacks)]

  object Iteration:

    inline given Conversion[Iteration1, Iteration] = { it => Iteration(it.params*) }

    def unapply(self: Iteration): Option[(Board, Any, Try[UUID], Boolean, Either[Simulate, Boolean], Flag, () => Unit, (ActorContext[?], Callbacks))] =
      (self.params(0), self.params(1), self.params(2), self.params(3), self.params(4), self.params(5)) match
        case (board: Board, tag: Any, pre: Boolean, run: Either[Simulate, Boolean], flg: Flag, end: (() => Unit)) =>
          Some((board, tag, Try(tag.asInstanceOf[UUID]), pre, run, flg, end, self.it))
        case _ =>
          None

  package bis:

    object Iteration2:

      import conf.actors.bis.Callbacks

      def unapply(self: Iteration1): Option[(Cycle, Any, Try[UUID], Flag, () => Unit, (ActorContext[?], Callbacks))] =
        (self.params(0), self.params(1), self.params(2), self.params(3), self.params.last) match
          case (cycle: Cycle, tag: Any, flg: Flag, end: (() => Unit), (ctx: ActorContext[?], cbs: Callbacks)) =>
            Some((cycle, tag, Try(tag.asInstanceOf[UUID]), flg, end, (ctx, cbs)))
          case _ =>
            None


package futures:

  import scala.concurrent.{ ExecutionContext, Future, SyncChannel }

  import conf.futures.Callbacks

  import conf.futures.AnyConf

  class Iteration(override val params: Any*)
      extends AnyVal
      with AnyConf:

    override def it: (ExecutionContext, Callbacks) = params.last.asInstanceOf[(ExecutionContext, Callbacks)]

  object Iteration:

    inline given Conversion[Iteration1, Iteration] = { it => Iteration(it.params*) }

    def unapply(self: Iteration): Option[((Board, Any, Try[UUID], Boolean, Either[Simulate, Boolean], Flag, () => Unit, (ExecutionContext, Callbacks)))] =
      (self.params(0), self.params(1), self.params(2), self.params(3), self.params(4), self.params(5)) match
        case (board: Board, tag: Any, pre: Boolean, run: Either[Simulate, Boolean], flg: Flag, end: (() => Unit)) =>
          Some((board, tag, Try(tag.asInstanceOf[UUID]), pre, run, flg, end, self.it))
        case _ =>
          None

  package bis:

    object Iteration2:

      import conf.futures.bis.Callbacks

      def unapply(self: Iteration1): Option[(Cycle, Any, Try[UUID], Flag, () => Unit, (ExecutionContext, Callbacks))] =
        (self.params(0), self.params(1), self.params(2), self.params(3), self.params.last) match
          case (cycle: Cycle, tag: Any, flg: Flag, end: (() => Unit), (ec: ExecutionContext, cbs: Callbacks)) =>
            Some((cycle, tag, Try(tag.asInstanceOf[UUID]), flg, end, (ec, cbs)))
          case _ =>
            None


package flow:

  import conf.flow.AnyConf

  class Iteration(override val params: Any*)
      extends AnyVal
      with AnyConf

  object Iteration:

    inline given Conversion[Iteration1, Iteration] = { it => Iteration(it.params*) }

    def unapply(self: Iteration): Option[(Board, Any, Try[UUID], Boolean, Either[Simulate, Boolean], Flag, () => Unit)] =
      (self.params(0), self.params(1), self.params(2), self.params(3), self.params(4), self.params(5)) match
         case (board: Board, tag: Any, pre: Boolean, run: Either[Simulate, Boolean], flg: Flag, end: (() => Unit)) =>
           Some((board, tag, Try(tag.asInstanceOf[UUID]), pre, run, flg, end))
         case _ =>
           None

  package bis:

    object Iteration2:

      def unapply(self: Iteration1): Option[(Cycle, Any, Try[UUID], Flag, () => Unit)] =
        (self.params(0), self.params(1), self.params(2), self.params(3)) match
          case (cycle: Cycle, tag: Any, flg: Flag, end: (() => Unit)) =>
            Some((cycle, tag, Try(tag.asInstanceOf[UUID]), flg, end))
          case _ =>
            None


package kafka:

  import org.apache.avro.generic.GenericRecord
  import org.apache.kafka.clients.producer.KafkaProducer

  import conf.kafka.AnyConf

  class Iteration(override val params: Any*)
      extends AnyVal
      with AnyConf:

    override def it: KafkaProducer[String, GenericRecord] = params.last.asInstanceOf[KafkaProducer[String, GenericRecord]]

  object Iteration:

    inline given Conversion[Iteration1, Iteration] = { it => Iteration(it.params*) }

    def unapply(self: Iteration): Option[(Board, Any, Try[UUID], Boolean, Either[Simulate, Boolean], Flag, () => Unit, KafkaProducer[String, GenericRecord])] =
      (self.params(0), self.params(1), self.params(2), self.params(3), self.params(4), self.params(5)) match
         case (board: Board, tag: Any, pre: Boolean, run: Either[Simulate, Boolean], flg: Flag, end: (() => Unit)) =>
           Some((board, tag, Try(tag.asInstanceOf[UUID]), pre, run, flg, end, self.it))
         case _ =>
           None

  package bis:

    object Iteration2:

      def unapply(self: Iteration1): Option[(Cycle, Any, Try[UUID], Flag, () => Unit, KafkaProducer[String, GenericRecord])] =
        (self.params(0), self.params(1), self.params(2), self.params(3), self.params.last) match
          case (cycle: Cycle, tag: Any, flg: Flag, end: (() => Unit), producer: KafkaProducer[String, GenericRecord]) =>
            Some((cycle, tag, Try(tag.asInstanceOf[UUID]), flg, end, producer))
          case _ =>
            None

package object kafka:

  import org.apache.avro.Schema

  private val _schema = """{
    "namespace": "solution.queens.avro",
    "type": "record",
    "name": "Solution",
    "fields": [
      {
        "name" : "number",
        "type": "long"
      },
      {
        "name": "points",
        "type": {
          "type": "array",
          "items": {
            "name": "point",
            "type": "record",
            "fields": [
              { "name": "row", "type": "int" },
              { "name": "column", "type": "int" }
            ]
          }
        }
      }
    ]
  }"""

  val schema = Schema.Parser().parse(_schema)


package rabbitMQ:

  import com.rabbitmq.client.Channel

  import conf.rabbitMQ.AnyConf

  class Iteration(override val params: Any*)
      extends AnyVal
      with AnyConf:

    override def it: Channel = params.last.asInstanceOf[Channel]

  object Iteration:

    inline given Conversion[Iteration1, Iteration] = { it => Iteration(it.params*) }

    def unapply(self: Iteration): Option[(Board, Any, Try[UUID], Boolean, Either[Simulate, Boolean], Flag, () => Unit, Channel)] =
      (self.params(0), self.params(1), self.params(2), self.params(3), self.params(4), self.params(5)) match
         case (board: Board, tag: Any, pre: Boolean, run: Either[Simulate, Boolean], flg: Flag, end: (() => Unit)) =>
           Some((board, tag, Try(tag.asInstanceOf[UUID]), pre, run, flg, end, self.it))
         case _ =>
           None

  package bis:

    object Iteration2:

      def unapply(self: Iteration1): Option[(Cycle, Any, Try[UUID], Flag, () => Unit, Channel)] =
        (self.params(0), self.params(1), self.params(2), self.params(3), self.params.last) match
          case (cycle: Cycle, tag: Any, flg: Flag, end: (() => Unit), channel: Channel) =>
            Some((cycle, tag, Try(tag.asInstanceOf[UUID]), flg, end, channel))
          case _ =>
            None


package awsSQS:

  import com.amazonaws.services.sqs.AmazonSQS

  import conf.awsSQS.AnyConf

  class Iteration(override val params: Any*)
      extends AnyVal
      with AnyConf:

    override def it: AmazonSQS = params.last.asInstanceOf[AmazonSQS]

  object Iteration:

    inline given Conversion[Iteration1, Iteration] = { it => Iteration(it.params*) }

    def unapply(self: Iteration): Option[(Board, Any, Try[UUID], Boolean, Either[Simulate, Boolean], Flag, () => Unit, AmazonSQS)] =
      (self.params(0), self.params(1), self.params(2), self.params(3), self.params(4), self.params(5)) match
         case (board: Board, tag: Any, pre: Boolean, run: Either[Simulate, Boolean], flg: Flag, end: (() => Unit)) =>
           Some((board, tag, Try(tag.asInstanceOf[UUID]), pre, run, flg, end, self.it))
         case _ =>
           None

  package bis:

    object Iteration2:

      def unapply(self: Iteration1): Option[(Cycle, Any, Try[UUID], Flag, () => Unit, AmazonSQS)] =
        (self.params(0), self.params(1), self.params(2), self.params(3), self.params.last) match
          case (cycle: Cycle, tag: Any, flg: Flag, end: (() => Unit), sqs: AmazonSQS) =>
            Some((cycle, tag, Try(tag.asInstanceOf[UUID]), flg, end, sqs))
          case _ =>
            None
