package queens
package dimensions.third.first

import scala.quoted.{ Expr, Type, Quotes }

import base.Board

import common.Macros.tryc


object Macros:

  import common.{ Counter, Flag }
  import Counter.given

  import dimensions.fifth.cycle.{ Progress, Preemption, Suspension, Simulation }

  def biSimCode(
    flg: Expr[Flag],
    triesM: Expr[Boolean], triesN: Expr[Long], triesC: Expr[Long], tries: Expr[Counter],
    findsM: Expr[Boolean], findsN: Expr[Long], findsC: Expr[Long], finds: Expr[Counter],
    simpro: Expr[(Simulate, Progress)])
   (using Quotes): Expr[(Boolean, Any) => Option[Unit]] =
   '{
      { (ctr: Boolean, any: Any) =>
        if (if ctr then $triesM else $findsM)
        then
          val (count, counter) =
            if ctr
            then
              (($triesN -> any) -> Left($triesC), $tries)
            else
              (($findsN -> any) -> Right($findsC), $finds)
          val generation = $simpro._2 -> count
          def run(simulation: Simulation[?]): Option[Unit] =
            simulation match
              case pre @ Preemption() =>
                val it = pre(generation)
                val n = it match
                  case Left(n) => n
                  case Right(n) => n
                if n == 0 then !$flg else counter(n)
                it.toOption.map(_=>())
              case it @ Suspension(preemption) =>
                it(generation).map(run).getOrElse(run(preemption))
          $simpro._1 match
            case Left(it) => run(it)
            case Right(it) => run(it)
        else
          None
      }
    }

  inline def biSim(
    inline flg: Flag,
    inline tries: Counter,
    inline finds: Counter,
    inline simpro: (Simulate, Progress)
  ): (Boolean, Any) => Option[Unit] =
   ${ biSimCode( 'flg,
                 '{ !tries }, '{ ~tries }, 'tries, 'tries,
                 '{ !finds }, '{ ~finds }, 'finds, 'finds, 'simpro)
    }

////////////////////////////////////////////////////////////////////////////////

  import java.nio.file.Path
  import java.io.{ EOFException, FileInputStream, ObjectInputStream, FileOutputStream, ObjectOutputStream }

  import common.bis.Cycle
  import Cycle.{ read, write }

  def loadCode(path: Expr[Path])
              (using Quotes): Expr[(Any, Option[(Long, Counter, Counter, LazyList[Solution], Option[Load])])] =
   '{
      val fis = FileInputStream($path.toString)
      val ois = ObjectInputStream(fis)
      Cycle.read(ois) -> {
        try
          Some {
            (
              ois.readLong(),
              ois.readObject().asInstanceOf[Counter],
              ois.readObject().asInstanceOf[Counter],
              ois.readObject().asInstanceOf[LazyList[Solution]],
              if ois.readBoolean()
              then
                Some(ois -> { () => tryc(ois.close); tryc(fis.close) })
              else
                tryc(ois.close)
                tryc(fis.close)
                None
            )
          }
        catch
          case _: EOFException =>
            None
      }
    }

  inline def load(
    inline path: Path
  ): (Any, Option[(Long, Counter, Counter, LazyList[Solution], Option[Load])]) =
   ${ loadCode('path) }

////////////////////////////////////////////////////////////////////////////////

  def saveCode(
    oos: Expr[ObjectOutputStream],
    tries: Expr[Counter],
    finds: Expr[Counter],
    solutions: Expr[LazyList[Solution]],
    number: Expr[Long],
    fun: Expr[Option[Save]],
    pad: Expr[Any])
    (using Quotes): Expr[Boolean] =
   '{
      $pad.asInstanceOf[Cycle].write($oos)
      $oos.writeLong($number)
      $oos.writeObject($tries)
      $oos.writeObject($finds)
      $oos.writeObject($solutions)
      $oos.writeBoolean($fun.nonEmpty)
      $fun.map(_(Some($oos))).getOrElse(false)
    }


  inline def save(
    inline oos: ObjectOutputStream,
    inline tries: Counter,
    inline finds: Counter,
    inline solutions: LazyList[Solution],
    inline number: Long,
    inline fun: Option[Save],
    inline pad: Any
  ): Boolean =
   ${ saveCode('oos, 'tries, 'finds, 'solutions, 'number, 'fun, 'pad) }

////////////////////////////////////////////////////////////////////////////////

  def suspendingCode(
    unsaved: Expr[Boolean],
    board: Expr[Board])
    (using Quotes): Expr[Boolean] =
   '{
      $board match
        case cycle: Cycle =>
          if $unsaved then cycle.image = None
          cycle.image.nonEmpty
        case _ =>
          false
    }

  inline def suspending(
    inline unsaved: Boolean,
    inline board: Board
  ): Boolean =
   ${ suspendingCode('unsaved, 'board) }

////////////////////////////////////////////////////////////////////////////////

  object zeroth:

    def loadCode(path: Expr[Path])
                (using Quotes): Expr[(Any, Option[(Long, LazyList[Solution], Option[Load])])] =
     '{
        val fis = FileInputStream($path.toString)
        val ois = ObjectInputStream(fis)
        Cycle.read(ois) -> {
          try
            Some {
              (
                ois.readLong(),
                ois.readObject().asInstanceOf[LazyList[Solution]],
                if ois.readBoolean()
                then
                  Some(ois -> { () => tryc(ois.close); tryc(fis.close) })
                else
                  tryc(ois.close)
                  tryc(fis.close)
                  None
              )
            }
          catch
            case _: EOFException =>
              None
        }
      }

    inline def load(
      inline path: Path
    ): (Any, Option[(Long, LazyList[Solution], Option[Load])]) =
     ${ loadCode('path) }

  ////////////////////////////////////////////////////////////////////////////////

    def saveCode(
      oos: Expr[ObjectOutputStream],
      solutions: Expr[LazyList[Solution]],
      number: Expr[Long],
      fun: Expr[Option[Save]],
      pad: Expr[Any])
      (using Quotes): Expr[Boolean] =
     '{
        $pad.asInstanceOf[Cycle].write($oos)
        $oos.writeLong($number)
        $oos.writeObject($solutions)
        $oos.writeBoolean($fun.nonEmpty)
        $fun.map(_(Some($oos))).getOrElse(false)
      }


    inline def save(
      inline oos: ObjectOutputStream,
      inline solutions: LazyList[Solution],
      inline number: Long,
      inline fun: Option[Save],
      inline pad: Any
    ): Boolean =
     ${ saveCode('oos, 'solutions, 'number, 'fun, 'pad) }

////////////////////////////////////////////////////////////////////////////////

  import java.util.concurrent.atomic.AtomicLong

  import akka.actor.typed.ActorRef
  import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }


  type Message = Either[Unit, Option[ActorRef[?]]]
  type Event = ActorRef[Either[Unit, Option[ActorRef[?]]]]


  def spawnCode(block: Expr[ActorContext[?] ?=> Unit],
                ctx_spindle: Expr[(ActorContext[?], AtomicLong)])
         (using Quotes): Expr[Event] =
   '{
      val (ctx, spindle) = $ctx_spindle
      spindle.incrementAndGet
      ctx
        .spawnAnonymous[Message](Behaviors
          .receive { (ctx, msg) =>
            $block(using ctx)
            msg match
              case Right(Some(ref: Event)) =>
                ref ! Left(())
              case _ =>
            spindle.decrementAndGet
            Behaviors.stopped
          }
        )
    }

  inline def `spawn*`(inline block: ActorContext[?] ?=> Unit)
               (using inline ctx_spindle: (ActorContext[?], AtomicLong)): Event =
    ${ spawnCode('block, 'ctx_spindle)  }
