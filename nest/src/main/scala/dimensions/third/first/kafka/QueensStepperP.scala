package queens
package dimensions.third.first
package kafka

import base.Board

import common.geom.Coord

import common.stepper.StepperP

import common.monad.Item


abstract trait QueensStepperP
    extends `Queens*`[Coord[StepperP]]:

  import dimensions.second.first.BoardStepper.start
  import common.stepper.factory.given

  override def foreach(block: Item[Solution] => Unit)
                      (using M: Long, iteration: Iteration): Iterable[Unit] =
    implicit val Iteration(Board(size, _, seed), _, _, _, _, _, _, _) = iteration

    given Coord[StepperP] = start[StepperP](size)
    super.`foreach*`(block)

  override def map[R](block: Item[Solution] => R)
                     (using M: Long, iteration: Iteration): Iterable[R] =
    implicit val Iteration(Board(size, _, seed), _, _, _, _, _, _, _) = iteration

    given Coord[StepperP] = start[StepperP](size)
    super.`map*`(block)

  override def flatMap[R](block: Item[Solution] => Iterable[R])
                         (using M: Long, iteration: Iteration): Iterable[R] =
    implicit val Iteration(Board(size, _, seed), _, _, _, _, _, _, _) = iteration

    given Coord[StepperP] = start[StepperP](size)
    super.`flatMap*`(block)


package `*`:

  import common.stepper.dup.{ StepperPDup => `StepperP*` }

  abstract trait `QueensStepperP*`
      extends `Queens*`[Coord[`StepperP*`]]:

    import dimensions.second.first.`*`.`BoardStepper*`.`start*`
    import common.stepper.dup.factory.given

    override def foreach(block: Item[Solution] => Unit)
                        (using M: Long, iteration: Iteration): Iterable[Unit] =
      implicit val Iteration(Board(size, _, seed), _, _, _, _, _, _, _) = iteration

      given Coord[`StepperP*`] = `start*`[`StepperP*`](size)
      super.`foreach*`(block)

    override def map[R](block: Item[Solution] => R)
                       (using M: Long, iteration: Iteration): Iterable[R] =
      implicit val Iteration(Board(size, _, seed), _, _, _, _, _, _, _) = iteration

      given Coord[`StepperP*`] = `start*`[`StepperP*`](size)
      super.`map*`(block)

    override def flatMap[R](block: Item[Solution] => Iterable[R])
                           (using M: Long, iteration: Iteration): Iterable[R] =
      implicit val Iteration(Board(size, _, seed), _, _, _, _, _, _, _) = iteration

      given Coord[`StepperP*`] = `start*`[`StepperP*`](size)
      super.`flatMap*`(block)
