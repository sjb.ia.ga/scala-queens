package queens
package dimensions.third.first
package kafka

import base.Board

import common.geom.Coord

import common.stepper.Stepper1

import common.monad.Item


abstract trait QueensStepper1
    extends `Queens*`[Coord[Stepper1]]:

  import dimensions.second.first.BoardStepper.start
  import common.stepper.factory.given

  override def foreach(block: Item[Solution] => Unit)
                      (using M: Long, iteration: Iteration): Iterable[Unit] =
    implicit val Iteration(Board(size, _, seed), _, _, _, _, _, _, _) = iteration

    given Coord[Stepper1] = start[Stepper1](size)
    super.`foreach*`(block)

  override def map[R](block: Item[Solution] => R)
                     (using M: Long, iteration: Iteration): Iterable[R] =
    implicit val Iteration(Board(size, _, seed), _, _, _, _, _, _, _) = iteration

    given Coord[Stepper1] = start[Stepper1](size)
    super.`map*`(block)

  override def flatMap[R](block: Item[Solution] => Iterable[R])
                         (using M: Long, iteration: Iteration): Iterable[R] =
    implicit val Iteration(Board(size, _, seed), _, _, _, _, _, _, _) = iteration

    given Coord[Stepper1] = start[Stepper1](size)
    super.`flatMap*`(block)


package `*`:

  import common.stepper.dup.{ Stepper1Dup => `Stepper1*` }

  abstract trait `QueensStepper1*`
      extends `Queens*`[Coord[`Stepper1*`]]:

    import dimensions.second.first.`*`.`BoardStepper*`.`start*`
    import common.stepper.dup.factory.given

    override def foreach(block: Item[Solution] => Unit)
                        (using M: Long, iteration: Iteration): Iterable[Unit] =
      implicit val Iteration(Board(size, _, seed), _, _, _, _, _, _, _) = iteration

      given Coord[`Stepper1*`] = `start*`[`Stepper1*`](size)
      super.`foreach*`(block)

    override def map[R](block: Item[Solution] => R)
                       (using M: Long, iteration: Iteration): Iterable[R] =
      implicit val Iteration(Board(size, _, seed), _, _, _, _, _, _, _) = iteration

      given Coord[`Stepper1*`] = `start*`[`Stepper1*`](size)
      super.`map*`(block)

    override def flatMap[R](block: Item[Solution] => Iterable[R])
                           (using M: Long, iteration: Iteration): Iterable[R] =
      implicit val Iteration(Board(size, _, seed), _, _, _, _, _, _, _) = iteration

      given Coord[`Stepper1*`] = `start*`[`Stepper1*`](size)
      super.`flatMap*`(block)
