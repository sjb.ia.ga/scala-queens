package queens
package dimensions.third.first
package flow

import java.util.UUID
import java.util.Date
import java.io.{ FileOutputStream, ObjectOutputStream }

import scala.Function.const

import scala.collection.mutable.{ ListBuffer => MutableList }

import base.Board

import common.bis.Cycle
import common.{ Counter, Flag, :- }
import common.monad.Item

import Counter.given
import Macros.{ biSim, load, save, suspending }
import common.Macros.tryc

import dimensions.fifth.cycle.{ Simulate, Progress }

import dimensions.Dimension.Model.flow

import version.less.nest.Nest


abstract trait Queens[At]
    extends dimensions.third.Queens[At]
    with dimensions.second.`Queens*`[At]:

  final override val model = flow

  protected def `solve*`[R](wrap: R => Unit = { (_: R) => })(
                            block: Item[Solution] => Option[R])(using
                            M: Long,
                            start: At,
                            iteration: Iteration): Unit =
    val Iteration(board, tag, _, pre, run, flg, end) = iteration

    assert(pre -> run.isRight)

    val dry = run.getOrElse(false)

    if M <= 0 || dry then
      return

    val nest = Nest(aspect, algorithm, model)
    val i @ (_, _, _, uuid) =
      board match
        case Cycle(((board, `nest`, `M`, uuid), _), _) => (board, nest, M, uuid)
        case _ => (board, nest, M, UUID.randomUUID)

    val sim = run.swap.toOption

    var tries = new Counter(sim.map(const(true)).getOrElse(false))
    var finds = new Counter(sim.map(const(true)).getOrElse(false))

    val bis: ((Simulate, Progress)) => (Boolean, Any) => Option[Unit] =
      biSim(flg, tries, finds, _)

    var started: Date = null

    var pad: Any = null
    var fos: FileOutputStream = null
    var oos: ObjectOutputStream = null

    var solutions = LazyList[Solution]()

    var (number, number2) = 0L -> 0L

    var thaw: Option[Either[Load, At]] = Some(Right(start))

    board match
      case Cycle(_, Some(path)) => load(path) match
        case (x, Some((n, t, f, s, l))) => pad = x
          thaw = l.map(Left(_)); tries = t; finds = f; solutions = s; number = n; number2 = n
        case (x, _) => pad = x
      case _ =>

    val preemptor: ((Option[Nest], (Progress, Boolean))) => Option[Unit] =
      { case (evt, (pro, ctr)) =>
          sim.map(_ -> pro).flatMap(bis(_)(ctr, evt))
      }

    given (Either[Save, Solution] => Boolean) = { s2 =>
      val its = s2.toOption

      if flg then
        if !pre then +(if its.isEmpty then tries else finds)

        its.map { it =>
          solutions = solutions :+ it
          number += 1
        }

        if its.isEmpty -> (tries -> pre)
        then
          val (s, n) = its.getOrElse(Nil) -> number

          if (s eq Nil) -> pre then
            val vi = (board, tag, uuid, n, started, Date())
            val it = new Item(s, flg.slot, s :- vi)
            block(it).foreach(wrap)
            started = vi._6

          if !pre then
            val pro = None -> (i -> its.flatMap { _ => sim.map { _ => LazyList.from(solutions) } })
            preemptor((s :- nest) -> (pro -> its.isEmpty))

        (board, s2) match
          case (cycle @ Cycle(_, Some(path)), Left(fun)) if number2 < number =>
            if flg
            then
              fos = FileOutputStream(path.toString)
              oos = ObjectOutputStream(fos)
              save(oos, tries, finds, solutions, number, Some(fun), pad)
              !flg
            else
              cycle.image = None
          case _ =>

      flg
    }

    given Board = board

    try

      given Either[Load, At] =
        if thaw.isEmpty
        then
          Right(start)

        else
          if thaw.get.isRight
          then
            if !pre
            then
              preemptor(None -> ((Some(board) -> (i -> None)) -> true))

            board match
              case (cycle @ Cycle(_, Some(path))) =>
                if flg
                then
                  fos = FileOutputStream(path.toString)
                  oos = ObjectOutputStream(fos)
                  save(oos, tries, finds, solutions, number, None, pad)
                  !flg
                  throw ComputationSuspended
                else
                  cycle.image = None
              case _ =>

          thaw.get

      if flg
      then
        started = Date()

        solve match
          case Some(true) =>
            cacheOps.put(cacheKey, solutions)
          case _ =>

    catch
      case ComputationSuspended =>
        tryc(oos.close)
        tryc(fos.close)
      case ResumeNotImplemented | SuspendNotSupported =>

    finally
      if !suspending(oos eq null, board)
      then
        if !pre
        then
          number :: finds
          preemptor(None -> ((None -> (i -> None)) -> false))
      tryc(end())


abstract trait `Queens*`[At]
    extends Queens[At]
    with dimensions.third.`Queens*`[At, Iteration]:

  final protected def `foreach*`(block: Item[Solution] => Unit)(using
                                 Long, At, Iteration): Iterable[Unit] =
    val `block*` = `apply*`(block)
    `solve*`[Unit]()(`block*`)
    Nil

  final protected def `map*`[R](block: Item[Solution] => R)(using
                                Long, At, Iteration): Iterable[R] =
    val `block*` = `apply*`(block)
    val r = MutableList[R]()
    `solve*`(r.addOne _)(`block*`)
    r

  final protected def `flatMap*`[R](block: Item[Solution] => Iterable[R])(using
                                    Long, At, Iteration): Iterable[R] =
    val `block*` = `apply*`(block)
    val r = MutableList[R]()
    `solve*`(r.addAll _)(`block*`)
    r


package bis:

  import base.CacheOps

  abstract trait Queens[At]
      extends dimensions.third.bis.Queens[At]
      with dimensions.third.first.flow.Queens[At]:

    override protected val cacheOps: CacheOps =
      CacheOps.WithoutCache
