package queens
package dimensions.third.first
package flow

import common.geom.Coord

import common.monad.Item


abstract trait QueensClassicCb
    extends `Queens*`[Coord[Int]]:

  import dimensions.second.first.ClassicCb.zero

  override def foreach(block: Item[Solution] => Unit)
                      (using Long, Iteration): Iterable[Unit] =
    super.`foreach*`(block)

  override def map[R](block: Item[Solution] => R)
                     (using Long, Iteration): Iterable[R] =
    super.`map*`(block)

  override def flatMap[R](block: Item[Solution] => Iterable[R])
                         (using Long, Iteration): Iterable[R] =
    super.`flatMap*`(block)
