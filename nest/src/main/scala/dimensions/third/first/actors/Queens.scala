package queens
package dimensions.third.first
package actors

import java.util.UUID
import java.io.{ FileOutputStream, ObjectOutputStream }

import java.util.concurrent.atomic.AtomicLong

import scala.Function.const

import scala.collection.mutable.{ LongMap => Lengthy }

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.{ ActorContext, Behaviors }

import base.Board

import common.bis.Cycle
import common.{ Counter, Flag, ParallelIterable, :- }
import common.monad.Item

import Counter.given
import Macros.{ biSim, `spawn*`, Event, load, save, suspending }
import common.Macros.{ tryc, busywaitUntil }

import conf.actors.bis.Callbacks

import dimensions.fifth.cycle.{ Simulate, Progress }

import dimensions.Dimension.Model.actors

import version.less.nest.Nest


abstract trait Queens[At]
    extends dimensions.third.Queens[At]
    with dimensions.second.`Queens*`[At]:

  final override val model = actors

  protected def `solve*`[R](wrap: ((Long, R)) => Unit)(
                            block: Item[Solution] => Option[R])(using
                            M: Long,
                            start: At,
                            iteration: Iteration): Unit =
    val Iteration(board, tag, _, pre, run, flg, end, (ctx, cbs)) = iteration

    assert(pre -> run.isRight)

    val dry = run.getOrElse(false)

    if M <= 0 || dry then
      end()
      return

    val nest = Nest(aspect, algorithm, model)
    val i @ (_, _, _, uuid) =
      board match
        case Cycle(((board, `nest`, `M`, uuid), _), _) => (board, nest, M, uuid)
        case _ => (board, nest, M, UUID.randomUUID)

    val solver = ctx.spawnAnonymous[AtomicLong](Behaviors
      .receive { (ctx, spindle) =>
        given ctx_spindle: (ActorContext[?], AtomicLong) = ctx -> spindle

        val sim = run.swap.toOption

        var tries = new Counter(sim.map(const(true)).getOrElse(false))
        var finds = new Counter(sim.map(const(true)).getOrElse(false))

        val bis: ((Simulate, Progress)) => (Boolean, Any) => Option[Unit] =
          biSim(flg, tries, finds, _)

        var pad: Any = null
        var fos: FileOutputStream = null
        var oos: ObjectOutputStream = null

        var solutions = LazyList[Solution]()

        var (number, number2) = 0L -> 0L

        var thaw: Option[Either[Load, At]] = Some(Right(start))

        board match
          case Cycle(_, Some(path)) => load(path) match
            case (x, Some((n, t, f, s, l))) => pad = x
              thaw = l.map(Left(_)); tries = t; finds = f; solutions = s; number = n; number2 = n
            case (x, _) => pad = x
          case _ =>

        var event: Event = null

        val preemptor: ((Option[Event], (Progress, Boolean))) => Option[Unit]  =
          { case (ref, (pro, ctr)) =>
              val evt = Some(ctx_spindle) zip ref.orElse(Some(null))
              sim.map(_ -> pro).flatMap(bis(_)(ctr, evt))
           }

        val iterator: Option[(Solution, (Long, (Progress, Boolean)))] => Unit =
          {
            case Some((s, (n, p))) =>

              event = null

              if (s eq Nil) -> pre then
                event = `spawn*` { `ctx*` ?=>
                  try
                    val vi = (board, tag, uuid, n, ctx.self, `ctx*`.self)
                    val it = new Item(s, flg.slot, s :- vi)
                    block(it).foreach(r => wrap(n -> r))
                  finally
                    tryc(cbs.solutionCallback(tag, nest, uuid, s :- n, ctx.self, Some((ctx, `ctx*`.self))))
                }

              ( if !pre
                then
                  preemptor((s :- event) -> p)
                else
                  None
              ) match
                case None if event ne null =>
                  event ! Right(None)
                case _ =>

              if !pre
              then
                preemptor((s :- event) -> p)

            case _ =>
              if !suspending(oos eq null, board)
              then
                tryc(cbs.solutionCallback(tag, nest, uuid, Some(number), ctx.self, None))
                if !pre
                then
                  number :: finds
                  preemptor(None -> ((None -> (i -> None)) -> false))
          }

        given (Either[Save, Solution] => Boolean) = { s2 =>
          val its = s2.toOption

          if flg
          then
            if !pre then +(if its.isEmpty then tries else finds)

            its.map { it =>
              solutions = solutions :+ it
              number += 1
            }

            if its.isEmpty -> (tries -> pre)
            then
              val pro = None -> (i -> its.flatMap { _ => sim.map { _ => LazyList.from(solutions) } })
              iterator(its.orElse(Some(Nil)).map(_ -> (number -> (pro -> its.isEmpty))))

            (board, s2) match
              case (cycle @ Cycle(_, Some(path)), Left(fun)) if number2 < number =>
                if flg
                then
                  fos = FileOutputStream(path.toString)
                  oos = ObjectOutputStream(fos)
                  save(oos, tries, finds, solutions, number, Some(fun), pad)
                  !flg
                else
                  cycle.image = None
              case _ =>

          flg
        }

        given Board = board

        try

          given Either[Load, At] =
            if thaw.isEmpty
            then
              Right(start)

            else

              if thaw.get.isRight
              then
                if !pre
                then
                  preemptor(None -> ((Some(board) -> (i -> None)) -> true))

                board match
                  case (cycle @ Cycle(_, Some(path))) =>
                    if flg
                    then
                      fos = FileOutputStream(path.toString)
                      oos = ObjectOutputStream(fos)
                      save(oos, tries, finds, solutions, number, None, pad)
                      !flg
                      throw ComputationSuspended
                    else
                      cycle.image = None
                  case _ =>

              thaw.get

          if flg then
            solve match
              case Some(true) =>
                cacheOps.put(cacheKey, solutions)
              case _ =>

        catch
          case ComputationSuspended =>
            tryc(oos.close)
            tryc(fos.close)
          case ResumeNotImplemented | SuspendNotSupported =>

        finally
          iterator(None)
          end()

        spindle.busywaitUntil(0L)(1L)

        spindle.decrementAndGet // bis

        Behaviors.stopped
      }
    )

    solver ! (
      cbs match
        case it: Callbacks => it.spindle
        case _ => AtomicLong(0)
    )

    cbs.iterationCallback(tag, nest, uuid, ctx, solver)


abstract trait `Queens*`[At]
    extends Queens[At]
    with dimensions.third.`Queens*`[At, Iteration]:

  inline protected def `solve*`[R](pit: ParallelIterable[?],
                                   wrap: ((Long, R)) => Unit = { (_: (Long, R)) => })(
                                   block: Item[Solution] => Option[R])(using
                                   M: Long,
                                   start: At,
                                   iteration: Iteration): Unit =
    val Iteration(board, tag, _, pre, run, flg, end, it) = iteration
    `solve*`[R](wrap)(block)(using M, start, Iteration(board, tag, pre, run, flg, { () => tryc { pit(); end() } }, it))

  final protected def `foreach*`(block: Item[Solution] => Unit)(using
                                 Long, At, Iteration): Iterable[Unit] =
    val `block*` = `apply*`(block)
    val r = new ParallelIterable[Unit]()
    `solve*`[Unit](r)(`block*`)
    Nil

  final protected def `map*`[R](block: Item[Solution] => R)(using
                                Long, At, Iteration): Iterable[R] =
    val `block*` = `apply*`(block)
    val r = new ParallelIterable[R]()
    val wrap: ((Long, R)) => Unit = { case (n, it) => r.addOne(n -> Some(it)) }
    `solve*`(r, wrap)(`block*`)
    r

  final protected def `flatMap*`[R](block: Item[Solution] => Iterable[R])(using
                                    Long, At, Iteration): Iterable[R] =
    val `block*` = `apply*`(block)
    val r = new ParallelIterable[R]()
    `solve*`(r, r.addOne _)(`block*`)
    r


package bis:

  import base.CacheOps

  abstract trait Queens[At]
      extends dimensions.third.bis.Queens[At]
      with dimensions.third.first.actors.Queens[At]:

    override protected val cacheOps: CacheOps =
      CacheOps.WithoutCache
