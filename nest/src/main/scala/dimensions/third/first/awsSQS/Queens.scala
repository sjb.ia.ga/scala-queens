package queens
package dimensions.third.first
package awsSQS

import java.util.UUID
import java.io.{ FileOutputStream, ObjectOutputStream }

import java.util.concurrent.atomic.AtomicLong

import com.amazonaws.services.sqs.model.SendMessageRequest

import scala.Function.const

import scala.collection.mutable.{ ListBuffer => MutableList }
import scala.collection.JavaConverters.asJava

import base.Board

import common.bis.Cycle
import common.{ Counter, Flag, :- }
import common.monad.Item

import Counter.given
import Macros.{ biSim, load, save, suspending }
import common.Macros.{ tryc, busywaitUntil }

import dimensions.fifth.cycle.{ Simulate, Progress }

import dimensions.Dimension.Model.awsSQS

import version.less.nest.Nest


abstract trait Queens[At]
    extends dimensions.third.Queens[At]
    with dimensions.second.`Queens*`[At]:

  final override val model = awsSQS

  protected def `solve*`[R](wrap: R => Unit = { (_: R) => })(
                            block: Item[Solution] => Option[R])(using
                            M: Long,
                            start: At,
                            iteration: Iteration): Unit =
    val Iteration(board, tag, _, pre, run, flg, end, sqsClient) = iteration

    assert(pre -> run.isRight)

    val dry = run.getOrElse(false)

    if M <= 0 || dry then
      return

    val nest = Nest(aspect, algorithm, model)
    val i @ (_, _, _, uuid) =
      board match
        case Cycle(((board, `nest`, `M`, uuid), _), _) => (board, nest, M, uuid)
        case _ => (board, nest, M, UUID.randomUUID)

    val major = s"${tag}${nest.aspect}${nest.algorithm}"
    val minor = uuid.toString.replace("-", "")
    var queueUrl: String = null

    val spindle = AtomicLong(0)

    val sim = run.swap.toOption

    var tries = new Counter(sim.map(const(true)).getOrElse(false))
    var finds = new Counter(sim.map(const(true)).getOrElse(false))

    val bis: ((Simulate, Progress)) => (Boolean, Any) => Option[Unit] =
      biSim(flg, tries, finds, _)

    var pad: Any = null
    var fos: FileOutputStream = null
    var oos: ObjectOutputStream = null

    var solutions = LazyList[Solution]()

    var (number, number2) = 0L -> 0L

    var thaw: Option[Either[Load, At]] = Some(Right(start))

    board match
      case Cycle(_, Some(path)) => load(path) match
        case (x, Some((n, t, f, s, l))) => pad = x
          thaw = l.map(Left(_)); tries = t; finds = f; solutions = s; number = n; number2 = n
        case (x, _) => pad = x
      case _ =>

    val preemptor: ((Option[Nest], (Progress, Boolean))) => Option[Unit] =
      { case (evt, (pro, ctr)) =>
          sim.map(_ -> pro).flatMap(bis(_)(ctr, evt))
      }

    given (Either[Save, Solution] => Boolean) = { s2 =>
      val its = s2.toOption

      if flg then
        if !pre then +(if its.isEmpty then tries else finds)

        its.map { it =>
          solutions = solutions :+ it
          number += 1
        }

        if its.isEmpty -> (tries -> pre)
        then
          val (s, n) = its.getOrElse(Nil) -> number

          if s ne Nil
          then
            spindle.incrementAndGet()

            try
              val sb = StringBuilder(s"$n")
              s.foreach { (row, col) => sb.append(s"\n$row\n$col") }
              val message = sb.toString

              val request = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(message)

              sqsClient.sendMessage(request)
            finally
              spindle.decrementAndGet()

          else if pre
          then
            val it = new Item[Solution](s, flg.slot, None)
            block(it).foreach(wrap)

          if !pre then
            val pro = None -> (i -> its.flatMap { _ => sim.map { _ => LazyList.from(solutions) } })
            preemptor((s :- nest) -> (pro -> its.isEmpty))

        (board, s2) match
          case (cycle @ Cycle(_, Some(path)), Left(fun)) if number2 < number =>
            if flg
            then
              fos = FileOutputStream(path.toString)
              oos = ObjectOutputStream(fos)
              save(oos, tries, finds, solutions, number, Some(fun), pad)
              !flg
            else
              cycle.image = None
          case _ =>

      flg
    }

    given Board = board

    try
      if thaw.nonEmpty && thaw.get.isRight
      then
        sqsClient.createQueue(s"$major$minor")

      queueUrl = sqsClient.getQueueUrl(s"$major$minor").getQueueUrl

      given Either[Load, At] =
        if thaw.isEmpty
        then
          Right(start)

        else
          if thaw.get.isRight
          then
            if !pre
            then
              preemptor(None -> ((Some(board) -> (i -> None)) -> true))

            board match
              case (cycle @ Cycle(_, Some(path))) =>
                if flg
                then
                  fos = FileOutputStream(path.toString)
                  oos = ObjectOutputStream(fos)
                  save(oos, tries, finds, solutions, number, None, pad)
                  !flg
                  throw ComputationSuspended
                else
                  cycle.image = None
              case _ =>

          thaw.get

      if flg then
        solve match
          case Some(true) =>
            cacheOps.put(cacheKey, solutions)
          case _ =>

    catch
      case ComputationSuspended =>
        tryc(oos.close)
        tryc(fos.close)
      case ResumeNotImplemented | SuspendNotSupported =>

      case _ =>

    finally
      if !suspending(oos eq null, board)
      then
        if number > 0
        then
          tryc {
            val vi = (board, tag, uuid, 0L, major, number.toString)
            val it = new Item[Solution](Nil, flg.slot, Some(vi))
            block(it).foreach(wrap)
          }
        tryc(sqsClient.deleteQueue(s"${major}_${minor}"))
        if !pre
        then
          number :: finds
          preemptor(None -> ((None -> (i -> None)) -> false))
      tryc(end())

    spindle.busywaitUntil(0L)(1L)

    spindle.decrementAndGet // bis


abstract trait `Queens*`[At]
    extends Queens[At]
    with dimensions.third.`Queens*`[At, Iteration]:

  final protected def `foreach*`(block: Item[Solution] => Unit)(using
                                 Long, At, Iteration): Iterable[Unit] =
    val `block*` = `apply*`(block)
    `solve*`[Unit]()(`block*`)
    Nil

  final protected def `map*`[R](block: Item[Solution] => R)(using
                                Long, At, Iteration): Iterable[R] =
    val `block*` = `apply*`(block)
    val r = MutableList[R]()
    `solve*`(r.addOne _)(`block*`)
    r

  final protected def `flatMap*`[R](block: Item[Solution] => Iterable[R])(using
                                    Long, At, Iteration): Iterable[R] =
    val `block*` = `apply*`(block)
    val r = MutableList[R]()
    `solve*`(r.addAll _)(`block*`)
    r


package bis:

  import base.CacheOps

  abstract trait Queens[At]
      extends dimensions.third.bis.Queens[At]
      with dimensions.third.first.awsSQS.Queens[At]:

    override protected val cacheOps: CacheOps =
      CacheOps.WithoutCache
