package queens
package dimensions.third

import common.monad.{ Item, `MonadWithFilter"` }
import dimensions.Dimension.Model

import conf.Conf


abstract trait Queens[At]
    extends dimensions.second.first.Queens[At]:

  val model: Model

  abstract override protected def tags: List[String] =
    model.toString() :: super.tags

  override def hashCode(): Int = (aspect, algorithm, model).##

  override def equals(any: Any) = any match
    case that: Queens[At] =>
      this.aspect == that.aspect &&
      this.algorithm == that.algorithm &&
      this.model == that.model
    case _ => false


abstract trait `Queens*`[At, It <: AnyVal]
    extends zeroth.Queens[Identity_, Iterable_, At, It]:

  override protected type * <: `Queens*`[At, It]


package bis:

  import dimensions.third.first.Iteration1

  abstract trait Queens[At]
      extends dimensions.third.Queens[At]:

    def apply[R](block: Item[Solution] ?=> R)(
                 using Iteration1, Simulate): Unit


package zeroth:

  abstract trait Queens[E[_], F[X[_], _], At, It <: AnyVal]
      extends dimensions.third.Queens[At]
      with common.monad.`0`.`MonadWithFilter"`[E, F, Solution, Long, It]:

    override protected type * <: Queens[E, F, At, It]

    def foreach(block: Item[Solution] => Unit)
               (using Long, It): F[E, Unit]

    def map[R](block: Item[Solution] => R)
              (using Long, It): F[E, R]

    def flatMap[R](block: Item[Solution] => F[E, R])
                  (using Long, It): F[E, R]
