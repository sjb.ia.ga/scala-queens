package queens
package dimensions.three
package recursive.tailcall
package kafka

import base.Board

import cache.CacheOpsDelegateTo

import common.monad.Item

import dimensions.second.third.first.recursive.tailcall.kafka.AbstractQueensClassicCb


class QueensClassicCb(
  override val validate: Validator,
  override protected val fs: Item[Solution] => Boolean*
) extends AbstractQueensClassicCb
    with dimensions.third.first.kafka.QueensClassicCb {

  override protected type * = QueensClassicCb

  override protected def `apply*`(s: Item[Solution] => Boolean*): * =
    new QueensClassicCb(validate, s*) {

      override protected val cacheOps = {
        val delegate = QueensClassicCb.this.cacheOps
        new CacheOpsDelegateTo(delegate.self)
      }

      QueensClassicCb.this.cacheOps.get(QueensClassicCb.this.cacheKey) match {
        case Some(solutions) =>
          cacheOps.put(cacheKey, solutions)
        case _ =>
      }

    }

}


package bis:

  import scala.Option.when

  import common.geom.x

  import common.monad.Item
  import Item.given

  import dimensions.third.first.Iteration1
  import dimensions.third.first.kafka.Iteration

  import dimensions.third.first.kafka.bis.Iteration2

  class QueensClassicCb(
    override val validate: Validator
  ) extends AbstractQueensClassicCb
      with dimensions.third.first.kafka.bis.Queens[Point]:

    override def apply[R](block: Item[Solution] ?=> R)(
                    using it: Iteration1, sim: Simulate): Unit =
      val `block*`: Item[Solution] => Option[R] = { it => when(it)(block(using it)) }

      val Iteration2(cycle, tag, _, flg, end, props) = it

      `solve*`[R]()(`block*`)(using cycle.io._1._3, 0 x 0, Iteration(cycle, tag, false, Left(sim), flg, end, props))
