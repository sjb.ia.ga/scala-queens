package queens
package dimensions.three
package recursive.extern
package futures

import base.Board

import cache.CacheOpsDelegateTo

import common.monad.Item

import common.stepper.factory.given

import dimensions.second.third.first.recursive.extern.futures.AbstractQueensStepperP


class QueensStepperP(
  override val validate: Validator,
  override protected val fs: Item[Solution] => Boolean*
) extends AbstractQueensStepperP
    with dimensions.third.first.futures.QueensStepperP {

  override protected type * = QueensStepperP

  override protected def `apply*`(s: Item[Solution] => Boolean*): * =
    new QueensStepperP(validate, s*) {

      override protected val cacheOps = {
        val delegate = QueensStepperP.this.cacheOps
        new CacheOpsDelegateTo(delegate.self)
      }

      QueensStepperP.this.cacheOps.get(QueensStepperP.this.cacheKey) match {
        case Some(solutions) =>
          cacheOps.put(cacheKey, solutions)
        case _ =>
      }

    }

}


package bis:

  import scala.Option.when

  import common.geom.Coord

  import common.stepper.StepperP

  import common.monad.Item
  import Item.given

  import dimensions.third.first.Iteration1
  import dimensions.third.first.futures.Iteration

  import dimensions.third.first.futures.bis.Iteration2

  class QueensStepperP(
    override val validate: Validator
  ) extends AbstractQueensStepperP
      with dimensions.third.first.futures.bis.Queens[Coord[StepperP]]:

    override def apply[R](block: Item[Solution] ?=> R)(
                    using iteration: Iteration1, sim: Simulate): Unit =
      val `block*`: Item[Solution] => Option[R] = { it => when(it)(block(using it)) }

      val `wrap*`: ((Long, R)) => Unit = { _ => }

      import dimensions.second.first.BoardStepper.start
      import common.stepper.factory.given

      implicit val Iteration2(cycle @ Board(size, _, seed), tag, _, flg, end, it) = iteration

      `solve*`[R](`wrap*`)(`block*`)(using cycle.io._1._3, start[StepperP](size), Iteration(cycle, tag, false, Left(sim), flg, end, it))
