package queens
package dimensions.three
package recursive.extern
package awsSQS

import base.Board

import cache.CacheOpsDelegateTo

import common.monad.Item

import common.stepper.factory.given

import dimensions.second.third.first.recursive.extern.awsSQS.AbstractQueensStepper1


class QueensStepper1(
  override val validate: Validator,
  override protected val fs: Item[Solution] => Boolean*
) extends AbstractQueensStepper1
    with dimensions.third.first.awsSQS.QueensStepper1 {

  override protected type * = QueensStepper1

  override protected def `apply*`(s: Item[Solution] => Boolean*): * =
    new QueensStepper1(validate, s*) {

      override protected val cacheOps = {
        val delegate = QueensStepper1.this.cacheOps
        new CacheOpsDelegateTo(delegate.self)
      }

      QueensStepper1.this.cacheOps.get(QueensStepper1.this.cacheKey) match {
        case Some(solutions) =>
          cacheOps.put(cacheKey, solutions)
        case _ =>
      }

    }

}


package bis:

  import scala.Option.when

  import common.geom.Coord

  import common.stepper.Stepper1

  import common.monad.Item
  import Item.given

  import dimensions.third.first.Iteration1
  import dimensions.third.first.awsSQS.Iteration

  import dimensions.third.first.awsSQS.bis.Iteration2

  class QueensStepper1(
    override val validate: Validator
  ) extends AbstractQueensStepper1
      with dimensions.third.first.awsSQS.bis.Queens[Coord[Stepper1]]:

    override def apply[R](block: Item[Solution] ?=> R)(
                    using it: Iteration1, sim: Simulate): Unit =
      val `block*`: Item[Solution] => Option[R] = { it => when(it)(block(using it)) }

      import dimensions.second.first.BoardStepper.start
      import common.stepper.factory.given

      implicit val Iteration2(cycle @ Board(size, _, seed), tag, _, flg, end, props) = it

      `solve*`[R]()(`block*`)(using cycle.io._1._3, start[Stepper1](size), Iteration(cycle, tag, false, Left(sim), flg, end, props))
