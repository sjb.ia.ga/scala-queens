package queens
package dimensions.three
package iterative
package awsSQS

import base.Board

import cache.CacheOpsDelegateTo

import common.monad.Item

import dimensions.second.third.first.iterative.awsSQS.AbstractQueensClassic


class QueensClassic(
  override val validate: Validator,
  override protected val fs: Item[Solution] => Boolean*
) extends AbstractQueensClassic
    with dimensions.third.first.awsSQS.QueensClassic {

  override protected type * = QueensClassic

  override protected def `apply*`(s: Item[Solution] => Boolean*): * =
    new QueensClassic(validate, s*) {

      override protected val cacheOps = {
        val delegate = QueensClassic.this.cacheOps
        new CacheOpsDelegateTo(delegate.self)
      }

      QueensClassic.this.cacheOps.get(QueensClassic.this.cacheKey) match {
        case Some(solutions) =>
          cacheOps.put(cacheKey, solutions)
        case _ =>
      }

    }

}


package bis:

  import scala.Option.when

  import common.geom.x

  import common.monad.Item
  import Item.given

  import dimensions.third.first.Iteration1
  import dimensions.third.first.awsSQS.Iteration

  import dimensions.third.first.awsSQS.bis.Iteration2

  class QueensClassic(
    override val validate: Validator
  ) extends AbstractQueensClassic
      with dimensions.third.first.awsSQS.bis.Queens[Point]:

    override def apply[R](block: Item[Solution] ?=> R)(
                    using it: Iteration1, sim: Simulate): Unit =
      val `block*`: Item[Solution] => Option[R] = { it => when(it)(block(using it)) }

      val Iteration2(cycle, tag, _, flg, end, props) = it

      `solve*`[R]()(`block*`)(using cycle.io._1._3, 0 x 0, Iteration(cycle, tag, false, Left(sim), flg, end, props))
