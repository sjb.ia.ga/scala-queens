package queens
package dimensions.three
package iterative
package futures

import base.Board

import cache.CacheOpsDelegateTo

import common.monad.Item

import common.stepper.dup.factory.given

import dimensions.second.third.first.iterative.futures.AbstractQueensStepper1


class QueensStepper1(
  override val validate: Validator,
  override protected val fs: Item[Solution] => Boolean*
) extends AbstractQueensStepper1
    with dimensions.third.first.futures.`*`.`QueensStepper1*` {

  override protected type * = QueensStepper1

  override protected def `apply*`(s: Item[Solution] => Boolean*): * =
    new QueensStepper1(validate, s*) {

      override protected val cacheOps = {
        val delegate = QueensStepper1.this.cacheOps
        new CacheOpsDelegateTo(delegate.self)
      }

      QueensStepper1.this.cacheOps.get(QueensStepper1.this.cacheKey) match {
        case Some(solutions) =>
          cacheOps.put(cacheKey, solutions)
        case _ =>
      }

    }

}


package bis:

  import scala.Option.when

  import common.geom.Coord

  import common.stepper.dup.{ Stepper1Dup => `Stepper1*` }

  import common.monad.Item
  import Item.given

  import dimensions.third.first.Iteration1
  import dimensions.third.first.futures.Iteration

  import dimensions.third.first.futures.bis.Iteration2

  class QueensStepper1(
    override val validate: Validator
  ) extends AbstractQueensStepper1
      with dimensions.third.first.futures.bis.Queens[Coord[`Stepper1*`]]:

    override def apply[R](block: Item[Solution] ?=> R)(
                    using iteration: Iteration1, sim: Simulate): Unit =
      val `block*`: Item[Solution] => Option[R] = { it => when(it)(block(using it)) }

      val `wrap*`: ((Long, R)) => Unit = { _ => }

      import dimensions.second.first.`*`.`BoardStepper*`.`start*`
      import common.stepper.dup.factory.given

      implicit val Iteration2(cycle @ Board(size, _, seed), tag, _, flg, end, it) = iteration

      `solve*`[R](`wrap*`)(`block*`)(using cycle.io._1._3, `start*`[`Stepper1*`](size), Iteration(cycle, tag, false, Left(sim), flg, end, it))
