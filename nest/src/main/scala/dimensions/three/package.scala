package queens
package dimensions.three

package iterative {

  package object actors {

    type Iteration = dimensions.third.first.actors.Iteration

  }

  package object futures {

    type Iteration = dimensions.third.first.futures.Iteration

  }

  package object flow {

    type Iteration = dimensions.third.first.flow.Iteration

  }

}

package recursive.native {

  package object actors {

    type Iteration = dimensions.third.first.actors.Iteration

  }

  package object futures {

    type Iteration = dimensions.third.first.futures.Iteration

  }

  package object flow {

    type Iteration = dimensions.third.first.flow.Iteration

  }

}

package recursive.extern {

  package object actors {

    type Iteration = dimensions.third.first.actors.Iteration

  }

  package object futures {

    type Iteration = dimensions.third.first.futures.Iteration

  }

  package object flow {

    type Iteration = dimensions.third.first.flow.Iteration

  }

}
