package queens
package dimensions.three
package breed

import dimensions.third.Queens

import base.breed.Breed


abstract trait Hatch[Q <: Queens[?]]
    extends Breed
    with Function0[Q]


object Hatch:

  given [Q <: Queens[?]]: Conversion[Hatch[?], Hatch[Q]] = _.asInstanceOf[Hatch[Q]]

  object Flavor
      extends Enumeration:

    val build = Value("build")

    val config = Value("config")

    val `di-*` = Value("*")

    val `di-guice` = Value("guice")

    val `di-macwire` = Value("macwire")

    def apply(flavor: String): Option[Flavor.Value] =
      List(build, config, `di-guice`, `di-macwire`)
        .find(_.toString == flavor)

    def apply(flavor: Flavor.Value): Boolean =
      values.contains(flavor)

    def apply(): Flavor.Value =
      val config = sys.BooleanProp
        .keyExists(_root_.queens.main.Properties.DependencyInjection.config)
        .value

      val build = sys.BooleanProp
        .keyExists(_root_.queens.main.Properties.DependencyInjection.build)
        .value

      val guice = sys.BooleanProp
        .keyExists(_root_.queens.main.Properties.DependencyInjection.guice)
        .value

      val macwire = sys.BooleanProp
        .keyExists(_root_.queens.main.Properties.DependencyInjection.macwire)
        .value

      if guice && !macwire then Flavor.`di-guice`
      else if macwire then Flavor.`di-macwire`
      else if config && !build then Flavor.config
      else Flavor.build

