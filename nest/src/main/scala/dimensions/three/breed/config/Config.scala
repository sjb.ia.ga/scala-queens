package queens
package dimensions.three
package breed
package config

import dimensions.Dimension.{ Monad, Aspect, Algorithm, Model }

import dimensions.third.Queens


abstract trait Config:

  abstract trait ByModel:

    abstract trait OfAlgorithm:

      abstract trait ToAspect:

        def breed[Q <: Queens[?]]: Hatch[Q]

        abstract trait AtMonad:

          def breed[Q <: Queens[?]]: Hatch[Q]

        def atMonad(monad: Monad): AtMonad = ???

      def toAspect(aspect: Aspect): ToAspect

    def ofAlgorithm(algorithm: Algorithm): OfAlgorithm

  def byModel(model: Model): ByModel


package bis:

  import dimensions.third.bis.{ Queens => Queens2 }

  abstract trait Config:

    abstract trait ByModel:

      abstract trait OfAlgorithm:

        abstract trait ToAspect:

          def breed[Q <: Queens2[?]]: Hatch[Q]

        def toAspect(aspect: Aspect): ToAspect

      def ofAlgorithm(algorithm: Algorithm): OfAlgorithm

    def byModel(model: Model): ByModel
