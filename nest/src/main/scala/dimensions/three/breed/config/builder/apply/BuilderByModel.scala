package queens
package dimensions.three
package breed
package config
package builder
package apply


abstract trait BuilderByModel:

  lazy val from: Builder => Builder#ByModel
