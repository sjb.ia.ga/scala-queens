package queens
package dimensions.three
package breed
package config
package builder
package apply

import dimensions.Dimension.{ Monad, Aspect, Algorithm, Model }

import dimensions.third.Queens


abstract trait Builder:

  abstract trait ByModel:

    abstract trait WithAlgorithm:

      abstract trait AndAspect:

        def breed[Q <: Queens[?]]: Hatch[Q]

        abstract trait ButMonad:

          def breed[Q <: Queens[?]]: Hatch[Q]

        def butMonad(monad: Monad): ButMonad = ???

      def andAspect(aspect: Aspect): AndAspect

    def withAlgorithm(algorithm: Algorithm): WithAlgorithm

  def byModel(model: Model): Builder#ByModel
