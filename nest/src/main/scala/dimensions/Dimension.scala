package queens
package dimensions

import scala.collection.Set


sealed abstract trait Dimension

object Dimension {

  type Scalar = Enumeration#Value

  type Projection = Seq[Scalar]

  type Axes = Set[Dimension]


  // 0th

  abstract sealed trait Monad

  case object Monad extends Enumeration with Dimension:

    abstract sealed class MonadVal(name: String)
        extends super.Val(name) { override def toString = s"$name" }

    object Effect:

      sealed abstract trait EffectIO

      case object MonixIterantCoeval
          extends EffectIO { override def toString = "MonixIterantCoeval" }

      case object MonixIterantTask
          extends EffectIO { override def toString = "MonixIterantTask" }

      case object FS2StreamIO
          extends EffectIO { override def toString = "FS2StreamIO" }

    import Effect._

    def effect$(io: String) = s"effect$io"
    def effect$(io: EffectIO): String = effect$(io.toString)

    final case class Effect(io: EffectIO)
        extends MonadVal(effect$(io))
        with Monad

    case object Iterable
        extends MonadVal("Iterable")
        with Monad

    val monixIterantCoeval: Monad = Effect(MonixIterantCoeval)

    val monixIterantTask: Monad = Effect(MonixIterantTask)

    val fs2StreamIO: Monad = Effect(FS2StreamIO)

    val iterable: Monad = Iterable

    implicit def valueToMonad(v: Scalar): Monad = v.asInstanceOf[Monad]

    def apply(monad: String): Option[Monad] =
      List(monixIterantCoeval, monixIterantTask, fs2StreamIO, iterable)
        .find(_.toString == monad)


  // 1st

  abstract sealed trait Aspect

  case object Aspect extends Enumeration with Dimension {

    abstract sealed class AspectVal(name: String)
        extends super.Val(name) { override def toString = s"$name" }

    object Classic {

      sealed abstract trait ClassicLook

      case object Straight
          extends ClassicLook { override def toString = "Straight" }
      case object Callback
          extends ClassicLook { override def toString = "Callback" }

    }

    import Classic._

    def classic$(look: String) = s"classic$look"
    def classic$(look: ClassicLook): String = classic$(look.toString)

    final case class Classic(look: ClassicLook)
        extends AspectVal(classic$(look))
        with Aspect

    object Stepper {

      sealed abstract trait StepperNext

      case object OneIncrement
          extends StepperNext { override def toString = "1" }
      case object Permutations
          extends StepperNext { override def toString = "P" }

    }

    import Stepper._

    def stepper$(next: String) = s"stepper$next"

    def stepper$(next: StepperNext): String = stepper$(next.toString)

    final case class Stepper(next: StepperNext)
        extends AspectVal(stepper$(next))
        with Aspect

    val classic: Aspect = Classic(Straight)

    val classicCb: Aspect = Classic(Callback)

    val stepper1: Aspect = Stepper(OneIncrement)

    val stepperP: Aspect = Stepper(Permutations)

    implicit def valueToAspect(v: Scalar): Aspect = v.asInstanceOf[Aspect]

    def apply(aspect: String): Option[Aspect] =
      List(classic, classicCb, stepper1, stepperP)
        .find(_.toString == aspect)

  }


  // 2nd

  abstract sealed trait Algorithm

  case object Algorithm extends Enumeration with Dimension {

    abstract sealed class AlgorithmVal(name: String)
        extends super.Val(name) { override def toString = s"$name" }

    object Recursive {

      sealed abstract trait RecursiveNature

      case object Native
          extends RecursiveNature { override def toString = "Native" }
      case object Extern
          extends RecursiveNature { override def toString = "Extern" }
      case object TailCall
          extends RecursiveNature { override def toString = "TailCall" }
      case object CatsEval
          extends RecursiveNature { override def toString = "CatsEval" }
      case object Continuation
          extends RecursiveNature { override def toString = "Continuation" }
      case object Closure
          extends RecursiveNature { override def toString = "Closure" }
      case object Trampoline
          extends RecursiveNature { override def toString = "Trampoline" }

    }

    import Recursive._

    def recursive$(nature: String) = s"recursive$nature"
    def recursive$(nature: RecursiveNature): String = recursive$(nature.toString)

    final case class Recursive(nature: RecursiveNature)
        extends AlgorithmVal(recursive$(nature))
        with Algorithm

    case object Iterative
        extends AlgorithmVal("iterative")
        with Algorithm

    val iterative: Algorithm = Iterative

    val native: Algorithm = Recursive(Native)

    val extern: Algorithm = Recursive(Extern)

    val tailcall: Algorithm = Recursive(TailCall)

    val catseval: Algorithm = Recursive(CatsEval)

    val continuation: Algorithm = Recursive(Continuation)

    val closure: Algorithm = Recursive(Closure)

    val trampoline: Algorithm = Recursive(Trampoline)

    implicit def valueToAlgorithm(v: Scalar): Algorithm = v.asInstanceOf[Algorithm]

    def apply(algorithm: String): Option[Algorithm] =
      List(iterative, native, extern, tailcall, catseval, continuation, closure, trampoline)
        .find(_.toString == algorithm)

  }


  // 3rd

  abstract sealed trait Model

  case object Model extends Enumeration with Dimension {

    abstract sealed class ModelVal(name: String)
        extends super.Val(name) { override def toString = s"$name" }

    object Parallel {

      sealed abstract trait ParallelParadigm

      case object Actors
          extends ParallelParadigm { override def toString = "Actors" }
      case object Futures
          extends ParallelParadigm { override def toString = "Futures" }

    }

    object Messaging {

      sealed abstract trait MessagingSystem

      case object AWSSQS
          extends MessagingSystem { override def toString = "AWSSQS" }

      case object Kafka
          extends MessagingSystem { override def toString = "Kafka" }

      case object RabbitMQ
          extends MessagingSystem { override def toString = "RabbitMQ" }

    }

    import Parallel._

    def parallel$(paradigm: String) = s"parallel$paradigm"
    def parallel$(paradigm: ParallelParadigm): String = parallel$(paradigm.toString())

    final case class Parallel(paradigm: ParallelParadigm)
        extends ModelVal(parallel$(s"$paradigm"))
        with Model

    import Messaging._

    def messaging$(system: String) = s"messaging$system"
    def messaging$(system: MessagingSystem): String = messaging$(system.toString())

    final case class Messaging(system: MessagingSystem)
        extends ModelVal(messaging$(s"$system"))
        with Model

    case object Flow
        extends ModelVal("flow")
        with Model

    val actors: Model = Parallel(Actors)

    val futures: Model = Parallel(Futures)

    val kafka: Model = Messaging(Kafka)

    val rabbitMQ: Model = Messaging(RabbitMQ)

    val awsSQS: Model = Messaging(AWSSQS)

    val flow: Model = Flow

    implicit def valueToModel(v: Scalar): Model = v.asInstanceOf[Model]

    def apply(model: String): Option[Model] =
      List(actors, futures, kafka, rabbitMQ, flow)
        .find(_.toString == model)

  }

////////////////////////////////////////////////////////////////////////////////

  // 4th

  abstract sealed trait Interface

  case object Interface extends Enumeration with Dimension {

    abstract sealed class InterfaceVal(name: String)
        extends super.Val(name) { override def toString = s"$name" }

    object Application {

      sealed abstract class ApplicationSpecific
        extends HasPlainJAVA("App")

      object Web {

        sealed abstract trait WebClient

        object Client {

          sealed abstract trait Serialization {
            val classes: Serialization.Classes
          }

          object Serialization {

            sealed abstract trait Classes

            sealed abstract trait Pojo
                extends Classes

            sealed abstract trait `4s`
                extends Classes

            object `4s` {

              case object Circe
                  extends `4s` { override def toString = "Circe" }

              case object `Circe and uPickle`
                  extends `4s` { override def toString = "Circe and uPickle" }

            }

            object Pojo {

              case object Gson
                  extends Pojo { override def toString = "Gson" }

              case object Moshi
                  extends Pojo { override def toString = "Moshi" }

              case object Jackson
                  extends Pojo { override def toString = "Jackson" }

            }

          }

          def serialization$(classes: String) = s"[${classes}]Serialization"
          def serialization$(classes: Serialization.Classes): String = serialization$(classes.toString)

          final case class http4s(override val classes: Serialization.Classes)
              extends Serialization { override def toString = s"http4s${serialization$(classes)}" }

          final case class Micronaut(override val classes: Serialization.Classes)
              extends Serialization { override def toString = s"Micronaut${serialization$(classes)}" }

          final case class OkHttp(override val classes: Serialization.Classes)
              extends Serialization { override def toString = s"OkHttp${serialization$(classes)}" }

          final case class Retrofit(override val classes: Serialization.Classes)
              extends Serialization { override def toString = s"Retrofit2${serialization$(classes)}" }

          final case class Spring(override val classes: Serialization.Classes)
              extends Serialization { override def toString = s"Spring${serialization$(classes)}" }

          final case class sttp(override val classes: Serialization.Classes)
              extends Serialization { override def toString = s"sttp${serialization$(classes)}" }

          final case class Vertx(override val classes: Serialization.Classes)
              extends Serialization { override def toString = s"Vertx${serialization$(classes)}" }

          final case class WebFlux(override val classes: Serialization.Classes)
              extends Serialization { override def toString = s"WebFlux${serialization$(classes)}" }

        }

        def client$(serialization: String) = s"${serialization}Client"
        def client$(serialization: Client.Serialization): String = client$(serialization.toString)

        final case class Client(library: Client.Serialization)
            extends WebClient { override def toString = client$(s"$library") }

      }

      import Web._

      def web$(client: String) = s"Web${client}"
      def web$(client: WebClient): String = web$(client.toString)

      final case class Web(client: WebClient, override val plainJAVA: Boolean)
          extends ApplicationSpecific { override def toString = web$(client) + super.toString }

    }

    object Program {

      sealed abstract trait ProgramStyle

      case object Cycle
          extends ProgramStyle { override def toString = "cycle" }

      case object Monad
          extends ProgramStyle { override def toString = "monad" }

      case object Simple
          extends ProgramStyle { override def toString = "simple" }

    }


    import Application._
    import Application.Web._
    import Application.Web.Client._

    def app$(specific: String) = s"${specific}Application"
    def app$(specific: ApplicationSpecific): String = app$(specific.toString)

    final case class Application(specific: ApplicationSpecific)
        extends InterfaceVal(app$(specific))
        with Interface

    val http4sClient: Interface = Application(Web(Client(http4s(Serialization.`4s`.Circe)), false))
    val micronautPlainJAVAClient: Interface = Application(Web(Client(Micronaut(Serialization.Pojo.Jackson)), true))
    val okhttpClient: Interface = Application(Web(Client(OkHttp(Serialization.Pojo.Moshi)), false))
    val okhttpPlainJAVAClient: Interface = Application(Web(Client(OkHttp(Serialization.Pojo.Moshi)), true))
    val retrofitClient: Interface = Application(Web(Client(Retrofit(Serialization.Pojo.Gson)), false))
    val retrofitPlainJAVAClient: Interface = Application(Web(Client(Retrofit(Serialization.Pojo.Gson)), true))
    val springClient: Interface = Application(Web(Client(Spring(Serialization.Pojo.Jackson)), false))
    val springPlainJAVAClient: Interface = Application(Web(Client(Spring(Serialization.Pojo.Jackson)), true))
    val sttpClient: Interface = Application(Web(Client(sttp(Serialization.`4s`.`Circe and uPickle`)), false))
    val vertxPlainJAVAClient: Interface = Application(Web(Client(Vertx(Serialization.Pojo.Jackson)), true))
    val webfluxClient: Interface = Application(Web(Client(WebFlux(Serialization.Pojo.Jackson)), false))
    val webfluxPlainJAVAClient: Interface = Application(Web(Client(WebFlux(Serialization.Pojo.Jackson)), false))


    import Program._

    def program$(style: String) = s"${style}Program"
    def program$(style: ProgramStyle): String = program$(style.toString)

    final case class Program(style: ProgramStyle)
        extends InterfaceVal(program$(style))
        with Interface

    val cycleProgram: Interface = Program(Cycle)

    val monadProgram: Interface = Program(Program.Monad)

    val simpleProgram: Interface = Program(Simple)


    implicit def valueToInterface(v: Scalar): Interface = v.asInstanceOf[Interface]

    def apply(interface: String): Option[Interface] =
      List(monadProgram,
           simpleProgram,
           http4sClient,
           micronautPlainJAVAClient,
           okhttpClient,
           okhttpPlainJAVAClient,
           retrofitClient,
           retrofitPlainJAVAClient,
           springClient,
           springPlainJAVAClient,
           sttpClient,
           vertxPlainJAVAClient,
           webfluxClient,
           webfluxPlainJAVAClient
      ).find(_.toString == interface)

  }

////////////////////////////////////////////////////////////////////////////////

  // 5th

  abstract sealed trait Use

  case object Use extends Enumeration with Dimension {

    abstract sealed class UseVal(name: String)
        extends super.Val(name) { override def toString = s"$name" }

    object Output {

      sealed abstract trait OutputKind

      object Database {

        sealed abstract trait DatabaseStorage {
          val plainSQL: Boolean = false
        }

        // Slick

        object Slick {

          sealed abstract trait Profile

          object Profile {

            case object H2
                extends Profile { override def toString = "H2" }

            case object SQLite
                extends Profile { override def toString = "SQLite" }

          }

        }

        def slick$(profile: String, plainSQL: Boolean): String = s"$profile${if plainSQL then "PlainSQL" else ""}Slick"
        def slick$(profile: Slick.Profile, plainSQL: Boolean): String = slick$(profile.toString, plainSQL)

        final case class Slick(profile: Slick.Profile, override val plainSQL: Boolean)
            extends DatabaseStorage { override def toString = slick$(profile, plainSQL) }


        // ScalikeJDBC

        object Scalike {

          sealed abstract trait Driver

          object Driver {

            case object H2
                extends Driver { override def toString = "H2" }

            case object SQLite
                extends Driver { override def toString = "SQLite" }

          }

        }

        def scalike$(driver: String, plainSQL: Boolean): String = s"$driver${if plainSQL then "PlainSQL" else ""}Scalike"
        def scalike$(driver: Scalike.Driver, plainSQL: Boolean): String = scalike$(driver.toString, plainSQL)

        final case class Scalike(driver: Scalike.Driver, override val plainSQL: Boolean)
            extends DatabaseStorage { override def toString = scalike$(driver, plainSQL) }

      }

      case object Buffered
          extends OutputKind { override def toString = "buffered" }

      case object Console
          extends OutputKind { override def toString = "console" }

      def database$(storage: String) = s"${storage}Database"
      def database$(storage: Database.DatabaseStorage): String = database$(storage.toString)

      final case class Database(storage: Database.DatabaseStorage)
          extends OutputKind { override def toString = database$(s"$storage") }

    }


    object Web {

      sealed abstract class WebServer
          extends HasPlainJAVA("WebServer")

      object Microservices {

        sealed abstract trait uFramework

      }

      import Microservices._

      def microservices$(uFramework: String) = s"${uFramework}Microservices"
      def microservices$(uFramework: uFramework): String = microservices$(uFramework.toString)

      final case class Microservices(uFramework: uFramework, override val plainJAVA: Boolean)
          extends WebServer { override def toString = microservices$(uFramework) + super.toString }

      object Reactive {

        sealed abstract trait Toolkit

        case object AkkaHTTP
            extends Toolkit { override def toString = "Akka HTTP" }

        case object http4s
            extends Toolkit { override def toString = "http4s" }

        case object Vertx
            extends Toolkit { override def toString = "Vert.x" }

        case object WebFlux
            extends Toolkit { override def toString = "Spring WebFlux" }

      }

      import Reactive._

      def reactive$(toolkit: String) = s"${toolkit}Reactive"
      def reactive$(toolkit: Toolkit): String = reactive$(toolkit.toString)

      final case class Reactive(toolkit: Toolkit, override val plainJAVA: Boolean)
          extends WebServer { override def toString: String = reactive$(toolkit) + super.toString }

      object Servlet {

        sealed abstract trait ServletTechnology {
          val webXML: Boolean = false
          override def toString = s" ${if webXML then "web.xml" else "annotation"}-based"
        }

        object Container {

          sealed abstract trait Framework

          case object Dropwizard
            extends Framework { override def toString = "Dropwizard" }

          case object JavaEE
              extends Framework { override def toString = "JavaEE" }

          case object Micronaut
            extends Framework { override def toString = "Micronaut" }

          case object SpringBoot
            extends Framework { override def toString = "SpringBoot" }

        }

        import Container._

        def container$(framework: String) = s"${framework}Container"
        def container$(framework: Framework): String = container$(framework.toString)

        final case class Container(framework: Framework, override val webXML: Boolean)
            extends ServletTechnology { override def toString: String = container$(framework) + super.toString }

      }

      import Servlet._

      def servlet$(container: String) = s"${container}Servlet"
      def servlet$(container: Container): String = servlet$(container.toString)

      final case class Servlet(container: Container, override val plainJAVA: Boolean)
          extends WebServer { override def toString: String = servlet$(container) + super.toString }

    }


    import Output._
    import Output.Database._

    def output$(kind: String) = s"${kind}Output"
    def output$(kind: OutputKind): String = output$(kind.toString)

    final case class Output(kind: OutputKind)
        extends UseVal(output$(s"$kind"))
        with Use

    val consoleOutput: Use = Output(Console)

    val bufferedOutput: Use = Output(Buffered)

    val H2PlainSQLSlickDatabaseOutput: Use = Output(Database(Slick(Slick.Profile.H2, true)))
    val H2SlickDatabaseOutput: Use = Output(Database(Slick(Slick.Profile.H2, false)))

    val SQLitePlainSQLSlickDatabaseOutput: Use = Output(Database(Slick(Slick.Profile.SQLite, true)))
    val SQLiteSlickDatabaseOutput: Use = Output(Database(Slick(Slick.Profile.SQLite, false)))

    val H2PlainSQLScalikeDatabaseOutput: Use = Output(Database(Scalike(Scalike.Driver.H2, true)))
    val H2ScalikeDatabaseOutput: Use = Output(Database(Scalike(Scalike.Driver.H2, false)))

    val SQLitePlainSQLScalikeDatabaseOutput: Use = Output(Database(Scalike(Scalike.Driver.SQLite, true)))
    val SQLiteScalikeDatabaseOutput: Use = Output(Database(Scalike(Scalike.Driver.SQLite, false)))


    import Web._
    import Web.Reactive._
    import Web.Servlet._
    import Web.Servlet.Container._

    def web$(server: String) = s"${server}Web"
    def web$(server: WebServer): String = web$(server.toString)

    final case class Web(server: WebServer)
        extends UseVal(web$(s"$server"))
        with Use

    val appAkkaHTTP: Use = Web(Reactive(AkkaHTTP, false))

    val apphttp4s: Use = Web(Reactive(http4s, false))

    val appDropwizardHasPlainJAVA: Use = Web(Servlet(Container(Dropwizard, false), true))

    val appJavaEE: Use = Web(Servlet(Container(JavaEE, false), false))

    val appMicronaut: Use = Web(Servlet(Container(Micronaut, false), false))

    val appMicronautHasPlainJAVA: Use = Web(Servlet(Container(Micronaut, false), true))

    val appSpringBoot: Use = Web(Servlet(Container(SpringBoot, false), false))

    val appSpringBootHasPlainJAVA: Use = Web(Servlet(Container(SpringBoot, false), true))

    val appSpringWebFlux: Use = Web(Reactive(WebFlux, false))

    val appVertxHasPlainJAVA: Use = Web(Reactive(Vertx, true))

    implicit def valueToUse(v: Scalar): Use = v.asInstanceOf[Use]

    def apply(use: String): Option[Use] =
      List(
        consoleOutput,
        bufferedOutput,
        H2PlainSQLSlickDatabaseOutput, H2SlickDatabaseOutput,
        SQLitePlainSQLSlickDatabaseOutput, SQLiteSlickDatabaseOutput,
        H2PlainSQLScalikeDatabaseOutput, H2ScalikeDatabaseOutput,
        SQLitePlainSQLScalikeDatabaseOutput, SQLiteScalikeDatabaseOutput,
        appAkkaHTTP,
        apphttp4s,
        appDropwizardHasPlainJAVA,
        appJavaEE,
        appMicronaut,
        appMicronautHasPlainJAVA,
        appSpringBoot,
        appSpringBootHasPlainJAVA,
        appSpringWebFlux,
        appVertxHasPlainJAVA
      ).find(_.toString == use)

  }

  def apply(ds: Dimension*): Axes =
    scala.collection.mutable.LinkedHashSet(ds*)

}

abstract trait HasPlainJAVA(name: String) {
  val plainJAVA: Boolean = false
  override def toString = s"${if plainJAVA then "Java" else "Scala"}$name"
}
