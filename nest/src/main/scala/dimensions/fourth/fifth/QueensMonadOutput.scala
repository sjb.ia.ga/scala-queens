package queens
package dimensions.fourth.fifth

import dimensions.fourth.program.monad.QueensMonadProgramInterface
import dimensions.fifth.output.QueensOutputUseSolution

import conf.Conf


abstract trait QueensMonadOutput[
  P <: Conf
] extends QueensOutputUseSolution
    with QueensMonadProgramInterface[P]:

  /*
   * @param predicate: exposes the otherwise protected member
   *                   @code [[queens]], which can thus be captured
   */
  final def apply(predicate: Q => Boolean): Boolean = predicate(queens)

  override def toString(): String = "Monad Program " + super.toString
