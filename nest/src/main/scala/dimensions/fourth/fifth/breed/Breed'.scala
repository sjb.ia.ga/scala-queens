package queens
package dimensions.fourth.fifth
package breed

import java.lang.Long
import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import scala.Function.const

import common.Flag
import common.monad.{ Item, Grow }


abstract trait `Base.Breed *'"`[T](
  override protected val iterations: Seq[UUID]
)(using
  override protected val `iterations*`: Map[UUID, (Boolean, Flag)]
) extends `Breed *'"`[T]


abstract trait `Breed *'"`[T]
    extends Grow
    with common.Tagged:

  def `#`(t: Any): `Breed *'"`[T]

  import Item.given

  def apply[E](it: Item[T])(expr: (UUID, Long) ?=> E): Option[E] =
    `just get*`(it) {
      case false => const(None)
      case _ => {
        case _ if !it => None
        case (_, (_, _, uuid, n, _, _)) =>
          val r = expr(using uuid, n)
          r match
            case false => this stop it
            case _ =>
          Some(r)
      }
    }
