package queens
package dimensions.fourth.fifth
package breed

import java.lang.Long
import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import scala.Function.const

import conf.monad.Conf

import common.Flag

import common.`Tagged°`
import common.monad.{ Grow, `WithFilter'°` }


abstract trait `Base.Breed *'°`[
  P <: Conf
](using
  override protected val * : P
)(
  override protected val iterations: Seq[UUID]
)(using
  override protected val `iterations*`: Map[UUID, (Boolean, Flag)]
) extends `Breed *'°`[P] { this: `WithFilter'°`[P] => }


abstract trait `Breed *'°`[
  P <: Conf
](using
  override protected val * : P
) extends Grow
    with `Tagged°`[P] { this: `WithFilter'°`[P] =>

  import common.monad.Item.given

  def apply[E](it: *.Item)(expr: (UUID, Long) ?=> E): Option[E] =
    `just get*`(it) {
      case false => const(None)
      case _ => {
        case _ if !it => None
        case (_, (_, _, uuid, n, _, _)) =>
          val r = expr(using uuid, n)
          r match
            case false => this stop it
            case _ =>
          Some(r)
      }
    }

}
