package queens
package dimensions.fourth.third
package program
package simple
package flow

import dimensions.third.first.Iteration1

import dimensions.fourth.program.simple.QueensSimpleProgramInterface


abstract trait QueensSimpleProgramInterfaceByFlow
    extends QueensSimpleProgramInterface[Iteration1]:

  override def toString(): String = super.toString + " by Flow"
