package queens
package dimensions.fourth.third
package program
package simple
package futures

import dimensions.third.first.Iteration1

import dimensions.fourth.program.simple.QueensSimpleProgramInterface


abstract trait QueensSimpleProgramInterfaceByFutures
    extends QueensSimpleProgramInterface[Iteration1]:

  override def toString(): String = super.toString + " by Futures"
