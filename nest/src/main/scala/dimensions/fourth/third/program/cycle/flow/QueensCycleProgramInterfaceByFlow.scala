package queens
package dimensions.fourth.third
package program
package cycle
package flow

import dimensions.third.first.Iteration1

import dimensions.fourth.program.cycle.QueensCycleProgramInterface


abstract trait QueensCycleProgramInterfaceByFlow
    extends QueensCycleProgramInterface[Iteration1]:

  override def toString(): String = super.toString + " by Flow"
