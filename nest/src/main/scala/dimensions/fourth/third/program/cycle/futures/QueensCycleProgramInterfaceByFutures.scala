package queens
package dimensions.fourth.third
package program
package cycle
package futures

import dimensions.third.first.Iteration1

import dimensions.fourth.program.cycle.QueensCycleProgramInterface


abstract trait QueensCycleProgramInterfaceByFutures
    extends QueensCycleProgramInterface[Iteration1]:

  override def toString(): String = super.toString + " by Futures"
