package queens
package dimensions.fourth
package app

import scala.reflect.ClassTag


sealed abstract trait Parameter[T]:
  val value: Option[T]

  final def apply(): Option[(String, String)] = value
    .map(Parameter.name(getClass) -> _.toString)

  override def toString = value
    .map { Parameter.name(getClass)+"="+_ }
    .getOrElse("")


object Parameter:

  def name[P <: Parameter[?]](using ct: ClassTag[P]): String = name(ct.runtimeClass.asInstanceOf[Class[P]])
  private def name[P <: Parameter[?]](clasz: Class[P]): String = clasz.getSimpleName.toLowerCase

  import dimensions.three.breed.Hatch.Flavor

  object Board:

    final case class size(override val value: Option[Int] = Some(6))
        extends Parameter[Int]

    final case class empty(override val value: Option[Boolean] = Some(false))
        extends Parameter[Boolean]

    final case class seed(override val value: Option[Long] = None)
        extends Parameter[Long]

  object Solution:

    final case class max(override val value: Option[Long] = None)
        extends Parameter[Long]

  final case class flavor(override val value: Option[Flavor.Value] = Some(Flavor.build))
      extends Parameter[Flavor.Value]

  object Text:

    final case class square(override val value: Option[String] = None)
        extends Parameter[String]

    final case class piece(override val value: Option[String] = None)
        extends Parameter[String]

    final case class queen(override val value: Option[String] = None)
        extends Parameter[String]

  object Json:

    final case class circe(override val value: Option[Boolean] = None)
        extends Parameter[Boolean]

    final case class upickle(override val value: Option[Boolean] = None)
        extends Parameter[Boolean]

  object Cue:

    final case class timeout(override val value: Option[Int] = None)
        extends Parameter[Int]

  import scala.reflect.ClassTag

  sealed class FindBooleanParameter[P <: Parameter[Boolean]](
    clasz: Class[P],
    params: Parameter[?]*
  ):
    def apply[
      R
    ](cond: => Boolean = true
    )(block: P ?=> R): Option[R] = params
      .find(_.getClass eq clasz)
        match
          case Some(it: P) if cond && it.value.getOrElse(false) => Some(block(using it))
          case _ => None

  extension(params: Seq[Parameter[?]])
    def apply[P <: Parameter[Boolean]](using ClassTag[P]): FindBooleanParameter[P] =
      FindBooleanParameter[P](implicitly[ClassTag[P]].runtimeClass.asInstanceOf[Class[P]], params*)
