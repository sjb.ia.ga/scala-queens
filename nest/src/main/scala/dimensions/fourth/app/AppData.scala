package queens
package dimensions.fourth
package app

import scala.util.Try

import common.monad.Grow


/**
  * @see [[queens.axis.fourth.webapp.WebMethod]]
  */
abstract trait AppData[O, AD <: AppData[O, AD]]:
  val params: Seq[Parameter[?]]
  var result: Try[O] = Try(???)


object AppData:

  class ItemValue[O, R, AD <: AppData[O, AD]](_it: Iterable[(Int, AD, Try[O], O => R)] = Nil)
      extends Grow.ItemValue[Iterable[(Int, AD, Try[O], O => R)]](_it)
