package queens
package dimensions.fourth
package webapp

import common.Pipeline


/**
  * @see [[queens.axis.fourth.webapp.spring.`Spring Web Client`]]
  * @see [[queens.axis.fourth.webapp.spring.plainjava.Spring$Web$Client]]
  * @see [[queens.axis.fourth.webapp.retrofit.`Retrofit Web Client`]]
  * @see [[queens.axis.fourth.webapp.retrofit.plainjava.Retrofit$Web$Client]]
  * @see [[queens.axis.fourth.webapp.okhttp.`OkHttp Web Client`]]
  * @see [[queens.axis.fourth.webapp.okhttp.plainjava.OkHttp$Web$Client]]
  * @see [[queens.dimensions.fourth.webapp.plainjava.PlainJAVA$Web$Client]]
  */
abstract trait `Web Client`
    extends Pipeline[Unit]:

  override protected type `-|-` = `Web Client`
