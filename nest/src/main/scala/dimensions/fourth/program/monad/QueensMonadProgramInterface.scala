package queens
package dimensions.fourth
package program
package monad

import base.QueensMonad

import conf.Conf

import dimensions.third.`Queens*`


abstract trait QueensMonadProgramInterface[
  P <: Conf
] extends QueensMonad[P]
    with QueensInterface:

  protected type It <: AnyVal

  protected type Q <: `Queens*`[?, It]

  protected def queens: Q
