package queens
package dimensions.fourth
package program
package simple

import dimensions.three.breed.Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


abstract trait QueensSimpleProgramInterface[It <: AnyRef]
    extends QueensInterface:

  protected type T

  protected def apply(wildcard: Wildcard,
                      multiplier: Multiplier[Nest]
                    )(using
                      iteration: It
                    )(using Long, Flavor.Value): T

  override def toString(): String = "Simple Program"
