package queens
package dimensions.fourth
package program
package cycle

import dimensions.three.breed.Hatch.Flavor

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


abstract trait QueensCycleProgramInterface[It <: AnyRef]
    extends QueensInterface:

  protected def apply(wildcard: Wildcard,
                      multiplier: Multiplier[Nest]
                    )(using
                      iteration: It
                    )(using Flavor.Value): Long

  override def toString(): String = "Cycle Program"
