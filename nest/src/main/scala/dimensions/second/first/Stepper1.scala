package queens
package dimensions.second.first

import common.geom.Coord


import common.stepper.{ Stepper1 => Stepper1Type }

abstract trait Stepper1
    extends Queens[Coord[Stepper1Type]] {

  override val aspect = dimensions.Dimension.Aspect.stepper1

  abstract override protected def tags: List[String] =
    List("Stepper_1-Increments", super.tags mkString("[", " ", "]"))

}


package `*` {

  import common.stepper.dup.{ Stepper1Dup => `Stepper1Type*` }

  abstract trait `Stepper1*`
      extends Queens[Coord[`Stepper1Type*`]]  {

    override val aspect = dimensions.Dimension.Aspect.stepper1

    abstract override protected def tags: List[String] =
      List("Stepper*_1-Increments", super.tags mkString("[", " ", "]"))

  }

}
