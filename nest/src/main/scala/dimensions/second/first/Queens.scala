package queens
package dimensions.second.first

import dimensions.Dimension.Aspect


abstract trait Queens[At]
    extends dimensions.second.Queens[At]:

  val aspect: Aspect

  abstract override protected def tags: List[String] =
    aspect.toString() :: super.tags
