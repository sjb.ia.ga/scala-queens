package queens
package dimensions.second.first

import common.geom.Coord


abstract trait ClassicCb
    extends Queens[Coord[Int]] {

  override val aspect = dimensions.Dimension.Aspect.classicCb

  abstract override protected def tags: List[String] =
    List("Classic_w/_Callbacks", super.tags mkString("[", " ", "]"))

}

object ClassicCb {

  import common.geom.x

  implicit lazy val zero: Coord[Int] = 0 x 0

}
