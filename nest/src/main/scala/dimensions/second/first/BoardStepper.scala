package queens
package dimensions.second.first

import common.geom.{ Coord, x }


import common.stepper.{ BoardStepper => BoardStepperType }
import common.stepper.BoardStepperFactory

object BoardStepper {

  def start[S <: BoardStepperType](N: Int)
                                  (using Option[Long])
                                  (using f: BoardStepperFactory[S]): Coord[S] =
    f(N) x f(N)

}


package `*` {

  import common.stepper.dup.{ BoardStepperDup => `BoardStepperType*` }
  import common.stepper.dup.{ BoardStepperDupFactory => `BoardStepperFactory*` }

  object `BoardStepper*` {

    def `start*`[S <: `BoardStepperType*`](N: Int)
                                          (using Option[Long])
                                          (using `f*`: `BoardStepperFactory*`[S]): Coord[S] =
      `f*`(N) x `f*`(N)

  }

}
