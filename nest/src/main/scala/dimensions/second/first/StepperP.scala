package queens
package dimensions.second.first

import common.geom.Coord


import common.stepper.{ StepperP => StepperPType }

abstract trait StepperP
    extends Queens[Coord[StepperPType]] {

  override val aspect = dimensions.Dimension.Aspect.stepperP

  abstract override protected def tags: List[String] =
    List("Stepper_w/_Permutations", super.tags mkString("[", " ", "]"))

}


package `*` {

  import common.stepper.dup.{ StepperPDup => `StepperPType*` }

  abstract trait `StepperP*`
      extends Queens[Coord[`StepperPType*`]] {

    override val aspect = dimensions.Dimension.Aspect.stepperP

    abstract override protected def tags: List[String] =
      List("Stepper*_w/_Permutations", super.tags mkString("[", " ", "]"))

  }


}
