package queens
package dimensions.second.first


abstract trait Classic
    extends Queens[Point]:

  override val aspect = dimensions.Dimension.Aspect.classic

  abstract override protected def tags: List[String] =
    List("Classic", super.tags mkString("[", " ", "]"))


object Classic:

  import common.geom.x

  implicit lazy val zero: Point = 0 x 0
