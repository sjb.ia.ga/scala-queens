package queens
package dimensions.second


abstract trait Classic
    extends first.Classic
    with Queens[Point]
