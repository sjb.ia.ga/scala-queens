package queens
package dimensions.second

import common.geom.Coord


abstract trait ClassicCb
    extends first.ClassicCb
    with Queens[Coord[Int]]
