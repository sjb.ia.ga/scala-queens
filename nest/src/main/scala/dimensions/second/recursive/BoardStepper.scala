package queens
package dimensions.second
package recursive

import common.stepper.{ BoardStepper => BoardStepperType }
import common.stepper.BoardStepperFactory


abstract trait BoardStepper[S <: BoardStepperType, F <: BoardStepperFactory[S]]
    extends LtdStepper[Int, S, F]
    with dimensions.second.BoardStepper[S, F]
