package queens
package dimensions.second
package recursive
package tailcall

import common.geom.Coord


abstract trait QueensWithCallbacks[T]
    extends recursive.QueensWithCallbacks[T]
    with Queens[Coord[T]]
