package queens
package dimensions.second
package recursive
package tailcall

import java.io.ObjectOutputStream

import scala.util.control.TailCalls.{ done, tailcall, TailRec }

import base.Board

import common.geom.{ row, col, x }


abstract trait Classic
    extends Queens[Point]
    with first.Classic:

  override protected def queens(using M: Long, thaw: Either[Load, Point])
                               (using board: Board)
                               (using pragma: Either[Save, Solution] => Boolean): Unit =

    var maxSolutions = M

    thaw match
      case Left((_, end)) =>
        end()
        throw ResumeNotImplemented
      case Right(start) =>
        cqueens(board.N, start)(Nil).result

    def cqueens(k: Int, q: Point)(implicit currentSolution: Solution): TailRec[Unit] =
      if !pragma(Left({ _ => throw SuspendNotSupported })) then
        throw AlgorithmInterrupted

      if q.row == board.N
      then
        done(())

      else if k == 0
      then

        if currentSolution.length == board.N && validate(currentSolution)
        then
          maxSolutions -= 1

          if !pragma(Right(currentSolution)) then
            throw AlgorithmInterrupted

          else if maxSolutions == 0 then
            throw MaxSolutionsReached

        done(())

      else if q.col == board.N
      then
        tailcall { cqueens(k, q.row + 1 x 0) }
      else
        for
          _ <- tailcall { cqueens(k, q.row x q.col + 1) }
          _ <- {
            if !check(q)
            then
              tailcall { cqueens(k - 1, q.row x q.col + 1)(currentSolution :+ q) }
            else
              done(())
          }
        yield ()
