package queens
package dimensions.second
package recursive

import base.Board
import common.geom.Coord


abstract trait QueensWithCallbacks[T]
    extends Queens[Coord[T]]
    with dimensions.second.callbacks.BorderCallbacks[Coord[T]]
    with callbacks.Callbacks[Coord[T]]:

  final override protected def onRecurse(k: Int)
                                  (using At)
                                  (using currentSolution: PartialSolution, maxSolutionsDec: () => Long)
                                  (using board: Board, resume: Boolean, save: Save)
                                  (using pragma: Either[Save, Solution] => Boolean): Boolean =

    if !resume && !pragma(Left(save))
    then
      if save(None)
      then
        throw ComputationSuspended
      else
        throw AlgorithmInterrupted

    if isBoardBottom
    then
      false

    else if k == 0
    then
      if currentSolution.length == board.N && validate(currentSolution)
      then
        val maxSolutions = maxSolutionsDec()

        if !pragma(Right(currentSolution.toList)) then
          throw AlgorithmInterrupted

        if maxSolutions == 0 then
          throw MaxSolutionsReached

      false
    else
      true

  inline final override protected def onAppend(using q: Point, s: PartialSolution): Unit = s.push(q)

  inline final override protected def onBacktrack(using s: PartialSolution): Unit = s.pop()
