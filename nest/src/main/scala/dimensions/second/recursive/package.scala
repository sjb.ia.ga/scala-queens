package queens
package dimensions.second


package object recursive:

  import scala.collection.mutable.Stack

  private[recursive] type PartialSolution = Stack[Point]
