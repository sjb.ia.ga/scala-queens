package queens
package dimensions.second
package recursive

import base.Board


abstract trait Queens[At]
    extends dimensions.second.Queens[At]
    with dimensions.second.`Queens*`[At]:

  protected def queens(using Long, Either[Load, At])
                      (using Board)
                      (using Either[Save, Solution] => Boolean): Unit

  override protected def solve(using Long, Either[Load, At])
                              (using Board)
                              (using Either[Save, Solution] => Boolean): Option[Boolean] =
    super.solve match
      case None =>
        try
          queens
          Some(true)
        catch
          case MaxSolutionsReached | AlgorithmInterrupted | SuspendNotSupported | ResumeNotImplemented =>
            Some(false)
          case AlgorithmError(t) =>
            throw t
      case it =>
        it
