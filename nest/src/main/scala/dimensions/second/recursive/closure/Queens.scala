package queens
package dimensions.second
package recursive
package closure


abstract trait Queens[At]
    extends recursive.Queens[At]:

  final override val algorithm = dimensions.Dimension.Algorithm.closure


private[closure] object Queens:

  import scala.annotation.tailrec

  sealed abstract trait Call extends Any:
    def apply(): Option[Call]

    @tailrec
    final def call: Unit =
      this() match
        case Some(call) => call.call
        case _ =>

  object - extends Call:
    override def apply(): Option[Call] = None

  class |(closure: () => Call) extends AnyVal with Call:
    override def apply(): Option[Call] = Some(closure())

  object | :
    inline def apply(closure: => Call): | = new |(() => closure)

  class *(val calls: Call*) extends AnyVal with Call:
    override def apply(): Option[Call] = calls
      .headOption
      .map {
        _() match
          case Some(it: *) => *((it.calls ++ calls.tail)*)
          case Some(call: |) => *((call +: calls.tail)*)
          case _ => *(calls.tail*)
      }
