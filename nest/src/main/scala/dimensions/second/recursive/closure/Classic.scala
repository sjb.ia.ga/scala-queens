package queens
package dimensions.second
package recursive
package closure

import base.Board

import common.geom.{ row, col, x }


abstract trait Classic
    extends Queens[Point]
    with first.Classic:

  import Queens._

  override protected def queens(using M: Long, thaw: Either[Load, Point])
                               (using board: Board)
                               (using pragma: Either[Save, Solution] => Boolean): Unit =
    var maxSolutions = M

    thaw match
      case Left((_, end)) =>
        end()
        throw ResumeNotImplemented
      case Right(start) =>
        | { cqueens(board.N, start)(Nil) }.call

    def cqueens(k: Int, q: Point)(implicit currentSolution: Solution): Call =
      if !pragma(Left({ _ => throw SuspendNotSupported })) then
        throw AlgorithmInterrupted

      if q.row == board.N
      then
        -

      else if k == 0
      then

        if currentSolution.length == board.N && validate(currentSolution)
        then
          maxSolutions -= 1

          if !pragma(Right(currentSolution.toList)) then
            throw AlgorithmInterrupted

          if maxSolutions == 0 then
            throw MaxSolutionsReached

        -

      else if q.col == board.N
      then
        | { cqueens(k, q.row + 1 x 0) }
      else
        * (
          | { cqueens(k, q.row x q.col + 1) }
        , if !check(q)
          then
            | { cqueens(k - 1, q.row x q.col + 1)(currentSolution :+ q) }
          else
            -
        )
