package queens
package dimensions.second
package recursive
package native

import base.Board

import common.geom.{ row, col, x }


abstract trait Classic
    extends Queens[Point]
    with first.Classic:

  override protected def queens(using M: Long, thaw: Either[Load, Point])
                               (using board: Board)
                               (using pragma: Either[Save, Solution] => Boolean): Unit =
    implicit val currentSolution: PartialSolution = scala.collection.mutable.Stack[Point]()

    var maxSolutions = M

    thaw match
      case Left((_, end)) =>
        end()
        throw ResumeNotImplemented
      case Right(start) =>
        cqueens(board.N, start)

    def cqueens(k: Int, q: Point): Unit =
      if !pragma(Left({ _ => throw SuspendNotSupported })) then
        throw AlgorithmInterrupted

      if q.row == board.N
      then
        return

      else if k == 0
      then

        if currentSolution.length == board.N && validate(currentSolution)
        then
          maxSolutions -= 1

          if !pragma(Right(currentSolution.toList)) then
            throw AlgorithmInterrupted

          if maxSolutions == 0 then
            throw MaxSolutionsReached

        return

      if q.col == board.N
      then
        cqueens(k, q.row + 1 x 0)
      else
        cqueens(k, q.row x q.col + 1)

        if !check(q)
        then
          currentSolution.push(q)
          cqueens(k - 1, q.row x q.col + 1)
          currentSolution.pop()
