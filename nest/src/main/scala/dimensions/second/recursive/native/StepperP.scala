package queens
package dimensions.second
package recursive
package native

import common.stepper.{ StepperP => StepperPType }
import common.stepper.BoardStepperFactory


abstract trait StepperP
    extends BoardStepper[StepperPType, BoardStepperFactory[StepperPType]]
    with dimensions.second.StepperP
