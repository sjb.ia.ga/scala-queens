package queens
package dimensions.second
package recursive
package native

import common.stepper.{ Stepper1 => Stepper1Type }
import common.stepper.BoardStepperFactory


abstract trait Stepper1
    extends BoardStepper[Stepper1Type, BoardStepperFactory[Stepper1Type]]
    with dimensions.second.Stepper1
