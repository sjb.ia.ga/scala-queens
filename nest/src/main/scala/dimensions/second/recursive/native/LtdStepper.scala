package queens
package dimensions.second
package recursive
package native

import java.io.ObjectOutputStream

import base.Board

import common.stepper.{ LtdStepperForwardLenient => LtdStepperType, StepperFactory }
import common.stepper.given
import common.geom.{ row, col, x }


abstract trait LtdStepper[T, S <: LtdStepperType[T], F <: StepperFactory[T, S]]
    extends Stepper[T, S, F]
    with recursive.LtdStepper[T, S, F]:

  override protected def queens(using M: Long, thaw: Either[Load, At])
                               (using board: Board)
                               (using Either[Save, Solution] => Boolean): Unit =
    implicit val currentSolution: PartialSolution = scala.collection.mutable.Stack[Point]()

    var maxSolutions = M
    given (() => Long) = { () => maxSolutions -= 1; maxSolutions }

    implicit val resume: Boolean = false
    implicit val save: (Option[ObjectOutputStream] => Boolean) =
    { case None => false case _ => throw SuspendNotSupported }

    thaw match
      case Left((_, end)) =>
        end()
        throw ResumeNotImplemented
      case Right(start) =>
        lsqueens(board.N)(using start)

    def lsqueens(k: Int)(using q: At): Unit =

      if onRecurse(k)
      then
        if isBoardBorder
        then
          q.row.forward
          given At = q.row x f(board.N)(using board.seed)
          lsqueens(k)
          q.row.backward
        else
          q.col.forward
          lsqueens(k)
          q.col.backward

          given p: Point = q.row x q.col

          if !check(p)
          then
            onAppend

            q.col.forward
            lsqueens(k - 1)
            q.col.backward

            onBacktrack
