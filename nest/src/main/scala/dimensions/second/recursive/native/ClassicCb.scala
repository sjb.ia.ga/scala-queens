package queens
package dimensions.second
package recursive
package native

import java.io.ObjectOutputStream

import base.Board

import common.geom.{ row, col, x }


abstract trait ClassicCb
    extends QueensWithCallbacks[Int]
    with dimensions.second.callbacks.PointBorderCallbacks
    with first.ClassicCb:

  override protected def queens(using M: Long, thaw: Either[Load, Point])
                               (using board: Board)
                               (using Either[Save, Solution] => Boolean): Unit =
    implicit val currentSolution: PartialSolution = scala.collection.mutable.Stack[Point]()

    var maxSolutions = M
    given (() => Long) = { () => maxSolutions -= 1; maxSolutions }

    implicit val resume: Boolean = false
    implicit val save: (Option[ObjectOutputStream] => Boolean) =
    { case None => false case _ => throw SuspendNotSupported }

    thaw match
      case Left((_, end)) =>
        end()
        throw ResumeNotImplemented
      case Right(start) =>
        cbqueens(board.N, start)

    def cbqueens(k: Int, q: At): Unit =

      given At = q

      if onRecurse(k)
      then
        if isBoardBorder
        then
          cbqueens(k, q.row + 1 x 0)
        else
          cbqueens(k, q.row x q.col + 1)

          if !check(q)
          then
            onAppend
            cbqueens(k - 1, q.row x q.col + 1)
            onBacktrack
