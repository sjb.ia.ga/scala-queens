package queens
package dimensions.second
package recursive
package trampoline

import java.io.ObjectOutputStream

import base.Board

import common.geom.{ row, col, x }

import common.Trampoline


abstract trait Classic
    extends Queens[Point]
    with first.Classic:

  import Queens._

  override protected def queens(using M: Long, thaw: Either[Load, Point])
                               (using board: Board)
                               (using pragma: Either[Save, Solution] => Boolean): Unit =

    var maxSolutions = M

    thaw match
      case Left((_, end)) =>
        end()
        throw ResumeNotImplemented
      case Right(start) =>
        cqueens(board.N, start)(Nil)()

    def cqueens(k: Int, q: Point)(implicit currentSolution: Solution): Trampoline[Unit] =
      if !pragma(Left({ _ => throw SuspendNotSupported })) then
        throw AlgorithmInterrupted

      if q.row == board.N
      then
        -

      else if k == 0
      then

        if currentSolution.length == board.N && validate(currentSolution)
        then
          maxSolutions -= 1

          if !pragma(Right(currentSolution)) then
            throw AlgorithmInterrupted

          else if maxSolutions == 0 then
            throw MaxSolutionsReached

        -

      else if q.col == board.N
      then
        | { cqueens(k, q.row + 1 x 0) }
      else
        for
          _ <- | { cqueens(k, q.row x q.col + 1) }
          _ <- | {
            if !check(q)
            then
              cqueens(k - 1, q.row x q.col + 1)(currentSolution :+ q)
            else
              -
          }
        yield ()
