package queens
package dimensions.second
package recursive
package trampoline


abstract trait Queens[At]
    extends recursive.Queens[At]:

  final override val algorithm = dimensions.Dimension.Algorithm.trampoline


private[trampoline] object Queens:

  import common.Trampoline

  val - = Trampoline.pure(())

  object | :
    inline def apply[A](closure: => Trampoline[A]): Trampoline[A] = Trampoline.Call(closure)

