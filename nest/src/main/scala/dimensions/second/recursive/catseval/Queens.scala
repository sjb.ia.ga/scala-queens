package queens
package dimensions.second
package recursive
package catseval


abstract trait Queens[At]
    extends recursive.Queens[At]:

  final override val algorithm = dimensions.Dimension.Algorithm.catseval
