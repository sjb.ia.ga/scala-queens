package queens
package dimensions.second
package recursive
package catseval

import java.io.ObjectOutputStream

import cats.Eval
import Eval.{ defer, Unit }

import base.Board

import common.geom.{ row, col, x }


abstract trait ClassicCb
    extends QueensWithCallbacks[Int]
    with dimensions.second.callbacks.PointBorderCallbacks
    with first.ClassicCb:

  override protected def queens(using M: Long, thaw: Either[Load, Point])
                               (using board: Board)
                               (using Either[Save, Solution] => Boolean): Unit =
    var maxSolutions = M
    given (() => Long) = { () => maxSolutions -= 1; maxSolutions }

    implicit val resume: Boolean = false
    implicit val save: (Option[ObjectOutputStream] => Boolean) =
    { case None => false case _ => throw SuspendNotSupported }

    thaw match
      case Left((_, end)) =>
        end()
        throw ResumeNotImplemented
      case Right(start) =>
        cbqueens(board.N, start)(Nil).value

    def cbqueens(k: Int, q: At)(implicit currentSolution: Solution): Eval[Unit] =
      given PartialSolution = scala.collection.mutable.Stack.from(currentSolution)

      given At = q

      if onRecurse(k)
      then
        if isBoardBorder
        then
          defer { cbqueens(k, q.row + 1 x 0) }
        else
          for
            _ <- defer { cbqueens(k, q.row x q.col + 1) }
            _ <- {
              if !check(q)
              then
                defer { cbqueens(k - 1, q.row x q.col + 1)(currentSolution :+ q) }
              else
                Unit
            }
          yield ()

      else
        Unit
