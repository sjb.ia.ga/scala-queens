package queens
package dimensions.second
package recursive
package catseval

import java.io.ObjectOutputStream

import cats.Eval
import Eval.{ defer, Unit }

import base.Board

import common.geom.{ row, col, x }


abstract trait Classic
    extends Queens[Point]
    with first.Classic:

  override protected def queens(using M: Long, thaw: Either[Load, Point])
                               (using board: Board)
                               (using pragma: Either[Save, Solution] => Boolean): Unit =

    var maxSolutions = M

    thaw match
      case Left((_, end)) =>
        end()
        throw ResumeNotImplemented
      case Right(start) =>
        cqueens(board.N, start)(Nil).value

    def cqueens(k: Int, q: Point)(implicit currentSolution: Solution): Eval[Unit] =
      if !pragma(Left({ _ => throw SuspendNotSupported })) then
        throw AlgorithmInterrupted

      if q.row == board.N
      then
        Unit

      else if k == 0
      then

        if currentSolution.length == board.N && validate(currentSolution)
        then
          maxSolutions -= 1

          if !pragma(Right(currentSolution)) then
            throw AlgorithmInterrupted

          else if maxSolutions == 0 then
            throw MaxSolutionsReached

        Unit

      else if q.col == board.N
      then
        defer { cqueens(k, q.row + 1 x 0) }
      else
        for
          _ <- defer { cqueens(k, q.row x q.col + 1) }
          _ <- {
            if !check(q)
            then
              defer { cqueens(k - 1, q.row x q.col + 1)(currentSolution :+ q) }
            else
              Unit
          }
        yield ()
