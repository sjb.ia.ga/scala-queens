package queens
package dimensions.second
package recursive
package continuation

import common.stepper.{ Stepper => StepperType }
import common.stepper.StepperFactory


abstract trait Stepper[T, S <: StepperType[T], F <: StepperFactory[T, S]]
    extends QueensWithCallbacks[S]
    with recursive.Stepper[T, S, F]
