package queens
package dimensions.second
package recursive
package continuation

import java.io.ObjectOutputStream

import scala.annotation.tailrec

import base.Board

import common.stepper.{ LtdStepperForwardLenient => LtdStepperType, StepperFactory }
import common.stepper.given
import common.geom.{ row, col, x }


abstract trait LtdStepper[T, S <: LtdStepperType[T], F <: StepperFactory[T, S]]
    extends Stepper[T, S, F]
    with recursive.LtdStepper[T, S, F]:

  override protected def queens(using M: Long, thaw: Either[Load, At])
                               (using board: Board)
                               (using Either[Save, Solution] => Boolean): Unit =
    implicit val currentSolution: PartialSolution = scala.collection.mutable.Stack[Point]()

    var maxSolutions = M
    given (() => Long) = { () => maxSolutions -= 1; maxSolutions }

    implicit val resume: Boolean = false
    implicit val save: (Option[ObjectOutputStream] => Boolean) =
    { case None => false case _ => throw SuspendNotSupported }

    thaw match
      case Left((_, end)) =>
        end()
        throw ResumeNotImplemented
      case Right(start) =>
        lsqueens(board.N)(None)(using start)

    @tailrec
    def lsqueens(k: Int)
                (continuation: Continuation[S])
                (using q: At): Unit =

      if onRecurse(k)
      then
        if isBoardBorder
        then
          q.row.forward
          given At = q.row x f(board.N)(using board.seed)
          lsqueens(k)(Some((-1, null, null, () => q.row.backward, continuation)))

        else
          given p: Point = q.row x q.col

          if !check(p)
          then
            val a = { () =>
              q.col.backward

              onAppend

              q.col.forward
            }

            val b = { () =>
              q.col.backward

              onBacktrack
            }

            q.col.forward
            lsqueens(k)(Some((k - 1, q, null, a, Some((-1, null, null, b, continuation)))))

          else
            q.col.forward
            lsqueens(k)(Some((-1, null, null, () => q.col.backward, continuation)))

      else
        var e: Either[Continuation[S], Continuation[S]] = Left(continuation)
        while e.isLeft && e.left.get.nonEmpty
        do
          val (_, q, _, a, c: Continuation[S]) = e.left.get.get : @unchecked
          a()
          e = if q eq null then Left(c) else e.swap
        if e.isRight && e.right.get.nonEmpty
        then
          val (k, q, _, _, c: Continuation[S]) = e.right.get.get : @unchecked
          lsqueens(k)(c)(using q)
