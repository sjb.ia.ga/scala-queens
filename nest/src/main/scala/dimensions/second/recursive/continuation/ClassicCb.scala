package queens
package dimensions.second
package recursive
package continuation

import java.io.ObjectOutputStream

import scala.annotation.tailrec

import scala.util.control.TailCalls.{ done, tailcall, TailRec }

import base.Board

import common.geom.{ row, col, x }


abstract trait ClassicCb
    extends QueensWithCallbacks[Int]
    with dimensions.second.callbacks.PointBorderCallbacks
    with first.ClassicCb:

  def onRecurse(k: Int)
               (continuation: Continuation[Int])
               (expr: => Unit)
         (using q: At)
         (using () => Long)
         (using board: Board, resume: Boolean, save: Continuation[Int] => Save)
         (using pragma: Either[Save, Solution] => Boolean)
      (implicit currentSolution: Solution): Boolean =

    if !resume && !pragma(Left(save(Some((k, q, currentSolution, null, continuation)))))
    then
      if save(None)(None)
      then
        throw ComputationSuspended
      else
        throw AlgorithmInterrupted

    expr

    given PartialSolution = scala.collection.mutable.Stack.from(currentSolution)
    given Save = { _ => false }
    given (Either[Save, Solution] => Boolean) = {
      case Left(_) => true
      case it => pragma(it)
    }

    super.onRecurse(k)

  override protected def queens(using M: Long, thaw: Either[Load, Point])
                               (using board: Board)
                               (using Either[Save, Solution] => Boolean): Unit =
    var maxSolutions = M
    given (() => Long) = { () => maxSolutions -= 1; maxSolutions }

    implicit var resume: Boolean = false
    var stream: ObjectOutputStream = null
    implicit def save(continuation: Continuation[Int]): (Option[ObjectOutputStream] => Boolean) = _ match
      case Some(it) =>
        stream = it
        it.writeLong(maxSolutions)
        var c = continuation
        var n = 0L
        while c ne None
        do
          val (_, _, _, _, _c: Continuation[Int]) = c.get : @unchecked
          c = _c
          n += 1
        it.writeLong(n)
        c = continuation
        while c ne None
        do
          val (k, q, s, _, _c: Continuation[Int]) = c.get : @unchecked
          it.writeInt(k)
          it.writeObject(q)
          it.writeObject(s)
          c = _c
        true
      case _ =>
        stream ne null

    thaw match
      case Left((stream, end)) =>
        def continuation(n: Long): TailRec[Continuation[Int]] =
          if n == 0L
          then
            done(None)
          else
            val k = stream.readInt()
            val q = stream.readObject().asInstanceOf[Point]
            val s = stream.readObject().asInstanceOf[Solution]
            for c <- tailcall(continuation(n-1))
            yield Some((k, q, s, null, c))
        maxSolutions = stream.readLong()
        val n = stream.readLong()
        val (k, q, s, _, c: Continuation[Int]) = continuation(n).result.get : @unchecked
        end()
        resume = true
        cbqueens(k, q)(c)(s)
      case Right(start) =>
        cbqueens(board.N, start)(None)(Nil)

    @tailrec
    def cbqueens(k: Int, q: At)
                (continuation: Continuation[Int])
       (implicit currentSolution: Solution): Unit =
      given At = q

      if onRecurse(k)(continuation) { resume = false }
      then
        if isBoardBorder
        then
          cbqueens(k, q.row + 1 x 0)(continuation)
        else
          cbqueens(k, q.row x q.col + 1) {
            if !check(q)
            then
              Some((k - 1, q.row x q.col + 1, currentSolution :+ q, null, continuation))
            else
              continuation
          }
      else if continuation.nonEmpty
      then
        val (k, q, s, _, c: Continuation[Int]) = continuation.get : @unchecked
        cbqueens(k, q)(c)(s)
