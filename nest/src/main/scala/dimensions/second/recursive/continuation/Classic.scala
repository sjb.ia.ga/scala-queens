package queens
package dimensions.second
package recursive
package continuation

import java.io.ObjectOutputStream

import scala.annotation.tailrec

import scala.util.control.TailCalls.{ done, tailcall, TailRec }

import base.Board

import common.geom.{ row, col, x }


abstract trait Classic
    extends Queens[Point]
    with first.Classic:

  override protected def queens(using M: Long, thaw: Either[Load, Point])
                               (using board: Board)
                               (using pragma: Either[Save, Solution] => Boolean): Unit =
    var maxSolutions = M

    var resume: Boolean = false
    var stream: ObjectOutputStream = null
    def save(continuation: Continuation[Int]): (Option[ObjectOutputStream] => Boolean) = _ match
      case Some(it) =>
        stream = it
        it.writeLong(maxSolutions)
        var c = continuation
        var n = 0L
        while c ne None
        do
          val (_, _, _, _, _c: Continuation[Int]) = c.get : @unchecked
          c = _c
          n += 1
        it.writeLong(n)
        c = continuation
        while c ne None
        do
          val (k, q, s, _, _c: Continuation[Int]) = c.get : @unchecked
          it.writeInt(k)
          it.writeObject(q)
          it.writeObject(s)
          c = _c
        true
      case _ =>
        stream ne null

    thaw match
      case Left((stream, end)) =>
        def continuation(n: Long): TailRec[Continuation[Int]] =
          if n == 0L
          then
            done(None)
          else
            val k = stream.readInt()
            val q = stream.readObject().asInstanceOf[Point]
            val s = stream.readObject().asInstanceOf[Solution]
            for c <- tailcall(continuation(n-1))
            yield Some((k, q, s, null, c))
        maxSolutions = stream.readLong()
        val n = stream.readLong()
        val (k, q, s, _, c: Continuation[Int]) = continuation(n).result.get : @unchecked
        end()
        resume = true
        cqueens(k, q)(c)(s)
      case Right(start) =>
        cqueens(board.N, start)(None)(Nil)

    @tailrec
    def cqueens(k: Int, q: Point)
               (continuation: Continuation[Int])
      (implicit currentSolution: Solution): Unit =
      if !resume && !pragma(Left(save(Some((k, q, currentSolution, null, continuation)))))
      then
        if save(None)(None)
        then
          throw ComputationSuspended
        else
          throw AlgorithmInterrupted

      resume = false

      if k == 0 || q.row == board.N
      then
        if k == 0
        then

          if currentSolution.length == board.N && validate(currentSolution)
          then
            maxSolutions -= 1

            if !pragma(Right(currentSolution)) then
              throw AlgorithmInterrupted

            if maxSolutions == 0 then
              throw MaxSolutionsReached

        if continuation.nonEmpty
        then
          val (k, q, s, _, c: Continuation[Int]) = continuation.get : @unchecked
          cqueens(k, q)(c)(s)

      else if q.col == board.N
      then
        cqueens(k, q.row + 1 x 0)(continuation)
      else
        cqueens(k, q.row x q.col + 1) {
          if !check(q)
          then
            Some((k - 1, q.row x q.col + 1, currentSolution :+ q, null, continuation))
          else
            continuation
        }
