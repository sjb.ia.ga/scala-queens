package queens
package dimensions.second
package recursive
package continuation


abstract trait Queens[At]
    extends recursive.Queens[At]:

  final override val algorithm = dimensions.Dimension.Algorithm.continuation
