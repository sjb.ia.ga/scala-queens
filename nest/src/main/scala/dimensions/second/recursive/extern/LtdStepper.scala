package queens
package dimensions.second
package recursive
package extern

import java.io.ObjectOutputStream

import base.Board

import common.stepper.{ LtdStepperForwardLenient => LtdStepperType, StepperFactory }

import common.geom.{ row, col }
import common.stepper.{ x, given }

import common.{ StackVirtualMachine => StackVM }


abstract trait LtdStepper[T, S <: LtdStepperType[T], F <: StepperFactory[T, S]]
    extends Stepper[T, S, F]
    with recursive.LtdStepper[T, S, F]:

  override protected def queens(using M: Long, start: Either[Load, At])
                               (using board: Board)
                               (using Either[Save, Solution] => Boolean): Unit =

    final class LtdStepperVM extends StackVM[Int, (Int, At)]:
      private implicit var currentSolution: PartialSolution = scala.collection.mutable.Stack[Point]()

      private var maxSolutions: Long = _
      private given (() => Long) = { () => maxSolutions -= 1; maxSolutions }

      private val lsqueens = Call { (_, _, _) => (45, (board.N, start.getOrElse(null))) }

      private implicit var resume: Boolean = false
      private var stream: ObjectOutputStream = null
      private implicit val save: (Option[ObjectOutputStream] => Boolean) =
      { case Some(it) => stream = it; true case _ => stream ne null }

      private val program = Map(

        41 -> Start { (_, _, _) => maxSolutions = M },
        42 -> Jmp(43),
        43 -> lsqueens,
        44 -> End(),
        45 -> Jmp(50),




        50 -> Mov,
        51 -> Tst[(Int, At)] { case (_, _, (k, q)) => given At = q; try onRecurse(k) catch ComputationSuspended => false },
        52 -> Noop { (_, _, _) => resume = false },
        53 -> CJmp(60),
        54 -> Tst { (_, _, _) => save(None) },
        55 -> CJmp(105),
        56 -> Retn,



        60 -> Mov,
        61 -> Tst[(_, At)] { case (_, _, (_, q)) => given At = q; isBoardBorder },
        62 -> CJmp(64),
        63 -> Jmp(70),
        64 -> Peek[(_, At)] { case (_, _, (_, q)) => q.row.forward },
        65 -> Jmp(85),
        66 -> Peek[(_, At)] { case (_, _, (_, q)) => q.row.backward },
        67 -> Retn,


        70 -> Peek[(_, At)] { case (_, _, (_, q)) => q.col.forward },
        71 -> Call { (_, _, it) => (45, it) },
        72 -> Peek[(_, At)] { case (_, _, (_, q)) => q.col.backward },
        73 -> Jmp(93),
        74 -> CJmp(80),
        75 -> Jmp(95),
        76 -> Peek[(_, At)] { case (_, _, (_, q)) => q.col.forward },
        77 -> Call { case (_, _, (k, q)) => (45, (k - 1, q)) },
        78 -> Peek[(_, At)] { case (_, _, (_, q)) => q.col.backward },
        79 -> Jmp(99),
        80 -> Retn,




        85 -> Call { case (_, _, (k, q)) => (45, (k, q.row x f(board.N)(using board.seed))) },
        86 -> Jmp(66),






        93 -> Tst[(_, At)] { case (_, _, (_, q)) => val p: Point = q; check(p) },
        94 -> Jmp(74),
        95 -> Peek[(_, At)] { case (_, _, (_, q)) => given Point = q; onAppend },
        96 -> Jmp(76),


        99 -> Peek[(_, At)] { case (_, _, (_, q)) => given Point = q; onBacktrack },
       100 -> Retn,




       105 -> Suspend { (_, _, _) => (stream, { it => it.writeLong(maxSolutions); it.writeObject(currentSolution); }, ComputationSuspended) },
       106 -> Resume { (_, _, it) => maxSolutions = it.readLong(); currentSolution = it.readObject().asInstanceOf[PartialSolution] },
       107 -> Noop { (_, _, _) => resume = true },
       108 -> Jmp(51)

      )

      def run: Nothing =
        import extern.given
        super.run(program, start.map(_=>41))

    try
      new LtdStepperVM().run
    catch
      case StackVM.ProgramEnded =>
      case StackVM.ProgramSuspendedWith(t, _) => throw t
      case StackVM.ProgramStoppedWith(t, _) => throw t
      case t: Throwable => throw AlgorithmError(t)
