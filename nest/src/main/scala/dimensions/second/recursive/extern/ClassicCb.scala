package queens
package dimensions.second
package recursive
package extern

import java.io.ObjectOutputStream

import base.Board

import common.geom.{ row, col, x }

import common.{ StackVirtualMachine => StackVM }


abstract trait ClassicCb
    extends QueensWithCallbacks[Int]
    with dimensions.second.callbacks.PointBorderCallbacks
    with first.ClassicCb:

  override protected def queens(using M: Long, thaw: Either[Load, At])
                               (using board: Board)
                               (using Either[Save, Solution] => Boolean): Unit =

    final class ClassicCbVM extends StackVM[Int, (Int, Point)]:
      private implicit var currentSolution: PartialSolution = scala.collection.mutable.Stack[Point]()

      private var maxSolutions: Long = _
      private given (() => Long) = { () => maxSolutions -= 1; maxSolutions }

      private val cbqueens = Call { (_, _, _) => (45, (board.N, thaw.getOrElse(null))) }

      private implicit var resume: Boolean = false
      private var stream: ObjectOutputStream = null
      private implicit val save: (Option[ObjectOutputStream] => Boolean) =
      { case Some(it) => stream = it; true case _ => stream ne null }

      private val program = Map(



        41 -> Start { (_, _, _) => maxSolutions = M },
        42 -> Jmp(43),
        43 -> cbqueens,
        44 -> End(),
        45 -> Jmp(50),




        50 -> Mov,
        51 -> Tst[(Int, Point)] { case (_, _, (k, q)) => given At = q; try onRecurse(k) catch ComputationSuspended => false },
        52 -> Noop { (_, _, _) => resume = false },
        53 -> CJmp(60),
        54 -> Tst { (_, _, _) => save(None) },
        55 -> CJmp(80),
        56 -> Retn,



        60 -> Mov,
        61 -> Tst[(?, Point)] { case (_, _, (_, q)) => given At = q; isBoardBorder },
        62 -> CJmp(64),
        63 -> Jmp(70),
        64 -> Call { case (_, _, (k, q)) => (45, (k, q.row + 1 x 0)) },
        65 -> Noop { (_, _, _) => resume = false },
        66 -> Retn,



        70 -> Call { case (_, _, (k, q)) => (45, (k, q.row x q.col + 1)) },
        71 -> Tst[(?, Point)] { case (_, _, (_, q)) => check(q) },
        72 -> CJmp(76),
        73 -> Peek[(?, Point)] { case (_, _, (_, q)) => given At = q; onAppend },
        74 -> Call { case (_, _, (k, q)) => (45, (k - 1, q.row x q.col + 1)) },
        75 -> Peek[(?, Point)] { case (_, _, (_, q)) => given At = q; onBacktrack },
        76 -> Retn,



        80 -> Suspend { (_, _, _) => (stream, { it => it.writeLong(maxSolutions); it.writeObject(currentSolution); }, ComputationSuspended) },
        81 -> Resume { (_, _, it) => maxSolutions = it.readLong(); currentSolution = it.readObject().asInstanceOf[PartialSolution] },
        82 -> Noop { (_, _, _) => resume = true },
        83 -> Jmp(51)

      )

      def run: Nothing =
        import extern.given
        super.run(program, thaw.map(_=>41))

    try
      new ClassicCbVM().run
    catch
      case StackVM.ProgramEnded =>
      case StackVM.ProgramSuspendedWith(t, _) => throw t
      case StackVM.ProgramStoppedWith(t, _) => throw t
      case t: Throwable => throw AlgorithmError(t)

    def freeze(stream: ObjectOutputStream) = {}
