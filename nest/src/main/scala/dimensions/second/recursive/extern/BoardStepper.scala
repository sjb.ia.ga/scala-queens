package queens
package dimensions.second
package recursive
package extern

import common.stepper.{ BoardStepper => BoardStepperType }
import common.stepper.BoardStepperFactory


abstract trait BoardStepper[S <: BoardStepperType, F <: BoardStepperFactory[S]]
    extends LtdStepper[Int, S, F]
    with recursive.BoardStepper[S, F]
