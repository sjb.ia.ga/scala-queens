package queens
package dimensions.second
package recursive
package extern

import java.io.ObjectOutputStream

import base.Board

import common.geom.{ row, col, x }

import common.{ StackVirtualMachine => StackVM }


abstract trait Classic
    extends Queens[Point]
    with first.Classic:

  override protected def queens(using M: Long, thaw: Either[Load, At])
                               (using board: Board)
                               (using pragma: Either[Save, Solution] => Boolean): Unit =

    final class ClassicVM extends StackVM[Int, (Int, Point)]:
      private implicit var currentSolution: PartialSolution = scala.collection.mutable.Stack[Point]()

      private var maxSolutions: Long = _
      private val cqueens = Call { (_, _, _) => (45, (board.N, thaw.getOrElse(null))) }

      private var resume: Boolean = false
      private var stream: ObjectOutputStream = null
      private val save: (Option[ObjectOutputStream] => Boolean) =
      { case Some(it) => stream = it; true case _ => stream ne null }

      private val program = Map(






        41 -> Start { (_, _, _) => maxSolutions = M },
        42 -> Jmp(43),
        43 -> cqueens,
        44 -> End(),
        45 -> Jmp(50),




        50 -> Mov,
        51 -> Tst { (_, _, _) => !resume && !pragma(Left(save)) },
        52 -> Noop { (_, _, _) => resume = false },
        53 -> CJmp(73),
        54 -> Tst[(Int, Point)] { case (_, _, (_, q)) => q.row == board.N },
        55 -> CJmp(99),
        56 -> Tst[(Int, Point)] { case (_, _, (k, _)) => k == 0 },
        57 -> CJmp(59),
        58 -> Jmp(80),
        59 -> Tst { (_, _, _) => currentSolution.length < board.N },
        60 -> CJmp(99),
        61 -> Tst { (_, _, _) => validate(currentSolution) },
        62 -> CJmp(64),
        63 -> Jmp(99),
        64 -> Noop { (_, _, _) => maxSolutions -= 1 },
        65 -> Poke { (_, _, _) => currentSolution.toList },
        66 -> Tst[Solution] { (_, _, solution) => !pragma(Right(solution)) },
        67 -> CJmp(76),
        68 -> Tst { (_, _, _) => maxSolutions == 0 },
        69 -> CJmp(Int.MaxValue),
        70 -> Jmp(99),


        73 -> Tst { (_, _, _) => !save(None) },
        74 -> CJmp(76),
        75 -> Jmp(105),
        76 -> Noop { (_, _, _) => throw AlgorithmInterrupted },



        80 -> Mov,
        81 -> Tst[(?, Point)] { case (_, _, (_, q)) => q.col == board.N },
        82 -> CJmp(84),
        83 -> Jmp(90),
        84 -> Call { case (_, _, (k, q)) => (45, (k, q.row + 1 x 0)) },
        85 -> Jmp(99),




        90 -> Call { case (_, _, (k, q)) => (45, (k, q.row x q.col + 1)) },
        91 -> Tst[(?, Point)] { case (_, _, (_, q)) => check(q) },
        92 -> CJmp(99),
        93 -> Peek[(?, Point)] { case (_, _, (_, q)) => currentSolution.push(q) },
        94 -> Call { case (_, _, (k, q)) => (45, (k - 1, q.row x q.col + 1)) },
        95 -> Noop { (_, _, _) => currentSolution.pop() },
        96 -> Jmp(99),


        99 -> Retn,





       105 -> Suspend { (_, _, _) => (stream, { it => it.writeLong(maxSolutions); it.writeObject(currentSolution); }, ComputationSuspended) },
       106 -> Resume { (_, _, it) => maxSolutions = it.readLong(); currentSolution = it.readObject().asInstanceOf[PartialSolution] },
       107 -> Noop { (_, _, _) => resume = true },
       108 -> Jmp(51),


        Int.MaxValue
           -> Noop { (_, _, _) => throw MaxSolutionsReached }

      )

      def run: Nothing =
        import extern.given
        super.run(program, thaw.map(_=>41))

    try
      new ClassicVM().run
    catch
      case StackVM.ProgramEnded =>
      case StackVM.ProgramSuspendedWith(t, _) => throw t
      case StackVM.ProgramStoppedWith(t, _) => throw t
      case t: Throwable => throw AlgorithmError(t)
