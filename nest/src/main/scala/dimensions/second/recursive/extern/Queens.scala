package queens
package dimensions.second
package recursive
package extern


abstract trait Queens[At]
    extends recursive.Queens[At]:

  override val algorithm = dimensions.Dimension.Algorithm.extern
