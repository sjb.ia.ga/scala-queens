package queens
package dimensions.second
package recursive
package callbacks

import base.Board


abstract trait Callbacks[At]:

  protected def onRecurse(k: Int)
                         (using At)
                         (using PartialSolution, () => Long)
                         (using Board, Boolean, Save)
                         (using Either[Save, Solution] => Boolean): Boolean

  protected def onAppend(using Point, PartialSolution): Unit

  protected def onBacktrack(using PartialSolution): Unit
