package queens
package dimensions.second
package recursive

import common.stepper.{ LtdStepperForwardLenient => LtdStepperType }
import common.stepper.StepperFactory


abstract trait LtdStepper[T, S <: LtdStepperType[T], F <: StepperFactory[T, S]]
    extends Stepper[T, S, F]
    with dimensions.second.LtdStepper[T, S, F]
