package queens
package dimensions.second
package recursive

import common.stepper.{ Stepper => StepperType }
import common.stepper.StepperFactory


abstract trait Stepper[T, S <: StepperType[T], F <: StepperFactory[T, S]]
    extends QueensWithCallbacks[S]
    with dimensions.second.Stepper[T, S, F]
