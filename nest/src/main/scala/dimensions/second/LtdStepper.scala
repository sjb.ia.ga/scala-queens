package queens
package dimensions.second

import base.Board

import common.stepper.{ LtdStepperForwardLenient => LtdStepperType }
import common.stepper.StepperFactory
import common.stepper.LtdStepper.ForwardLimitReached
import common.geom.Coord
import common.geom.{ row, col }


abstract trait LtdStepper[T, S <: LtdStepperType[T], F <: StepperFactory[T, S]]
    extends Stepper[T, S, F]
    with Queens[Coord[S]]
    with callbacks.BorderCallbacks[Coord[S]]:

  inline private def isBorder(s: S): Boolean =
    try
      s.forward.backward
      false
    catch
      case ForwardLimitReached => true

  inline override protected def isBoardBorder(using q: Coord[S])
                                             (using Board): Boolean =
    isBorder(q.col)

  inline override protected def isBoardBottom(using q: Coord[S])
                                             (using Board): Boolean =
    isBorder(q.row)

