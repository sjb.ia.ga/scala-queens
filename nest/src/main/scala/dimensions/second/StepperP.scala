package queens
package dimensions.second

import common.geom.Coord


import common.stepper.{ StepperP => StepperPType }
import common.stepper.BoardStepperFactory

abstract trait StepperP
    extends first.StepperP
    with Queens[Coord[StepperPType]]
    with BoardStepper[StepperPType, BoardStepperFactory[StepperPType]]


package `*` {

  import common.stepper.dup.{ StepperPDup => `StepperPType*` }
  import common.stepper.dup.{ BoardStepperDupFactory => `BoardStepperFactory*` }

  abstract trait `StepperP*`
      extends first.`*`.`StepperP*`
      with Queens[Coord[`StepperPType*`]]
      with BoardStepper[`StepperPType*`, `BoardStepperFactory*`[`StepperPType*`]]


}
