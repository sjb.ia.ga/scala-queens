package queens
package dimensions.second

import common.stepper.{ Stepper => StepperType }
import common.stepper.StepperFactory


abstract trait Stepper[T, S <: StepperType[T], F <: StepperFactory[T, S]]:

  protected def f: F
