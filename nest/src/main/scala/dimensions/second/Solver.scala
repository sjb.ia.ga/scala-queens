package queens
package dimensions.second

import base.Board


abstract trait Solver[T]:

  protected final type At = T

  protected sealed class QueensThrowable extends Throwable

  protected case object MaxSolutionsReached extends QueensThrowable

  protected case object AlgorithmInterrupted extends QueensThrowable

  protected case object ComputationSuspended extends QueensThrowable

  protected case object SuspendNotSupported extends QueensThrowable

  protected case object ResumeNotImplemented extends QueensThrowable

  protected case class AlgorithmError(throwable: Throwable) extends QueensThrowable

  protected def solve(using Long, Either[Load, At])
                     (using Board)
                     (using Either[Save, Solution] => Boolean): Option[Boolean]
