package queens
package dimensions.second.third
package kafka


abstract trait Queens[At]
    extends dimensions.second.Queens[At]
    with dimensions.third.first.kafka.Queens[At]
