package queens
package dimensions.second.third
package flow


abstract trait Queens[At]
    extends dimensions.second.Queens[At]
    with dimensions.third.first.flow.Queens[At]
