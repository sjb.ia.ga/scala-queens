package queens
package dimensions.second.third
package flow


abstract class AbstractQueens[At]
    extends Queens[At]:

  override def toString(): String = tags mkString " "
