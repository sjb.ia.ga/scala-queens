package queens
package dimensions.second.third
package flow

import common.geom.Coord
import common.stepper.{ BoardStepper => BoardStepperType }
import common.stepper.BoardStepperFactory


abstract class AbstractQueensBoardStepper[
  S <: BoardStepperType,
  F <: BoardStepperFactory[S]
](using override val f: F)
    extends AbstractQueens[Coord[S]]
    with dimensions.second.BoardStepper[S, F]
