package queens
package dimensions.second.third
package futures


abstract trait Queens[At]
    extends dimensions.second.Queens[At]
    with dimensions.third.first.futures.Queens[At]
