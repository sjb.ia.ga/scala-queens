package queens
package dimensions.second.third.first
package iterative
package flow


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.iterative.ClassicCb
