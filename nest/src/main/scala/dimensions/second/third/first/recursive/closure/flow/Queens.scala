package queens
package dimensions.second.third.first
package recursive
package closure
package flow


abstract trait Queens[At]
    extends dimensions.second.recursive.closure.Queens[At]
    with dimensions.second.third.flow.Queens[At]
