package queens
package dimensions.second.third.first
package recursive
package native
package kafka


abstract trait Queens[At]
    extends dimensions.second.recursive.native.Queens[At]
    with dimensions.second.third.kafka.Queens[At]
