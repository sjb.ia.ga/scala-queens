package queens
package dimensions.second.third.first
package recursive
package continuation
package futures

import common.stepper.{ StepperP => StepperPType }
import common.stepper.BoardStepperFactory


abstract class AbstractQueensStepperP(using BoardStepperFactory[StepperPType])
    extends AbstractQueensBoardStepper[StepperPType, BoardStepperFactory[StepperPType]]
    with dimensions.second.recursive.continuation.StepperP
