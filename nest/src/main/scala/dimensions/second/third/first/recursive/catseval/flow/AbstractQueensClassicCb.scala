package queens
package dimensions.second.third.first
package recursive
package catseval
package flow


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.recursive.catseval.ClassicCb
