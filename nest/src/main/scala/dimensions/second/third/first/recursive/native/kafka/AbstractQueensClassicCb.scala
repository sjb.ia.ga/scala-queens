package queens
package dimensions.second.third.first
package recursive
package native
package kafka


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.recursive.native.ClassicCb
