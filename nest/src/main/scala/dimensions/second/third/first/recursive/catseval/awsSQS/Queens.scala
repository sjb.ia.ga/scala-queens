package queens
package dimensions.second.third.first
package recursive
package catseval
package awsSQS


abstract trait Queens[At]
    extends dimensions.second.recursive.catseval.Queens[At]
    with dimensions.second.third.awsSQS.Queens[At]
