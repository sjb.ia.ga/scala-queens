package queens
package dimensions.second.third.first
package recursive
package extern
package rabbitMQ


abstract trait Queens[At]
    extends dimensions.second.recursive.extern.Queens[At]
    with dimensions.second.third.rabbitMQ.Queens[At]
