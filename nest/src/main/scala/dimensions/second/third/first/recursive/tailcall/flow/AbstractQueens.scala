package queens
package dimensions.second.third.first
package recursive
package tailcall
package flow


abstract class AbstractQueens[At]
    extends dimensions.second.third.flow.AbstractQueens[At]
    with Queens[At]
