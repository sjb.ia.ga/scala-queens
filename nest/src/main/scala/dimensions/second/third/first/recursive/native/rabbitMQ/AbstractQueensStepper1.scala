package queens
package dimensions.second.third.first
package recursive
package native
package rabbitMQ

import common.stepper.{ Stepper1 => Stepper1Type }
import common.stepper.BoardStepperFactory


abstract class AbstractQueensStepper1(using BoardStepperFactory[Stepper1Type])
    extends AbstractQueensBoardStepper[Stepper1Type, BoardStepperFactory[Stepper1Type]]
    with dimensions.second.recursive.native.Stepper1
