package queens
package dimensions.second.third.first
package recursive
package catseval
package rabbitMQ


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.recursive.catseval.ClassicCb
