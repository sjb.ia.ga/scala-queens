package queens
package dimensions.second.third.first
package recursive
package extern
package futures


abstract class AbstractQueensClassic
    extends AbstractQueens[Point]
    with dimensions.second.recursive.extern.Classic
