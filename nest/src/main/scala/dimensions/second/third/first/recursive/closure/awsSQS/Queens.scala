package queens
package dimensions.second.third.first
package recursive
package closure
package awsSQS


abstract trait Queens[At]
    extends dimensions.second.recursive.closure.Queens[At]
    with dimensions.second.third.awsSQS.Queens[At]
