package queens
package dimensions.second.third.first
package recursive
package continuation
package awsSQS


abstract trait Queens[At]
    extends dimensions.second.recursive.continuation.Queens[At]
    with dimensions.second.third.awsSQS.Queens[At]
