package queens
package dimensions.second.third.first
package iterative
package awsSQS


abstract class AbstractQueens[At]
    extends dimensions.second.third.awsSQS.AbstractQueens[At]
    with Queens[At]
