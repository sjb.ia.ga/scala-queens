package queens
package dimensions.second.third.first
package recursive
package native
package futures


abstract trait Queens[At]
    extends dimensions.second.recursive.native.Queens[At]
    with dimensions.second.third.futures.Queens[At]
