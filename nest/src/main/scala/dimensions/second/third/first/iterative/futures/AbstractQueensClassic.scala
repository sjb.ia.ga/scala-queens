package queens
package dimensions.second.third.first
package iterative
package futures


abstract class AbstractQueensClassic
    extends AbstractQueens[Point]
    with dimensions.second.iterative.Classic
