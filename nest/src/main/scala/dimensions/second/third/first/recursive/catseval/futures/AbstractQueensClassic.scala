package queens
package dimensions.second.third.first
package recursive
package catseval
package futures


abstract class AbstractQueensClassic
    extends AbstractQueens[Point]
    with dimensions.second.recursive.catseval.Classic
