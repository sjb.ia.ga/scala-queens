package queens
package dimensions.second.third.first
package recursive
package closure
package flow


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.recursive.closure.ClassicCb
