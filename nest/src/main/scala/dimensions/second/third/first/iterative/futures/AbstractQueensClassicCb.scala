package queens
package dimensions.second.third.first
package iterative
package futures


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.iterative.ClassicCb
