package queens
package dimensions.second.third.first
package iterative
package rabbitMQ


abstract trait Queens[At]
    extends dimensions.second.iterative.Queens[At]
    with dimensions.second.third.rabbitMQ.Queens[At]
