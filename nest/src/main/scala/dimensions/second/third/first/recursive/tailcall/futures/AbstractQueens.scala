package queens
package dimensions.second.third.first
package recursive
package tailcall
package futures


abstract class AbstractQueens[At]
    extends dimensions.second.third.futures.AbstractQueens[At]
    with Queens[At]
