package queens
package dimensions.second.third.first
package iterative
package futures


abstract trait Queens[At]
    extends dimensions.second.iterative.Queens[At]
    with dimensions.second.third.futures.Queens[At]
