package queens
package dimensions.second.third.first
package recursive
package extern
package kafka


abstract trait Queens[At]
    extends dimensions.second.recursive.extern.Queens[At]
    with dimensions.second.third.kafka.Queens[At]
