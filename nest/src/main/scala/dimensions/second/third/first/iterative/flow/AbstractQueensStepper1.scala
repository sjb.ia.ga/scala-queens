package queens
package dimensions.second.third.first
package iterative
package flow

import common.stepper.dup.{ Stepper1Dup => Stepper1Type }
import common.stepper.dup.{ BoardStepperDupFactory => BoardStepperFactory }


abstract class AbstractQueensStepper1(using BoardStepperFactory[Stepper1Type])
    extends AbstractQueensBoardStepper[Stepper1Type, BoardStepperFactory[Stepper1Type]]
    with dimensions.second.iterative.`Stepper1*`
