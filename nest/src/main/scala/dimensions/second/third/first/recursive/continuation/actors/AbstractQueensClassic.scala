package queens
package dimensions.second.third.first
package recursive
package continuation
package actors


abstract class AbstractQueensClassic
    extends AbstractQueens[Point]
    with dimensions.second.recursive.continuation.Classic
