package queens
package dimensions.second.third.first
package recursive
package tailcall
package actors


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.recursive.tailcall.ClassicCb
