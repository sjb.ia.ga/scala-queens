package queens
package dimensions.second.third.first
package recursive
package tailcall
package rabbitMQ


abstract trait Queens[At]
    extends dimensions.second.recursive.tailcall.Queens[At]
    with dimensions.second.third.rabbitMQ.Queens[At]
