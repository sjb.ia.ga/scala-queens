package queens
package dimensions.second.third.first
package recursive
package native
package kafka

import common.stepper.{ BoardStepper => BoardStepperType }
import common.stepper.BoardStepperFactory
import common.geom.Coord


abstract class AbstractQueensBoardStepper[
  S <: BoardStepperType,
  F <: BoardStepperFactory[S]
](using F)
    extends dimensions.second.third.kafka.AbstractQueensBoardStepper[S, F]
    with Queens[Coord[S]]
    with dimensions.second.recursive.native.BoardStepper[S, F]
