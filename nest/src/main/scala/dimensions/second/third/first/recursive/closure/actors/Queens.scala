package queens
package dimensions.second.third.first
package recursive
package closure
package actors


abstract trait Queens[At]
    extends dimensions.second.recursive.closure.Queens[At]
    with dimensions.second.third.actors.Queens[At]
