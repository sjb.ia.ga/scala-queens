package queens
package dimensions.second.third.first
package iterative
package flow


abstract trait Queens[At]
    extends dimensions.second.iterative.Queens[At]
    with dimensions.second.third.flow.Queens[At]
