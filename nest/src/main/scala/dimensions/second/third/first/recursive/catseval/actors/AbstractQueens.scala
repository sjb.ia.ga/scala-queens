package queens
package dimensions.second.third.first
package recursive
package catseval
package actors


abstract class AbstractQueens[At]
    extends dimensions.second.third.actors.AbstractQueens[At]
    with Queens[At]
