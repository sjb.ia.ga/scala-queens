package queens
package dimensions.second.third.first
package recursive
package catseval
package futures


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.recursive.catseval.ClassicCb
