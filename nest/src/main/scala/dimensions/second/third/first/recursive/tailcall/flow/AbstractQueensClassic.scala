package queens
package dimensions.second.third.first
package recursive
package tailcall
package flow


abstract class AbstractQueensClassic
    extends AbstractQueens[Point]
    with dimensions.second.recursive.tailcall.Classic
