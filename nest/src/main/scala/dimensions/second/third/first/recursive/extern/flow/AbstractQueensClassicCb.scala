package queens
package dimensions.second.third.first
package recursive
package extern
package flow


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.recursive.extern.ClassicCb
