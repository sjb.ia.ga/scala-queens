package queens
package dimensions.second.third.first
package iterative
package actors


abstract trait Queens[At]
    extends dimensions.second.iterative.Queens[At]
    with dimensions.second.third.actors.Queens[At]
