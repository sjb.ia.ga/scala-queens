package queens
package dimensions.second.third.first
package recursive
package catseval
package actors


abstract trait Queens[At]
    extends dimensions.second.recursive.catseval.Queens[At]
    with dimensions.second.third.actors.Queens[At]
