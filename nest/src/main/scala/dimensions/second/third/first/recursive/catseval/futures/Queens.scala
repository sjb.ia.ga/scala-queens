package queens
package dimensions.second.third.first
package recursive
package catseval
package futures


abstract trait Queens[At]
    extends dimensions.second.recursive.catseval.Queens[At]
    with dimensions.second.third.futures.Queens[At]
