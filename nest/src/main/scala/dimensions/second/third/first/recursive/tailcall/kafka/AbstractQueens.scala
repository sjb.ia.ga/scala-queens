package queens
package dimensions.second.third.first
package recursive
package tailcall
package kafka


abstract class AbstractQueens[At]
    extends dimensions.second.third.kafka.AbstractQueens[At]
    with Queens[At]
