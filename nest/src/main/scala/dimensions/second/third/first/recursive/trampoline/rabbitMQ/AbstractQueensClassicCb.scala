package queens
package dimensions.second.third.first
package recursive
package trampoline
package rabbitMQ


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.recursive.trampoline.ClassicCb
