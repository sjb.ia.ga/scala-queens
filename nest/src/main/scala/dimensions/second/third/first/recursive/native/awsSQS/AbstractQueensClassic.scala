package queens
package dimensions.second.third.first
package recursive
package native
package awsSQS


abstract class AbstractQueensClassic
    extends AbstractQueens[Point]
    with dimensions.second.recursive.native.Classic
