package queens
package dimensions.second.third.first
package recursive
package tailcall
package kafka


abstract trait Queens[At]
    extends dimensions.second.recursive.tailcall.Queens[At]
    with dimensions.second.third.kafka.Queens[At]
