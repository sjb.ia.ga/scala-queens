package queens
package dimensions.second.third.first
package recursive
package trampoline
package kafka


abstract class AbstractQueensClassic
    extends AbstractQueens[Point]
    with dimensions.second.recursive.trampoline.Classic
