package queens
package dimensions.second.third.first
package recursive
package continuation
package futures


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.recursive.continuation.ClassicCb
