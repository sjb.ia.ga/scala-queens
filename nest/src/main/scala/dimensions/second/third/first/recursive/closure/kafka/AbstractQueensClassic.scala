package queens
package dimensions.second.third.first
package recursive
package closure
package kafka


abstract class AbstractQueensClassic
    extends AbstractQueens[Point]
    with dimensions.second.recursive.closure.Classic
