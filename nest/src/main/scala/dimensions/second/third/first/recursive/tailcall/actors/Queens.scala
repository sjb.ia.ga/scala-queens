package queens
package dimensions.second.third.first
package recursive
package tailcall
package actors


abstract trait Queens[At]
    extends dimensions.second.recursive.tailcall.Queens[At]
    with dimensions.second.third.actors.Queens[At]
