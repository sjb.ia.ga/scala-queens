package queens
package dimensions.second.third.first
package recursive
package extern
package awsSQS

import common.stepper.{ BoardStepper => BoardStepperType }
import common.stepper.BoardStepperFactory
import common.geom.Coord


abstract class AbstractQueensBoardStepper[
  S <: BoardStepperType,
  F <: BoardStepperFactory[S],
](using F)
    extends dimensions.second.third.awsSQS.AbstractQueensBoardStepper[S, F]
    with Queens[Coord[S]]
    with dimensions.second.recursive.extern.BoardStepper[S, F]
