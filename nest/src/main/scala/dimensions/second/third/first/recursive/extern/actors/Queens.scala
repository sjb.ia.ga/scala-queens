package queens
package dimensions.second.third.first
package recursive
package extern
package actors


abstract trait Queens[At]
    extends dimensions.second.recursive.extern.Queens[At]
    with dimensions.second.third.actors.Queens[At]
