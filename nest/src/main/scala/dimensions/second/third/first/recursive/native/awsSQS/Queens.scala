package queens
package dimensions.second.third.first
package recursive
package native
package awsSQS


abstract trait Queens[At]
    extends dimensions.second.recursive.native.Queens[At]
    with dimensions.second.third.awsSQS.Queens[At]
