package queens
package dimensions.second.third.first
package recursive
package native
package rabbitMQ


abstract class AbstractQueens[At]
    extends dimensions.second.third.rabbitMQ.AbstractQueens[At]
    with Queens[At]
