package queens
package dimensions.second.third.first
package recursive
package tailcall
package awsSQS


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.recursive.tailcall.ClassicCb
