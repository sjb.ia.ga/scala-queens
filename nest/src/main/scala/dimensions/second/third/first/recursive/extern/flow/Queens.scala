package queens
package dimensions.second.third.first
package recursive
package extern
package flow


abstract trait Queens[At]
    extends dimensions.second.recursive.extern.Queens[At]
    with dimensions.second.third.flow.Queens[At]
