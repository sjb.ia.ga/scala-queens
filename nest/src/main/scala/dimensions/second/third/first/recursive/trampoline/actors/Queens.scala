package queens
package dimensions.second.third.first
package recursive
package trampoline
package actors


abstract trait Queens[At]
    extends dimensions.second.recursive.trampoline.Queens[At]
    with dimensions.second.third.actors.Queens[At]
