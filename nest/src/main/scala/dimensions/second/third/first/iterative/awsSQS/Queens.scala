package queens
package dimensions.second.third.first
package iterative
package awsSQS


abstract trait Queens[At]
    extends dimensions.second.iterative.Queens[At]
    with dimensions.second.third.awsSQS.Queens[At]
