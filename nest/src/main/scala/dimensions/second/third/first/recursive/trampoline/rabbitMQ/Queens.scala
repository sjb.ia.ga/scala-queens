package queens
package dimensions.second.third.first
package recursive
package trampoline
package rabbitMQ


abstract trait Queens[At]
    extends dimensions.second.recursive.trampoline.Queens[At]
    with dimensions.second.third.rabbitMQ.Queens[At]
