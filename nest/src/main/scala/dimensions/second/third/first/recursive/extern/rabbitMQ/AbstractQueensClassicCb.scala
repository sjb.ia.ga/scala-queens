package queens
package dimensions.second.third.first
package recursive
package extern
package rabbitMQ


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.recursive.extern.ClassicCb
