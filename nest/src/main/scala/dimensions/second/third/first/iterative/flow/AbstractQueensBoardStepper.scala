package queens
package dimensions.second.third.first
package iterative
package flow

import common.stepper.dup.{ BoardStepperDup => BoardStepperType }
import common.stepper.dup.{ BoardStepperDupFactory => BoardStepperFactory }
import common.geom.Coord


abstract class AbstractQueensBoardStepper[
  S <: BoardStepperType,
  F <: BoardStepperFactory[S],
](using F)
    extends dimensions.second.third.flow.AbstractQueensBoardStepper[S, F]
    with Queens[Coord[S]]
    with dimensions.second.iterative.`BoardStepper*`[S, F]
