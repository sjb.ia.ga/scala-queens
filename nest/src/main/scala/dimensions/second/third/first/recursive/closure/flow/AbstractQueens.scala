package queens
package dimensions.second.third.first
package recursive
package closure
package flow


abstract class AbstractQueens[At]
    extends dimensions.second.third.flow.AbstractQueens[At]
    with Queens[At]
