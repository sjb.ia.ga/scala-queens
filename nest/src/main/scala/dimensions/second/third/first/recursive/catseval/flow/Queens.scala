package queens
package dimensions.second.third.first
package recursive
package catseval
package flow


abstract trait Queens[At]
    extends dimensions.second.recursive.catseval.Queens[At]
    with dimensions.second.third.flow.Queens[At]
