package queens
package dimensions.second.third.first
package recursive
package extern
package rabbitMQ

import common.stepper.{ BoardStepper => BoardStepperType }
import common.stepper.BoardStepperFactory
import common.geom.Coord


abstract class AbstractQueensBoardStepper[
  S <: BoardStepperType,
  F <: BoardStepperFactory[S],
](using F)
    extends dimensions.second.third.rabbitMQ.AbstractQueensBoardStepper[S, F]
    with Queens[Coord[S]]
    with dimensions.second.recursive.extern.BoardStepper[S, F]
