package queens
package dimensions.second.third.first
package iterative
package rabbitMQ


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.iterative.ClassicCb
