package queens
package dimensions.second.third.first
package recursive
package tailcall
package awsSQS


abstract trait Queens[At]
    extends dimensions.second.recursive.tailcall.Queens[At]
    with dimensions.second.third.awsSQS.Queens[At]
