package queens
package dimensions.second.third.first
package iterative
package rabbitMQ

import common.stepper.dup.{ StepperPDup => StepperPType }
import common.stepper.dup.{ BoardStepperDupFactory => BoardStepperFactory }


abstract class AbstractQueensStepperP(using BoardStepperFactory[StepperPType])
    extends AbstractQueensBoardStepper[StepperPType, BoardStepperFactory[StepperPType]]
    with dimensions.second.iterative.`StepperP*`
