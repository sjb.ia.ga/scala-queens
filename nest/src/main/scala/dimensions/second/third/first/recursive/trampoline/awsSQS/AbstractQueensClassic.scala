package queens
package dimensions.second.third.first
package recursive
package trampoline
package awsSQS


abstract class AbstractQueensClassic
    extends AbstractQueens[Point]
    with dimensions.second.recursive.trampoline.Classic
