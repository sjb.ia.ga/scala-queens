package queens
package dimensions.second.third.first
package iterative
package kafka


abstract trait Queens[At]
    extends dimensions.second.iterative.Queens[At]
    with dimensions.second.third.kafka.Queens[At]
