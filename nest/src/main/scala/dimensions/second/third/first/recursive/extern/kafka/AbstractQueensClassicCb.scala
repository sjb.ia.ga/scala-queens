package queens
package dimensions.second.third.first
package recursive
package extern
package kafka


abstract class AbstractQueensClassicCb
    extends AbstractQueens[Point]
    with dimensions.second.recursive.extern.ClassicCb
