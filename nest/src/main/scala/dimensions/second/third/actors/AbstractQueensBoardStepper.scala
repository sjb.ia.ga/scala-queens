package queens
package dimensions.second.third
package actors

import common.stepper.{ BoardStepper => BoardStepperType }
import common.stepper.BoardStepperFactory
import common.geom.Coord


abstract class AbstractQueensBoardStepper[
  S <: BoardStepperType,
  F <: BoardStepperFactory[S]
](using override val f: F)
    extends AbstractQueens[Coord[S]]
    with dimensions.second.BoardStepper[S, F]
