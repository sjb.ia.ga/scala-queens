package queens
package dimensions.second.third
package actors


abstract trait Queens[At]
    extends dimensions.second.Queens[At]
    with dimensions.third.first.actors.Queens[At]
