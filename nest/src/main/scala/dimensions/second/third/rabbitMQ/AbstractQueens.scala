package queens
package dimensions.second.third
package rabbitMQ


abstract class AbstractQueens[At]
    extends Queens[At]:

  override def toString(): String = tags mkString " "
