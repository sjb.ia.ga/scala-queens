package queens
package dimensions.second.third
package rabbitMQ


abstract trait Queens[At]
    extends dimensions.second.Queens[At]
    with dimensions.third.first.rabbitMQ.Queens[At]
