package queens
package dimensions.second.third
package awsSQS


abstract trait Queens[At]
    extends dimensions.second.Queens[At]
    with dimensions.third.first.awsSQS.Queens[At]
