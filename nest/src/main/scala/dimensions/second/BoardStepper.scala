package queens
package dimensions.second


import common.stepper.{ BoardStepper => BoardStepperType }
import common.stepper.BoardStepperFactory

abstract trait BoardStepper[S <: BoardStepperType, F <: BoardStepperFactory[S]]
    extends LtdStepper[Int, S, F]


package `*` {

  import common.stepper.dup.{ BoardStepperDup => `BoardStepperType*` }
  import common.stepper.dup.{ BoardStepperDupFactory => `BoardStepperFactory*` }

  abstract trait `BoardStepper*`[S <: `BoardStepperType*`, F <: `BoardStepperFactory*`[S]]
      extends LtdStepper[Int, S, F]

}
