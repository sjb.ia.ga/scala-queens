package queens
package dimensions.second

import common.geom.Coord


import common.stepper.{ Stepper1 => Stepper1Type }
import common.stepper.BoardStepperFactory

abstract trait Stepper1
    extends first.Stepper1
    with Queens[Coord[Stepper1Type]]
    with BoardStepper[Stepper1Type, BoardStepperFactory[Stepper1Type]]


package `*` {

  import common.stepper.dup.{ Stepper1Dup => `Stepper1Type*` }
  import common.stepper.dup.{ BoardStepperDupFactory => `BoardStepperFactory*` }

  abstract trait `Stepper1*`
      extends first.`*`.`Stepper1*`
      with Queens[Coord[`Stepper1Type*`]]
      with BoardStepper[`Stepper1Type*`, `BoardStepperFactory*`[`Stepper1Type*`]]

}
