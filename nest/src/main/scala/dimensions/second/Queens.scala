package queens
package dimensions.second

import base.Board
import dimensions.Dimension.Algorithm


abstract trait Queens[At]
    extends base.Queens:

  val algorithm: Algorithm

  abstract override protected def tags: List[String] =
    algorithm.toString() :: super.tags


abstract trait `Queens*`[At]
    extends cache.Queens
    with base.BoardChecker
    with Solver[At]:

  override protected def solve(using M: Long, start: Either[Load, At])
                              (using base.Board)
                              (using pragma: Either[Save, Solution] => Boolean): Option[Boolean] =
    cacheOps
      .get(cacheKey) match

        case Some(solutions) =>

          try
            if !pragma(Left({ _ => throw SuspendNotSupported })) then
              throw AlgorithmInterrupted

            var maxSolutions = M

            solutions.foreach { it =>

              maxSolutions -= 1

              if !pragma(Right(it)) then
                throw AlgorithmInterrupted

              if maxSolutions == 0 then
                throw MaxSolutionsReached
            }

            Some(true)
          catch
            case MaxSolutionsReached | AlgorithmInterrupted | SuspendNotSupported | ResumeNotImplemented =>
              Some(false)

            case AlgorithmError(t) =>
              throw t

        case _ =>
          None
