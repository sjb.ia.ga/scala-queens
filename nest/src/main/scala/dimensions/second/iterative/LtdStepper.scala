package queens
package dimensions.second
package iterative

import java.io.ObjectOutputStream

import scala.collection.mutable.Queue

import base.Board

import common.stepper.dup.{ LtdStepperForwardLenientDup => LtdStepperType }
import common.stepper.StepperFactory
import common.stepper.{ x, given }
import common.geom.{ row, col }


abstract trait `LtdStepper*`[T, S <: LtdStepperType[T], F <: StepperFactory[T, S]]
    extends Stepper[T, S, F]
    with LtdStepper[T, S, F]:

  import `LtdStepper*`._

  extension (q: At)
    inline private def dup: At =
      q.row.dup x q.col.dup
    inline private def `+1 x 0`(using board: Board): At =
      val p: At = q.row.dup x f(board.N)(using board.seed)
      p.row.forward
      p


  override protected def queens(using M: Long, thaw: Either[Load, At])
                               (using board: Board)
                               (using Either[Save, Solution] => Boolean): Unit =
    var maxSolutions = M
    given (() => Long) = { () => maxSolutions -= 1; maxSolutions }

    var top: El[At] = null

    // Queue

    val lsqueue = Queue[El[At]]()

    inline def call(k: Int, q: At, iterate: Boolean = true)
             (using currentSolution: PartialSolution): Unit =
      lsqueue.enqueue(k -> Recursion(q, currentSolution, iterate))


    implicit var resume: Boolean = false
    var stream: ObjectOutputStream = null
    implicit val save: (Option[ObjectOutputStream] => Boolean) = _ match
      case Some(it) =>
        stream = it
        it.writeLong(maxSolutions)
        lsqueue.insert(0, top)
        it.writeInt(lsqueue.size)
        while lsqueue.nonEmpty do
          stream.writeObject(lsqueue.dequeue)
        true
      case _ =>
        stream ne null

    thaw match
      case Left((stream, end)) =>
        maxSolutions = stream.readLong()
        for
          _ <- 1 to stream.readInt()
        do
          lsqueue.enqueue(stream.readObject().asInstanceOf[El[At]])
        end()
        resume = true
      case Right(start) =>
        call(board.N, start)(using Nil)


    def lsqueens(k: Int)(using Recursion[At]): Unit =
      val Recursion(q, currentSolution, iterate) = implicitly[Recursion[At]]

      given At = q
      given PartialSolution = currentSolution

      if onIterate(k)
      then

        if isBoardBorder
        then
          call(k, q.`+1 x 0`)
        else
          val p: Point = q

          q.col.forward

          if !check(p)
          then
            given PartialSolution = p :: currentSolution
            call(k - 1, q.dup)

          while iterate
          do
            call(k, q.dup, false)
            if isBoardBorder
            then
              return
            else
              q.col.forward
      resume = false

    while lsqueue.nonEmpty
    do
      top = lsqueue.dequeue
      implicit val (k, it) = top
      lsqueens(k)


private object `LtdStepper*`:

  type El[At] = (Int, Recursion[At])

  case class Recursion[At](q: At, currentSolution: PartialSolution, iterate: Boolean)
