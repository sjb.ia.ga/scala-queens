package queens
package dimensions.second
package iterative

import common.stepper.dup.{ BoardStepperDup => BoardStepperType }
import common.stepper.dup.BoardStepperDupFactory


abstract trait `BoardStepper*`[S <: BoardStepperType, F <: BoardStepperDupFactory[S]]
    extends `LtdStepper*`[Int, S, F]
    with dimensions.second.`*`.`BoardStepper*`[S, F]
