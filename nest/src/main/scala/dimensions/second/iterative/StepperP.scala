package queens
package dimensions.second
package iterative

import common.stepper.dup.{ StepperPDup => `StepperPType*` }
import common.stepper.dup.{ BoardStepperDupFactory => `BoardStepperFactory*` }


abstract trait `StepperP*`
    extends `BoardStepper*`[`StepperPType*`, `BoardStepperFactory*`[`StepperPType*`]]
    with *.`StepperP*`
