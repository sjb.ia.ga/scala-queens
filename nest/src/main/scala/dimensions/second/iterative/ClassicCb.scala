package queens
package dimensions.second
package iterative

import java.io.ObjectOutputStream

import scala.collection.mutable.Stack

import base.Board


abstract trait ClassicCb
    extends QueensWithCallbacks[Int]
    with dimensions.second.callbacks.PointBorderCallbacks
    with first.ClassicCb:

  override protected def queens(using M: Long, thaw: Either[Load, At])
                               (using board: Board)
                               (using Either[Save, Solution] => Boolean): Unit =
    var maxSolutions = M
    given (() => Long) = { () => maxSolutions -= 1; maxSolutions }

    var cbqueens: Stack[(Int, At, PartialSolution)] = null

    var top: (Int, At, PartialSolution) = null

    implicit var resume: Boolean = false
    var stream: ObjectOutputStream = null
    implicit val save: (Option[ObjectOutputStream] => Boolean) = _ match
      case Some(it) =>
        stream = it
        it.writeLong(maxSolutions)
        it.writeObject(cbqueens.push(top))
        true
      case _ =>
        stream ne null

    thaw match
      case Left((stream, end)) =>
        maxSolutions = stream.readLong()
        cbqueens = stream.readObject().asInstanceOf[Stack[(Int, At, PartialSolution)]]
        end()
        resume = true
      case Right(start) =>
        cbqueens = Stack[(Int, At, PartialSolution)]()
        cbqueens.push((board.N, start, Nil))

    while cbqueens.nonEmpty
    do
      import common.geom.{ row, col, x }

      top = cbqueens.pop
      implicit var (k, q, currentSolution) = top

      inline def stack: Unit = cbqueens.push((k, q, currentSolution))

      if onIterate(k)
      then

        if isBoardBorder
        then
          q = q.row + 1 x 0
          stack
        else
          val p = q

          q = q.row x q.col + 1
          stack

          if !check(p)
          then
            currentSolution = p :: currentSolution
            k -= 1
            stack

      resume = false
