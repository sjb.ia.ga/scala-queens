package queens
package dimensions.second
package iterative

import common.stepper.dup.{ Stepper1Dup => `Stepper1Type*` }
import common.stepper.dup.{ BoardStepperDupFactory => `BoardStepperFactory*` }


abstract trait `Stepper1*`
    extends `BoardStepper*`[`Stepper1Type*`, `BoardStepperFactory*`[`Stepper1Type*`]]
    with *.`Stepper1*`
