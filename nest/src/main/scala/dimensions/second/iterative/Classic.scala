package queens
package dimensions.second
package iterative

import java.io.ObjectOutputStream

import scala.collection.mutable.Stack

import base.Board


abstract trait Classic
    extends Queens[Point]
    with first.Classic:

  override protected def queens(using M: Long, thaw: Either[Load, At])
                               (using board: Board)
                               (using pragma: Either[Save, Solution] => Boolean): Unit =
    var maxSolutions = M

    var cqueens: Stack[(Int, At, PartialSolution)] = null

    var top: (Int, At, PartialSolution) = null

    var resume: Boolean = false
    var stream: ObjectOutputStream = null
    implicit val save: (Option[ObjectOutputStream] => Boolean) = _ match
      case Some(it) =>
        stream = it
        it.writeLong(maxSolutions)
        it.writeObject(cqueens.push(top))
        true
      case _ =>
        stream ne null

    thaw match
      case Left((stream, end)) =>
        maxSolutions = stream.readLong()
        cqueens = stream.readObject().asInstanceOf[Stack[(Int, At, PartialSolution)]]
        end()
        resume = true
      case Right(start) =>
        cqueens = Stack[(Int, At, PartialSolution)]()
        cqueens.push((board.N, start, Nil))

    while cqueens.nonEmpty
    do
      import common.geom.{ row, col, x }

      top = cqueens.pop
      var (k, q, currentSolution) = top

      inline def stack: Unit = cqueens.push((k, q, currentSolution))

      if !resume && !pragma(Left(save))
      then
        if save(None)
        then
          throw ComputationSuspended
        else
          throw AlgorithmInterrupted

      else if k == 0 || q.row == board.N
      then
        if k == 0 && currentSolution.length == board.N && validate(currentSolution)
        then
          maxSolutions -= 1

          if !pragma(Right(currentSolution)) then
            throw AlgorithmInterrupted

          if maxSolutions == 0 then
            throw MaxSolutionsReached

      else
        if q.col == board.N
        then
          q = q.row + 1 x 0
          stack
        else
          val p = q

          q = q.row x q.col + 1
          stack

          given PartialSolution = currentSolution

          if !check(p)
          then
            currentSolution = p :: currentSolution
            k -= 1
            stack

      resume = false
