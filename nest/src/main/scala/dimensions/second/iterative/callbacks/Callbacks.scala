package queens
package dimensions.second
package iterative
package callbacks

import base.Board


abstract trait Callbacks[At]:

  protected def onIterate(k: Int)
                         (using q: At)
                         (using PartialSolution, () => Long)
                         (using Board, Boolean, Save)
                         (using Either[Save, Solution] => Boolean): Boolean
