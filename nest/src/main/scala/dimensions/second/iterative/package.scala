package queens
package dimensions.second


package object iterative:

  private[iterative] type PartialSolution = List[Point]
