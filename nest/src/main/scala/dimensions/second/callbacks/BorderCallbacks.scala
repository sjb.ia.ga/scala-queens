package queens
package dimensions.second
package callbacks

import base.Board


abstract trait BorderCallbacks[At]:

  protected def isBoardBorder(using At)(using Board): Boolean

  protected def isBoardBottom(using At)(using Board): Boolean
