package queens
package dimensions.second
package callbacks

import base.Board

import common.geom.{ row, col }


abstract trait PointBorderCallbacks
    extends BorderCallbacks[Point]:

  inline override protected def isBoardBottom(using q: Point)
                                             (using board: Board): Boolean =
    q.row == board.N

  inline override protected def isBoardBorder(using q: Point)
                                             (using board: Board): Boolean =
    q.col == board.N
