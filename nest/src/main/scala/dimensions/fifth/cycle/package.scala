package queens
package dimensions.fifth

import scala.Function.const


package cycle:

  trait Simulation[S]
      extends (Generation => S)

  abstract case class Preemption()
      extends Simulation[Either[Long, Long]]

  abstract case class Suspension(preemption: Preemption)
      extends Simulation[Option[Simulation[?]]]:

    def apply(preemption: Preemption = this.preemption)(block: Generation => Option[Simulation[?]]): Suspension =
      new Suspension(preemption):
        override def apply(generation: Generation): Option[Simulation[?]] = block(generation)

  object Simulation:

    object stop extends Preemption():
      override def apply(generation: Generation): Either[Long, Long] = Left(0)

  object Preemption:

    def step(n: Long = 1): Preemption = Preemption(const(Left(n)))

    def apply(block: Generation => Either[Long, Long]): Preemption =
      new Preemption():
        override def apply(generation: Generation): Either[Long, Long] = block(generation)

  object Suspension:

    def apply(preemption: Preemption = Simulation.stop)(block: Generation => Option[Simulation[?]]): Suspension =
      new Suspension(preemption):
        override def apply(generation: Generation): Option[Simulation[?]] = block(generation)


  object Simulate:

    def apply(suspension: Suspension): Simulate = Right(suspension)

    def apply(block: Generation => Either[Long, Long]): Simulate = Left(Preemption(block))


package object cycle:

  import java.util.UUID

  import base.Board

  import version.less.nest.Nest

  type IO = Board.IO[Nest]
  type Progress = (Option[Board], IO)
  type Generation = (Progress, ((Long, Any), Either[Long, Long]))

  type Simulate = Either[Preemption, Suspension]
