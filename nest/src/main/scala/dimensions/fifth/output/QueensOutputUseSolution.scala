package queens
package dimensions.fifth
package output


abstract trait QueensOutputUseSolution
    extends QueensUseSolution:

  override def toString(): String = "Output"
