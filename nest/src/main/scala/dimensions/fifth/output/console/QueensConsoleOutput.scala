package queens
package dimensions.fifth
package output.console

import common.pipeline.Context
import Context.Round

import common.pipeline.Metadata
import Metadata.{ Boot, Text }

import common.geom.x

import dimensions.third.Queens

import output.QueensOutputUseSolution


abstract trait QueensConsoleOutput
    extends QueensOutputUseSolution:

  protected type Q <: Queens[?]

  protected def queens: Q

  override protected def <<(metadata: Metadata): Option[Metadata] =
    metadata match
      case Boot(_, ctxt: Round, _) => Some(Text(ctxt, None))

  override protected val >> = QueensConsoleOutput()

  override def toString(): String = "Console :: " + super.toString + " :: " + queens


object QueensConsoleOutput:

  private def apply()
                   (metadata: Metadata)
                   (solution: Solution): Boolean = metadata match
    case Text(Round(board, tag, queens, number), _) =>
      Console
        .println(s"Solution #$number {${queens.toString.replace('_', ' ')}}\n" + {
          val hole = board((i, j) => if solution.exists((i x j).==) then "⊛" else " ")("⬡")
            .map(_ mkString " ")
          val vert = board((i, j) => if solution.exists((i x j).==) then "⊛" else "⋮")("↑")
            .map(_ mkString " ")
          val horz = board((i, j) => if solution.exists((i x j).==) then "⊛" else "⋯")("→")
            .map(_ mkString " ")
          val main = board((i, j) => if solution.exists((i x j).==) then "⊛" else "⋱")("↘")
            .map(_ mkString " ")
          val diag = board((i, j) => if solution.exists((i x j).==) then "⊛" else "⋰")("↗")
            .map(_ mkString " ")
          val orig = board((_, _) => "∴")("⬡")
            .map(_ mkString " ")

          for
            i <- 0 until board.N
          yield (hole(i) :: vert(i) :: horz(i) :: main(i) :: diag(i) :: orig(i) :: Nil) mkString "    ::    "
        }.mkString("\n", "\n", "\n"))
      true
    case _ => false


  import base.Board

  import version.less.nest.Nest

  class `QueensConsoleOutput*`(
    override val queens: `QueensConsoleOutput*`#Q
  ) extends QueensConsoleOutput
      with QueensUseSolution:

    override protected type Q = Queens[?]

    def this(nest: Nest) = this(`QueensConsoleOutput*`.`Queens*`(nest))


  private object `QueensConsoleOutput*`:

    class `Queens*`(nest: Nest) extends Queens[Point]:
        override val aspect = nest.aspect
        override val algorithm = nest.algorithm
        override val model = nest.model

        override def toString(): String = tags.mkString("/* ", " ", " */")


  import scala.collection.mutable.{ ListBuffer => List }
  import scala.concurrent.duration._
  import com.github.blemale.scaffeine.{ Scaffeine, Cache }

  private val cache: Cache[Int, List[(Queens[?], `QueensConsoleOutput*`)]] =
    Scaffeine()
      .recordStats()
      .expireAfterWrite(23.minutes)
      .initialCapacity(10 * 2 * 2 * 1 * 1)
      .maximumSize(1000 * 4 * 3 * 3 * 10)
      .build[Int, List[(Queens[?], `QueensConsoleOutput*`)]]()

  def apply(queens: Queens[?]): QueensUseSolution =
    if cache.getIfPresent(queens.##).map(_.size).getOrElse(0) > 69520
    then cache.invalidate(queens.##)
    val ls = cache
      .getIfPresent(queens.##)
      .getOrElse {
        val it = List[(Queens[?], `QueensConsoleOutput*`)]()
        cache.put(queens.##, it)
        it
      }
    ls.find(_._1 eq queens)
      .map(_._2)
      .getOrElse {
        val it = `QueensConsoleOutput*`(queens)
        ls += queens -> it
        it
      }

  def apply(ctxt: Context): QueensUseSolution = ctxt match
    case Round(_, _, qs, _) => QueensConsoleOutput(qs)

  def apply(nest: Nest): QueensUseSolution = `QueensConsoleOutput*`(nest)
