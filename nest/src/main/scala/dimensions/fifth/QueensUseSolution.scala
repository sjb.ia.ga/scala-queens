package queens
package dimensions.fifth

import common.Pipeline


abstract trait QueensUseSolution
    extends Pipeline[Solution]:

  override protected type `-|-` = QueensUseSolution
