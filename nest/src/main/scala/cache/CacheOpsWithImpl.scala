package queens
package cache

import base.CacheOps


class CacheOpsWithImpl(cache: Cache)
    extends CacheOps {

  override def put(key: Key, it: LazyList[Solution]): Unit =
    cache.put(key, it)

  override def get(key: Key): Option[LazyList[Solution]] =
    cache.getIfPresent(key)

  override def invalidate(key: Key): Unit =
    cache.invalidate(key)

  override val self = this

}
