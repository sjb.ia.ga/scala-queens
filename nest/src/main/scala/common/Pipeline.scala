package queens
package common

import scala.Function.const

import pipeline.{ Context, Metadata }


/**
  * @see [[queens.dimensions.fifth.QueensUseSolution]]
  * @see [[queens.dimensions.fourth.fifth.breed.query.`Query *'q`]]
  * @see [[queens.dimensions.fifth.fourth.third.web.simple.flow.QueensWebSimpleProgramByFlow]]
  * @see [[queens.axis.fourth.webapp.spring.`Spring Web Client`]]
  * @see [[queens.axis.fourth.webapp.spring.plainjava.Spring$Web$Client]]
  * @see [[queens.axis.fourth.webapp.retrofit.`Retrofit Web Client`]]
  * @see [[queens.axis.fourth.webapp.retrofit.plainjava.Retrofit$Web$Client]]
  */
abstract trait Pipeline[T]:

  protected type `-|-` <: Pipeline[T]

  type Element = Pipeline.Element[T]

  import Context.Empty

  protected def next: Option[Context => `-|-`] = None

  def ||(it: T, ctxt: Context = Empty, data: Option[Any] = None): Boolean =
    Pipeline
      .||(this, ctxt, data)
      .foldLeft(true) { case (stop, (fn, md)) => stop && fn(md)(it) }

  protected def *(slot: Option[Int]): Boolean = false

  protected def <<(metadata: Metadata): Option[Metadata] = Some(metadata)

  /**
    * @see [[queens.plane.fourth.fifth.webapp.output.retrofit.console.Main]]
    * @see [[queens.plane.fourth.fifth.webapp.output.spring.console.Main]]
    */
  protected def >> : Element = const(const(true))

  @inline protected def >>(feed: Element => Unit): Unit = feed(>>)


object Pipeline:

  type Element[T] = Metadata => T => Boolean


  import scala.annotation.tailrec
  import scala.collection.mutable.{ ListBuffer => MutableList }

  import Metadata.Boot

  private def ||[
    S,
    O <: Pipeline[S]
  ](head: O,
    ctxt: Context,
    data: Option[Any],
  ): Iterable[(Element[S], Metadata)] =
    val pipe = MutableList[(?, Metadata)]()

    @tailrec
    def *[ // traverse through [slot and] next
      T,
      P <: Pipeline[T]
    ](prev: Option[P],
      slot: Option[Int]
    ): Unit = prev match

      case Some(self) =>

        val slot1 =
          if self.*(slot)
          then
            slot.orElse(Some(0)).map(_+1)
          else
            None

        if slot.nonEmpty -> slot1.nonEmpty then
          val boot = Boot(slot1, ctxt, data)
          (self << boot).map { md => self >> (pipe += _ -> md) }

        val next = slot1
          .map(const(const(self)))
          .orElse(self.next)

        *(next.map(_(ctxt)), slot1)

      case _ =>

    *(Some(head), None)

    pipe.asInstanceOf[Iterable[(Element[S], Metadata)]]


  final case class Noop[T](ctxt: Context)
      extends Pipeline[T]:

    override protected def <<(metadata: Metadata): Option[Metadata] = None
