package queens
package common

import java.util.{ Date, UUID }
import java.io.{ FileInputStream, ObjectInputStream, FileOutputStream, ObjectOutputStream }
import java.nio.file.{ Files, FileSystems, Path }

import base.Board

import common.Macros.tryc

import version.less.nest.{ Nest, Wildcard }
import version.base.Multiplier


package object bis:

  type Cycle = Board.Cycle[Nest]


package bis:

  object Cycle:

    def unapply(self: Cycle): Option[(IO, Option[Path])] = Some(self.io -> self.image)

////////////////////////////////////////////////////////////////////////////////

    def apply()(using board: Board, M: Long): Cycle =
      new Cycle((board, null, M, null) -> None, null)()

    def apply(nests: Nest*)
       (using board: Board, M: Long): Cycle = create(nests.toList)

    def apply(wildcard: Wildcard,
              multiplier: Multiplier[Nest])
       (using board: Board, M: Long): Cycle = create(wildcard(multiplier))

    private def create(exp: List[Nest])
                (using board: Board, M: Long): Cycle =
      def cycle(i: Int): Cycle =
        if i == exp.size
        then
          null
        else
          val uuid = UUID.randomUUID
          new Cycle((board, exp(i), M, uuid) -> None, cycle(i+1))()
      cycle(0)

    def apply(cycle: Cycle): Cycle =
      if cycle eq null then null
      else new Cycle(cycle.io, apply(cycle.turn), cycle.image)(null, -1L)

////////////////////////////////////////////////////////////////////////////////

    extension(self: Cycle)
      def apply(tmpdir: Path, filename: String): Path =
        val bin = Files.createTempFile(tmpdir, filename, ".bin")
        self.image = Some(bin)
        self.since = Date()
        self.idle = 0L
        val fos = FileOutputStream(bin.toString)
        val oos = ObjectOutputStream(fos)
        try
          self.write(oos)
        finally
          tryc(oos.close)
          tryc(fos.close)
        bin
      inline def write(oos: ObjectOutputStream): Unit =
        var c = self
        var n = 0
        while c ne null do { c = c.turn; n += 1 }
        oos.writeInt(n)
        self.write(oos, n)
      private def write(oos: ObjectOutputStream, n: Int): Unit =
        oos.writeObject(self.io)
        if n - 1 > 0
        then
          self.turn.write(oos, n - 1)
        oos.writeObject(self.image.map(_.toString))
        oos.writeObject(self.since)
        oos.writeLong(self.idle)

////////////////////////////////////////////////////////////////////////////////

    extension(self: List[Nest])
      def apply(bin: Path): (Cycle, Option[Nest]) =
        val fis = FileInputStream(bin.toString)
        val ois = ObjectInputStream(fis)
        try
          val cycle = Cycle.read(ois)
          var c = cycle
          var n = 0
          while c ne null do { c = c.turn; n += 1 }
          val i = self.size - n
          cycle -> (if i < 0 then None else Some(self(i)))
        finally
          tryc(ois.close)
          tryc(fis.close)

    inline def read(ois: ObjectInputStream): Cycle =
      read(ois, ois.readInt)
    private def read(ois: ObjectInputStream, n: Int): Cycle =
      if n == 0 then return null
      new Cycle(ois.readObject().asInstanceOf[IO],
                read(ois, n-1),
                ois.readObject.asInstanceOf[Option[String]].map(FileSystems.getDefault.getPath(_))
              )(ois.readObject().asInstanceOf[Date],
                ois.readLong())
