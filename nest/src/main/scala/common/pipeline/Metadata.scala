package queens
package common
package pipeline


/**
  * Identifies @code [[queens.common.Pipeline]] elements.
  */
sealed abstract trait Metadata:
  val context: Context
  val data: Option[Any]

  override def toString() = data.map(_.toString).getOrElse("")


object Metadata:

  case class Boot(slot: Option[Int], override val context: Context, override val data: Option[Any])
      extends Metadata { override def toString(): String = "init" + super.toString }

  /**
    * @see [[queens.dimensions.fifth.QueensUseSolution.SelfCounting]]
    * @see [[queens.dimensions.fifth.fourth.web.simple.QueensWebSimpleProgram]]
    */
  case class Wrap(override val context: Context, override val data: Option[Any])
      extends Metadata { override def toString(): String = "wrap" + super.toString }

  /**
    * @see [[queens.dimensions.fifth.output.console.QueensConsoleOutput]]
    */
  case class Text(override val context: Context, override val data: Option[Any])
      extends Metadata { override def toString(): String = "text" + super.toString }

  /**
    * @see [[queens.axis.fifth.output.buffered.QueensBufferedOutput]]
    */
  case class Copy(override val context: Context, override val data: Option[Any])
      extends Metadata { override def toString(): String = "copy" + super.toString }

  sealed abstract trait Database extends Metadata

  object Database:
    case class Store(override val context: Context, override val data: Option[Any])
        extends Database { override def toString(): String = "store" + super.toString }
    case class Delete(override val context: Context, override val data: Option[Any])
        extends Database { override def toString(): String = "delete" + super.toString }
    case class Query(override val context: Context, override val data: Option[Any])
        extends Database { override def toString(): String = "query" + super.toString }

  /**
    * @see [[queens.dimensions.fourth.webapp.QueensWebAppInterface]]
    * @see [[queens.axis.fourth.webapp.spring.`Spring Web Client`]]
    * @see [[queens.axis.fourth.webapp.retrofit.`Retrofit Web Client`]]
    */
  case class Media(override val context: Context, override val data: Option[Any])
     extends Metadata { override def toString(): String = "media" + super.toString }
