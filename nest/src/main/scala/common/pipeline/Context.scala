package queens
package common
package pipeline

import java.util.UUID

import base.Board

import dimensions.third.Queens

import dimensions.fourth.app.AppData
import dimensions.fourth.app.Parameter


sealed abstract trait Context


object Context:

  case object Empty
      extends Context

  case class Round(board: Board, tag: Any, queens: Queens[?], number: Long)
      extends Context

  /**
    * @see [[queens.dimensions.fourth.webapp.QueensWebAppInterface.Web]]
    */
  abstract trait App[
    O, AD <: AppData[O, AD],
    CTX <: App[O, AD, CTX]
  ](val params: Parameter[?]*) extends Context
