package queens
package common

import java.io.{ ObjectInputStream, ObjectOutputStream }

import scala.collection.mutable.Stack


object StackVirtualMachine:

  sealed abstract class ProgramThrowable[L] extends Throwable { val IP: L }

  final case class ProgramSuspendedWith[L](t: Throwable, override val IP: L) extends ProgramThrowable[L]
  final case class ProgramStoppedWith[L](t: Throwable, override val IP: L) extends ProgramThrowable[L]

  private final case class ProgramNeverReached[L](override val IP: L) extends ProgramThrowable[L]
  final case class ProgramAlreadyStarted[L](override val IP: L) extends ProgramThrowable[L]
  final case class ProgramNotStarted[L](override val IP: L) extends ProgramThrowable[L]
  final case class ProgramNotSuspended[L](override val IP: L) extends ProgramThrowable[L]
  final case class ProgramNotResumed[L](override val IP: L) extends ProgramThrowable[L]
  case object ProgramEnded extends ProgramThrowable[Int] { override val IP = Int.MinValue }
  final case class ProgramNoSuchInstruction[L](override val IP: L) extends ProgramThrowable[L]
  final case class ProgramIllegalInstruction[L](override val IP: L) extends ProgramThrowable[L]
  final case class ProgramInfiniteLoop[L](override val IP: L) extends ProgramThrowable[L]
  final case class ProgramEmptyStack[L](override val IP: L) extends ProgramThrowable[L]
  final case class ProgramTypeError[L](override val IP: L) extends ProgramThrowable[L]
  final case class ProgramStackOverflow[L](override val IP: L) extends ProgramThrowable[L]


abstract class StackVirtualMachine[L, S <: AnyRef]:

  final protected type Action[T, R] = (L, Instruction[?, ?], T) => R

  sealed abstract class Instruction[T, R] { val action: Action[T, R] }

  abstract class noopInstruction extends Instruction[Unit, Unit] { override val action = noop }

  final case class Start(override val action: Action[Unit, Unit] = noop) extends Instruction[Unit, Unit]
  final case class Suspend(override val action: Action[Unit, (ObjectOutputStream, ObjectOutputStream => Unit, Throwable)]) extends Instruction[Unit, (ObjectOutputStream, ObjectOutputStream => Unit, Throwable)]
  final case class Resume(override val action: Action[ObjectInputStream, Unit]) extends Instruction[ObjectInputStream, Unit]
  final case class End(override val action: Action[Unit, Unit] = noop) extends Instruction[Unit, Unit]
  final case class Noop(override val action: Action[Unit, Unit] = noop) extends Instruction[Unit, Unit]
  final case class Poke[P <: AnyRef](override val action: Action[P, AnyRef]) extends Instruction[P, AnyRef]
  final case class Peek[P <: AnyRef](override val action: Action[P, Unit]) extends Instruction[P, Unit]
  final case class Tst[P <: AnyRef](override val action: Action[P, Boolean]) extends Instruction[P, Boolean]
  final case class CJmp(label: L) extends noopInstruction
  final case class Jmp(label: L) extends noopInstruction
  final case class Call(override val action: Action[S, (L, S)]) extends Instruction[S, (L, S)]
  case object Retn extends noopInstruction
  case object Mov extends noopInstruction

  val noop: Action[Unit, Unit] = { (_, _, _) => }

  final protected def run(program: Map[L, Instruction[?, ?]],
                          thaw: Either[Load, L])
                   (using next: L => L): Nothing =
    import StackVirtualMachine._

    var flag = false
    var store: AnyRef = null
    var stack = Stack[(L, S)]()

    try
      var (start, resume) = thaw match
        case Left((stream, _)) => stream.readObject.asInstanceOf[L] -> true
        case Right(ip) => ip -> false

      if !program.contains(start) then
        throw ProgramNoSuchInstruction(start)

      var IP = start

      try

        program(IP) match
          case i @ Start(a) if !resume =>
            a(IP, i, ())
            stack.push((IP, null.asInstanceOf[S]))

          case i @ Resume(a) if resume =>
            val (stream, _) = thaw.left.get
            flag = stream.readBoolean()
            store = stream.readObject()
            stack = stream.readObject().asInstanceOf[Stack[(L, S)]]
            a(IP, i, stream)

          case _ if resume =>
            throw ProgramNotResumed(IP)

          case _ =>
            throw ProgramNotStarted(IP)

        var ip = next(IP)

        while true do
          if IP == ip then
            throw ProgramInfiniteLoop(IP)

          IP = ip

          if !program.contains(IP) then
            throw ProgramNoSuchInstruction(IP)

          var step = true

          program(IP) match
            case i @ Noop(a) =>
              a(IP, i, ())

            case i @ End(a) =>
              a(IP, i, ())
              throw ProgramEnded

            case i @ Suspend(a) =>
              val (stream, f, t) = a(IP, i, ())
              if stream ne null then
                stream.writeObject(next(IP))
                stream.writeBoolean(flag)
                stream.writeObject(store)
                stream.writeObject(stack)
                f(stream)
                throw ProgramSuspendedWith(t, IP)

            case i @ Poke(a: Action[AnyRef, AnyRef]) =>
              store = a(IP, i, store)

            case i @ Peek(a: Action[AnyRef, AnyRef]) =>
              a(IP, i, store)

            case i @ Tst(a: Action[AnyRef, AnyRef]) =>
              flag = a(IP, i, store)

            case i @ CJmp(l) =>
              if flag then
                ip = l
                step = false

            case i @ Jmp(l) =>
              ip = l
              step = false

            case i @ Call(a) =>
              val top: S = if stack.isEmpty then null.asInstanceOf[S] else stack.top._2
              a(IP, i, top) match
                case (ip_, it) =>
                  try
                    stack.push((IP, it))
                  catch
                    case _: OutOfMemoryError => throw ProgramStackOverflow(IP)
                  ip = ip_
              step = false

            case Retn =>
              try
                stack.pop() match
                  case (ip, _) => IP = ip

                store = stack.top._2
              catch
                case _: NoSuchElementException => throw ProgramEmptyStack(IP)

            case Mov =>
              try
                store = stack.top._2
              catch
                case _: NoSuchElementException => throw ProgramEmptyStack(IP)

            case Resume(_) =>
              throw ProgramNotSuspended(IP)

            case Start(_) =>
              throw ProgramAlreadyStarted(IP)

            case _ =>
              throw ProgramIllegalInstruction(IP)

          if step then
            ip = next(IP)

        throw ProgramNeverReached(IP)
      catch
        case t: ProgramThrowable[L] => throw t
        case t: Throwable => throw ProgramStoppedWith(t, IP)

    finally
      thaw match
        case Left((_, end)) => end()
        case _ =>

