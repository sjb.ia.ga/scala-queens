package queens
package common
package monad

import java.util.UUID
import java.util.Date

import base.Board

import common.monad.Grow


object tag:

  object no:

    given (Any => NoTag.type) = _.asInstanceOf[NoTag.type]

  object integer:

    given (Any =>Int) = _.asInstanceOf[Int]

  object string:

    given (Any => String) = _.asInstanceOf[String]

  object uuid:

    given (Any => UUID) = _.asInstanceOf[UUID]


package actors:

  import akka.actor.typed.ActorRef

  final case class Just[T](override val board: Board,
                           override val tag: T,
                           override val uuid: UUID,
                           override val number: Long,
                           solver: ActorRef[?],
                           looper: ActorRef[?])
      extends Grow.Just[T]

  implicit class ToJust[T](using tag: Any => T)
      extends Grow.ToJust[T, ActorRef[?]]:
    override type J = Just[T]
    inline def apply(r: (Board, T, UUID, Long, ActorRef[?], ActorRef[?])): J =
      Just(r._1, r._2, r._3, r._4, r._5, r._6)


package futures:

  import scala.concurrent.Future

  final case class Just[T](override val board: Board,
                           override val tag: T,
                           override val uuid: UUID,
                           override val number: Long,
                           solver: Future[?],
                           looper: Future[?])
      extends Grow.Just[T]

  implicit class ToJust[T](using tag: Any => T)
      extends Grow.ToJust[T, Future[?]]:
    override type J = Just[T]
    inline def apply(r: (Board, T, UUID, Long, Future[?], Future[?])): J =
      Just(r._1, r._2, r._3, r._4, r._5, r._6)


package flow:

  import version.less.nest.Nest

  final case class Just[T](override val board: Board,
                           override val tag: T,
                           override val uuid: UUID,
                           override val number: Long,
                           started: Date,
                           ended: Date)
      extends Grow.Just[T]

  implicit class ToJust[T](using tag: Any => T)
      extends Grow.ToJust[T, Date]:
    override type J = Just[T]
    inline def apply(r: (Board, T, UUID, Long, Date, Date)): J =
      Just(r._1, r._2, r._3, r._4, r._5, r._6)


package kafka:

  import version.less.nest.Nest

  final case class Just[T](override val board: Board,
                           override val tag: T,
                           override val uuid: UUID,
                           override val number: Long,
                           schema: String,
                           topic: String)
      extends Grow.Just[T]

  implicit class ToJust[T](using tag: Any => T)
      extends Grow.ToJust[T, String]:
    override type J = Just[T]
    inline def apply(r: (Board, T, UUID, Long, String, String)): J =
      Just(r._1, r._2, r._3, r._4, r._5, r._6)


package rabbitMQ:

  import version.less.nest.Nest

  final case class Just[T](override val board: Board,
                           override val tag: T,
                           override val uuid: UUID,
                           override val number: Long,
                           major: String,
                           count: String)
      extends Grow.Just[T]

  implicit class ToJust[T](using tag: Any => T)
      extends Grow.ToJust[T, String]:
    override type J = Just[T]
    inline def apply(r: (Board, T, UUID, Long, String, String)): J =
      Just(r._1, r._2, r._3, r._4, r._5, r._6)


package awsSQS:

  import version.less.nest.Nest

  final case class Just[T](override val board: Board,
                           override val tag: T,
                           override val uuid: UUID,
                           override val number: Long,
                           major: String,
                           count: String)
      extends Grow.Just[T]

  implicit class ToJust[T](using tag: Any => T)
      extends Grow.ToJust[T, String]:
    override type J = Just[T]
    inline def apply(r: (Board, T, UUID, Long, String, String)): J =
      Just(r._1, r._2, r._3, r._4, r._5, r._6)
