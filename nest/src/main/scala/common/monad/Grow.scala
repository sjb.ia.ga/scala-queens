package queens
package common
package monad

import java.util.UUID

import java.util.concurrent.{ ConcurrentHashMap => Map }

import scala.Function.const

import scala.collection.JavaConverters.mapAsScalaConcurrentMapConverter

import base.Board

import common.Flag


abstract trait Grow
    extends common.Tagged:

  protected val iterations: Seq[UUID]

  protected val `iterations*`: Map[UUID, (Boolean, Flag)]

  protected def `flag*`(id: UUID): Flag = Flag((iterations :+ id).toString)

  import Grow._

  protected type T56

  protected def `just get*`[R](it: Item[?])
                              (fun: Boolean => ((String, (Board, Any, UUID, Long, T56, T56))) => R): R =
    `iterations*`.asScala.find { case (_, (_, Flag(slot))) => it(slot).nonEmpty } match
      case Some((_, (_, Flag(slot)))) =>
        it.apply[(Board, Any, UUID, Long, T56, T56)](slot) match
          case Some(Some(r)) =>
            return fun(true)(slot -> r)
          case Some(_) =>
            return fun(false)(null)
          case _ =>
      case _ =>

    throw ItemException(it)

  @inline infix def `just get`[S, S56](it: Item[?])(using toJ: ToJust[S, S56]): toJ.J =
    `just get*`(it)
               (const { case (_, r: (Board, S, UUID, Long, S56, S56)) =>
                          toJ(r).asInstanceOf[toJ.J]
                      }
               )

  infix def stop(it: Item[?]): Boolean = synchronized {

    `iterations*`.asScala.find { case (_, (_, Flag(slot))) => it(slot).nonEmpty } match
      case Some((id, (true, flg))) =>
        !flg
        `iterations*`.put(id, (false, flg))
        true
      case Some(_) =>
        true
      case _ =>
        false
  }


object Grow:

  final case class ItemException[T](it: T)
      extends base.QueensException(s"Item $it")


  abstract trait Just[T]:
    val board: Board
    val tag: T
    val uuid: UUID
    val number: Long


  class ItemValue[T <: AnyRef](var it: T = null)

  extension[T <: AnyRef](it: T)
    inline def `it's`[S <: ItemValue[T]](self: S) = self.it = it


  abstract trait ToJust[T, T56](using tag: Any => T)
      extends Function1[(Board, T, UUID, Long, T56, T56), ToJust[T, T56]#J]:
    type J <: Just[T]
