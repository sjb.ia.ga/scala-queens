package queens
package common
package monad

import conf.monad.Conf

import dimensions.fourth.fifth.breed.`Breed *'°`


abstract trait `WithFilter'°`[
  P <: Conf
](using
  protected val * : P
) { this: `Breed *'°`[P] =>

  protected val fs: Seq[*.Item => Boolean]

  import Item.given

  final protected def `apply°`[R](block: *.Item => R): *.Item => Option[R] = { it =>
    if fs.foldLeft(true) { (r, f) => r && f(it) }
    then
      Some(block(it))
    else
      None
  }

  protected type * <: `WithFilter'°`[P]

  final def withFilter(f: *.Item => Boolean): * =
    this.`apply°`(fs :+ f*)

  protected def `apply°`(s: *.Item => Boolean*): *

}
