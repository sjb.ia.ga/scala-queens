package queens
package common
package monad

import conf.monad.Conf

import dimensions.fourth.fifth.breed.`Breed *'°`


abstract trait `MonadWithFilter'°`[
  P <: Conf,
  P1 <: AnyVal
](using
  override protected val * : P
) extends `0`.`MonadWithFilter'°`[Identity_, Iterable_, P, P1] { this: `Breed *'°`[P] =>

  override protected type * <: `MonadWithFilter'°`[P, P1]

}

package `0`:

  abstract trait `MonadWithFilter'°`[E[_], F[X[_], _],
    P <: Conf,
    P1 <: AnyVal
  ](using
    override protected val * : P
  ) extends `WithFilter'°`[P] { this: `Breed *'°`[P] =>

    override protected type * <: `MonadWithFilter'°`[E, F, P, P1]

    protected def `for*`[R](block: *.Item => R)(using P1): F[E, R] = ???

  }
