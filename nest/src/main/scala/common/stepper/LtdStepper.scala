package queens
package common
package stepper


abstract trait LtdStepper[T]
    extends Stepper[T]:

  protected val MIN: Int

  protected val MAX: Int

  require(MIN <= MAX)

  override def forward: S =
    if pos.get == MAX then
      throw LtdStepper.ForwardLimitReached
    super.forward

  override def backward: S =
    if pos.get == MIN then
      throw LtdStepper.BackwardLimitReached
    super.backward

  override def hashCode(): Int = (super.hashCode, MIN, MAX).##

  override def equals(any: Any): Boolean = any match
    case that: LtdStepper[T] =>
      this.MIN == that.MIN &&
      this.MAX == that.MAX &&
      super.equals(any)
    case _ => false

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[LtdStepper[T]]


object LtdStepper:

  object BackwardLimitReached extends Throwable

  object ForwardLimitReached extends Throwable
