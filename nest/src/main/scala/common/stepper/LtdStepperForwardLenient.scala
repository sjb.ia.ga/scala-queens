package queens
package common
package stepper


abstract trait LtdStepperForwardLenient[T]
    extends LtdStepper[T]:

  protected val lenient: Boolean2

  override def forward: S =
    if pos.get == MAX
    then
      if !lenient.get then
        throw LtdStepper.ForwardLimitReached
      lenient.set(false)
      self
    else
      super.forward

  override def backward: S =
    if !lenient.get
    then
      lenient.set(true)
      self
    else
      super.backward

  def isLenient: Boolean = lenient.get

  override def hashCode(): Int = (super.hashCode, lenient).##

  override def equals(any: Any): Boolean = any match
    case that: LtdStepperForwardLenient[T] =>
      this.lenient == that.lenient &&
      super.equals(any)
    case _ => false

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[LtdStepperForwardLenient[T]]
