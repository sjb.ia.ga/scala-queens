package queens
package common
package stepper


abstract trait BoardStepper
    extends LtdStepperForwardLenient[Int]:

  protected val N: Int

  require(MIN <= pos.get)
  require(pos.get <= MAX)
