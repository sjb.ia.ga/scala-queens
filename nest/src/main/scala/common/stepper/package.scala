package queens
package common


package object stepper {
  import common.geom.Coord

  extension [S <: Stepper[?], R <: S](row: S)
    inline def x(col: S): Coord[R] = (row, col).asInstanceOf[Coord[R]]

  inline given [S <: Stepper[?]]: Conversion[Coord[S], Point] = { it =>
    (it._1.current -> it._2.current).asInstanceOf[Point]
  }

}

package stepper {

  package object factory {

    object Implicits {

      implicit object Stepper1Factory
          extends BoardStepperFactory[Stepper1]:
        override def apply(N: Int)(using Option[Long]): Stepper1 = Stepper1(N)

      implicit object StepperPFactory
          extends BoardStepperFactory[StepperP]:
        override def apply(N: Int)(using Option[Long]): StepperP = StepperP(N)

    }

    given BoardStepperFactory[Stepper1] = Implicits.Stepper1Factory
    given BoardStepperFactory[StepperP] = Implicits.StepperPFactory

  }

  package dup {

    package object factory {

      object Implicits {

        implicit object Stepper1DupFactory
            extends BoardStepperDupFactory[Stepper1Dup]:
          override def apply(N: Int)(using Option[Long]): Stepper1Dup = Stepper1Dup(N)

        implicit object StepperPDupFactory
            extends BoardStepperDupFactory[StepperPDup]:
          override def apply(N: Int)(using Option[Long]): StepperPDup = StepperPDup(N)

      }

      given BoardStepperDupFactory[Stepper1Dup] = Implicits.Stepper1DupFactory
      given BoardStepperDupFactory[StepperPDup] = Implicits.StepperPDupFactory

    }

  }

}
