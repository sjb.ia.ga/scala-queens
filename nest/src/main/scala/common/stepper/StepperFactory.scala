package queens
package common
package stepper


abstract trait StepperFactory[T, S <: Stepper[T]] {
  def apply(N: Int)(using seed: Option[Long]): S
}
