package queens
package common
package stepper

import java.util.concurrent.{ ConcurrentHashMap => Map }

import scala.util.Random


object StepperP:

  private val permutations = Map[Int, List[IndexedSeq[Int]]]()

  private[stepper] def next(N: Int)(using seed: Option[Long]): IndexedSeq[Int] =
    if !permutations.containsKey(N) then
      permutations.put(N, (0 until N).permutations.toList)
    val p = permutations.get(N)
    var s: Long = 1
    var m: Long = N
    while m > 0 do
      s *= m
      m -= 1

    m = (seed.map(Random(_)).getOrElse(Random())).nextLong.abs % (s min Int.MaxValue)

    p(m.toInt)

  inline private[common] def apply(N: Int)(using Option[Long]): StepperP =
    new StepperP(N):
      override type S = StepperP


abstract case class StepperP protected (
  protected val per: IndexedSeq[Int],
  override protected val N: Int
)(
  override protected val MIN: Int,
  override protected val MAX: Int,
  override protected val pos: Integer2,
  override protected val lenient: Boolean2
) extends BoardStepper:

  protected def this(p: IndexedSeq[Int], n: Int) = this(p, n)(0, n-1, Integer2(0), Boolean2(true))

  def this(n: Int)(using Option[Long]) = this(StepperP.next(n), n)

  inline override def current = if isLenient then per(pos.get) else N

  override def hashCode(): Int = (super.hashCode, N, per).##

  override def equals(any: Any): Boolean = any match
    case that: StepperP =>
      this.N == that.N &&
      this.per == that.per &&
      super.equals(any)
    case _ => false

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[StepperP]
