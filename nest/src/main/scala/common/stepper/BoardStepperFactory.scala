package queens
package common
package stepper


abstract trait BoardStepperFactory[S <: BoardStepper]
    extends StepperFactory[Int, S]
