package queens
package common
package stepper


abstract trait Stepper[T]
    extends Equals:

  type S <: Stepper[T]

  final protected val self: S = this.asInstanceOf[S]

  protected val pos: Integer2

  def current: T

  def forward: S =
    pos.inc
    self

  def backward: S =
    pos.dec
    self

  override def toString(): String = "stepper current=" + current.toString()

  override def hashCode(): Int = pos.##

  override def equals(any: Any): Boolean = any match
    case that: Stepper[T] =>
      (that canEqual this) &&
      this.pos == that.pos
    case _ => false

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[Stepper[T]]
