package queens
package common
package stepper
package dup


abstract trait LtdStepperDup[T]
    extends LtdStepper[T] {

  protected def dup[R <: S](r: S): R

  def dup[R <: S]: R

}
