package queens
package common
package stepper
package dup


abstract trait BoardStepperDupFactory[S <: BoardStepperDup]
    extends BoardStepperFactory[S]
