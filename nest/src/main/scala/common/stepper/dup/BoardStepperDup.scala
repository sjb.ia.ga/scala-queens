package queens
package common
package stepper
package dup


abstract trait BoardStepperDup
    extends BoardStepper
    with LtdStepperForwardLenientDup[Int]:

  override protected def dup[R <: S](r: S): R =
    val s = r.asInstanceOf[BoardStepperDup]
    s.pos.set(pos.get)
    s.lenient.set(lenient.get)
    s.asInstanceOf[R]
