package queens
package common
package stepper
package dup


abstract class StepperPDup (
  p: IndexedSeq[Int],
  n: Int
) extends StepperP(p, n)
    with BoardStepperDup:

  inline override def dup[R <: S]: R =
    dup[R](StepperPDup(per, N).asInstanceOf[S])


object StepperPDup:

  inline private[common] def apply(N: Int)(using Option[Long]): StepperPDup =
    apply(StepperP.next(N), N)

  inline private[common] def apply(p: IndexedSeq[Int], N: Int): StepperPDup =
    new StepperPDup(p, N):
      override type S = StepperPDup
