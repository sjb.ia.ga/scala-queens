package queens
package common
package stepper
package dup


abstract class Stepper1Dup(n: Int)
    extends Stepper1(n)
    with BoardStepperDup:

  inline override def dup[R <: S]: R =
    dup[R](Stepper1Dup(N).asInstanceOf[S])


object Stepper1Dup:

  inline private[common] def apply(N: Int): Stepper1Dup =
    new Stepper1Dup(N):
      override type S = Stepper1Dup
