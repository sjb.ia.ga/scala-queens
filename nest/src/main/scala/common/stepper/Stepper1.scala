package queens
package common
package stepper


abstract case class Stepper1 protected (
  override protected val N: Int
)(
  override protected val MIN: Int,
  override protected val MAX: Int,
  override protected val pos: Integer2,
  override protected val lenient: Boolean2
) extends BoardStepper:

  def this(n: Int) = this(n)(0, n-1, Integer2(0), Boolean2(true))

  inline override def current = if isLenient then pos.get else N

  override def hashCode(): Int = (super.hashCode, N).##

  override def equals(any: Any): Boolean = any match
    case that: Stepper1 =>
      this.N == that.N &&
      super.equals(any)
    case _ => false

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[Stepper1]


object Stepper1:

  inline private[common] def apply(N: Int): Stepper1 =
    new Stepper1(N):
      override type S = Stepper1
