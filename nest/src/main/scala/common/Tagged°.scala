package queens
package common

import conf.monad.Conf

import dimensions.fourth.fifth.breed.`Breed *'°`


abstract trait `Tagged°`[
  P <: Conf
](using
  override protected val * : P
) extends Tagged { this: `Breed *'°`[P] & monad.`WithFilter'°`[P] =>

  def `#`(t: Any): *

}
