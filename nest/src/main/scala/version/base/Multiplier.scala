package queens
package version
package base


case class Multiplier[P <: Plane](factor: P => Int):

  def apply(it: P) = factor(it)

  def apply[Q <: Plane](map: Q => P): Multiplier[Q] =
    Multiplier[Q] { it => factor(map(it)) }


object Multiplier:

  given [P <: Plane]: Conversion[Function[P, Int], Multiplier[P]] = Multiplier(_)

  final class Once[P <: Plane] extends Multiplier[P]({ _ => 1 })

  object Once:

    def apply[P <: Plane]() = new Once[P]

