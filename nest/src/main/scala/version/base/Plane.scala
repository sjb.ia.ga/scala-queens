package queens
package version
package base

import dimensions.Dimension
import dimensions.Dimension.Axes


abstract class Plane
    extends Comparable[Plane]:

  def axes: Axes

  def toVector: Vector

  protected val str: List[String]

  override def toString(): String =
    s"${getClass.getSimpleName}[${str mkString ","}]"

  override def hashCode(): Int = toVector.##

  override def equals(any: Any) = any match
    case that: Plane =>
      (that canEqual this) &&
      this.toVector == that.toVector
    case _ => false

  def canEqual(any: Any): Boolean

  override def compareTo(that: Plane): Int =
    require(this.axes == that.axes)

    val u = this.toVector
    val v = that.toVector
    var r = 0

    if u.aspect ne null then r = u.aspect.toString.compareTo(v.aspect.toString)
    if r == 0
    then
      if u.algorithm ne null then r = u.algorithm.toString.compareTo(v.algorithm.toString)
      if r == 0
      then
        if u.model ne null then r = u.model.toString.compareTo(v.model.toString)
        if r == 0
        then
          if u.interface ne null then r = u.interface.toString.compareTo(v.interface.toString)
          if r == 0
          then
            if u.use ne null then r = u.use.toString.compareTo(v.use.toString)
            r
          else
            r
        else
          r
      else
        r
    else
      r
