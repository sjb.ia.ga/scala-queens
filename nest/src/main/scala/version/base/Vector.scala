package queens
package version
package base

import dimensions.Dimension
import dimensions.Dimension.Axes
import dimensions.Dimension.{ Aspect, Algorithm, Model, Interface, Use }


class Vector(
  val aspect: Aspect,
  val algorithm: Algorithm,
  val model: Model,
  val interface: Interface,
  val use: Use
) extends Plane:

  override def axes: Axes = Vector.axes

  override def toVector: Vector = this

  override protected val str = List(
    s"aspect=$aspect",
    s"algorithm=$algorithm",
    s"model=$model",
    s"interface=$interface",
    s"use=$use"
  )

  override def hashCode(): Int = (aspect, algorithm, model, interface, use).##

  override def equals(any: Any) = any match
    case that: Vector =>
      (that canEqual this) &&
      this.aspect == that.aspect &&
      this.algorithm == that.algorithm &&
      this.model == that.model &&
      this.interface == that.interface &&
      this.use == that.use
    case _ => false

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[Vector]


object Vector:

  val axes: Axes = Dimension(Aspect, Algorithm, Model, Interface, Use)
