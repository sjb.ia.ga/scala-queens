package queens
package version
package base

import scala.collection.Map
import scala.collection.mutable.{ LinkedHashMap => MutableMap }
import scala.collection.mutable.{ ListBuffer => MutableList }

import dimensions.Dimension
import dimensions.Dimension.{ Axes, Projection, Scalar }


object Wildcard:

  type Filter = (Dimension, Scalar) => Boolean

  protected def apply[P <: Plane](wildcard: Wildcard[P]): Map[Dimension, Seq[Scalar]] =
    MutableMap[Dimension, Seq[Scalar]]()
      .addAll {
        for {
          di <- wildcard.axes
          en = di.asInstanceOf[Enumeration]
        } yield di -> Seq.from {
          for {
            it <- en.values
            if wildcard.filter(di, it)
          } yield it
        }
      }
      .filterInPlace((_, it) => it.nonEmpty)


abstract trait Wildcard[P <: Plane]
    extends Equals:

  protected[version] val filter: Wildcard.Filter

  val axes: Axes

  def apply(multiplier: Multiplier[P] = Multiplier.Once()): List[P] =
    apply(multiplier.factor)

  protected def apply(factor: P => Int): List[P]

  private val cache: MutableMap[(Projection => P, P => Int), List[P]] = new MutableMap()

  final protected def apply(mkFactor: P => Int, mkPlane: Projection => P): List[P] =
    synchronized {
      cache.find { case ((_1, _2), _) => (_1 eq mkPlane) && (_2 eq mkFactor) } match
        case Some((_, ls)) => return ls
        case _ =>
    }

    val vs = Wildcard(this)

    if axes != vs.keySet
    then
      Nil
    else
      val ps = MutableList[P]()

      val dimensions = axes.toSeq

      def addProjections(n: Int = dimensions.size, r: Projection = Nil): Unit =
        if n == 0
        then
          ps += mkPlane(r)
        else
          vs(dimensions(n - 1))
            .map(_ +: r)
            .foreach(addProjections(n - 1, _))

      addProjections()

      synchronized {
        val ls = ps
          .flatMap(it => List.fill(mkFactor(it))(it))
          .toList
        cache += (mkPlane, mkFactor) -> ls
        ls
      }

  override def equals(any: Any): Boolean = any match
    case that: Wildcard[P] =>
      (that canEqual this) &&
      this().toSet == that().toSet
    case _ => false
