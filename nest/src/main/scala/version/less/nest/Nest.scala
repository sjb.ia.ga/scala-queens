package queens
package version.less
package nest

import dimensions.Dimension
import dimensions.Dimension.{ Axes, Projection, Scalar }
import dimensions.Dimension.{ Aspect, Algorithm, Model }

import version.base.{ Plane, Vector }


case class Nest(aspect: Aspect, algorithm: Algorithm, model: Model)
    extends Plane:

  override def axes: Axes = Nest.axes

  override def toVector: Vector =
    new Vector(aspect, algorithm, model, null, null)

  override protected val str = List(
    s"aspect=$aspect",
    s"algorithm=$algorithm",
    s"model=$model"
  )

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[Nest]


object Nest:

  val axes: Axes = Dimension(Aspect, Algorithm, Model)

  def apply(it: Projection): Nest = {
    assert(it.size == 3)
    Nest(it(0), it(1), it(2))
  }

  import Dimension.Aspect.valueToAspect
  import Dimension.Algorithm.valueToAlgorithm
  import Dimension.Model.valueToModel

  implicit def valuesToNest(aspect: Scalar, algorithm: Scalar, model: Scalar): Nest =
    Nest(aspect, algorithm, model)
