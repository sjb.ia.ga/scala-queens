package queens
package version.less
package nest

import dimensions.Dimension
import dimensions.Dimension.{ Aspect, Algorithm, Model }

import version.base.Plane


class Wildcard protected (
  override protected[version] val filter: Wildcard.Filter
) extends version.base.Wildcard[Nest]:

  override val axes = Nest.axes

  def this() = this(Wildcard.star)

  override protected def apply(factor: Nest => Int): List[Nest] =
    super.apply(factor, Nest.apply)

  override def equals(any: Any): Boolean = any match
    case that: Wildcard => super.equals(any)
    case _ => false

  override def canEqual(any: Any): Boolean =
    any.isInstanceOf[Wildcard]


object Wildcard:

  type Filter = version.base.Wildcard.Filter

  private val star: Filter = (_, _) match
    case (Aspect, _) | (Algorithm, _) | (Model, _) => true
    case (_, _) => false

  def * = new Wildcard()

  def apply(filter: Filter, wildcard: Wildcard = *) =
    new Wildcard({ (di, it) =>
      filter(di, it) && wildcard.filter(di, it)
    })

  def apply(nest: Nest*): Filter = (_, _) match
    case (Aspect, it: Aspect) => nest.map(_.aspect).exists(it.==)
    case (Algorithm, it: Algorithm) => nest.map(_.algorithm).exists(it.==)
    case (Model, it: Model) => nest.map(_.model).exists(it.==)
    case (_, _) => false

  def apply(aspect: Aspect, algorithm: Algorithm, model: Model): Filter = (_, _) match
    case (Aspect, `aspect`) | (Algorithm, `algorithm`) | (Model, `model`) => true
    case (_, _) => false
