package queens
package conf.actors

import scala.concurrent.duration.{ Duration, FiniteDuration }

import akka.actor.typed.scaladsl.ActorContext


sealed trait It
    extends Any:

  def it: (ActorContext[?], Callbacks)


abstract trait Conf
    extends conf.Conf
    with It:

  // not used
  var pollInterval: FiniteDuration = Duration.fromNanos(10000)


abstract trait AnyConf
    extends Any
    with conf.AnyConf
    with It
