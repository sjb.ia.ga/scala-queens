package queens
package conf.actors

import java.util.UUID

import java.util.concurrent.atomic.AtomicLong

import akka.actor.typed.ActorRef
import akka.actor.typed.scaladsl.ActorContext

import dimensions.third.first.Macros.Event

import version.less.nest.Nest


trait Callbacks:

  def iterationCallback(tag: Any,
                        nest: Nest,
                        uuid: UUID,
                        ctx: ActorContext[?],
                        s: ActorRef[?]): Unit = ()

  def solutionCallback(tag: Any,
                       nest: Nest,
                       uuid: UUID,
                       number: Option[Long],
                       r: ActorRef[?],
                       `r*`: Option[(ActorContext[?], ActorRef[?])]): Unit = ()


object Callbacks:

  def apply[T](iterationCb: (T, Nest, UUID, ActorContext[?], ActorRef[?]) => Unit
                          = { (_: T, _: Nest, _: UUID, _: ActorContext[?], _: ActorRef[?]) => },
               solutionCb: (T, Nest, UUID, Option[Long], ActorRef[?], Option[(ActorContext[?], ActorRef[?])]) => Unit
                         = { (_: T, _: Nest, _: UUID, _: Option[Long], _: ActorRef[?], _: Option[(ActorContext[?], ActorRef[?])]) => }) =

    new Callbacks:
      override def iterationCallback(tag: Any,
                                     nest: Nest,
                                     uuid: UUID,
                                     ctx: ActorContext[?],
                                     s: ActorRef[?]): Unit =
        iterationCb(tag.asInstanceOf[T], nest, uuid, ctx, s)

      override def solutionCallback(tag: Any,
                                    nest: Nest,
                                    uuid: UUID,
                                    number: Option[Long],
                                    r: ActorRef[?],
                                    `r*`: Option[(ActorContext[?], ActorRef[?])]): Unit =
        solutionCb(tag.asInstanceOf[T], nest, uuid, number, r, `r*`)


package bis:

  import common.Macros.busywaitUntil

  import common.Wrapper

  trait Callbacks
      extends conf.actors.Callbacks:

    object ctx extends Wrapper[ActorContext[?]]

    val spindle = AtomicLong(1)

    def apply(): Unit =
      spindle.busywaitUntil(-1)(1L) // solver
      spindle.set(1)


  object Callbacks:

    def apply() =

      new Callbacks:

        override def iterationCallback(tag: Any,
                                       nest: Nest,
                                       uuid: UUID,
                                       ctx: ActorContext[?],
                                       ref: ActorRef[?]): Unit = {}

        override def solutionCallback(tag: Any,
                                      nest: Nest,
                                      uuid: UUID,
                                      number: Option[Long],
                                      r: ActorRef[?],
                                      `r*`: Option[(ActorContext[?], ActorRef[?])]): Unit = {}
