package queens
package conf.kafka

import org.apache.avro.generic.GenericRecord
import org.apache.kafka.clients.producer.KafkaProducer


sealed trait It
    extends Any:

  def it: KafkaProducer[String, GenericRecord]


abstract trait Conf
    extends conf.Conf
    with It


abstract trait AnyConf
    extends Any
    with conf.AnyConf
    with It
