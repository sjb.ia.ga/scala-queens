package queens
package conf


abstract trait AnyConf
    extends Any:

  /**
    * @see [[queens.conf.flow.AnyConf]]
    * @see [[queens.dimensions.third.first.flow.Iteration]]
    */
  val params: Seq[?]
