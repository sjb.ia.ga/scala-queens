package queens
package conf.futures

import scala.concurrent.{ Await, ExecutionContext, Future }
import scala.concurrent.duration.{ Duration, FiniteDuration }


sealed trait It
    extends Any:

  def it: (ExecutionContext, Callbacks)


abstract trait Conf
    extends conf.Conf
    with It:

  // not used
  var pollInterval: FiniteDuration = Duration.fromNanos(10000)


abstract trait AnyConf
    extends Any
    with conf.AnyConf
    with It
