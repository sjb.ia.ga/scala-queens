package queens
package conf.futures

import java.util.UUID

import java.util.concurrent.atomic.AtomicLong

import java.util.concurrent.TimeoutException

import scala.concurrent.{ Await, ExecutionContext, Future }
import scala.concurrent.duration.Duration

import common.Macros.trys

import version.less.nest.Nest


trait Callbacks:

  implicit protected val executionContext: ExecutionContext

  def iterationCallback(tag: Any,
                        nest: Nest,
                        uuid: UUID,
                        s: Future[?]): Unit =
    Callbacks.await(s)

  def solutionCallback(tag: Any,
                       nest: Nest,
                       uuid: UUID,
                       number: Option[Long],
                       f: Future[?],
                       `f*`: Option[Future[?]]): Unit =
    `f*`.foreach(Callbacks.await)


object Callbacks:

  import scala.util.{ Success, Failure }

  def await(f: Future[?])(using ExecutionContext): Unit =
    try
      Await.ready(f, Duration.Inf)
    catch
      case _: InterruptedException | _: TimeoutException =>

  def apply[T](iterationCb: (T, Nest, UUID, Future[?]) => Unit
                          = { (_: T, _: Nest, _: UUID, _: Future[?]) => },
               solutionCb: (T, Nest, UUID, Option[Long], Future[?], Option[Future[?]]) => Unit
                         = { (_: T, _: Nest, _: UUID, _: Option[Long], _: Future[?], _: Option[Future[?]]) => }
       )(using ec: ExecutionContext): Callbacks =

    new Callbacks:

      override protected val executionContext: ExecutionContext = ec

      override def iterationCallback(tag: Any,
                                     nest: Nest,
                                     uuid: UUID,
                                     s: Future[?]): Unit =
        iterationCb(tag.asInstanceOf[T], nest, uuid, s)

      override def solutionCallback(tag: Any,
                                    nest: Nest,
                                    uuid: UUID,
                                    number: Option[Long],
                                    f: Future[?],
                                    `f*`: Option[Future[?]]): Unit =
        solutionCb(tag.asInstanceOf[T], nest, uuid, number, f, `f*`)


package bis:

  import common.Macros.busywaitUntil

  import conf.futures.Callbacks.await

  trait Callbacks(
    using override val executionContext: ExecutionContext
  ) extends conf.futures.Callbacks:

    val spindle = AtomicLong(1)

    def apply(batch: Option[Future[?]]): Unit =
      spindle.decrementAndGet() // solver
      batch.map(await(_))
      spindle.busywaitUntil(-1)(1L)
      spindle.set(1)

  object Callbacks:

    def apply()(using ExecutionContext): Callbacks =

      new Callbacks:

        override def iterationCallback(_tag: Any,
                                       _nest: Nest,
                                       _uuid: UUID,
                                       s: Future[?]): Unit = {}

        override def solutionCallback(_tag: Any,
                                      _nest: Nest,
                                      _uuid: UUID,
                                      _number: Option[Long],
                                      _f: Future[?],
                                      `f*`: Option[Future[?]]): Unit = {}
