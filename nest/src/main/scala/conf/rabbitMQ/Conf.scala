package queens
package conf.rabbitMQ

import com.rabbitmq.client.Channel


sealed trait It
    extends Any:

  def it: Channel


abstract trait Conf
    extends conf.Conf
    with It


abstract trait AnyConf
    extends Any
    with conf.AnyConf
    with It
