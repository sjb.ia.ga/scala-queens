package queens
package conf.awsSQS

import com.amazonaws.services.sqs.AmazonSQS


sealed trait It
    extends Any:

  def it: AmazonSQS


abstract trait Conf
    extends conf.Conf
    with It


abstract trait AnyConf
    extends Any
    with conf.AnyConf
    with It
