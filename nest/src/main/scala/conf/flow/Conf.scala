package queens
package conf.flow


abstract trait Conf
    extends conf.Conf


abstract trait AnyConf
    extends Any
    with conf.AnyConf
