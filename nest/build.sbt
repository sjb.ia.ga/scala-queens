resolvers += "confluent" at "https://packages.confluent.io/maven/"

libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",

  "com.github.blemale" %% "scaffeine" % "5.2.1" % "compile",

  // actors
  "com.typesafe.akka" %% "akka-actor-typed" % "2.8.5",
  "com.typesafe.akka" %% "akka-actor-testkit-typed" % "2.8.5" % Test,

  // cats
  "org.typelevel" %% "cats-core" % "2.10.0",

  // kafka
  "org.apache.kafka" % "kafka-clients" % "7.4.0-ce",
  "org.apache.avro" % "avro" % "1.11.2",
  "io.confluent" % "kafka-avro-serializer" % "7.4.0",

  // rabbitMQ
  "com.rabbitmq" % "amqp-client" % "5.21.0",

  // awsSQS
  "com.amazonaws" % "aws-java-sdk-sqs" % "1.12.744",

  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
