package queens
package pojo

import scala.quoted.{ Expr, Quotes }


object Macros:

  import java.util.Date

  def spanCode(idle: Expr[Long], from: Expr[Long], now: Expr[Long])(using Quotes): Expr[(Long, Long)] =
   '{ ($now, $idle + $now - $from) }

  inline def span(inline idle: Long, from: Date): (Long, Long) =
   ${ spanCode('idle, '{ from.getTime }, '{ System.currentTimeMillis }) }

  inline def span(inline idle: Long, from: Date, to: Date): (Long, Long) =
   ${ spanCode('idle, '{ from.getTime }, '{ to.getTime }) }
