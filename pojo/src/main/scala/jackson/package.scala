package queens
package pojo
package jackson


object FromResponseToQueens
  extends pojo.FromResponseToQueens[Queens]:

  import scala.collection.JavaConverters.iterableAsScalaIterableConverter

  import pojo.jPojo.given

  override def apply(queens: Queens, tag: Any, key: String): Iterable[`Queens3*`] =
    val board = queens.getBoard
    for
      it <- queens.getQueens.asScala
    yield
      Queens3[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2](it, board)(tag, key)


object jPojoJackson:

  import scala.reflect.ClassTag

  import queens.base.Board

  import version.less.nest.Nest

  import pojo.jPojo.given

  given Conversion[Board, Board2] = given_Conversion_Board_B[Board2, Square2](classTagBoard2, classTagSquare2)(_)
  given Conversion[Nest, Nest2] = given_Conversion_Nest_N[Nest2](classTagNest2)(_)
  given Conversion[Solution, Solution2] = given_Conversion_Solution_S[Board2, Square2, Solution2, Point2](classTagSolution2, classTagPoint2)(_)

  val classTagQueens: ClassTag[Queens] = { def it(using ct: ClassTag[Queens]) = ct; it }
  val classTagBoard2: ClassTag[Board2] = { def it(using ct: ClassTag[Board2]) = ct; it }
  val classTagSquare2: ClassTag[Square2] = { def it(using ct: ClassTag[Square2]) = ct; it }
  val classTagSolution2: ClassTag[Solution2] = { def it(using ct: ClassTag[Solution2]) = ct; it }
  val classTagPoint2: ClassTag[Point2] = { def it(using ct: ClassTag[Point2]) = ct; it }
  val classTagQueens2: ClassTag[Queens2] = { def it(using ct: ClassTag[Queens2]) = ct; it }
  val classTagNest2: ClassTag[Nest2] = { def it(using ct: ClassTag[Nest2]) = ct; it }

  extension(self: String)
    @inline def toPojo(spacing: String = " ",
                       separator: String = "\n",
                       delimiters: (String, String, String, String) = (":", "-", "[", "]") // yaml
    ): Option[Queens] =
      import pojo.parser.read.`toPojo*`
      self.`toPojo*`[Queens, Board2, Square2, Solution2, Point2, Queens2, Nest2](spacing, separator, delimiters)
