package queens
package pojo

import java.util.UUID

import scala.collection.Seq
import scala.collection.mutable.{ ListBuffer => MutableList }
import scala.collection.JavaConverters.collectionAsScalaIterableConverter

import queens.base.Board

import common.monad.Item
import common.given

import version.less.nest.Nest


private[pojo] final class Queens3(
  board: Board,
  override protected val tags: List[String]
)(
  tag: Any,
  key: String,
)(
  nest: Nest,
  solutions: Seq[Solution]
)(
  override protected val fs: Item[Solution] => Boolean*
) extends `Queens3*`:

  override def toString(): String = tags.map(_.replace('_', ' ')).mkString(" ")

  override val aspect = nest.aspect
  override val algorithm = nest.algorithm
  override val model = nest.model

  override protected def `for*`[R](block: Item[Solution] => R)(using
                                   M: Long, preemption: Boolean): Iterable[R] =
    val uuid = UUID.randomUUID

    val `block*` = `apply*`(block)

    val r = solutions
      .take(M.toInt)
      .zipWithIndex
      .map { (it, n) =>
        if preemption then
          block(new Item(it, key))
        val vi = (board, tag, uuid, n+1L, (), ())
        `block*`(new Item(it, key, Some(vi)))
      }
    r

  override def foreach(block: Item[Solution] => Unit)
                      (using Long, Boolean): Iterable[Unit] =
    `for*`(block)

  override def map[R](block: Item[Solution] => R)
                     (using Long, Boolean): Iterable[R] =
    `for*`(block)

  override def flatMap[R](block: Item[Solution] => Iterable[R])
                         (using Long, Boolean): Iterable[R] =
    `for*`(block).flatten

  override protected type * = Queens3

  override protected def `apply*`(s: Item[Solution] => Boolean*): * =
    new Queens3(board, tags)(tag, key)(nest, solutions)(s*)


object Queens3:

  import pojo.jPojo.given

  def apply[
    O <: Queens[R, B, P, S, N, Q],
    B <: Board2[R, B],
    R <: Square2,
    S <: Solution2[R, B, P, S],
    P <: Point2,
    Q <: Queens2[R, B, P, S, N, Q],
    N <: Nest2
  ](queens2: Q,
    board: Board
  )(
    tag: Any,
    key: String
  ) =
    new Queens3(board, queens2.getTags.asScala.toList)
               (tag, key)
               (queens2.getNest, queens2.getSolutions)
               ()
