package queens
package pojo
package parser

import scala.collection.mutable.{ HashMap => MutableMap }
import scala.collection.Map

import scala.util.control.NonLocalReturns.{ returning, throwReturn => thr }


package text:

  object jText: // plainjava

    extension(self: Iterable[?])
      def toText(indentation: Int = 2,
                 spacing: String = " ",
                 separator: String = "\n",
                 delimiters: (String, String, String, String) = (":", "-", "[", "]") // yaml
      ): String =
        given Int = 1 max indentation
        val (colon, item, opened, closed) = delimiters
        val spacing1 = if spacing.isEmpty then " " else spacing
        val item1 = if item.isEmpty then "-" else item
        val spacing2 = spacing1.take(item1.length.min(spacing1.length))
        val item2 = item1.take(spacing1.length.min(item1.length))
        given String = spacing2
        given (String, String, String, String) = (colon, item2, opened, closed)
        self.mkText(spacing1.head.toString.repeat(spacing1.length) != spacing1).mkString(separator)


package object text:

  extension(self: Iterable[?])
    private def mkText(fixed: Boolean,
                       indent: String = "")
                      (using
                       indentation: Int,
                       spacing: String,
                       delimiters: (String, String, String, String)
    ): List[String] =
      val (colon, item, opened, closed) = delimiters

      val indent1 =
        if fixed
        then
          val indentation0 = indentation * spacing.length
          val factor = indent.length / indentation0 + (if indent.length % indentation0 == 0 then 1 else 2)
          spacing.repeat(indentation * factor)
        else
          spacing.head.toString.repeat(indent.length) + spacing.repeat(indentation)

      inline def cons(it: Iterable[?], indent: String) = indent :: it.mkText(fixed, indent)

      self match
        case m: Map[?, ?] =>
          m.foldLeft(List[String]()) { case (acc, (key, value)) =>
                                         val indent2 = s"$indent1$key$colon"
                                         acc ++ (
                                           value match
                                             case it: Iterable[?] => cons(it, indent2)
                                             case _ => s"$indent2$spacing$value" :: Nil
                                         )
                                     }
        case _ =>
         self.foldLeft(List[String]()) { (acc, elem) =>
                                         val indent2 = s"$indent1$item$spacing"
                                         acc ++ (
                                           elem match
                                             case it: Iterable[?] =>
                                               val indent3 = s"$indent2$opened"
                                               cons(it, indent3) :+ s"$indent1$spacing$spacing$closed"
                                             case _ => s"$indent2$elem" :: Nil
                                         )
                                       }


  extension(self: String)
    private def align(spacing: String): String =
      val m = self.length
      val n = spacing.length
      var indent1 = ""
      var l = -1
      while l != 0
      do
        l = 0
        while l < n && {
          val spacing1 = spacing.take(l+1)
          val m1 = indent1.length
          val n1 = spacing1.length
          m >= m1+n1 && self.substring(m1, m1+n1) == spacing1
        } do
          l += 1
        if l > 0
        then
          indent1 += spacing.take(l)
      indent1


  extension(self: List[String])
    def fromText(spacing: String,
                 quoting: MutableMap[String, String])
                (using
                 delimiters: (String, String, String)
    ): Option[Map[String, AnyRef]] =
      self.parse(spacing, quoting) match
        case Some((it, Nil, false)) => Some(it)
        case _ => None

  extension(self: List[String])
    private def parse(spacing: String,
                      quoting: MutableMap[String, String],
                      closing: Option[String] = None)
                     (using
                      delimiters: (String, String, String)
    ): Option[(Map[String, AnyRef], List[String], Boolean)] = returning {
      val (item, opened, closed) = delimiters

      inline def error(): Nothing = thr(None)

      var head = self.headOption.getOrElse(error())
      var tail = self

      val m = head.align(spacing).length

      if head.length == m then error()

      var indent1: String = null
      var head1: String = null

      var m1: Int = 0
      val n = spacing.length

      val result = MutableMap[String, AnyRef]()

      var key: String = null

      if head.length > m && head.substring(m).startsWith(item)
      then
        error()

      var opening1: Option[String] = None
      var listing1: Option[String] = None

      def done(closed: Boolean = false): Nothing =
        if !closed then closing.map { _ => error() }
        assert(opening1.isEmpty && listing1.isEmpty)
        if !closed && tail.nonEmpty then tail = head :: tail
        thr(Some((result, tail, closed)))

      def next(): Boolean =
        head = tail.headOption.getOrElse(done())
        tail = tail.tail

        if tail.isEmpty
        then
          false
        else
          head1 = tail.head

          indent1 = head1.align(spacing)
          m1 = indent1.length

          val itemizing = opening1.nonEmpty || listing1.nonEmpty

          if opening1.map(!head1.startsWith(_)).getOrElse(false)
          then
            opening1 = None

          if opening1.nonEmpty || listing1.map(!head1.startsWith(_)).getOrElse(false)
          then
            listing1 = None

          if closing.map(_ + item+closed == head).getOrElse(false)
          then
            assert(opening1.isEmpty && listing1.isEmpty)
            done(closed = true)

          if opening1.isEmpty && listing1.isEmpty
          then
            if itemizing
            then
              next()
            else
              val indent = head.align(spacing)
              val m0 = indent.length

              if head.startsWith(indent + item+closed)
              then
                if m0 < m && closing.isEmpty
                then
                  done()
                else
                  error()
              else if m0 > m
              then
                error()
              else if m0 < m
              then
                done()
              else
                key = head.substring(m)

                if result.contains(key)
                then
                  error()
                else
                  result(key) = None

          true

      def quote(it: String): String =
        if quoting.contains(it)
        then
          val s = quoting(it)
          quoting -= it
          s
        else
          it

      def put(it: AnyRef, op: List[String] => List[String] = identity): Unit =
        assert(result(key) eq None)
        result(key) = it
        tail = op(tail)

      def add(it: AnyRef, op: List[String] => List[String] = identity): Unit =
        if result(key) eq None then result(key) = Nil
        result(key) match
          case ls: List[AnyRef] => result(key) = it :: ls
          case it => error()
        tail = head :: op(tail)


      while next()
      do

        if head1.startsWith(indent1 + item+closed)
        then
          error()

        else if m1 > m
        then

          if head1.length == m1
          then
            error()

          else if item+opened == head1.substring(m1)
          then

            opening1
              .map(_.length == m1+(item+opened).length || error())
              .getOrElse { opening1 = Some(indent1+item+opened) }

            tail.tail.parse(spacing, quoting, Some(indent1)) match

              case Some((it, tail1, true)) =>

                tail = tail1

                add(it)

              case _ =>
                error()

          else if head1.length >= m1+item.length && item == head1.substring(m1, m1+item.length)
          then
            listing1
              .map(_.length == m1+item.length || error())
              .getOrElse { listing1 = Some(indent1+item) }

            val it = head1.substring(m1 + item.length)
            add(quote(it), _.tail)

          else
            tail.parse(spacing, quoting) match
              case Some((it, tail1, false)) =>

                tail = tail1

                put(it)

              case _ =>
                error()

        else if m1 == 0
        then
          put(quote(head1), _.tail)

        else if m1 < m
        then
          done()

      thr(Some((result, Nil, closing.nonEmpty)))
    }
