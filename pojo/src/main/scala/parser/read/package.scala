package queens
package pojo
package parser
package read

import java.util.Collections.sort
import java.util.UUID

import scala.collection.mutable.{ ListBuffer => MutableList }
import scala.collection.mutable.HashMap
import scala.collection.Map

import scala.reflect.ClassTag

import common.given
import date.setDated
import pojo.jPojo.given


extension(self: String)
  inline def `toPojo*`[O <: Queens[R, B, P, S, N, Q] : ClassTag,
                       B <: Board2[R, B] : ClassTag,
                       R <: Square2 : ClassTag,
                       S <: Solution2[R, B, P, S] : ClassTag,
                       P <: Point2 : ClassTag,
                       Q <: Queens2[R, B, P, S, N, Q] : ClassTag,
                       N <: Nest2 : ClassTag
  ](spacing: String = " ",
    separator: String = "\n",
    delimiters: (String, String, String, String) = (":", "-", "[", "]") // yaml
  ): Option[O] =
    toPojo[O, B, R, S, P, Q, N](spacing, separator, delimiters)
                               (using
                                implicitly[ClassTag[O]],
                                implicitly[ClassTag[B]],
                                implicitly[ClassTag[R]],
                                implicitly[ClassTag[S]],
                                implicitly[ClassTag[P]],
                                implicitly[ClassTag[Q]],
                                implicitly[ClassTag[N]])
  private def toPojo[O <: Queens[R, B, P, S, N, Q],
                     B <: Board2[R, B],
                     R <: Square2,
                     S <: Solution2[R, B, P, S],
                     P <: Point2,
                     Q <: Queens2[R, B, P, S, N, Q],
                     N <: Nest2
  ](spacing: String,
    separator: String,
    delimiters: (String, String, String, String)
  )(using
    ct_o: ClassTag[O],
    ct_b: ClassTag[B],
    ct_r: ClassTag[R],
    ct_s: ClassTag[S],
    ct_p: ClassTag[P],
    ct_q: ClassTag[Q],
    ct_n: ClassTag[N]
  ): Option[O] =
    val quoting = HashMap[String, String]()

    val l = MutableList[String]()

    var i = 0

    for
      it <- self.split("[']")
    do
      i = i + 1
      if i % 2 == 0 then
        val uuid = UUID.randomUUID.toString
        quoting(uuid) = it
        l += uuid
      else l += it

    import text.fromText

    val (colon, item, opened, closed) = delimiters
    val spacing1 = if spacing.isEmpty then " " else spacing
    val item1 = if item.isEmpty then "-" else item
    val spacing2 = spacing1.take(item1.length.min(spacing1.length))
    val item2 = item1.take(spacing1.length.min(item1.length))

    given (String, String, String) = (item2, opened, closed)

    l
      .mkString
      .replace(s"$colon$spacing", colon)
      .replace(s"$item2$spacing$opened", item2+opened)
      .replace(s"$spacing$spacing$closed", item2+closed)
      .replace(s"$item2$spacing", item2)
      .split(s"[$colon$separator]")
      .filterNot(_.isEmpty)
      .map(_.replace('_', ' '))
      .toList
      .fromText(spacing2, quoting)
      .flatMap(_.toQueens[O, B, R, S, P, Q, N]
                         (ct_o, ct_b, ct_r, ct_s, ct_p, ct_q, ct_n))


extension(self: Map[String, AnyRef])
  private def mkObj[T](obj: T)(using
                       fun: T => Map[String, AnyRef => Unit]): Option[T] =
    val fs = fun(obj)
    if fs.keys.toList.sorted == self.keys.toList.sorted
    then
      try
        fs.foreach { case (f, s) => s(self(f)) }
        Some(obj)
      catch _ =>
        None
    else
      None


extension(text: Map[String, AnyRef])
  private def toQueens[O <: Queens[R, B, P, S, N, Q],
                       B <: Board2[R, B],
                       R <: Square2,
                       S <: Solution2[R, B, P, S],
                       P <: Point2,
                       Q <: Queens2[R, B, P, S, N, Q],
                       N <: Nest2
  ](cts: ClassTag[?]*): Option[O] =
    given (O => Map[String, AnyRef => Unit]) = { self =>
      Map[String, AnyRef => Unit](
        "board" ->  { case it: Map[String, AnyRef] =>
                        it.toBoard2[B, R](cts.tail.take(2)*)
                          .map(self.setBoard(_)).get },
        "count" -> { case it: String => self.setCount(it.toLong) },
        "queens" -> { case it: List[Map[String, AnyRef]] =>
                        val qs = MutableList[Q]()
                        it.foreach(qs += _.toQueens2[B, R, S, P, Q, N](cts.tail*).get)
                        self.setQueens(qs.toList)
                      case None =>
                        self.setQueens(Nil)
                    }
      ) ++ self.setDated()
    }

    text.mkObj[O](cts(0)).map { self => sort(self.getQueens); self }


extension(text: Map[String, AnyRef])
  private def toBoard2[B <: Board2[R, B],
                       R <: Square2
  ](cts: ClassTag[?]*): Option[B] =
    if text.contains("graphics") then
      return (text - "graphics").toBoard2[B, R](cts*)

    given (B => Map[String, AnyRef => Unit]) = { self =>
      Map[String, AnyRef => Unit](
        "squares" ->  { case it: List[Map[String, AnyRef]] =>
                          val sqs = MutableList[R]()
                          it.foreach(sqs += _.toSquare2[R](cts(1)).get)
                          self.setSquares(sqs.toList)
                        case None =>
                          self.setSquares(Nil)
                      },
        "seed" -> { case it: String => self.setSeed(it.toLong) }
      ) ++ self.setDated()
    }

    text.mkObj[B](cts(0)).map { self => sort(self.getSquares); self }


extension(text: Map[String, AnyRef])
  private def toSquare2[R <: Square2](ct: ClassTag[?]): Option[R] =
    given (R => Map[String, AnyRef => Unit]) = { self =>
      Map(
        "row" -> { case it: String => self.setRow(it.toInt) },
        "column" -> { case it: String => self.setColumn(it.toInt) },
        "free" -> { case it: String => self.setFree(java.lang.Boolean.parseBoolean(it)) }
      )
    }

    text.mkObj[R](ct)


extension(text: Map[String, AnyRef])
  private def toQueens2[B <: Board2[R, B],
                        R <: Square2,
                        S <: Solution2[R, B, P, S],
                        P <: Point2,
                        Q <: Queens2[R, B, P, S, N, Q],
                        N <: Nest2
  ](cts: ClassTag[?]*): Option[Q] =
    given (Q => Map[String, AnyRef => Unit]) = { self =>
      Map[String, AnyRef => Unit](
        "tags" -> { case it: List[String] => self.setTags(it.sorted) },
        "validator" -> { case it: String => self.setValidator(it) },
        "tag" -> { case it: String => self.setTag(it) },
        "tag" -> { case it: String => self.setTag(UUID.fromString(it).toString) },
        "uuid" -> { case it: String => self.setUUID(UUID.fromString(it)) },
        "nest" -> { case it: Map[String, AnyRef] =>
                      it.toNest2[N](cts(5))
                        .map(self.setNest(_)).get },
        "solutions" ->  { case it: List[Map[String, AnyRef]] =>
                            val qs = MutableList[S]()
                            it.foreach(qs += _.toSolution2[B, R, S, P](cts.take(4)*).get)
                            self.setSolutions(qs.toList.sortBy(_.getNumber))
                          case None =>
                            self.setSolutions(Nil)
                        }
      ) ++ self.setDated()
    }

    text.mkObj[Q](cts(4))


extension(text: Map[String, AnyRef])
  private def toNest2[N <: Nest2](ct: ClassTag[?]): Option[N] =
    import dimensions.Dimension.{ Aspect, Algorithm, Model }

    given (N => Map[String, AnyRef => Unit]) = { self =>
      Map(
        "aspect" -> { case it: String => self.setAspect(Aspect(it).get.toString) },
        "algorithm" -> { case it: String => self.setAlgorithm(Algorithm(it).get.toString) },
        "model" -> { case it: String => self.setModel(Model(it).get.toString) }
      )
    }

    text.mkObj[N](ct)


extension(text: Map[String, AnyRef])
  private def toSolution2[B <: Board2[R, B],
                          R <: Square2,
                          S <: Solution2[R, B, P, S],
                          P <: Point2,
  ](cts: ClassTag[?]*): Option[S] =
    given (S => Map[String, AnyRef => Unit]) = { self =>
      Map[String, AnyRef => Unit](
        "number" -> { case it: String => self.setNumber(it.toLong) },
        "board" ->  { case it: Map[String, AnyRef] =>
                        it.toBoard2[B, R](cts.take(2)*)
                          .map(self.setBoard(_)).get },
        "queens" -> { case it: List[Map[String, AnyRef]] =>
                        val qs = MutableList[P]()
                        it.foreach(qs += _.toPoint2[P](cts(3)).get)
                        self.setQueens(qs.toList)
                      case None =>
                        self.setQueens(Nil)
                    }
      ) ++ self.setDated()
    }

    text.mkObj[S](cts(2)).map { self => sort(self.getQueens); self }


extension(text: Map[String, AnyRef])
  private def toPoint2[P <: Point2](ct: ClassTag[?]): Option[P] =
    given (P => Map[String, AnyRef => Unit]) = { self =>
      Map(
        "row" -> { case it: String => self.setRow(it.toInt) },
        "column" -> { case it: String => self.setColumn(it.toInt) }
      )
    }

    text.mkObj[P](ct)
