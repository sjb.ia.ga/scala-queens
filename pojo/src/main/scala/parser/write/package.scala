package queens
package pojo
package parser

import scala.collection.JavaConverters.iterableAsScalaIterableConverter

import scala.collection.mutable.{ ListBuffer => MutableList }
import scala.collection.Map

import queens.base.Board

import date.getDated
import pojo.jPojo.given


package write:

  object jWrite: // plainjava

    import scala.collection.mutable.HashMap

    extension[O <: Queens[R, B, P, S, N, Q],
              B <: Board2[R, B],
              R <: Square2,
              S <: Solution2[R, B, P, S],
              P <: Point2,
              Q <: Queens2[R, B, P, S, N, Q],
              N <: Nest2
    ](self: O)
      def toData(square: String = "",
                 piece: String = "",
                 queen: String = ""
      ): Map[String, Any] =

        val response = HashMap[String, Any](
          "count" -> self.getCount
        ) ++ self.getDated()

        val board: Board = self.getBoard

        response("board") = Map[String, Any](
          "squares" -> board.mkSquares(),
          "seed" -> board.seed.getOrElse(Long.MinValue)
        ) ++ (
          if square.length == piece.length &&
             piece.length == queen.length &&
             square.nonEmpty
          then
            Map[String, Any]("graphics" -> Map[String, Any]("asciiart" -> board.mkBoard0(square, piece)))
          else
            Map.empty
        ) ++ self.getBoard.getDated()

        if self.getCount == 0 then
          response("queens") = Nil
          return response

        given Option[Solution => List[String]] =
          if square.length == piece.length &&
             piece.length == queen.length &&
             square.nonEmpty
          then
            Some(board.mkBoard1(square, piece, queen))
          else
            None

        given Board = board

        val q = MutableList[Map[String, Any]]()

        self.getQueens.asScala.foreach { it =>
          q += it.mkQueens ++ it.getDated()
        }

        response("queens") = q.toList

        response


package object write:

  import scala.Function.const

  import common.geom.x

  import version.less.nest.Nest

  extension(self: Board)
    def mkSquares(): List[Any] =
      val squares = MutableList[Any]()
      for
        i <- 0 until self.N
        j <- 0 until self.N
      do
        val square = Map[String, Any]("row" -> i, "column" -> j, "free" -> !self(i)(j))
        squares += square
      squares.toList

    def mkBoard0(square: String,
                 piece: String
    ): List[String] =
      self { (i, j) => square }(piece).map(_.mkString).toList

    def mkBoard1(square: String,
                 piece: String,
                 queen: String
    ): Solution => List[String] = { it =>
      self { (i, j) =>
              it.find((i x j).==)
               .map(const(queen))
               .getOrElse(square)
      }(piece)
        .map(_.mkString)
        .toList
    }


  extension[B <: Board2[R, B],
            R <: Square2,
            S <: Solution2[R, B, P, S],
            P <: Point2,
            Q <: Queens2[R, B, P, S, N, Q],
            N <: Nest2
  ](self: Q)
    private def mkQueens(using
                         board: Board,
                         mkBoard: Option[Solution => List[String]]
    ): Map[String, Any] =
      val nest: Nest = self.getNest
      Map[String, Any](
        "tags" -> self.getTags.asScala.toList,
        "validator" -> self.getValidator,
        "tag" -> self.getTag,
        "uuid" -> self.getUUID,
        "nest" -> Map[String, Any]("aspect" -> nest.aspect,
                                   "algorithm" -> nest.algorithm,
                                   "model" -> nest.model),
        "solutions" -> {

          import Board.apply

          self.getSolutions.asScala
            .map { it =>
              val solution: Solution = it

              Map[String, Any](
                "number" -> it.getNumber,
                "queens" -> solution.map(mkPoint),
                "board" -> (
                  Map[String, Any](
                    "squares" -> board(it).mkSquares(),
                    "seed" -> board.seed.getOrElse(Long.MinValue)
                  ) ++ (
                    mkBoard match
                      case Some(mk) => Map[String, Any]("graphics" -> Map[String, Any]("asciiart" -> mk(it)))
                      case _ => Map.empty
                  ) ++ it.getBoard.getDated()
                )
              ) ++ it.getDated()
            }.toList
        }
    )

    private def mkPoint(it: Point): Map[String, Any] =
      import common.geom.{ row, col }
      Map("row" -> it.row, "column" -> it.col)
