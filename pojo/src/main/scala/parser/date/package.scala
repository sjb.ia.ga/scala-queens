package queens
package pojo


package parser:

  import java.util.Date

  import scala.collection.immutable.ListMap


  package date:

    object jDate: // plainjava

      extension(self: Dated)
        @inline def apply(started: Date, ended: Date, idle: Option[Long] = None): Long =
          self.setStarted(started)
          self.setEnded(ended)
          self.setElapsed(ended.getTime - started.getTime)
          self.setIdle(idle.map { (_: Long).span[(Date, Long)](ended) match
                                                                        case (now, it) =>
                                                                          self.setEnded(now)
                                                                          it
                                }.getOrElse(0L))
          self.getIdle()

      import scala.reflect.ClassTag

      extension(self: Long)
        @inline def spanFrom(from: Date): Long =
          Macros.span(self, from)._2


    import scala.collection.Map

    extension(self: Dated)
      @inline def setDated(): Map[String, Any => Unit] = Map(
        "started" -> { case it: String => self.setStarted(df.parse(it)) },
        "ended" -> { case it: String => self.setEnded(df.parse(it)) },
        "elapsed" -> { case it: String => self.setElapsed(it.toLong) },
        "idle" -> { case it: String => self.setIdle(it.toLong) }
      )

    extension(self: Dated)
      inline def getDated(quote: String = "'"): Map[String, Any] =
        ListMap[String, Any](
          "started" -> s"$quote${df.format(self.getStarted)}$quote",
          "ended" -> s"$quote${df.format(self.getEnded)}$quote",
          "elapsed" -> self.getElapsed,
          "idle" -> self.getIdle
        )

    import scala.reflect.ClassTag

    extension(self: Long)
      @inline def span[R](from: Date)(using
                          classTag: ClassTag[R]): R =
        val (now, idle) = Macros.span(self, from)
        ( if classTag.runtimeClass eq classOf[Long]
          then idle else (Date(now), idle)
        ).asInstanceOf[R]
      @inline def span(from: Date, to: Date): Long =
        Macros.span(self, from, to)._2
