package queens
package pojo


package object parser:

  import java.text.SimpleDateFormat

  lazy val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
