package object queens:

  import common.geom.Coord

  type Point = Coord[Int]

  type Solution = List[Point]


  type Identity_[A] = A

  type Iterable_[X[_], A] = Iterable[A]


  import java.util.UUID

  import base.Board

  import version.less.nest.Nest

  type Cue = ((Board, Nest, Long, UUID), Long, Long, Long, Long)


  type `Queens3*` = dimensions.third.`Queens*`[Point, Boolean]


package queens:

  import java.util.Collections.sort

  import scala.collection.Map
  import scala.collection.Seq
  import scala.collection.mutable.{ ListBuffer => MutableList }
  import scala.collection.JavaConverters.iterableAsScalaIterableConverter

  import scala.reflect.ClassTag

  import base.Board
  import common.given

  import version.less.nest.Nest


  package pojo:

    object jPojo: // plainjava

      inline given [T]: Conversion[Seq[T], java.util.List[T]] = { it =>
        val ls = new java.util.LinkedList[T]();
        it.foreach(ls.add(_));
        ls
      }

      inline given [K, V]: Conversion[Map[K, V], java.util.Map[K, V]] = { it =>
        val m = new java.util.HashMap[K, V]();
        it.foreach(m.put(_, _));
        m
      }


      given [B <: Board2[R, B],
             R <: Square2
      ]: Conversion[B, Board] = { self =>
        given Option[Long] = if self.getSeed == Long.MinValue then None else Some(self.getSeed)

        if self.getSquares.isEmpty
        then
          new Board(Nil)
        else
          val ps = new java.util.ArrayList[R]()
          ps.addAll(self.getSquares.asInstanceOf[java.util.List[R]])
          sort(ps.asInstanceOf[java.util.List[Point2]])
          var N = 0
          var p: Option[R] = None
          val squares = MutableList[Boolean]()
          for
            sq <- ps.asScala
          do
            if N == 0 then
              if sq.getRow == 1 then N = p.get.getColumn + 1
              else p = Some(sq)
            squares += !sq.isFree
          new Board(squares.toList.sliding(N, N).toList)
      }

      given [B <: Board2[R, B] : ClassTag,
             R <: Square2 : ClassTag
      ]: Conversion[Board, B] with
        def apply(self: Board): B =
          val board: B = implicitly[ClassTag[B]]
          val squares = new java.util.ArrayList[R]()
          for
            i <- 0 until self.N
            j <- 0 until self.N
          do
            val square: R = implicitly[ClassTag[R]]
            square.setRow(i)
            square.setColumn(j)
            square.setFree(!self(i)(j))
            squares.add(square)
          board.setSquares(squares)
          board.setSeed(self.seed.getOrElse(Long.MinValue))
          board


      import dimensions.Dimension.{ Aspect, Algorithm, Model }

      given [N <: Nest2]: Conversion[N, Nest] = { it => Nest(Aspect(it.getAspect).get
                                                            ,Algorithm(it.getAlgorithm).get
                                                            ,Model(it.getModel).get) }

      given [N <: Nest2 : ClassTag]: Conversion[Nest, N] with
        def apply(it: Nest): N =
          val nest: N = implicitly[ClassTag[N]]
          nest.setAspect(it.aspect.toString)
          nest.setAlgorithm(it.algorithm.toString)
          nest.setModel(it.model.toString)
          nest


      import common.geom.x

      given [B <: Board2[R, B],
             R <: Square2,
             S <: Solution2[R, B, P, S] : ClassTag,
             P <: Point2 : ClassTag
      ]: Conversion[Solution, S] with
        def apply(it: Solution): S =
          val solution: S = implicitly[ClassTag[S]]
          val queens = it.map { (row, col) =>
                                val point: P = implicitly[ClassTag[P]]
                                point.setRow(row)
                                point.setColumn(col)
                                point
                              }
          solution.setQueens(queens)
          solution

      given [S <: Solution2[?, ?, ?, ?]]: Conversion[S, Solution] = _.getQueens.asScala
        .map { it => it.getRow x it.getColumn }.toList

      given [S <: Solution2[?, ?, ?, ?]]: Conversion[java.util.List[S], Seq[Solution]] with
        def apply(self: java.util.List[S]): Seq[Solution] =
          val r: MutableList[Solution] = MutableList.fill(self.size)(Nil)
          for
            it <- self.asScala
          do
            r(it.getNumber.toInt-1) = it
          r


      given [B <: Board2[R, B] : ClassTag,
             R <: Square2 : ClassTag,
             N <: Nest2 : ClassTag,
             C <: Cue2[R, B, N] : ClassTag
      ]: Conversion[Cue, C] = { case ((board, nest, max, uuid), number, cue, _, _) =>
          val r: C = implicitly[ClassTag[C]]
          r.setBoard(board)
          r.setNest(nest)
          r.setMax(max)
          r.setUUID(uuid)
          r.setNumber(number)
          r.setCue(cue)
          r
      }

      given [B <: Board2[R, B],
             R <: Square2,
             N <: Nest2,
             C <: Cue2[R, B, N]
      ]: Conversion[C, Cue] with
        def apply(it: C): Cue =
          val board: Board = it.getBoard()
          val nest: Nest = it.getNest()
          ((board, nest, it.getMax, it.getUUID), it.getNumber, it.getCue, -1L, -1L)


  package object pojo:

    type FromResponseToQueens[Q] = Function3[Q, Any, String, Iterable[`Queens3*`]]

    extension[T](self: java.util.List[T])
      inline def last: T = self.get(self.size-1)
