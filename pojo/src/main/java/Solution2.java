package queens.pojo;

import java.util.List;


public interface Solution2<R extends Square2, B extends Board2<R, B>, P extends Point2, S extends Solution2<R, B, P, S>> extends Dated, Comparable<S> {
    long getNumber();

    void setNumber(long number);

    List<P> getQueens();

    void setQueens(List<P> queens);

    B getBoard();

    void setBoard(B board);

    default int compareTo(S that) {
        return this.getNumber() < that.getNumber() ? -1 : this.getNumber() > that.getNumber() ? 1 : 0;
    }
}
