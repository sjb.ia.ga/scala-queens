package queens.pojo;

import java.util.UUID;
import java.util.List;


public interface Queens2<R extends Square2, B extends Board2<R, B>, P extends Point2, S extends Solution2<R, B, P, S>, N extends Nest2, Q extends Queens2<R, B, P, S, N, Q>> extends Dated, Comparable<Q> {
    List<String> getTags();

    String getValidator();

    void setValidator(String validator);

    void setTags(List<String> tags);

    String getTag();

    void setTag(String tag);

    UUID getUUID();

    void setUUID(UUID uuid);

    N getNest();

    void setNest(N nest);

    List<S> getSolutions();

    void setSolutions(List<S> solutions);

    default int compareTo(Q that) {
        return this.getTag().equals(that.getTag())
            ? this.getUUID().compareTo(that.getUUID())
            : this.getTag().compareTo(that.getTag());

    }
}
