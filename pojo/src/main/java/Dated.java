package queens.pojo;

import java.util.Date;


public interface Dated {
    Date getStarted();

    void setStarted(Date started);

    Date getEnded();

    void setEnded(Date ended);

    long getElapsed();

    void setElapsed(long elapsed);

    long getIdle();

    void setIdle(long idle);
}
