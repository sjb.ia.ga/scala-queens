package queens.pojo.moshi;

import java.util.Date;


public abstract class Dated<D extends Dated<D>> implements queens.pojo.base.Dated<D> {
    private Date started;

    private Date ended;

    private long elapsed;

    private long idle;

    public Date getStarted() {
        return started;
    }

    public void setStarted(Date started) {
        this.started = started;
    }

    public Date getEnded() {
        return ended;
    }

    public void setEnded(Date ended) {
        this.ended = ended;
    }

    public long getElapsed() {
        return elapsed;
    }

    public void setElapsed(long elapsed) {
        this.elapsed = elapsed;
    }

    public long getIdle() {
        return idle;
    }

    public void setIdle(long idle) {
        this.idle = idle;
    }
}
