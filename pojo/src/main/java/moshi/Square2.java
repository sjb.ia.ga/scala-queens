package queens.pojo.moshi;


public class Square2 extends queens.pojo.moshi.base.Point2<Square2> implements queens.pojo.base.Square2<Square2> {
    private boolean free;

    public Square2() {
    }

    public Square2(int row, int column, boolean free) {
        super(row, column);
        this.free = free;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }
}
