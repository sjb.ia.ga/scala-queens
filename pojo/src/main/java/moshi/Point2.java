package queens.pojo.moshi;

import com.squareup.moshi.JsonClass;


@JsonClass(generateAdapter = false)
public class Point2 extends queens.pojo.moshi.base.Point2<Point2> {
    public Point2() {
    }

    public Point2(int row, int column) {
        super(row, column);
    }
}
