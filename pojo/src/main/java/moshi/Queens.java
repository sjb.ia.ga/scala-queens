package queens.pojo.moshi;

import java.util.Date;
import java.util.List;

import com.squareup.moshi.JsonClass;


@JsonClass(generateAdapter = false)
public class Queens extends Dated<Queens> implements queens.pojo.base.Queens<Square2, Board2, Point2, Solution2, Nest2, Queens2, Queens> {
    private Board2 board;

    private List<Queens2> queens;

    private long count;

    public Queens() {
    }

    public void setBoard(Board2 board) {
        this.board = board;
    }

    public Board2 getBoard() {
        return board;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getCount() {
        return count;
    }

    public List<Queens2> getQueens() {
        return queens;
    }

    public void setQueens(List<Queens2> queens) {
        this.queens = queens;
    }

    @Override
    public int hashCode() {
        return hashCodeOops();
    }

    @Override
     public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equalsOops((Queens) obj);
    }
}
