package queens.pojo;

import java.util.List;


public interface Board2<R extends Square2, B extends Board2<R, B>> extends Dated {
    List<R> getSquares();

    void setSquares(List<R> squares);

    long getSeed();

    void setSeed(long seed);
}
