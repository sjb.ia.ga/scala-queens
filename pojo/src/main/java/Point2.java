package queens.pojo;


public interface Point2 extends Comparable<Point2> {
    int getRow();

    void setRow(int row);

    int getColumn();

    void setColumn(int column);

    default int compareTo(Point2 that) {
        return this.getRow() == that.getRow()
            ? this.getColumn() - that.getColumn()
            : this.getRow() - that.getRow();
    }
}
