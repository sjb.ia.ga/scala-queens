package queens.pojo;

import java.util.Date;
import java.util.List;


public interface Queens<R extends Square2, B extends Board2<R, B>, P extends Point2, S extends Solution2<R, B, P, S>, N extends Nest2, Q extends Queens2<R, B, P, S, N, Q>> extends Dated {
    void setBoard(B board);

    B getBoard();

    void setCount(long count);

    long getCount();

    List<Q> getQueens();

    void setQueens(List<Q> queens);
}
