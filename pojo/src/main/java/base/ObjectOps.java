package queens.pojo.base;


public interface ObjectOps<O extends ObjectOps<O>> {
    int hashCodeOops();

    boolean equalsOops(O that);
}
