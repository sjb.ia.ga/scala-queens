package queens.pojo.base;

import java.util.UUID;
import java.util.List;

import scala.Tuple6;


public interface Queens2<R extends Square2<R>, B extends Board2<R, B>, P extends Point2<P>, S extends Solution2<R, B, P, S>, N extends Nest2<N>, Q extends Queens2<R, B, P, S, N, Q>> extends queens.pojo.Queens2<R, B, P, S, N, Q>, Dated<Q>, ObjectOps<Q> {
    default int hashCodeOops() {
        return Tuple6.apply(hashCodeDated(), getTags(), getTag(), getUUID(), getNest(), getSolutions()).hashCode();
    }

    default boolean equalsOops(Q that) {
        if (equalsDated(that) &&
            this.getTag().equals(that.getTag()) &&
            this.getUUID().equals(that.getUUID()) &&
            this.getNest().equals(that.getNest())) {

            int size = 0;

            if (this.getTags().size() != that.getTags().size())
                return false;

            if (this.getSolutions().size() != that.getSolutions().size())
                return false;

            size = getTags().size();

            for (int i = 0; i < size; i++)
                if (!this.getTags().get(i).equals(that.getTags().get(i)))
                    return false;

            size = getSolutions().size();

            for (int i = 0; i < size; i++)
                if (!this.getSolutions().get(i).equals(that.getSolutions().get(i)))
                    return false;

            return true;
        }

        return false;
    }
}
