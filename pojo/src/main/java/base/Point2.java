package queens.pojo.base;

import scala.Tuple2;


public interface Point2<P extends Point2<P>> extends queens.pojo.Point2, ObjectOps<P> {
    default int hashCodeOops() {
        return Tuple2.apply(getRow(), getColumn()).hashCode();
    }

    default boolean equalsOops(P that) {
        return
            this.getRow() == that.getRow() &&
            this.getColumn() == that.getColumn();
    }
}
