package queens.pojo.base;

import scala.Tuple6;


public interface Cue2<R extends Square2<R>, B extends Board2<R, B>, N extends Nest2<N>, C extends Cue2<R, B, N, C>> extends queens.pojo.Cue2<R, B, N>, ObjectOps<C> {
    default int hashCodeOops() {
        return Tuple6.apply(getBoard(), getNest(), getMax(), getUUID(), getNumber(), getCue()).hashCode();
    }

    default boolean equalsOops(C that) {
        return
            this.getMax() == that.getMax() &&
            this.getNumber() == that.getNumber() &&
            this.getCue() == that.getCue() &&
            this.getUUID().equals(that.getUUID()) &&
            this.getNest().equals(that.getNest()) &&
            this.getBoard().equals(that.getBoard());
    }
}
