package queens.pojo.base;

import scala.Tuple3;


public interface Nest2<N extends Nest2<N>> extends queens.pojo.Nest2, ObjectOps<N> {
    default int hashCodeOops() {
        return Tuple3.apply(getAspect(), getAlgorithm(), getModel()).hashCode();
    }

    default boolean equalsOops(N that) {
        return
            this.getAspect().equals(that.getAspect()) &&
            this.getAlgorithm().equals(that.getAlgorithm()) &&
            this.getModel().equals(that.getModel());
    }
}
