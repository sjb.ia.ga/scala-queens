package queens.pojo.base;

import java.util.Date;
import java.util.List;

import scala.Tuple4;


public interface Queens<R extends Square2<R>, B extends Board2<R, B>, P extends Point2<P>, S extends Solution2<R, B, P, S>, N extends Nest2<N>, Q extends Queens2<R, B, P, S, N, Q>, O extends Queens<R, B, P, S, N, Q, O>> extends queens.pojo.Queens<R, B, P, S, N, Q>, Dated<O>, ObjectOps<O> {
    default int hashCodeOops() {
        return Tuple4.apply(hashCodeDated(), getBoard(), getQueens(), getCount()).hashCode();
    }

    default boolean equalsOops(O that) {
        if (equalsDated(that) &&
            this.getCount() == that.getCount() &&
            this.getBoard().equals(that.getBoard())) {

            if (this.getQueens().size() != that.getQueens().size())
                return false;

            int size = getQueens().size();

            for (int i = 0; i < size; i++)
                if (!this.getQueens().get(i).equals(that.getQueens().get(i)))
                    return false;

            return true;
        }

        return false;
    }
}
