package queens.pojo.base;

import java.util.List;

import scala.Tuple4;


public interface Solution2<R extends Square2<R>, B extends Board2<R, B>, P extends Point2<P>, S extends Solution2<R, B, P, S>> extends queens.pojo.Solution2<R, B, P, S>, Dated<S>, ObjectOps<S> {
    default int hashCodeOops() {
        return Tuple4.apply(hashCodeDated(), getNumber(), getQueens(), getBoard()).hashCode();
    }

    default boolean equalsOops(S that) {
        if (equalsDated(that) &&
            this.getNumber() == that.getNumber() &&
            this.getBoard().equals(that.getBoard())) {

            if (this.getQueens().size() != that.getQueens().size())
                return false;

            int size = getQueens().size();

            for (int i = 0; i < size; i++)
                if (!this.getQueens().get(i).equals(that.getQueens().get(i)))
                    return false;

            return true;
        }

        return false;
    }
}
