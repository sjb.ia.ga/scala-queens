package queens.pojo.base;

import scala.Tuple3;


public interface Square2<R extends Square2<R>> extends queens.pojo.Square2, Point2<R> {
    default int hashCodeOops() {
        return Tuple3.apply(getRow(), getColumn(), isFree()).hashCode();
    }

    default boolean equalsOops(R that) {
        return
            this.getRow() == that.getRow() &&
            this.getColumn() == that.getColumn() &&
            this.isFree() == that.isFree();
    }
}
