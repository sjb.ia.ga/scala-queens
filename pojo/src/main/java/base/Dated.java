package queens.pojo.base;

import java.util.Date;

import scala.Tuple4;


public interface Dated<D extends Dated<D>> extends queens.pojo.Dated {
    default int hashCodeDated() {
        return Tuple4.apply(getStarted(), getEnded(), getElapsed(), getIdle()).hashCode();
    }

    default boolean equalsDated(D that) {
        return
            this.getStarted().equals(that.getStarted()) &&
            this.getEnded().equals(that.getEnded()) &&
            this.getElapsed() == that.getElapsed() &&
            this.getIdle() == that.getIdle();
    }
}
