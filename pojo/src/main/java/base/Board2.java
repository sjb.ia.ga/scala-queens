package queens.pojo.base;

import java.util.List;

import scala.Tuple2;


public interface Board2<R extends Square2<R>, B extends Board2<R, B>> extends queens.pojo.Board2<R, B>, Dated<B>, ObjectOps<B> {
    default int hashCodeOops() {
        return Tuple2.apply(hashCodeDated(), getSquares()).hashCode();
    }

    default boolean equalsOops(B that) {
        if (equalsDated(that) &&
            this.getSeed() == that.getSeed()) {
            if (this.getSquares().size() != that.getSquares().size())
                return false;

            int size = getSquares().size();

            for (int i = 0; i < size; i++)
                if (!this.getSquares().get(i).equals(that.getSquares().get(i)))
                    return false;

            return true;
        }

        return false;
    }
}
