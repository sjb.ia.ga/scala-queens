package queens.pojo;

import java.util.UUID;


public interface Cue2<R extends Square2, B extends Board2<R, B>, N extends Nest2> {
    void setBoard(B board);

    B getBoard();

    void setNest(N nest);

    N getNest();

    void setMax(long max);

    long getMax();

    void setUUID(UUID uuid);

    UUID getUUID();

    void setNumber(long number);

    long getNumber();

    void setCue(long cue);

    long getCue();
}
