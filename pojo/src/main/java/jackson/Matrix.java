package queens.pojo.jackson;

import java.util.Map;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class Matrix implements queens.pojo.Matrix {

    @JacksonXmlElementWrapper(localName = "aspects")
    @JacksonXmlProperty(localName = "aspects")
    private Map<String, Integer> aspects;

    @JacksonXmlElementWrapper(localName = "algorithms")
    @JacksonXmlProperty(localName = "algorithms")
    private Map<String, Integer> algorithms;


    public Map<String, Integer> getAspects() {
        return aspects;
    }

    public void setAspects(Map<String, Integer> aspects) {
        this.aspects = aspects;
    }

    public Map<String, Integer> getAlgorithms() {
        return algorithms;
    }

    public void setAlgorithms(Map<String, Integer> algorithms) {
        this.algorithms = algorithms;
    }
}
