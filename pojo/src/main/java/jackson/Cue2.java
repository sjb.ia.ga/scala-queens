package queens.pojo.jackson;

import java.util.UUID;


public class Cue2 implements queens.pojo.base.Cue2<Square2, Board2, Nest2, Cue2> {
    private Board2 board;

    private Nest2 nest;

    private long max;

    private UUID uuid;

    private long number;

    private long cue;

    public Cue2() {
    }

    public Board2 getBoard() {
        return board;
    }

    public void setBoard(Board2 board) {
        this.board = board;
    }

    public Nest2 getNest() {
        return nest;
    }

    public void setNest(Nest2 nest) {
        this.nest = nest;
    }

    public long getMax() {
        return max;
    }

    public void setMax(long max) {
        this.max = max;
    }

    public UUID getUUID() {
        return uuid;
    }

    public void setUUID(UUID uuid) {
        this.uuid = uuid;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public long getCue() {
        return cue;
    }

    public void setCue(long cue) {
        this.cue = cue;
    }

    @Override
    public int hashCode() {
        return hashCodeOops();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equalsOops((Cue2) obj);
    }

    @Override
    public String toString() {
        return "Cue2 [number=" + number + ", cue=" + cue + "]";
    }
}
