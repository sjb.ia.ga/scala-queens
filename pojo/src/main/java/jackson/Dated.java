package queens.pojo.jackson;

import java.util.Date;

// https://stackoverflow.com/questions/12463049/date-format-mapping-to-json-jackson
import com.fasterxml.jackson.annotation.JsonFormat;


public abstract class Dated<D extends Dated<D>> implements queens.pojo.base.Dated<D> {
    private Date started;

    private Date ended;

    private long elapsed;

    private long idle;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    public Date getStarted() {
        return started;
    }

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    public void setStarted(Date started) {
        this.started = started;
    }

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    public Date getEnded() {
        return ended;
    }

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    public void setEnded(Date ended) {
        this.ended = ended;
    }

    public long getElapsed() {
        return elapsed;
    }

    public void setElapsed(long elapsed) {
        this.elapsed = elapsed;
    }

    public long getIdle() {
        return idle;
    }

    public void setIdle(long idle) {
        this.idle = idle;
    }
}
