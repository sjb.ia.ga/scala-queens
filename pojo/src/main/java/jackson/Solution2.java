package queens.pojo.jackson;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


public class Solution2 extends Dated<Solution2> implements queens.pojo.base.Solution2<Square2, Board2, Point2, Solution2> {
    private long number;

    @JacksonXmlElementWrapper(localName = "queens")
    @JacksonXmlProperty(localName = "point")
    private List<Point2> queens;

    private Board2 board;

    public Solution2() {
    }

    public Solution2(List<Point2> queens) {
        this.queens = queens;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public List<Point2> getQueens() {
        return queens;
    }

    public void setQueens(List<Point2> queens) {
        this.queens = queens;
    }

    public Board2 getBoard() {
        return board;
    }

    public void setBoard(Board2 board) {
        this.board = board;
    }

    @Override
    public int hashCode() {
        return hashCodeOops();
    }

    @Override
     public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equalsOops((Solution2) obj);
    }
}
