package queens.pojo;


public interface Square2 extends Point2 {
    boolean isFree();

    void setFree(boolean free);
}
