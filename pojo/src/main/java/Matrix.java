package queens.pojo;

import java.util.Map;


public interface Matrix {
    Map<String, Integer> getAspects();

    void setAspects(Map<String, Integer> aspects);

    Map<String, Integer> getAlgorithms();

    void setAlgorithms(Map<String, Integer> algorithms);
}
