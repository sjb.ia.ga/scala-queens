package queens.pojo.gson.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("unchecked")
public abstract class Point2<P extends Point2<P>> implements queens.pojo.base.Point2<P> {
    @Expose
    @SerializedName("row")
    private int row;

    @Expose
    @SerializedName("column")
    private int column;

    protected Point2() {
    }

    protected Point2(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    @Override
    public int hashCode() {
        return hashCodeOops();
    }

    @Override
     public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equalsOops((P) obj);
    }
}
