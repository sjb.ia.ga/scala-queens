package queens.pojo.gson;

import java.util.UUID;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Queens2 extends Dated<Queens2> implements queens.pojo.base.Queens2<Square2, Board2, Point2, Solution2, Nest2, Queens2> {
    @Expose
    @SerializedName("tags")
    private List<String> tags;

    @Expose
    @SerializedName("validator")
    private String validator;

    @Expose
    @SerializedName("iteration")
    private String tag;

    @Expose
    @SerializedName("uuid")
    private UUID uuid;

    @Expose
    @SerializedName("nest")
    private Nest2 nest;

    @Expose
    @SerializedName("solutions")
    private List<Solution2> solutions;

    public Queens2() {
    }

    public Queens2(String tag, UUID uuid, Nest2 nest) {
        this.tag = tag;
        this.uuid = uuid;
        this.nest = nest;
    }

    public List<String> getTags() {
        return tags;
    }

    public String getValidator() {
        return validator;
    }

    public void setValidator(String validator) {
        this.validator = validator;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public UUID getUUID() {
        return uuid;
    }

    public void setUUID(UUID uuid) {
        this.uuid = uuid;
    }

    public Nest2 getNest() {
        return nest;
    }

    public void setNest(Nest2 nest) {
        this.nest = nest;
    }

    public List<Solution2> getSolutions() {
        return solutions;
    }

    public void setSolutions(List<Solution2> solutions) {
        this.solutions = solutions;
    }

    @Override
    public int hashCode() {
        return hashCodeOops();
    }

    @Override
     public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equalsOops((Queens2) obj);
    }
}
