package queens.pojo.gson;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Solution2 extends Dated<Solution2> implements queens.pojo.base.Solution2<Square2, Board2, Point2, Solution2> {
    @Expose
    @SerializedName("number")
    private long number;

    @Expose
    @SerializedName("queens")
    private List<Point2> queens;

    @Expose
    @SerializedName("board")
    private Board2 board;

    public Solution2() {
    }

    public Solution2(List<Point2> queens) {
        this.queens = queens;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public List<Point2> getQueens() {
        return queens;
    }

    public void setQueens(List<Point2> queens) {
        this.queens = queens;
    }

    public Board2 getBoard() {
        return board;
    }

    public void setBoard(Board2 board) {
        this.board = board;
    }

    @Override
    public int hashCode() {
        return hashCodeOops();
    }

    @Override
     public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equalsOops((Solution2) obj);
    }
}
