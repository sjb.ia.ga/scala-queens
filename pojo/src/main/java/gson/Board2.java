package queens.pojo.gson;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Board2 extends Dated<Board2> implements queens.pojo.base.Board2<Square2, Board2> {
    @Expose
    @SerializedName("squares")
    private List<Square2> squares;

    @Expose
    @SerializedName("seed")
    private long seed;

    public Board2() {
    }

    public List<Square2> getSquares() {
        return squares;
    }

    public void setSquares(List<Square2> squares) {
        this.squares = squares;
    }

    public long getSeed() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

    @Override
    public int hashCode() {
        return hashCodeOops();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equalsOops((Board2) obj);
    }
}
