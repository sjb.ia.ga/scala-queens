package queens.pojo.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Square2 extends queens.pojo.gson.base.Point2<Square2> implements queens.pojo.base.Square2<Square2> {
    @Expose
    @SerializedName("free")
    private boolean free;

    public Square2() {
    }

    public Square2(int row, int column, boolean free) {
        super(row, column);
        this.free = free;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }
}
