package queens.pojo.gson;

import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public abstract class Dated<D extends Dated<D>> implements queens.pojo.base.Dated<D> {
    @Expose
    @SerializedName("started")
    private Date started;

    @Expose
    @SerializedName("ended")
    private Date ended;

    @Expose
    @SerializedName("elapsed")
    private long elapsed;

    @Expose
    @SerializedName("idle")
    private long idle;

    public Date getStarted() {
        return started;
    }

    public void setStarted(Date started) {
        this.started = started;
    }

    public Date getEnded() {
        return ended;
    }

    public void setEnded(Date ended) {
        this.ended = ended;
    }

    public long getElapsed() {
        return elapsed;
    }

    public void setElapsed(long elapsed) {
        this.elapsed = elapsed;
    }

    public long getIdle() {
        return idle;
    }

    public void setIdle(long idle) {
        this.idle = idle;
    }
}
