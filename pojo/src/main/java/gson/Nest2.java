package queens.pojo.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Nest2 implements queens.pojo.base.Nest2<Nest2> {
    @Expose
    @SerializedName("aspect")
    private String aspect;

    @Expose
    @SerializedName("algorithm")
    private String algorithm;

    @Expose
    @SerializedName("model")
    private String model;

    public Nest2() {
    }

    public Nest2(String aspect, String algorithm, String model) {
        this.aspect = aspect;
        this.algorithm = algorithm;
        this.model = model;
    }

    public String getAspect() {
        return aspect;
    }

    public void setAspect(String aspect) {
        this.aspect = aspect;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @Override
    public int hashCode() {
        return hashCodeOops();
    }

    @Override
     public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        return equalsOops((Nest2) obj);
    }
}
