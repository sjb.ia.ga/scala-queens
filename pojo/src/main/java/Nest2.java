package queens.pojo;


public interface Nest2 {
    String getAspect();

    void setAspect(String aspect);

    String getAlgorithm();

    void setAlgorithm(String algorithm);

    String getModel();

    void setModel(String model);
}
