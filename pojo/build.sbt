val jacksonVersion = "2.17.1"

libraryDependencies ++= Seq(
//  "org.wvlet.airframe" % "airframe_3" % "24.6.0",
  "com.google.code.gson" % "gson" % "2.10.1",
  "com.fasterxml.jackson.core" % "jackson-annotations" % jacksonVersion,
  "com.fasterxml.jackson.dataformat" % "jackson-dataformat-xml" % jacksonVersion,
  "com.squareup.moshi" % "moshi" % "1.15.1", // % "runtime",
  "org.scalatest" %% "scalatest" % "3.2.18" % Test
)
